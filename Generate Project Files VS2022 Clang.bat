@echo off

if not exist build mkdir build
pushd build
cmake .. -G "Visual Studio 17 2022" -T ClangCL -A x64 -DVULKAN_BUILD=TRUE -DDESKTOP_BUILD=TRUE -Wno-dev
popd
pause
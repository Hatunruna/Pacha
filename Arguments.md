# Command Line Arguments
| Command  | Description | Example |
| :--------: | ----------- | ------- |
| -wd               | Sets the working directory a.k.a the project path   | -wd=C:\ProjectPath      |
| -onethread        | Run the scheduler in single threaded mode           | -onethread              |
| -headless         | Run in headless mode                                | -headless               |
| -fibreStackSize   | Sets the fibre stack size in bytes                  | -fibreStackSize=1048576 |
| -schedulerThreads | Sets the number of threads for the scheduler to use | -schedulerThreads=8     |

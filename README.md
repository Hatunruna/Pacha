# Pacha
Open source game engine.

## Features:
- TODO

## Requirements
- CMake 3.25+
- Latest Vulkan SDK

##### Windows:
- Visual Studio

##### Linux:
- Make
- GCC or Clang

## Tested on:
- GCC13
- Clang14
- Visual Studio 2022

## Compiling
- Install the requirements.
- Clone repo and update submodules.
- Run the provided .bat/.sh.
- Alternatively use the CMakeTools extension for VSCode or CLion
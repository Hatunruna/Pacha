#pragma once
#include "Pacha.hlsli"

namespace Pacha
{
	enum class VertexLayout
	{
		CUSTOM				= 0,
		COMPRESSED			= 1 << 0,
		COMPRESSED_ALPHA	= 1 << 1,
		UNCOMPRESSED		= 1 << 2,
		UNCOMPRESSED_ALPHA	= 1 << 3
	};
}
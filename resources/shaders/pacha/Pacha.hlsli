#pragma once

#ifdef __spirv__
#define PUSH_CONSTANT [[vk::push_constant]]
#else
#define PUSH_CONSTANT 
#endif

namespace Pacha
{
	struct RenderBatch
	{
		uint batchOffset;
	
		//Bindless
		uint vertexFormat;
		uint vertexDataOffset;
		uint vertexBufferIndex;
	};
	
	struct GlobalState
	{
		float delta;
		float time;
		uint frame;
	};
	
	struct SceneViewData
	{
		float4x4 viewProjection;
		float3 worldPos;
	};

	struct InstanceData
	{
		float4x4 model;
		float3x3 normalMatrix;
	};
}

PUSH_CONSTANT
ConstantBuffer<Pacha::RenderBatch> BatchInfo;

ConstantBuffer <Pacha::GlobalState> GlobalState : register(b0, space0);
StructuredBuffer<Pacha:: InstanceData> InstanceData : register(u0, space0);

//Per Pass Data in space1

ConstantBuffer<Pacha::SceneViewData> SceneViewData : register(b0, space2);
StructuredBuffer<uint> InstanceIndex : register(u0, space2);

//Material data in space3
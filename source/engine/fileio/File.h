#pragma once
#include <memory>
#include <fstream>
#include <filesystem>

namespace Pacha
{
	enum class OpenMode
	{
		BINARY,
		TEXT
	};

	class File
	{
	public:
		File() = default;
		File(const std::filesystem::path& path, const OpenMode = OpenMode::BINARY);
		~File();

		bool OpenFile();
		bool CloseFile();
		
		std::string GetTextData();
		std::shared_ptr<uint8_t> GetBinaryData();

		size_t GetFileSize() const;
		size_t GetLastWriteTimestamp();
		std::ifstream& GetFileStream();

		static bool OpenFile(const std::filesystem::path& path, File& file, const OpenMode mode = OpenMode::BINARY);
		static bool Exists(const std::filesystem::path& path);
		static size_t GetLastWriteTimestamp(const std::filesystem::path& path);
	
	private:
		size_t m_Size;
		OpenMode m_Mode;
		std::ifstream m_File;
		std::filesystem::path m_Path;
		std::shared_ptr<uint8_t> m_BinaryData;
	};
}
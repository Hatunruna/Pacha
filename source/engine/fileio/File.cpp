#include "File.h"
#include "core/Log.h"

#include <cstring>

namespace Pacha
{
	File::File(const std::filesystem::path& path, const OpenMode openMode)
	{
		m_Path = path;
		m_Mode = openMode;
	}

	File::~File()
	{
		if (m_File.is_open())
			m_File.close();
	}

	bool File::OpenFile()
	{
		if (m_Mode == OpenMode::BINARY)
			m_File.open(m_Path, std::ios::binary | std::ios::ate);
		else
			m_File.open(m_Path, std::ios::ate);

		if (!m_File.is_open())
		{
			LOGERROR("Error while opening file: "<< m_Path);
			return false;
		}

		m_Size = static_cast<size_t>(m_File.tellg());
		m_File.seekg(0, std::ios::beg);
		return true;
	}

	bool File::CloseFile()
	{
		if (m_File.is_open())
		{
			m_File.close();
			return true;
		}

		return false;
	}

	std::string File::GetTextData()
	{
		std::string content{ std::istreambuf_iterator<char>(m_File), std::istreambuf_iterator<char>() };
		return content;
	}

	std::shared_ptr<uint8_t> File::GetBinaryData()
	{
		if (!m_BinaryData)
		{
			m_BinaryData = std::shared_ptr<uint8_t>(new uint8_t[m_Size]);
			if (!m_File.read(reinterpret_cast<char*>(m_BinaryData.get()), m_Size))
			{
				LOGERROR("Error while reading data from file: " << m_Path);
				return nullptr;
			}
		}

		return m_BinaryData;
	}

	size_t File::GetFileSize() const
	{
		return m_Size;
	}

	size_t File::GetLastWriteTimestamp()
	{
		return GetLastWriteTimestamp(m_Path);
	}

	std::ifstream& File::GetFileStream()
	{
		return m_File;
	}

	bool File::OpenFile(const std::filesystem::path& path, File& file, const OpenMode mode)
	{
		if (!File::Exists(path))
			return false;

		file.m_Path = path;
		file.m_Mode = mode;
		return file.OpenFile();
	}

	bool File::Exists(const std::filesystem::path& path)
	{
		return std::filesystem::exists(path);
	}

	size_t File::GetLastWriteTimestamp(const std::filesystem::path& path)
	{
		std::error_code ec;
		std::filesystem::file_time_type time = std::filesystem::last_write_time(path, ec);

		if (ec)
		{
			LOGERROR("Failed to get last write timestamp for file " << path);
			return SIZE_MAX;
		}
		else
			return time.time_since_epoch().count();
	}
}

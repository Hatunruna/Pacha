#pragma once
#include "Version.h"
#include "core/scheduler/Scheduler.h"
#include "core/graph/RenderingExecutionGraph.h"
#include "core/graph/SimulationExecutionGraph.h"

namespace Pacha
{
	class Engine
	{
	public:
		virtual bool Initialize(const int& argc, char* argv[]);
		virtual void Run();
		virtual void Destroy();
	
		void SetFrameLimiter(const int32_t fps);
		void SetVersion(const Version& version);
		void SetVersion(const uint32_t& major, const uint32_t& minor, const uint32_t& patch);
		
		const Version& GetVersion() const;

	protected:
		Version m_Version;

		float m_CappedFPS = -1;
		CpuTimer m_SimulationTimer;
		Scheduler* m_Scheduler = nullptr;
		marl::Event m_CloseThreads = marl::Event(marl::Event::Mode::Manual);
		
		RenderingExecutionGraph m_RenderGraph;
		SimulationExecutionGraph m_SimulationGraph;

		//https://blat-blatnik.github.io/computerBear/making-accurate-sleep-function/
		void PreciseSleep(double seconds);
	};
}
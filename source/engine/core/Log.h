#pragma once
#include <sstream>

#ifdef _MSC_VER
#define DEBUG_BREAK() __debugbreak()
#else
#include <signal.h>
#define DEBUG_BREAK() raise(SIGTRAP)
#endif

#if DEBUG
#define PrintToLog(logType, message)																		\
{																											\
	std::ostringstream oss;																					\
	oss << message;																							\
	std::string filePath = __FILE__;																		\
	std::string fileOnly = filePath.substr(filePath.find_last_of("/\\") + 1, filePath.size());				\
	Pacha::Log::AddLogEntry(logType, "[" + fileOnly + ":" + std::to_string(__LINE__) + "] " + oss.str());	\
}
#else
#define PrintToLog(logType, message)		\
{											\
	std::ostringstream oss;					\
	oss << message;							\
	Log::AddLogEntry(logType, oss.str());	\
}
#endif

#if DEBUG
#define LOGCRITICAL(message)	{ PrintToLog(Pacha::Log::LogType::CRITICAL, message) DEBUG_BREAK(); }
#else
#define LOGCRITICAL(message)	PrintToLog(Pacha::Log::LogType::CRITICAL, message)
#endif

#define LOGERROR(message)		PrintToLog(Pacha::Log::LogType::ERROR, message)
#define LOGWARNING(message)		PrintToLog(Pacha::Log::LogType::WARNING, message)
#define LOGTRACE(message)		PrintToLog(Pacha::Log::LogType::TRACE, message)
#define LOGINFO(message)		PrintToLog(Pacha::Log::LogType::INFO, message)

namespace Pacha
{
	class Log
	{
	public:
		enum class LogType
		{
			INFO,
			TRACE,
			WARNING,
			ERROR,
			CRITICAL
		};

		static void AddLogEntry(const LogType& level, const std::string& message);

	private:
		static std::ofstream s_LogFile;
	};
}
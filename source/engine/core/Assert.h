#pragma once
#include "Log.h"
#include <portable-file-dialogs.h>

#ifdef DEBUG
using pfdMessage = pfd::message;
#define PACHA_ASSERT(condition, message)	\
if(!(condition)) 							\
{ 											\
	std::ostringstream assertOss;			\
	assertOss << message;					\
											\
	pfdMessage prompt("Pacha Assert",		\
	assertOss.str(),						\
	pfd::choice::ok,						\
	pfd::icon::error);						\
											\
	LOGCRITICAL(message)					\
}
#else
#define PACHA_ASSERT(condition, message)
#endif
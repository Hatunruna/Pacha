#include "ProjectConstants.h"
#include "math/Utilities.h"
#include "core/CommandLineArguments.h"

#include <marl/scheduler.h>

#include <filesystem>

namespace Pacha
{
	void ProjectConstants::Initialize()
	{
		CommandLineArguments& cla = CommandLineArguments::GetInstance();
		std::filesystem::path projectPath = ".";
		if ((m_HasWorkingDirectory = cla.GetValue("wd", projectPath)))
		{
			if (projectPath.is_relative())
				m_WorkingDirectory = std::filesystem::absolute(projectPath);
			else
				m_WorkingDirectory = projectPath;

			m_AssetsPath = m_WorkingDirectory / "assets/";
			m_LibraryPath = m_WorkingDirectory / "library/";

			if (!std::filesystem::exists(m_AssetsPath))
				std::filesystem::create_directory(m_AssetsPath);

			if (!std::filesystem::exists(m_LibraryPath))
				std::filesystem::create_directory(m_LibraryPath);
		}

		cla.GetValue("headless", m_HeadlessRendering, false);
		cla.GetValue("onethread", m_SingleThreaded, false);
		cla.GetValue("fibreStackSize", m_DefaultFibreStackSize, SizeMB<size_t>(1));
		cla.GetValue("schedulerThreads", m_NumberOfSchedulerThreads, marl::Thread::numLogicalCPUs());
	}

	bool ProjectConstants::IsSingleThreaded() const
	{
		return m_SingleThreaded;
	}

	bool ProjectConstants::HasWorkingDirectory() const
	{
		return m_HasWorkingDirectory;
	}

	bool ProjectConstants::UsingHeadlessRendering() const
	{
		return m_HeadlessRendering;
	}

	uint32_t ProjectConstants::GetNumberOfSchedulerThreads() const
	{
		return m_NumberOfSchedulerThreads;
	}

	size_t ProjectConstants::GetDefaultFibreStackSize() const
	{
		return m_DefaultFibreStackSize;
	}
}
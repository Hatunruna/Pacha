#include "Log.h"
#include "Engine.h"

#include "level/LevelManager.h"
#include "assets/AssetManager.h"
#include "core/ProjectConstants.h"
#include "core/time/RenderTime.h"
#include "platform/PlatformEvents.h"
#include "core/time/SimulationTime.h"
#include "core/CommandLineArguments.h"
#include "graphics/device/GfxDevice.h"
#include "graphics/renderdoc/RenderDoc.h"
#include "graphics/swapchain/GfxSwapchain.h"
#include "graphics/utilities/GfxMemoryManager.h"
#include "graphics/utilities/GfxBindlessResourceManager.h"


namespace Pacha
{
	bool Engine::Initialize(const int& argc, char* argv[])
	{
		LOGINFO("--- Initializing ---");
		LOGINFO("Pacha " << m_Version.GetString());

		ProjectConstants& projectConstants = ProjectConstants::GetInstance();
		CommandLineArguments::GetInstance().Initialize(argc, argv);
		projectConstants.Initialize();

#if !PACHA_EDITOR
		if (!AssetManager::GetInstance().Initialize())
			return false;
#endif

		GfxWindow& mainWindow = GfxWindow::GetMainWindow();
		if (!mainWindow.Initialize(1280, 720, "Pacha"))
			return false;

		PlatformEvents& eventsHandler = PlatformEvents::GetInstance();
		if (!eventsHandler.Initialize())
			return false;

		GfxDevice& gfxDevice = GfxDevice::GetInstance();
		if (!gfxDevice.Initialize())
			return false;

		GfxSwapchain& gfxSwapchain = GfxSwapchain::GetMainSwapchain();
		if (!gfxSwapchain.Initialize(&mainWindow))
			return false;

		if (GfxDevice::SupportsBindlessResources())
		{
			GfxBindlessResourceManager& bindlessManager = GfxBindlessResourceManager::GetInstance();
			if (!bindlessManager.Initialize())
				return false;
		}

		size_t fibreStackSize = projectConstants.GetDefaultFibreStackSize();
		uint32_t numThreads = projectConstants.IsSingleThreaded() ? 1 : projectConstants.GetNumberOfSchedulerThreads();
		
		m_Scheduler = new Scheduler(numThreads, fibreStackSize);
		if (!m_SimulationGraph.Initialize())
			return false;

		return true;
	}

	void Engine::Run()
	{
		LOGINFO("--- Running ---");

		marl::schedule([&]
		{
			RenderDoc& renderDoc = RenderDoc::GetInstance();
			RenderTime& renderTime = RenderTime::GetInstance();
			GfxSwapchain& gfxSwapchain = GfxSwapchain::GetMainSwapchain();
			m_RenderGraph.Setup(gfxSwapchain);

			while (!m_CloseThreads.isSignalled())
			{
				m_SimulationGraph.WaitForPacketReady();

				const FramePacket& framePacket = m_SimulationGraph.LockFramePacket();
				renderTime.Update();

				if (gfxSwapchain.Prepare())
				{
					renderDoc.BeginFrame();
					{
						m_RenderGraph.Execute(gfxSwapchain, framePacket);
						gfxSwapchain.Present();
					}
					renderDoc.EndFrame();
				}

				m_SimulationGraph.UnlockFramePacket();
			}

			m_RenderGraph.Shutdown();
		});

		PlatformEvents& eventsHandler = PlatformEvents::GetInstance();
		SimulationTime& simulationTime = SimulationTime::GetInstance();

		while (!eventsHandler.ShouldClose())
		{
			eventsHandler.PollEvents();
			if (m_CappedFPS != -1.f)
			{
				const float elapsed = m_SimulationTimer.GetElapsedTime();
				if (elapsed < m_CappedFPS)
					PreciseSleep(m_CappedFPS - elapsed);
			}

			simulationTime.Update();
			m_SimulationTimer.StartCapture();
			m_SimulationGraph.Execute();
			m_SimulationTimer.EndCapture();
		}

		m_CloseThreads.signal();
		m_SimulationGraph.Shutdown();

		m_RenderGraph.WaitTillFinished();
		m_SimulationGraph.WaitTillFinished();
	}

	void Engine::Destroy()
	{
		LOGINFO("--- Destroying ---");

		GfxDevice::GetInstance().WaitIdle();
		GfxSwapchain::GetMainSwapchain().Destroy();

		m_RenderGraph.Destroy();
		m_SimulationGraph.Destroy();
		AssetManager::GetInstance().Release();

		LevelManager::GetInstance().DestroyLevelData();
		GfxBindlessResourceManager::GetInstance().Release();
		GfxMemoryManager::GetInstance().FreeStagingBuffers();
		GfxMemoryManager::GetInstance().DeleteAllPendingObjects();
		delete m_Scheduler;
	}

	void Engine::SetFrameLimiter(int32_t fps)
	{
		if (fps != -1)
			m_CappedFPS = 1.f / static_cast<float>(fps);
		else
			m_CappedFPS = -1.f;
	}

	void Engine::SetVersion(const Version& version)
	{
		m_Version = version;
	}

	void Engine::SetVersion(const uint32_t& major, const uint32_t& minor, const uint32_t& patch)
	{
		m_Version = Version::MakeVersion(major, minor, patch);
	}

	const Version& Engine::GetVersion() const
	{
		return m_Version;
	}

	void Engine::PreciseSleep(double seconds)
	{
		static uint64_t count = 1;
		static double estimate = 0;

		static double mean = 0;
		static double mean2 = 0;

		while (seconds > estimate)
		{
			std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();
			std::this_thread::sleep_for(std::chrono::milliseconds(1));
			std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();

			double observed = (end - start).count() / 1e9;
			seconds -= observed;

			++count;
			double delta = observed - mean;
			mean += delta / count;
			mean2 += delta * (observed - mean);
			double stddev = sqrt(mean2 / (count - 1));
			estimate = mean + stddev;
		}

		std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();
		while ((std::chrono::high_resolution_clock::now() - start).count() / 1e9 < seconds);
	}
}
#include "Scheduler.h"

namespace Pacha
{
	Scheduler::Scheduler(uint32_t threadCount, size_t fiberStackSize)
	{
		marl::Scheduler::Config config;
		config.setWorkerThreadCount(threadCount);
		config.setFiberStackSize(fiberStackSize);

		m_Scheduler = new marl::Scheduler(config);
		m_Scheduler->bind();
	}
	
	Scheduler::~Scheduler()
	{
		m_Scheduler->unbind();
		delete m_Scheduler;
	}
}
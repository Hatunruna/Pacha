#pragma once
#include <marl/event.h>
#include <marl/waitgroup.h>
#include <marl/scheduler.h>

namespace Pacha
{
	class Scheduler
	{
	public:
		Scheduler(uint32_t threadCount = marl::Thread::numLogicalCPUs(), size_t fiberStackSize = marl::Scheduler::Config::DefaultFiberStackSize);
		~Scheduler();

	private:
		marl::Scheduler* m_Scheduler = nullptr;
	};
}
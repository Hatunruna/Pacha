#pragma once
#include "ExecutionGraph.h"
#include "platform/GfxWindow.h"
#include "core/graph/SimulationTask.h"
#include "collections/LockableCircularArray.h"
#include "graphics/renderpipeline/FramePacket.h"

#include <string>
#include <atomic>
#include <vector>
#include <unordered_map>
#include <marl/waitgroup.h>

namespace Pacha
{
	class SimulationExecutionGraph : public ExecutionGraph
	{
	public:
		bool Initialize();
		void Destroy();
		void Shutdown() override;

		bool Validate();
		void Execute();

		void WaitForPacketReady();
		const FramePacket& LockFramePacket();
		void UnlockFramePacket();

	private:
		template<typename T>
		bool AddTask(const PachaId& id)
		{
			T* task = new T(id);
			if (!task->Setup())
				return false;

			m_Tasks.push_back(task);
			return true;
		}

		marl::Event m_FramePacketReady;
		std::vector<SimulationTask*> m_Tasks;
		std::unordered_map<PachaId, marl::Event> m_Events;

		int32_t m_FramePacketIndex = 0;
		LockableCircularArray<FramePacket, 8> m_FramePackets;

		glm::uvec2 m_WindowSize = glm::ivec2(1);
		void OnMainWindowResized(GfxWindow* window);

#if !PACHA_SHIPPING
		std::deque<ProfilerStats> m_LastFrameStats;
		CpuProfiler m_CpuProfiler = CpuProfiler("Game Thread"_ID);
		void GatherCpuProfilerTimings();
#endif
	};
}

#define AddTask(type) if(!AddTask<type>(#type##_ID)) return false
#include "SimulationTask.h"
#include "core/graph/SimulationExecutionGraph.h"

namespace Pacha
{
	bool SimulationTask::Setup()
	{
		return false;
	}

	SimulationTask::SimulationTask(const PachaId& id)
		CPU_PROFILER_CONSTRUCTOR(id)
	{
		m_TaskId = id;
	}

	void SimulationTask::WaitsOn(const PachaId& id)
	{
		m_WaitIds.resize(1);
		m_WaitIds[0] = id;
	}

	void SimulationTask::WaitsOn(const std::initializer_list<PachaId>& ids)
	{
		m_WaitIds = ids;
	}

	void SimulationTask::Signals(const PachaId& id)
	{
		m_SignalId = id;
	}

	const PachaId& SimulationTask::GetSignalId()
	{
		return m_SignalId;
	}

	const std::vector<PachaId>& SimulationTask::GetWaitIds()
	{
		return m_WaitIds;
	}

	const char* SimulationTask::GetName()
	{
		return m_TaskId.GetName();
	}

	const PachaId& SimulationTask::GetId()
	{
		return m_TaskId;
	}
}
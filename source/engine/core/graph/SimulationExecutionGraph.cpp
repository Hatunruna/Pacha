#include "SimulationExecutionGraph.h"

#include "level/LevelManager.h"
#include "core/SystemInfo.h"
#include "core/ProjectConstants.h"
#include "platform/PlatformEvents.h"
#include "simulation/input/InputTask.h"
#include "simulation/animation/AnimationTask.h"
#include "simulation/update/ScriptsUpdateTask.h"
#include "simulation/update/ComponentsUpdateTask.h"
#include "simulation/synchronization/SynchronizationTask.h"

#if PACHA_EDITOR
#include "tasks/EditorUITask.h"
#endif

namespace Pacha
{
	bool SimulationExecutionGraph::Initialize()
	{
		PlatformEvents::GetInstance().AddMainWindowResizeCallback(this, &SimulationExecutionGraph::OnMainWindowResized);

		AddTask(InputTask);
		AddTask(AnimationTask);
		AddTask(ScriptsUpdateTask);
		AddTask(SynchronizationTask);
		AddTask(ComponentsUpdateTask);
		
#if PACHA_EDITOR
		if(!ProjectConstants::GetInstance().UsingHeadlessRendering())
			AddTask(EditorUITask);
#endif

		return Validate();
	}

	void SimulationExecutionGraph::Destroy()
	{
		PlatformEvents::GetInstance().RemoveMainWindowResizeCallback(this);
		
		for (auto& task : m_Tasks)
			delete task;

		m_Tasks.clear();
	}

	void SimulationExecutionGraph::Shutdown()
	{
		m_FramePacketReady.signal();
		ExecutionGraph::Shutdown();
	}

	bool SimulationExecutionGraph::Validate()
	{
		std::unordered_set<PachaId> signalingIds;

		m_Events.clear();
		for (auto& system : m_Tasks)
		{
			const PachaId& signalId = system->GetSignalId();
			if (signalId)
			{
				auto insertion = signalingIds.insert(signalId);
				if (!insertion.second)
				{
					LOGCRITICAL(system->GetName() << ": More than one system wants to signal the same event " << signalId.GetName());
					return false;
				}

				m_Events.emplace(signalId, marl::Event(marl::Event::Mode::Manual));

				const std::vector<PachaId>& waitIds = system->GetWaitIds();
				for (auto const& id : waitIds)
				{
					if (id == signalId)
					{
						LOGCRITICAL(system->GetName() << ": A system can't signal and wait fot the same event: " << signalId.GetName());
						return false;
					}
				}
			}
		}

		for (auto& system : m_Tasks)
		{
			for (auto waitId : system->GetWaitIds())
			{
				if (!signalingIds.count(waitId))
				{
					LOGCRITICAL(system->GetName() << ": Trying to wait for an event that is never signaled: " << waitId.GetName());
					return false;
				}
			}
		}

		return true;
	}

	void SimulationExecutionGraph::Execute()
	{
		NOT_SHIPPING(m_CpuProfiler.BeginProfiler();)

		Level* currentLevel = LevelManager::GetInstance().GetCurrentLevel();
		SimulationTime& simulationTime = SimulationTime::GetInstance();
		
		FramePacket& framePacket = m_FramePackets.Get();
		framePacket.simulationFrame = simulationTime.GetFrameCount();
		framePacket.viewportResolution = m_WindowSize;
		NOT_SHIPPING(framePacket.cpuProfilerStats = m_LastFrameStats;)
		framePacket.Clear();

		SimulationContext context
		(
			currentLevel,
			framePacket,
			simulationTime
		);

		marl::WaitGroup tasksWaitGroup(static_cast<uint32_t>(m_Tasks.size()));
		for (auto& task : m_Tasks)
		{
			marl::schedule([&, tasksWaitGroup]
			{
				for (const PachaId& eventId : task->GetWaitIds())
					m_Events.at(eventId).wait();

				CPU_BEGIN_PROFILING(task);
				task->Execute(context);
				CPU_END_PROFILING(task);

				const PachaId& signalId = task->GetSignalId();
				if (signalId)
					m_Events.at(signalId).signal();

				tasksWaitGroup.done();
			});
		}
		tasksWaitGroup.wait();

#if !PACHA_SHIPPING
		m_CpuProfiler.EndProfiler();
		GatherCpuProfilerTimings();
#endif

		m_FramePacketIndex = m_FramePackets.GetIndex();
		m_FramePacketReady.signal();
	}

	void SimulationExecutionGraph::WaitForPacketReady()
	{
		m_FramePacketReady.wait();
	}

	const FramePacket& SimulationExecutionGraph::LockFramePacket()
	{
		return m_FramePackets.LockElement(m_FramePacketIndex);
	}

	void SimulationExecutionGraph::UnlockFramePacket()
	{
		m_FramePackets.UnLockElement();
	}

	void SimulationExecutionGraph::OnMainWindowResized(GfxWindow* window)
	{
		m_WindowSize = window->GetSize();
	}

#if !PACHA_SHIPPING
	void SimulationExecutionGraph::GatherCpuProfilerTimings()
	{
		m_LastFrameStats.clear();

		ProfilerStats& root = m_LastFrameStats.emplace_back();
		root.id = m_CpuProfiler.GetId();
		root.time = m_CpuProfiler.GetAverageCaptureTime();

		for (auto& task : m_Tasks)
		{
			root.children.push_back(static_cast<uint32_t>(m_LastFrameStats.size()));

			CpuProfiler& taskProfiler = task->GetProfiler();
			CpuPerformanceTimer& timer = taskProfiler.GetTimer();
			GatherTimerEntryStats(timer, m_LastFrameStats);
		}

		ProfilerStats& untracked = m_LastFrameStats.emplace_back();
		untracked.id = "Untracked"_ID;
		untracked.time = SimulationTime::GetInstance().GetDelta() - root.time;
	}
#endif
}
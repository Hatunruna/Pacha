#include "SimulationContext.h"

namespace Pacha
{
	SimulationContext::SimulationContext(Level* level, FramePacket& framePacket, const SimulationTime& gameTime)
		: m_Level(level)
		, m_FramePacket(framePacket)
		, m_SimulationTime(gameTime)
	{ }
	
	Level* SimulationContext::GetLevel() const
	{
		return m_Level;
	}

	FramePacket& SimulationContext::GetFramePacket() const
	{
		return m_FramePacket;
	}

	const SimulationTime& SimulationContext::GetSimulationTime() const
	{
		return m_SimulationTime;
	}
}
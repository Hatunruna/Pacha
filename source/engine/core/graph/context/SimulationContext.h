#pragma once
#include "level/LevelManager.h"
#include "core/time/SimulationTime.h"
#include "graphics/renderpipeline/FramePacket.h"

#include <entt/entt.hpp>

namespace Pacha
{
	template<typename T>
	class StorageHolder
	{
		friend class SimulationContext;

	public:
		StorageHolder(bool reserve2)
		{
			if(reserve2)
				m_Views.resize(2);
			else
				m_Views.resize(1);
		}

		template<typename Function>
		void ForEach(const Function& function)
		{
			for(const auto& view : m_Views)
				view.each(function);
		}

		size_t Size()
		{
			size_t size = 0;
			for (const auto& view : m_Views)
				size += view.size_hint();
			return size;
		}

	private:
		std::vector<T> m_Views;
	};

	class SimulationContext
	{
	public:
		SimulationContext(Level* level, FramePacket& framePacket, const SimulationTime& simulationTime);
		
		Level* GetLevel() const;
		FramePacket& GetFramePacket() const;
		const SimulationTime& GetSimulationTime() const;

		template<typename ...Args>
		auto GetView(Level* level)
		{
			return entt::basic_view{ level->GetStorage<Args>()... };
		}

		template<typename ...Args>
		auto GetViews()
		{
			Level* persistent = LevelManager::GetInstance().GetPersistentLevel();
			auto persistentView = GetView<Args...>(persistent);
			
			StorageHolder<decltype(persistentView)> holder(m_Level);
			std::swap(holder.m_Views[0], persistentView);

			if (m_Level)
			{
				auto levelView = GetView<Args...>(m_Level);
				std::swap(holder.m_Views[1], levelView);
			}

			return holder;
		}

	private:
		Level* m_Level = nullptr;
		FramePacket& m_FramePacket;
		const SimulationTime& m_SimulationTime;
	};
}
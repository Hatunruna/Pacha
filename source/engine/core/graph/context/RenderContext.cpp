#include "RenderContext.h"

namespace Pacha
{
	RenderContext::RenderContext(const FramePacket& framePacket)
		: m_FramePacket(framePacket)
	{
	}
	
	const FramePacket& RenderContext::GetFramePacket() const
	{
		return m_FramePacket;
	}

	GfxSceneView& RenderContext::AddSceneView(const GfxSceneViewDescriptor& descriptor)
	{
		if (!m_SceneViews.count(descriptor))
			m_SceneViews[descriptor] = GfxSceneView(descriptor);
		return m_SceneViews[descriptor];
	}

	GfxSceneView& RenderContext::GetSceneView(const GfxSceneViewDescriptor& descriptor)
	{
		PACHA_ASSERT(m_SceneViews.count(descriptor), "Missing SceneView for requested descriptor");
		return m_SceneViews[descriptor];
	}
	
	std::unordered_map<GfxSceneViewDescriptor, GfxSceneView>& RenderContext::GetSceneViews()
	{
		return m_SceneViews;
	}

	const std::vector<const GfxSceneView*> RenderContext::GetCompatibleSceneViews(GfxRenderBatchDescriptor& renderBatchDescriptor)
	{
		std::vector<const GfxSceneView*> sceneViews = {};
		for (auto& [descriptor, sceneView] : m_SceneViews)
		{
			if (sceneView.GetDescriptor().type & renderBatchDescriptor.sceneViewType)
				sceneViews.push_back(&sceneView);
		}

		return sceneViews;
	}
}
#pragma once
#include "graphics/renderpipeline/FramePacket.h"
#include "graphics/renderpipeline/GfxSceneView.h"

#include <vector>

namespace Pacha
{
	class RenderContext
	{
	public:
		RenderContext(const FramePacket& framePacket);
		const FramePacket& GetFramePacket() const;

		GfxSceneView& AddSceneView(const GfxSceneViewDescriptor& descriptor);
		GfxSceneView& GetSceneView(const GfxSceneViewDescriptor& descriptor);
		std::unordered_map<GfxSceneViewDescriptor, GfxSceneView>& GetSceneViews();
		const std::vector<const GfxSceneView*> GetCompatibleSceneViews(GfxRenderBatchDescriptor& renderBatchDescriptor);

	private:
		const FramePacket& m_FramePacket;
		std::unordered_map<GfxSceneViewDescriptor, GfxSceneView> m_SceneViews;
	};
}
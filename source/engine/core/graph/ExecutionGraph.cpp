#include "ExecutionGraph.h"
#include <marl/defer.h>

namespace Pacha
{
	void ExecutionGraph::Shutdown()
	{
		m_HasFinished.signal();
	}

	void ExecutionGraph::WaitTillFinished()
	{
		m_HasFinished.wait();
	}
}
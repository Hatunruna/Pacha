#include "CpuTaskProfiler.h"

namespace Pacha
{
	CpuTaskProfiler::CpuTaskProfiler(const PachaId& id)
		: m_TaskProfiler(id)
	{
	}

	void CpuTaskProfiler::BeginProfiling()
	{
		m_TaskProfiler.BeginProfiler();
	}

	void CpuTaskProfiler::EndProfiling()
	{
		m_TaskProfiler.EndProfiler();
	}

	CpuProfiler& CpuTaskProfiler::GetProfiler()
	{
		return m_TaskProfiler;
	}
}
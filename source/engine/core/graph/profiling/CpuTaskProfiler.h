#pragma once
#include "utilities/profiling/CpuProfiler.h"

#if PACHA_SHIPPING
#define CPU_PROFILER_CONSTRUCTOR(id)
#define CPU_INHERIT_PROFILER 

#define CPU_BEGIN_PROFILING(system)
#define CPU_END_PROFILING(system)

#define CPU_PROFILER_BEGIN_CAPTURE(event)
#define CPU_PROFILER_END_CAPTURE(event)
#define CPU_PROFILER_CAPTURE_SCOPE(event)
#define CPU_PROFILER_CAPTURE_SCOPE_PARENT_EVENT(event, parentEvent)
#else
#define CPU_PROFILER_CONSTRUCTOR(id) : CpuTaskProfiler(id)
#define CPU_INHERIT_PROFILER : public CpuTaskProfiler 

#define CPU_BEGIN_PROFILING(task) task->CpuTaskProfiler::BeginProfiling()
#define CPU_END_PROFILING(task) task->CpuTaskProfiler::EndProfiling()

#define CPU_PROFILER_BEGIN_CAPTURE(event) m_TaskProfiler.BeginCapture(#event##_ID)
#define CPU_PROFILER_END_CAPTURE(event) m_TaskProfiler.EndCapture(#event##_ID)
#define CPU_PROFILER_CAPTURE_SCOPE(event) ScopedProfilerCapture capture##event(#event##_ID, m_TaskProfiler)
#define CPU_PROFILER_CAPTURE_SCOPE_PARENT_EVENT(event, parentEvent) ScopedProfilerCapture capture##event(#event##_ID, capture##parentEvent.GetProfiler())
#endif

namespace Pacha
{
	class CpuTaskProfiler
	{
	public:
		CpuTaskProfiler(const PachaId& id);

		void BeginProfiling();
		void EndProfiling();
		CpuProfiler& GetProfiler();

	protected:
		CpuProfiler m_TaskProfiler;
	};
}

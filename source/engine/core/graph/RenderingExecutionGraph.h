#pragma once
#include "ExecutionGraph.h"
#include "utilities/profiling/CpuProfiler.h"
#include "graphics/swapchain/GfxSwapchain.h"
#include "core/graph/context/RenderContext.h"
#include "graphics/allocators/CommandBufferAllocator.h"

namespace Pacha
{
	class RenderingExecutionGraph : public ExecutionGraph
	{
	public:
		void Setup(GfxSwapchain& swapchain);
		void Execute(GfxSwapchain& swapchain, const FramePacket& framePacket);
		void Destroy();

	private:
		class GfxRenderPipeline* m_RenderPipeline = nullptr;
		std::vector<CommandBufferAllocator> m_CommandBufferAllocators;

#if !PACHA_SHIPPING
		void GatherCpuProfilerTimings();
		void GatherGpuProfilerTimings();
		CpuProfiler m_RenderThreadProfiler = CpuProfiler("Render Thread"_ID);
#endif
	};
}
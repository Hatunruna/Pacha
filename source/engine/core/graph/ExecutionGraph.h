#pragma once
#include <string>
#include <thread>
#include <marl/event.h>

namespace Pacha
{
	class ExecutionGraph
	{
	public:
		virtual void Shutdown();
		virtual void WaitTillFinished();
	
	private:
		marl::Event m_HasFinished;
	};
}
#pragma once
#include <string>

#include "core/Log.h"
#include "level/Level.h"
#include "utilities/Hash.h"
#include "core/graph/profiling/CpuTaskProfiler.h"
#include "core/graph/context/SimulationContext.h"

#define TASK_CONSTRUCTOR using SimulationTask::SimulationTask

namespace Pacha
{
	class SimulationTask CPU_INHERIT_PROFILER
	{
	public:
		SimulationTask(const PachaId& id);
		virtual ~SimulationTask() {};

		virtual bool Setup() = 0;
		virtual void Execute(SimulationContext& context) = 0;

		void WaitsOn(const PachaId& id);
		void WaitsOn(const std::initializer_list<PachaId>& ids);
		void Signals(const PachaId& id);

		const PachaId& GetSignalId();
		const std::vector<PachaId>& GetWaitIds();

		const PachaId& GetId();
		const char* GetName();

	private:
		std::vector<PachaId> m_WaitIds;
		PachaId m_TaskId;
		PachaId m_SignalId;
	};
}
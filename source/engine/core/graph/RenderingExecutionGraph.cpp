#include "RenderingExecutionGraph.h"

#include "core/SystemInfo.h"
#include "core/time/RenderTime.h"
#include "graphics/renderpass/GfxRenderPass.h"
#include "graphics/renderpipeline/FramePacket.h"
#include "graphics/utilities/GfxMemoryManager.h"
#include "graphics/renderpipeline/StandardRenderPipeline.h"

#if PACHA_EDITOR
#include "graphics/imgui/ImGuiRendererBackend.h"
#endif

namespace Pacha
{
	void RenderingExecutionGraph::Setup(GfxSwapchain& swapchain)
	{
		size_t backBuffersCount = swapchain.GetBackBufferCount();
		m_CommandBufferAllocators.resize(backBuffersCount);
		m_RenderPipeline = new StandardRenderPipeline();
	}

	void RenderingExecutionGraph::Execute(GfxSwapchain& swapchain, const FramePacket& framePacket)
	{
#if !PACHA_SHIPPING
		m_RenderThreadProfiler.BeginProfiler();
		m_RenderThreadProfiler.StartCapture("Command Buffer Allocation"_ID);
#endif

		const uint32_t frameIndex = swapchain.GetCurrentFrameIndex();
		CommandBufferAllocator& cmdBufferAlloctor = m_CommandBufferAllocators[frameIndex];

		RenderContext context(framePacket);
		cmdBufferAlloctor.Reserve(m_RenderPipeline->GetPassesCount());

		NOT_SHIPPING(m_RenderThreadProfiler.EndCapture("Command Buffer Allocation"_ID);)

		m_RenderPipeline->Setup(context);
		m_RenderPipeline->Render(context, cmdBufferAlloctor);

		NOT_SHIPPING(m_RenderThreadProfiler.StartCapture("Memory Management"_ID);)

		GfxMemoryManager& memoryManager = GfxMemoryManager::GetInstance();
		swapchain.AddCommandBuffer(memoryManager.GetUploadCommandBuffer());
		swapchain.AddCommandBuffers(m_RenderPipeline->GetSubmissionOrderedCommandBuffers());
		memoryManager.DeletePendingObjects();

#if !PACHA_SHIPPING
		m_RenderThreadProfiler.EndCapture("Memory Management"_ID);
		m_RenderThreadProfiler.EndProfiler();
		GatherCpuProfilerTimings();
		GatherGpuProfilerTimings();
#endif
	}

	void RenderingExecutionGraph::Destroy()
	{
		delete m_RenderPipeline;
	}

#if !PACHA_SHIPPING
	void RenderingExecutionGraph::GatherCpuProfilerTimings()
	{
		std::deque<ProfilerStats> stats = {};
		const CpuProfiler& renderPipelineSetupProfiler = m_RenderPipeline->GetSetupCpuProfiler();
		const CpuProfiler& renderPipelineRenderProfiler = m_RenderPipeline->GetRenderCpuProfiler();
		CpuPerformanceTimer& renderThreadTimer = m_RenderThreadProfiler.GetTimer();
		
		ProfilerStats& renderThread = stats.emplace_back();
		renderThread.id = "RenderThread"_ID;
		renderThread.time = m_RenderThreadProfiler.GetAverageCaptureTime();
		renderThread.children.push_back(static_cast<uint32_t>(stats.size()));

		CpuPerformanceTimer& commandBufferAllocationTimer = renderThreadTimer.GetChild("Command Buffer Allocation"_ID);
		ProfilerStats& renderThreadCmdBufferAlloc = stats.emplace_back();
		renderThreadCmdBufferAlloc.id = commandBufferAllocationTimer.GetId();
		renderThreadCmdBufferAlloc.time = commandBufferAllocationTimer.GetAverageCaptureTime();
		renderThread.children.push_back(static_cast<uint32_t>(stats.size()));

		CpuPerformanceTimer& memoryManagementTimer = renderThreadTimer.GetChild("Memory Management"_ID);
		ProfilerStats& renderThreadMemoryManagement = stats.emplace_back();
		renderThreadMemoryManagement.id = memoryManagementTimer.GetId();
		renderThreadMemoryManagement.time = memoryManagementTimer.GetAverageCaptureTime();
		renderThread.children.push_back(static_cast<uint32_t>(stats.size()));
		
		ProfilerStats& renderThreadSetup = stats.emplace_back();
		renderThreadSetup.id = renderPipelineSetupProfiler.GetId();
		renderThreadSetup.time = renderPipelineSetupProfiler.GetAverageCaptureTime();
		renderThread.children.push_back(static_cast<uint32_t>(stats.size()));

		ProfilerStats& renderThreadRender = stats.emplace_back();
		renderThreadRender.id = renderPipelineRenderProfiler.GetId();
		renderThreadRender.time = renderPipelineRenderProfiler.GetAverageCaptureTime();

		const std::vector<GfxRenderPass*> renderPasses = m_RenderPipeline->GetExecutableRenderPasses();
		for (auto& renderPass : renderPasses)
		{
			renderThreadRender.children.push_back(static_cast<uint32_t>(stats.size()));

			CpuProfiler& taskProfiler = renderPass->CpuTaskProfiler::GetProfiler();
			CpuPerformanceTimer& timer = taskProfiler.GetTimer();
			GatherTimerEntryStats(timer, stats);
		}

		RenderTime& renderTime = RenderTime::GetInstance();

		ProfilerStats& untracked = stats.emplace_back();
		untracked.id = "Untracked"_ID;
		untracked.time = renderTime.GetDelta() - renderThread.time;

		renderTime.SetCpuProfilerStats(stats);
	}

	void RenderingExecutionGraph::GatherGpuProfilerTimings()
	{
		std::deque<ProfilerStats> stats = {};
		ProfilerStats& renderPipeline = stats.emplace_back();
		renderPipeline.id = m_RenderPipeline->GetId();

		const std::vector<GfxRenderPass*> renderPasses = m_RenderPipeline->GetExecutableRenderPasses();
		for (auto& renderPass : renderPasses)
		{
			renderPipeline.children.push_back(static_cast<uint32_t>(stats.size()));

			GpuProfiler& taskProfiler = renderPass->RenderPassProfiler::GetProfiler();
			GpuPerformanceTimer& timer = taskProfiler.GetTimer();
			GatherTimerEntryStats(timer, stats);
		}

		for (auto& child : renderPipeline.children)
			renderPipeline.time += stats[child].time;

		RenderTime::GetInstance().SetGpuProfilerStats(stats);
	}
#endif
}

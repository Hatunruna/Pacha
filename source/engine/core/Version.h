#pragma once
#include <string>
#include <stdint.h>

namespace Pacha
{
	struct Version
	{
		uint32_t major;
		uint32_t minor;
		uint32_t patch;

		static Version MakeVersion(const uint32_t& major, const uint32_t& minor, const uint32_t& patch)
		{
			return { major, minor, patch };
		}

		std::string GetString()
		{
			return "v" + std::to_string(major) + "." + std::to_string(minor) + "." + std::to_string(patch);
		}
	};
}
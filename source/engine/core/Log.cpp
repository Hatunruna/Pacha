#include "Log.h"
#include "fileio/File.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>

#if DEBUG
#if PLATFORM_WINDOWS
#include <Windows.h>
#include <debugapi.h>
#endif
#endif

#include <marl/ticket.h>

namespace Pacha
{
	std::ofstream Log::s_LogFile;
	const std::string kLogPrefixes[] =
	{
		"[INFO]",
		"[TRACE]",
		"[WARNING]",
		"[ERROR]",
		"[CRITICAL]"
	};

	void Log::AddLogEntry(const LogType& level, const std::string& message)
	{
		std::stringstream ss;
		std::time_t t = std::time(nullptr);
		ss << std::put_time(std::gmtime(&t), "[%T]");

		std::string logOutput = kLogPrefixes[static_cast<uint32_t>(level)] + ss.str() + message;

		if (!s_LogFile.is_open())
		{
			const std::filesystem::path logPath = "./Pacha.log";
			if (File::Exists(logPath))
			{
				const std::filesystem::path lastLogPath = "./Pacha_Last.log";
				std::filesystem::rename(logPath, lastLogPath);
			}
			
			s_LogFile.open(logPath, std::ios_base::out);
		}

		static marl::Ticket::Queue sTicketQueue;
		marl::Ticket ticket = sTicketQueue.take();
		ticket.wait();

		if(level != LogType::INFO)
			std::cout << logOutput << std::endl;

#if DEBUG
#if PLATFORM_WINDOWS
		OutputDebugString((logOutput + "\n").c_str());
#else
		printf("%s", (logOutput + "\n").c_str());
#endif
#endif

		s_LogFile << logOutput << std::endl;
		ticket.done();
	}
}
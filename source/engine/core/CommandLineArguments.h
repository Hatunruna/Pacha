#pragma once
#include "core/Log.h"

#include <argh.h>

namespace Pacha
{
	class CommandLineArguments
	{
	private:
		CommandLineArguments() = default;
		CommandLineArguments(const CommandLineArguments&) = delete;
		CommandLineArguments& operator=(const CommandLineArguments&) = delete;

	public:
		static CommandLineArguments& GetInstance()
		{
			static CommandLineArguments sInstance;
			return sInstance;
		}

		void Initialize(const int& argc, char* argv[])
		{
			m_CLA.parse(argc, argv);
		}

		template<typename T>
		bool GetValue(const std::string& key, T& value)
		{
			if (!(m_CLA(key) >> value))
			{
				LOGINFO("Command Line Argument '" << key << "' not found");
				return false;
			}

			return true;
		}

		template<typename T>
		bool GetValue(const std::string& key, T& value, const T& default)
		{
			if (!(m_CLA(key, default) >> value))
				return false;
			return true;
		}

	private:
		argh::parser m_CLA;
	};
}
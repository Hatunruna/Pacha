#pragma once

#if PACHA_EDITOR
#define EDITOR_PARAMETER(x) , x
#define IN_EDITOR(x) x
#define NOT_EDITOR(x)
#else
#define EDITOR_PARAMETER(x)
#define IN_EDITOR(x)
#define NOT_EDITOR(x) x
#endif

#if PACHA_SHIPPING
#define SHIPPING_PARAMETER(x) , x
#define IN_SHIPPING(x) x
#define NOT_SHIPPING(x)
#else
#define SHIPPING_PARAMETER(x)
#define IN_SHIPPING(x)
#define NOT_SHIPPING(x) x
#endif

#if PLATFORM_WINDOWS
#define WINDOWS_PARAMETER(x) , x
#else
#define WINDOWS_PARAMETER(x)
#endif

#if PLATFORM_LINUX
#define LINUX_PARAMETER(x) , x
#else
#define LINUX_PARAMETER(x)
#endif

namespace Pacha
{
	namespace SystemInfo
	{
		constexpr bool IsInEditorMode		= PACHA_EDITOR;
		constexpr bool IsInServerMode		= PACHA_SERVER;

		constexpr bool IsRunningOnDesktop	= PLATFORM_DESKTOP;
		constexpr bool IsRunningOnMobile	= PLATFORM_MOBILE;

		constexpr bool IsRunningOnWindows	= PLATFORM_WINDOWS;
		constexpr bool IsRunningOnApple		= PLATFORM_APPLE;
		constexpr bool IsRunningOnIOS		= PLATFORM_IOS;
		constexpr bool IsRunningOnLinux		= PLATFORM_LINUX;
		constexpr bool IsRunningOnAndroid	= PLATFORM_ANDROID;

		constexpr bool RendererIsVulkan		= RENDERER_VK;
		constexpr bool RendererIsDX12		= RENDERER_DX12;
	}
}
#pragma once
#include "utilities/timer/CpuTimer.h"

#include <shared_mutex>

namespace Pacha
{
	class RenderTime
	{
		friend class Engine;
		friend class Editor;
		
	private:
		RenderTime() = default;
		RenderTime(const RenderTime&) = delete;
		RenderTime& operator=(const RenderTime&) = delete;

	public:
		static RenderTime& GetInstance()
		{
			static RenderTime sInstance;
			return sInstance;
		}

		float GetDelta() const;
		float GetSmoothDelta() const;
		uint64_t GetFrameCount() const;
		float GetTimeSinceStart() const;
		CpuPerformanceTimer& GetTimer();

#if !PACHA_SHIPPING
		void SetCpuProfilerStats(const std::deque<ProfilerStats>& stats);
		void SetGpuProfilerStats(const std::deque<ProfilerStats>& stats);

		std::deque<ProfilerStats> GetCpuProfilerStats();
		std::deque<ProfilerStats> GetGpuProfilerStats();
#endif

	private:
		float m_SmoothDelta = 0;
		uint64_t m_FrameCount = 0;
		float m_TimeSinceStart = 0;
		CpuPerformanceTimer m_Timer = CpuPerformanceTimer("RenderTime"_ID);
		
#if !PACHA_SHIPPING
		std::shared_mutex m_StatsMutex;
		std::deque<ProfilerStats> m_CpuProfilerStats;
		std::deque<ProfilerStats> m_GpuProfilerStats;
#endif

		void Update();
	};
}
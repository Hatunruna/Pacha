#pragma once
#include "utilities/timer/CpuTimer.h"

namespace Pacha
{
	class SimulationTime
	{
		friend class Engine;
		friend class Editor;
		
	private:
		SimulationTime() = default;
		SimulationTime(const SimulationTime&) = delete;
		SimulationTime& operator=(const SimulationTime&) = delete;

	public:
		static SimulationTime& GetInstance()
		{
			static SimulationTime sInstance;
			return sInstance;
		}

		float GetDelta() const;
		float GetSmoothDelta() const;
		uint64_t GetFrameCount() const;
		float GetTimeSinceStart() const;
		CpuPerformanceTimer& GetTimer();

	private:
		float m_SmoothDelta = 0;
		uint64_t m_FrameCount = 0;
		float m_TimeSinceStart = 0;
		CpuPerformanceTimer m_Timer = CpuPerformanceTimer("SimulationTime"_ID);

		void Update();
	};
}
#include "RenderTime.h"

namespace Pacha
{
	float RenderTime::GetDelta() const
	{
		return m_Timer.GetAverageCaptureTime();
	}

	float RenderTime::GetSmoothDelta() const
	{
		return m_SmoothDelta;
	}

	uint64_t RenderTime::GetFrameCount() const
	{
		return m_FrameCount;
	}

	float RenderTime::GetTimeSinceStart() const
	{
		return m_TimeSinceStart;
	}

	CpuPerformanceTimer& RenderTime::GetTimer()
	{
		return m_Timer;
	}

	void RenderTime::SetCpuProfilerStats(const std::deque<ProfilerStats>& stats)
	{
		std::unique_lock lock(m_StatsMutex);
		m_CpuProfilerStats = stats;
	}

	void RenderTime::SetGpuProfilerStats(const std::deque<ProfilerStats>& stats)
	{
		std::unique_lock lock(m_StatsMutex);
		m_GpuProfilerStats = stats;
	}

	std::deque<ProfilerStats> RenderTime::GetCpuProfilerStats()
	{
		std::shared_lock lock(m_StatsMutex);
		return m_CpuProfilerStats;
	}

	std::deque<ProfilerStats> RenderTime::GetGpuProfilerStats()
	{
		std::shared_lock lock(m_StatsMutex);
		return m_GpuProfilerStats;
	}

	void RenderTime::Update()
	{
		++m_FrameCount;
		m_Timer.EndCapture();
		m_Timer.ComputeAverage();

		float delta = m_Timer.GetAverageCaptureTime();
		m_TimeSinceStart += delta;
		m_SmoothDelta = (delta * 0.1f) + (0.9f * m_SmoothDelta);

		m_Timer.StartCapture();
	}
}
#include "SimulationTime.h"

namespace Pacha
{
	float SimulationTime::GetDelta() const
	{
		return m_Timer.GetAverageCaptureTime();
	}

	float SimulationTime::GetSmoothDelta() const
	{
		return m_SmoothDelta;
	}

	uint64_t SimulationTime::GetFrameCount() const
	{
		return m_FrameCount;
	}

	float SimulationTime::GetTimeSinceStart() const
	{
		return m_TimeSinceStart;
	}

	CpuPerformanceTimer& SimulationTime::GetTimer()
	{
		return m_Timer;
	}

	void SimulationTime::Update()
	{
		++m_FrameCount;
		m_Timer.EndCapture();
		m_Timer.ComputeAverage();

		float delta = m_Timer.GetAverageCaptureTime();

		m_TimeSinceStart += delta;
		m_SmoothDelta = (delta * 0.1f) + (0.9f * m_SmoothDelta);

		m_Timer.StartCapture();
	}
}
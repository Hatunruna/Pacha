#pragma once
#include <stdint.h>

#if _MSC_VER
#define Placeholder std::_Ph
#define forceinline __forceinline

forceinline uint32_t FindFirstBitSet64(const uint64_t& mask)
{
	if (mask == 0) return 0;

	unsigned long index;
	_BitScanForward64(&index, mask);
	return static_cast<uint32_t>(index);
}

forceinline uint32_t FindLastBitSet64(const uint64_t& mask)
{
	if (mask == 0) return 0;

	unsigned long index;
	_BitScanReverse64(&index, mask);
	return static_cast<uint32_t>(index);
}
#endif

#if __GNUC__
#define Placeholder std::_Placeholder
#define forceinline __attribute__((always_inline))

inline uint32_t FindFirstBitSet64(const uint64_t& mask)
{
	if (mask == 0) return 0;
	return static_cast<uint32_t>(__builtin_ffsll(mask) - 1);
}

inline uint32_t FindLastBitSet64(const uint64_t& mask)
{
	if (mask == 0) return 0;
	return 64 - __builtin_clzll(mask);
}
#endif
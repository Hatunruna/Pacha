#pragma once
#include "core/Assert.h"
#include "core/Engine.h"

#if PACHA_EDITOR
#include "core/Editor.h"
namespace Pacha { typedef Editor Application; }
#else
namespace Pacha { typedef Engine Application; }
#endif

#if PLATFORM_DESKTOP
#include <SDL.h>
#endif
#pragma once
#include <filesystem>

namespace Pacha
{
	class ProjectConstants
	{
	private:
		ProjectConstants() = default;
		ProjectConstants(const ProjectConstants&) = delete;
		ProjectConstants& operator=(const ProjectConstants&) = delete;

	public:
		static ProjectConstants& GetInstance()
		{
			static ProjectConstants sInstance;
			return sInstance;
		}

		void Initialize();
		bool IsSingleThreaded() const;
		bool HasWorkingDirectory() const;
		bool UsingHeadlessRendering() const;
		
		uint32_t GetNumberOfSchedulerThreads() const;
		size_t GetDefaultFibreStackSize() const;

		const std::filesystem::path& GetAssetsPath() { return m_AssetsPath; };
		const std::filesystem::path& GetLibraryPath() { return m_LibraryPath; };
		const std::filesystem::path& GetWorkingDirectory() { return m_WorkingDirectory; };

	private:
		bool m_SingleThreaded = false;
		bool m_HeadlessRendering = false;
		bool m_HasWorkingDirectory = false;
		
		uint32_t m_NumberOfSchedulerThreads = 0;
		size_t m_DefaultFibreStackSize = 0;

		std::filesystem::path m_AssetsPath = ".";
		std::filesystem::path m_LibraryPath = ".";
		std::filesystem::path m_WorkingDirectory = ".";
	};
}
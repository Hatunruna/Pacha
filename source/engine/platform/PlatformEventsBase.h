#pragma once
#include "core/Assert.h"
#include "platform/GfxWindow.h"
#include "core/PlatformInterop.h"

#include <marl/event.h>
#include <unordered_map>

namespace Pacha
{
	typedef std::function<void(GfxWindow*)> OnWindowResize;

	class PlatformEventsBase
	{
	public:
		virtual bool Initialize() = 0;
		virtual void PollEvents() = 0;
		virtual void TriggerQuit() = 0;

		void TriggerInputEvents();
		const bool& ShouldClose();

		template<typename OwnerClass, typename... Types>
		void AddMainWindowResizeCallback(void* owner, void(OwnerClass::* callback)(Types... Args))
		{
			PACHA_ASSERT(!m_OnMainWindowResizedCallbacks.count(owner), "Callback already registered for this event");
			auto function = AutomaticPlaceholderExpansionBind(owner, callback);
			m_OnMainWindowResizedCallbacks[owner] = function;
		}

		void RemoveMainWindowResizeCallback(void* owner);

	protected:
		virtual ~PlatformEventsBase() = default;

		bool m_ShouldClose = false;
		bool m_HasMainWindowResized = false;

	private:
		marl::Event m_PoolingEvent = marl::Event(marl::Event::Mode::Manual);
		std::unordered_map<void*, OnWindowResize> m_OnMainWindowResizedCallbacks;

		template<typename OwnerClass, typename... Types, int... Index>
		auto AutomaticPlaceholderExpansionBind(void* owner, void(OwnerClass::* callback)(Types... Args), std::integer_sequence<int, Index...>)
		{
			return std::bind(callback, static_cast<OwnerClass*>(owner), Placeholder<1 + Index>{}...);
		}

		template<typename OwnerClass, typename... Types>
		auto AutomaticPlaceholderExpansionBind(void* owner, void(OwnerClass::* callback)(Types... Args))
		{
			return AutomaticPlaceholderExpansionBind(owner, callback, std::make_integer_sequence<int, sizeof...(Types)>{});
		}
	};
}
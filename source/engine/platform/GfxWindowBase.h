#pragma once
#include <string>
#include <glm/glm.hpp>

#if PLATFORM_DESKTOP
#define PLATFORM_CLASS PlatformEventsSDL;
#endif

namespace Pacha
{
	enum WindowInitFlagBits
	{
		WINDOW_FULLSCREEN		= 1 << 0,
		WINDOW_HIDDEN			= 1 << 1,
		WINDOW_BORDERLESS		= 1 << 2,
		WINDOW_RESIZABLE		= 1 << 3,
		WINDOW_MINIMIZED		= 1 << 4,
		WINDOW_MAXIMIZED		= 1 << 5,
		WINDOW_ALLOW_HIGHDPI	= 1 << 6,
		WINDOW_ALWAYS_ON_TOP	= 1 << 7,
		WINDOW_NO_TASK_BAR_ICON = 1 << 8
	};
	typedef uint32_t WindowInitFlags;

	enum class WindowMode
	{
		WINDOWED,
		FULLSCREEN,
		WINDOWED_FULLSCREEN
	};

	class GfxWindowBase
	{
		friend class PLATFORM_CLASS;

	public:
		virtual ~GfxWindowBase() {};
		virtual bool Initialize(const uint32_t& sizeX, const uint32_t& sizeY, const std::string& title, WindowInitFlags initFlags = WINDOW_RESIZABLE) = 0;
		WindowMode GetWindowMode();
		
		virtual void SetTitle(const std::string& title) = 0;
		
		virtual void SetSize(const glm::uvec2& size) = 0;
		virtual glm::uvec2 GetSize() = 0;
		
		virtual void SetPosition(const glm::uvec2& position) = 0;
		virtual glm::uvec2 GetPosition() = 0;

		virtual void SetWindowMode(WindowMode mode) = 0;
		
		virtual void SetBorderless(bool state) = 0;
		virtual void SetResizeable(bool state) = 0;
		virtual void SetAlwaysOnTop(bool state) = 0;
		virtual void SetHidden(bool state) = 0;
		virtual void SetAlpha(float alpha) = 0;

		virtual void Focus() = 0;
		virtual void Maximize() = 0;
		virtual void Minimize() = 0;

		virtual bool IsMinimized() const = 0;
		virtual bool IsMaximized() const = 0;
		virtual bool HasFocus() const = 0;
		virtual bool HasInputFocus() const = 0;
		virtual bool HasMouseFocus() const = 0;

		virtual void* GetHandle() const = 0;

	protected:
		WindowMode m_WindowMode;
	};
}
#include "PlatformEvents.h"
#include "platform/GfxWindow.h"

namespace Pacha
{
	void PlatformEventsBase::TriggerInputEvents()
	{
		if (m_HasMainWindowResized)
		{
			GfxWindow& window = GfxWindow::GetMainWindow();
			for (auto const& [owner, callback] : m_OnMainWindowResizedCallbacks)
				callback(&window);
			
			m_HasMainWindowResized = false;
		}
	}

	const bool& PlatformEventsBase::ShouldClose()
	{
		return m_ShouldClose;
	}

	void PlatformEventsBase::RemoveMainWindowResizeCallback(void* owner)
	{
		PACHA_ASSERT(m_OnMainWindowResizedCallbacks.count(owner), "Callback not registered for this owner");
		m_OnMainWindowResizedCallbacks.erase(owner);
	}
}
#pragma once
#include "PlatformEventsBase.h"

#include <SDL.h>

namespace Pacha
{
	class PlatformEventsSDL : public PlatformEventsBase
	{
	public:
		static PlatformEventsSDL& GetInstance()
		{
			static PlatformEventsSDL sInstance;
			return sInstance;
		}

		~PlatformEventsSDL();
		bool Initialize() override;
		void PollEvents() override;
		void TriggerQuit() override;

	private:
		PlatformEventsSDL() = default;
		PlatformEventsSDL(const PlatformEventsSDL&) = delete;
		PlatformEventsSDL& operator=(const PlatformEventsSDL&) = delete;

		Uint32 m_MainWindowId = 0;
	};
}
#include "GfxWindowSDL.h"

#include "core/Log.h"

namespace Pacha
{
	GfxWindowSDL::~GfxWindowSDL()
	{
		SDL_DestroyWindow(m_Window);
	}

	bool GfxWindowSDL::Initialize(const uint32_t& sizeX, const uint32_t& sizeY, const std::string& title, WindowInitFlags initFlags)
	{
		if (SDL_Init(SDL_INIT_VIDEO) != 0)
		{
			LOGCRITICAL("Failed to initialize SDL's Video subsystem: " << SDL_GetError());
			return false;
		}

		Uint32 flags = 0;
#if RENDERER_VK
		flags |= SDL_WINDOW_VULKAN;
#endif
		if (initFlags & WINDOW_FULLSCREEN)			flags |= SDL_WINDOW_FULLSCREEN;
		if (initFlags & WINDOW_HIDDEN)				flags |= SDL_WINDOW_HIDDEN;
		if (initFlags & WINDOW_BORDERLESS)			flags |= SDL_WINDOW_BORDERLESS;
		if (initFlags & WINDOW_RESIZABLE)			flags |= SDL_WINDOW_RESIZABLE;
		if (initFlags & WINDOW_MINIMIZED)			flags |= SDL_WINDOW_MINIMIZED;
		if (initFlags & WINDOW_MAXIMIZED)			flags |= SDL_WINDOW_MAXIMIZED;
		if (initFlags & WINDOW_ALLOW_HIGHDPI)		flags |= SDL_WINDOW_ALLOW_HIGHDPI;
		if (initFlags & WINDOW_ALWAYS_ON_TOP)		flags |= SDL_WINDOW_ALWAYS_ON_TOP;
		if (initFlags & WINDOW_NO_TASK_BAR_ICON)	flags |= SDL_WINDOW_SKIP_TASKBAR;

		m_Window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, static_cast<int>(sizeX), static_cast<int>(sizeY), flags);
		if (m_Window == nullptr)
		{
			LOGCRITICAL("Failed to create SDL window: " << SDL_GetError());
			return false;
		}

		SetSize(glm::uvec2(sizeX, sizeY));
		return true;
	}

	void GfxWindowSDL::SetTitle(const std::string& title)
	{
		SDL_SetWindowTitle(m_Window, title.c_str());
	}

	void GfxWindowSDL::SetSize(const glm::uvec2& size)
	{
		SDL_DisplayMode mode;
		SDL_GetWindowDisplayMode(m_Window, &mode);

		mode.w = size.x;
		mode.h = size.y;

		SDL_SetWindowSize(m_Window, size.x, size.y);
		SDL_SetWindowDisplayMode(m_Window, &mode);
	}

	glm::uvec2 GfxWindowSDL::GetSize()
	{
		int w = 0, h = 0;
		SDL_GetWindowSize(m_Window, &w, &h);
		return glm::uvec2(w, h);
	}

	void GfxWindowSDL::SetPosition(const glm::uvec2& position)
	{
		SDL_SetWindowPosition(m_Window, position.x, position.y);
	}

	glm::uvec2 GfxWindowSDL::GetPosition()
	{
		int x = 0, y = 0;
		SDL_GetWindowPosition(m_Window, &x, &y);
		return glm::uvec2(x, y);
	}

	void GfxWindowSDL::SetWindowMode(WindowMode mode)
	{
		switch (mode)
		{
			case WindowMode::FULLSCREEN:
				SDL_SetWindowFullscreen(m_Window, SDL_WINDOW_FULLSCREEN);
				break;
			case WindowMode::WINDOWED_FULLSCREEN:
				SDL_SetWindowFullscreen(m_Window, SDL_WINDOW_FULLSCREEN_DESKTOP);
				break;
			case WindowMode::WINDOWED:
				SDL_SetWindowFullscreen(m_Window, 0);
				break;
		}

		m_WindowMode = mode;
	}

	void GfxWindowSDL::SetBorderless(bool state)
	{
		SDL_SetWindowBordered(m_Window, static_cast<SDL_bool>(!state));
	}

	void GfxWindowSDL::SetResizeable(bool state)
	{
		SDL_SetWindowResizable(m_Window, static_cast<SDL_bool>(state));
	}

	void GfxWindowSDL::SetAlwaysOnTop(bool state)
	{
		SDL_SetWindowAlwaysOnTop(m_Window, static_cast<SDL_bool>(state));
	}

	void GfxWindowSDL::SetHidden(bool state)
	{
		if (state)
			SDL_HideWindow(m_Window);
		else
			SDL_ShowWindow(m_Window);
	}

	void GfxWindowSDL::SetAlpha(float alpha)
	{
		SDL_SetWindowOpacity(m_Window, alpha);
	}

	void GfxWindowSDL::Focus()
	{
		SDL_RaiseWindow(m_Window);
	}

	void GfxWindowSDL::Maximize()
	{
		SDL_MaximizeWindow(m_Window);
	}

	void GfxWindowSDL::Minimize()
	{
		SDL_MinimizeWindow(m_Window);
	}

	bool GfxWindowSDL::IsMinimized() const
	{
		return SDL_GetWindowFlags(m_Window) & SDL_WINDOW_MINIMIZED;
	}

	bool GfxWindowSDL::IsMaximized() const
	{
		return SDL_GetWindowFlags(m_Window) & SDL_WINDOW_MAXIMIZED;
	}

	bool GfxWindowSDL::HasFocus() const
	{
		return HasInputFocus() || HasMouseFocus();
	}

	bool GfxWindowSDL::HasInputFocus() const
	{
		return SDL_GetWindowFlags(m_Window) & SDL_WINDOW_INPUT_FOCUS;
	}

	bool GfxWindowSDL::HasMouseFocus() const
	{
		return SDL_GetWindowFlags(m_Window) & SDL_WINDOW_MOUSE_FOCUS;
	}

	void* GfxWindowSDL::GetHandle() const
	{
		return m_Window;
	}
}
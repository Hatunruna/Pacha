#pragma once
#if PLATFORM_DESKTOP
#include "platform/GfxWindowSDL.h"
#endif

namespace Pacha
{
#if PLATFORM_DESKTOP
	using GfxWindow = GfxWindowSDL;
#endif
}
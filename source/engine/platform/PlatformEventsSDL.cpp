#include "PlatformEventsSDL.h"

#include "core/Log.h"
#include "input/Input.h"
#include "core/SystemInfo.h"
#include "platform/GfxWindow.h"

#include <SDL.h>

#if PACHA_EDITOR
#include <backends/imgui_impl_sdl2.h>
#endif

namespace Pacha
{
	PlatformEventsSDL::~PlatformEventsSDL()
	{
		SDL_Quit();
	}

	bool PlatformEventsSDL::Initialize()
	{
		if (SDL_InitSubSystem(SDL_INIT_EVENTS | SDL_INIT_GAMECONTROLLER | SDL_INIT_HAPTIC))
		{
			LOGCRITICAL("Failed to initialize SDL's Input subsystems: " << SDL_GetError());
			return false;
		}

		SDL_SetHint(SDL_HINT_ACCELEROMETER_AS_JOYSTICK, "0");
		SDL_SetHint(SDL_HINT_JOYSTICK_HIDAPI_JOY_CONS, "1");
		SDL_SetHint(SDL_HINT_JOYSTICK_HIDAPI_PS4_RUMBLE, "1");
		SDL_SetHint(SDL_HINT_JOYSTICK_HIDAPI_PS5_RUMBLE, "1");
		SDL_SetHint(SDL_HINT_LINUX_JOYSTICK_DEADZONES, "1");

		m_MainWindowId = SDL_GetWindowID(reinterpret_cast<SDL_Window*>(GfxWindow::GetMainWindow().GetHandle()));
		return true;
	}

	void PlatformEventsSDL::PollEvents()
	{
		Input::ValidateState();

#if PACHA_EDITOR
		ImGui_ImplSDL2_NewFrame();
#endif
		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
#if PACHA_EDITOR
			ImGui_ImplSDL2_ProcessEvent(&event);
#endif
			switch (event.type)
			{
				case SDL_EventType::SDL_MOUSEMOTION:
				{
					Input::m_InputState.mousePosition = glm::vec2(event.motion.x, event.motion.y);
					Input::m_InputState.mouseDelta = glm::vec2(event.motion.xrel, -event.motion.yrel);
					break;
				}
				case SDL_EventType::SDL_MOUSEWHEEL:
				{
					Input::m_InputState.mouseWheel.x = static_cast<float>(event.wheel.x);
					Input::m_InputState.mouseWheel.y = static_cast<float>((event.wheel.direction == SDL_MOUSEWHEEL_NORMAL ? 1 : -1) * event.wheel.y);
					break;
				}
				case SDL_EventType::SDL_MOUSEBUTTONDOWN:
				{
					PressState& state = Input::m_InputState.mouseButtonStates[static_cast<MouseButton>(event.button.button)];
					if (state.currentState != PressState::State::HELD)
						state.currentState = PressState::State::PRESSED;
					break;
				}
				case SDL_EventType::SDL_MOUSEBUTTONUP:
				{
					Input::m_InputState.mouseButtonStates[static_cast<MouseButton>(event.button.button)].currentState = PressState::State::RELEASED;
					break;
				}
				case SDL_EventType::SDL_KEYDOWN:
				{
					PressState& state = Input::m_InputState.keyStates[static_cast<KeyCode>(event.key.keysym.sym)];
					if (state.currentState != PressState::State::HELD)
						state.currentState = PressState::State::PRESSED;
					break;
				}
				case SDL_EventType::SDL_KEYUP:
				{
					Input::m_InputState.keyStates[static_cast<KeyCode>(event.key.keysym.sym)].currentState = PressState::State::RELEASED;
					break;
				}
				case SDL_EventType::SDL_WINDOWEVENT:
				{
					if (event.window.windowID != m_MainWindowId)
						break;

					switch (event.window.event)
					{
						case SDL_WINDOWEVENT_CLOSE:
						{
							m_ShouldClose = true;
							break;
						}
						case SDL_WINDOWEVENT_RESIZED:
						case SDL_WINDOWEVENT_SIZE_CHANGED:
						{
							m_HasMainWindowResized = true;
							break;
						}
					}

					break;
				}
				case SDL_EventType::SDL_QUIT:
				{
					m_ShouldClose = true;
					break;
				}
			}
		}
	}

	void PlatformEventsSDL::TriggerQuit()
	{
		SDL_Event ev;
		ev.type = SDL_QUIT;
		SDL_PushEvent(&ev);
	}
}
#pragma once
#if PLATFORM_DESKTOP
#include "platform/PlatformEventsSDL.h"
#endif

namespace Pacha
{
#if PLATFORM_DESKTOP
	using PlatformEvents = PlatformEventsSDL;
#endif
}
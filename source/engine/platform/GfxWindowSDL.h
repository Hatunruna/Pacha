#pragma once
#include "GfxWindowBase.h"

#include <SDL.h>

namespace Pacha
{
	class GfxWindowSDL : public GfxWindowBase
	{
	public:
		static GfxWindowSDL& GetMainWindow()
		{
			static GfxWindowSDL sInstance;
			return sInstance;
		}

		virtual ~GfxWindowSDL();
		bool Initialize(const uint32_t& sizeX, const uint32_t& sizeY, const std::string& title, WindowInitFlags initFlags = WINDOW_RESIZABLE) override;

		void SetTitle(const std::string& title) override;
		
		void SetSize(const glm::uvec2& size) override;
		glm::uvec2 GetSize() override;
		
		void SetPosition(const glm::uvec2& position) override;
		glm::uvec2 GetPosition() override;

		void SetWindowMode(WindowMode mode) override;

		void SetBorderless(bool state) override;
		void SetResizeable(bool state) override;
		void SetAlwaysOnTop(bool state) override;
		void SetHidden(bool state) override;
		void SetAlpha(float alpha) override;

		void Focus() override;
		void Maximize() override;
		void Minimize() override;

		bool IsMinimized() const override;
		bool IsMaximized() const override;
		bool HasFocus() const override;
		bool HasInputFocus() const override;
		bool HasMouseFocus() const override;
		void* GetHandle() const override;

	private:
		SDL_Window* m_Window;
	};
}
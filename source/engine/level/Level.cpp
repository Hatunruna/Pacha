#include "Level.h"
#include "ecs/Entity.h"
#include "fileio/File.h"
#include "ecs/components/Transform.h"
#include "serialization/JsonSerializer.h"
#include "serialization/JsonDeserializer.h"

namespace Pacha
{
	Level::Level(const std::filesystem::path& path)
	{
		m_RootTransform = Transform::CreateAsRoot();

		if (path.empty())
			m_Name = "New Level";
		else
		{
			m_Path = path;
			m_Name = m_Path.filename().string();
		}

		ComponentStorageWarmUp::WarmUp(m_Registry);
	}

	Level::~Level()
	{
		for (auto it : m_Registry.storage<entt::entity>().each())
		{
			entt::entity& entity = std::get<0>(it);
			for (const auto&& [id, storage] : m_Registry.storage())
			{
				if (storage.type() != entt::type_id<Entity>())
				{
					if (storage.contains(entity))
					{
						if (Component* component = static_cast<Component*>(storage.value(entity)))
							component->OnDestroy();
					}
				}
			}
		}

		delete m_RootTransform;
	}

	void Level::Save()
	{
		if(m_IsDirty)
		{
			JsonSerializer serializer(this);
			serializer.StartSerialization();
			Serialization::SaveLevel(serializer);
			serializer.EndSerialization();
			serializer.SaveToFile(m_Path);

			m_IsDirty = false;
		}
	}

	void Level::Load()
	{
		File levelJson(m_Path, OpenMode::TEXT);
		if (levelJson.OpenFile())
		{
			JsonDeserializer deserializer(levelJson.GetTextData(), this);
			Serialization::LoadLevel(deserializer);
			m_Loaded = true;
		}
		else
			LOGERROR("Failed to open Level file: " << m_Path);
	}

	bool Level::Loaded()
	{
		return m_Loaded;
	}

	bool Level::IsDirty()
	{
		return m_IsDirty;
	}

	void Level::SetDirty(const bool& dirty)
	{
		m_IsDirty = dirty;
	}

	bool Level::HasPath() const
	{
		return !m_Path.empty();
	}

	const std::filesystem::path& Level::GetPath()
	{
		return m_Path;
	}

	void Level::SetPath(const std::filesystem::path& path)
	{
		m_Path = path;
		m_Name = m_Path.filename().string();
	}

	const std::string& Level::GetName()
	{
		return m_Name;
	}

	Transform* Level::GetRootTransform()
	{
		return m_RootTransform;
	}
}

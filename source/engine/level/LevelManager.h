#pragma once
#include "Level.h"
#include "ecs/Entity.h"

#include <string>
#include <unordered_map>

namespace Pacha
{
	class LevelManager
	{
	private:
		LevelManager();
		LevelManager(const LevelManager&) = delete;
		LevelManager& operator=(const LevelManager&) = delete;

	public:
		static LevelManager& GetInstance()
		{
			static LevelManager sInstance;
			return sInstance;
		}

#if PACHA_EDITOR
		Level* New();
		void Save(Level* level, const std::filesystem::path& path = {});
		bool IsCurrentLevelDirty();
#endif

		Level* Load(const std::string& path);
		Level* LoadAndSet(const std::string& path, bool deleteCurrent);

		Level* GetCurrentLevel();
		void Set(Level* level, bool deleteCurrent);

		Level* GetPersistentLevel();
		void DestroyLevelData();

	private:
		Level* m_CurrentLevel = nullptr;
		Level* m_PersitentLevel = nullptr;
	};
}
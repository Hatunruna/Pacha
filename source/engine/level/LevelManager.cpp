#include "LevelManager.h"

#include <portable-file-dialogs.h>

namespace Pacha
{
	LevelManager::LevelManager()
	{
		m_PersitentLevel = new Level();
	}

#if PACHA_EDITOR
	Level* LevelManager::New()
	{
		if (m_CurrentLevel != nullptr)
		{
			if (m_CurrentLevel->IsDirty())
			{
				if(m_CurrentLevel->HasPath())
				{
					//Should show confirmation box with Save and Continue and Cancel options
					m_CurrentLevel->Save();
				}
				else
				{
					//Show path thingy if they want to save
				}
			}

			delete m_CurrentLevel;
		}

		m_CurrentLevel = new Level();
		return m_CurrentLevel;
	}

	void LevelManager::Save(Level* level, const std::filesystem::path& path)
	{
		if (path.empty() && level->HasPath())
			level->Save();
		else
		{
			level->SetPath(path);
			level->Save();
		}
	}

	bool LevelManager::IsCurrentLevelDirty()
	{
		if (m_CurrentLevel != nullptr)
			return m_CurrentLevel->IsDirty();
		return false;
	}
#endif

	Level* LevelManager::Load(const std::string& path)
	{
		Level* level = new Level(path);
		level->Load();

		if (level->Loaded())
			return level;
		else
		{
			delete level;
			return nullptr;
		}
	}

	Level* LevelManager::LoadAndSet(const std::string& path, bool deleteCurrent)
	{
		Level* level = Load(path);
		Set(level, deleteCurrent);
		return level;
	}

	Level* LevelManager::GetCurrentLevel()
	{
		return m_CurrentLevel;
	}

	void LevelManager::Set(Level* level, bool deleteCurrent)
	{
		if (level)
		{
			if (deleteCurrent)
				delete m_CurrentLevel;
			m_CurrentLevel = level;
		}
		else
			m_CurrentLevel = nullptr;
	}

	Level* LevelManager::GetPersistentLevel()
	{
		return m_PersitentLevel;
	}
	
	void LevelManager::DestroyLevelData()
	{
		delete m_PersitentLevel;
	}
}
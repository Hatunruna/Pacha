#pragma once
#include "ecs/ComponentTraits.h"

#include <vector>
#include <filesystem>
#include <entt/entt.hpp>

namespace Pacha
{
	class Level
	{
		friend class LevelManager;
		friend class EntityInspector;

	public:
		Level(const std::filesystem::path& path = {});
		~Level();

		void Save();
		void Load();
		bool Loaded();

		bool IsDirty();
		void SetDirty(const bool& dirty);

		bool HasPath() const;
		const std::filesystem::path& GetPath();
		void SetPath(const std::filesystem::path& path);

		const std::string& GetName();
		class Transform* GetRootTransform();
		
		template<typename T, typename = std::enable_if_t<IsComponentV<T> || IsScriptV<T> || std::is_same_v<Entity, T>>>
		auto& GetStorage()
		{
			return m_Registry.storage<T>();
		}

		entt::registry& GetRegistry()
		{
			return m_Registry;
		}

	private:
		bool m_Loaded = false;
		bool m_IsDirty = false;

		std::string m_Name = {};
		std::filesystem::path m_Path = {};

		entt::registry m_Registry;
		class Transform* m_RootTransform = nullptr;
	};
}
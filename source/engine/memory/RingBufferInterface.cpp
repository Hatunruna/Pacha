#include "RingBufferInterface.h"

namespace Pacha
{
	RingBufferInterface::RingBufferInterface(size_t maxSize, uint64_t keepAliveFrames)
		: m_MaxSize(maxSize)
		, m_KeepAliveFrames(keepAliveFrames)
	{ }

	size_t RingBufferInterface::Allocate(size_t Size)
	{
		if (IsFull())
		{
			return kInvalidOffset;
		}

		if (m_Tail >= m_Head)
		{
			//                     Head             Tail     MaxSize
			//                     |                |        |
			//  [                  xxxxxxxxxxxxxxxxx         ]
			//                                         
			//

			if (m_Tail + Size <= m_MaxSize)
			{
				auto Offset = m_Tail;
				m_Tail += Size;
				m_UsedSize += Size;
				m_CurrentFrameSize += Size;
				return Offset;
			}
			else if (Size <= m_Head)
			{
				// Allocate from the beginning of the buffer
				size_t AddSize = (m_MaxSize - m_Tail) + Size;
				m_UsedSize += AddSize;
				m_CurrentFrameSize += AddSize;
				m_Tail = Size;
				return 0;
			}
		}
		else if (m_Tail + Size <= m_Head)
		{
			//
			//       Tail          Head             
			//       |             |             
			//  [xxxx              xxxxxxxxxxxxxxxxxxxxxxxxxx]
			//
			
			size_t Offset = m_Tail;
			m_Tail += Size;
			m_UsedSize += Size;
			m_CurrentFrameSize += Size;
			return Offset;
		}

		return kInvalidOffset;
	}

	void RingBufferInterface::FinishCurrentFrame(uint64_t frameNumber)
	{
		m_CompletedFrameTails.emplace_back(frameNumber, m_Tail, m_CurrentFrameSize);
		m_CurrentFrameSize = 0;
	}

	void RingBufferInterface::ReleaseCompletedFrames(uint64_t lastFrame)
	{
		while (!m_CompletedFrameTails.empty() && m_CompletedFrameTails.front().frame + m_KeepAliveFrames < lastFrame)
		{
			const TailAttributes& OldestFrameTail = m_CompletedFrameTails.front();
			m_UsedSize -= OldestFrameTail.size;
			m_Head = OldestFrameTail.offset;
			m_CompletedFrameTails.pop_front();
		}
	}
}
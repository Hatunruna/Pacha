#pragma once

#include <deque>

namespace Pacha
{
	//https://diligentgraphics.com/2016/04/20/implementing-dynamic-resources-with-direct3d12/
	
	class RingBufferInterface
	{
	public:
		static constexpr size_t kInvalidOffset = static_cast<size_t>(-1);

		struct TailAttributes
		{
			TailAttributes(size_t size, size_t offset, uint64_t frame)
				: frame(frame), offset(offset), size(size)
			{ }

			size_t size;
			size_t offset;
			uint64_t frame;
		};

		RingBufferInterface(size_t maxSize, uint64_t keepAliveFrames);
		size_t Allocate(size_t Size);
		void FinishCurrentFrame(uint64_t frameNumber);
		void ReleaseCompletedFrames(uint64_t lastFrame);

		size_t GetMaxSize() const { return m_MaxSize; }
		bool IsFull() const { return m_UsedSize == m_MaxSize; };
		bool IsEmpty() const { return m_UsedSize == 0; };
		size_t GetUsedSize() const { return m_UsedSize; }

	private:
		size_t m_Head = 0;
		size_t m_Tail = 0;
		size_t m_MaxSize = 0;
		size_t m_UsedSize = 0;
		size_t m_CurrentFrameSize = 0;
		uint64_t m_KeepAliveFrames = 0;
		std::deque<TailAttributes> m_CompletedFrameTails;
	};
}
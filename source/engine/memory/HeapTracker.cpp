#include "HeapTracker.h"
#include "core/Assert.h"

#include <cstdlib>

void* operator new(size_t size)
{
	PACHA_ASSERT(size != 0, "Tried to allocate 0 bytes from the heap");
	static Pacha::HeapTracker& HeapTracker = Pacha::HeapTracker::GetInstance();
	HeapTracker.Allocate(size);
	return std::malloc(size);
}

void* operator new[](std::size_t size)
{
	PACHA_ASSERT(size != 0, "Tried to allocate 0 bytes from the heap");
	static Pacha::HeapTracker& HeapTracker = Pacha::HeapTracker::GetInstance();
	HeapTracker.Allocate(size);
	return std::malloc(size);
}

void operator delete(void* ptr, size_t size)
{
	PACHA_ASSERT(size != 0, "Tried to deallocate 0 bytes from the heap!");
	static Pacha::HeapTracker& HeapTracker = Pacha::HeapTracker::GetInstance();
	HeapTracker.Deallocate(size);
	std::free(ptr);
}

void operator delete[](void* ptr, size_t size)
{
	PACHA_ASSERT(size != 0, "Tried to deallocate 0 bytes from the heap!");
	static Pacha::HeapTracker& HeapTracker = Pacha::HeapTracker::GetInstance();
	HeapTracker.Deallocate(size);
	std::free(ptr);
}

namespace Pacha
{
	void HeapTracker::Allocate(size_t size)
	{
		++m_AllocationCount;
		m_AllocatedBytes += size;
	}

	void HeapTracker::Deallocate(size_t size)
	{
		--m_AllocationCount;
		m_AllocatedBytes -= size;
	}

	size_t HeapTracker::GetAllocationsCount() const
	{
		return m_AllocationCount;
	}

	size_t HeapTracker::GetHeapSize() const
	{
		return m_AllocatedBytes;
	}
}

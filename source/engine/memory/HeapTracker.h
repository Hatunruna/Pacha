#pragma once
#include <atomic>

namespace Pacha
{
	class HeapTracker
	{
	private:
		HeapTracker() = default;
		HeapTracker(const HeapTracker&) = delete;
		HeapTracker& operator=(const HeapTracker&) = delete;

	public:
		static HeapTracker& GetInstance()
		{
			static HeapTracker sInstance;
			return sInstance;
		}

		void Allocate(size_t size);
		void Deallocate(size_t size);

		size_t GetAllocationsCount() const;
		size_t GetHeapSize() const;

	private:
		std::atomic_size_t m_AllocatedBytes = 0;
		std::atomic_size_t m_AllocationCount = 0;
	};
}
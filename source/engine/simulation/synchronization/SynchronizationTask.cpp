#include "SynchronizationTask.h"
#include "ecs/components/Camera.h"
#include "ecs/components/Transform.h"
#include "ecs/components/MeshRenderer.h"
#include "graphics/renderpipeline/GfxRenderPipeline.h"

namespace Pacha
{
	bool SynchronizationTask::Setup()
	{
#if PACHA_EDITOR
		WaitsOn("EditorUI"_ID);
#else
		WaitsOn("ComponentsUpdate"_ID);
#endif
		Signals("Synchronization"_ID);
		return true;
	}

	void SynchronizationTask::Execute(SimulationContext& context)
	{
		FramePacket& framePacket = context.GetFramePacket();
		std::vector<GfxCameraProxy>& cameras = framePacket.cameras;
		std::vector<GfxRenderable>& dynamicObjects = framePacket.dynamicObjects;
		std::vector<GfxRenderable>& selectedObjects = framePacket.selectedObjects;

		context.GetViews<Entity, Camera, Transform>().ForEach([&](Entity& entity, Camera& camera, Transform& transform)
		{
			if (entity.IsActive() && camera.IsActive())
			{
				camera.UpdateViewportAndMatrices(transform, framePacket.viewportResolution);
				cameras.push_back(camera);
			}
		});

		std::atomic_uint32_t meshRendererIndex = 0;
		auto&& meshRendererView = context.GetViews<Entity, MeshRenderer, Transform>();
		meshRendererView.ForEach([&](Entity& entity, MeshRenderer& meshRenderer, Transform& transform)
		{
			if (entity.IsActive() && meshRenderer.IsActive() && meshRenderer.IsVisible() && meshRenderer.IsValid())
			{
				const uint32_t index = meshRendererIndex.fetch_add(1);
				const glm::mat4& modelMatrix = transform.GetWorldTransform();
				
				GfxRenderable renderable(index, modelMatrix, meshRenderer);
				dynamicObjects.push_back(renderable);

				if(meshRenderer.GetRenderFlags() & RenderFlags_SELECTED)
					selectedObjects.push_back(renderable);
			}
		});
	}
}
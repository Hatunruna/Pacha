#include "AnimationTask.h"

#include <marl/scheduler.h>
#include <marl/waitgroup.h>

namespace Pacha
{
	bool AnimationTask::Setup()
	{
		WaitsOn("ScriptsUpdate"_ID);
		Signals("Animation"_ID);
		return true;
	}
	
	void AnimationTask::Execute(SimulationContext&)
	{
	}
}
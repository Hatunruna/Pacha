#include "ComponentsUpdateTask.h"
#include "level/LevelManager.h"
#include "ecs/components/Camera.h"
#include "ecs/components/Transform.h"
#include "ecs/components/LightSource.h"
#include "ecs/components/MeshRenderer.h"
#include "graphics/renderpipeline/GfxRenderPipeline.h"

#include <marl/waitgroup.h>

namespace Pacha
{
	bool ComponentsUpdateTask::Setup()
	{
		WaitsOn("ScriptsUpdate"_ID);
		Signals("ComponentsUpdate"_ID);
		return true;
	}

	void ComponentsUpdateTask::Execute(SimulationContext& context)
	{
		//Update the Scene Graph
		marl::WaitGroup sceneGraphUpdateTasks(2);
		{
			CPU_PROFILER_CAPTURE_SCOPE(SceneGraphUpdate);
			marl::schedule([&, sceneGraphUpdateTasks]
			{
				if (Level* level = context.GetLevel())
					level->GetRootTransform()->Update();
				sceneGraphUpdateTasks.done();
			});

			marl::schedule([&, sceneGraphUpdateTasks]
			{
				if (Level* level = LevelManager::GetInstance().GetPersistentLevel())
					level->GetRootTransform()->Update();
				sceneGraphUpdateTasks.done();
			});
			sceneGraphUpdateTasks.wait();
		}

		//Lights
		marl::WaitGroup updateTasks(2);
		marl::schedule([&, updateTasks]
		{
			CPU_PROFILER_CAPTURE_SCOPE(LightSourcesUpdate);
			context.GetViews<Entity, LightSource, Transform>().ForEach([&](Entity& entity, LightSource& light, Transform& transform)
			{
				if(entity.IsActive() && light.IsActive())
					light.UpdatePositionalData(transform);
			});

			updateTasks.done();
		});

		//MeshRenderers
		marl::schedule([&, updateTasks]
		{
			CPU_PROFILER_CAPTURE_SCOPE(MeshRenderersUpdate);
			context.GetViews<Entity, MeshRenderer, Transform>().ForEach([&](Entity& entity, MeshRenderer& renderer, Transform& transform)
			{
				if(entity.IsActive() && renderer.IsActive() && renderer.IsVisible())
					renderer.UpdateOBB(transform);
			});

			updateTasks.done();
		});

		updateTasks.wait();
	}
}
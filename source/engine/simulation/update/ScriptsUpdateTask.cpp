#include "ScriptsUpdateTask.h"
#include "ecs/ScriptComponent.h"

namespace Pacha
{
	bool ScriptsUpdateTask::Setup()
	{
		WaitsOn("Input"_ID);
		Signals("ScriptsUpdate"_ID);
		return true;
	}
	
	void ScriptsUpdateTask::Execute(SimulationContext& context)
	{
		ScriptComponentUpdateDatabase::Update(context);
	}
}
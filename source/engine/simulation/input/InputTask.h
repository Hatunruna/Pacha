#pragma once
#include "core/graph/SimulationTask.h"

namespace Pacha
{
	class InputTask : public SimulationTask
	{
		TASK_CONSTRUCTOR;

	public:
		bool Setup() override;
		void Execute(SimulationContext& context) override;
	};
}
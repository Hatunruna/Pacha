#include "InputTask.h"
#include "platform/PlatformEvents.h"

namespace Pacha
{
	bool InputTask::Setup()
	{
		Signals("Input"_ID);
		return true;
	}

	void InputTask::Execute(SimulationContext&)
	{
		PlatformEvents& eventsHandler = PlatformEvents::GetInstance();
		eventsHandler.TriggerInputEvents();
	}
}
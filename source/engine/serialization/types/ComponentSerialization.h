#pragma once
#include "level/Level.h"
#include "ecs/Component.h"

#include <string>

namespace Pacha
{
	namespace Serialization
	{
		template<typename Serializer>
		class ComponentUpcastDatabase
		{
		public:
			static std::unordered_map<uint64_t, std::function<void(void*, Component*)>>& GetUpcastTable()
			{
				static std::unordered_map<uint64_t, std::function<void(void*, Component*)>> sUpcastTable;
				return sUpcastTable;
			}

			static void Serialize(const uint64_t id, void* serializerPtr, Component* component)
			{
				std::unordered_map<uint64_t, std::function<void(void*, Component*)>>& sUpcastTable = GetUpcastTable();
				PACHA_ASSERT(sUpcastTable.count(id), "Component " << std::quoted(component->GetTypeInfo().name()) << " has not been registered in the Component Upcast Database");
				sUpcastTable[id](serializerPtr, component);
			}
		};

		template<typename Serializer, typename T>
		class ComponentUpcastDatabaseEntry : public ComponentUpcastDatabase<Serializer>
		{
		public:
			ComponentUpcastDatabaseEntry()
			{
				const uint64_t id = entt::type_id<T>().hash();
				std::unordered_map<uint64_t, std::function<void(void*, Component*)>>& sUpcastTable = GetUpcastTable();

				if (sUpcastTable.count(id))
					return;

				sUpcastTable[id] = [&](void* serializerPtr, Component* componentPtr)
				{
					Serializer& serializer = *static_cast<Serializer*>(serializerPtr);
					T& component = *reinterpret_cast<T*>(componentPtr);
					
					constexpr bool kIsSaving = std::is_base_of_v<SerializerBase, Serializer>;
					Accessor::SerializeElement<kIsSaving>(serializer, component);
				};
			}
		};

		template<typename Serializer>
		void SerializeComponent(Serializer& serializer, Component* component)
		{
			const entt::type_info& typeInfo = component->GetTypeInfo();
			ComponentUpcastDatabase<Serializer>::Serialize(typeInfo.hash(), &serializer, component);
		}

		//Component references
		template<bool IsSaving, typename Serializer, typename T, typename = std::enable_if_t<IsComponentV<T>>>
		void Serialize(Serializer& serializer, T* component)
		{
			if constexpr (IsSaving)
			{
				serializer.StartObject();
				serializer(MakeNVP("Id", component->GetTypeInfo().hash()));
				serializer(MakeNVP("Entity", component->GetEntity()->GetEnttId()));
				serializer.EndObject();
			}
			else
			{
				Level* level = reinterpret_cast<Level*>(serializer.GetUserData());

				entt::id_type id = {};
				entt::entity entity = {};

				serializer.StartObject();
				serializer(MakeNVP("Id", id));
				serializer(MakeNVP("Entity", entity));
				serializer.EndObject();

				entt::registry& registry = level->GetRegistry();
				entt::sparse_set* storage = registry.storage(id);

				if (!storage->contains(entity))
					storage->push(entity);

				component = reinterpret_cast<T*>(storage->value(entity));
			}
		}
	}
}
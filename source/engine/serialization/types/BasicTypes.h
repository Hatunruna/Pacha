#pragma once
#include <string>
#include <filesystem>
#include <type_traits>

namespace Pacha
{
	namespace Serialization
	{
		//Arithmetic and string types
		template<bool IsSaving, typename Serializer, typename T, typename std::enable_if<std::is_arithmetic_v<T> || std::is_same_v<T, std::string>, T>::type* = nullptr>
		void Serialize(Serializer& serializer, T& value)
		{
			if constexpr (IsSaving)
				serializer.WriteValue(value);
			else
				serializer.LoadValue(value);
		}

		//Enum types and underlying arithmetic types
		template<bool IsSaving, typename Serializer, typename T, typename std::enable_if<std::is_enum_v<T> || std::is_arithmetic_v<std::underlying_type_t<T>>, T>::type* = nullptr>
		void Serialize(Serializer& serializer, T& value)
		{
			if constexpr (IsSaving)
				serializer.WriteValue(static_cast<typename std::underlying_type<T>::type>(value));
			else
			{
				typename std::underlying_type<T>::type data;
				serializer.LoadValue(data);
				value = static_cast<T>(data);
			}
		}

		//filesystem::path
		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& serializer, std::filesystem::path& value)
		{
			if constexpr (IsSaving)
				serializer.WriteValue(value.string());
			else
			{
				std::string path;
				serializer.LoadValue(path);
				value = std::filesystem::path(path);
			}
		}
	}
}
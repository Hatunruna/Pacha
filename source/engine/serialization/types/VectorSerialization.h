#pragma once
#include <vector>

namespace Pacha
{
	namespace Serialization
	{
		template<bool IsSaving, typename Serializer, typename T, typename Allocator>
		void Serialize(Serializer& serializer, std::vector<T, Allocator>& vector)
		{
			if constexpr (IsSaving)
			{
				if (vector.size())
				{
					serializer.StartArray();
					for (auto const& it : vector)
						serializer(it);
					serializer.EndArray();
				}
				else
					serializer.WriteNull();
			}
			else
			{
				if (serializer.IsNotNull())
				{
					size_t length = serializer.StartArray();
					vector.resize(length);

					for (size_t i = 0; i < length; ++i)
						serializer(vector[i]);

					serializer.EndArray();
				}
				else
					vector.clear();
			}
		}
	}
}
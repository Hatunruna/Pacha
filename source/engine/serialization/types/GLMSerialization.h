#pragma once
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>

namespace Pacha
{
	namespace Serialization
	{
		template<bool IsSaving, typename Serializer, glm::length_t L, typename T, glm::qualifier Q>
		void Serialize(Serializer& serializer, glm::vec<L, T, Q>& value)
		{
			serializer.StartArray();
			for (int i = 0; i < L; ++i)
				serializer(value[i]);
			serializer.EndArray();
		}

		template<bool IsSaving, typename Serializer, typename T, glm::qualifier Q>
		void Serialize(Serializer& serializer, glm::qua<T, Q>& value)
		{
			serializer.StartArray();
			for (int i = 0; i < 4; ++i)
				serializer(value[i]);
			serializer.EndArray();
		}

		template<bool IsSaving, typename Serializer, glm::length_t R, glm::length_t C, typename T, glm::qualifier Q>
		void Serialize(Serializer& serializer, glm::mat<R, C, T, Q>& value)
		{
			serializer.StartArray();
			for (int i = 0; i < R; ++i)
				for (int j = 0; j < C; ++j)
					serializer(value[i][j]);
			serializer.EndArray();
		}
	}
}
#pragma once
#include <unordered_map>

namespace Pacha
{
	namespace Serialization
	{
		using namespace KeyConversion;

		template<bool IsSaving, typename Serializer, typename K, typename V>
		void Serialize(Serializer& serializer, std::unordered_map<K, V>& unorderedMap)
		{
			if constexpr (IsStringyfiable<K>())
			{
				if constexpr (IsSaving)
				{
					if (unorderedMap.size())
					{
						serializer.StartObject();
						for (auto const& [key, value] : unorderedMap)
						{
							serializer.SetKey(GetStringKey(key));
							serializer(value);
						}
						serializer.EndObject();
					}
					else
						serializer.WriteNull();
				}
				else
				{
					if (serializer.IsNotNull())
					{
						serializer.StartObject();
						while (serializer.IsNotLastObjectMember())
						{
							serializer(unorderedMap[GetKeyValue<K>(serializer.GetCurrentKey())]);
							serializer.NextObjectMember();
						}
						serializer.EndObject();
					}
					else
						unorderedMap.clear();
				}
			}
			else
			{
				if constexpr (IsSaving)
				{
					if (unorderedMap.size())
					{
						serializer.StartArray();
						for (auto const& [key, value] : unorderedMap)
						{
							serializer.StartObject();
							serializer.SetKey("Key");
							serializer(key);
							serializer.SetKey("Value");
							serializer(value);
							serializer.EndObject();
						}
						serializer.EndArray();
					}
					else
						serializer.WriteNull();
				}
				else
				{
					if (serializer.IsNotNull())
					{
						size_t elements = serializer.StartArray();
						for (size_t i = 0; i < elements; ++i)
						{
							K key;
							V value;

							if constexpr (std::is_pointer_v<K>)
								key = nullptr;

							if constexpr (std::is_pointer_v<V>)
								value = nullptr;

							serializer.StartObject();
							serializer.SetKey("Key");
							serializer(key);
							serializer.SetKey("Value");
							serializer(value);
							serializer.EndObject();

							unorderedMap[key] = value;
						}
						serializer.EndArray();
					}
				}
			}
		}
	}
}
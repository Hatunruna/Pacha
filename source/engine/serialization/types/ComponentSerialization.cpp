#pragma once
#include "ComponentSerialization.h"

#include "serialization/JsonSerializer.h"
#include "serialization/JsonDeserializer.h"

#define REGISTER_COMPONENT_SERIALIZATION_UPCAST(serializer, component)	namespace Pacha { static Serialization::ComponentUpcastDatabaseEntry<serializer, component> s_##serializer##component; }
#define REGISTER_COMPONENT_UPCAST_TO_ALL_SERIALIZERS(component)		\
REGISTER_COMPONENT_SERIALIZATION_UPCAST(JsonSerializer, component)	\
REGISTER_COMPONENT_SERIALIZATION_UPCAST(JsonDeserializer, component)

//Components
#include "ecs/components/Camera.h"
#include "ecs/components/LightSource.h"
#include "ecs/components/MeshRenderer.h"
#include "ecs/components/Transform.h"

REGISTER_COMPONENT_UPCAST_TO_ALL_SERIALIZERS(Camera)
REGISTER_COMPONENT_UPCAST_TO_ALL_SERIALIZERS(LightSource)
REGISTER_COMPONENT_UPCAST_TO_ALL_SERIALIZERS(MeshRenderer)
REGISTER_COMPONENT_UPCAST_TO_ALL_SERIALIZERS(Transform)
#pragma once
#include "graphics/resources/technique/GfxTechnique.h"

namespace Pacha
{
	namespace Serialization
	{
		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& serializer, GfxRendererState::GfxRenderTargetState::GfxBlendOperations& operations)
		{
			serializer.StartObject();
			serializer
			(
				MakeNVP("Alpha", operations.alpha),
				MakeNVP("Color", operations.color)
			);
			serializer.EndObject();
		}

		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& serializer, GfxRendererState::GfxRenderTargetState::GfxBlendFactors& factors)
		{
			serializer.StartObject();
			serializer
			(
				MakeNVP("DestinationAlpha", factors.destinationAlpha),
				MakeNVP("DestinationColor", factors.destinationColor),
				MakeNVP("SourceAlpha", factors.sourceAlpha),
				MakeNVP("SourceColor", factors.sourceColor)
			);
			serializer.EndObject();
		}

		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& serializer, GfxRendererState::GfxRenderTargetState& state)
		{
			serializer.StartObject();
			serializer
			(
				MakeNVP("ColorMask", state.colorMask),
				MakeNVP("Enabled", state.blendEnabled),
				MakeNVP("BlendFactors", state.blendFactors),
				MakeNVP("BlendOperations", state.blendOperations)
			);
			serializer.EndObject();
		}

		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& serializer, GfxRendererState& state)
		{
			serializer.StartObject();
			serializer
			(
				MakeNVP("AlphaToCoverage", state.alphaToCoverage),
				MakeNVP("DepthBias", state.depthBias),
				MakeNVP("DepthBiasSlope", state.depthBiasSlope),
				MakeNVP("DepthClamp", state.depthClamp),
				MakeNVP("DepthTest", state.depthTest),
				MakeNVP("DepthWrite", state.depthWrite),
				MakeNVP("FaceCulling", state.faceCulling),
				MakeNVP("FillMode", state.fillMode),
				MakeNVP("LineWidth", state.lineWidth),
				MakeNVP("LogicalBlending", state.logicalBlending),
				MakeNVP("PatchControlPoints", state.patchControlPoints),
				MakeNVP("RenderTargetStates", state.renderTargetStates),
				MakeNVP("WindingOrder", state.windingOrder)
			);
			serializer.EndObject();
		}

		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& serializer, GfxTechniquePass& pass)
		{
			serializer.StartObject();
			serializer
			(
				MakeNVP("Defines", pass.defines),
				MakeNVP("PassId", pass.passId),
				MakeNVP("RendererState", pass.rendererState),
				MakeNVP("DefineMasks", pass.stageDefineMask),
				MakeNVP("UniqueGroups", pass.uniqueGroups),
				MakeNVP("ShaderHashes", pass.shaderHashes)
			);
			serializer.EndObject();
		}

		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& serializer, GfxSamplerInfo& info)
		{
			serializer.StartObject();
			serializer(MakeNVP("Binding", info.binding));
			serializer.EndObject();
		}

		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& serializer, GfxTextureInfo& info)
		{
			serializer.StartObject();
			serializer(MakeNVP("Binding", info.binding));
			serializer(MakeNVP("Type", info.type));
			serializer.EndObject();
		}

		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& serializer, GfxBufferMemberInfo& info)
		{
			serializer.StartObject();
			serializer
			(
				MakeNVP("Offset", info.offset),
				MakeNVP("Size", info.size),
				MakeNVP("Members", info.structMembers),
				MakeNVP("Traits", info.traits)
			);
			serializer.EndObject();
		}

		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& serializer, GfxConstantBufferInfo& info)
		{
			serializer.StartObject();
			serializer
			(
				MakeNVP("Binding", info.binding),
				MakeNVP("Members", info.members),
				MakeNVP("Size", info.size),
				MakeNVP("Type", info.type)
			);
			serializer.EndObject();
		}

		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& serializer, GfxBufferInfo& info)
		{
			serializer.StartObject();
			serializer
			(
				MakeNVP("Binding", info.binding),
				MakeNVP("Type", info.type)
			);
			serializer.EndObject();
		}

		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& serializer, GfxShaderReflectionInfo& reflectionInfo)
		{
			serializer.StartObject();
			serializer
			(
				MakeNVP("Buffers", reflectionInfo.buffers),
				MakeNVP("ConstantBuffers", reflectionInfo.constantBuffers),
				MakeNVP("Samplers", reflectionInfo.samplers),
				MakeNVP("Textures", reflectionInfo.textures)
			);
			serializer.EndObject();
		}
	}
}
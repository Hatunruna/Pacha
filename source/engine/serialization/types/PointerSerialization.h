#pragma once
#include <memory>

namespace Pacha
{
	namespace Serialization
	{
		//Raw Pointers
		template<bool IsSaving, typename Serializer, typename T>
		void Serialize(Serializer& serializer, T*& value)
		{
			if constexpr (IsSaving)
			{
				if (value)
					serializer(*value);
				else
					serializer.WriteNull();
			}
			else
			{
				PACHA_ASSERT(value == nullptr, "Pointer must be null");

				if (serializer.IsNotNull())
				{
					value = new T();
					serializer(*value);
				}
			}
		}

		//Shared Pointers
		template<bool IsSaving, typename Serializer, typename T>
		void Serialize(Serializer& serializer, std::shared_ptr<T>& value)
		{
			if constexpr (IsSaving)
			{
				if (value)
					serializer(*value.get());
				else
					serializer.WriteNull();
			}
			else
			{
				PACHA_ASSERT(value == nullptr, "Pointer must be null");

				if (serializer.IsNotNull())
				{
					value = std::make_shared<T>();
					serializer(*value.get());
				}
			}
		}

		//Unique Pointers
		template<bool IsSaving, typename Serializer, typename T>
		void Serialize(Serializer& serializer, std::unique_ptr<T>& value)
		{
			if constexpr (IsSaving)
			{
				if (value)
					serializer(*value.get());
				else
					serializer.WriteNull();
			}
			else
			{
				PACHA_ASSERT(value == nullptr, "Pointer must be null");

				if (serializer.IsNotNull())
				{
					std::unique_ptr<T> temp = std::make_unique<T>();
					serializer(*temp.get());
					std::swap(temp, value);
				}
			}
		}
	}
}
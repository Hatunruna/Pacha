#pragma once
#include "ecs/Entity.h"
#include "ComponentSerialization.h"

#include <string>
#include <filesystem>
#include <type_traits>

namespace Pacha
{
	namespace Serialization
	{
		template<typename Serializer>
		void SaveLevel(Serializer& serializer)
		{
			Level* level = reinterpret_cast<Level*>(serializer.GetUserData());

			serializer.SetKey("Entities");
			serializer.StartArray();
			level->GetRegistry().view<Entity>().each([&](Entity& entity)
			{
				serializer.StartObject();
				serializer(MakeNVP("Id", entity.GetEnttId()));
				Accessor::SerializeElement<true>(serializer, entity);
				serializer.EndObject();
			});
			serializer.EndArray();

			serializer.SetKey("Components");
			serializer.StartArray();
			for (const auto&& [id, storage] : level->GetRegistry().storage())
			{
				const entt::type_info& componentType = storage.type();
				if (!storage.empty() && componentType != entt::type_id<Entity>())
				{
					serializer.StartObject();
					serializer(MakeNVP("ComponentId", id));

					serializer.SetKey("Elements");
					serializer.StartArray();
					for (auto& entity : storage)
					{
						if (Component* component = static_cast<Component*>(storage.value(entity)))
						{
							serializer.StartObject();
							serializer(MakeNVP("EnttId", entity));
							SerializeComponent(serializer, component);
							serializer.EndObject();
						}
					}
					serializer.EndArray();
					serializer.EndObject();
				}
			}
			serializer.EndArray();
		}

		class EntityAccesor
		{
		public:
			static void SetEntityPrivateValues(Entity& entity, entt::entity enttId, Level* level)
			{
				entity.SetLevel(level);
				entity.SetEnttId(enttId);
			}
		};

		class ComponentAccesor
		{
		public:
			static void SetComponentPrivateValues(Component* component, Entity* entity)
			{
				component->SetEntity(entity);
			}
		};

		template<typename Serializer>
		void LoadLevel(Serializer& serializer)
		{
			Level* level = reinterpret_cast<Level*>(serializer.GetUserData());
			entt::registry& registry = level->GetRegistry();

			serializer.SetKey("Entities");
			if (serializer.IsNotNull())
			{
				size_t count = serializer.StartArray();
				for (size_t i = 0; i < count; ++i)
				{
					serializer.StartObject();

					entt::entity enttId = {};
					serializer(MakeNVP("Id", enttId));

					enttId = registry.create(enttId);
					Entity& entity = level->GetStorage<Entity>().emplace(enttId);
					EntityAccesor::SetEntityPrivateValues(entity, enttId, level);
					Accessor::SerializeElement<false>(serializer, entity);
					serializer.EndObject();
				}
				serializer.EndArray();
			}

			serializer.SetKey("Components");
			if (serializer.IsNotNull())
			{
				size_t count = serializer.StartArray();
				for (size_t i = 0; i < count; ++i)
				{
					serializer.StartObject();

					entt::id_type componentId = {};
					serializer(MakeNVP("ComponentId", componentId));
					entt::sparse_set* storage = registry.storage(componentId);

					serializer.SetKey("Elements");
					size_t elements = serializer.StartArray();
					for (size_t j = 0; j < elements; ++j)
					{
						serializer.StartObject();

						entt::entity enttId = {};
						serializer(MakeNVP("EnttId", enttId));

						if (!storage->contains(enttId))
							storage->push(enttId);

						Component* component = reinterpret_cast<Component*>(storage->value(enttId));
						ComponentAccesor::SetComponentPrivateValues(component, &registry.get<Entity>(enttId));
						SerializeComponent(serializer, component);
						component->OnCreate();

						serializer.EndObject();
					}
					serializer.EndArray();
					serializer.EndObject();
				}
				serializer.EndArray();
			}
		}
	}
}
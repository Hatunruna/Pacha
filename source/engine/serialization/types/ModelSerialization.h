#pragma once
#include "graphics/resources/mesh/GfxModelData.h"

namespace Pacha
{
	namespace Serialization
	{
		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& serializer, GfxModelData& modelData)
		{
			serializer
			(
				MakeNVP("Nodes", modelData.nodes),
				MakeNVP("Meshes", modelData.meshes),
				MakeNVP("Materials", modelData.materials),
				MakeNVP("IndexType", modelData.indexType),
				MakeNVP("IndexDataSize", modelData.indexDataSize),
				MakeNVP("VertexDataSize", modelData.vertexDataSize),
				MakeNVP("InterleavedDataOffset", modelData.interleavedDataOffset),
				MakeNVP("AlphaPositionDataOffset", modelData.alphaPositionDataOffset),
				MakeNVP("AlphaInterleavedDataOffset", modelData.alphaInterleavedDataOffset)
			);
		}

		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& serializer, GfxModelData::Node& node)
		{
			serializer.StartObject();
			serializer
			(
				MakeNVP("Name", node.name),
				MakeNVP("Meshes", node.meshes),
				MakeNVP("Children", node.children),
				MakeNVP("Transform", node.transform)
			);
			serializer.EndObject();
		}

		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& serializer, GfxModelData::Mesh& mesh)
		{
			serializer.StartObject();
			serializer
			(
				MakeNVP("Name", mesh.name),
				MakeNVP("AABB", mesh.aabb),
				MakeNVP("Material", mesh.material),
				MakeNVP("PrimitiveType", mesh.primitiveType),
				MakeNVP("IndexCount", mesh.indexCount),
				MakeNVP("FirstIndex", mesh.firstIndex),
				MakeNVP("VertexCount", mesh.vertexCount),
				MakeNVP("FirstVertex", mesh.firstVertex),
				MakeNVP("VertexLayout", mesh.vertexLayout)
			);
			serializer.EndObject();
		}

		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& serializer, GfxModelData::Material& material)
		{
			serializer.StartObject();
			serializer
			(
				MakeNVP("Name", material.name),
				MakeNVP("Diffuse", material.diffuse),
				MakeNVP("Emissive", material.emissive),
				MakeNVP("Metallic", material.metallic),
				MakeNVP("Roughness", material.roughness),
				MakeNVP("AlphaCutoff", material.alphaCutoff),
				MakeNVP("DoubleSided", material.doubleSided),
				MakeNVP("RenderType", material.renderType),
				MakeNVP("DiffuseTexture", material.diffuseTexture),
				MakeNVP("MetallicRoughnessTexture", material.metallicRoughnessTexture),
				MakeNVP("NormalTexture", material.normalTexture),
				MakeNVP("OcclusionTexture", material.occlusionTexture),
				MakeNVP("EmissiveTexture", material.emissiveTexture)
			);
			serializer.EndObject();
		}
	}
}
#pragma once
#include <string>
#include <filesystem>
#include <functional>

namespace Pacha
{
	class SerializerBase {};
	class DeserializerBase {};

	namespace Serialization
	{
		namespace KeyConversion
		{
			//This handles objects that have well defined conversions from and to arthmetic types and std::string
			template<typename T, typename C>
			struct UsableAsKey : std::bool_constant<std::is_constructible_v<T, C> && std::is_convertible_v<T, C> && !std::is_enum_v<T>>
			{
				using type = C;
			};

			template<typename T>
			using Keyfiable = typename std::disjunction<
				UsableAsKey<T, std::string>,
				UsableAsKey<T, uint64_t>,
				UsableAsKey<T, uint32_t>,
				UsableAsKey<T, uint16_t>,
				UsableAsKey<T, uint8_t>,
				UsableAsKey<T, int64_t>,
				UsableAsKey<T, int32_t>,
				UsableAsKey<T, int16_t>,
				UsableAsKey<T, int8_t>,
				UsableAsKey<T, float>,
				UsableAsKey<T, double>
			>;

			template<typename T>
			T ToArithmeticValue(const std::string& value)
			{
				if constexpr (std::is_same_v<T, uint64_t> || std::is_same_v<T, uint32_t> || std::is_same_v<T, uint16_t> || std::is_same_v<T, uint8_t>)
					return static_cast<T>(std::stoull(value));

				if constexpr (std::is_same_v<T, int64_t> || std::is_same_v<T, int32_t> || std::is_same_v<T, int16_t> || std::is_same_v<T, int8_t>)
					return static_cast<T>(std::stoll(value));

				if constexpr (std::is_same_v<T, float>)
					return std::stof(value);

				if constexpr (std::is_same_v<T, double>)
					return std::stod(value);
			}

			template<typename T>
			constexpr bool IsStringyfiable()
			{
				if constexpr (std::is_same_v<T, std::string>)
					return true;

				if constexpr (std::is_same_v<T, std::filesystem::path>)
					return true;

				if constexpr (std::is_arithmetic_v<T>)
					return true;

				if constexpr (std::is_enum_v<T>)
					return true;

				if constexpr (Keyfiable<T>::value)
					return true;

				return false;
			}

			template<typename T>
			inline std::string GetStringKey(const T& key)
			{
				if constexpr (std::is_same_v<T, std::string>)
					return key;

				if constexpr (std::is_same_v<T, std::filesystem::path>)
					return key.string();

				if constexpr (std::is_arithmetic_v<T>)
					return std::to_string(key);

				if constexpr (std::is_enum_v<T>)
				{
					using EnumBaseType = typename std::underlying_type<T>::type;
					EnumBaseType arithmeticValue = static_cast<EnumBaseType>(key);
					return std::to_string(arithmeticValue);
				}

				if constexpr (Keyfiable<T>::value)
				{
					using KeyType = typename Keyfiable<T>::type;
					if constexpr (std::is_arithmetic_v<KeyType>)
					{
						const KeyType arithmeticKey = static_cast<KeyType>(key);
						return std::to_string(arithmeticKey);
					}
				
					if constexpr (std::is_same_v<KeyType, std::string>)
						return key;
				}
			}

			template<typename T>
			T GetKeyValue(const std::string& value)
			{
				if constexpr (std::is_same_v<T, std::string>)
					return value;

				if constexpr (std::is_same_v<T, std::filesystem::path>)
					return std::filesystem::path(value);

				if constexpr (std::is_arithmetic_v<T>)
					return ToArithmeticValue<T>(value);

				if constexpr (std::is_enum_v<T>)
				{
					using EnumBaseType = typename std::underlying_type<T>::type;
					EnumBaseType keyArithmeticValue = ToArithmeticValue<EnumBaseType>(value);
					return static_cast<T>(keyArithmeticValue);
				}

				if constexpr (Keyfiable<T>::value)
				{
					using KeyType = typename Keyfiable<T>::type;
					if constexpr (std::is_arithmetic_v<KeyType>)
						return ToArithmeticValue<KeyType>(value);

					if constexpr (std::is_same_v<KeyType, std::string>)
						return value;
				}
			}
		}
		
		template<typename T>
		class NameValuePair
		{
			using Type =
				typename std::conditional<std::is_lvalue_reference<T>::value,
				typename std::conditional<std::is_const<typename std::remove_reference<T>::type>::value,
				typename std::decay<T>::type, T>::type,
				typename std::decay<T>::type>::type;

		public:
			NameValuePair(const std::string& name, T&& value)
				: m_Value(std::forward<T>(value)), m_Name(name)
			{ }

			const std::string& GetName() { return m_Name; }
			Type GetValue() { return m_Value; }

		private:
			Type m_Value;
			std::string m_Name;
		};

		class Accessor
		{
		public:
			template<bool IsSaving, typename Serializer, typename T>
			static void SerializeObject(Serializer& serializer, T& value)
			{
				serializer.StartObject();
				value.template Serialize<IsSaving>(serializer);
				serializer.EndObject();
			}

			template<bool IsSaving, typename Serializer, typename T>
			static void SerializeElement(Serializer& serializer, T& value)
			{
				value.template Serialize<IsSaving>(serializer);
			}

			template<bool IsSaving, typename Base, typename Serializer, typename Derived>
			inline static void SerializeBaseClass(Serializer& serializer, Derived&& derived)
			{
				Base& baseClass = *static_cast<Base*>(derived);
				baseClass.template Serialize<IsSaving>(serializer);
			}
		};

		template<bool IsSaving, typename Serializer, typename T>
		void Serialize(Serializer& serializer, NameValuePair<T>& nvp)
		{
			serializer.SetKey(nvp.GetName());
			serializer(nvp.GetValue());
		}

		template <typename T>
		inline static NameValuePair<T> MakeNVP(const std::string& name, T&& value)
		{
			return { name, std::forward<T>(value) };
		}
	}

	struct SerializableObject
	{
	public:
		template <typename T>
		inline static Serialization::NameValuePair<T> MakeNVP(const std::string& name, T&& value)
		{
			return Serialization::MakeNVP(name, std::forward<T>(value));
		}

		template<bool IsSaving, typename Base, typename Serializer, typename Derived>
		inline static void SerializeBaseClass(Serializer& serializer, Derived&& derived)
		{
			Serialization::Accessor::SerializeBaseClass<IsSaving, Base>(serializer, derived);
		}
	};

	struct SerializableElement
	{
	public:
		template<bool IsSaving, typename Base, typename Serializer, typename Derived>
		inline static void SerializeBaseClass(Serializer& serializer, Derived&& derived)
		{
			Serialization::Accessor::SerializeBaseClass<IsSaving, Base>(serializer, derived);
		}
	};
}
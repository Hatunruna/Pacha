#pragma once
#include "Serialization.h"
#include "SerializationTypes.h"

#include <stack>
#include <string>
#include <rapidjson/document.h>

namespace Pacha
{
	class JsonDeserializer : DeserializerBase
	{
	public:
		JsonDeserializer(const std::string& json, void* userData = nullptr);
		
		void* GetUserData() const;

		void StartObject();
		void EndObject();

		size_t StartArray();
		void EndArray();

		void SetKey(const std::string& key);
		const std::string& GetCurrentKey();
		void NextObjectMember();
		bool IsNotLastObjectMember();
		bool IsNotNull();
		void GoToNextElement();

		void LoadValue(const rapidjson::Value::ConstMemberIterator& iterator, bool& value);
		void LoadValue(const rapidjson::Value::ConstMemberIterator& iterator, int8_t& value);
		void LoadValue(const rapidjson::Value::ConstMemberIterator& iterator, int16_t& value);
		void LoadValue(const rapidjson::Value::ConstMemberIterator& iterator, int32_t& value);
		void LoadValue(const rapidjson::Value::ConstMemberIterator& iterator, int64_t& value);
		void LoadValue(const rapidjson::Value::ConstMemberIterator& iterator, float& value);
		void LoadValue(const rapidjson::Value::ConstMemberIterator& iterator, double& value);
		void LoadValue(const rapidjson::Value::ConstMemberIterator& iterator, uint8_t& value);
		void LoadValue(const rapidjson::Value::ConstMemberIterator& iterator, uint16_t& value);
		void LoadValue(const rapidjson::Value::ConstMemberIterator& iterator, uint32_t& value);
		void LoadValue(const rapidjson::Value::ConstMemberIterator& iterator, uint64_t& value);
		void LoadValue(const rapidjson::Value::ConstMemberIterator& iterator, std::string& value);

		void LoadValue(const rapidjson::Value* arrayValue, bool& value);
		void LoadValue(const rapidjson::Value* arrayValue, int8_t& value);
		void LoadValue(const rapidjson::Value* arrayValue, int16_t& value);
		void LoadValue(const rapidjson::Value* arrayValue, int32_t& value);
		void LoadValue(const rapidjson::Value* arrayValue, int64_t& value);
		void LoadValue(const rapidjson::Value* arrayValue, float& value);
		void LoadValue(const rapidjson::Value* arrayValue, double& value);
		void LoadValue(const rapidjson::Value* arrayValue, uint8_t& value);
		void LoadValue(const rapidjson::Value* arrayValue, uint16_t& value);
		void LoadValue(const rapidjson::Value* arrayValue, uint32_t& value);
		void LoadValue(const rapidjson::Value* arrayValue, uint64_t& value);
		void LoadValue(const rapidjson::Value* arrayValue, std::string& value);

		template<typename T>
		void LoadValue(T& value)
		{
			if (m_TopElementTypeStack.size() && m_TopElementTypeStack.top() == TopElementType::ARRAY)
			{
				const rapidjson::Value* arrayValue = m_ArrayIteratorStack.top()++;
				LoadValue(arrayValue, value);
			}
			else
			{
				const ObjectIteratorPair& currentPair = m_ObjectIteratorStack.top();
				if (currentPair.iteratorIndex == -1)
				{
					value = T();
					return;
				}

				const rapidjson::Value::ConstMemberIterator& iterator = currentPair.begin + currentPair.iteratorIndex;
				LoadValue(iterator, value);
			}
		}

		template<typename T>
		bool GetValueIfPresent(Serialization::NameValuePair<T>&& value)
		{
			SetKey(value.GetName());
			if (IsNotNull())
			{
				Process(value);
				return true;
			}

			return false;
		}

		template<typename ... Args>
		void operator()(Args&&... args)
		{
			Process(std::forward<Args>(args)...);
		}

	private:

		template<typename T>
		void Process(T&& value)
		{
			ProcessImpl(value);
		}

		template<typename T>
		void ProcessImpl(const T&)
		{
			PACHA_ASSERT(false, "Can't deserializer a const value");
		}

		template <typename T, typename ... Args>
		void Process(T&& first, Args&& ... args)
		{
			Process(std::forward<T>(first));
			Process(std::forward<Args>(args)...);
		}

		template<typename T>
		void ProcessImpl(T& value)
		{
			if constexpr (std::is_base_of_v<SerializableObject, T>)
				Serialization::Accessor::SerializeObject<false>(*this, value);
			else if constexpr (std::is_base_of_v<SerializableElement, T>)
				Serialization::Accessor::SerializeElement<false>(*this, value);
			else
				Serialization::Serialize<false>(*this, value);
		}

		void Search();

		struct ObjectIteratorPair
		{
			int iteratorIndex = 0;
			rapidjson::Value::ConstMemberIterator begin;
			rapidjson::Value::ConstMemberIterator end;
		};

		enum class TopElementType
		{
			ARRAY,
			OBJECT
		};

		std::string m_NextKey;
		std::string m_CurrentKey;
		void* m_UserData = nullptr;

		rapidjson::Document m_Document;
		std::stack<TopElementType> m_TopElementTypeStack;
		std::stack<ObjectIteratorPair> m_ObjectIteratorStack;
		std::stack<const rapidjson::Value*> m_ArrayIteratorStack;
	};
}
#pragma once
#include "Serialization.h"
#include "SerializationTypes.h"

#include <string>
#include <filesystem>
#include <rapidjson/prettywriter.h>

namespace Pacha
{
	class JsonSerializer : SerializerBase
	{
	public:
		JsonSerializer(void* userData = nullptr);
		~JsonSerializer();

		void* GetUserData() const;

		void StartSerialization();
		void EndSerialization();

		void StartObject();
		void EndObject();

		void StartArray();
		void EndArray();

		void SetKey(const std::string& name);

		void WriteNull();
		void WriteValue(const bool& value);
		void WriteValue(const int8_t& value);
		void WriteValue(const int16_t& value);
		void WriteValue(const int32_t& value);
		void WriteValue(const int64_t& value);
		void WriteValue(const float& value);
		void WriteValue(const double& value);
		void WriteValue(const uint8_t& value);
		void WriteValue(const uint16_t& value);
		void WriteValue(const uint32_t& value);
		void WriteValue(const uint64_t& value);
		void WriteValue(const std::string& value);

		std::string GetJson();
		bool SaveToFile(const std::filesystem::path& path);

		template<typename ... Args>
		void operator()(Args&&... args)
		{
			Process(std::forward<Args>(args)...);
		}

	private:
		template<typename T>
		void Process(T&& value)
		{
			ProcessImpl(value);
		}

		template <typename T, typename ... Args>
		void Process(T&& first, Args&& ... args)
		{
			Process(std::forward<T>(first));
			Process(std::forward<Args>(args)...);
		}

		template<typename T>
		void ProcessImpl(const T& value)
		{
			ProcessImpl(const_cast<T&>(value));
		}

		template<typename T>
		void ProcessImpl(T& value)
		{
			if constexpr (std::is_base_of_v<SerializableObject, T>)
				Serialization::Accessor::SerializeObject<true>(*this, value);
			else if constexpr (std::is_base_of_v<SerializableElement, T>)
				Serialization::Accessor::SerializeElement<true>(*this, value);
			else
				Serialization::Serialize<true>(*this, value);
		}

		void* m_UserData = nullptr;
		rapidjson::StringBuffer m_Buffer;
		rapidjson::PrettyWriter<rapidjson::StringBuffer>* m_Writer = nullptr;
	};
}

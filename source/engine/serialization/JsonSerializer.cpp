#include "JsonSerializer.h"

#include <fstream>

namespace Pacha
{
	JsonSerializer::JsonSerializer(void* userData)
		: m_UserData(userData)
	{
		m_Writer = new rapidjson::PrettyWriter<rapidjson::StringBuffer>(m_Buffer);
		m_Writer->SetFormatOptions(rapidjson::PrettyFormatOptions::kFormatSingleLineArray);
		m_Writer->SetIndent('\t', 1);
	}

	JsonSerializer::~JsonSerializer()
	{
		if (m_Writer != nullptr)
		{
			delete m_Writer;
			m_Writer = nullptr;
		}
	}

	void* JsonSerializer::GetUserData() const
	{
		return m_UserData;
	}

	void JsonSerializer::StartSerialization()
	{
		m_Writer->StartObject();
	}

	void JsonSerializer::EndSerialization()
	{
		m_Writer->EndObject();
	}

	void JsonSerializer::StartObject()
	{
		m_Writer->StartObject();
	}

	void JsonSerializer::EndObject()
	{
		m_Writer->EndObject();
	}

	void JsonSerializer::StartArray()
	{
		m_Writer->StartArray();
	}

	void JsonSerializer::EndArray()
	{
		m_Writer->EndArray();
	}

	void JsonSerializer::SetKey(const std::string& name)
	{
		m_Writer->Key(name.c_str(), static_cast<rapidjson::SizeType>(name.size()));
	}

	void JsonSerializer::WriteNull()
	{
		m_Writer->Null();
	}

	void JsonSerializer::WriteValue(const bool& value)
	{
		m_Writer->Bool(value);
	}

	void JsonSerializer::WriteValue(const int8_t& value)
	{
		m_Writer->Int(static_cast<int32_t>(value));
	}

	void JsonSerializer::WriteValue(const int16_t& value)
	{
		m_Writer->Int(static_cast<int32_t>(value));
	}

	void JsonSerializer::WriteValue(const int32_t& value)
	{
		m_Writer->Int(value);
	}

	void JsonSerializer::WriteValue(const int64_t& value)
	{
		m_Writer->Int64(value);
	}

	void JsonSerializer::WriteValue(const float& value)
	{
		m_Writer->Double(static_cast<double>(value));
	}

	void JsonSerializer::WriteValue(const double& value)
	{
		m_Writer->Double(value);
	}

	void JsonSerializer::WriteValue(const uint8_t& value)
	{
		m_Writer->Uint(static_cast<uint32_t>(value));
	}

	void JsonSerializer::WriteValue(const uint16_t& value)
	{
		m_Writer->Uint(static_cast<uint32_t>(value));
	}

	void JsonSerializer::WriteValue(const uint32_t& value)
	{
		m_Writer->Uint(value);
	}

	void JsonSerializer::WriteValue(const uint64_t& value)
	{
		m_Writer->Uint64(value);
	}

	void JsonSerializer::WriteValue(const std::string& value)
	{
		m_Writer->String(value.c_str(), static_cast<rapidjson::SizeType>(value.size()));
	}

	std::string JsonSerializer::GetJson()
	{
		return std::string(m_Buffer.GetString(), m_Buffer.GetSize());
	}
	
	bool JsonSerializer::SaveToFile(const std::filesystem::path& path)
	{
		std::ofstream output(path);
		if (output.is_open())
		{
			output << GetJson();
			output.close();
			return true;
		}

		return false;
	}
}
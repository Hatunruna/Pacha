#include "JsonDeserializer.h"

namespace Pacha
{
	JsonDeserializer::JsonDeserializer(const std::string& json, void* userData)
		: m_UserData(userData)
	{
		m_Document.Parse(json.c_str());
		m_ObjectIteratorStack.push({ 0, m_Document.MemberBegin(), m_Document.MemberEnd() });
	}

	void* JsonDeserializer::GetUserData() const
	{
		return m_UserData;
	}

	void JsonDeserializer::StartObject()
	{
		if (m_TopElementTypeStack.size() && m_TopElementTypeStack.top() == TopElementType::ARRAY)
		{
			const rapidjson::Value* arrayValue = m_ArrayIteratorStack.top()++;
			if (arrayValue->IsObject())
			{
				m_TopElementTypeStack.push(TopElementType::OBJECT);
				m_ObjectIteratorStack.push({ 0, arrayValue->MemberBegin(), arrayValue->MemberEnd() });
				m_CurrentKey = std::string(arrayValue->MemberBegin()->name.GetString(), arrayValue->MemberBegin()->name.GetStringLength());
			}
		}
		else
		{
			const ObjectIteratorPair& currentPair = m_ObjectIteratorStack.top();
			if (currentPair.iteratorIndex == -1)
				return;

			const rapidjson::Value::ConstMemberIterator& iterator = currentPair.begin + currentPair.iteratorIndex;
			if (iterator->value.IsObject())
			{
				m_TopElementTypeStack.push(TopElementType::OBJECT);
				m_ObjectIteratorStack.push({ 0, iterator->value.MemberBegin(), iterator->value.MemberEnd() });
				m_CurrentKey = std::string(iterator->value.MemberBegin()->name.GetString(), iterator->value.MemberBegin()->name.GetStringLength());
			}
		}
	}

	void JsonDeserializer::EndObject()
	{
		m_TopElementTypeStack.pop();
		m_ObjectIteratorStack.pop();
	}

	size_t JsonDeserializer::StartArray()
	{
		if (m_TopElementTypeStack.size() && m_TopElementTypeStack.top() == TopElementType::ARRAY)
		{
			const rapidjson::Value* arrayValue = m_ArrayIteratorStack.top()++;
			if (arrayValue->IsArray())
			{
				m_TopElementTypeStack.push(TopElementType::ARRAY);
				m_ArrayIteratorStack.push(arrayValue->Begin());
				return static_cast<size_t>(arrayValue->Size());
			}
			else
				return 0;
		}
		else
		{
			const ObjectIteratorPair& currentPair = m_ObjectIteratorStack.top();
			const rapidjson::Value::ConstMemberIterator& iterator = currentPair.begin + currentPair.iteratorIndex;

			if (iterator->value.IsArray())
			{
				m_TopElementTypeStack.push(TopElementType::ARRAY);
				const rapidjson::GenericArray jsonArray = iterator->value.GetArray();
				m_ArrayIteratorStack.push(jsonArray.Begin());
				return static_cast<size_t>(jsonArray.Size());
			}
			else
				return 0;
		}
	}

	void JsonDeserializer::EndArray()
	{
		m_TopElementTypeStack.pop();
		m_ArrayIteratorStack.pop();
	}

	void JsonDeserializer::SetKey(const std::string& key)
	{
		m_NextKey = key;
		Search();
	}

	const std::string& JsonDeserializer::GetCurrentKey()
	{
		return m_CurrentKey;
	}

	void JsonDeserializer::NextObjectMember()
	{
		ObjectIteratorPair& currentPair = m_ObjectIteratorStack.top();
		++currentPair.iteratorIndex;

		const rapidjson::Value::ConstMemberIterator& iterator = currentPair.begin + currentPair.iteratorIndex;

		if (iterator != currentPair.end)
			m_CurrentKey = std::string(iterator->name.GetString(), iterator->name.GetStringLength());
	}

	bool JsonDeserializer::IsNotLastObjectMember()
	{
		const ObjectIteratorPair& currentPair = m_ObjectIteratorStack.top();
		const rapidjson::Value::ConstMemberIterator& iterator = currentPair.begin + currentPair.iteratorIndex;
		return iterator != currentPair.end;
	}

	bool JsonDeserializer::IsNotNull()
	{
		if (m_TopElementTypeStack.size() && m_TopElementTypeStack.top() == TopElementType::ARRAY)
		{
			const rapidjson::Value* arrayValue = m_ArrayIteratorStack.top();
			return !arrayValue->IsNull();
		}
		else
		{
			const ObjectIteratorPair& currentPair = m_ObjectIteratorStack.top();
			if (currentPair.iteratorIndex == -1)
				return false;

			const rapidjson::Value::ConstMemberIterator& iterator = currentPair.begin + currentPair.iteratorIndex;
			return !iterator->value.IsNull();
		}
	}

	void JsonDeserializer::GoToNextElement()
	{
		if (m_TopElementTypeStack.size() && m_TopElementTypeStack.top() == TopElementType::ARRAY)
			++m_ArrayIteratorStack.top();
		else
			NextObjectMember();
	}

	void JsonDeserializer::LoadValue(const rapidjson::Value::ConstMemberIterator& iterator, bool& value)
	{
		value = iterator->value.GetBool();
	}

	void JsonDeserializer::LoadValue(const rapidjson::Value::ConstMemberIterator& iterator, int8_t& value)
	{
		value = static_cast<int8_t>(iterator->value.GetInt());
	}

	void JsonDeserializer::LoadValue(const rapidjson::Value::ConstMemberIterator& iterator, int16_t& value)
	{
		value = static_cast<int16_t>(iterator->value.GetInt());
	}

	void JsonDeserializer::LoadValue(const rapidjson::Value::ConstMemberIterator& iterator, int32_t& value)
	{
		value = iterator->value.GetInt();
	}

	void JsonDeserializer::LoadValue(const rapidjson::Value::ConstMemberIterator& iterator, int64_t& value)
	{
		value = iterator->value.GetInt64();
	}

	void JsonDeserializer::LoadValue(const rapidjson::Value::ConstMemberIterator& iterator, float& value)
	{
		value = iterator->value.GetFloat();
	}

	void JsonDeserializer::LoadValue(const rapidjson::Value::ConstMemberIterator& iterator, double& value)
	{
		value = iterator->value.GetDouble();
	}

	void JsonDeserializer::LoadValue(const rapidjson::Value::ConstMemberIterator& iterator, uint8_t& value)
	{
		value = static_cast<uint8_t>(iterator->value.GetUint());
	}

	void JsonDeserializer::LoadValue(const rapidjson::Value::ConstMemberIterator& iterator, uint16_t& value)
	{
		value = static_cast<uint16_t>(iterator->value.GetUint());
	}

	void JsonDeserializer::LoadValue(const rapidjson::Value::ConstMemberIterator& iterator, uint32_t& value)
	{
		value = iterator->value.GetUint();
	}

	void JsonDeserializer::LoadValue(const rapidjson::Value::ConstMemberIterator& iterator, uint64_t& value)
	{
		value = iterator->value.GetUint64();
	}

	void JsonDeserializer::LoadValue(const rapidjson::Value::ConstMemberIterator& iterator, std::string& value)
	{
		value = std::string(iterator->value.GetString(), iterator->value.GetStringLength());
	}

	void JsonDeserializer::LoadValue(const rapidjson::Value* arrayValue, bool& value)
	{
		value = arrayValue->GetBool();
	}

	void JsonDeserializer::LoadValue(const rapidjson::Value* arrayValue, int8_t& value)
	{
		value = static_cast<int8_t>(arrayValue->GetInt());
	}

	void JsonDeserializer::LoadValue(const rapidjson::Value* arrayValue, int16_t& value)
	{
		value = static_cast<int16_t>(arrayValue->GetInt());
	}

	void JsonDeserializer::LoadValue(const rapidjson::Value* arrayValue, int32_t& value)
	{
		value = arrayValue->GetInt();
	}

	void JsonDeserializer::LoadValue(const rapidjson::Value* arrayValue, int64_t& value)
	{
		value = arrayValue->GetInt64();
	}

	void JsonDeserializer::LoadValue(const rapidjson::Value* arrayValue, float& value)
	{
		value = arrayValue->GetFloat();
	}

	void JsonDeserializer::LoadValue(const rapidjson::Value* arrayValue, double& value)
	{
		value = arrayValue->GetDouble();
	}

	void JsonDeserializer::LoadValue(const rapidjson::Value* arrayValue, uint8_t& value)
	{
		value = static_cast<uint8_t>(arrayValue->GetUint());
	}

	void JsonDeserializer::LoadValue(const rapidjson::Value* arrayValue, uint16_t& value)
	{
		value = static_cast<uint16_t>(arrayValue->GetUint());
	}

	void JsonDeserializer::LoadValue(const rapidjson::Value* arrayValue, uint32_t& value)
	{
		value = arrayValue->GetUint();
	}

	void JsonDeserializer::LoadValue(const rapidjson::Value* arrayValue, uint64_t& value)
	{
		value = arrayValue->GetUint64();
	}

	void JsonDeserializer::LoadValue(const rapidjson::Value* arrayValue, std::string& value)
	{
		value = std::string(arrayValue->GetString(), arrayValue->GetStringLength());
	}

	void JsonDeserializer::Search()
	{
		ObjectIteratorPair& currentPair = m_ObjectIteratorStack.top();

		int index = 0;
		for (auto it = currentPair.begin; it != currentPair.end; ++it)
		{
			std::string name(it->name.GetString(), it->name.GetStringLength());
			if (name == m_NextKey)
			{
				currentPair.iteratorIndex = index;
				m_CurrentKey = m_NextKey;
				return;
			}

			++index;
		}

		currentPair.iteratorIndex = -1;
	}
}
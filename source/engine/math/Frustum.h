#pragma once
#include "OBB.h"
#include "Plane.h"

namespace Pacha
{
	enum FrustumPlane
	{
		FrustumPlane_LEFT,
		FrustumPlane_RIGHT,
		FrustumPlane_TOP,
		FrustumPlane_BOTTOM,
		FrustumPlane_FAR,
		FrustumPlane_NEAR
	};

	class Frustum
	{
	public:
		Frustum() = default;
		Frustum(const glm::mat4& viewProj);

		bool ContainsOBB(const OBB& obb, bool noNearPlane = false) const;
		bool ContainsAABB(const AABB& aabb, bool noNearPlane = false) const;

		bool ContainsPoint(const glm::vec3& position, bool noNearPlane = false);
		bool ContainsSphere(const glm::vec3& position, const float& radius, bool noNearPlane = false);
		bool ContainsCone(const glm::vec3& position, const glm::vec3& direction, const float& range, const float& coneAngle, bool noNearPlane = false);

		const Plane& GetPlane(const FrustumPlane& plane);
		
	protected:
		Plane m_Planes[6];
	};
}
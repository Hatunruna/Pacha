#include "Plane.h"

namespace Pacha
{
	Plane::Plane(const glm::vec3& normal, const float& distance)
	{
		float length = glm::length(normal);
		m_Normal = normal / length;
		m_Distance = distance / length;
	}

	void Plane::SetNormal(const glm::vec3& normal)
	{
		m_Normal = normal;
	}

	const glm::vec3& Plane::GetNormal() const
	{
		return m_Normal;
	}

	void Plane::SetDistance(const float& dist)
	{
		m_Distance = dist;
	}

	const float& Plane::GetDistance() const
	{
		return m_Distance;
	}

	glm::vec4 Plane::GetPlane() const
	{
		return glm::vec4(m_Normal, m_Distance);
	}

	bool Plane::AABBInPlane(const AABB& aabb) const
	{
		glm::vec3 minMulNormal = aabb.GetMin() * m_Normal;
		glm::vec3 maxMulNormal = aabb.GetMax() * m_Normal;
		glm::vec3 max = glm::max(minMulNormal, maxMulNormal);

		float d = max.x + max.y + max.z;
		return d > -m_Distance;
	}

	bool Plane::OBBInPlane(const OBB& obb) const
	{
		const glm::vec3& extents = obb.GetExtents();
		float r = extents.x * glm::abs(glm::dot(m_Normal, obb.GetXAxis())) +
			extents.y * glm::abs(glm::dot(m_Normal, obb.GetYAxis())) +
			extents.z * glm::abs(glm::dot(m_Normal, obb.GetZAxis()));

		float d = glm::dot(m_Normal, obb.GetCenter());
		return d + r > -m_Distance;
	}

	bool Plane::PointInPlane(const glm::vec3& position)
	{
		return SphereInPlane(position, 0.f);
	}

	bool Plane::SphereInPlane(const glm::vec3& position, const float& radius)
	{
		return glm::dot(glm::vec4(m_Normal, m_Distance), glm::vec4(position, 1.f)) > -radius;
	}

	bool Plane::ConeInPlane(const glm::vec3& position, const glm::vec3& direction, const float& range, const float& spotRadius)
	{
		glm::vec3 furthestPointDir = glm::normalize(glm::cross(glm::cross(m_Normal, direction), direction));
		glm::vec3 furthestConePoint = position + (direction * range) - (furthestPointDir * spotRadius);
		return PointInPlane(position) || PointInPlane(furthestConePoint);
	}
}
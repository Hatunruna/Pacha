#pragma once
#include <stdint.h>
#include <type_traits>

namespace Pacha
{
	template<typename T>
	constexpr inline T SizeKB(const T value)
	{
		return value * static_cast<T>(1024);
	}

	template<typename T>
	constexpr inline T SizeMB(const T value)
	{
		return SizeKB<T>(value) * static_cast<T>(1024);
	}

	template<typename T>
	constexpr inline T SizeGB(const T value)
	{
		return SizeMB<T>(value) * static_cast<T>(1024);
	}

	template<typename T>
	constexpr inline T RoundToNextMultiple(const T value, const T multiple)
	{
		return ((value + multiple - 1) / multiple) * multiple;
	}

	constexpr inline uint8_t NextPowerOf2(uint8_t value)
	{
		--value;
		value |= value >> 1;
		value |= value >> 2;
		value |= value >> 4;
		return ++value;
	}

	constexpr inline uint16_t NextPowerOf2(uint16_t value)
	{
		--value;
		value |= value >> 1;
		value |= value >> 2;
		value |= value >> 4;
		value |= value >> 8;
		return ++value;
	}

	constexpr inline uint32_t NextPowerOf2(uint32_t value)
	{
		--value;
		value |= value >> 1;
		value |= value >> 2;
		value |= value >> 4;
		value |= value >> 8;
		value |= value >> 16;
		return ++value;
	}

	constexpr inline uint64_t NextPowerOf2(uint64_t value)
	{
		--value;
		value |= value >> 1;
		value |= value >> 2;
		value |= value >> 4;
		value |= value >> 8;
		value |= value >> 16;
		value |= value >> 32;
		return ++value;
	}

	template<typename T>
	static std::string ByteSizeToShortString(const T bytes)
	{
		std::stringstream stringStream = {};
		stringStream << std::fixed;
		stringStream << std::setprecision(2);

		if (bytes >= SizeGB<T>(1))
		{
			double sizeInGB = bytes / SizeGB<double>(1);
			stringStream << sizeInGB << " GB";
		}
		else if (bytes >= SizeMB<T>(1))
		{
			double sizeInMB = bytes / SizeMB<double>(1);
			stringStream << sizeInMB << " MB";
		}
		else if (bytes >= SizeKB<T>(1))
		{
			double sizeInKB = bytes / SizeKB<double>(1);
			stringStream << sizeInKB << " KB";
		}
		else
		{
			stringStream << bytes << " B";
		}

		return stringStream.str();
	}
}
#pragma once
#include "AABB.h"

namespace Pacha
{
	class OBB : public SerializableObject
	{
	public:
		OBB() = default;
		OBB(const AABB& aabb, const glm::mat4& matrix);
		OBB(const glm::vec3& center, const glm::vec3& extents, const glm::vec3& xaxis, const glm::vec3& yaxis, const glm::vec3& zaxis);

		const glm::vec3& GetCenter() const;
		const glm::vec3& GetExtents() const;

		const glm::vec3& GetMin() const;
		const glm::vec3& GetMax() const;

		const glm::vec3& GetXAxis() const;
		const glm::vec3& GetYAxis() const;
		const glm::vec3& GetZAxis() const;

	private:
		glm::vec3 m_Center = glm::vec3(0.f);
		glm::vec3 m_Extents = glm::vec3(0.5f);

		glm::vec3 m_Min = glm::vec3(-0.5f);
		glm::vec3 m_Max = glm::vec3(0.5f);

		glm::vec3 m_XAxis = glm::vec3(1.f, 0.f, 0.f);
		glm::vec3 m_YAxis = glm::vec3(0.f, 1.f, 0.f);
		glm::vec3 m_ZAxis = glm::vec3(0.f, 0.f, 1.f);

		friend class Serialization::Accessor;
		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& serializer)
		{
			serializer
			(
				MakeNVP("Center", m_Center),
				MakeNVP("Extents", m_Extents),
				MakeNVP("XAxis", m_XAxis),
				MakeNVP("YAxis", m_YAxis),
				MakeNVP("ZAxis", m_ZAxis)
			);
		}
	};
}
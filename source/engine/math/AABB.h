#pragma once
#include "serialization/Serialization.h"

#include <glm/glm.hpp>

namespace Pacha
{
	class AABB : public SerializableObject
	{
	public:
		AABB() = default;
		AABB(const glm::vec3& min, const glm::vec3& max);

		const glm::vec3& GetMin() const;
		const glm::vec3& GetMax() const;

		void SetMin(const glm::vec3& min);
		void SetMax(const glm::vec3& max);

		void Union(const AABB& aabb);
		void Transform(const glm::mat4& transform);

		glm::vec3 GetCenter() const;
		glm::vec3 GetExtents() const;
		float GetArea() const;

		bool Contains(const AABB& other) const;
		bool Overlaps(const AABB& other) const;

		static AABB Union(const AABB& aabb1, const AABB& aabb2);
		static AABB Transform(const AABB& aabb, const glm::mat4& transform);
		static AABB FromCenterExtents(const glm::vec3& center, const glm::vec3& extents);

	private:
		glm::vec3 m_Min = glm::vec3(-0.5f);
		glm::vec3 m_Max = glm::vec3(0.5f);

		friend class Serialization::Accessor;
		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& serializer)
		{
			serializer
			(
				MakeNVP("Min", m_Min),
				MakeNVP("Max", m_Max)
			);
		}
	};
}
#include "OBB.h"

namespace Pacha
{
	OBB::OBB(const AABB& aabb, const glm::mat4& transform)
	{
		const glm::vec3& min = aabb.GetMin();
		const glm::vec3& max = aabb.GetMax();

		glm::vec3 corners[4] =
		{
			glm::vec3(min.x, min.y, min.z),
			glm::vec3(max.x, min.y, min.z),
			glm::vec3(min.x, max.y, min.z),
			glm::vec3(min.x, min.y, max.z)
		};

		for (size_t i = 0; i < 4; ++i)
			corners[i] = (transform * glm::vec4(corners[i], 1.f));

		m_XAxis = corners[1] - corners[0];
		m_YAxis = corners[2] - corners[0];
		m_ZAxis = corners[3] - corners[0];

		m_Center = corners[0] + (0.5f * (m_XAxis + m_YAxis + m_ZAxis));
		m_Extents = glm::vec3(glm::length(m_XAxis), glm::length(m_YAxis), glm::length(m_ZAxis));

		m_XAxis /= m_Extents.x;
		m_YAxis /= m_Extents.y;
		m_ZAxis /= m_Extents.z;
		m_Extents *= 0.5f;

		m_Min = m_Center - m_Extents;
		m_Max = m_Center + m_Extents;
	}

	OBB::OBB(const glm::vec3& center, const glm::vec3& extents, const glm::vec3& xaxis, const glm::vec3& yaxis, const glm::vec3& zaxis)
	{
		m_Center = center;
		m_Extents = extents;

		m_XAxis = xaxis;
		m_YAxis = yaxis;
		m_ZAxis = zaxis;
	
		m_Min = m_Center - m_Extents;
		m_Max = m_Center + m_Extents;
	}

	const glm::vec3& OBB::GetCenter() const
	{
		return m_Center;
	}

	const glm::vec3& OBB::GetExtents() const
	{
		return m_Extents;
	}

	const glm::vec3& OBB::GetMin() const
	{
		return m_Min;
	}

	const glm::vec3& OBB::GetMax() const
	{
		return m_Max;
	}

	const glm::vec3& OBB::GetXAxis() const
	{
		return m_XAxis;
	}

	const glm::vec3& OBB::GetYAxis() const
	{
		return m_YAxis;
	}

	const glm::vec3& OBB::GetZAxis() const
	{
		return m_ZAxis;
	}
}
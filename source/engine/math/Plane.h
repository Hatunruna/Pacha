#pragma once
#include "OBB.h"
#include <glm/glm.hpp>

namespace Pacha
{
	class Plane
	{
	public:
		Plane() = default;
		Plane(const glm::vec3& normal, const float& distance);

		void SetNormal(const glm::vec3& normal);
		const glm::vec3& GetNormal() const;

		void SetDistance(const float& dist);
		const float& GetDistance()	const;
		glm::vec4 GetPlane() const;

		bool AABBInPlane(const AABB& aabb) const;
		bool OBBInPlane(const OBB& obb) const;

		bool PointInPlane(const glm::vec3& position);
		bool SphereInPlane(const glm::vec3& position, const float& radius);
		bool ConeInPlane(const glm::vec3& position, const glm::vec3& direction, const float& range, const float& spotRadius);

	private:
		float m_Distance;
		glm::vec3 m_Normal;
	};
}

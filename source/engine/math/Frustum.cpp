#include "Frustum.h"

namespace Pacha
{

	Frustum::Frustum(const glm::mat4& viewProj)
	{
		glm::vec3 xaxis(viewProj[0][0], viewProj[1][0], viewProj[2][0]);
		glm::vec3 yaxis(viewProj[0][1], viewProj[1][1], viewProj[2][1]);
		glm::vec3 zaxis(viewProj[0][2], viewProj[1][2], viewProj[2][2]);
		glm::vec3 waxis(viewProj[0][3], viewProj[1][3], viewProj[2][3]);

		m_Planes[0] = Plane(waxis + xaxis, viewProj[3][3] + viewProj[3][0]);	// LEFT
		m_Planes[1] = Plane(waxis - xaxis, viewProj[3][3] - viewProj[3][0]);	// RIGHT
		m_Planes[2] = Plane(waxis - yaxis, viewProj[3][3] - viewProj[3][1]);	// TOP
		m_Planes[3] = Plane(waxis + yaxis, viewProj[3][3] + viewProj[3][1]);	// BOTTOM
		m_Planes[4] = Plane(waxis - zaxis, viewProj[3][3] - viewProj[3][2]);	// FAR
		m_Planes[5] = Plane(waxis + zaxis, viewProj[3][3] + viewProj[3][2]);	// NEAR
	}

	bool Frustum::ContainsOBB(const OBB& obb, bool noNearPlane) const
	{
		const int32_t planeCount = noNearPlane ? 4 : 5;
		for (int32_t plane = planeCount; plane >= 0; --plane)
		{
			if (!m_Planes[plane].OBBInPlane(obb))
				return false;
		}

		return true;
	}

	bool Frustum::ContainsAABB(const AABB& aabb, bool noNearPlane) const
	{
		const int32_t planeCount = noNearPlane ? 4 : 5;
		for (int32_t plane = planeCount; plane >= 0; --plane)
		{
			if (!m_Planes[plane].AABBInPlane(aabb))
				return false;
		}

		return true;
	}

	bool Frustum::ContainsPoint(const glm::vec3& position, bool noNearPlane)
	{
		return ContainsSphere(position, 0.f, noNearPlane);
	}

	bool Frustum::ContainsSphere(const glm::vec3& position, const float& radius, bool noNearPlane)
	{
		const int32_t planeCount = noNearPlane ? 4 : 5;
		for (int p = planeCount; p >= 0; --p)
		{
			if (!m_Planes[p].SphereInPlane(position, radius))
				return false;
		}

		return true;
	}

	bool Frustum::ContainsCone(const glm::vec3& position, const glm::vec3& direction, const float& range, const float& coneAngle, bool noNearPlane)
	{
		const float spotRadius = range * glm::tan(coneAngle);
		const int32_t planeCount = noNearPlane ? 4 : 5;
		for (int p = planeCount; p >= 0; --p)
		{
			if (!m_Planes[p].ConeInPlane(position, direction, range, spotRadius))
				return false;
		}

		return true;
	}

	const Plane& Frustum::GetPlane(const FrustumPlane& plane)
	{
		return m_Planes[plane];
	}
}

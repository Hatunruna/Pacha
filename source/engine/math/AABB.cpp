#include "AABB.h"

namespace Pacha
{
	AABB::AABB(const glm::vec3& min, const glm::vec3& max)
	{
		m_Min = min;
		m_Max = max;
	}

	const glm::vec3& AABB::GetMin() const
	{
		return m_Min;
	}

	const glm::vec3& AABB::GetMax() const
	{
		return m_Max;
	}

	void AABB::SetMin(const glm::vec3& min)
	{
		m_Min = min;
	}

	void AABB::SetMax(const glm::vec3& max)
	{
		m_Max = max;
	}

	void AABB::Union(const AABB& aabb)
	{
		m_Min = glm::min(m_Min, aabb.m_Min);
		m_Max = glm::max(m_Max, aabb.m_Max);
	}

	void AABB::Transform(const glm::mat4& transform)
	{
		glm::vec3 xa = transform[0] * m_Min.x;
		glm::vec3 xb = transform[0] * m_Max.x;

		glm::vec3 ya = transform[1] * m_Min.y;
		glm::vec3 yb = transform[1] * m_Max.y;

		glm::vec3 za = transform[2] * m_Min.z;
		glm::vec3 zb = transform[2] * m_Max.z;

		glm::vec3 translation = glm::vec3(transform[3][0], transform[3][1], transform[3][2]);
		m_Min = glm::min(xa, xb) + glm::min(ya, yb) + glm::min(za, zb) + translation;
		m_Max = glm::max(xa, xb) + glm::max(ya, yb) + glm::max(za, zb) + translation;
	}

	glm::vec3 AABB::GetCenter() const
	{
		return (m_Max + m_Min) * 0.5f;
	}

	glm::vec3 AABB::GetExtents() const
	{
		return (m_Max - m_Min) * 0.5f;
	}

	float AABB::GetArea() const
	{
		glm::vec3 d = m_Max - m_Min;
		return 2.0f * (d.x * d.y + d.y * d.z + d.z * d.x);
	}

	bool AABB::Contains(const AABB& other) const
	{
		bool result = true;
		result = result && m_Min.x <= other.m_Min.x;
		result = result && m_Min.y <= other.m_Min.y;
		result = result && m_Min.z <= other.m_Min.z;

		result = result && m_Max.x <= other.m_Max.x;
		result = result && m_Max.y <= other.m_Max.y;
		result = result && m_Max.z <= other.m_Max.z;
		return result;
	}

	bool AABB::Overlaps(const AABB& other) const
	{
		glm::vec3 d1, d2;
		d1 = other.m_Min - m_Max;
		d2 = m_Min - other.m_Max;

		if (d1.x > 0.0f || d1.y > 0.0f || d1.z >= 0.0f)
			return false;

		if (d2.x > 0.0f || d2.y > 0.0f || d2.z >= 0.0f)
			return false;

		return true;
	}

	AABB AABB::Union(const AABB& aabb1, const AABB& aabb2)
	{
		AABB bb;
		bb.m_Min = glm::min(aabb1.m_Min, aabb2.m_Min);
		bb.m_Max = glm::max(aabb1.m_Max, aabb2.m_Max);
		return bb;
	}

	AABB AABB::Transform(const AABB& aabb, const glm::mat4& transform)
	{
		AABB bb = aabb;
		bb.Transform(transform);
		return bb;
	}

	AABB AABB::FromCenterExtents(const glm::vec3& center, const glm::vec3& extents)
	{
		AABB bb;
		bb.m_Min = center - extents;
		bb.m_Max = center + extents;
		return bb;
	}
}

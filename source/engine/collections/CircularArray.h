#pragma once
#include "core/Assert.h"

#include <array>
#include <atomic>

namespace Pacha
{
	template<typename T, uint32_t _Size>
	class CircularArray
	{
	public:
		const T& Peek()
		{
			return m_Array[m_Index];
		}

		T& Get()
		{
			int32_t index = m_Index;
			m_Index = (m_Index + 1) % _Size;
			return m_Array[index];
		}

		void Push(const T& value)
		{
			m_Array[m_Index] = value;
			m_Index = (m_Index + 1) % _Size;
		}

		T& operator[](const int32_t index) const
		{
			return m_Array[index % _Size];
		}

		uint32_t GetIndex() const
		{
			return m_Index;
		}

		constexpr int32_t Size() const
		{
			return _Size;
		}

		const std::array<T, _Size>& GetData() const
		{
			return m_Array;
		}

	private:
		uint32_t m_Index = 0;
		std::array<T, _Size> m_Array;
	};
}
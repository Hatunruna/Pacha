#pragma once
#include "core/Assert.h"

#include <array>
#include <atomic>

namespace Pacha
{
	template<typename T, uint32_t _Size>
	class LockableCircularArray
	{
	public:
		const T& Peek()
		{
			return m_Array[m_Index];
		}

		T& Get()
		{
			if (m_NextIndex == m_LockedIndex)
				m_NextIndex = (m_NextIndex + 1) % _Size;

			m_Index = m_NextIndex;
			m_NextIndex = (m_NextIndex + 1) % _Size;
			return m_Array[m_Index];
		}

		T& operator[](const uint32_t index)
		{
			PACHA_ASSERT((index % _Size) != m_LockedIndex, "Trying to reference a locked element");
			return m_Array[index % _Size];
		}

		uint32_t GetIndex() const
		{
			return m_Index;
		}

		T& LockElement(const uint32_t index)
		{
			PACHA_ASSERT(m_LockedIndex == UINT_MAX, "There is an element already locked");
			m_LockedIndex = (index % _Size);
			return m_Array[m_LockedIndex];
		}

		void UnLockElement()
		{
			PACHA_ASSERT(m_LockedIndex != -1, "There is no locked element");
			m_LockedIndex = UINT_MAX;
		}

		constexpr int32_t Size() const
		{
			return _Size;
		}

		const std::array<T, _Size>& GetData() const
		{
			return m_Array;
		}

	private:
		uint32_t m_Index = 0;
		uint32_t m_NextIndex = 0;
		std::array<T, _Size> m_Array;
		std::atomic_uint32_t m_LockedIndex = -1;
	};
}
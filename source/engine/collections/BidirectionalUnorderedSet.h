#pragma once
#include "core/Assert.h"
#include <unordered_set>

namespace Pacha
{
	template<class K, class V>
	class BidirectionalUnorderedSet
	{
	public:
		
		class iterator 
		{
		public:
			using value_type = std::pair<typename std::unordered_set<K>::iterator, typename std::unordered_set<V>::iterator>;
			using difference_type = std::ptrdiff_t;
			using iterator_category = std::bidirectional_iterator_tag;
			using pointer = value_type*;
			using reference = value_type&;

			iterator(const value_type& itPair)
				: m_IteratorPair(std::move(itPair))
			{}

			pointer operator->() { return &m_IteratorPair; }
			reference operator*() { return m_IteratorPair; }
			iterator& operator++() { ++m_IteratorPair.first; ++m_IteratorPair.second; return *this; }
			iterator operator++(int) { iterator tmp = *this; ++(*this); return tmp; }

			friend bool operator== (const iterator& a, const iterator& b) { return a.m_IteratorPair.first == b.m_IteratorPair.first; };
			friend bool operator!= (const iterator& a, const iterator& b) { return a.m_IteratorPair.first != b.m_IteratorPair.first; };

		private:
			value_type m_IteratorPair = {};
		};

		BidirectionalUnorderedSet() = default;
		BidirectionalUnorderedSet(std::initializer_list<std::pair<K, V>> list)
		{
			for (const auto& elem : list)
				Insert(elem);
		}

		iterator begin() { return iterator({ m_Keys.begin(), m_Values.begin() }); }
		iterator end() { return iterator({ m_Keys.end(), m_Values.end() }); }

		void Insert(const std::pair<K, V>& kvPair)
		{
			if (!HasKey(key))
			{
				m_Keys.emplace_hint(m_Keys.begin(), std::move(kvPair.first));
				m_Values.emplace_hint(m_Values.begin(), std::move(kvPair.second));
			}
		}

		void Insert(const K& key, const V& value)
		{
			if (!HasKey(key))
			{
				m_Keys.emplace_hint(m_Keys.begin(), std::move(key));
				m_Values.emplace_hint(m_Values.begin(), std::move(value));
			}
		}

		void EraseUsingKey(const K& key)
		{
			if (HasKey(key))
			{
				const auto keyIterator = m_Keys.find(key);
				const auto offset = std::distance(m_Keys.begin(), keyIterator);

				auto valueIterator = m_Values.begin();
				std::advance(valueIterator, offset);

				m_Keys.erase(keyIterator);
				m_Values.erase(valueIterator);
			}
		}

		void EraseUsingValue(const V& value)
		{
			if (HasValue(value))
			{
				const auto valueIterator = m_Values.find(value);
				const auto offset = std::distance(m_Values.begin(), valueIterator);

				auto keyIterator = m_Keys.begin();
				std::advance(keyIterator, offset);

				m_Keys.erase(keyIterator);
				m_Values.erase(valueIterator);
			}
		}

		bool HasKey(const K& key) const
		{
			return m_Keys.count(key);
		}

		bool HasValue(const V& value) const
		{
			return m_Values.count(value);
		}

		V GetValue(const K& key) const
		{
			if (HasKey(key))
			{
				const size_t offset = FindKeyIndex(key);
				auto valueIterator = m_Values.begin();
				std::advance(valueIterator, offset);
				return *valueIterator;
			}
			else
			{
				LOGERROR("Key not found");
				return V{};
			}
		}

		K GetKey(const V& value) const
		{
			if (HasValue(value))
			{
				const size_t offset = FindValueIndex(value);
				auto keyIterator = m_Keys.begin();
				std::advance(keyIterator, offset);
				return *keyIterator;
			}
			else
			{
				LOGERROR("Value not found");
				return K{};
			}
		}

		size_t Size() const
		{
			return m_Keys.size();
		}

	private:
		inline size_t FindKeyIndex(const K& key) const
		{
			const auto keyIterator = m_Keys.find(key);
			return std::distance(m_Keys.begin(), keyIterator);
		}

		inline size_t FindValueIndex(const V& value) const
		{
			const auto valueIterator = m_Values.find(value);
			return std::distance(m_Values.begin(), valueIterator);
		}

		std::unordered_set<K> m_Keys;
		std::unordered_set<V> m_Values;
	};
}
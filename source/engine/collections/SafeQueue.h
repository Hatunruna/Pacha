#pragma once
#include <queue>
#include <shared_mutex>

namespace Pacha
{
	template<typename T>
	class SafeQueue
	{
	public:
		bool Empty()
		{
			std::shared_lock lock(m_Mutex);
			return m_Queue.empty();
		}

		size_t Size()
		{
			std::shared_lock lock(m_Mutex);
			return m_Queue.size();
		}

		T Front()
		{
			std::unique_lock lock(m_Mutex);
			T value = m_Queue.front();
			m_Queue.pop();
			return value;
		}

		T& FrontReference()
		{
			std::shared_lock lock(m_Mutex);
			return m_Queue.front();
		}

		bool TryFront(T& out)
		{
			if (Empty())
				return false;

			std::unique_lock lock(m_Mutex);
			if (!m_Queue.empty())
			{
				out = m_Queue.front();
				m_Queue.pop();
				return true;
			}

			return false;
		}

		T& Back()
		{
			std::unique_lock lock(m_Mutex);
			T value = m_Queue.back();
			m_Queue.pop();
			return value;
		}

		T& BackReference()
		{
			std::shared_lock lock(m_Mutex);
			return m_Queue.back();
		}

		void Push(T& value)
		{
			std::unique_lock lock(m_Mutex);
			m_Queue.push(value);
		}

		void Push(T&& value)
		{
			std::unique_lock lock(m_Mutex);
			m_Queue.push(value);
		}

		void Pop()
		{
			std::unique_lock lock(m_Mutex);
			m_Queue.pop();
		}

	private:
		std::queue<T> m_Queue;
		std::shared_mutex m_Mutex;
	};
}
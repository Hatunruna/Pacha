#pragma once
#include "core/Assert.h"
#include "utilities/Hash.h"
#include "serialization/Serialization.h"

#include <vector>
#include <string>
#include <numeric>
#include <type_traits>

namespace Pacha
{
	template<typename BlockType, typename = std::enable_if_t<std::is_integral_v<BlockType>>>
	class DynamicBitSet : public SerializableObject
	{
		static constexpr size_t kBitsPerBlock = std::numeric_limits<BlockType>::digits;

	public:
		DynamicBitSet() = default;
		~DynamicBitSet() = default;

		template<typename InitialValueType, typename = std::enable_if_t<std::is_integral_v<BlockType>>>
		DynamicBitSet(const InitialValueType initialValue)
		{
			constexpr size_t kInitValueBitSize = std::numeric_limits<InitialValueType>::digits;
			constexpr size_t kNeededBlocks = (kInitValueBitSize / kBitsPerBlock) + static_cast<size_t>(kInitValueBitSize % kBitsPerBlock > 0);
			constexpr BlockType kMask = std::numeric_limits<BlockType>::max();

			m_BitCount = kInitValueBitSize;
			m_Blocks.resize(kNeededBlocks, 0);

			if constexpr (kNeededBlocks == 1)
				m_Blocks[0] = static_cast<BlockType>(initialValue);
			else
			{
				for (size_t block = 0; block < kNeededBlocks; ++block)
					m_Blocks[block] = static_cast<BlockType>(kMask & (initialValue >> (block * kBitsPerBlock)));
			}
		}

		template<typename InitialValueType>
		DynamicBitSet(const size_t bitCount, const InitialValueType initialValue)
		{
			PACHA_ASSERT(bitCount <= std::numeric_limits<InitialValueType>::digits, "Bit count needs to be less or equal to the input value type bit count");

			const size_t kInitValueBitSize = bitCount;
			const size_t kNeededBlocks = (kInitValueBitSize / kBitsPerBlock) + static_cast<size_t>(kInitValueBitSize % kBitsPerBlock > 0);
			constexpr BlockType kMask = std::numeric_limits<BlockType>::max();

			m_BitCount = kInitValueBitSize;
			m_Blocks.resize(kNeededBlocks, 0);

			if (kNeededBlocks == 1)
				m_Blocks[0] = static_cast<BlockType>(initialValue);
			else
			{
				for (size_t block = 0; block < kNeededBlocks; ++block)
					m_Blocks[block] = static_cast<BlockType>(kMask & (initialValue >> (block * kBitsPerBlock)));
			}
		}

		DynamicBitSet(const size_t bitCount, const std::vector<BlockType> blocks)
		{
			m_BitCount = bitCount;
			m_Blocks = blocks;
		}

		DynamicBitSet(const std::string& initialValue)
		{
			const size_t kInitValueBitSize = initialValue.size();
			const size_t kNeededBlocks = kInitValueBitSize / kBitsPerBlock + static_cast<size_t>(kInitValueBitSize % kBitsPerBlock > 0);

			m_BitCount = kInitValueBitSize;
			m_Blocks.resize(kNeededBlocks, 0);

			size_t bitsLeft = kInitValueBitSize;
			size_t stringPos = kInitValueBitSize - 1;
			for (size_t block = 0; block < kNeededBlocks; ++block)
			{
				size_t bitsToFill = std::min(bitsLeft, kBitsPerBlock);
				for (size_t bit = 0; bit < bitsToFill; ++bit)
				{
					if (initialValue[stringPos--] == '1')
						m_Blocks[block] |= (1 << bit);
				}

				bitsLeft -= kBitsPerBlock;
			}
		}

		DynamicBitSet(const char* initialValue)
			: DynamicBitSet(std::string(initialValue))
		{ }

		const std::string ToString()
		{
			std::string string;
			string.resize(m_BitCount, '0');

			for (size_t i = 0; i < m_BitCount; ++i)
			{
				if (Test(i))
					string[m_BitCount - i - 1] = '1';
			}

			return string;
		}

		size_t GetBitCount() const { return m_BitCount; }
		size_t GetBlockCount() const { return m_Blocks.size(); }
		const std::vector<BlockType>& GetBlocks() const { return m_Blocks; }
		const BlockType& GetBlock(const size_t block) const { return m_Blocks[block]; }

		void Clear()
		{
			m_Blocks.clear();
			m_BitCount = 0;
		}

		void Resize(const size_t bitCount)
		{
			if (m_BitCount == bitCount)
				return;

			if (bitCount == 0)
			{
				Clear();
				return;
			}

			const size_t kInitValueBitSize = bitCount;
			const size_t kNeededBlocks = (kInitValueBitSize / kBitsPerBlock) + static_cast<size_t>(kInitValueBitSize % kBitsPerBlock > 0);

			if (m_Blocks.size() && kNeededBlocks <= m_Blocks.size())
			{
				const size_t blockOffset = bitCount % kBitsPerBlock;
				if (blockOffset != 0)
				{
					const size_t blockId = bitCount / kBitsPerBlock;
					const size_t blockBitStart = blockId * kBitsPerBlock;

					for (size_t i = kBitsPerBlock - 1; i >= blockOffset; --i)
					{
						const size_t bitIndex = blockBitStart + i;
						if(bitIndex < m_BitCount)
							Unset(bitIndex);
					}
				}
			}

			m_Blocks.resize(kNeededBlocks);
			m_BitCount = bitCount;
		}

		void Set(const size_t index, const bool resize = false)
		{
			if (resize)
			{
				if (index + 1 > kBitsPerBlock * m_Blocks.size())
					Resize(index + 1);
			}

			PACHA_ASSERT(index < kBitsPerBlock * m_Blocks.size(), "Index out of range");

			const size_t blockId = index / kBitsPerBlock;
			const size_t blockOffset = index % kBitsPerBlock;
			m_Blocks[blockId] |= static_cast<BlockType>(1) << blockOffset;

			if (index >= m_BitCount)
				m_BitCount = index + 1;
		}

		void Unset(const size_t index)
		{
			PACHA_ASSERT(index < kBitsPerBlock * m_Blocks.size(), "Index out of range");

			const size_t blockId = index / kBitsPerBlock;
			const size_t blockOffset = index % kBitsPerBlock;
			m_Blocks[blockId] &= ~(static_cast<BlockType>(1) << blockOffset);
		}

		void Push(const size_t value = 0)
		{
			Resize(m_BitCount + 1);

			if (value)
				Set(m_BitCount - 1);
		}

		bool Test(const size_t index)
		{
			PACHA_ASSERT(index < kBitsPerBlock * m_Blocks.size(), "Index out of range");

			if (index == 0)
				return static_cast<BlockType>((m_Blocks[0] & static_cast<BlockType>(1)) != static_cast<BlockType>(0));
			else
			{
				const size_t blockId = index / kBitsPerBlock;
				const size_t blockOffset = index % kBitsPerBlock;
				return (m_Blocks[blockId] & (static_cast<BlockType>(1) << blockOffset)) != static_cast<BlockType>(0);
			}
		}

		bool operator==(const DynamicBitSet<BlockType>& rhs)
		{
			if (m_Blocks.size() != rhs.m_Blocks.size())
				return false;

			bool returnValue = true;
			for (size_t i = 0; i < m_Blocks.size(); ++i)
				returnValue &= (m_Blocks[i] == rhs.m_Blocks[i]);
			return returnValue;
		}

		bool operator!=(const DynamicBitSet<BlockType>& rhs)
		{
			if (m_Blocks.size() != rhs.m_Blocks.size())
				return true;

			bool returnValue = false;
			for (size_t i = 0; i < m_Blocks.size(); ++i)
				returnValue |= (m_Blocks[i] != rhs.m_Blocks[i]);
			return returnValue;
		}

		DynamicBitSet<BlockType> operator~()
		{
			DynamicBitSet<BlockType> returnBitSet = *this;
			for (size_t i = 0; i < returnBitSet.m_Blocks.size(); ++i)
				returnBitSet.m_Blocks[i] = ~returnBitSet.m_Blocks[i];
			return returnBitSet;
		}

		DynamicBitSet<BlockType>& operator&=(const DynamicBitSet<BlockType>& rhs)
		{
			size_t minBlockCount = std::min(m_Blocks.size(), rhs.m_Blocks.size());
			for (size_t i = 0; i < minBlockCount; ++i)
				m_Blocks[i] &= rhs.m_Blocks[i];
			return *this;
		}

		DynamicBitSet<BlockType> operator&(const DynamicBitSet<BlockType>& rhs)
		{
			DynamicBitSet<BlockType> returnBitSet = *this;
			return returnBitSet &= rhs;
		}

		DynamicBitSet<BlockType>& operator|=(const DynamicBitSet<BlockType>& rhs)
		{
			size_t minBlockCount = std::min(m_Blocks.size(), rhs.m_Blocks.size());
			for (size_t i = 0; i < minBlockCount; ++i)
				m_Blocks[i] |= rhs.m_Blocks[i];
			return *this;
		}

		DynamicBitSet<BlockType> operator|(const DynamicBitSet<BlockType>& rhs)
		{
			DynamicBitSet<BlockType> returnBitSet = *this;
			return returnBitSet |= rhs;
		}

		DynamicBitSet<BlockType>& operator^=(const DynamicBitSet<BlockType>& rhs)
		{
			size_t minBlockCount = std::min(m_Blocks.size(), rhs.m_Blocks.size());
			for (size_t i = 0; i < minBlockCount; ++i)
				m_Blocks[i] ^= rhs.m_Blocks[i];
			return *this;
		}

		DynamicBitSet<BlockType> operator^(const DynamicBitSet<BlockType>& rhs)
		{
			DynamicBitSet<BlockType> returnBitSet = *this;
			return returnBitSet ^= rhs;
		}

		DynamicBitSet<BlockType>& operator<<=(const size_t shift)
		{
			if (shift == 0)
				return *this;

			if (shift >= m_BitCount)
			{
				for (auto& block : m_Blocks)
					block = 0;
				return *this;
			}

			const int blockShift = static_cast<int>(shift / kBitsPerBlock);
			const size_t shiftOffset = shift % kBitsPerBlock;

			for (int block = static_cast<int>(m_Blocks.size()) - 1; block >= 0 ; --block)
			{
				const int newBlockIndex = block - blockShift;
				
				if (newBlockIndex >= 0)
				{
					m_Blocks[block] = m_Blocks[newBlockIndex] << shiftOffset;
					if (block - 1 >= 0)
						m_Blocks[block] |= (m_Blocks[block - 1] >> (kBitsPerBlock - shiftOffset));
				}
				else
					m_Blocks[block] = static_cast<BlockType>(0);
			}

			return *this;
		}

		DynamicBitSet<BlockType> operator<<(const size_t shift)
		{
			DynamicBitSet<BlockType> returnBitSet = *this;
			return returnBitSet <<= shift;
		}

		DynamicBitSet<BlockType>& operator>>=(const size_t shift)
		{
			if (shift == 0)
				return *this;

			if (shift >= m_BitCount)
			{
				for (auto& block : m_Blocks)
					block = 0;
				return *this;
			}

			const int blockShift = static_cast<int>(shift / kBitsPerBlock);
			const size_t shiftOffset = shift % kBitsPerBlock;

			for (int block = 0; block < m_Blocks.size(); ++block)
			{
				const int newBlockIndex = block + blockShift;

				if (newBlockIndex < m_Blocks.size())
				{
					m_Blocks[block] = m_Blocks[newBlockIndex] >> shiftOffset;
					if (block + 1 < m_Blocks.size())
						m_Blocks[block] |= (m_Blocks[block + 1] << (kBitsPerBlock - shiftOffset));
				}
				else
					m_Blocks[block] = static_cast<BlockType>(0);
			}

			return *this;
		}

		DynamicBitSet<BlockType> operator>>(const size_t shift)
		{
			DynamicBitSet<BlockType> returnBitSet = *this;
			return returnBitSet >>= shift;
		}

		friend bool operator==(const DynamicBitSet<BlockType>& lhs, const DynamicBitSet<BlockType>& rhs)
		{
			if (lhs.m_Blocks.size() != rhs.m_Blocks.size())
				return false;

			bool returnValue = true;
			for (size_t i = 0; i < lhs.m_Blocks.size(); ++i)
				returnValue &= (lhs.m_Blocks[i] == rhs.m_Blocks[i]);
			return returnValue;
		}

	private:
		size_t m_BitCount = 0;
		std::vector<BlockType> m_Blocks = {};

		friend class Serialization::Accessor;
		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& serializer)
		{
			serializer
			(
				MakeNVP("BitCount", m_BitCount),
				MakeNVP("Blocks", m_Blocks)
			);
		}
	};
}

namespace std
{
	template <typename BlockType>
	struct hash<Pacha::DynamicBitSet<BlockType>>
	{
		size_t operator()(const Pacha::DynamicBitSet<BlockType>& dynamicBitset) const
		{
			size_t hash = 0;
			const size_t blockCount = dynamicBitset.GetBlockCount();

			for (size_t i = 0; i < blockCount; ++i)
				Pacha::HashCombine(hash, dynamicBitset.GetBlock(i));

			return hash;
		}
	};
}
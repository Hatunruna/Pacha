#pragma once
#include "core/Assert.h"

#include <type_traits>
#include <unordered_map>

namespace Pacha
{
	template<typename Key, typename Value, int32_t Frames, bool ManageMemory = false>
	class FrameCountedMap
	{
	public:
		~FrameCountedMap()
		{
			Clear();
		}

		struct FrameCountedPair
		{
			Value value = {};
			int32_t frame = Frames;
		};

		void ForEach(std::function<void(Key&, Value&)> function)
		{
			for (auto& [key, pair] : m_Elements)
				function(key, pair.value);
		}

		bool Contains(const Key& key)
		{
			return m_Elements.count(key);
		}

		Value* Find(const Key& key)
		{
			if (m_Elements.count(key))
			{
				m_Elements[key].frame = Frames;
				return &m_Elements[key].value;
			}
			else
				return nullptr;
		}

		Value& FindOrAdd(const Key& key)
		{
			if (m_Elements.count(key))
				m_Elements[key].frame = Frames;
			else
				m_Elements[key] = {};

			return m_Elements[key].value;
		}

		void ClearFrameCounted()
		{
			for (auto iterator = m_Elements.begin(); iterator != m_Elements.end(); ++iterator)
			{
				--iterator->second.frame;
				if (iterator->second.frame <= 0)
				{
					if constexpr (ManageMemory && std::is_pointer_v<Value>)
						delete iterator->second.value;
			
					iterator = m_Elements.erase(iterator);
					if (iterator == m_Elements.end())
						break;
				}
			}
		}

		void Clear()
		{
			if constexpr (ManageMemory && std::is_pointer_v<Value>)
			{
				for (auto& [key, pair] : m_Elements)
					delete pair.value;
			}

			m_Elements.clear();
		}

	private:
		std::unordered_map<Key, FrameCountedPair> m_Elements;
	};
}
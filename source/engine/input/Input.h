#pragma once
#include <glm/glm.hpp>
#include <unordered_map>

#if PLATFORM_DESKTOP
#include "MappingSDL.h"
#endif

namespace Pacha
{
	struct PressState
	{
		enum class State
		{
			PRESSED,
			HELD,
			RELEASED,
			IDLE
		} currentState = State::IDLE;

		void Validate()
		{
			if (currentState == State::PRESSED)
				currentState = State::HELD;
			else if (currentState == State::RELEASED)
				currentState = State::IDLE;
		}
	};

	struct InputState
	{
		glm::vec2 mouseDelta;
		glm::vec2 mouseWheel;
		glm::vec2 mousePosition;
		std::unordered_map<KeyCode, PressState> keyStates;
		std::unordered_map<MouseButton, PressState> mouseButtonStates;
	};

	class Input
	{
		friend class PlatformEventsSDL;

	public:
		static bool GetKey(const KeyCode& key);
		static bool GetKeyUp(const KeyCode& key);
		static bool GetKeyDown(const KeyCode& key);

		static const glm::vec2& GetMouseWheel();
		static const glm::vec2& GetMouseDelta();
		static const glm::vec2& GetMousePosition();

		static bool GetMouseButton(const MouseButton& button);
		static bool GetMouseButtonUp(const MouseButton& button);
		static bool GetMouseButtonDown(const MouseButton& button);

	private:
		static void ValidateState();
		static InputState m_InputState;
	};
}
#include "Input.h"

namespace Pacha
{
	InputState Input::m_InputState;

	bool Input::GetKey(const KeyCode& key)
	{
		return m_InputState.keyStates[key].currentState == PressState::State::HELD;
	}
	
	bool Input::GetKeyUp(const KeyCode& key)
	{
		return m_InputState.keyStates[key].currentState == PressState::State::RELEASED;
	}

	bool Input::GetKeyDown(const KeyCode& key)
	{
		return m_InputState.keyStates[key].currentState == PressState::State::PRESSED;
	}

	bool Input::GetMouseButton(const MouseButton& button)
	{
		return m_InputState.mouseButtonStates[button].currentState == PressState::State::HELD;
	}

	bool Input::GetMouseButtonUp(const MouseButton& button)
	{
		return m_InputState.mouseButtonStates[button].currentState == PressState::State::RELEASED;
	}

	bool Input::GetMouseButtonDown(const MouseButton& button) 
	{
		return m_InputState.mouseButtonStates[button].currentState == PressState::State::PRESSED;
	}

	const glm::vec2& Input::GetMouseWheel()
	{
		return m_InputState.mouseWheel;
	}
	
	const glm::vec2& Input::GetMouseDelta()
	{
		return m_InputState.mouseDelta;
	}
	
	const glm::vec2& Input::GetMousePosition()
	{
		return m_InputState.mousePosition;
	}
	
	void Input::ValidateState()
	{
		m_InputState.mouseWheel = glm::vec2(0.f);
		m_InputState.mouseDelta = glm::vec2(0.f);

		for (auto& [keyCode, state] : m_InputState.keyStates)
			state.Validate();
	}
}
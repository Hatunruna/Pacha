#include "Component.h"

namespace Pacha
{
	bool Component::IsActive() const
	{
		return m_IsActive;
	}

	void Component::SetActive(const bool& state)
	{
		m_IsActive = state;
	}

	Entity* Component::GetEntity() const
	{
		return m_Entity;
	}

	void Component::SetEntity(Entity* entity)
	{
		m_Entity = entity;
	}
}
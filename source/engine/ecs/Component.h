#pragma once
#include "utilities/PachaId.h"
#include "ecs/ComponentTraits.h"
#include "serialization/Serialization.h"

#if PACHA_EDITOR
#include "graphics/ui/utilities/InspectorBuilder.h"
#endif

#include <vector>
#include <functional>
#include <unordered_map>
#include <entt/entt.hpp>

#define REGISTER_COMPONENT(component)	namespace Pacha { class component; } template<> struct Pacha::IsComponent<Pacha::component> : std::true_type { };					 \
										template<> struct entt::internal::in_place_delete<Pacha::component> : std::true_type {};											 \
										namespace Pacha { static const ComponentStorageWarmUpFunctionInitializer<component> s_ComponentWarmup##component(#component##_ID); }

#define COMPONENT_TYPE_INFO	public:																		\
							const entt::type_info& GetTypeInfo() const override							\
							{																			\
								using T = std::remove_cv_t<std::remove_reference_t<decltype(*this)>>;	\
								return entt::type_id<T>();												\
							}

namespace Pacha { namespace Serialization { class ComponentAccesor; } }

namespace Pacha
{
	class ComponentStorageWarmUp
	{
	public:
		static void WarmUp(entt::registry& registry)
		{
			for (const auto& [id, function] : m_WarmUpFunctions)
				function(registry);
		}

		static void AddFunction(const PachaId& id, const std::function<void(entt::registry&)>& function)
		{
			if (m_WarmUpFunctions.count(id))
				return;

			m_WarmUpFunctions[id] = function;
		}

	private:
		static inline std::unordered_map<PachaId, std::function<void(entt::registry&)>> m_WarmUpFunctions;
	};

	template<typename T>
	class ComponentStorageWarmUpFunctionInitializer
	{
	public:
		ComponentStorageWarmUpFunctionInitializer(const PachaId& id)
		{
			ComponentStorageWarmUp::AddFunction(id, std::bind(&ComponentStorageWarmUpFunctionInitializer<T>::WarmUpComponent, std::placeholders::_1));
		}

	private:
		static void WarmUpComponent(entt::registry& registry)
		{
			registry.storage<T>();
		}
	};

	class Component : public SerializableObject
	{
	public:
		bool IsActive() const;
		virtual void SetActive(const bool& state);

		class Entity* GetEntity() const;
		virtual void OnCreate() {};
		virtual void OnDestroy() {};
		virtual const entt::type_info& GetTypeInfo() const = 0;

#if PACHA_EDITOR
		virtual void DrawInspectorUI(class InspectorBuilder&) {};
#endif

	protected:
		class Entity* m_Entity;
		bool m_IsActive = true;

	private:
		friend class Entity;
		friend class ScriptingInterface;
		friend Serialization::ComponentAccesor;

		void SetEntity(class Entity* entity);
	};
}
#pragma once
#include "core/Assert.h"
#include "level/Level.h"
#include "serialization/Serialization.h"

#include <string>
#include <entt/entt.hpp>

namespace Pacha { class Entity; }
namespace Pacha { namespace Serialization { class EntityAccesor; } }
template<> struct entt::internal::in_place_delete<Pacha::Entity> : std::true_type {};

namespace Pacha
{
	class Entity* CreateEntity(const std::string&, Level* level = nullptr);
	void DestroyEntity(class Entity* entity);

#if PACHA_EDITOR
	enum EditorFlagBits
	{
		EditorFlags_HIDDEN_IN_EDITOR		= 1 << 0,
		EditorFlags_UPDATE_IN_EDITOR_MODE	= 1 << 1
	};
	typedef uint32_t EditorFlags;
#endif

	class Entity : public SerializableObject
	{
	public:
		//Components
		template<typename T, typename = std::enable_if_t<IsComponentV<T> || IsScriptV<T>>>
		T* AddComponent()
		{
			PACHA_ASSERT(!HasComponent<T>(), "Component already in entity");
			Entity& entity = m_Level->GetStorage<Entity>().get(m_EnttId);

			T& component = m_Level->GetStorage<T>().emplace(m_EnttId);
			component.SetEntity(&entity);
			component.OnCreate();
			return &component;
		}

		template<typename T, typename = std::enable_if_t<IsComponentV<T> || IsScriptV<T>>>
		T* GetComponent() const
		{
			if(HasComponent<T>())
				return &m_Level->GetStorage<T>().get(m_EnttId);
			return nullptr;
		}

		template<typename T, typename = std::enable_if_t<IsComponentV<T> || IsScriptV<T>>>
		bool HasComponent() const
		{
			return m_Level->GetStorage<T>().contains(m_EnttId);
		}

		template<typename T, typename = std::enable_if_t<IsComponentV<T> || IsScriptV<T>>>
		void RemoveComponent()
		{
			if (T* component = GetComponent<T>())
			{
				component->OnDestroy();
				m_Level->GetStorage<T>().remove(m_EnttId);
			}
		}

		//Helpers
		template<typename T, typename = std::enable_if_t<IsComponentV<T>>>
		std::vector<T*> GetComponentsInChildren(const bool recursive = true, const bool includeInactiveChildren = true)
		{
			std::vector<T*> components;
			if (Transform* transform = GetComponent<Transform>())
			{
				std::vector<Transform*> children = transform->GetChildren(recursive);
				for (auto& child : children)
				{
					Entity* entity = child->GetEntity();
					if (entity->IsActive() || includeInactiveChildren)
					{
						if (T* component = entity->GetComponent<T>())
							components.push_back(component);
					}
				}
			}

			return components;
		}

		template<typename T, typename = std::enable_if_t<IsScriptV<T>>>
		std::vector<T*> GetScriptsInChildren(const bool recursive = true, const bool includeInactiveChildren = true)
		{
			std::vector<T*> scripts;
			if (Transform* transform = GetComponent<Transform>())
			{
				std::vector<Transform*> children = transform->GetChildren(recursive);
				for (auto& child : children)
				{
					Entity* entity = child->GetEntity();
					if (entity->IsActive() || includeInactiveChildren)
					{
						if (T* script = entity->GetScript<T>())
							scripts.push_back(script);
					}
				}
			}

			return components;
		}

		const std::string& GetName() const;
		void SetName(const std::string& name);

		bool IsActive() const;
		void SetActive(const bool& state);

		Level* GetLevel();
		entt::entity GetEnttId() const;

#if PACHA_EDITOR
		const EditorFlags& GetEditorFlags();
		void SetEditorFlags(const EditorFlags& flags);
#endif

	protected:
		Level* m_Level = nullptr;
		entt::entity m_EnttId = {};
		
		bool m_IsActive = true;
		std::string m_Name = {};
#if PACHA_EDITOR
		EditorFlags m_EditorFlags = 0;
#endif

	private:
		void SetLevel(Level* level);
		void SetEnttId(entt::entity entity);

		friend class EntityAccesor;
		friend class EntityInspector;
		friend Serialization::EntityAccesor;

		friend class Serialization::Accessor;
		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& serializer)
		{
			serializer
			(
				MakeNVP("Name", m_Name),
				MakeNVP("Active", m_IsActive)
			);
		}
	};
}
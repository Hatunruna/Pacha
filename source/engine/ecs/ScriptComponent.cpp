#include "ScriptComponent.h"

namespace Pacha
{
	void ScriptComponent::SetActive(const bool& state)
	{
		if (m_IsActive != state)
		{
			m_IsActive = state;
		
			if (m_IsActive)
				OnEnable();
			else
				OnDisable();
		}
	}
}
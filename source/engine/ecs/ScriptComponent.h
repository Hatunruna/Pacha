#pragma once
#include "Component.h"
#include "core/SystemInfo.h"
#include "core/graph/context/SimulationContext.h"

#define REGISTER_SCRIPT_ORDER(component, order)	namespace Pacha { class component; } template<> struct Pacha::IsScript<Pacha::component> : std::true_type { };							\
												template<> struct entt::internal::in_place_delete<Pacha::component> : std::true_type {};												\
												namespace Pacha { static const ScriptComponentUpdateInitializer<component, order> s_ScriptComponentUpdate##component(#component##_ID); }

#define REGISTER_SCRIPT(component)	REGISTER_SCRIPT_ORDER(component, 0)

namespace Pacha
{
	class ScriptComponentUpdateDatabase
	{
	public:
		static void Update(SimulationContext& context)
		{
			for (const auto& [order, function] : m_UpdateFunctions)
				function(context);
		}

		static void AddFunction(const PachaId& id, int order, const std::function<void(SimulationContext&)>& function)
		{
			if (m_AddedFunctions.count(id))
				return;

			m_AddedFunctions.emplace(id);
			m_UpdateFunctions.insert(std::pair(order, function));
		}

	private:
		static inline std::unordered_set<PachaId> m_AddedFunctions;
		static inline std::multimap<int, std::function<void(SimulationContext&)>> m_UpdateFunctions;
	};

	template<typename T, int Order>
	class ScriptComponentUpdateInitializer
	{
	public:
		ScriptComponentUpdateInitializer(const PachaId& id)
		{
			ScriptComponentUpdateDatabase::AddFunction(id, Order, std::bind(&ScriptComponentUpdateInitializer<T, Order>::UpdateComponent, std::placeholders::_1));
		}

	private:
		static void UpdateComponent(SimulationContext& context)
		{
			context.GetViews<Entity, T>().ForEach([&](Entity& entity, T& scriptComponent)
			{
				if (entity.IsActive() && scriptComponent.IsActive())
				{
					scriptComponent.OnUpdate(context.GetSimulationTime());
				}
			});
		}
	};

	class ScriptComponent : public Component
	{
	public:
		void SetActive(const bool& state) override;

		virtual void OnUpdate(const SimulationTime&) {};
		virtual void OnEnable() {};
		virtual void OnDisable() {};
		virtual const entt::type_info& GetTypeInfo() const = 0;
	};
}
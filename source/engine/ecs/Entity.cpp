#include "Entity.h"
#include "Component.h"
#include "level/LevelManager.h"

namespace Pacha
{
	const std::string& Entity::GetName() const
	{
		return m_Name;
	}

	void Entity::SetName(const std::string& name)
	{
		m_Name = name;
	}

	bool Entity::IsActive() const
	{
		return m_IsActive;
	}

	void Entity::SetActive(const bool& state)
	{
		m_IsActive = state;
	}

	Level* Entity::GetLevel()
	{
		return m_Level;
	}

	entt::entity Entity::GetEnttId() const
	{
		return m_EnttId;
	}

	void Entity::SetLevel(Level* level)
	{
		m_Level = level;
	}

	void Entity::SetEnttId(entt::entity entity)
	{
		m_EnttId = entity;
	}

#if PACHA_EDITOR
	const EditorFlags& Entity::GetEditorFlags()
	{
		return m_EditorFlags;
	}

	void Entity::SetEditorFlags(const EditorFlags& flags)
	{
		m_EditorFlags = flags;
	}
#endif

	class EntityAccesor
	{
	public:
		static void SetEntityPrivateValues(Entity* entity, entt::entity enttId, Level* level, const std::string& name)
		{
			entity->SetLevel(level);
			entity->SetEnttId(enttId);
			entity->SetName(name);
		}
	};

	Entity* CreateEntity(const std::string& name, Level* level)
	{
		if (!level)
			level = LevelManager::GetInstance().GetCurrentLevel();

		entt::entity enttId = level->GetRegistry().create();
		Entity* entity = &level->GetStorage<Entity>().emplace(enttId);
		EntityAccesor::SetEntityPrivateValues(entity, enttId, level, name);
		return entity;
	}

	void DestroyEntity(Entity* entity)
	{
		Level* level = entity->GetLevel();
		entt::registry& registry = level->GetRegistry();
		const entt::entity enttId = entity->GetEnttId();

		for (auto&& [id, storage] : registry.storage())
		{
			if (storage.type() != entt::type_id<Entity>())
			{
				if (storage.contains(enttId))
				{
					if (Component* component = static_cast<Component*>(storage.value(enttId)))
						component->OnDestroy();

					storage.remove(enttId);
				}
			}
		}

		registry.destroy(enttId);
	}
}
#include "LightSource.h"
#include "Transform.h"

#include <mutex>
#include <queue>

namespace Pacha
{
	static uint32_t s_LightIndex = 0;
	static std::queue<uint32_t> s_FreeIndices;

	void LightSource::OnCreate()
	{
		if (s_FreeIndices.size())
		{
			m_LightIndex = s_FreeIndices.front();
			s_FreeIndices.pop();
		}
		else
			m_LightIndex = s_LightIndex++;
	}

	void LightSource::OnDestroy()
	{
		s_FreeIndices.push(m_LightIndex);
	}

	void LightSource::SetLightType(const LightType& type)
	{
		m_Data.type = type;
		m_IsDirty = true;
	}

	LightType LightSource::GetType()
	{
		return m_Data.type;
	}

	void LightSource::SetColor(const glm::vec3& color)
	{
		m_Data.color = color;
		m_IsDirty = true;
	}

	const glm::vec3& LightSource::GetColor()
	{
		return m_Data.color;
	}

	void LightSource::SetIntensity(const float& intensity)
	{
		m_Data.intensity = intensity;
		m_IsDirty = true;
	}

	float LightSource::GetIntensity()
	{
		return m_Data.intensity;
	}

	void LightSource::SetConeAngle(const float& angle)
	{
		m_Data.coneAngle = glm::radians(angle);
		m_IsDirty = true;
	}
	float LightSource::GetConeAngle()
	{
		return glm::degrees(m_Data.coneAngle);
	}

	void LightSource::SetInnerConeAngle(const float& angle)
	{
		m_Data.innerConeAngle = glm::radians(angle);
		m_IsDirty = true;
	}

	float LightSource::GetInnerConeAngle()
	{
		return glm::degrees(m_Data.innerConeAngle);
	}

	void LightSource::SetRange(const float& newRange)
	{
		m_Data.range = newRange;
		m_IsDirty = true;
	}

	float LightSource::GetRange()
	{
		return m_Data.range;
	}

	void LightSource::SetRadius(const float& radius)
	{
		m_Data.radius = radius;
		m_IsDirty = true;
	}

	float LightSource::GetRadius()
	{
		return m_Data.radius;
	}

	void LightSource::SetAttenuationFactor(const float& factor)
	{
		m_Data.attenuationFactor = factor;
		m_IsDirty = true;
	}

	float LightSource::GetAttenuationFactor()
	{
		return m_Data.attenuationFactor;
	}

	void LightSource::SetCastShadows(const bool& enabled)
	{
		m_CastsShadows = enabled;
	}

	bool LightSource::CastsShadows()
	{
		return m_CastsShadows;
	}

	void LightSource::SetShadowAtlasPosition(const glm::vec4& atlasPosition)
	{
		m_Data.shadowAtlasPosition = atlasPosition;
		m_IsDirty = true;
	}

	const glm::vec4& LightSource::GetShadowAtlasPosition()
	{
		return m_Data.shadowAtlasPosition;
	}

	const LightData& LightSource::GetLightData()
	{
		return m_Data;
	}

	uint32_t LightSource::GetLightIndex()
	{
		return m_LightIndex;
	}

	void LightSource::UpdatePositionalData(Transform& transform)
	{
		if (transform.HasMoved())
		{
			m_Data.direction = transform.Forward();
			m_Data.position = transform.GetWorldPosition();
			m_IsDirty = true;
		}
	}

	bool LightSource::IsDirty()
	{
		return m_IsDirty;
	}
	
	void LightSource::SetClean()
	{
		m_IsDirty = false;
	}
}
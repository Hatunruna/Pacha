#pragma once
#include "ecs/Component.h"
#include "ecs/components/Transform.h"
#include "graphics/structures/GfxViewport.h"
#include "graphics/resources/rendertarget/GfxRenderTarget.h"

#include <memory>
#include <glm/glm.hpp>

REGISTER_COMPONENT(Camera);

namespace Pacha
{
	enum class ViewportSetup
	{
		FULL,

		TOP_HALF,
		BOTTOM_HALF,
		LEFT_HALF,
		RIGHT_HALF,

		TOP_LEFT,
		TOP_RIGHT,
		BOTTOM_LEFT,
		BOTTOM_RIGHT,

		CUSTOM
	};

	enum class ProjectionType
	{
		PERSPECTIVE,
		ORTOGRAPHIC
	};

	enum class CameraType
	{
		GAME	= 1 << 0,
		EDITOR	= 1 << 1,
		PREVIEW = 1 << 2
	};

	class Camera : public Component
	{
		COMPONENT_TYPE_INFO;

	public:
		void SetProjectionType(const ProjectionType& type);
		ProjectionType GetProjectionType() const;

		void SetFieldOfView(const float& fieldOfView);
		float GetFieldOfView()const;

		void SetNearFarPlaneDistance(const glm::vec2& nearFarDistance);
		const glm::vec2& GetNearFarPlaneDistance()const;

		void SetFarPlaneDistance(const float& farPlaneDistance);
		float GetFarPlaneDistance()const;

		void SetNearPlaneDistance(const float& nearPlaneDistance);
		float GetNearPlaneDistance()const;

		void SetClearColor(const glm::vec4& clearColor);
		const glm::vec4& GetClearColor() const;

		void SetViewport(const GfxViewport& viewport);
		void SetViewportSetup(const ViewportSetup& viewportSetup);
		ViewportSetup GetViewportSetup() const;

		GfxRenderTarget* GetRenderTarget() const;
		void SetRenderTarget(GfxRenderTarget* renderTarget);

		const GfxViewport& GetViewport() const;
		const glm::mat4& GetProjectionMatrix() const;

		const glm::mat4& GetViewMatrix() const;
		const glm::mat4& GetViewProjectionMatrix() const;
		const glm::mat4& GetInverseViewProjectionMatrix() const;

		void UpdateViewportAndMatrices(Transform& transform, const glm::uvec2& resolution);

		CameraType GetType() const { return m_CameraType; }
		void SetType(CameraType type) { m_CameraType = type; }

		size_t GetCameraId() const { return reinterpret_cast<std::uintptr_t>(this); }

	private:
		float m_FOV = 60.f;
		bool m_ProjectionNeedsUpdate = true;
		glm::vec2 m_NearFarDistance = glm::vec2(0.005f, 1000.f);

		glm::mat4 m_View = glm::mat4(1.f);
		glm::mat4 m_Projection = glm::mat4(1.f);
		glm::mat4 m_ViewProjection = glm::mat4(1.f);
		glm::mat4 m_InverseViewProjection = glm::mat4(1.f);

		glm::vec4 m_ClearColor = glm::vec4(0.1f, 0.1f, 0.1f, 1.f);
		ProjectionType m_ProjectionType = ProjectionType::PERSPECTIVE;

		GfxViewport m_Viewport = { 0.f, 0.f, 1.f, 1.f, 0.f, 1.f };
		ViewportSetup m_ViewportSetup = ViewportSetup::FULL;
		
		GfxRenderTarget* m_RenderTarget = nullptr;
		glm::uvec2 m_LastResolution = { 1 ,1 };
		CameraType m_CameraType = CameraType::GAME;

		friend class Serialization::Accessor;
		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& /*serializer*/)
		{
			//serializer
			//(
			//	Serialization::MakeNVP("Fov", m_FOV),
			//	Serialization::MakeNVP("NearFarPlane", m_NearFarDistance),
			//	Serialization::MakeNVP("ClearColor", m_ClearColor),
			//	Serialization::MakeNVP("ProjectionType", m_ProjectionType),
			//	Serialization::MakeNVP("ViewportSetup", m_ViewportSetup),
			//	Serialization::MakeNVP("Viewport", m_Viewport)
			//);
		}
	};
}
#include "Camera.h"
#include "ecs/components/Transform.h"

namespace Pacha
{
	void Camera::SetProjectionType(const ProjectionType& type)
	{
		m_ProjectionType = type;
		m_ProjectionNeedsUpdate = true;
	}

	ProjectionType Camera::GetProjectionType() const
	{
		return m_ProjectionType;
	}

	void Camera::SetFieldOfView(const float& fieldOfView)
	{
		m_FOV = glm::clamp(fieldOfView, 1.f, 179.f);
		m_ProjectionNeedsUpdate = true;
	}

	float Camera::GetFieldOfView() const
	{
		return m_FOV;
	}

	void Camera::SetNearFarPlaneDistance(const glm::vec2& nearFarDistance)
	{
		m_NearFarDistance.x = glm::clamp(nearFarDistance.x, 0.005f, nearFarDistance.y);
		m_NearFarDistance.y = glm::clamp(nearFarDistance.y, nearFarDistance.x, std::numeric_limits<float>::max());
		m_ProjectionNeedsUpdate = true;
	}

	const glm::vec2& Camera::GetNearFarPlaneDistance() const
	{
		return m_NearFarDistance;
	}

	void Camera::SetFarPlaneDistance(const float& farPlaneDistance)
	{
		m_NearFarDistance.y = glm::clamp(farPlaneDistance, m_NearFarDistance.x, std::numeric_limits<float>::max());
		m_ProjectionNeedsUpdate = true;
	}

	float Camera::GetFarPlaneDistance() const
	{
		return m_NearFarDistance.y;
	}

	void Camera::SetNearPlaneDistance(const float& nearPlaneDistance)
	{
		m_NearFarDistance.x = glm::clamp(nearPlaneDistance, 0.005f, m_NearFarDistance.y);
		m_ProjectionNeedsUpdate = true;
	}

	float Camera::GetNearPlaneDistance() const
	{
		return m_NearFarDistance.x;
	}

	void Camera::SetClearColor(const glm::vec4& clearColor)
	{
		m_ClearColor = clearColor;
	}

	const glm::vec4& Camera::GetClearColor() const
	{
		return m_ClearColor;
	}

	void Camera::SetViewport(const GfxViewport& viewport)
	{
		m_Viewport = viewport;
		m_ViewportSetup = ViewportSetup::CUSTOM;
		m_ProjectionNeedsUpdate = true;
	}

	void Camera::SetViewportSetup(const ViewportSetup& viewportSetup)
	{
		m_ViewportSetup = viewportSetup;
		m_ProjectionNeedsUpdate = true;
	}

	ViewportSetup Camera::GetViewportSetup() const
	{
		return m_ViewportSetup;
	}

	GfxRenderTarget* Camera::GetRenderTarget() const
	{
		return m_RenderTarget;
	}

	void Camera::SetRenderTarget(GfxRenderTarget* renderTarget)
	{
		PACHA_ASSERT(renderTarget != nullptr, "Invalid RenderTarget");
		m_RenderTarget = renderTarget;
	}

	const GfxViewport& Camera::GetViewport() const
	{
		return m_Viewport;
	}

	const glm::mat4& Camera::GetProjectionMatrix() const
	{
		return m_Projection;
	}

	const glm::mat4& Camera::GetViewMatrix() const
	{
		return m_View;
	}

	const glm::mat4& Camera::GetViewProjectionMatrix() const
	{
		return m_ViewProjection;
	}

	const glm::mat4& Camera::GetInverseViewProjectionMatrix() const
	{
		return m_InverseViewProjection;
	}

	void Camera::UpdateViewportAndMatrices(Transform& transform, const glm::uvec2& resolution)
	{
		const bool resized = m_LastResolution != resolution;
		const bool needsViewUpdate = transform.HasMoved();
		const bool needsProjectionChange = m_ProjectionNeedsUpdate || resized;

		m_LastResolution = resolution;
		m_ProjectionNeedsUpdate = false;

		if (needsProjectionChange)
		{
			float width = static_cast<float>(resolution.x);
			float height = static_cast<float>(resolution.y);

			if (m_ViewportSetup != ViewportSetup::CUSTOM)
			{
				switch (m_ViewportSetup)
				{
					default:
					case ViewportSetup::FULL:
						m_Viewport = { 0.0f, 0.0f, 1.0f, 1.0f, 0.f, 1.f };
						break;

					case ViewportSetup::TOP_HALF:
						m_Viewport = { 0.0f, 0.0f, 1.0f, 0.5f, 0.f, 1.f };
						break;

					case ViewportSetup::BOTTOM_HALF:
						m_Viewport = { 0.0f, 0.5f, 1.0f, 0.5f, 0.f, 1.f };
						break;

					case ViewportSetup::LEFT_HALF:
						m_Viewport = { 0.0f, 0.0f, 0.5f, 1.0f, 0.f, 1.f };
						break;

					case ViewportSetup::RIGHT_HALF:
						m_Viewport = { 0.5f, 0.0f, 0.5f, 1.0f, 0.f, 1.f };
						break;

					case ViewportSetup::TOP_LEFT:
						m_Viewport = { 0.0f, 0.0f, 0.5f, 0.5f, 0.f, 1.f };
						break;

					case ViewportSetup::TOP_RIGHT:
						m_Viewport = { 0.5f, 0.0f, 0.5f, 0.5f, 0.f, 1.f };
						break;

					case ViewportSetup::BOTTOM_LEFT:
						m_Viewport = { 0.0f, 0.5f, 0.5f, 0.5f, 0.f, 1.f };
						break;

					case ViewportSetup::BOTTOM_RIGHT:
						m_Viewport = { 0.5f, 0.5f, 0.5f, 0.5f, 0.f, 1.f };
						break;
				}
			}

			if (m_ProjectionType == ProjectionType::PERSPECTIVE)
			{
				m_Projection = glm::perspectiveFov(
					glm::radians(m_FOV),
					static_cast<float>(m_Viewport.width * width),
					static_cast<float>(m_Viewport.height * height),
					m_NearFarDistance.x,
					m_NearFarDistance.y
				);
			}
			else
			{
				switch (m_ViewportSetup)
				{
					default:
					case ViewportSetup::CUSTOM:
						width *= m_Viewport.width / 2;
						height *= m_Viewport.height / 2;
						break;
					case ViewportSetup::FULL:
						width /= 2;
						height /= 2;
						break;
					case ViewportSetup::TOP_LEFT:
					case ViewportSetup::TOP_RIGHT:
					case ViewportSetup::BOTTOM_LEFT:
					case ViewportSetup::BOTTOM_RIGHT:
						width /= 4;
						height /= 4;
						break;
					case ViewportSetup::TOP_HALF:
					case ViewportSetup::BOTTOM_HALF:
						width /= 2;
						height /= 4;
						break;
					case ViewportSetup::LEFT_HALF:
					case ViewportSetup::RIGHT_HALF:
						width /= 4;
						height /= 2;
						break;
				}

				m_Projection = glm::ortho(-width, width, -height, height, m_NearFarDistance.x, m_NearFarDistance.y);
			}
		}

		if (needsViewUpdate)
			m_View = glm::inverse(transform.GetWorldTransform());

		if (needsViewUpdate || needsProjectionChange)
		{
			m_ViewProjection = m_Projection * m_View;
			m_InverseViewProjection = glm::inverse(m_ViewProjection);
		}
	}
}
#pragma once
#include "ecs/Component.h"
#include "ecs/components/Transform.h"

#include <glm/glm.hpp>

REGISTER_COMPONENT(LightSource);

namespace Pacha
{
	enum class LightType
	{
		DIRECTIONAL,
		POINT,
		SPOT
	};

	struct LightData
	{
		float intensity = 1.f;
		glm::vec3 color = glm::vec3(1.f);

		float radius = 1.f;
		glm::vec3 direction = glm::vec3(0.f, 0.f, 1.f);

		float range = 1.f;
		glm::vec3 position = glm::vec3(0.f);

		LightType type = LightType::DIRECTIONAL;
		float coneAngle = glm::radians(45.f);
		float innerConeAngle = glm::radians(40.f);
		float attenuationFactor = 1.f;

		glm::vec4 shadowAtlasPosition = glm::vec4(0.f, 0.f, 1.f, 1.f);
	};

	class LightSource : public Component
	{
		COMPONENT_TYPE_INFO;

	public:
		void OnCreate() override;
		void OnDestroy() override;

		void SetLightType(const LightType& type);
		LightType GetType();

		void SetColor(const glm::vec3& color);
		const glm::vec3& GetColor();

		void SetIntensity(const float& intensity);
		float GetIntensity();

		void SetConeAngle(const float& angle);
		float GetConeAngle();
		
		void SetInnerConeAngle(const float& angle);
		float GetInnerConeAngle();

		void SetRange(const float& newRange);
		float GetRange();

		void SetRadius(const float& radius);
		float GetRadius();

		void SetAttenuationFactor(const float& factor);
		float GetAttenuationFactor();

		void SetCastShadows(const bool& enabled);
		bool CastsShadows();

		void SetShadowAtlasPosition(const glm::vec4& atlasPosition);
		const glm::vec4& GetShadowAtlasPosition();

		const LightData& GetLightData();
		uint32_t GetLightIndex();
		void UpdatePositionalData(Transform& transform);

		bool IsDirty();
		void SetClean();

	private:
		LightData m_Data;
		uint32_t m_LightIndex = 0;
		
		bool m_IsDirty = false;
		bool m_CastsShadows = false;

		friend class Serialization::Accessor;
		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& /*serializer*/)
		{
			//serializer
			//(
			//	Serialization::MakeNVP("Fov", m_FOV),
			//	Serialization::MakeNVP("NearFarPlane", m_NearFarDistance),
			//	Serialization::MakeNVP("ClearColor", m_ClearColor),
			//	Serialization::MakeNVP("ProjectionType", m_ProjectionType),
			//	Serialization::MakeNVP("RenderOrder", m_RenderOrder),
			//	Serialization::MakeNVP("ViewportSetup", m_ViewportSetup),
			//	Serialization::MakeNVP("Viewport", m_Viewport)
			//);
		}
	};
}
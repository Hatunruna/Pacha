#include "MeshRenderer.h"

#include <mutex>
#include <queue>

namespace Pacha
{
	GfxMesh* MeshRenderer::GetMesh(const size_t index)
	{
		PACHA_ASSERT(index < m_Meshes.size(), "Mesh index out of range");
		return m_Meshes[index];
	}

	void MeshRenderer::SetMesh(GfxMesh* mesh, const size_t index)
	{
		PACHA_ASSERT(index < m_Meshes.size(), "Mesh index out of range");
		m_Meshes[index] = mesh;
		RecalculateBoundingBox();
	}

	const std::vector<GfxMesh*>& MeshRenderer::GetMeshes() const
	{
		return m_Meshes;
	}

	GfxMaterial* MeshRenderer::GetMaterial(const size_t index)
	{
		PACHA_ASSERT(index < m_Materials.size(), "Material index out of range");
		return m_Materials[index];
	}

	void MeshRenderer::SetMaterial(GfxMaterial* material, const size_t index)
	{
		PACHA_ASSERT(index < m_Materials.size(), "Material index out of range");
		m_Materials[index] = material;
	}

	const std::vector<GfxMaterial*>& MeshRenderer::GetMaterials() const
	{
		return m_Materials;
	}

	bool MeshRenderer::IsVisible() const
	{
		return m_RenderFlags & RenderFlags_VISIBLE;
	}

	void MeshRenderer::SetVisible(bool visible)
	{
		if (visible)
			m_RenderFlags |= RenderFlags_VISIBLE;
		else
			m_RenderFlags &= ~RenderFlags_VISIBLE;
	}

	RenderFlags MeshRenderer::GetRenderFlags() const
	{
		return m_RenderFlags;
	}

	void MeshRenderer::SetRenderFlags(const RenderFlags& mask)
	{
		m_RenderFlags = mask;
	}

	const OBB& MeshRenderer::GetOBB() const
	{
		return m_OBB;
	}

	const AABB& MeshRenderer::GetLocalAABB() const
	{
		return m_LocalAABB;
	}

	void MeshRenderer::UpdateOBB(Transform& transform)
	{
		if (transform.HasMoved() || m_OBBIsDirty)
		{
			m_OBB = OBB(m_LocalAABB, transform.GetWorldTransform());
			m_OBBIsDirty = false;
		}
	}

	bool MeshRenderer::IsValid()
	{
		PACHA_ASSERT(m_Meshes.size() == m_Materials.size(), "Mesh and Material amount not the same");

		for (size_t i = 0; i < m_Meshes.size(); ++i)
		{
			if (m_Meshes[i] && m_Materials[i])
				return true;
		}

		return false;
	}

#if PACHA_EDITOR
	void MeshRenderer::SetSelected(bool selected)
	{
		if (selected)
			m_RenderFlags |= RenderFlags_SELECTED;
		else
			m_RenderFlags &= ~RenderFlags_SELECTED;

	}

	void MeshRenderer::SetHovered(bool hovered)
	{
		if (hovered)
			m_RenderFlags |= RenderFlags_HOVERED;
		else
			m_RenderFlags &= ~RenderFlags_HOVERED;
	}
#endif

	void MeshRenderer::RecalculateBoundingBox()
	{
		if (!m_Meshes.empty())
		{
			m_LocalAABB = AABB(glm::vec3(0.f), glm::vec3(0.f));

			for (auto const& mesh : m_Meshes)
				if (mesh) 
					m_LocalAABB.Union(mesh->GetAABB());

			m_OBBIsDirty = true;
		}
	}
}
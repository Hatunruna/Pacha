#include "Transform.h"
#include "ecs/Entity.h"
#include "core/time/SimulationTime.h"

#include <glm/gtx/transform.hpp>

namespace Pacha
{
	void Transform::OnCreate()
	{
		m_Root = m_Entity->GetLevel()->GetRootTransform();
		m_Root->AddChild(this);
		NeedsUpdate();
	}

	void Transform::OnDestroy()
	{
		std::vector<Transform*> children = m_Children;
		for (auto& it : children)
			it->SetParent(nullptr);

		m_Children.clear();
		m_ChildrenToUpdate.clear();

		if (m_Parent)
			m_Parent->RemoveChild(this);
	}

	void Transform::SetLocalPosition(const glm::vec3& newPosition)
	{
		m_LocalPosition = newPosition;
		NeedsUpdate();
	}

	void Transform::SetWorldPosition(const glm::vec3& newPosition)
	{
		if (m_Parent)
			m_LocalPosition = m_Parent->ConvertWorldToLocalPosition(newPosition);
		else
			m_LocalPosition = newPosition;
		NeedsUpdate();
	}

	void Transform::SetLocalRotation(const glm::quat& newRotation)
	{
		m_LocalRotation = glm::normalize(newRotation);
		NeedsUpdate();
	}

	void Transform::SetWorldRotation(const glm::quat& newRotation)
	{
		if (m_Parent)
			m_LocalRotation = m_Parent->ConvertWorldToLocalRotation(newRotation);
		else
			m_LocalRotation = newRotation;

		m_LocalRotation = glm::normalize(m_LocalRotation);
		NeedsUpdate();
	}

	void Transform::SetLocalScale(const glm::vec3& newScale)
	{
		m_LocalScale = newScale;
		NeedsUpdate();
	}

	void Transform::SetWorldScale(const glm::vec3& newScale)
	{
		if (m_Parent)
			m_LocalScale = newScale * (1.f / m_Parent->GetWorldScale());
		else
			m_LocalScale = newScale;
		NeedsUpdate();
	}

	void Transform::SetLocalTransform(const glm::mat4& transform)
	{
		m_LocalTransform = transform;
		DecomposeLocalTransform();
		NeedsUpdate();
	}

	void Transform::SetWorldTransform(const glm::mat4& transform)
	{
		if (m_Parent)
			m_LocalTransform = glm::inverse(m_Parent->GetWorldTransform()) * transform;
		else
			m_LocalTransform = transform;

		DecomposeLocalTransform();
		NeedsUpdate();
	}

	const glm::vec3& Transform::GetLocalPosition()
	{
		return m_LocalPosition;
	}

	const glm::vec3& Transform::GetWorldPosition()
	{
		if (m_NeedsParentUpdate)
			UpdateFromParent();
		return m_WorldPosition;
	}

	const glm::quat& Transform::GetLocalRotation()
	{
		return m_LocalRotation;
	}

	const glm::quat& Transform::GetWorldRotation()
	{
		if (m_NeedsParentUpdate)
			UpdateFromParent();
		return m_WorldRotation;
	}

	const glm::vec3& Transform::GetLocalScale()
	{
		return m_LocalScale;
	}

	const glm::vec3& Transform::GetWorldScale()
	{
		if (m_NeedsParentUpdate)
			UpdateFromParent();
		return m_WorldScale;
	}

	const glm::mat4& Transform::GetLocalTransform()
	{
		if (m_CachedLocalTransformOutOfDate)
		{
			m_LocalTransform = glm::translate(m_LocalPosition);
			m_LocalTransform *= glm::toMat4(m_LocalRotation);
			m_LocalTransform *= glm::scale(m_LocalScale);

			m_CachedLocalTransformOutOfDate = false;
		}

		return m_LocalTransform;
	}

	const glm::mat4& Transform::GetWorldTransform()
	{
		if (m_NeedsParentUpdate)
			UpdateFromParent();

		if (m_CachedWorldTransformOutOfDate)
		{
			if (m_Parent)
				m_WorldTransform = m_Parent->GetWorldTransform() * GetLocalTransform();
			else
				m_WorldTransform = GetLocalTransform();

			m_CachedWorldTransformOutOfDate = false;
		}

		return m_WorldTransform;
	}

	void Transform::Translate(const glm::vec3& translation, const Space& space)
	{
		switch (space)
		{
			case Space::LOCAL:
			{
				m_LocalPosition += m_LocalRotation * translation;
				break;
			}
			case Space::PARENT:
			{
				m_LocalPosition += translation;
				break;
			}
			case Space::WORLD:
			{
				if (m_Parent)
					m_LocalPosition += m_Parent->ConvertWorldToLocalDirection(translation);
				else
					m_LocalPosition += translation;
				break;
			}
		}

		NeedsUpdate();
	}

	void Transform::Rotate(const glm::quat& rotation, const Space& space)
	{
		switch (space)
		{
			case Space::LOCAL:
			{
				m_LocalRotation = m_LocalRotation * rotation;
				break;
			}
			case Space::PARENT:
			{
				m_LocalRotation = rotation * m_LocalRotation;
				break;
			}
			case Space::WORLD:
			{
				m_LocalRotation = m_LocalRotation * glm::inverse(m_WorldRotation) * rotation * m_WorldRotation;
				break;
			}
		}

		m_LocalRotation = glm::normalize(m_LocalRotation);
		NeedsUpdate();
	}

	void Transform::Rotate(const glm::vec3& rotation, const Space& space)
	{
		Rotate(glm::quat(glm::radians(rotation)), space);
	}

	void Transform::Rotate(const float& angle, const glm::vec3& axis, const Space& space)
	{
		Rotate(glm::angleAxis(glm::radians(angle), axis), space);
	}

	void Transform::LookAt(const glm::vec3& at, const glm::vec3& up)
	{
		glm::vec3 pos = GetWorldPosition();
		if (pos == at)
			return;

		glm::vec3 dir = glm::normalize(at - pos);

		if (m_Parent)
			m_LocalRotation = m_Parent->ConvertWorldToLocalRotation(glm::quat_cast(glm::lookAt(pos, pos + dir, up)));
		else
			m_LocalRotation = glm::quat_cast(glm::lookAt(pos, pos + dir, up));

		m_LocalRotation = glm::normalize(glm::inverse(m_LocalRotation));
		NeedsUpdate();
	}

	void Transform::Scale(const glm::vec3& scaler)
	{
		m_LocalScale *= scaler;
		NeedsUpdate();
	}

	void Transform::Scale(const float& scaler)
	{
		m_LocalScale *= scaler;
		NeedsUpdate();
	}

	glm::vec3 Transform::Up()
	{
		return glm::normalize(GetWorldRotation() * glm::vec3(0.f, 1.f, 0.f));
	}

	glm::vec3 Transform::Right()
	{
		return glm::normalize(GetWorldRotation() * glm::vec3(1.f, 0.f, 0.f));
	}

	glm::vec3 Transform::Forward()
	{
		return glm::normalize(GetWorldRotation() * glm::vec3(0.f, 0.f, 1.f));
	}

	glm::vec3 Transform::ConvertWorldToLocalPosition(const glm::vec3& position)
	{
		if (m_NeedsParentUpdate)
			UpdateFromParent();
		return glm::inverse(GetWorldTransform()) * glm::vec4(position, 1.f);
	}

	glm::vec3 Transform::ConvertWorldToLocalDirection(const glm::vec3& direction)
	{
		if (m_NeedsParentUpdate)
			UpdateFromParent();
		return glm::inverse(GetWorldTransform()) * glm::vec4(direction, 0.f);
	}

	glm::quat Transform::ConvertWorldToLocalRotation(const glm::quat& rotation)
	{
		if (m_NeedsParentUpdate)
			UpdateFromParent();
		return glm::inverse(m_WorldRotation) * rotation;
	}

	glm::vec3 Transform::ConvertLocalToWorldPosition(const glm::vec3& position)
	{
		if (m_NeedsParentUpdate)
			UpdateFromParent();
		return GetWorldTransform() * glm::vec4(position, 1.f);
	}

	glm::vec3 Transform::ConvertLocalToWorldDirection(const glm::vec3& direction)
	{
		if (m_NeedsParentUpdate)
			UpdateFromParent();
		return GetWorldTransform() * glm::vec4(direction, 0.f);
	}

	glm::quat Transform::ConvertLocalToWorldRotation(const glm::quat& rotation)
	{
		if (m_NeedsParentUpdate)
			UpdateFromParent();
		return m_WorldRotation * rotation;
	}

	Transform* Transform::GetParent()
	{
		if (m_Parent == m_Root)
			return nullptr;
		return m_Parent;
	}

	std::vector<Transform*> Transform::GetParents()
	{
		std::vector<Transform*> parents;

		Transform* currentParent = m_Parent;
		while (currentParent != nullptr)
		{
			parents.push_back(currentParent);
			currentParent = currentParent->GetParent();
		}

		return parents;
	}

	void Transform::SetParent(Transform* parent, const bool& keepWorldTransform)
	{
		Transform* newParent = parent == nullptr ? m_Root : parent;

		if (m_Parent)
			m_Parent->RemoveChild(this);

		if (keepWorldTransform)
		{
			glm::mat4 worldTransform = GetWorldTransform();
			newParent->AddChild(this);
			SetWorldTransform(worldTransform);
		}
		else
			newParent->AddChild(this);

		m_ParentNotified = false;
		NeedsUpdate();
	}

	void Transform::AddChild(Transform* child)
	{
		m_Children.push_back(child);
		child->m_Parent = this;
	}

	void Transform::RemoveChild(Transform* child)
	{
		if (child)
		{
			auto found = std::find(m_Children.begin(), m_Children.end(), child);
			if (found != m_Children.end())
			{
				CancelUpdate(child);
				std::swap(*found, m_Children.back());
				m_Children.pop_back();
				child->m_Parent = nullptr;
			}
		}
	}

	bool Transform::HasChildren() const
	{
		return m_Children.size() > 0;
	}

	bool Transform::HasParent() const
	{
		if (m_Parent == m_Root)
			return false;
		return m_Parent != nullptr;
	}

	bool Transform::IsChildOf(Transform* parent, const bool& recursive)
	{
		if (m_Parent == nullptr || this == parent)
			return false;

		if (m_Parent == parent)
			return true;
		else
		{
			if (recursive)
				return m_Parent->IsChildOf(parent, recursive);
			return false;
		}
	}

	std::vector<Transform*> Transform::GetChildren(const bool& recursive)
	{
		if (recursive)
		{
			std::vector<Transform*> children;
			children.insert(children.end(), m_Children.begin(), m_Children.end());

			for (auto const& it : m_Children)
			{
				std::vector<Transform*> childChildren = it->GetChildren(true);
				children.insert(children.end(), childChildren.begin(), childChildren.end());
			}

			return children;
		}
		else
			return m_Children;
	}

	void Transform::DecomposeLocalTransform()
	{
		m_LocalPosition = glm::vec3(m_LocalTransform[3][0], m_LocalTransform[3][1], m_LocalTransform[3][2]);

		m_LocalScale.x = glm::sqrt(glm::pow(m_LocalTransform[0][0], 2.f) + glm::pow(m_LocalTransform[0][1], 2.f) + glm::pow(m_LocalTransform[0][2], 2.f));
		m_LocalScale.y = glm::sqrt(glm::pow(m_LocalTransform[1][0], 2.f) + glm::pow(m_LocalTransform[1][1], 2.f) + glm::pow(m_LocalTransform[1][2], 2.f));
		m_LocalScale.z = glm::sqrt(glm::pow(m_LocalTransform[2][0], 2.f) + glm::pow(m_LocalTransform[2][1], 2.f) + glm::pow(m_LocalTransform[2][2], 2.f));

		glm::mat3 rotationMatrix(m_LocalTransform);

		if (m_LocalScale.x != 0.f)
			rotationMatrix[0] /= m_LocalScale.x;
		if (m_LocalScale.y != 0.f)
			rotationMatrix[1] /= m_LocalScale.y;
		if (m_LocalScale.z != 0.f)
			rotationMatrix[2] /= m_LocalScale.z;

		m_LocalRotation = glm::quat_cast(rotationMatrix);
	}

	void Transform::Update(const bool& parentHasChanged)
	{
		m_ParentNotified = false;

		if (m_NeedsParentUpdate || parentHasChanged)
			UpdateFromParent();

		if (m_NeedsChildUpdate || parentHasChanged)
		{
			for (const auto& child : m_Children)
				child->Update(true);
		}
		else
		{
			for (const auto& child : m_ChildrenToUpdate)
				child->Update(false);
		}

		m_ChildrenToUpdate.clear();
		m_NeedsChildUpdate = false;
	}

	bool Transform::HasMoved() const
	{
		return m_LastFrameMoved == SimulationTime::GetInstance().GetFrameCount();
	}

	void Transform::UpdateFromParent()
	{
		m_NeedsParentUpdate = false;
		m_CachedWorldTransformOutOfDate = true;

		if (m_Parent)
		{
			const glm::mat4& worldTransform = GetWorldTransform();
			m_WorldPosition = glm::vec3(worldTransform[3][0], worldTransform[3][1], worldTransform[3][2]);

			m_WorldScale.x = glm::sqrt(glm::pow(worldTransform[0][0], 2.f) + glm::pow(worldTransform[0][1], 2.f) + glm::pow(worldTransform[0][2], 2.f));
			m_WorldScale.y = glm::sqrt(glm::pow(worldTransform[1][0], 2.f) + glm::pow(worldTransform[1][1], 2.f) + glm::pow(worldTransform[1][2], 2.f));
			m_WorldScale.z = glm::sqrt(glm::pow(worldTransform[2][0], 2.f) + glm::pow(worldTransform[2][1], 2.f) + glm::pow(worldTransform[2][2], 2.f));

			glm::mat3 rotationMatrix(worldTransform);

			if (m_WorldScale.x != 0.f)
				rotationMatrix[0] /= m_WorldScale.x;

			if (m_WorldScale.y != 0.f)
				rotationMatrix[1] /= m_WorldScale.y;

			if (m_WorldScale.z != 0.f)
				rotationMatrix[2] /= m_WorldScale.z;

			m_WorldRotation = glm::quat_cast(rotationMatrix);
		}
		else
		{
			m_WorldRotation = m_LocalRotation;
			m_WorldPosition = m_LocalPosition;
			m_WorldScale = m_LocalScale;
		}

		m_LastFrameMoved = SimulationTime::GetInstance().GetFrameCount();
	}

	void Transform::CancelUpdate(Transform* child)
	{
		m_ChildrenToUpdate.erase(child);
		if (m_ChildrenToUpdate.empty() && m_Parent && !m_NeedsChildUpdate)
		{
			m_Parent->CancelUpdate(this);
			m_ParentNotified = false;
		}
	}

	void Transform::NeedsUpdate()
	{
		m_NeedsChildUpdate = true;
		m_NeedsParentUpdate = true;
		m_CachedWorldTransformOutOfDate = true;
		m_CachedLocalTransformOutOfDate = true;

		if (m_Parent && !m_ParentNotified)
		{
			m_Parent->RequestUpdate(this);
			m_ParentNotified = true;
		}

		m_ChildrenToUpdate.clear();
	}

	void Transform::RequestUpdate(Transform* child)
	{
		if (m_NeedsChildUpdate)
			return;

		m_ChildrenToUpdate.insert(child);
		if (m_Parent && !m_ParentNotified)
		{
			m_Parent->RequestUpdate(this);
			m_ParentNotified = true;
		}
	}
}
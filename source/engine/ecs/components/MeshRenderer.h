#pragma once
#include "math/OBB.h"
#include "ecs/Component.h"
#include "ecs/components/Transform.h"
#include "graphics/resources/mesh/GfxModel.h"
#include "graphics/ui/utilities/EditorEnums.h"

REGISTER_COMPONENT(MeshRenderer);

namespace Pacha
{
	EDITOR_FLAGS(Render,
		RenderFlags_VISIBLE						= 1 << 0,
		RenderFlags_CAST_LOCAL_SHADOWS			= 1 << 1,
		RenderFlags_CAST_DIRECTIONAL_SHADOWS	= 1 << 2,
		RenderFlags_RECEIVE_SHADOWS				= 1 << 3,

		//PACHA_EDITOR
		RenderFlags_SELECTED					= 1 << 30,
		RenderFlags_HOVERED						= 1 << 31,
		//~PACHA_EDITOR

		RenderFlags_CAST_SHADOWS				= RenderFlags_CAST_LOCAL_SHADOWS | RenderFlags_CAST_DIRECTIONAL_SHADOWS
	);

	class MeshRenderer : public Component
	{
		COMPONENT_TYPE_INFO;

	public:
		GfxMesh* GetMesh(const size_t index);
		void SetMesh(GfxMesh* mesh, const size_t index);

		GfxMaterial* GetMaterial(const size_t index);
		void SetMaterial(GfxMaterial* material, const size_t index);

		const std::vector<GfxMesh*>& GetMeshes() const;
		const std::vector<GfxMaterial*>& GetMaterials() const;

		void SetVisible(bool isVisible);
		bool IsVisible() const;

		void SetRenderFlags(const RenderFlags& mask);
		RenderFlags GetRenderFlags() const;

		const OBB& GetOBB() const;
		const AABB& GetLocalAABB() const;
		void UpdateOBB(Transform& transform);

		bool IsValid();

#if PACHA_EDITOR
		void SetSelected(bool selected);
		void SetHovered(bool hovered);
#endif

	private:
		OBB m_OBB;
		AABB m_LocalAABB;
		std::vector<GfxMesh*> m_Meshes;
		std::vector<GfxMaterial*> m_Materials;

		bool m_OBBIsDirty = true;
		RenderFlags m_RenderFlags = ~0u;

		void RecalculateBoundingBox();

		friend class Serialization::Accessor;
		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& /*serializer*/)
		{
			//serializer
			//(
			//	Serialization::MakeNVP("Fov", m_FOV),
			//	Serialization::MakeNVP("NearFarPlane", m_NearFarDistance),
			//	Serialization::MakeNVP("ClearColor", m_ClearColor),
			//	Serialization::MakeNVP("ProjectionType", m_ProjectionType),
			//	Serialization::MakeNVP("RenderOrder", m_RenderOrder),
			//	Serialization::MakeNVP("ViewportSetup", m_ViewportSetup),
			//	Serialization::MakeNVP("Viewport", m_Viewport)
			//);
		}
	};
}
#pragma once
#include "ecs/Component.h"

#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>

REGISTER_COMPONENT(Transform);

namespace Pacha
{
	class Transform : public Component
	{
		COMPONENT_TYPE_INFO;
	
	public:
		enum class Space
		{
			LOCAL,
			PARENT,
			WORLD
		};

		void OnCreate() override;
		void OnDestroy() override;

		// Setters
		void SetLocalPosition(const glm::vec3& newPosition);
		void SetWorldPosition(const glm::vec3& newPosition);

		void SetLocalRotation(const glm::quat& newRotation);
		void SetWorldRotation(const glm::quat& newRotation);

		void SetLocalScale(const glm::vec3& newScale);
		void SetWorldScale(const glm::vec3& newScale);

		void SetLocalTransform(const glm::mat4& transform);
		void SetWorldTransform(const glm::mat4& transform);

		// Getters
		const glm::vec3& GetLocalPosition();
		const glm::vec3& GetWorldPosition();

		const glm::quat& GetLocalRotation();
		const glm::quat& GetWorldRotation();

		const glm::vec3& GetLocalScale();
		const glm::vec3& GetWorldScale();

		const glm::mat4& GetLocalTransform();
		const glm::mat4& GetWorldTransform();

		//Position - functions
		void Translate(const glm::vec3& translation, const Space& space = Space::LOCAL);

		//Rotation - functions
		void Rotate(const glm::quat& rotation, const Space& space = Space::LOCAL);
		void Rotate(const glm::vec3& rotation, const Space& space = Space::LOCAL);
		void Rotate(const float& angle, const glm::vec3& axis, const Space& space = Space::LOCAL);
		void LookAt(const glm::vec3& at, const glm::vec3& up = glm::vec3(0.f, 1.f, 0.f));

		//Scaling - functions
		void Scale(const glm::vec3& scaler);
		void Scale(const float& scaler);

		//Direction - function
		glm::vec3 Up();
		glm::vec3 Right();
		glm::vec3 Forward();

		//Conversion Functions
		glm::vec3 ConvertWorldToLocalPosition(const glm::vec3& position);
		glm::vec3 ConvertWorldToLocalDirection(const glm::vec3& direction);
		glm::quat ConvertWorldToLocalRotation(const glm::quat& rotation);

		glm::vec3 ConvertLocalToWorldPosition(const glm::vec3& position);
		glm::vec3 ConvertLocalToWorldDirection(const glm::vec3& direction);
		glm::quat ConvertLocalToWorldRotation(const glm::quat& rotation);

		// Scene Graph
		Transform* GetParent();
		std::vector<Transform*> GetParents();

		void SetParent(Transform* newParent, const bool& keepWorldTransform = false);
		bool HasParent() const;

		bool HasChildren() const;
		bool IsChildOf(Transform* parent, const bool& recursive = false);
		std::vector<Transform*> GetChildren(const bool& recursive = false);

		void Update(const bool& parentHasChanged = false);
		bool HasMoved() const;

	private:
		Transform* m_Root = nullptr;
		Transform* m_Parent = nullptr;
		uint64_t m_LastFrameMoved = 0;
		std::vector<Transform*> m_Children;
		std::unordered_set<Transform*> m_ChildrenToUpdate;

		glm::mat4 m_LocalTransform = glm::mat4(1.f);
		glm::mat4 m_WorldTransform = glm::mat4(1.f);

		glm::vec3 m_LocalScale = glm::vec3(1.f);
		glm::quat m_LocalRotation = glm::quat(1.f, 0.f, 0.f, 0.f);
		glm::vec3 m_LocalPosition = glm::vec3(0.f);

		glm::vec3 m_WorldScale = glm::vec3(1.f);
		glm::quat m_WorldRotation = glm::quat(1.f, 0.f, 0.f, 0.f);
		glm::vec3 m_WorldPosition = glm::vec3(0.f);

#if PACHA_EDITOR
		glm::vec3 m_EditorDegreesLocalRotation = glm::vec3(0.f);
#endif

		bool m_ParentNotified = false;
		bool m_NeedsChildUpdate = true;
		bool m_NeedsParentUpdate = true;
		bool m_CachedLocalTransformOutOfDate = true;
		bool m_CachedWorldTransformOutOfDate = true;

		void UpdateFromParent();
		void CancelUpdate(Transform* child);

		void NeedsUpdate();
		void RequestUpdate(Transform* child);

		void AddChild(Transform* child);
		void RemoveChild(Transform* child);

		void DecomposeLocalTransform();

		friend class Level;
		static Transform* CreateAsRoot()
		{
			Transform* transform = new Transform();
			transform->m_Root = transform;
			return transform;
		}
		
		friend class Serialization::Accessor;
		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& serializer)
		{
			serializer
			(
				Serialization::MakeNVP("Position", m_LocalPosition),
				Serialization::MakeNVP("Rotation", m_LocalRotation),
				Serialization::MakeNVP("Scale", m_LocalScale)
			);

#if PACHA_EDITOR
			if constexpr (!IsSaving)
				m_EditorDegreesLocalRotation = glm::degrees(glm::eulerAngles(m_LocalRotation));
#endif
		}

#if PACHA_EDITOR
	private:
		void DrawInspectorUI(InspectorBuilder& builder) override
		{
			static float kLabelAlignment = builder.GetLabelWidth("Position");
			if (builder.NonRemovableComponentHeader("Transform"))
			{
				bool needsUpdate = false;

				builder.PushLabelAlignment(kLabelAlignment);
				needsUpdate |= builder.PropertyField("Position", m_LocalPosition);
				needsUpdate |= builder.PropertyField("Rotation", m_EditorDegreesLocalRotation);
				needsUpdate |= builder.PropertyField("Scale", m_LocalScale);
				builder.PopLabelAlignment();

				if (needsUpdate)
				{
					m_LocalRotation = glm::quat(glm::radians(m_EditorDegreesLocalRotation));
					NeedsUpdate();
				}
			}
		};
#endif
	};
}
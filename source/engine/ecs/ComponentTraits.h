#pragma once

namespace Pacha
{
	template<typename T>
	struct IsComponent : std::false_type {};

	template<typename T>
	inline constexpr bool IsComponentV = IsComponent<T>::value;

	template<typename T>
	struct IsScript : std::false_type {};

	template<typename T>
	inline constexpr bool IsScriptV = IsScript<T>::value;
}
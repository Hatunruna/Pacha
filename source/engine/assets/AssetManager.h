#pragma once
#include "ModelAsset.h"
#include "TextureAsset.h"
#include "TechniqueAsset.h"
#include "utilities/PachaUUID.h"

#include <unordered_map>

namespace Pacha
{
	class AssetManager
	{
		friend class Engine;
		friend class Editor;
	
	private:
		AssetManager() = default;
		AssetManager(const AssetManager&) = delete;
		AssetManager& operator=(const AssetManager&) = delete;

	public:
		static AssetManager& GetInstance()
		{
			static AssetManager sInstance;
			return sInstance;
		}

		bool Initialize();
		void Release();

		template<typename T, typename = std::enable_if_t<std::is_base_of_v<AssetBase, T>>>
		bool Contains(const PachaUUID& id)
		{
			auto& assetMap = GetAssetsMap<T>();
			return assetMap.count(id);
		}

		template<typename T, typename = std::enable_if_t<std::is_base_of_v<AssetBase, T>>>
		bool Contains(const std::filesystem::path& path)
		{
			const auto& id = m_PathToId.find(path);
			if (id != m_PathToId.end())
				return Contains<T>(id->second);
			return false;
		}

		template<typename T, typename = std::enable_if_t<std::is_base_of_v<AssetBase, T>>>
		T* Get(const PachaUUID& id)
		{
			auto& assetMap = GetAssetsMap<T>();
			auto asset = assetMap.find(id);
			if (asset != assetMap.end())
				return asset->second;

			return nullptr;
		}

		template<typename T, typename = std::enable_if_t<std::is_base_of_v<AssetBase, T>>>
		T* Get(const std::filesystem::path& path)
		{
			const auto& id = m_PathToId.find(path);

			if (id != m_PathToId.end())
				return Get<T>(id->second);

			return nullptr;
		}

		template<typename T, typename = std::enable_if_t<std::is_base_of_v<AssetBase, T>>>
		T* GetAndLoad(const PachaUUID& id)
		{
			if (T* asset = Get<T>(id))
			{
				if (asset->Load())
					return asset;
			}

			return nullptr;
		}

		template<typename T, typename = std::enable_if_t<std::is_base_of_v<AssetBase, T>>>
		T* GetAndLoad(const std::filesystem::path& path)
		{
			if (T* asset = Get<T>(path))
			{
				if (asset->Load())
					return asset;
			}

			return nullptr;
		}

#if PACHA_EDITOR
		template<typename T, typename = std::enable_if_t<std::is_base_of_v<AssetBase, T>>>
		T* Create(const std::filesystem::path& path)
		{
			if (Contains<T>(path))
				return Get<T>(path);

			auto& assetMap = GetAssetsMap<T>();

			T* asset = new T(path);
			const PachaUUID& uuid = asset->GetUUID();
			assetMap[uuid] = asset;

			m_PathToId[path] = uuid;
			m_IsDirty = true;
			return asset;
		}

		template<typename T, typename = std::enable_if_t<std::is_base_of_v<AssetBase, T>>>
		T* GetOrCreate(const std::filesystem::path& path)
		{
			if (Contains<T>(path))
				return Get<T>(path);
			else
				return Create<T>(path);
		}

		bool IsDirty();
#endif

	private:
		std::unordered_map<std::filesystem::path, PachaUUID> m_PathToId;
		
		template<typename T, typename = std::enable_if_t<std::is_base_of_v<AssetBase, T>>>
		std::unordered_map<PachaUUID, T*>& GetAssetsMap()
		{
			static std::unordered_map<PachaUUID, T*> sAssetMap;
			return sAssetMap;
		}

		template<typename T, typename = std::enable_if_t<std::is_base_of_v<AssetBase, T>>>
		void UnloadAssetsMap()
		{
			for (auto& [id, asset] : GetAssetsMap<T>())
			{
				if (asset->IsLoaded())
					asset->Unload();
				delete asset;
			}
		}

		template<typename T, typename = std::enable_if_t<std::is_base_of_v<AssetBase, T>>>
		void InitializePathToId()
		{
			for (auto& [id, asset] : GetAssetsMap<T>())
				m_PathToId[asset->GetPath()] = asset->GetUUID();
		}

		bool LoadDatabase();

#if PACHA_EDITOR
		bool m_IsDirty = false;
		void SaveDatabase();
#endif
	};
}
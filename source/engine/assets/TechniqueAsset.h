#pragma once
#include "AssetBase.h"
#include "graphics/resources/technique/GfxTechnique.h"

namespace Pacha
{
	class TechniqueAsset : public AssetBase
	{
	public:
		using AssetBase::AssetBase;
		~TechniqueAsset();

		GfxTechnique* GetGfxTechnique() const { return m_Technique; }

	private:
		bool LoadImpl() override;
		void UnloadImpl() override;

		GfxTechnique* m_Technique = nullptr;
#if PACHA_EDITOR
		std::vector<std::string> m_Sources = {};
#endif

		friend class Serialization::Accessor;
		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& serializer)
		{
			SerializeBaseClass<IsSaving, AssetBase>(serializer, this);
		}
	};
}
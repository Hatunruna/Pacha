#pragma once
#include "AssetBase.h"
#include "graphics/resources/mesh/GfxModel.h"

namespace Pacha
{
	class ModelAsset : public AssetBase
	{
	public:
		using AssetBase::AssetBase;
		~ModelAsset();

		GfxModel* GetGfxModel() const { return m_Model; }

	private:
		bool LoadImpl() override;
		void UnloadImpl() override;

		GfxModel* m_Model = nullptr;
		
		friend class Serialization::Accessor;
		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& serializer)
		{
			SerializeBaseClass<IsSaving, AssetBase>(serializer, this);
		}
	};
}
#include "ModelAsset.h"
#include "fileio/File.h"
#include "core/SystemInfo.h"
#include "core/ProjectConstants.h"
#include "graphics/device/GfxDevice.h"
#include "serialization/JsonDeserializer.h"
#include "graphics/utilities/GfxBindlessResourceManager.h"

namespace Pacha
{
	ModelAsset::~ModelAsset()
	{
		if (m_Model)
			delete m_Model;
	}

	bool ModelAsset::LoadImpl()
	{
		std::shared_ptr<uint8_t> modelFile = nullptr;

#if PACHA_EDITOR
		const std::filesystem::path libraryPath = ProjectConstants::GetInstance().GetLibraryPath();
		const std::filesystem::path modelLibraryPath = libraryPath / (m_UUID.GetString() + ".pmdl");

		if (File::Exists(modelLibraryPath))
		{
			File modelDataFile(modelLibraryPath, OpenMode::BINARY);
			if (modelDataFile.OpenFile())
				modelFile = modelDataFile.GetBinaryData();
			else
				return false;
		}
		else
			return false;
#else
		LOGCRITICAL("Not Implemented");
#endif

		size_t jsonSize = 0;
		std::string modelDefinition = {};

		memcpy(&jsonSize, modelFile.get(), sizeof(size_t));
		modelDefinition.resize(jsonSize);
		memcpy(modelDefinition.data(), modelFile.get() + sizeof(size_t), jsonSize);

		GfxModelData modelData = {};
		JsonDeserializer deserializer(modelDefinition);
		deserializer(modelData);

		const size_t dataOffset = sizeof(size_t) + jsonSize;
		std::shared_ptr<uint8_t> vertexData = std::shared_ptr<uint8_t>(modelFile, modelFile.get() + dataOffset);

		GfxVertexBuffer* vertexBuffer = new GfxVertexBuffer(modelData.vertexDataSize, vertexData);
		vertexBuffer->SetName(m_Path.filename().string() + "_VB");

		GfxIndexBufferBase* indexBuffer = nullptr;
		if (modelData.indexDataSize)
		{
			std::shared_ptr<uint8_t> indexData = std::shared_ptr<uint8_t>(modelFile, modelFile.get() + dataOffset + modelData.vertexDataSize);
			if (modelData.indexType == GfxIndexType::U16)
				indexBuffer = new GfxIndexBuffer<GfxIndexType::U16>(modelData.indexDataSize, indexData);
			else
				indexBuffer = new GfxIndexBuffer<GfxIndexType::U32>(modelData.indexDataSize, indexData);
			indexBuffer->SetName(m_Path.filename().string() + "_IB");
		}

		m_Model = new GfxModel(vertexBuffer, indexBuffer, modelData);
		m_Model->SetName(m_Path.filename().string());
		return true;
	}

	void ModelAsset::UnloadImpl()
	{
		if (m_Model)
			delete m_Model;
		m_Model = nullptr;
	}
}
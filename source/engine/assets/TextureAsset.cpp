#include "TextureAsset.h"
#include "fileio/File.h"
#include "core/SystemInfo.h"
#include "core/ProjectConstants.h"
#include "assets/utilities/Ktx2Reader.h"

namespace Pacha
{
	TextureAsset::~TextureAsset()
	{
		if (m_Texture)
			delete m_Texture;
	}

	bool TextureAsset::LoadImpl()
	{
		size_t ktx2DataSize = 0;
		std::shared_ptr<uint8_t> ktx2Data = nullptr;

#if PACHA_EDITOR
			const std::filesystem::path libraryPath = ProjectConstants::GetInstance().GetLibraryPath();
			const std::filesystem::path assetLibraryPath = libraryPath / (m_UUID.GetString() + ".ktx2");

			File ktx2File(assetLibraryPath);
			if (!ktx2File.OpenFile())
			{
				LOGERROR("Failed to load texture at path: " << assetLibraryPath);
				return false;
			}

			ktx2Data = ktx2File.GetBinaryData();
			ktx2DataSize = ktx2File.GetFileSize();
#else
			LOGCRITICAL("Not Implemented");
#endif

		Ktx2Reader ktx2Reader;
		if (!ktx2Reader.Read(ktx2Data.get(), ktx2DataSize))
		{
			LOGERROR("Failed to read ktx2 file");
			return false;
		}

		const GfxTextureDescriptor& descriptor = ktx2Reader.GetDescriptor();
		const std::shared_ptr<uint8_t>& data = ktx2Reader.GetData();

		m_Texture = new GfxTexture(descriptor, data);
		m_Texture->SetName(m_Path.filename().string());
		return true;
	}

	void TextureAsset::UnloadImpl()
	{
		if (m_Texture)
			delete m_Texture;
		m_Texture = nullptr;
	}
}
#include "AssetBase.h"
#include "core/SystemInfo.h"
#include "core/ProjectConstants.h"

#include <fstream>

namespace Pacha
{
	AssetBase::AssetBase(const std::filesystem::path& path) :
		m_Path(path)
	{
#if PACHA_EDITOR
		m_UUID.Initialize();
		CreateAssetUUIDFile();
#endif
	}
	
	AssetBase::~AssetBase()
	{ }

	bool AssetBase::Load()
	{
		if (IsLoaded())
			return true;
		m_Loaded = LoadImpl();
		return m_Loaded;
	}

	void AssetBase::Unload()
	{
		if (IsLoaded())
			UnloadImpl();
	}
	
#if PACHA_EDITOR
	void AssetBase::CreateAssetUUIDFile()
	{
		std::filesystem::path uuidPath = ProjectConstants::GetInstance().GetAssetsPath() / m_Path;
		uuidPath.concat(".uuid");

		std::ofstream uuid(uuidPath, std::ios::binary);
		uuid.write(reinterpret_cast<char*>(&m_UUID), sizeof(PachaUUID));
		uuid.close();
	}
#endif
}
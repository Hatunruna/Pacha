#pragma once
#include "utilities/PachaUUID.h"
#include "serialization/Serialization.h"

#include <filesystem>

namespace Pacha
{
	class AssetBase : public SerializableObject
	{
		friend class AssetManager;

	public:
		AssetBase() = default;
		AssetBase(const std::filesystem::path& path);
		virtual ~AssetBase();

		bool Load();
		void Unload();

		bool IsLoaded() { return m_Loaded; }

		const PachaUUID& GetUUID() { return m_UUID; }
		void SetUUID(const PachaUUID& uuid) { m_UUID = uuid; }

		const std::filesystem::path& GetPath() { return m_Path; };
		void SetPath(const std::filesystem::path& path) { m_Path = path; };

	protected:
		PachaUUID m_UUID;
		bool m_Loaded = false;
		std::filesystem::path m_Path;

		virtual bool LoadImpl() = 0;
		virtual void UnloadImpl() = 0;

#if PACHA_EDITOR
		void CreateAssetUUIDFile();
#endif

	private:
		friend class Serialization::Accessor;
		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& serializer)
		{
 			serializer
			(
				MakeNVP("GUID", m_UUID),
				MakeNVP("Path", m_Path)
			);
		}
	};
}
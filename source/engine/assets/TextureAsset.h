#pragma once
#include "AssetBase.h"
#include "graphics/resources/texture/GfxTexture.h"

namespace Pacha
{
	class TextureAsset : public AssetBase
	{
	public:
		using AssetBase::AssetBase;
		~TextureAsset();

		GfxTexture* const& GetGfxTexture() const { return m_Texture; }

	private:
		bool LoadImpl() override;
		void UnloadImpl() override;

		GfxTexture* m_Texture = nullptr;

		friend class Serialization::Accessor;
		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& serializer)
		{
			SerializeBaseClass<IsSaving, AssetBase>(serializer, this);
		}
	};
}
#include "AssetManager.h"
#include "fileio/File.h"
#include "core/SystemInfo.h"
#include "core/ProjectConstants.h"
#include "serialization/JsonDeserializer.h"

#if PACHA_EDITOR
#include "serialization/JsonSerializer.h"
#endif

namespace Pacha
{
	bool AssetManager::Initialize()
	{
		return LoadDatabase();
	}

	void AssetManager::Release()
	{
#if PACHA_EDITOR
		if (m_IsDirty)
			SaveDatabase();
#endif
		UnloadAssetsMap<ModelAsset>();
		UnloadAssetsMap<TextureAsset>();
		UnloadAssetsMap<TechniqueAsset>();
	}

#if PACHA_EDITOR
	bool AssetManager::IsDirty()
	{
		return m_IsDirty;
	}

	void AssetManager::SaveDatabase()
	{
		JsonSerializer serializer;
		serializer.StartSerialization();
		serializer(Serialization::MakeNVP("ModelAssets", GetAssetsMap<ModelAsset>()));
		serializer(Serialization::MakeNVP("TextureAssets", GetAssetsMap<TextureAsset>()));
		serializer(Serialization::MakeNVP("TechniqueAssets", GetAssetsMap<TechniqueAsset>()));
		serializer.EndSerialization();

		serializer.SaveToFile(ProjectConstants::GetInstance().GetLibraryPath() / "AssetDB.json");
		m_IsDirty = false;
	}
#endif

	bool AssetManager::LoadDatabase()
	{
		std::string databaseString = {};

#if PACHA_EDITOR
		const std::filesystem::path dbPath = ProjectConstants::GetInstance().GetLibraryPath() / "AssetDB.json";

		if (File::Exists(dbPath))
		{
			File database(dbPath, OpenMode::TEXT);
			if (database.OpenFile())
				databaseString = database.GetTextData();
			else
			{
				LOGCRITICAL("Failed to load Asset Database");
				return false;
			}
		}
#else
		LOGCRITICAL("Not Implemented");
#endif

		if (!databaseString.empty())
		{
			JsonDeserializer deserializer(databaseString);
			deserializer(Serialization::MakeNVP("ModelAssets", GetAssetsMap<ModelAsset>()));
			deserializer(Serialization::MakeNVP("TextureAssets", GetAssetsMap<TextureAsset>()));
			deserializer(Serialization::MakeNVP("TechniqueAssets", GetAssetsMap<TechniqueAsset>()));

			InitializePathToId<ModelAsset>();
			InitializePathToId<TextureAsset>();
			InitializePathToId<TechniqueAsset>();
		}

		return true;
	}
}

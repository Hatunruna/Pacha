#pragma once
#include "graphics/resources/texture/GfxTexture.h"

#include <filesystem>

namespace Pacha
{
	class Ktx2Writer
	{
	public:
		Ktx2Writer(const std::filesystem::path& path);
		bool Write(const GfxTextureDescriptor& descriptor, uint8_t* textureData);
		bool IsCompressibleFormat(const GfxPixelFormat format);
	
	private:
		std::filesystem::path m_Path = {};

		bool BasisuCompress(const GfxTextureDescriptor& descriptor, uint8_t* textureData);
	};
}
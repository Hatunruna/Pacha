#include "Ktx2Reader.h"
#include "fileio/File.h"
#include "assets/utilities/Ktx2FormatHelpers.h"

#include <fstream>

namespace Pacha
{
	bool Ktx2Reader::Read(const std::filesystem::path& path)
	{
		File ktx2File(path, OpenMode::BINARY);
		if (!ktx2File.OpenFile())
		{
			LOGERROR("Failed to open ktx2 file " << path);
			return false;
		}

		std::shared_ptr<uint8_t> ktx2Data = ktx2File.GetBinaryData();
		return Read(ktx2Data.get(), ktx2File.GetFileSize());
	}

	bool Ktx2Reader::Read(uint8_t* ktx2File, const size_t size)
	{
		if (NeedsTranscoding(ktx2File))
			return BasisuTranscode(ktx2File, size);

		basist::ktx2_header* header = reinterpret_cast<basist::ktx2_header*>(ktx2File);

		m_Descriptor.mips = header->m_level_count;
		m_Descriptor.width = header->m_pixel_width;
		m_Descriptor.height = header->m_pixel_height == 0 ? 1 : static_cast<uint32_t>(header->m_pixel_height);
		m_Descriptor.depth = header->m_pixel_depth == 0 ? 1 : static_cast<uint32_t>(header->m_pixel_depth);
		m_Descriptor.slices = header->m_layer_count == 0 ? 1 : static_cast<uint32_t>(header->m_layer_count * header->m_face_count);
		m_Descriptor.format = GetPixelFormatFromVkFormat(static_cast<VkFormat>(header->m_vk_format.operator unsigned int()));
		
		if (header->m_face_count > 1)
		{
			if (header->m_layer_count > 0)
				m_Descriptor.type = GfxTextureType::CUBEMAP_ARRAY;
			else
				m_Descriptor.type = GfxTextureType::CUBEMAP;
		}
		else
		{
			if (header->m_layer_count > 0)
			{
				if(header->m_pixel_height > 0)
					m_Descriptor.type = GfxTextureType::TEXTURE_ARRAY_2D;
				else
					m_Descriptor.type = GfxTextureType::TEXTURE_ARRAY_1D;
			}
			else
			{
				if (header->m_pixel_depth > 0)
					m_Descriptor.type = GfxTextureType::TEXTURE_3D;
				else if (header->m_pixel_height > 0)
					m_Descriptor.type = GfxTextureType::TEXTURE_2D;
				else
					m_Descriptor.type = GfxTextureType::TEXTURE_1D;
			}
		}

		std::vector<GfxMipInfo> mipsInfo = {};
		size_t textureSize = GfxTexture::CalculateTextureDataSizeAndMipsInfo(m_Descriptor, mipsInfo);
		m_Data = std::shared_ptr<uint8_t>(new uint8_t[textureSize]);

		basist::ktx2_level_index* levelIndices = reinterpret_cast<basist::ktx2_level_index*>(ktx2File + sizeof(basist::ktx2_header));

		for (int32_t slice = static_cast<int32_t>(m_Descriptor.slices) - 1; slice >= 0; --slice)
		{
			for (int32_t mip = static_cast<int32_t>(m_Descriptor.mips) - 1; mip >= 0; --mip)
			{
				const uint32_t index = (slice * m_Descriptor.mips) + mip;
				basist::ktx2_level_index& levelIndex = levelIndices[index];

				uint8_t* data = m_Data.get() + mipsInfo[index].offset;
				memcpy(data, ktx2File + levelIndex.m_byte_offset, levelIndex.m_byte_length);
			}
		}

		return true;
	}

	const GfxTextureDescriptor& Ktx2Reader::GetDescriptor()
	{
		return m_Descriptor;
	}

	const std::shared_ptr<uint8_t>& Ktx2Reader::GetData()
	{
		return m_Data;
	}

	bool Ktx2Reader::NeedsTranscoding(uint8_t* ktx2File)
	{
		basist::ktx2_header* header = reinterpret_cast<basist::ktx2_header*>(ktx2File);
		const uint8_t* dfd = ktx2File + header->m_dfd_byte_offset;
		const uint32_t dfd_bits = basisu::read_le_dword(dfd + 3 * sizeof(uint32_t));
		const uint32_t colorModel = dfd_bits & 255;

		if (colorModel == basist::KTX2_KDF_DF_MODEL_ETC1S || colorModel == basist::KTX2_KDF_DF_MODEL_UASTC)
			return true;
		return false;
	}

	bool Ktx2Reader::BasisuTranscode(uint8_t* ktx2File, const size_t size)
	{
		basist::basisu_transcoder_init();
		basist::ktx2_header* header = reinterpret_cast<basist::ktx2_header*>(ktx2File);
		GfxPixelFormat pixelFormat = GetTranscodedPixelFormatFromVKFormat(static_cast<VkFormat>(header->m_vk_format.operator unsigned int()));
		const basist::transcoder_texture_format transcodeFormat = GetBasisTranscodeFormat(pixelFormat);

		header->m_vk_format = 0;
		basist::ktx2_transcoder basisDecoder;
		if (!basisDecoder.init(ktx2File, static_cast<uint32_t>(size)))
		{
			LOGERROR("Failed to initialize basis decoder");
			return false;
		}

		if (!basisDecoder.start_transcoding())
		{
			LOGERROR("Failed to start transcoding basis file");
			return false;
		}

		m_Descriptor.format = pixelFormat;
		m_Descriptor.width = basisDecoder.get_width();
		m_Descriptor.height = basisDecoder.get_height();
		m_Descriptor.mips = basisDecoder.get_levels();
		
		std::vector<GfxMipInfo> mipsInfo = {};
		size_t textureSize = GfxTexture::CalculateTextureDataSizeAndMipsInfo(m_Descriptor, mipsInfo);
		m_Data = std::shared_ptr<uint8_t>(new uint8_t[textureSize]);

		for (uint32_t slice = 0; slice < m_Descriptor.slices; ++slice)
		{
			for (uint32_t mip = 0; mip < m_Descriptor.mips; ++mip)
			{
				const uint32_t index = (slice * m_Descriptor.mips) + mip;
				uint8_t* data = m_Data.get() + mipsInfo[index].offset;

				basist::ktx2_image_level_info levelInfo;
				basisDecoder.get_image_level_info(levelInfo, mip, slice, 0);

				if (!basisDecoder.transcode_image_level(mip, slice, 0, data, levelInfo.m_total_blocks, transcodeFormat))
				{
					LOGERROR("Failed to transcode mip level");
					return false;
				}
			}
		}

		return true;
	}
}

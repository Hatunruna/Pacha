#pragma once
#include "graphics/resources/texture/GfxTexture.h"

#include <filesystem>

namespace Pacha
{
	class Ktx2Reader
	{
	public:
		bool Read(const std::filesystem::path& path);
		bool Read(uint8_t* ktx2File, const size_t size);

		const GfxTextureDescriptor& GetDescriptor();
		const std::shared_ptr<uint8_t>& GetData();
		
	private:
		std::shared_ptr<uint8_t> m_Data;
		GfxTextureDescriptor m_Descriptor;

		bool NeedsTranscoding(uint8_t* ktx2File);
		bool BasisuTranscode(uint8_t* ktx2File, const size_t size);
	};
}
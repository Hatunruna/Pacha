#include "Ktx2Writer.h"
#include "assets/utilities/Ktx2FormatHelpers.h"

#include <fstream>

#include <dfd.h>
#include <basisu.h>
#include <basisu_enc.h>
#include <basisu_comp.h>
#include <basisu_uastc_enc.h>
#include <basisu_transcoder.h>

namespace Pacha
{
	Ktx2Writer::Ktx2Writer(const std::filesystem::path& path)
		: m_Path(path)
	{ }

	bool Ktx2Writer::Write(const GfxTextureDescriptor& descriptor, uint8_t* textureData)
	{
		if (IsCompressibleFormat(descriptor.format))
			return BasisuCompress(descriptor, textureData);

		basist::ktx2_header header = {};
		memcpy(header.m_identifier, basist::g_ktx2_file_identifier, sizeof(basist::g_ktx2_file_identifier));

		header.m_pixel_width = descriptor.width;
		header.m_pixel_height = descriptor.type == GfxTextureType::TEXTURE_2D ? descriptor.height : 0;
		header.m_pixel_depth = descriptor.type == GfxTextureType::TEXTURE_3D ? descriptor.depth : 0;

		header.m_level_count = descriptor.mips;
		header.m_face_count = IsCubemapTexture(descriptor.type) ? 6 : 1;

		uint32_t slices = IsCubemapTexture(descriptor.type) ? descriptor.slices / 6 : descriptor.slices;
		header.m_layer_count = IsArrayTexture(descriptor.type) ? slices : 0;

		header.m_vk_format = GetVkFormat(descriptor.format);
		header.m_type_size = (GetPixelSize(descriptor.format) * 2) / 8;
		header.m_supercompression_scheme = 0;

		VkFormat vkFormat = static_cast<VkFormat>(header.m_vk_format.operator unsigned int());
		uint32_t* dfd = vk2dfd(vkFormat);
		size_t dfdSize = 4 + (24 + (16 * KHR_DFDSAMPLECOUNT(dfd + 1)));

		std::vector<GfxMipInfo> mipsInfo;
		const size_t textureDataSize = GfxTexture::CalculateTextureDataSizeAndMipsInfo(descriptor, mipsInfo);

		std::vector<basist::ktx2_level_index> levelIndices;
		levelIndices.resize(descriptor.mips);

		const size_t levelIndicesSize = sizeof(basist::ktx2_level_index) * levelIndices.size();
		const size_t dfdOffset = sizeof(basist::ktx2_header) + levelIndicesSize;

		header.m_dfd_byte_offset = dfdOffset;
		header.m_dfd_byte_length = dfdSize;

		size_t mipPadding = 0;
		size_t dataOffset = dfdOffset + dfdSize;

		//Mip padding
		if (header.m_supercompression_scheme == basist::KTX2_SS_NONE)
		{
			uint32_t offset = dataOffset & 15;
			mipPadding = (16 - offset) & 15;
			dataOffset += mipPadding;
		}

		//Allocate ktxfile
		std::vector<uint8_t> ktxFileData;
		ktxFileData.resize(sizeof(basist::ktx2_header) + levelIndicesSize + dfdSize + mipPadding + textureDataSize);

		//Reverse mip order
		size_t mipOffset = dataOffset;
		for (int32_t i = static_cast<int32_t>(mipsInfo.size()) - 1; i >= 0; --i)
		{
			const GfxMipInfo& mip = mipsInfo[i];
			basist::ktx2_level_index& level = levelIndices[i];

			level.m_byte_length = mip.size;
			level.m_uncompressed_byte_length = mip.size;

			//Copy texture data
			memcpy(ktxFileData.data() + mipOffset, textureData + mip.offset, mip.size);

			level.m_byte_offset = mipOffset;
			mipOffset += mip.size;
		}

		//Write header and info
		memcpy(ktxFileData.data(), &header, sizeof(basist::ktx2_header));
		memcpy(ktxFileData.data() + sizeof(basist::ktx2_header), levelIndices.data(), levelIndicesSize);
		memcpy(ktxFileData.data() + dfdOffset, dfd, dfdSize);

		std::ofstream ktxFile(m_Path, std::ios::binary);
		if (ktxFile.is_open())
		{
			ktxFile.write(reinterpret_cast<const char*>(ktxFileData.data()), ktxFileData.size());
			ktxFile.close();
			return true;
		}
		else
		{
			LOGERROR("Failed to open ktx2 file for saving " << m_Path);
			return false;
		}
	}

	bool Ktx2Writer::IsCompressibleFormat(const GfxPixelFormat format)
	{
		return !IsBlockCompressedPixelFormat(format) && !IsDepthPixelFormat(format) && !IsHDRPixelFormat(format);
	}
	
	bool Ktx2Writer::BasisuCompress(const GfxTextureDescriptor& descriptor, uint8_t* textureData)
	{
		basisu::basisu_encoder_init();

		basisu::basis_compressor_params basisCompressorParams;
		basisCompressorParams.m_uastc = true;
		basisCompressorParams.m_pack_uastc_flags = basisu::cPackUASTCFavorBC7Error;

		basisCompressorParams.m_multithreading = true;
		basisCompressorParams.m_create_ktx2_file = true;

		basisCompressorParams.m_mip_srgb = IsSRGB(descriptor.format);
		basisCompressorParams.m_perceptual = IsSRGB(descriptor.format);

		//if (diskCompressionLevel >= 1)
		//	basisCompressorParams.m_ktx2_uastc_supercompression = basist::KTX2_SS_ZSTANDARD;

		//if(diskCompressionLevel == 2)
		//{
		//	basisCompressorParams.m_rdo_uastc = true;
		//	basisCompressorParams.m_rdo_uastc_quality_scalar = 1;
		//	basisCompressorParams.m_rdo_uastc_multithreading = true;
		//}

		const uint32_t channels = static_cast<uint32_t>(GetChannelCount(descriptor.format));
		basisCompressorParams.m_source_images.push_back(basisu::image(textureData, descriptor.width, descriptor.height, channels));
		basisCompressorParams.m_source_mipmap_images.resize(1);

		std::vector<GfxMipInfo> mipsInfo = {};
		GfxTextureBase::CalculateTextureDataSizeAndMipsInfo(descriptor, mipsInfo);

		for (uint32_t j = 0; j < descriptor.slices; ++j)
		{
			for (uint32_t i = 0; i < descriptor.mips; ++i)
			{
				if (i != 0 || j != 0)
				{
					const size_t index = i + (j * descriptor.slices);
					const GfxMipInfo& mipInfo = mipsInfo[index];

					const uint32_t mipWidth = descriptor.width >> i;
					const uint32_t mipHeight = descriptor.height >> i;

					basisu::image img(textureData + mipInfo.offset, mipWidth, mipHeight, channels);
					basisCompressorParams.m_source_mipmap_images[0].push_back(img);
				}
			}
		}

		basisu::job_pool jobPool(std::thread::hardware_concurrency());
		basisCompressorParams.m_pJob_pool = &jobPool;

		basisu::basis_compressor basisCompressor;
		if (basisCompressor.init(basisCompressorParams))
		{
			basisu::basis_compressor::error_code result = basisCompressor.process();
			if (result != basisu::basis_compressor::cECSuccess)
			{
				basisu::basisu_encoder_deinit();
				LOGERROR("Failed to compress basis texture");
				return false;
			}
		}
		else
		{
			basisu::basisu_encoder_deinit();
			LOGERROR("Failed to initialize basis compressor");
			return false;
		}

		const basisu::uint8_vec& ktx2 = basisCompressor.get_output_ktx2_file();

		basist::ktx2_header* header = (basist::ktx2_header*)ktx2.data();
		header->m_vk_format = GetVkFormat(descriptor.format);

		std::ofstream ktx2File(m_Path, std::ios::binary);
		ktx2File.write(reinterpret_cast<const char*>(ktx2.data()), ktx2.size());
		ktx2File.close();

		basisu::basisu_encoder_deinit();
		return true;
	}
}

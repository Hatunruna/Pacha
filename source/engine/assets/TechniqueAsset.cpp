#include "TechniqueAsset.h"

#include "fileio/File.h"
#include "core/SystemInfo.h"
#include "core/ProjectConstants.h"
#include "serialization/JsonDeserializer.h"

namespace Pacha
{
	TechniqueAsset::~TechniqueAsset()
	{
	}

	bool TechniqueAsset::LoadImpl()
	{
		std::string jsonData = {};

#if PACHA_EDITOR
		const std::filesystem::path libraryPath = ProjectConstants::GetInstance().GetLibraryPath();
		const std::filesystem::path assetLibraryPath = libraryPath / (m_UUID.GetString() + ".tech");

		File jsonFile = {};
		if (File::OpenFile(assetLibraryPath, jsonFile, Pacha::OpenMode::TEXT))
			jsonData = jsonFile.GetTextData();
#else
		LOGCRITICAL("Not Implemented");
#endif

		GfxShaderReflectionInfo reflectionInfo = {};
		std::unordered_map<PachaId, GfxTechniquePass> passes = {};

		JsonDeserializer deserializer(jsonData);
		deserializer(Serialization::MakeNVP("Passes", passes));
		deserializer(Serialization::MakeNVP("ReflectionInfo", reflectionInfo));
#if PACHA_EDITOR
		deserializer(Serialization::MakeNVP("Sources", m_Sources));
#endif

		m_Technique = new GfxTechnique(passes, reflectionInfo);
		return true;
	}

	void TechniqueAsset::UnloadImpl()
	{
		if (m_Technique)
			delete m_Technique;
		m_Technique = nullptr;
	}
}

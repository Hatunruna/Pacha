#pragma once
#include <string>
#include <glm/glm.hpp>

#if RENDERER_VK
#include <volk.h>
#endif

namespace Pacha
{
#if RENDERER_VK
	struct GfxDebugNamingPayload
	{
		GfxDebugNamingPayload(const std::string& name, const VkObjectType& type, const uint64_t& handle)
		{
			nameInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_OBJECT_NAME_INFO_EXT;
			nameInfo.objectType = type;
			nameInfo.objectHandle = handle;
			nameInfo.pObjectName = name.c_str();
		}

		VkDebugUtilsObjectNameInfoEXT nameInfo = {};
	};

	struct GfxDebugBeginMarkePayload
	{
		GfxDebugBeginMarkePayload(const std::string& markerName, const glm::vec4& color, const VkCommandBuffer& buffer)
		{
			commandBuffer = buffer;

			markerInfo = {};
			markerInfo.sType = VK_STRUCTURE_TYPE_DEBUG_MARKER_MARKER_INFO_EXT;
			markerInfo.color[0] = color.x;
			markerInfo.color[1] = color.y;
			markerInfo.color[2] = color.z;
			markerInfo.color[3] = color.w;
			markerInfo.pMarkerName = markerName.c_str();
		}

		VkCommandBuffer commandBuffer;
		VkDebugMarkerMarkerInfoEXT markerInfo;
	};

	struct GfxDebugEndMarkePayload
	{
		GfxDebugEndMarkePayload(const VkCommandBuffer& buffer)
		{
			commandBuffer = buffer;
		}

		VkCommandBuffer commandBuffer;
	};
#endif
}
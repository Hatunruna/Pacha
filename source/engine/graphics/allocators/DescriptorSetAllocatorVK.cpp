#include "DescriptorSetAllocatorVK.h"
#include "graphics/device/GfxDevice.h"

namespace Pacha
{
	DescriptorSetAllocatorVK::~DescriptorSetAllocatorVK()
	{
		GfxDevice& gfxDevice = GfxDevice::GetInstance();
		for (auto const& pool : m_DescriptorPools)
			vkDestroyDescriptorPool(gfxDevice.GetVkDevice(), pool, nullptr);
	}

	void DescriptorSetAllocatorVK::Initialize(const DescriptorPoolMultipliers& poolSizeMultipliers, const uint32_t& defaultPoolSize)
	{
		m_PoolSize = defaultPoolSize;
		m_PoolSizeMultipliers = poolSizeMultipliers;
		m_DescriptorPools.resize(1);
		AllocatePool();
	}

	VkDescriptorSet DescriptorSetAllocatorVK::AllocateDescriptorSet(const VkDescriptorSetLayout& layout)
	{
		GfxDevice& gfxDevice = GfxDevice::GetInstance();

		VkDescriptorSet set = VK_NULL_HANDLE;
		VkDescriptorSetAllocateInfo allocateInfo = {};
		allocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		allocateInfo.descriptorPool = m_DescriptorPools[m_CurrentPool];
		allocateInfo.descriptorSetCount = 1;
		allocateInfo.pSetLayouts = &layout;

		VkResult result = vkAllocateDescriptorSets(gfxDevice.GetVkDevice(), &allocateInfo, &set);
		switch (result)
		{
			case VK_ERROR_FRAGMENTED_POOL:
			case VK_ERROR_OUT_OF_POOL_MEMORY:
				++m_CurrentPool;

				if (m_CurrentPool >= m_DescriptorPools.size())
				{
					m_DescriptorPools.resize(m_CurrentPool + 1);
					AllocatePool();
				}

				set = AllocateDescriptorSet(layout);
				break;
			default:
			case VK_SUCCESS:
				break;
		}

		return set;
	}

	void DescriptorSetAllocatorVK::Release()
	{
		GfxDevice& gfxDevice = GfxDevice::GetInstance();

		m_CurrentPool = 0;
		for (auto const& pool : m_DescriptorPools)
			vkDestroyDescriptorPool(gfxDevice.GetVkDevice(), pool, nullptr);
		m_DescriptorPools.clear();
	}

	void DescriptorSetAllocatorVK::Reset()
	{
		GfxDevice& gfxDevice = GfxDevice::GetInstance();

		m_CurrentPool = 0;
		for (auto const& pool : m_DescriptorPools)
			vkResetDescriptorPool(gfxDevice.GetVkDevice(), pool, 0);
	}

	void DescriptorSetAllocatorVK::AllocatePool()
	{
		GfxDevice& gfxDevice = GfxDevice::GetInstance();

		std::vector<VkDescriptorPoolSize> descriptorPoolSizes;
		if (m_PoolSizeMultipliers.count(VK_DESCRIPTOR_TYPE_SAMPLER))
			descriptorPoolSizes.push_back({ VK_DESCRIPTOR_TYPE_SAMPLER, m_PoolSizeMultipliers[VK_DESCRIPTOR_TYPE_SAMPLER] * m_PoolSize });

		if (m_PoolSizeMultipliers.count(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER))
			descriptorPoolSizes.push_back({ VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, m_PoolSizeMultipliers[VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER] * m_PoolSize });

		if (m_PoolSizeMultipliers.count(VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE))
			descriptorPoolSizes.push_back({ VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, m_PoolSizeMultipliers[VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE] * m_PoolSize });

		if (m_PoolSizeMultipliers.count(VK_DESCRIPTOR_TYPE_STORAGE_IMAGE))
			descriptorPoolSizes.push_back({ VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, m_PoolSizeMultipliers[VK_DESCRIPTOR_TYPE_STORAGE_IMAGE] * m_PoolSize });

		if (m_PoolSizeMultipliers.count(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER))
			descriptorPoolSizes.push_back({ VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, m_PoolSizeMultipliers[VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER] * m_PoolSize });

		if (m_PoolSizeMultipliers.count(VK_DESCRIPTOR_TYPE_STORAGE_BUFFER))
			descriptorPoolSizes.push_back({ VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, m_PoolSizeMultipliers[VK_DESCRIPTOR_TYPE_STORAGE_BUFFER] * m_PoolSize });

		if (m_PoolSizeMultipliers.count(VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR))
			descriptorPoolSizes.push_back({ VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR, m_PoolSizeMultipliers[VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR] * m_PoolSize });

		VkDescriptorPoolCreateInfo descriptorPoolCreateInfo = {};
		descriptorPoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
		descriptorPoolCreateInfo.poolSizeCount = static_cast<uint32_t>(descriptorPoolSizes.size());
		descriptorPoolCreateInfo.pPoolSizes = descriptorPoolSizes.data();
		descriptorPoolCreateInfo.maxSets = m_PoolSize;

		VkResult result = VK_SUCCESS;
		if ((result = vkCreateDescriptorPool(gfxDevice.GetVkDevice(), &descriptorPoolCreateInfo, nullptr, &m_DescriptorPools[m_CurrentPool])) != VK_SUCCESS)
			LOGERROR("Failed to create descriptor pool: " << GfxDeviceVK::GetErrorString(result));
	}
}
#include "CommandBufferAllocator.h"
#include "core/Assert.h"

namespace Pacha
{
    void CommandBufferAllocator::Reserve(const size_t& size)
	{
		PACHA_ASSERT(size > 0, "Reserve size can't be 0");
		
		m_CurrentBufferIndex = 0;
		if (m_CommandBuffers.size() < size)
			m_CommandBuffers.resize(size);
	}

	GfxCommandBuffer& CommandBufferAllocator::GetCommandBuffer()
	{
		marl::Ticket ticket = m_TicketQueue.take();
		ticket.wait();

		PACHA_ASSERT(m_CurrentBufferIndex != m_CommandBuffers.size(), "There are no command buffers left");
		size_t index = m_CurrentBufferIndex++;
		ticket.done();

		return m_CommandBuffers[index];
	}
}

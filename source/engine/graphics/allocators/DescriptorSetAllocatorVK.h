#pragma once

#include <vector>
#include <unordered_map>
#include <volk.h>

namespace Pacha
{
	typedef std::unordered_map<VkDescriptorType, uint32_t> DescriptorPoolMultipliers;

	class DescriptorSetAllocatorVK
	{
	public:
		~DescriptorSetAllocatorVK();

		void Initialize(const DescriptorPoolMultipliers& poolSizeMultipliers, const uint32_t& defaultPoolSize = 1000);
		VkDescriptorSet AllocateDescriptorSet(const VkDescriptorSetLayout& layout);
		void Release();
		void Reset();

	private:
		uint32_t m_PoolSize = 1000;
		uint32_t m_CurrentPool = 0;
		std::vector<VkDescriptorPool> m_DescriptorPools;
		DescriptorPoolMultipliers m_PoolSizeMultipliers;

		void AllocatePool();
	};
}
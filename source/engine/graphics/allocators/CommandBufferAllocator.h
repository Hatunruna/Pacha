#pragma once
#include "graphics/resources/commandbuffer/GfxCommandBuffer.h"

#include <vector>
#include <atomic>

#include <marl/ticket.h>

namespace Pacha
{
	class CommandBufferAllocator
	{
	public:
		void Reserve(const size_t& size);
		GfxCommandBuffer& GetCommandBuffer();

	private:
		size_t m_CurrentBufferIndex = 0;
		marl::Ticket::Queue m_TicketQueue;
		std::vector<GfxCommandBuffer> m_CommandBuffers;
	};
}
#include "GpuRingBuffer.h"

namespace Pacha
{
	GpuRingBuffer::GpuRingBuffer(const GfxBufferDescriptor& descriptor, uint64_t keepAliveFrames)
		: RingBufferInterface(descriptor.size, keepAliveFrames)
	{
		m_Buffer = new GfxBuffer(descriptor, nullptr);
	}

	GpuRingBuffer::~GpuRingBuffer()
	{
		delete m_Buffer;
	}

	GpuRingBufferAllocation GpuRingBuffer::Allocate(size_t size)
	{
		size_t offset = RingBufferInterface::Allocate(size);
		if (offset != RingBufferInterface::kInvalidOffset)
		{
			GpuRingBufferAllocation allocation = {};
			allocation.size = size;
			allocation.offset = offset;
			allocation.buffer = m_Buffer;

			if(m_Buffer->GetDescriptor().flags & GfxBufferFlags_CPU_ACCESS)
				allocation.mappedDataPtr = m_Buffer->GetData() + offset;

			return allocation;
		}

		return {};
	}
}

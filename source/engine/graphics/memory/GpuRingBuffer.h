#pragma once
#include "memory/RingBufferInterface.h"
#include "graphics/resources/buffer/GfxBuffer.h"

namespace Pacha
{
	struct GpuRingBufferAllocation
	{
		size_t size = 0;
		size_t offset = 0;
		GfxBuffer* buffer = nullptr;
		uint8_t* mappedDataPtr = nullptr;
	};

	class GpuRingBuffer : public RingBufferInterface
	{
	public:
		GpuRingBuffer(const GfxBufferDescriptor& descriptor, uint64_t keepAliveFrames);
		~GpuRingBuffer();

		GpuRingBufferAllocation Allocate(size_t size);

	private:
		GfxBuffer* m_Buffer = nullptr;
	};
}
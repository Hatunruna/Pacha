#pragma once
#include <cstdint>

namespace Pacha
{
	struct GfxRect
	{
		int32_t x;
		int32_t y;
		uint32_t width;
		uint32_t height;
	};
}
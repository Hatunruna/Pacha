#pragma once

namespace Pacha
{
	struct GfxViewport
	{
		float x;
		float y;
		float width;
		float height;
		float minDepth;
		float maxDepth;
	};
}
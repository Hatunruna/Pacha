#pragma once

#include <glm/glm.hpp>

namespace Pacha
{
	namespace Shaders
	{
		struct RenderBatch
		{
			uint32_t batchOffset;

			//Bindless
			uint32_t vertexFormat;
			uint32_t vertexDataOffset;
			uint32_t vertexBufferIndex;
		};

		struct GlobalState
		{
			float delta;
			float time;
			uint32_t frame;
		};

		struct SceneViewData
		{
			glm::mat4 viewProjection;
			glm::vec3 worldPos;
		};

		struct InstanceData
		{
			glm::mat4 model;
			glm::mat4x3 normalMatrix;
		};
	}
}
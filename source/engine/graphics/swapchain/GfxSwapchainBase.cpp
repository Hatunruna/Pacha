#include "GfxSwapchainBase.h"

namespace Pacha
{
	void GfxSwapchainBase::Destroy()
	{
		m_BackBuffers.clear();
		m_CommandBuffers.clear();
		m_BackBufferTextures.clear();
	}

	void GfxSwapchainBase::AddCommandBuffer(GfxCommandBuffer* const& commandBuffer)
	{
		if(commandBuffer)
			m_CommandBuffers.push_back(commandBuffer);
	}

	void GfxSwapchainBase::AddCommandBuffers(const std::vector<GfxCommandBuffer*>& commandBuffers)
	{
		for(auto& commandBuffer : commandBuffers)
			if(commandBuffer)
				m_CommandBuffers.push_back(commandBuffer);
	}

	void GfxSwapchainBase::ClearCommandBuffers()
	{
		m_CommandBuffers.clear();
	}

	void GfxSwapchainBase::SetPresentMode(const GfxPresentMode& mode)
	{
		if (m_PresentMode != mode)
		{
			m_PresentMode = mode;
			OnPresentModeChanged();
		}
	}

	GfxPresentMode GfxSwapchainBase::GetPresentMode()
	{
		return m_PresentMode;
	}

	uint32_t GfxSwapchainBase::GetCurrentFrameIndex()
	{
		return m_CurrentFrameIndex;
	}

	GfxRenderTarget& GfxSwapchainBase::GetCurrentBackbuffer()
	{
		return m_BackBuffers[m_CurrentBackBufferIndex];
	}
	
	uint32_t GfxSwapchainBase::GetBackBufferCount()
	{
		return static_cast<uint32_t>(m_BackBuffers.size());
	}
}
#pragma once
#include "platform/GfxWindow.h"
#include "graphics/resources/commandbuffer/GfxCommandBuffer.h"

#include <vector>

namespace Pacha
{
	enum class GfxPresentMode
	{
		FIFO,
		FIFO_RELAXED,
		IMMEDIATE,
		MAILBOX
	};

	class GfxSwapchainBase
	{
	public:
		virtual bool Initialize(GfxWindow* window) = 0;
		virtual void Destroy();

		virtual bool Prepare() = 0;
		virtual void Present(GfxSwapchainBase* swapchainToWaitFor = nullptr) = 0;

		void AddCommandBuffer(GfxCommandBuffer* const& commandBuffer);
		void AddCommandBuffers(const std::vector<GfxCommandBuffer*>& commandBuffers);
		void ClearCommandBuffers();

		void SetPresentMode(const GfxPresentMode& mode);
		GfxPresentMode GetPresentMode();
		uint32_t GetCurrentFrameIndex();

		GfxRenderTarget& GetCurrentBackbuffer();
		uint32_t GetBackBufferCount();

	protected:
		bool m_IsVsyncOn = true;
		uint32_t m_CurrentFrameIndex = 0;
		uint32_t m_CurrentBackBufferIndex = 0;
		std::vector<GfxRenderTarget> m_BackBuffers;
		std::vector<GfxTexture> m_BackBufferTextures;
		std::vector<GfxCommandBuffer*> m_CommandBuffers;
		GfxPresentMode m_PresentMode = GfxPresentMode::FIFO;
	
		virtual void OnPresentModeChanged() = 0;
	};
}
#pragma once
#if RENDERER_VK
#include "GfxSwapchainVK.h"
#endif

namespace Pacha
{
#if RENDERER_VK
	using GfxSwapchain = GfxSwapchainVK;
#endif
}
#include "GfxSwapchainVK.h"

#include "core/Log.h"
#include "core/ProjectConstants.h"
#include "graphics/device/GfxDevice.h"

#include <SDL_vulkan.h>

namespace Pacha
{
	bool GfxSwapchainVK::Initialize(GfxWindow* window)
	{
		PACHA_ASSERT(!ProjectConstants::GetInstance().UsingHeadlessRendering() && window != nullptr, "Window must be null when using headless rendering")

		m_Window = window;

		if (m_Window && !CreatePresentationSurface())
			return false;

		if (!InitializeData())
			return false;

		if (!CreateSwapchain())
			return false;

		if (!CreateBackBuffer())
			return false;

		if (!CreateSemaphores())
			return false;

		if (!CreateFences())
			return false;

		return true;
	}

	void GfxSwapchainVK::Destroy()
	{
		GfxDevice& gfxDevice = GfxDevice::GetInstance();
		const VkDevice& device = gfxDevice.GetVkDevice();
		const VkInstance& instance = gfxDevice.GetVkInstance();

		m_Swapchain.Destroy(device);

		if (m_PresentationSurface != VK_NULL_HANDLE)
			vkDestroySurfaceKHR(instance, m_PresentationSurface, nullptr);

		for (auto const& it : m_ImageAcquireSemaphores)
			vkDestroySemaphore(device, it, nullptr);

		for (auto const& it : m_FinishedRenderingSemaphores)
			vkDestroySemaphore(device, it, nullptr);

		for (auto const& it : m_Fences)
			vkDestroyFence(device, it, nullptr);

		GfxSwapchainBase::Destroy();
	}

	bool GfxSwapchainVK::Prepare()
	{
		GfxDevice& gfxDevice = GfxDevice::GetInstance();
		const VkDevice& device = gfxDevice.GetVkDevice();

		bool windowSizeChanged = false;
		if (m_Window)
		{
			const glm::uvec2& windowSize = m_Window->GetSize();
			const GfxTexture& backBuffer = m_BackBufferTextures[0];
			windowSizeChanged = windowSize.x != backBuffer.GetWidth() || windowSize.y != backBuffer.GetHeight();
		}

		if (m_NeedsRecreation || windowSizeChanged)
		{
			if (m_Window)
			{
				VkSurfaceCapabilitiesKHR capabilities;
				vkGetPhysicalDeviceSurfaceCapabilitiesKHR(gfxDevice.GetVkPhysicalDevice(), m_PresentationSurface, &capabilities);
				if(capabilities.maxImageExtent.width == 0 || capabilities.maxImageExtent.height == 0)
					return false;
			}

			if (!CreateSwapchain())
			{
				LOGCRITICAL("Error while resizing swapchain images");
				return false;
			}

			if (!CreateBackBuffer())
			{
				LOGCRITICAL("Error while creating new back buffers");
				return false;
			}

			m_NeedsRecreation = false;
		}

		VkResult result = VK_SUCCESS;
		if ((result = vkWaitForFences(device, 1, &m_Fences[m_CurrentFrameIndex], true, UINT64_MAX)) != VK_SUCCESS)
			LOGERROR("Error while waiting for fence: " << GfxDeviceVK::GetErrorString(result));

		if ((result = vkResetFences(device, 1, &m_Fences[m_CurrentFrameIndex])) != VK_SUCCESS)
			LOGERROR("Error while reseting fence: " << GfxDeviceVK::GetErrorString(result));

		if (!AcquireNextImageIndex())
			return false;

		return true;
	}

	void GfxSwapchainVK::Present(GfxSwapchainBase* swapchainToWaitFor)
	{
		GfxDevice& gfxDevice = GfxDevice::GetInstance();

		std::vector<VkCommandBuffer> submitBuffers;
		if (m_CommandBuffers.size())
		{
			submitBuffers.reserve(m_CommandBuffers.size());
			for (auto& buffer : m_CommandBuffers)
				submitBuffers.push_back(buffer->GetBuffer());
			ClearCommandBuffers();
		}
		else
		{
			if (m_FallbackCommandBuffers.size() != m_Fences.size())
				m_FallbackCommandBuffers.resize(m_Fences.size());

			GfxCommandBuffer& buffer = m_FallbackCommandBuffers[m_CurrentFrameIndex];
			buffer.BeginCommandBuffer();
			buffer.TransitionTexture(&m_BackBufferTextures[m_CurrentBackBufferIndex], GfxResourceState::UNDEFINED, GfxResourceState::PRESENT);
			buffer.EndCommandBuffer();
			submitBuffers.push_back(buffer.GetBuffer());
		}

		VkSubmitInfo submitInfo = {};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submitInfo.commandBufferCount = static_cast<uint32_t>(submitBuffers.size());
		submitInfo.pCommandBuffers = submitBuffers.data();
		submitInfo.signalSemaphoreCount = 1;
		submitInfo.pSignalSemaphores = &m_FinishedRenderingSemaphores[m_CurrentFrameIndex];

		std::vector<VkPipelineStageFlags> waitStages = { VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT };
		std::vector<VkSemaphore> semaphoresToWaitFor = { m_ImageAcquireSemaphores[m_CurrentFrameIndex] };
		if (swapchainToWaitFor)
		{
			GfxSwapchainVK* vkSwapchainToWaitFor = static_cast<GfxSwapchainVK*>(swapchainToWaitFor);
			semaphoresToWaitFor.push_back(vkSwapchainToWaitFor->GetCurrentFinishedRenderingSemaphore());
			waitStages.push_back(VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT);
		}

		submitInfo.waitSemaphoreCount = static_cast<uint32_t>(semaphoresToWaitFor.size());
		submitInfo.pWaitSemaphores = semaphoresToWaitFor.data();
		submitInfo.pWaitDstStageMask = waitStages.data();

		VkResult result = VK_SUCCESS;
		if ((result = vkQueueSubmit(gfxDevice.GetGraphicsQueue(), 1, &submitInfo, m_Fences[m_CurrentFrameIndex])) != VK_SUCCESS)
		{
			LOGERROR("Failed to submit graphics queue: " << GfxDeviceVK::GetErrorString(result));
			return;
		}

		if (m_PresentationSurface != VK_NULL_HANDLE)
		{
			VkPresentInfoKHR presentInfo = {};
			presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
			presentInfo.waitSemaphoreCount = 1;
			presentInfo.pWaitSemaphores = &m_FinishedRenderingSemaphores[m_CurrentFrameIndex];
			presentInfo.swapchainCount = 1;
			presentInfo.pSwapchains = &m_Swapchain.handle;
			presentInfo.pImageIndices = &m_CurrentBackBufferIndex;

			result = vkQueuePresentKHR(gfxDevice.GetPresentationQueue(), &presentInfo);
			switch (result)
			{
				case VK_SUCCESS:
					break;

				case VK_SUBOPTIMAL_KHR:
				case VK_ERROR_OUT_OF_DATE_KHR:

					VkSurfaceCapabilitiesKHR capabilities;
					vkGetPhysicalDeviceSurfaceCapabilitiesKHR(gfxDevice.GetVkPhysicalDevice(), m_PresentationSurface, &capabilities);
					if (capabilities.maxImageExtent.width == 0 || capabilities.maxImageExtent.height == 0)
						break;

					m_NeedsRecreation = true;
					break;

				default:
					LOGERROR("Failed to present image: " << GfxDeviceVK::GetErrorString(result));
					return;
			}
		}

		m_CurrentFrameIndex = (m_CurrentFrameIndex + 1) % m_Swapchain.imageCount;
	}

	bool GfxSwapchainVK::InitializeData()
	{
		GfxDevice& gfxDevice = GfxDevice::GetInstance();
		const VkPhysicalDevice& device = gfxDevice.GetVkPhysicalDevice();

		VkResult result = VK_SUCCESS;
		VkSurfaceCapabilitiesKHR surfaceCapabilities;
		if ((result = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, m_PresentationSurface, &surfaceCapabilities)) != VK_SUCCESS)
		{
			LOGERROR("Failed to check presentation surface capabilities: " << GfxDeviceVK::GetErrorString(result));
			return false;
		}

		uint32_t surfaceFormatsCount;
		if ((result = vkGetPhysicalDeviceSurfaceFormatsKHR(device, m_PresentationSurface, &surfaceFormatsCount, nullptr)) != VK_SUCCESS || surfaceFormatsCount == 0)
		{
			LOGERROR("Failed to enumerate presentation surface formats: " << GfxDeviceVK::GetErrorString(result));
			return false;
		}

		std::vector<VkSurfaceFormatKHR> surfaceFormats(surfaceFormatsCount);
		if ((result = vkGetPhysicalDeviceSurfaceFormatsKHR(device, m_PresentationSurface, &surfaceFormatsCount, surfaceFormats.data())) != VK_SUCCESS)
		{
			LOGERROR("Failed to enumerate presentation surface formats: " << GfxDeviceVK::GetErrorString(result));
			return false;
		}

		uint32_t presentModesCount;
		if (vkGetPhysicalDeviceSurfacePresentModesKHR(device, m_PresentationSurface, &presentModesCount, nullptr) != VK_SUCCESS || presentModesCount == 0)
		{
			LOGERROR("Failed to enumerate presentation surface present modes: " << GfxDeviceVK::GetErrorString(result));
			return false;
		}

		m_PresentModes.resize(presentModesCount);
		if (vkGetPhysicalDeviceSurfacePresentModesKHR(device, m_PresentationSurface, &presentModesCount, m_PresentModes.data()) != VK_SUCCESS)
		{
			LOGERROR("Failed to enumerate presentation surface present modes: " << GfxDeviceVK::GetErrorString(result));
			return false;
		}

		//Validate requested image number
		m_Swapchain.imageCount = surfaceCapabilities.minImageCount + 1;
		if (surfaceCapabilities.maxImageCount > 0 && m_Swapchain.imageCount > surfaceCapabilities.maxImageCount)
			m_Swapchain.imageCount = surfaceCapabilities.maxImageCount;

		//Select a surface format
		//There is no preference for surface format
		if (surfaceFormats.size() == 1 && surfaceFormats[0].format == VK_FORMAT_UNDEFINED)
			m_Swapchain.format = { VK_FORMAT_R8G8B8A8_UNORM, VK_COLORSPACE_SRGB_NONLINEAR_KHR };
		else
		{
			bool foundFormat = false;
			for (size_t i = 0; i < surfaceFormats.size(); ++i)
			{
				if (surfaceFormats[i].format == VK_FORMAT_R8G8B8A8_UNORM)
				{
					m_Swapchain.format = surfaceFormats[i];
					foundFormat = true;
					break;
				}
			}

			//If not found chose the prefered format
			if (!foundFormat)
				m_Swapchain.format = surfaceFormats[0];
		}

		//Select image usage type
		m_Swapchain.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;

		//Select image presentation transformation
		if (surfaceCapabilities.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR)
			m_Swapchain.transformation = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
		else
			m_Swapchain.transformation = surfaceCapabilities.currentTransform;

		//Select presentation mode
		for (VkPresentModeKHR& mode : m_PresentModes)
		{
			if (mode == VK_PRESENT_MODE_FIFO_KHR)
			{
				m_Swapchain.presentMode = mode;
				break;
			}
		}

		if (m_Swapchain.presentMode == static_cast<VkPresentModeKHR>(-1))
		{
			LOGERROR("No suitable present mode found on the surface");
			return false;
		}

		m_Swapchain.views.resize(m_Swapchain.imageCount);
		m_Swapchain.images.resize(m_Swapchain.imageCount);

		m_BackBuffers.reserve(m_Swapchain.imageCount);
		m_BackBufferTextures.reserve(m_Swapchain.imageCount);

		for (uint32_t i = 0; i < m_Swapchain.imageCount; ++i)
		{
			m_BackBuffers.emplace_back();
			m_BackBufferTextures.emplace_back();
		}

		return true;
	}

	bool GfxSwapchainVK::CreatePresentationSurface()
	{
		if (!SDL_Vulkan_CreateSurface(reinterpret_cast<SDL_Window*>(m_Window->GetHandle()), GfxDevice::GetInstance().GetVkInstance(), &m_PresentationSurface))
		{
			LOGERROR("Failed to crete vulkan presentation surface: " << SDL_GetError());
			return false;
		}

		return true;
	}

	bool GfxSwapchainVK::CreateSwapchain()
	{
		GfxDevice& gfxDevice = GfxDevice::GetInstance();
		const VkDevice& device = gfxDevice.GetVkDevice();

		VkSwapchainKHR oldSwapchain = m_Swapchain.handle;
		VkSwapchainCreateInfoKHR swapchainCreateInfo = {};
		swapchainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;

		swapchainCreateInfo.surface = m_PresentationSurface;
		swapchainCreateInfo.minImageCount = m_Swapchain.imageCount;
		swapchainCreateInfo.imageFormat = m_Swapchain.format.format;
		swapchainCreateInfo.imageColorSpace = m_Swapchain.format.colorSpace;

		if (m_Window)
		{
			VkSurfaceCapabilitiesKHR capabilities;
			vkGetPhysicalDeviceSurfaceCapabilitiesKHR(gfxDevice.GetVkPhysicalDevice(), m_PresentationSurface, &capabilities);
			swapchainCreateInfo.imageExtent = m_Swapchain.extents = { capabilities.maxImageExtent.width, capabilities.maxImageExtent.height };
		}

		swapchainCreateInfo.preTransform = m_Swapchain.transformation;
		swapchainCreateInfo.presentMode = m_Swapchain.presentMode;
		swapchainCreateInfo.imageUsage = m_Swapchain.usage;
		swapchainCreateInfo.oldSwapchain = oldSwapchain;

		swapchainCreateInfo.imageArrayLayers = 1;
		swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
		swapchainCreateInfo.pQueueFamilyIndices = nullptr;
		swapchainCreateInfo.queueFamilyIndexCount = 0;
		swapchainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
		swapchainCreateInfo.clipped = VK_TRUE;

		VkResult result = VK_SUCCESS;
		if ((result = vkCreateSwapchainKHR(device, &swapchainCreateInfo, nullptr, &m_Swapchain.handle)) != VK_SUCCESS)
		{
			LOGERROR("Failed to create Vulkan swapchain: " << GfxDeviceVK::GetErrorString(result));
			return false;
		}

		if (oldSwapchain != VK_NULL_HANDLE)
			vkDestroySwapchainKHR(device, oldSwapchain, nullptr);

		if ((result = vkGetSwapchainImagesKHR(device, m_Swapchain.handle, &m_Swapchain.imageCount, m_Swapchain.images.data())) != VK_SUCCESS)
		{
			LOGERROR("Failed to get swapchain images handles: " << GfxDeviceVK::GetErrorString(result));
			return false;
		}

		return true;
	}

	bool GfxSwapchainVK::CreateBackBuffer()
	{
		GfxDevice& gfxDevice = GfxDevice::GetInstance();
		const VkDevice& device = gfxDevice.GetVkDevice();

		for (auto const& it : m_Swapchain.views)
			vkDestroyImageView(device, it, nullptr);

		for (uint32_t i = 0; i < m_Swapchain.imageCount; ++i)
		{
			VkImageViewCreateInfo imageViewCreateInfo = {};
			imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
			imageViewCreateInfo.image = m_Swapchain.images[i];
			imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
			imageViewCreateInfo.format = m_Swapchain.format.format;
			imageViewCreateInfo.components = { VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A };
			imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
			imageViewCreateInfo.subresourceRange.levelCount = 1;
			imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
			imageViewCreateInfo.subresourceRange.layerCount = 1;

			VkResult result = VK_SUCCESS;
			if ((result = vkCreateImageView(device, &imageViewCreateInfo, nullptr, &m_Swapchain.views[i])) != VK_SUCCESS)
			{
				LOGERROR("Failed to create back buffer image view: " << GfxDeviceVK::GetErrorString(result));
				return false;
			}

			GfxTextureDescriptor backBufferDescriptor;
			backBufferDescriptor.width = m_Swapchain.extents.width;
			backBufferDescriptor.height = m_Swapchain.extents.height;
			backBufferDescriptor.flags = GfxTextureFlags_DEFAULT_RT;
			backBufferDescriptor.format = GetPixelFormat(m_Swapchain.format.format);

			m_BackBufferTextures[i].SetFromNativeHandle(m_Swapchain.images[i], nullptr, backBufferDescriptor);
			m_BackBufferTextures[i].SetName("BackBuffer_" + std::to_string(i));

			GfxRenderTargetAttachment attachment;
			attachment.index = 0;
			attachment.texture = &m_BackBufferTextures[i];
			attachment.loadOperation = GfxLoadOperation::DONT_CARE;
			attachment.storeOperation = GfxStoreOperation::STORE;
			attachment.initialState = GfxResourceState::UNDEFINED;
			attachment.finalState = GfxResourceState::PRESENT;
			m_BackBuffers[i].SetColorAttachment(attachment);
		}

		return true;
	}

	bool GfxSwapchainVK::CreateSemaphores()
	{
		GfxDevice& gfxDevice = GfxDevice::GetInstance();
		const VkDevice& device = gfxDevice.GetVkDevice();

		VkSemaphoreCreateInfo semaphoreCreateInfo = {};
		semaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

		m_ImageAcquireSemaphores.resize(m_Swapchain.imageCount);
		m_FinishedRenderingSemaphores.resize(m_Swapchain.imageCount);

		VkResult result = VK_SUCCESS;
		for (uint32_t i = 0; i < m_Swapchain.imageCount; ++i)
		{
			if ((result = vkCreateSemaphore(device, &semaphoreCreateInfo, nullptr, &m_ImageAcquireSemaphores[i])) != VK_SUCCESS)
			{
				LOGERROR("Failed to create semaphore: " << GfxDeviceVK::GetErrorString(result));
				return false;
			}

			if ((result = vkCreateSemaphore(device, &semaphoreCreateInfo, nullptr, &m_FinishedRenderingSemaphores[i])) != VK_SUCCESS)
			{
				LOGERROR("Failed to create semaphore: " << GfxDeviceVK::GetErrorString(result));
				return false;
			}
		}

		return true;
	}

	bool GfxSwapchainVK::CreateFences()
	{
		GfxDevice& gfxDevice = GfxDevice::GetInstance();
		const VkDevice& device = gfxDevice.GetVkDevice();

		VkFenceCreateInfo fenceInfo = {};
		fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;
		fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
		m_Fences.resize(m_Swapchain.imageCount);

		VkResult result = VK_SUCCESS;
		for (uint32_t i = 0; i < m_Swapchain.imageCount; ++i)
		{
			if ((result = vkCreateFence(device, &fenceInfo, nullptr, &m_Fences[i])) != VK_SUCCESS)
			{
				LOGERROR("Failed to create fence: " << GfxDeviceVK::GetErrorString(result));
				return false;
			}
		}

		return true;
	}

	void GfxSwapchainVK::OnPresentModeChanged()
	{
		bool supportedMode = false;
		VkPresentModeKHR vkPresentMode = GetVkPresentMode(m_PresentMode);

		for (VkPresentModeKHR& presentMode : m_PresentModes)
			if (presentMode == vkPresentMode)
				supportedMode = true;

		if (supportedMode)
			m_Swapchain.presentMode = vkPresentMode;
		else
		{
			m_PresentMode = GfxPresentMode::FIFO;
			m_Swapchain.presentMode = VK_PRESENT_MODE_FIFO_KHR;
		}

		m_NeedsRecreation = true;
	}

	bool GfxSwapchainVK::AcquireNextImageIndex()
	{
		GfxDevice& gfxDevice = GfxDevice::GetInstance();
		const VkDevice& device = gfxDevice.GetVkDevice();

		m_CurrentBackBufferIndex = UINT32_MAX;
		VkResult result = vkAcquireNextImageKHR(device, m_Swapchain.handle, UINT64_MAX, m_ImageAcquireSemaphores[m_CurrentFrameIndex], VK_NULL_HANDLE, &m_CurrentBackBufferIndex);

		switch (result)
		{
			case VK_SUCCESS:
				break;
			case VK_SUBOPTIMAL_KHR:
			case VK_ERROR_OUT_OF_DATE_KHR:
			{
				VkSurfaceCapabilitiesKHR capabilities;
				vkGetPhysicalDeviceSurfaceCapabilitiesKHR(gfxDevice.GetVkPhysicalDevice(), m_PresentationSurface, &capabilities);
				if (capabilities.maxImageExtent.width == 0 || capabilities.maxImageExtent.height == 0)
					return false;

				if (!CreateSwapchain())
				{
					LOGCRITICAL("Failed to recreate swapchain on Image Acquire");
					return false;
				}

				if (!CreateBackBuffer())
				{
					LOGCRITICAL("Error while creating new back buffers");
					return false;
				}

				vkAcquireNextImageKHR(device, m_Swapchain.handle, UINT64_MAX, m_ImageAcquireSemaphores[m_CurrentFrameIndex], VK_NULL_HANDLE, &m_CurrentBackBufferIndex);
				break;
			}
			default:
			{
				m_CurrentBackBufferIndex = UINT32_MAX;
				LOGERROR("Error while getting next swapchain image: " << GfxDeviceVK::GetErrorString(result));
				return false;
			}
		}

		return true;
	}

	const VkSemaphore& GfxSwapchainVK::GetCurrentFinishedRenderingSemaphore()
	{
		//We get last frame index since the swapchain has been submitted for this semaphore to be signaled
		uint32_t lastFrameIndex = m_CurrentFrameIndex = (m_CurrentFrameIndex - 1) % m_Swapchain.imageCount;
		return m_FinishedRenderingSemaphores[lastFrameIndex];
	}

	GfxPixelFormat GfxSwapchainVK::GetPixelFormat(const VkFormat& format)
	{
		switch (format)
		{
			case VK_FORMAT_R8_UINT:						return GfxPixelFormat::R8_UINT;
			case VK_FORMAT_R8_SINT:						return GfxPixelFormat::R8_SINT;
			case VK_FORMAT_R8_UNORM:					return GfxPixelFormat::R8_UNORM;
			case VK_FORMAT_R8_SNORM:					return GfxPixelFormat::R8_SNORM;
			case VK_FORMAT_R8G8_UINT:					return GfxPixelFormat::RG8_UINT;
			case VK_FORMAT_R8G8_SINT:					return GfxPixelFormat::RG8_SINT;
			case VK_FORMAT_R8G8_UNORM:					return GfxPixelFormat::RG8_UNORM;
			case VK_FORMAT_R8G8_SNORM:					return GfxPixelFormat::RG8_SNORM;
			case VK_FORMAT_R8G8B8A8_UINT:				return GfxPixelFormat::RGBA8_UINT;
			case VK_FORMAT_R8G8B8A8_SINT:				return GfxPixelFormat::RGBA8_SINT;
			case VK_FORMAT_R8G8B8A8_UNORM:				return GfxPixelFormat::RGBA8_UNORM;
			case VK_FORMAT_R8G8B8A8_SNORM:				return GfxPixelFormat::RGBA8_SNORM;
			case VK_FORMAT_R8G8B8A8_SRGB:				return GfxPixelFormat::RGBA8_SRGB;
			case VK_FORMAT_B8G8R8A8_UINT:				return GfxPixelFormat::BGRA8_UINT;
			case VK_FORMAT_B8G8R8A8_SINT:				return GfxPixelFormat::BGRA8_SINT;
			case VK_FORMAT_B8G8R8A8_UNORM:				return GfxPixelFormat::BGRA8_UNORM;
			case VK_FORMAT_B8G8R8A8_SNORM:				return GfxPixelFormat::BGRA8_SNORM;
			case VK_FORMAT_B8G8R8A8_SRGB:				return GfxPixelFormat::BGRA8_SRGB;
			case VK_FORMAT_A2B10G10R10_UNORM_PACK32:	return GfxPixelFormat::R10G10B10A2_UNORM;
			case VK_FORMAT_R16_UINT:					return GfxPixelFormat::R16_UINT;
			case VK_FORMAT_R16_SINT:					return GfxPixelFormat::R16_SINT;
			case VK_FORMAT_R16_UNORM:					return GfxPixelFormat::R16_UNORM;
			case VK_FORMAT_R16_SNORM:					return GfxPixelFormat::R16_SNORM;
			case VK_FORMAT_R16_SFLOAT:					return GfxPixelFormat::R16_FLOAT;
			case VK_FORMAT_R16G16_UINT:					return GfxPixelFormat::RG16_UINT;
			case VK_FORMAT_R16G16_SINT:					return GfxPixelFormat::RG16_SINT;
			case VK_FORMAT_R16G16_UNORM:				return GfxPixelFormat::RG16_UNORM;
			case VK_FORMAT_R16G16_SNORM:				return GfxPixelFormat::RG16_SNORM;
			case VK_FORMAT_R16G16_SFLOAT:				return GfxPixelFormat::RG16_FLOAT;
			case VK_FORMAT_R16G16B16A16_UINT:			return GfxPixelFormat::RGBA16_UINT;
			case VK_FORMAT_R16G16B16A16_SINT:			return GfxPixelFormat::RGBA16_SINT;
			case VK_FORMAT_R16G16B16A16_UNORM:			return GfxPixelFormat::RGBA16_UNORM;
			case VK_FORMAT_R16G16B16A16_SNORM:			return GfxPixelFormat::RGBA16_SNORM;
			case VK_FORMAT_R16G16B16A16_SFLOAT:			return GfxPixelFormat::RGBA16_FLOAT;
			case VK_FORMAT_R32_UINT:					return GfxPixelFormat::R32_UINT;
			case VK_FORMAT_R32_SINT:					return GfxPixelFormat::R32_SINT;
			case VK_FORMAT_R32_SFLOAT:					return GfxPixelFormat::R32_FLOAT;
			case VK_FORMAT_R32G32_UINT:					return GfxPixelFormat::RG32_UINT;
			case VK_FORMAT_R32G32_SINT:					return GfxPixelFormat::RG32_SINT;
			case VK_FORMAT_R32G32_SFLOAT:				return GfxPixelFormat::RG32_FLOAT;
			case VK_FORMAT_R32G32B32A32_UINT:			return GfxPixelFormat::RGBA32_UINT;
			case VK_FORMAT_R32G32B32A32_SINT:			return GfxPixelFormat::RGBA32_SINT;
			case VK_FORMAT_R32G32B32A32_SFLOAT:			return GfxPixelFormat::RGBA32_FLOAT;
			case VK_FORMAT_BC1_RGB_UNORM_BLOCK:			return GfxPixelFormat::BC1_RGB_UNORM;
			case VK_FORMAT_BC1_RGB_SRGB_BLOCK:			return GfxPixelFormat::BC1_RGB_SRGB;
			case VK_FORMAT_BC1_RGBA_UNORM_BLOCK:		return GfxPixelFormat::BC1_RGBA_UNORM;
			case VK_FORMAT_BC1_RGBA_SRGB_BLOCK:			return GfxPixelFormat::BC1_RGBA_SRGB;
			case VK_FORMAT_BC2_UNORM_BLOCK:				return GfxPixelFormat::BC2_UNORM;
			case VK_FORMAT_BC2_SRGB_BLOCK:				return GfxPixelFormat::BC2_SRGB;
			case VK_FORMAT_BC3_UNORM_BLOCK:				return GfxPixelFormat::BC3_UNORM;
			case VK_FORMAT_BC3_SRGB_BLOCK:				return GfxPixelFormat::BC3_SRGB;
			case VK_FORMAT_BC4_UNORM_BLOCK:				return GfxPixelFormat::BC4_UNORM;
			case VK_FORMAT_BC4_SNORM_BLOCK:				return GfxPixelFormat::BC4_SNORM;
			case VK_FORMAT_BC5_UNORM_BLOCK:				return GfxPixelFormat::BC5_UNORM;
			case VK_FORMAT_BC5_SNORM_BLOCK:				return GfxPixelFormat::BC5_SNORM;
			case VK_FORMAT_BC6H_UFLOAT_BLOCK:			return GfxPixelFormat::BC6H_UF16;
			case VK_FORMAT_BC6H_SFLOAT_BLOCK:			return GfxPixelFormat::BC6H_SF16;
			case VK_FORMAT_BC7_UNORM_BLOCK:				return GfxPixelFormat::BC7_UNORM;
			case VK_FORMAT_BC7_SRGB_BLOCK:				return GfxPixelFormat::BC7_SRGB;
			case VK_FORMAT_D32_SFLOAT_S8_UINT:			return GfxPixelFormat::D32_SFLOAT_S8_UINT;
			case VK_FORMAT_D32_SFLOAT:					return GfxPixelFormat::D32_SFLOAT;
			case VK_FORMAT_D24_UNORM_S8_UINT:			return GfxPixelFormat::D24_UNORM_S8_UINT;
			case VK_FORMAT_D16_UNORM:					return GfxPixelFormat::D16_UNORM;
			default:
				LOGCRITICAL("Vulkan format not registered on this function");
				return GfxPixelFormat::R8_UNORM;
		}
	}

	VkPresentModeKHR GfxSwapchainVK::GetVkPresentMode(const GfxPresentMode& presentMode)
	{
		switch (presentMode)
		{
			case GfxPresentMode::FIFO:			return VK_PRESENT_MODE_FIFO_KHR;
			case GfxPresentMode::FIFO_RELAXED:	return VK_PRESENT_MODE_FIFO_RELAXED_KHR;
			case GfxPresentMode::MAILBOX:		return VK_PRESENT_MODE_MAILBOX_KHR;
			case GfxPresentMode::IMMEDIATE:		return VK_PRESENT_MODE_IMMEDIATE_KHR;
			default:
				LOGCRITICAL("Present mode not registered in this function");
				return VK_PRESENT_MODE_FIFO_KHR;
		}
	}
}
#pragma once 
#include "GfxSwapchainBase.h"
#include "graphics/resources/texture/GfxTexture.h"
#include "graphics/resources/rendertarget/GfxRenderTarget.h"

#include <vector>
#include <volk.h>

namespace Pacha
{
	class GfxSwapchainVK : public GfxSwapchainBase
	{
		struct Swapchain
		{
			VkSurfaceFormatKHR format;
			VkSwapchainKHR handle = VK_NULL_HANDLE;

			uint32_t imageCount = 0;
			std::vector<VkImage> images;
			std::vector<VkImageView> views;

			VkExtent2D extents;
			VkImageUsageFlags usage = static_cast<VkImageUsageFlags>(-1);
			VkPresentModeKHR presentMode = static_cast<VkPresentModeKHR>(-1);
			VkSurfaceTransformFlagBitsKHR transformation = static_cast<VkSurfaceTransformFlagBitsKHR>(-1);

			void Destroy(const VkDevice& device)
			{
				for (const VkImageView& view : views)
				{
					if(view != VK_NULL_HANDLE)
						vkDestroyImageView(device, view, nullptr);
				}

				if(handle != VK_NULL_HANDLE)
					vkDestroySwapchainKHR(device, handle, nullptr);
			}
		};

	public:
		static GfxSwapchainVK& GetMainSwapchain()
		{
			static GfxSwapchainVK sInstance;
			return sInstance;
		}

		bool Initialize(GfxWindow* window) override;
		void Destroy() override;

		bool Prepare() override;
		void Present(GfxSwapchainBase* swapchainToWaitFor = nullptr) override;

	private:
		bool InitializeData();
		bool CreatePresentationSurface();
		bool CreateSwapchain();
		bool CreateBackBuffer();
		bool CreateSemaphores();
		bool CreateFences();

		void OnPresentModeChanged() override;
		bool AcquireNextImageIndex();
		const VkSemaphore& GetCurrentFinishedRenderingSemaphore();

		GfxPixelFormat GetPixelFormat(const VkFormat& format);
		VkPresentModeKHR GetVkPresentMode(const GfxPresentMode& presentMode);

		Swapchain m_Swapchain;
		VkSurfaceKHR m_PresentationSurface = VK_NULL_HANDLE;

		GfxWindow* m_Window = nullptr;
		bool m_NeedsRecreation = false;
		
		std::vector<VkFence> m_Fences;
		std::vector<VkSemaphore> m_ImageAcquireSemaphores;
		std::vector<VkSemaphore> m_FinishedRenderingSemaphores;

		std::vector<VkPresentModeKHR> m_PresentModes;
		std::vector<GfxCommandBuffer> m_FallbackCommandBuffers;
	};
}
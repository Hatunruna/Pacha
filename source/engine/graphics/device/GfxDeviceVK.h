#pragma once
#include "GfxDeviceBase.h"

#include <vector>
#include <vk_mem_alloc.h>

namespace Pacha
{
	class GfxDeviceVK : public GfxDeviceBase
	{
	private:
		struct DeviceInfo
		{
			uint32_t score = 0;
			VkPhysicalDevice device;
			VkPhysicalDeviceFeatures2 deviceFeatures;
			VkPhysicalDeviceProperties deviceProperties;
			VkPhysicalDeviceDescriptorIndexingFeatures indexingFeatures;
			
			uint32_t computeQueueFamilyIndex = UINT32_MAX;
			uint32_t transferQueueFamilyIndex = UINT32_MAX;
			uint32_t graphicsQueueFamilyIndex = UINT32_MAX;
			uint32_t presentationQueueFamilyIndex = UINT32_MAX;
		};

	private:
		GfxDeviceVK() = default;
		virtual ~GfxDeviceVK();
		GfxDeviceVK(const GfxDeviceVK&) = delete;
		GfxDeviceVK& operator=(const GfxDeviceVK&) = delete;
		
	public:
		static GfxDeviceVK& GetInstance()
		{
			static GfxDeviceVK sInstance;
			return sInstance;
		}

		bool Initialize();
		void SubmitAndWait(const std::vector<GfxCommandBuffer*>& commandBuffers);
		void WaitIdle();

		const VkDevice& GetVkDevice();
		const VkInstance& GetVkInstance();
		const VkPhysicalDevice& GetVkPhysicalDevice();
		const VmaAllocator& GetMemoryAllocator();

		size_t GetVramUsed();
		void SetObjectDebugName(const GfxDebugNamingPayload& payload);
		void SectionMarkerBegin(const GfxDebugBeginMarkePayload& payload);
		void SectionMarkerEnd(const GfxDebugEndMarkePayload& payload);
	
		VkQueue GetComputeQueue();
		VkQueue GetTransferQueue();
		VkQueue GetGraphicsQueue();
		VkQueue GetPresentationQueue();

		uint32_t GetComputeQueueFamilyIndex();
		uint32_t GetTransferQueueFamilyIndex();
		uint32_t GetGraphicsQueueFamilyIndex();
		uint32_t GetPresentationQueueFamilyIndex();

	private:
		bool CreateInstance();
		bool CreateLogicalDevices();
		bool CreateMemoryAllocator();
		void FillDeviceCapabilities();

		VkDevice m_Device = VK_NULL_HANDLE;
		VkInstance m_Instance = VK_NULL_HANDLE;

		DeviceInfo m_SelectedDevice = {};
		VkQueue m_ComputeQueue = VK_NULL_HANDLE;
		VkQueue m_TransferQueue = VK_NULL_HANDLE;
		VkQueue m_GraphicsQueue = VK_NULL_HANDLE;
		VkQueue m_PresentationQueue = VK_NULL_HANDLE;

		uint32_t m_DeviceLocalMemoryHeapIndex = 0;
		VmaAllocator m_MemoryAllocator = VK_NULL_HANDLE;

		DeviceInfo GetDeviceScore(VkPhysicalDevice& physicalDevice);
		bool IsExtensionAvailable(const char* extensionName, const std::vector<VkExtensionProperties>& availableExtensions);
		
	public:
		static const char* GetErrorString(const VkResult result);

	private:
#ifdef DEBUG
		const char* kValidationLayerName = "VK_LAYER_KHRONOS_validation";
		bool ValidationLayersSupported();

		VkDebugUtilsMessengerEXT m_DebugMessenger;
		static VKAPI_ATTR VkBool32 VKAPI_CALL ValidationLayerCallback(VkDebugUtilsMessageSeverityFlagBitsEXT severity, VkDebugUtilsMessageTypeFlagsEXT type, const VkDebugUtilsMessengerCallbackDataEXT* data, void* userData);
#endif
	};
}
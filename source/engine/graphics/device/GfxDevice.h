#pragma once
#if RENDERER_VK
#include "graphics/device/GfxDeviceVK.h"
#endif

namespace Pacha
{
#if RENDERER_VK
	using GfxDevice = GfxDeviceVK;
#endif
}
#define VMA_IMPLEMENTATION
#include "GfxDeviceVK.h"

#include "core/Log.h"
#include "platform/GfxWindow.h"
#include "core/ProjectConstants.h"

#include <volk.h>
#include <SDL_vulkan.h>
#include <unordered_map>

namespace Pacha
{
	GfxDeviceVK::~GfxDeviceVK()
	{
		vmaDestroyAllocator(m_MemoryAllocator);

		if (m_Device != VK_NULL_HANDLE)
			vkDestroyDevice(m_Device, nullptr);

#ifdef DEBUG
		vkDestroyDebugUtilsMessengerEXT(m_Instance, m_DebugMessenger, nullptr);
#endif
		
		if (m_Instance != VK_NULL_HANDLE)
			vkDestroyInstance(m_Instance, nullptr);
	}

	bool GfxDeviceVK::Initialize()
	{
		if (volkInitialize() != VK_SUCCESS)
		{
			LOGCRITICAL("Failed to load Vulkan entrypoints");
			return false;
		}

		if (!CreateInstance())
			return false;

		if (!CreateLogicalDevices())
			return false;

		if (!CreateMemoryAllocator())
			return false;

		FillDeviceCapabilities();
		return true;
	}

	void GfxDeviceVK::SubmitAndWait(const std::vector<GfxCommandBuffer*>& commandBuffers)
	{
		std::vector<VkCommandBuffer> submitBuffers;
		submitBuffers.reserve(commandBuffers.size());
		
		for (auto& buffer : commandBuffers)
			submitBuffers.push_back(buffer->GetBuffer());
		
		VkSubmitInfo submitInfo = {};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submitInfo.commandBufferCount = static_cast<uint32_t>(submitBuffers.size());
		submitInfo.pCommandBuffers = submitBuffers.data();
		submitInfo.signalSemaphoreCount = 0;
		submitInfo.pSignalSemaphores = nullptr;
		submitInfo.waitSemaphoreCount = 0;
		submitInfo.pWaitSemaphores = nullptr;
		submitInfo.pWaitDstStageMask = nullptr;

		VkResult result = VK_SUCCESS;
		if ((result = vkQueueSubmit(m_GraphicsQueue, 1, &submitInfo, VK_NULL_HANDLE)) != VK_SUCCESS)
		{
			LOGERROR("Failed to submit graphics queue: " << GfxDeviceVK::GetErrorString(result));
			return;
		}

		WaitIdle();
	}

	void GfxDeviceVK::WaitIdle()
	{
		if(m_Device != VK_NULL_HANDLE)
			vkDeviceWaitIdle(m_Device);
	}

	const VkDevice& GfxDeviceVK::GetVkDevice()
	{
		return m_Device;
	}

	const VkInstance& GfxDeviceVK::GetVkInstance()
	{
		return m_Instance;
	}

	const VkPhysicalDevice& GfxDeviceVK::GetVkPhysicalDevice()
	{
		return m_SelectedDevice.device;
	}

	const VmaAllocator& GfxDeviceVK::GetMemoryAllocator()
	{
		return m_MemoryAllocator;
	}

	size_t GfxDeviceVK::GetVramUsed()
	{
		static VmaTotalStatistics sStats;
		m_MemoryAllocator->CalculateStatistics(&sStats);
		return static_cast<size_t>(sStats.memoryHeap[m_DeviceLocalMemoryHeapIndex].statistics.allocationBytes);
	}

	void GfxDeviceVK::SetObjectDebugName(const GfxDebugNamingPayload& payload)
	{
		if (vkSetDebugUtilsObjectNameEXT)
			vkSetDebugUtilsObjectNameEXT(m_Device, &payload.nameInfo);
	}

	void GfxDeviceVK::SectionMarkerBegin(const GfxDebugBeginMarkePayload& payload)
	{
		if (vkCmdDebugMarkerBeginEXT)
			vkCmdDebugMarkerBeginEXT(payload.commandBuffer, &payload.markerInfo);
	}

	void GfxDeviceVK::SectionMarkerEnd(const GfxDebugEndMarkePayload& payload)
	{
		if (vkCmdDebugMarkerEndEXT)
			vkCmdDebugMarkerEndEXT(payload.commandBuffer);
	}

	VkQueue GfxDeviceVK::GetComputeQueue()
	{
		return m_ComputeQueue;
	}

	VkQueue GfxDeviceVK::GetTransferQueue()
	{
		return m_TransferQueue;
	}

	VkQueue GfxDeviceVK::GetGraphicsQueue()
	{
		return m_GraphicsQueue;
	}

	VkQueue GfxDeviceVK::GetPresentationQueue()
	{
		return m_PresentationQueue;
	}

	uint32_t GfxDeviceVK::GetComputeQueueFamilyIndex()
	{
		return m_SelectedDevice.computeQueueFamilyIndex;
	}

	uint32_t GfxDeviceVK::GetTransferQueueFamilyIndex()
	{
		return m_SelectedDevice.transferQueueFamilyIndex;
	}

	uint32_t GfxDeviceVK::GetGraphicsQueueFamilyIndex()
	{
		return m_SelectedDevice.graphicsQueueFamilyIndex;
	}

	uint32_t GfxDeviceVK::GetPresentationQueueFamilyIndex()
	{
		return m_SelectedDevice.presentationQueueFamilyIndex;
	}

	bool GfxDeviceVK::CreateInstance()
	{
		//Create Vulkan Instance
		VkApplicationInfo appInfo = {};
		appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		appInfo.apiVersion = VK_API_VERSION_1_2;

		//Get platform extensions
		unsigned int extensionsCount = 0;
		SDL_Vulkan_GetInstanceExtensions(nullptr, &extensionsCount, nullptr);

		std::vector<const char*> extensions(extensionsCount);
		SDL_Vulkan_GetInstanceExtensions(nullptr, &extensionsCount, extensions.data());

		VkInstanceCreateInfo instanceInfo = {};
		instanceInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		instanceInfo.pApplicationInfo = &appInfo;

#ifdef DEBUG
		bool layersSupported = false;
		if (ValidationLayersSupported())
		{
			layersSupported = true;
			extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
			instanceInfo.enabledLayerCount = 1;
			instanceInfo.ppEnabledLayerNames = &kValidationLayerName;
		}
		else
#endif
		{
			instanceInfo.enabledLayerCount = 0;
			instanceInfo.ppEnabledLayerNames = nullptr;
		}

		instanceInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
		instanceInfo.ppEnabledExtensionNames = extensions.data();

		VkResult result = VK_SUCCESS;
		if ((result = vkCreateInstance(&instanceInfo, nullptr, &m_Instance)) != VK_SUCCESS)
		{
			LOGERROR("Failed to create the vulkan instance: " << GetErrorString(result));
			return false;
		}

		volkLoadInstanceOnly(m_Instance);

#ifdef DEBUG
		if (layersSupported)
		{
			VkDebugUtilsMessengerCreateInfoEXT debugMessagecreateInfo = {};
			debugMessagecreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
			debugMessagecreateInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
			debugMessagecreateInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
			debugMessagecreateInfo.pfnUserCallback = ValidationLayerCallback;

			if ((result = vkCreateDebugUtilsMessengerEXT(m_Instance, &debugMessagecreateInfo, nullptr, &m_DebugMessenger)) != VK_SUCCESS)
				LOGERROR("Failed to initialize validation layer: " << GetErrorString(result));
		}
#endif
		return true;
	}

	bool GfxDeviceVK::CreateLogicalDevices()
	{
		uint32_t numDevices = 0;
		VkResult result = VkResult::VK_SUCCESS;
		if ((result = vkEnumeratePhysicalDevices(m_Instance, &numDevices, nullptr)) != VK_SUCCESS || numDevices == 0)
		{
			LOGERROR("Failed to get number of physical vulkan devices: " << GetErrorString(result));
			return false;
		}

		std::vector<VkPhysicalDevice> physicalDevices(numDevices);
		if ((result = vkEnumeratePhysicalDevices(m_Instance, &numDevices, &physicalDevices[0])) != VK_SUCCESS)
		{
			LOGERROR("Failed to get physical vulkan devices: " << GetErrorString(result));
			return false;
		}

		//Check capabilities and decide
		std::vector<DeviceInfo> deviceInfo(numDevices);
		for (uint32_t i = 0; i < numDevices; ++i)
			deviceInfo[i] = GetDeviceScore(physicalDevices[i]);

		int32_t deviceId = -1;
		uint32_t maxScore = 0;
		for (uint32_t i = 0; i < numDevices; ++i)
		{
			if (deviceInfo[i].score >= maxScore)
			{
				deviceId = i;
				maxScore = deviceInfo[i].score;
			}
		}

		if (deviceId == -1)
		{
			LOGERROR("Failed to find a suitable vulkan device.");
			return false;
		}

		m_SelectedDevice = deviceInfo[deviceId];
		if (m_SelectedDevice.graphicsQueueFamilyIndex == UINT32_MAX)
		{
			LOGERROR("Failed to find a suitable graphics device.");
			return false;
		}

		LOGINFO("Selected device: " << m_SelectedDevice.deviceProperties.deviceName);

		uint32_t extensionsCount = 0;
		if ((result = vkEnumerateDeviceExtensionProperties(m_SelectedDevice.device, nullptr, &extensionsCount, nullptr)) != VK_SUCCESS || extensionsCount == 0)
		{
			LOGERROR("Failed to enumerate available physical device extensions: " << GetErrorString(result));
			return false;
		}

		std::vector<VkExtensionProperties> availableExtensions(extensionsCount);
		if ((result = vkEnumerateDeviceExtensionProperties(m_SelectedDevice.device, nullptr, &extensionsCount, availableExtensions.data())) != VK_SUCCESS)
		{
			LOGERROR("Failed to enumerate available physical device extensions: " << GetErrorString(result));
			return false;
		}

		//Required Extensions
		std::vector<const char*> enabledExtensions;
		if (!IsExtensionAvailable(VK_KHR_SWAPCHAIN_EXTENSION_NAME, availableExtensions))
		{
			LOGERROR("Required extension '" << VK_KHR_SWAPCHAIN_EXTENSION_NAME << "' is not available.");
			return false;
		}
		else
			enabledExtensions.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);

		//Optional Extensions
		if (IsExtensionAvailable(VK_EXT_DEBUG_MARKER_EXTENSION_NAME, availableExtensions))
			enabledExtensions.push_back(VK_EXT_DEBUG_MARKER_EXTENSION_NAME);

		if (IsExtensionAvailable(VK_KHR_MULTIVIEW_EXTENSION_NAME, availableExtensions))
		{
			m_SupportsMultiViewRendering = true;
			enabledExtensions.push_back(VK_KHR_MULTIVIEW_EXTENSION_NAME);
		}

		if (IsExtensionAvailable(VK_KHR_RAY_TRACING_PIPELINE_EXTENSION_NAME, availableExtensions) &&
			IsExtensionAvailable(VK_KHR_ACCELERATION_STRUCTURE_EXTENSION_NAME, availableExtensions) &&
			IsExtensionAvailable(VK_KHR_DEFERRED_HOST_OPERATIONS_EXTENSION_NAME, availableExtensions))
		{
			m_SupportsRaytracing = true;
			enabledExtensions.push_back(VK_KHR_RAY_TRACING_PIPELINE_EXTENSION_NAME);
			enabledExtensions.push_back(VK_KHR_ACCELERATION_STRUCTURE_EXTENSION_NAME);
			enabledExtensions.push_back(VK_KHR_DEFERRED_HOST_OPERATIONS_EXTENSION_NAME);

			if (IsExtensionAvailable(VK_KHR_RAY_QUERY_EXTENSION_NAME, availableExtensions))
			{
				m_SupportsInlineRaytracing = true;
				enabledExtensions.push_back(VK_KHR_RAY_QUERY_EXTENSION_NAME);
			}
		}

		if (IsExtensionAvailable(VK_KHR_FRAGMENT_SHADING_RATE_EXTENSION_NAME, availableExtensions))
		{
			m_SupportsVariableRateShading = true;
			enabledExtensions.push_back(VK_KHR_FRAGMENT_SHADING_RATE_EXTENSION_NAME);
		}

		if (IsExtensionAvailable(VK_EXT_MEMORY_BUDGET_EXTENSION_NAME, availableExtensions))
			enabledExtensions.push_back(VK_EXT_MEMORY_BUDGET_EXTENSION_NAME);

		m_TimestampPeriod = m_SelectedDevice.deviceProperties.limits.timestampPeriod;
		
		//Check for Bindless support
		if (m_SelectedDevice.indexingFeatures.descriptorBindingPartiallyBound && m_SelectedDevice.indexingFeatures.runtimeDescriptorArray)
			m_SupportsBindlessResources = true;

		//Get Device queues
		std::vector<float> queuePriorities = { 1.0f };
		std::vector<VkDeviceQueueCreateInfo> deviceQueueInfos;

		VkDeviceQueueCreateInfo queueCreateInfo = {};
		queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo.queueCount = static_cast<uint32_t>(queuePriorities.size());
		queueCreateInfo.pQueuePriorities = queuePriorities.data();

		//Grahics queue
		queueCreateInfo.queueFamilyIndex = m_SelectedDevice.graphicsQueueFamilyIndex;
		deviceQueueInfos.push_back(queueCreateInfo);

		//Presentation Queue
		if (m_SelectedDevice.presentationQueueFamilyIndex != UINT32_MAX)
		{
			if (m_SelectedDevice.presentationQueueFamilyIndex != m_SelectedDevice.graphicsQueueFamilyIndex)
			{
				queueCreateInfo.queueFamilyIndex = m_SelectedDevice.presentationQueueFamilyIndex;
				deviceQueueInfos.push_back(queueCreateInfo);
			}
		}
		else
		{
			if (!ProjectConstants::GetInstance().UsingHeadlessRendering())
			{
				LOGERROR("Failed to find a suitable presentation device.");
				return false;
			}
		}

		//Async Compute Queue
		if (m_SelectedDevice.computeQueueFamilyIndex != m_SelectedDevice.graphicsQueueFamilyIndex)
		{
			m_SupportesAsyncCompute = true;
			queueCreateInfo.queueFamilyIndex = m_SelectedDevice.computeQueueFamilyIndex;
			deviceQueueInfos.push_back(queueCreateInfo);
		}

		//Async Transfer Queue
		if (m_SelectedDevice.transferQueueFamilyIndex != m_SelectedDevice.graphicsQueueFamilyIndex)
		{
			m_SupportsAsyncTransfers = true;
			queueCreateInfo.queueFamilyIndex = m_SelectedDevice.transferQueueFamilyIndex;
			deviceQueueInfos.push_back(queueCreateInfo);
		}

		VkDeviceCreateInfo deviceCreateInfo = {};
		deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
		deviceCreateInfo.queueCreateInfoCount = static_cast<uint32_t>(deviceQueueInfos.size());
		deviceCreateInfo.pQueueCreateInfos = deviceQueueInfos.data();
		deviceCreateInfo.enabledExtensionCount = static_cast<uint32_t>(enabledExtensions.size());
		deviceCreateInfo.ppEnabledExtensionNames = enabledExtensions.data();
		deviceCreateInfo.enabledLayerCount = 0;
		deviceCreateInfo.ppEnabledLayerNames = nullptr;
		deviceCreateInfo.pNext = &m_SelectedDevice.deviceFeatures;
		m_SelectedDevice.deviceFeatures.pNext = &m_SelectedDevice.indexingFeatures;

		VkPhysicalDeviceMultiviewFeatures multiviewFeatures = {};
		multiviewFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_FEATURES;
		multiviewFeatures.multiview = true;
		multiviewFeatures.multiviewGeometryShader = true;
		multiviewFeatures.multiviewTessellationShader = true;

		if (m_SupportsMultiViewRendering)
			m_SelectedDevice.indexingFeatures.pNext = &multiviewFeatures;

		if ((result = vkCreateDevice(m_SelectedDevice.device, &deviceCreateInfo, nullptr, &m_Device)) != VK_SUCCESS)
		{
			LOGERROR("Failed to create the Vulkan device: " << GetErrorString(result));
			return false;
		}

		volkLoadDevice(m_Device);

		//Get the queues created by the device.
		vkGetDeviceQueue(m_Device, m_SelectedDevice.computeQueueFamilyIndex, 0, &m_ComputeQueue);
		vkGetDeviceQueue(m_Device, m_SelectedDevice.transferQueueFamilyIndex, 0, &m_TransferQueue);
		vkGetDeviceQueue(m_Device, m_SelectedDevice.graphicsQueueFamilyIndex, 0, &m_GraphicsQueue);
		vkGetDeviceQueue(m_Device, m_SelectedDevice.presentationQueueFamilyIndex, 0, &m_PresentationQueue);
		return true;
	}

	bool GfxDeviceVK::CreateMemoryAllocator()
	{
		VmaVulkanFunctions functions;
		functions.vkGetInstanceProcAddr = vkGetInstanceProcAddr;
		functions.vkGetDeviceProcAddr = vkGetDeviceProcAddr;
		functions.vkGetPhysicalDeviceProperties = vkGetPhysicalDeviceProperties;
		functions.vkGetPhysicalDeviceMemoryProperties = vkGetPhysicalDeviceMemoryProperties;
		functions.vkAllocateMemory = vkAllocateMemory;
		functions.vkFreeMemory = vkFreeMemory;
		functions.vkMapMemory = vkMapMemory;
		functions.vkUnmapMemory = vkUnmapMemory;
		functions.vkFlushMappedMemoryRanges = vkFlushMappedMemoryRanges;
		functions.vkInvalidateMappedMemoryRanges = vkInvalidateMappedMemoryRanges;
		functions.vkBindBufferMemory = vkBindBufferMemory;
		functions.vkBindImageMemory = vkBindImageMemory;
		functions.vkGetBufferMemoryRequirements = vkGetBufferMemoryRequirements;
		functions.vkGetImageMemoryRequirements = vkGetImageMemoryRequirements;
		functions.vkCreateBuffer = vkCreateBuffer;
		functions.vkDestroyBuffer = vkDestroyBuffer;
		functions.vkCreateImage = vkCreateImage;
		functions.vkDestroyImage = vkDestroyImage;
		functions.vkCmdCopyBuffer = vkCmdCopyBuffer;

#if VMA_DEDICATED_ALLOCATION || VMA_VULKAN_VERSION >= 1001000
		functions.vkGetBufferMemoryRequirements2KHR = vkGetBufferMemoryRequirements2;
		functions.vkGetImageMemoryRequirements2KHR = vkGetImageMemoryRequirements2;
#endif

#if VMA_BIND_MEMORY2 || VMA_VULKAN_VERSION >= 1001000
		functions.vkBindBufferMemory2KHR = vkBindBufferMemory2;
		functions.vkBindImageMemory2KHR = vkBindImageMemory2;
#endif

#if VMA_MEMORY_BUDGET
		functions.vkGetPhysicalDeviceMemoryProperties2KHR = vkGetPhysicalDeviceMemoryProperties2;
#endif

#if VMA_VULKAN_VERSION >= 1003000
		functions.vkGetDeviceBufferMemoryRequirements = vkGetDeviceBufferMemoryRequirements;
		functions.vkGetDeviceImageMemoryRequirements = vkGetDeviceImageMemoryRequirements;
#endif

		VmaAllocatorCreateInfo allocatorInfo = {};
		allocatorInfo.flags = VMA_ALLOCATOR_CREATE_EXT_MEMORY_BUDGET_BIT;
		allocatorInfo.physicalDevice = m_SelectedDevice.device;
		allocatorInfo.device = m_Device;
		allocatorInfo.instance = m_Instance;
		allocatorInfo.pVulkanFunctions = &functions;

		VkResult result = VK_SUCCESS;
		if ((result = vmaCreateAllocator(&allocatorInfo, &m_MemoryAllocator)) != VK_SUCCESS)
		{
			LOGERROR("Failed to create memory allocator: " << GetErrorString(result));
			return false;
		}

		VkPhysicalDeviceMemoryProperties memoryProperties = {};
		vkGetPhysicalDeviceMemoryProperties(m_SelectedDevice.device, &memoryProperties);
		for (uint32_t i = 0; i < memoryProperties.memoryTypeCount; ++i)
		{
			VkMemoryType& memoryType = memoryProperties.memoryTypes[i];
			if (memoryType.propertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
			{
				m_DeviceLocalMemoryHeapIndex = memoryType.heapIndex;
				break;
			}
		}

		return true;
	}

	void GfxDeviceVK::FillDeviceCapabilities()
	{
		m_MaxColorAttachments = m_SelectedDevice.deviceProperties.limits.maxFragmentOutputAttachments;
	}

	GfxDeviceVK::DeviceInfo GfxDeviceVK::GetDeviceScore(VkPhysicalDevice& physicalDevice)
	{
		DeviceInfo info;
		info.device = physicalDevice;

		vkGetPhysicalDeviceProperties(physicalDevice, &info.deviceProperties);
		LOGINFO("Found device: " << info.deviceProperties.deviceName);

		uint32_t queueFamiliesCount = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamiliesCount, nullptr);

		if (info.deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
		{
			LOGINFO("\t Type: Discrete GPU");
			info.score += 20;
		}

		if (info.deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU)
		{
			LOGINFO("\t Type: Virtual GPU");
			info.score += 10;
		}

		if (info.deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU)
		{
			LOGINFO("\t Type: Integrated GPU");
			info.score += 5;
		}

		if (info.deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_CPU)
		{
			LOGINFO("\t Type: CPU");
			info.score += 1;
		}

		if (info.deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_OTHER)
			LOGINFO("\t Type: Other");

		std::vector<VkBool32> queuePresentSupport(queueFamiliesCount);
		std::vector<VkQueueFamilyProperties> queueFamilyProperties(queueFamiliesCount);
		vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamiliesCount, queueFamilyProperties.data());

		bool foundPresentationQueue = false;
		bool foundGraphicsComputeQueue = false;
		bool foundAsyncComputeQueue = false;
		bool foundAsyncTransferQueue = false;

		VkSurfaceKHR temporarySurface = VK_NULL_HANDLE;
		if (!ProjectConstants::GetInstance().UsingHeadlessRendering())
		{
			GfxWindow& mainWIndow = GfxWindow::GetMainWindow();
			if (!SDL_Vulkan_CreateSurface(reinterpret_cast<SDL_Window*>(mainWIndow.GetHandle()), m_Instance, &temporarySurface))
			{
				LOGERROR("Failed to crete temporal vulkan presentation surface: " << SDL_GetError());
				return info;
			}
		}

		for (uint32_t i = 0; i < queueFamiliesCount; ++i)
		{
			if (queueFamilyProperties[i].queueCount > 0)
			{
				if (!ProjectConstants::GetInstance().UsingHeadlessRendering())
					vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, i, temporarySurface, &queuePresentSupport[i]);

				const bool supportsPresentation = queuePresentSupport[i];
				const bool supportsGraphics = queueFamilyProperties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT;
				const bool supportsCompute = queueFamilyProperties[i].queueFlags & VK_QUEUE_COMPUTE_BIT;
				const bool supportsTransfers = queueFamilyProperties[i].queueFlags & VK_QUEUE_TRANSFER_BIT;

				if (!foundGraphicsComputeQueue && supportsGraphics && supportsCompute)
				{
					if (!foundPresentationQueue && supportsPresentation)
					{
						foundPresentationQueue = true;
						info.presentationQueueFamilyIndex = i;
						LOGINFO("\t Supports presentation");
					}

					foundGraphicsComputeQueue = true;
					info.graphicsQueueFamilyIndex = i;
					info.computeQueueFamilyIndex = i;
					info.transferQueueFamilyIndex = i;
					LOGINFO("\t Supports graphics");
					LOGINFO("\t Supports compute");
				}

				if (!foundAsyncComputeQueue && supportsCompute && !supportsGraphics)
				{
					info.score += 1;
					foundAsyncComputeQueue = true;
					info.computeQueueFamilyIndex = i;
					LOGINFO("\t Supports async compute");
				}

				if (!foundAsyncTransferQueue && supportsTransfers && !supportsGraphics && !supportsCompute)
				{
					info.score += 1;
					foundAsyncTransferQueue = true;
					info.transferQueueFamilyIndex = i;
					LOGINFO("\t Supports async transfers");
				}
			}
		}

		if (temporarySurface != VK_NULL_HANDLE)
			vkDestroySurfaceKHR(m_Instance, temporarySurface, nullptr);

		info.indexingFeatures = {};
		info.indexingFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES_EXT;

		info.deviceFeatures = {};
		info.deviceFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
		info.deviceFeatures.pNext = &info.indexingFeatures;

		vkGetPhysicalDeviceFeatures2(physicalDevice, &info.deviceFeatures);
		if (info.indexingFeatures.descriptorBindingPartiallyBound && info.indexingFeatures.runtimeDescriptorArray)
		{
			info.score += 1;
			LOGINFO("\t Supports bindless resources");
		}
		
		LOGINFO("\t Device score: " << info.score);
		return info;
	}

	bool GfxDeviceVK::IsExtensionAvailable(const char* extensionName, const std::vector<VkExtensionProperties>& availableExtensions)
	{
		for (size_t i = 0; i < availableExtensions.size(); ++i)
			if (strcmp(extensionName, availableExtensions[i].extensionName) == 0)
				return true;
		return false;
	}

	const char* GfxDeviceVK::GetErrorString(const VkResult result)
	{
		static const std::unordered_map<VkResult, const char*> kErrorStrings =
		{
			{ VK_SUCCESS, "VK_SUCCESS" },
			{ VK_NOT_READY, "VK_NOT_READY"},
			{ VK_TIMEOUT, "VK_TIMEOUT"},
			{ VK_EVENT_SET, "VK_EVENT_SET"},
			{ VK_EVENT_RESET, "VK_EVENT_RESET"},
			{ VK_INCOMPLETE, "VK_INCOMPLETE"},
			{ VK_ERROR_OUT_OF_HOST_MEMORY, "VK_ERROR_OUT_OF_HOST_MEMORY"},
			{ VK_ERROR_OUT_OF_DEVICE_MEMORY, "VK_ERROR_OUT_OF_DEVICE_MEMORY"},
			{ VK_ERROR_INITIALIZATION_FAILED, "VK_ERROR_INITIALIZATION_FAILED"},
			{ VK_ERROR_DEVICE_LOST, "VK_ERROR_DEVICE_LOST"},
			{ VK_ERROR_MEMORY_MAP_FAILED, "VK_ERROR_MEMORY_MAP_FAILED"},
			{ VK_ERROR_LAYER_NOT_PRESENT, "VK_ERROR_LAYER_NOT_PRESENT"},
			{ VK_ERROR_EXTENSION_NOT_PRESENT, "VK_ERROR_EXTENSION_NOT_PRESENT"},
			{ VK_ERROR_FEATURE_NOT_PRESENT, "VK_ERROR_FEATURE_NOT_PRESENT"},
			{ VK_ERROR_INCOMPATIBLE_DRIVER, "VK_ERROR_INCOMPATIBLE_DRIVER"},
			{ VK_ERROR_TOO_MANY_OBJECTS, "VK_ERROR_TOO_MANY_OBJECTS"},
			{ VK_ERROR_FORMAT_NOT_SUPPORTED, "VK_ERROR_FORMAT_NOT_SUPPORTED"},
			{ VK_ERROR_FRAGMENTED_POOL, "VK_ERROR_FRAGMENTED_POOL"},
			{ VK_ERROR_UNKNOWN, "VK_ERROR_UNKNOWN"},
			{ VK_ERROR_OUT_OF_POOL_MEMORY, "VK_ERROR_OUT_OF_POOL_MEMORY"},
			{ VK_ERROR_INVALID_EXTERNAL_HANDLE, "VK_ERROR_INVALID_EXTERNAL_HANDLE"},
			{ VK_ERROR_FRAGMENTATION, "VK_ERROR_FRAGMENTATION"},
			{ VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS, "VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS"},
			{ VK_ERROR_SURFACE_LOST_KHR, "VK_ERROR_SURFACE_LOST_KHR"},
			{ VK_ERROR_NATIVE_WINDOW_IN_USE_KHR, "VK_ERROR_NATIVE_WINDOW_IN_USE_KHR"},
			{ VK_SUBOPTIMAL_KHR, "VK_SUBOPTIMAL_KHR"},
			{ VK_ERROR_OUT_OF_DATE_KHR, "VK_ERROR_OUT_OF_DATE_KHR"},
			{ VK_ERROR_INCOMPATIBLE_DISPLAY_KHR, "VK_ERROR_INCOMPATIBLE_DISPLAY_KHR"},
			{ VK_ERROR_VALIDATION_FAILED_EXT, "VK_ERROR_VALIDATION_FAILED_EXT"},
			{ VK_ERROR_INVALID_SHADER_NV, "VK_ERROR_INVALID_SHADER_NV"},
			{ VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT, "VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT"},
			{ VK_ERROR_NOT_PERMITTED_EXT, "VK_ERROR_NOT_PERMITTED_EXT"},
			{ VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT, "VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT"},
			{ VK_THREAD_IDLE_KHR, "VK_THREAD_IDLE_KHR"},
			{ VK_THREAD_DONE_KHR, "VK_THREAD_DONE_KHR"},
			{ VK_OPERATION_DEFERRED_KHR, "VK_OPERATION_DEFERRED_KHR"},
			{ VK_OPERATION_NOT_DEFERRED_KHR, "VK_OPERATION_NOT_DEFERRED_KHR"},
			{ VK_PIPELINE_COMPILE_REQUIRED_EXT, "VK_PIPELINE_COMPILE_REQUIRED_EXT"},
			{ VK_ERROR_OUT_OF_POOL_MEMORY_KHR, "VK_ERROR_OUT_OF_POOL_MEMORY_KHR"},
			{ VK_ERROR_INVALID_EXTERNAL_HANDLE_KHR, "VK_ERROR_INVALID_EXTERNAL_HANDLE_KHR"},
			{ VK_ERROR_FRAGMENTATION_EXT, "VK_ERROR_FRAGMENTATION_EXT"},
			{ VK_ERROR_INVALID_DEVICE_ADDRESS_EXT,  "VK_ERROR_INVALID_DEVICE_ADDRESS_EXT"},
			{ VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS_KHR, "VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS_KHR"},
			{ VK_ERROR_PIPELINE_COMPILE_REQUIRED_EXT, "VK_ERROR_PIPELINE_COMPILE_REQUIRED_EXT"}
		};

		return kErrorStrings.at(result);
	}

#ifdef DEBUG
	bool GfxDeviceVK::ValidationLayersSupported()
	{
		uint32_t layerCount;
		vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

		std::vector<VkLayerProperties> availableLayers(layerCount);
		vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

		bool found = false;
		for (const auto& layerProperties : availableLayers)
		{
			if (strcmp(kValidationLayerName, layerProperties.layerName) == 0)
			{
				found = true;
				break;
			}
		}

		return found;
	}

	VKAPI_ATTR VkBool32 VKAPI_CALL GfxDeviceVK::ValidationLayerCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity, VkDebugUtilsMessageTypeFlagsEXT messageType, const VkDebugUtilsMessengerCallbackDataEXT* data, void*)
	{
		if (messageType >= VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT)
		{
			switch (messageSeverity)
			{
				case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
					LOGINFO(data->pMessage);
					break;

				case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
					LOGWARNING(data->pMessage);
					break;

				case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
					LOGCRITICAL(data->pMessage);
					break;

				default:
					break;
			}
		}

		return VK_FALSE;
	}
#endif
}
#pragma once
#include "graphics/debug/GfxDebugPayloads.h"
#include "graphics/resources/commandbuffer/GfxCommandBuffer.h"

#include <string>
#include <vector>

namespace Pacha
{
	class GfxDeviceBase
	{
	public:
		virtual bool Initialize() = 0;
		virtual void SubmitAndWait(const std::vector<GfxCommandBuffer*>& commandBuffers) = 0;
		virtual void WaitIdle() = 0;

		virtual size_t GetVramUsed() = 0;
		virtual void SetObjectDebugName(const GfxDebugNamingPayload& payload) = 0;
		virtual void SectionMarkerBegin(const GfxDebugBeginMarkePayload& payload) = 0;
		virtual void SectionMarkerEnd(const GfxDebugEndMarkePayload& payload) = 0;

		float GetTimestampPeriod();
		uint32_t MaxColorAttachments();

		static bool SupportsAsyncCompute();
		static bool SupportsAsyncTransfers();
		static bool SupportsBindlessResources();
		static bool SupportsMultiViewRendering();
		
		static bool SupportsRaytracing();
		static bool SupportsInlineRaytracing();
		static bool SupportsVariableRateShading();

	protected:
		GfxDeviceBase() = default;
		virtual ~GfxDeviceBase() { };

		float m_TimestampPeriod = 1;
		uint32_t m_MaxColorAttachments = 1;
		
		static inline bool m_SupportesAsyncCompute = false;
		static inline bool m_SupportsAsyncTransfers = false;
		static inline bool m_SupportsBindlessResources = false;
		static inline bool m_SupportsMultiViewRendering = false;
		
		static inline bool m_SupportsRaytracing = false;
		static inline bool m_SupportsInlineRaytracing = false;
		static inline bool m_SupportsVariableRateShading = false;
	};
}
#include "GfxDevice.h"

namespace Pacha
{
	float GfxDeviceBase::GetTimestampPeriod()
	{
		return m_TimestampPeriod;
	}

	uint32_t GfxDeviceBase::MaxColorAttachments()
	{
		return m_MaxColorAttachments;
	}

	bool GfxDeviceBase::SupportsAsyncCompute()
	{
		return m_SupportesAsyncCompute;
	}

	bool GfxDeviceBase::SupportsAsyncTransfers()
	{
		return m_SupportsAsyncTransfers;
	}

	bool GfxDeviceBase::SupportsBindlessResources()
	{
		return m_SupportsBindlessResources;
	}

	bool GfxDeviceBase::SupportsMultiViewRendering()
	{
		return m_SupportsMultiViewRendering;
	}

	bool GfxDeviceBase::SupportsRaytracing()
	{
		return m_SupportsRaytracing;
	}

	bool GfxDeviceBase::SupportsInlineRaytracing()
	{
		return m_SupportsInlineRaytracing;
	}

	bool GfxDeviceBase::SupportsVariableRateShading()
	{
		return m_SupportsVariableRateShading;
	}
}
#pragma once
#include "GfxHwdMaterialBase.h"
#include "graphics/allocators/DescriptorSetAllocatorVK.h"

#include <thread>
#include <unordered_map>

namespace Pacha
{
	typedef std::unordered_map<std::thread::id, DescriptorSetAllocatorVK> ThreadDescriptorSetAllocators;
	class GfxHwdMaterialVK : public GfxHwdMaterialBase
	{
	public:
		GfxHwdMaterialVK(GfxTechnique* technique);
		~GfxHwdMaterialVK();
	
	private:
		std::vector<VkDescriptorSet> m_DescriptorSets;
		static ThreadDescriptorSetAllocators s_ThreadDescriptorAllocators;
	};
}
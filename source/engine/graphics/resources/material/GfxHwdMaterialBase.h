#pragma once
#include "graphics/resources/sampler/GfxSampler.h"
#include "graphics/resources/texture/GfxTexture.h"
#include "graphics/resources/technique/GfxTechnique.h"
#include "graphics/resources/buffer/GfxConstantBuffer.h"
#include "graphics/resources/buffer/GfxDynamicConstantBuffer.h"

namespace Pacha
{
	enum class GfxResourceType
	{
		TEXTURE,
		BUFFER,
		SAMPLER
	};

	struct GfxMaterialResourceSetOperation
	{
		void* resource = nullptr;
		GfxResourceType type = GfxResourceType::TEXTURE;
	};

	struct GfxMaterialBufferCopyOperation
	{
		uint8_t* data = nullptr;
		
		~GfxMaterialBufferCopyOperation()
		{
			delete[] data;
		}
	};

	typedef std::unordered_map<PachaId, GfxMaterialBufferCopyOperation> GfxBufferCopyOperations;
	typedef std::unordered_map<PachaId, GfxMaterialResourceSetOperation> GfxResourceSetOperations;
	typedef std::unordered_map<PachaId, std::unordered_map<PachaId, GfxMaterialBufferCopyOperation>> GfxBufferMemberCopyOperations;
	
	class GfxHwdMaterialBase
	{
	public:
		GfxHwdMaterialBase(GfxTechnique* technique);
		virtual ~GfxHwdMaterialBase() {}

	protected:
		GfxTechnique* m_Technique = nullptr;
		std::unordered_map<PachaId, GfxConstantBuffer> m_ConstantBuffers;
		std::unordered_map<PachaId, GfxDynamicConstantBuffer> m_DynamicConstantBuffers;

		bool m_IsDirty = false;
		GfxResourceSetOperations m_ResourceSetOperation;
		GfxBufferCopyOperations m_ConstantBufferCopyOperation;
		GfxBufferMemberCopyOperations m_ConstantBufferMemberCopyOperation;
	};
}
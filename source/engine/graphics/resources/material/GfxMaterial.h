#pragma once
#include "core/Assert.h"
#include "GfxHwdMaterial.h"
#include "collections/SafeQueue.h"

#include <vector>
#include <unordered_map>
#include <utilities/PachaId.h>
#include <glm/gtc/type_aligned.hpp>

#if PACHA_EDITOR
#define SAVE_TEXTURE(textureId, texture) m_Textures[textureId] = texture
#else
#define SAVE_TEXTURE(textureId, texture)
#endif

namespace Pacha
{
#if PACHA_EDITOR
	union GfxEditorUniformValue
	{
		bool				_bool;
		glm::packed_bvec2	_bool2;
		glm::packed_bvec3	_bool3;
		glm::packed_bvec4	_bool4;

		int32_t				_int;
		glm::packed_ivec2	_int2;
		glm::packed_ivec3	_int3;
		glm::packed_ivec4	_int4;

		uint32_t			_uint;
		glm::packed_uvec2	_uint2;
		glm::packed_uvec3	_uint3;
		glm::packed_uvec4	_uint4;

		float				_float;
		glm::packed_vec2	_float2;
		glm::packed_vec3	_float3;
		glm::packed_vec4	_float4;

		double				_double;
		glm::packed_dvec2	_double2;
		glm::packed_dvec3	_double3;
		glm::packed_dvec4	_double4;
	};
#endif

	EDITOR_ENUM(GfxRenderType,
		OPAQUE		= 1 << 0,
		ALPHA_TEST	= 1 << 1,
		TRANSPARENT = 1 << 2
	);

	class GfxMaterial
	{
	public:
		GfxMaterial(GfxTechnique* technique);
		~GfxMaterial();

		void SetName(const std::string& name);
		const std::string& GetName();

		void SetRenderType(GfxRenderType renderType);
		GfxRenderType GetRenderType();

		void SetRendererState(const GfxRendererState& rendererState);
		const GfxRendererState& GetRendererState();

		void SetBuffer(const PachaId& bufferId, GfxBuffer* buffer);
		void SetTexture(const PachaId& textureId, GfxTexture* texture);
		void SetSampler(const PachaId& samplerId, GfxSampler& sampler);

		void SetBool(const PachaId& bufferId, const PachaId& memberId, const bool value);
		void SetBool2(const PachaId& bufferId, const PachaId& memberId, const glm::packed_bvec2& value);
		void SetBool3(const PachaId& bufferId, const PachaId& memberId, const glm::packed_bvec3& value);
		void SetBool4(const PachaId& bufferId, const PachaId& memberId, const glm::packed_bvec4& value);

		void SetInt(const PachaId& bufferId, const PachaId& memberId, const int32_t value);
		void SetInt2(const PachaId& bufferId, const PachaId& memberId, const glm::packed_ivec2& value);
		void SetInt3(const PachaId& bufferId, const PachaId& memberId, const glm::packed_ivec3& value);
		void SetInt4(const PachaId& bufferId, const PachaId& memberId, const glm::packed_ivec4& value);

		void SetUint(const PachaId& bufferId, const PachaId& memberId, const uint32_t value);
		void SetUint2(const PachaId& bufferId, const PachaId& memberId, const glm::packed_uvec2& value);
		void SetUint3(const PachaId& bufferId, const PachaId& memberId, const glm::packed_uvec3& value);
		void SetUint4(const PachaId& bufferId, const PachaId& memberId, const glm::packed_uvec4& value);

		void SetFloat(const PachaId& bufferId, const PachaId& memberId, const float value);
		void SetFloat2(const PachaId& bufferId, const PachaId& memberId, const glm::packed_vec2& value);
		void SetFloat3(const PachaId& bufferId, const PachaId& memberId, const glm::packed_vec3& value);
		void SetFloat4(const PachaId& bufferId, const PachaId& memberId, const glm::packed_vec4& value);

		void SetDouble(const PachaId& bufferId, const PachaId& memberId, const double value);
		void SetDouble2(const PachaId& bufferId, const PachaId& memberId, const glm::packed_dvec2& value);
		void SetDouble3(const PachaId& bufferId, const PachaId& memberId, const glm::packed_dvec3& value);
		void SetDouble4(const PachaId& bufferId, const PachaId& memberId, const glm::packed_dvec4& value);

		uint64_t GetId() const;
		GfxTechnique* GetTechnique() const;
		GfxHwdMaterial* GetHwdMaterial() const;

		template<typename T>
		void CopyDataToConstantBuffer(const PachaId& bufferId, const T& value)
		{
			PACHA_ASSERT(m_Technique->HasConstantBuffer(bufferId), "Buffer does not exist in current technique");

			const GfxConstantBufferInfo& bufferInfo = m_Technique->GetConstantBufferInfo(bufferId);
			PACHA_ASSERT(bufferInfo.size == sizeof(T), "Value size does not match buffer size");

			GfxMaterialBufferCopyOperation& copyOperation = m_ConstantBufferCopyOperation[bufferId];
			if (copyOperation.data != nullptr)
				delete[] copyOperation.data;

			copyOperation.data = new uint8_t[bufferInfo.size];
			memcpy(copyOperation.data, &value, bufferInfo.size);
		}

		template<typename T>
		void CopyDataToConstantBufferMember(const PachaId& bufferId, const PachaId& memberId, const T& value)
		{
			const GfxBufferMemberInfo& memberInfo = m_Technique->GetConstantBufferMemberInfo(bufferId, memberId);
			PACHA_ASSERT(memberInfo.size == sizeof(T), "Member size doesn't match the supplied value size");

			GfxMaterialBufferCopyOperation& copyOperation = m_ConstantBufferMemberCopyOperation[bufferId][memberId];

			if (copyOperation.data != nullptr)
				delete[] copyOperation.data;

			copyOperation.data = new uint8_t[memberInfo.size];
			memcpy(copyOperation.data, &value, memberInfo.size);
		}

	private:
		std::string m_Name = {};
		GfxRendererState m_RendererState;
		GfxRenderType m_RenderType = GfxRenderType::OPAQUE;

		uint16_t m_Id = 0;
		static SafeQueue<uint16_t> m_FreeIndexList;
		static std::atomic_uint16_t m_MaterialCounter;

		GfxTechnique* m_Technique = nullptr;
		GfxHwdMaterial* m_HwdMaterial = nullptr;

		GfxResourceSetOperations m_ResourceSetOperation;
		GfxBufferCopyOperations m_ConstantBufferCopyOperation;
		GfxBufferMemberCopyOperations m_ConstantBufferMemberCopyOperation;

#if PACHA_EDITOR
	public:
		bool GetBool(const PachaId& bufferId, const PachaId& memberId);
		const glm::packed_bvec2& GetBool2(const PachaId& bufferId, const PachaId& memberId);
		const glm::packed_bvec3& GetBool3(const PachaId& bufferId, const PachaId& memberId);
		const glm::packed_bvec4& GetBool4(const PachaId& bufferId, const PachaId& memberId);

		int32_t GetInt(const PachaId& bufferId, const PachaId& memberId);
		const glm::packed_ivec2& GetInt2(const PachaId& bufferId, const PachaId& memberId);
		const glm::packed_ivec3& GetInt3(const PachaId& bufferId, const PachaId& memberId);
		const glm::packed_ivec4& GetInt4(const PachaId& bufferId, const PachaId& memberId);

		uint32_t GetUint(const PachaId& bufferId, const PachaId& memberId);
		const glm::packed_uvec2& GetUint2(const PachaId& bufferId, const PachaId& memberId);
		const glm::packed_uvec3& GetUint3(const PachaId& bufferId, const PachaId& memberId);
		const glm::packed_uvec4& GetUint4(const PachaId& bufferId, const PachaId& memberId);

		float GetFloat(const PachaId& bufferId, const PachaId& memberId);
		const glm::packed_vec2& GetFloat2(const PachaId& bufferId, const PachaId& memberId);
		const glm::packed_vec3& GetFloat3(const PachaId& bufferId, const PachaId& memberId);
		const glm::packed_vec4& GetFloat4(const PachaId& bufferId, const PachaId& memberId);

		double GetDouble(const PachaId& bufferId, const PachaId& memberId);
		const glm::packed_dvec2& GetDouble2(const PachaId& bufferId, const PachaId& memberId);
		const glm::packed_dvec3& GetDouble3(const PachaId& bufferId, const PachaId& memberId);
		const glm::packed_dvec4& GetDouble4(const PachaId& bufferId, const PachaId& memberId);

		GfxTexture* GetTexture(const PachaId& textureId);

	protected:
		std::unordered_map<PachaId, GfxTexture*> m_Textures;
		std::unordered_map<PachaId, std::unordered_map<PachaId, GfxEditorUniformValue>> m_UniformValues;
#endif
	};
}
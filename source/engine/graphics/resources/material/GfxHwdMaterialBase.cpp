#include "GfxHwdMaterialBase.h"
#include "graphics/swapchain/GfxSwapchain.h"

namespace Pacha
{
	GfxHwdMaterialBase::GfxHwdMaterialBase(GfxTechnique* technique)
		: m_Technique(technique)
	{
		GfxSwapchain& gfxSwapchain = GfxSwapchain::GetMainSwapchain();
		for (auto const& [id, bufferInfo] : m_Technique->GetConstantBuffers())
		{
			if (bufferInfo.type & GfxBufferDescriptorType_CONSTANT)
			{
				if (bufferInfo.type == GfxBufferDescriptorType_CONSTANT_STATIC)
					m_ConstantBuffers.emplace(id, GfxConstantBuffer(bufferInfo.size, nullptr));
				else
					m_DynamicConstantBuffers.emplace(id, GfxDynamicConstantBuffer(bufferInfo.size, gfxSwapchain.GetBackBufferCount()));
			}
		}
	}
}

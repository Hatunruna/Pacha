#pragma once
#if RENDERER_VK
#include "GfxHwdMaterialVK.h"
#endif

namespace Pacha
{
#if RENDERER_VK
	using GfxHwdMaterial = GfxHwdMaterialVK;
#endif
}
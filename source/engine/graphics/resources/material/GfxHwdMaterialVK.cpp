#include "GfxHwdMaterialVK.h"

namespace Pacha
{
	ThreadDescriptorSetAllocators GfxHwdMaterialVK::s_ThreadDescriptorAllocators;

	GfxHwdMaterialVK::GfxHwdMaterialVK(GfxTechnique* technique)
		: GfxHwdMaterialBase(technique)
	{
		const std::thread::id threadId = std::this_thread::get_id();
		if (!s_ThreadDescriptorAllocators.count(threadId))
		{
			DescriptorPoolMultipliers poolMultipliers = {};
			poolMultipliers[VK_DESCRIPTOR_TYPE_SAMPLER] = 3;
			poolMultipliers[VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE] = 3;
			poolMultipliers[VK_DESCRIPTOR_TYPE_STORAGE_IMAGE] = 3;
			poolMultipliers[VK_DESCRIPTOR_TYPE_STORAGE_BUFFER] = 3;
			poolMultipliers[VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER] = 3;
			poolMultipliers[VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER] = 3;
			poolMultipliers[VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER] = 3;
			poolMultipliers[VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR] = 1;

			s_ThreadDescriptorAllocators[threadId].Initialize(poolMultipliers, 10);
		}

		//GfxSwapchain& swapchain = ServiceLocator::GetGfxSwapchain();
		//DescriptorSetAllocatorVK& setAllocator = s_ThreadDescriptorAllocators[threadId];
		//
		//for (size_t i = 0; i < swapchain.GetBackBufferCount(); ++i)
		//	m_DescriptorSets.push_back(setAllocator.AllocateDescriptorSet(m_Technique->GetDescriptorSetLayout()));
	}

	GfxHwdMaterialVK::~GfxHwdMaterialVK()
	{
		//Delayed free of desciptor sets
	}
}

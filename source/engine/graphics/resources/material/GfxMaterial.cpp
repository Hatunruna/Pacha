#include "GfxMaterial.h"

#if PACHA_EDITOR
#define SAVE_UNIFORM_VALUE(bufferId, memberId, value, type) m_UniformValues[bufferId][memberId]._##type = value
#define SAVE_TEXTURE(textureId, texture) m_Textures[textureId] = texture
#define GET_UNIFORM_VALUE(bufferId, memberId, type) m_UniformValues[bufferId][memberId]._##type
#else
#define SAVE_UNIFORM_VALUE(bufferId, memberId, value, type)
#define SAVE_TEXTURE(textureId, texture)
#endif

namespace Pacha
{
	SafeQueue<uint16_t> GfxMaterial::m_FreeIndexList;
	std::atomic_uint16_t GfxMaterial::m_MaterialCounter;

	GfxMaterial::GfxMaterial(GfxTechnique* technique)
		: m_Technique(technique)
	{
		if (!m_FreeIndexList.TryFront(m_Id))
			m_Id = m_MaterialCounter.fetch_add(1);
		
		PACHA_ASSERT(m_MaterialCounter < UINT16_MAX, "Amount of materials exceeded");
		m_HwdMaterial = new GfxHwdMaterial(technique);
	}

	GfxMaterial::~GfxMaterial()
	{
		m_FreeIndexList.Push(m_Id);
		delete m_HwdMaterial;
	}

	void GfxMaterial::SetName(const std::string& name)
	{
		m_Name = name;
	}

	const std::string& GfxMaterial::GetName()
	{
		return m_Name;
	}

	void GfxMaterial::SetRenderType(GfxRenderType renderType)
	{
		m_RenderType = renderType;
	}

	GfxRenderType GfxMaterial::GetRenderType()
	{
		return m_RenderType;
	}

	void GfxMaterial::SetRendererState(const GfxRendererState& rendererState)
	{
		m_RendererState = rendererState;
	}

	const GfxRendererState& GfxMaterial::GetRendererState()
	{
		return m_RendererState;
	}

	void GfxMaterial::SetBuffer(const PachaId& bufferId, GfxBuffer* buffer)
	{
		PACHA_ASSERT(m_Technique->HasBuffer(bufferId), "Buffer not found in technique");
		m_ResourceSetOperation[bufferId] = { buffer, GfxResourceType::BUFFER };
	}

	void GfxMaterial::SetTexture(const PachaId& textureId, GfxTexture* texture)
	{
		PACHA_ASSERT(m_Technique->HasTexture(textureId), "Texture not found in technique");
		m_ResourceSetOperation[textureId] = { texture, GfxResourceType::TEXTURE };
		SAVE_TEXTURE(textureId, texture);
	}

	void GfxMaterial::SetSampler(const PachaId& samplerId, GfxSampler& sampler)
	{
		PACHA_ASSERT(m_Technique->HasSampler(samplerId), "Sampler not found in technique");
		m_ResourceSetOperation[samplerId] = { &sampler, GfxResourceType::SAMPLER };
	}

	void GfxMaterial::SetBool(const PachaId& bufferId, const PachaId& memberId, const bool value)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		SAVE_UNIFORM_VALUE(bufferId, memberId, value, bool);
		CopyDataToConstantBufferMember(bufferId, memberId, value);
	}

	void GfxMaterial::SetBool2(const PachaId& bufferId, const PachaId& memberId, const glm::packed_bvec2& value)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		SAVE_UNIFORM_VALUE(bufferId, memberId, value, bool2);
		CopyDataToConstantBufferMember(bufferId, memberId, value);
	}

	void GfxMaterial::SetBool3(const PachaId& bufferId, const PachaId& memberId, const glm::packed_bvec3& value)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		SAVE_UNIFORM_VALUE(bufferId, memberId, value, bool3);
		CopyDataToConstantBufferMember(bufferId, memberId, value);
	}

	void GfxMaterial::SetBool4(const PachaId& bufferId, const PachaId& memberId, const glm::packed_bvec4& value)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		SAVE_UNIFORM_VALUE(bufferId, memberId, value, bool4);
		CopyDataToConstantBufferMember(bufferId, memberId, value);
	}

	void GfxMaterial::SetInt(const PachaId& bufferId, const PachaId& memberId, const int32_t value)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		SAVE_UNIFORM_VALUE(bufferId, memberId, value, int);
		CopyDataToConstantBufferMember(bufferId, memberId, value);
	}

	void GfxMaterial::SetInt2(const PachaId& bufferId, const PachaId& memberId, const glm::packed_ivec2& value)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		SAVE_UNIFORM_VALUE(bufferId, memberId, value, int2);
		CopyDataToConstantBufferMember(bufferId, memberId, value);
	}

	void GfxMaterial::SetInt3(const PachaId& bufferId, const PachaId& memberId, const glm::packed_ivec3& value)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		SAVE_UNIFORM_VALUE(bufferId, memberId, value, int3);
		CopyDataToConstantBufferMember(bufferId, memberId, value);
	}

	void GfxMaterial::SetInt4(const PachaId& bufferId, const PachaId& memberId, const glm::packed_ivec4& value)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		SAVE_UNIFORM_VALUE(bufferId, memberId, value, int4);
		CopyDataToConstantBufferMember(bufferId, memberId, value);
	}

	void GfxMaterial::SetUint(const PachaId& bufferId, const PachaId& memberId, const uint32_t value)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		SAVE_UNIFORM_VALUE(bufferId, memberId, value, uint);
		CopyDataToConstantBufferMember(bufferId, memberId, value);
	}

	void GfxMaterial::SetUint2(const PachaId& bufferId, const PachaId& memberId, const glm::packed_uvec2& value)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		SAVE_UNIFORM_VALUE(bufferId, memberId, value, uint2);
		CopyDataToConstantBufferMember(bufferId, memberId, value);
	}

	void GfxMaterial::SetUint3(const PachaId& bufferId, const PachaId& memberId, const glm::packed_uvec3& value)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		SAVE_UNIFORM_VALUE(bufferId, memberId, value, uint3);
		CopyDataToConstantBufferMember(bufferId, memberId, value);
	}

	void GfxMaterial::SetUint4(const PachaId& bufferId, const PachaId& memberId, const glm::packed_uvec4& value)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		SAVE_UNIFORM_VALUE(bufferId, memberId, value, uint4);
		CopyDataToConstantBufferMember(bufferId, memberId, value);
	}

	void GfxMaterial::SetFloat(const PachaId& bufferId, const PachaId& memberId, const float value)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		SAVE_UNIFORM_VALUE(bufferId, memberId, value, float);
		CopyDataToConstantBufferMember(bufferId, memberId, value);
	}

	void GfxMaterial::SetFloat2(const PachaId& bufferId, const PachaId& memberId, const glm::packed_vec2& value)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		SAVE_UNIFORM_VALUE(bufferId, memberId, value, float2);
		CopyDataToConstantBufferMember(bufferId, memberId, value);
	}

	void GfxMaterial::SetFloat3(const PachaId& bufferId, const PachaId& memberId, const glm::packed_vec3& value)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		SAVE_UNIFORM_VALUE(bufferId, memberId, value, float3);
		CopyDataToConstantBufferMember(bufferId, memberId, value);
	}

	void GfxMaterial::SetFloat4(const PachaId& bufferId, const PachaId& memberId, const glm::packed_vec4& value)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		SAVE_UNIFORM_VALUE(bufferId, memberId, value, float4);
		CopyDataToConstantBufferMember(bufferId, memberId, value);
	}

	void GfxMaterial::SetDouble(const PachaId& bufferId, const PachaId& memberId, const double value)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		SAVE_UNIFORM_VALUE(bufferId, memberId, value, double);
		CopyDataToConstantBufferMember(bufferId, memberId, value);
	}

	void GfxMaterial::SetDouble2(const PachaId& bufferId, const PachaId& memberId, const glm::packed_dvec2& value)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		SAVE_UNIFORM_VALUE(bufferId, memberId, value, double2);
		CopyDataToConstantBufferMember(bufferId, memberId, value);
	}

	void GfxMaterial::SetDouble3(const PachaId& bufferId, const PachaId& memberId, const glm::packed_dvec3& value)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		SAVE_UNIFORM_VALUE(bufferId, memberId, value, double3);
		CopyDataToConstantBufferMember(bufferId, memberId, value);
	}

	void GfxMaterial::SetDouble4(const PachaId& bufferId, const PachaId& memberId, const glm::packed_dvec4& value)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		SAVE_UNIFORM_VALUE(bufferId, memberId, value, double4);
		CopyDataToConstantBufferMember(bufferId, memberId, value);
	}
	
	uint64_t GfxMaterial::GetId() const
	{
		return static_cast<uint64_t>(m_Id);
	}

	GfxTechnique* GfxMaterial::GetTechnique() const
	{
		return m_Technique;
	}

	GfxHwdMaterial* GfxMaterial::GetHwdMaterial() const
	{
		return m_HwdMaterial;
	}

#if PACHA_EDITOR
	bool GfxMaterial::GetBool(const PachaId& bufferId, const PachaId& memberId)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		return GET_UNIFORM_VALUE(bufferId, memberId, bool);
	}

	const glm::packed_bvec2& GfxMaterial::GetBool2(const PachaId& bufferId, const PachaId& memberId)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		return GET_UNIFORM_VALUE(bufferId, memberId, bool2);
	}

	const glm::packed_bvec3& GfxMaterial::GetBool3(const PachaId& bufferId, const PachaId& memberId)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		return GET_UNIFORM_VALUE(bufferId, memberId, bool3);
	}

	const glm::packed_bvec4& GfxMaterial::GetBool4(const PachaId& bufferId, const PachaId& memberId)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		return GET_UNIFORM_VALUE(bufferId, memberId, bool4);
	}

	int32_t GfxMaterial::GetInt(const PachaId& bufferId, const PachaId& memberId)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		return GET_UNIFORM_VALUE(bufferId, memberId, int);
	}

	const glm::packed_ivec2& GfxMaterial::GetInt2(const PachaId& bufferId, const PachaId& memberId)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		return GET_UNIFORM_VALUE(bufferId, memberId, int2);
	}

	const glm::packed_ivec3& GfxMaterial::GetInt3(const PachaId& bufferId, const PachaId& memberId)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		return GET_UNIFORM_VALUE(bufferId, memberId, int3);
	}

	const glm::packed_ivec4& GfxMaterial::GetInt4(const PachaId& bufferId, const PachaId& memberId)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		return GET_UNIFORM_VALUE(bufferId, memberId, int4);
	}

	uint32_t GfxMaterial::GetUint(const PachaId& bufferId, const PachaId& memberId)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		return GET_UNIFORM_VALUE(bufferId, memberId, uint);
	}

	const glm::packed_uvec2& GfxMaterial::GetUint2(const PachaId& bufferId, const PachaId& memberId)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		return GET_UNIFORM_VALUE(bufferId, memberId, uint2);
	}

	const glm::packed_uvec3& GfxMaterial::GetUint3(const PachaId& bufferId, const PachaId& memberId)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		return GET_UNIFORM_VALUE(bufferId, memberId, uint3);
	}

	const glm::packed_uvec4& GfxMaterial::GetUint4(const PachaId& bufferId, const PachaId& memberId)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		return GET_UNIFORM_VALUE(bufferId, memberId, uint4);
	}

	float GfxMaterial::GetFloat(const PachaId& bufferId, const PachaId& memberId)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		return GET_UNIFORM_VALUE(bufferId, memberId, float);
	}

	const glm::packed_vec2& GfxMaterial::GetFloat2(const PachaId& bufferId, const PachaId& memberId)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		return GET_UNIFORM_VALUE(bufferId, memberId, float2);
	}

	const glm::packed_vec3& GfxMaterial::GetFloat3(const PachaId& bufferId, const PachaId& memberId)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		return GET_UNIFORM_VALUE(bufferId, memberId, float3);
	}

	const glm::packed_vec4& GfxMaterial::GetFloat4(const PachaId& bufferId, const PachaId& memberId)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		return GET_UNIFORM_VALUE(bufferId, memberId, float4);
	}

	double GfxMaterial::GetDouble(const PachaId& bufferId, const PachaId& memberId)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		return GET_UNIFORM_VALUE(bufferId, memberId, double);
	}

	const glm::packed_dvec2& GfxMaterial::GetDouble2(const PachaId& bufferId, const PachaId& memberId)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		return GET_UNIFORM_VALUE(bufferId, memberId, double2);
	}

	const glm::packed_dvec3& GfxMaterial::GetDouble3(const PachaId& bufferId, const PachaId& memberId)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		return GET_UNIFORM_VALUE(bufferId, memberId, double3);
	}

	const glm::packed_dvec4& GfxMaterial::GetDouble4(const PachaId& bufferId, const PachaId& memberId)
	{
		PACHA_ASSERT(m_Technique->ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		return GET_UNIFORM_VALUE(bufferId, memberId, double4);
	}
	
	GfxTexture* GfxMaterial::GetTexture(const PachaId& textureId)
	{
		PACHA_ASSERT(m_Technique->HasTexture(textureId), "Texture does not exist in current material");
		return m_Textures[textureId];
	}
#endif
}

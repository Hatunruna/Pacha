#pragma once
#if RENDERER_VK
#include "graphics/resources/texture/GfxTextureVK.h"
#endif

namespace Pacha
{
#if RENDERER_VK
	using GfxTexture = GfxTextureVK;
#endif
}
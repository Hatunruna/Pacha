#pragma once
#include "core/Log.h"
#include "core/Assert.h"

#include <unordered_map>

namespace Pacha
{
	enum class GfxPixelFormat
	{
		UNDEFINED,

		R8_UINT,
		R8_SINT,
		R8_UNORM,
		R8_SNORM,

		RG8_UINT,
		RG8_SINT,
		RG8_UNORM,
		RG8_SNORM,

		RGBA8_UINT,
		RGBA8_SINT,
		RGBA8_SNORM,
		RGBA8_UNORM,
		RGBA8_SRGB,

		BGRA8_UINT,
		BGRA8_SINT,
		BGRA8_SNORM,
		BGRA8_UNORM,
		BGRA8_SRGB,

		R10G10B10A2_UNORM,

		R16_UINT,
		R16_SINT,
		R16_UNORM,
		R16_SNORM,
		R16_FLOAT,

		RG16_UINT,
		RG16_SINT,
		RG16_UNORM,
		RG16_SNORM,
		RG16_FLOAT,

		RGBA16_UINT,
		RGBA16_SINT,
		RGBA16_UNORM,
		RGBA16_SNORM,
		RGBA16_FLOAT,

		R32_UINT,
		R32_SINT,
		R32_FLOAT,

		RG32_UINT,
		RG32_SINT,
		RG32_FLOAT,

		RGBA32_UINT,
		RGBA32_SINT,
		RGBA32_FLOAT,

		BC1_RGB_UNORM,
		BC1_RGB_SRGB,

		BC1_RGBA_UNORM,
		BC1_RGBA_SRGB,

		BC2_UNORM,
		BC2_SRGB,

		BC3_UNORM,
		BC3_SRGB,

		BC4_UNORM,
		BC4_SNORM,

		BC5_UNORM,
		BC5_SNORM,

		BC6H_UF16,
		BC6H_SF16,

		BC7_UNORM,
		BC7_SRGB,

		D32_SFLOAT_S8_UINT,
		D32_SFLOAT,
		D24_UNORM_S8_UINT,
		D16_UNORM,
	};

	inline bool IsSRGB(const GfxPixelFormat format)
	{
		switch (format)
		{
			case GfxPixelFormat::RGBA8_SRGB:
			case GfxPixelFormat::BGRA8_SRGB:
			case GfxPixelFormat::BC1_RGB_SRGB:
			case GfxPixelFormat::BC1_RGBA_SRGB:
			case GfxPixelFormat::BC2_SRGB:
			case GfxPixelFormat::BC3_SRGB:
			case GfxPixelFormat::BC7_SRGB:
				return true;

			default:
				return false;
		}
	}

	inline bool SupportsSRGB(const GfxPixelFormat format)
	{
		switch (format)
		{
			case GfxPixelFormat::RGBA8_UNORM:
			case GfxPixelFormat::BGRA8_UNORM:
			case GfxPixelFormat::BC1_RGB_UNORM:
			case GfxPixelFormat::BC1_RGBA_UNORM:
			case GfxPixelFormat::BC2_UNORM:
			case GfxPixelFormat::BC3_UNORM:
			case GfxPixelFormat::BC7_UNORM:
				return true;

			default:
				return false;
		}
	}

	inline GfxPixelFormat GetSRGB(const GfxPixelFormat format)
	{
		if (!IsSRGB(format) && SupportsSRGB(format))
		{
			GfxPixelFormat returnFormat = static_cast<GfxPixelFormat>(static_cast<uint32_t>(format) + 1);
			PACHA_ASSERT(IsSRGB(returnFormat), "Return pixel format is not SRGB.");
			return returnFormat;
		}
		else
			return format;
	}

	inline bool IsDepthPixelFormat(const GfxPixelFormat format)
	{
		return format >= GfxPixelFormat::D32_SFLOAT_S8_UINT;
	}

	inline bool PixelFormatHasStencil(const GfxPixelFormat format)
	{
		return format == GfxPixelFormat::D32_SFLOAT_S8_UINT
			|| format == GfxPixelFormat::D24_UNORM_S8_UINT;
	}

	inline bool IsHDRPixelFormat(const GfxPixelFormat format)
	{
		return format >= GfxPixelFormat::R10G10B10A2_UNORM && format <= GfxPixelFormat::RGBA32_FLOAT;
	}

	inline bool IsBlockCompressedPixelFormat(const GfxPixelFormat format)
	{
		return format >= GfxPixelFormat::BC1_RGB_UNORM && format <= GfxPixelFormat::BC7_SRGB;
	}

	inline size_t GetPixelSize(const GfxPixelFormat format)
	{
		switch (format)
		{
			default:
				LOGCRITICAL("Pixel format not registered on this function");
				return 0;

			case GfxPixelFormat::R8_UINT:
			case GfxPixelFormat::R8_SINT:
			case GfxPixelFormat::R8_UNORM:
			case GfxPixelFormat::R8_SNORM:
				return 1;

			case GfxPixelFormat::RG8_UINT:
			case GfxPixelFormat::RG8_SINT:
			case GfxPixelFormat::RG8_UNORM:
			case GfxPixelFormat::RG8_SNORM:

			case GfxPixelFormat::R16_UINT:
			case GfxPixelFormat::R16_SINT:
			case GfxPixelFormat::R16_UNORM:
			case GfxPixelFormat::R16_SNORM:
			case GfxPixelFormat::R16_FLOAT:

			case GfxPixelFormat::D16_UNORM:
				return 2;

			case GfxPixelFormat::RGBA8_UINT:
			case GfxPixelFormat::RGBA8_SINT:
			case GfxPixelFormat::RGBA8_UNORM:
			case GfxPixelFormat::RGBA8_SNORM:
			case GfxPixelFormat::RGBA8_SRGB:

			case GfxPixelFormat::BGRA8_UINT:
			case GfxPixelFormat::BGRA8_SINT:
			case GfxPixelFormat::BGRA8_UNORM:
			case GfxPixelFormat::BGRA8_SNORM:
			case GfxPixelFormat::BGRA8_SRGB:

			case GfxPixelFormat::R10G10B10A2_UNORM:

			case GfxPixelFormat::RG16_UINT:
			case GfxPixelFormat::RG16_SINT:
			case GfxPixelFormat::RG16_UNORM:
			case GfxPixelFormat::RG16_SNORM:
			case GfxPixelFormat::RG16_FLOAT:

			case GfxPixelFormat::R32_UINT:
			case GfxPixelFormat::R32_SINT:
			case GfxPixelFormat::R32_FLOAT:

			case GfxPixelFormat::D32_SFLOAT:
			case GfxPixelFormat::D24_UNORM_S8_UINT:
				return 4;

			case GfxPixelFormat::D32_SFLOAT_S8_UINT:
				return 5;

			case GfxPixelFormat::RGBA16_UINT:
			case GfxPixelFormat::RGBA16_SINT:
			case GfxPixelFormat::RGBA16_UNORM:
			case GfxPixelFormat::RGBA16_SNORM:
			case GfxPixelFormat::RGBA16_FLOAT:

			case GfxPixelFormat::RG32_UINT:
			case GfxPixelFormat::RG32_SINT:
			case GfxPixelFormat::RG32_FLOAT:
				return 8;

			case GfxPixelFormat::RGBA32_UINT:
			case GfxPixelFormat::RGBA32_SINT:
			case GfxPixelFormat::RGBA32_FLOAT:
				return 16;
		}
	}

	inline size_t GetCompressedBlockSize(const GfxPixelFormat format)
	{
		switch (format)
		{
			default:
				LOGCRITICAL("Pixel format not registered on this function");
				return 0;

			case GfxPixelFormat::BC1_RGB_UNORM:
			case GfxPixelFormat::BC1_RGB_SRGB:

			case GfxPixelFormat::BC1_RGBA_UNORM:
			case GfxPixelFormat::BC1_RGBA_SRGB:

			case GfxPixelFormat::BC4_UNORM:
			case GfxPixelFormat::BC4_SNORM:
				return 8;

			case GfxPixelFormat::BC2_UNORM:
			case GfxPixelFormat::BC2_SRGB:

			case GfxPixelFormat::BC3_UNORM:
			case GfxPixelFormat::BC3_SRGB:

			case GfxPixelFormat::BC5_UNORM:
			case GfxPixelFormat::BC5_SNORM:

			case GfxPixelFormat::BC6H_UF16:
			case GfxPixelFormat::BC6H_SF16:

			case GfxPixelFormat::BC7_UNORM:
			case GfxPixelFormat::BC7_SRGB:
				return 16;
		}
	}

	inline size_t GetChannelCount(const GfxPixelFormat format)
	{
		switch (format)
		{
			default:
				LOGCRITICAL("Pixel format not registered on this function");
				return 0;

			case GfxPixelFormat::R8_UINT:
			case GfxPixelFormat::R8_SINT:
			case GfxPixelFormat::R8_UNORM:
			case GfxPixelFormat::R8_SNORM:

			case GfxPixelFormat::R16_UINT:
			case GfxPixelFormat::R16_SINT:
			case GfxPixelFormat::R16_UNORM:
			case GfxPixelFormat::R16_SNORM:
			case GfxPixelFormat::R16_FLOAT:

			case GfxPixelFormat::R32_UINT:
			case GfxPixelFormat::R32_SINT:
			case GfxPixelFormat::R32_FLOAT:

			case GfxPixelFormat::D16_UNORM:
			case GfxPixelFormat::D32_SFLOAT:
				return 1;

			case GfxPixelFormat::RG8_UINT:
			case GfxPixelFormat::RG8_SINT:
			case GfxPixelFormat::RG8_UNORM:
			case GfxPixelFormat::RG8_SNORM:

			case GfxPixelFormat::RG16_UINT:
			case GfxPixelFormat::RG16_SINT:
			case GfxPixelFormat::RG16_UNORM:
			case GfxPixelFormat::RG16_SNORM:
			case GfxPixelFormat::RG16_FLOAT:
			
			case GfxPixelFormat::RG32_UINT:
			case GfxPixelFormat::RG32_SINT:
			case GfxPixelFormat::RG32_FLOAT:

			case GfxPixelFormat::D24_UNORM_S8_UINT:
			case GfxPixelFormat::D32_SFLOAT_S8_UINT:
				return 2;

			case GfxPixelFormat::RGBA8_UINT:
			case GfxPixelFormat::RGBA8_SINT:
			case GfxPixelFormat::RGBA8_UNORM:
			case GfxPixelFormat::RGBA8_SNORM:
			case GfxPixelFormat::RGBA8_SRGB:

			case GfxPixelFormat::BGRA8_UINT:
			case GfxPixelFormat::BGRA8_SINT:
			case GfxPixelFormat::BGRA8_UNORM:
			case GfxPixelFormat::BGRA8_SNORM:
			case GfxPixelFormat::BGRA8_SRGB:

			case GfxPixelFormat::RGBA16_UINT:
			case GfxPixelFormat::RGBA16_SINT:
			case GfxPixelFormat::RGBA16_UNORM:
			case GfxPixelFormat::RGBA16_SNORM:
			case GfxPixelFormat::RGBA16_FLOAT:

			case GfxPixelFormat::RGBA32_UINT:
			case GfxPixelFormat::RGBA32_SINT:
			case GfxPixelFormat::RGBA32_FLOAT:

			case GfxPixelFormat::R10G10B10A2_UNORM:
				return 4;
		}
	} 
}
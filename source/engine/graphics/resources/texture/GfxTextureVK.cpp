#include "GfxTextureVK.h"

#include "core/Log.h"
#include "utilities/Hash.h"
#include "graphics/device/GfxDevice.h"
#include "graphics/utilities/GfxMemoryManager.h"

namespace Pacha
{
	GfxTextureVK::GfxTextureVK(const GfxTextureDescriptor& descriptor, const std::shared_ptr<uint8_t>& data, const GfxResourceState& afterUploadState)
		: GfxTextureBase(descriptor, data)
	{
		m_HasOwnership = true;
		GfxDevice& gfxDevice = GfxDevice::GetInstance();
		const VmaAllocator& allocator = gfxDevice.GetMemoryAllocator();

		VkImageUsageFlags usageFlags = GetVkUsageFlags(m_Descriptor.flags);
		bool autoResolveMSAA = m_Descriptor.msaa > GfxMSAA::X1 && m_Descriptor.flags & GfxTextureFlags_AUTO_RESOLVE_MSAA;

		VkImageCreateInfo imageCreateInfo = {};
		imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		imageCreateInfo.imageType = GetVkImageType(m_Descriptor.type);
		imageCreateInfo.extent.width = m_Descriptor.width;
		imageCreateInfo.extent.height = m_Descriptor.height;
		imageCreateInfo.extent.depth = m_Descriptor.depth;
		imageCreateInfo.mipLevels = m_Descriptor.mips;
		imageCreateInfo.arrayLayers = m_Descriptor.slices;
		imageCreateInfo.format = GetVkFormat(m_Descriptor.format);
		imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
		imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		imageCreateInfo.usage = usageFlags;
		imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		imageCreateInfo.samples = autoResolveMSAA ? VK_SAMPLE_COUNT_1_BIT : GetVkMSAA(m_Descriptor.msaa);
		imageCreateInfo.flags = GetVkImageCreateFlags(m_Descriptor.type);

		VmaAllocationCreateInfo allocationInfo = {};
		allocationInfo.usage = VMA_MEMORY_USAGE_AUTO;
		allocationInfo.flags = VMA_ALLOCATION_CREATE_STRATEGY_BEST_FIT_BIT | VMA_ALLOCATION_CREATE_DEDICATED_MEMORY_BIT;

		VkResult result = VK_SUCCESS;
		if ((result = vmaCreateImage(allocator, &imageCreateInfo, &allocationInfo, &m_Image, &m_Allocation, nullptr)) != VK_SUCCESS)
			LOGCRITICAL("Failed to create texture: " << GfxDeviceVK::GetErrorString(result));

		CreateViews(m_Image, m_Views);

		if (autoResolveMSAA)
		{
			const VkImageUsageFlags kAttchmentMask = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;

			imageCreateInfo.samples = GetVkMSAA(m_Descriptor.msaa);
			imageCreateInfo.usage = VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | (usageFlags & kAttchmentMask);

#if PLATFORM_ANDROID
			allocationInfo.usage = VMA_MEMORY_USAGE_GPU_LAZILY_ALLOCATED;
			allocationInfo.requiredFlags = VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT;
#endif

			if ((result = vmaCreateImage(allocator, &imageCreateInfo, &allocationInfo, &m_MultisampledImage, &m_MultisampledAllocation, nullptr)) != VK_SUCCESS)
				LOGCRITICAL("Failed to create resolved texture: " << GfxDeviceVK::GetErrorString(result));

			CreateViews(m_MultisampledImage, m_MultisampledViews);
		}

		m_Hash = Hash(m_Image);
		if (m_Descriptor.flags & GfxTextureFlags_COPY_DESTINATION && data)
			GfxMemoryManager::GetInstance().AddTextureUpload(this, data, GfxResourceState::UNDEFINED, afterUploadState);
	}

	GfxTextureVK::~GfxTextureVK()
	{
		GfxMemoryManager::GetInstance().AddTextureToDelete(*this);
	}

	void GfxTextureVK::CreateViews(const VkImage& image, std::vector<VkImageView>& views)
	{
		GfxDevice& gfxDevice = GfxDevice::GetInstance();
		const VkDevice& device = gfxDevice.GetVkDevice();

		uint32_t mips = 1;
		uint32_t slices = 1;
		bool createAllViews = (m_Descriptor.flags & GfxTextureFlags_BIND_AS_RENDER_TARGET && m_Descriptor.flags & GfxTextureFlags_RENDER_INTO_SUB_RESOURCES) &&
			(m_Descriptor.slices > 1 || m_Descriptor.mips > 1);

		if (createAllViews)
		{
			mips = m_Descriptor.mips;
			slices = m_Descriptor.slices;
			views.resize(m_Descriptor.slices * m_Descriptor.mips);
		}
		else
			views.resize(1);

		VkResult result = VK_SUCCESS;
		for (uint32_t i = 0; i < slices; ++i)
		{
			for (uint32_t j = 0; j < mips; ++j)
			{
				VkImageViewCreateInfo viewCreateInfo = {};
				viewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
				viewCreateInfo.image = image;
				viewCreateInfo.viewType = GetVkImageViewType(m_Descriptor.type);
				viewCreateInfo.format = GetVkFormat(m_Descriptor.format);
				viewCreateInfo.subresourceRange.aspectMask = GetVkImageAspectMask(m_Descriptor.format);
				viewCreateInfo.subresourceRange.baseMipLevel = j;
				viewCreateInfo.subresourceRange.levelCount = VK_REMAINING_MIP_LEVELS;
				viewCreateInfo.subresourceRange.baseArrayLayer = i;
				viewCreateInfo.subresourceRange.layerCount = VK_REMAINING_ARRAY_LAYERS;
				viewCreateInfo.components = { VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A };

				uint32_t viewIndex = i * m_Descriptor.mips + j;
				if ((result = vkCreateImageView(device, &viewCreateInfo, nullptr, &views[viewIndex])) != VK_SUCCESS)
				{
					LOGCRITICAL("Failed to create image view: " << GfxDeviceVK::GetErrorString(result));
					return;
				}
			}
		}
	}

	void GfxTextureVK::SetFromNativeHandle(void* handle, void* multisampledHandle, const GfxTextureDescriptor& descriptor)
	{
		PACHA_ASSERT(!m_HasOwnership, "Can not set native handle to a managed texture");
		PACHA_ASSERT(handle, "Handle can not be null");

		if (!m_HasOwnership)
		{
			GfxMemoryManager::GetInstance().AddTextureToDelete(*this);
			m_Descriptor = descriptor;

			m_Image = reinterpret_cast<VkImage>(handle);
			m_Views.clear();
			CreateViews(m_Image, m_Views);
			m_Hash = Hash(m_Image);

			if (multisampledHandle)
			{
				m_MultisampledImage = reinterpret_cast<VkImage>(multisampledHandle);
				m_MultisampledViews.clear();
				CreateViews(m_MultisampledImage, m_MultisampledViews);
				HashCombine(m_Hash, m_MultisampledImage);
			}
		}
	}

	VkImage GfxTextureVK::GetImage()
	{
		return m_Image;
	}

	VkImageView GfxTextureVK::GetImageView(const uint32_t& mip, const uint32_t& slice)
	{
		PACHA_ASSERT(mip <= m_Descriptor.mips && slice <= m_Descriptor.slices, "Subresource index out of scope");
		const uint32_t index = (slice * m_Descriptor.mips) + mip;
		return m_Views[index];
	}

	VkImage GfxTextureVK::GetMultisampledImage()
	{
		return m_MultisampledImage;
	}

	VkImageView GfxTextureVK::GetMultisampledImageView(const uint32_t& mip, const uint32_t& slice)
	{
		PACHA_ASSERT(mip <= m_Descriptor.mips && slice <= m_Descriptor.slices, "Subresource index out of scope");
		const uint32_t index = (slice * m_Descriptor.mips) + mip;
		return m_MultisampledViews[index];
	}

	VkFormat GfxTextureVK::GetVkFormat(const GfxPixelFormat& format)
	{
		switch (format)
		{
			case GfxPixelFormat::R8_UINT:				return VK_FORMAT_R8_UINT;
			case GfxPixelFormat::R8_SINT:				return VK_FORMAT_R8_SINT;
			case GfxPixelFormat::R8_UNORM:				return VK_FORMAT_R8_UNORM;
			case GfxPixelFormat::R8_SNORM:				return VK_FORMAT_R8_SNORM;
			case GfxPixelFormat::RG8_UINT:				return VK_FORMAT_R8G8_UINT;
			case GfxPixelFormat::RG8_SINT:				return VK_FORMAT_R8G8_SINT;
			case GfxPixelFormat::RG8_UNORM:				return VK_FORMAT_R8G8_UNORM;
			case GfxPixelFormat::RG8_SNORM:				return VK_FORMAT_R8G8_SNORM;
			case GfxPixelFormat::RGBA8_UINT:			return VK_FORMAT_R8G8B8A8_UINT;
			case GfxPixelFormat::RGBA8_SINT:			return VK_FORMAT_R8G8B8A8_SINT;
			case GfxPixelFormat::RGBA8_UNORM:			return VK_FORMAT_R8G8B8A8_UNORM;
			case GfxPixelFormat::RGBA8_SNORM:			return VK_FORMAT_R8G8B8A8_SNORM;
			case GfxPixelFormat::RGBA8_SRGB:			return VK_FORMAT_R8G8B8A8_SRGB;
			case GfxPixelFormat::BGRA8_UINT:			return VK_FORMAT_B8G8R8A8_UINT;
			case GfxPixelFormat::BGRA8_SINT:			return VK_FORMAT_B8G8R8A8_SINT;
			case GfxPixelFormat::BGRA8_UNORM:			return VK_FORMAT_B8G8R8A8_UNORM;
			case GfxPixelFormat::BGRA8_SNORM:			return VK_FORMAT_B8G8R8A8_SNORM;
			case GfxPixelFormat::BGRA8_SRGB:			return VK_FORMAT_B8G8R8A8_SRGB;
			case GfxPixelFormat::R10G10B10A2_UNORM:		return VK_FORMAT_A2B10G10R10_UNORM_PACK32;
			case GfxPixelFormat::R16_UINT:				return VK_FORMAT_R16_UINT;
			case GfxPixelFormat::R16_SINT:				return VK_FORMAT_R16_SINT;
			case GfxPixelFormat::R16_UNORM:				return VK_FORMAT_R16_UNORM;
			case GfxPixelFormat::R16_SNORM:				return VK_FORMAT_R16_SNORM;
			case GfxPixelFormat::R16_FLOAT:				return VK_FORMAT_R16_SFLOAT;
			case GfxPixelFormat::RG16_UINT:				return VK_FORMAT_R16G16_UINT;
			case GfxPixelFormat::RG16_SINT:				return VK_FORMAT_R16G16_SINT;
			case GfxPixelFormat::RG16_UNORM:			return VK_FORMAT_R16G16_UNORM;
			case GfxPixelFormat::RG16_SNORM:			return VK_FORMAT_R16G16_SNORM;
			case GfxPixelFormat::RG16_FLOAT:			return VK_FORMAT_R16G16_SFLOAT;
			case GfxPixelFormat::RGBA16_UINT:			return VK_FORMAT_R16G16B16A16_UINT;
			case GfxPixelFormat::RGBA16_SINT:			return VK_FORMAT_R16G16B16A16_SINT;
			case GfxPixelFormat::RGBA16_UNORM:			return VK_FORMAT_R16G16B16A16_UNORM;
			case GfxPixelFormat::RGBA16_SNORM:			return VK_FORMAT_R16G16B16A16_SNORM;
			case GfxPixelFormat::RGBA16_FLOAT:			return VK_FORMAT_R16G16B16A16_SFLOAT;
			case GfxPixelFormat::R32_UINT:				return VK_FORMAT_R32_UINT;
			case GfxPixelFormat::R32_SINT:				return VK_FORMAT_R32_SINT;
			case GfxPixelFormat::R32_FLOAT:				return VK_FORMAT_R32_SFLOAT;
			case GfxPixelFormat::RG32_UINT:				return VK_FORMAT_R32G32_UINT;
			case GfxPixelFormat::RG32_SINT:				return VK_FORMAT_R32G32_SINT;
			case GfxPixelFormat::RG32_FLOAT:			return VK_FORMAT_R32G32_SFLOAT;
			case GfxPixelFormat::RGBA32_UINT:			return VK_FORMAT_R32G32B32A32_UINT;
			case GfxPixelFormat::RGBA32_SINT:			return VK_FORMAT_R32G32B32A32_SINT;
			case GfxPixelFormat::RGBA32_FLOAT:			return VK_FORMAT_R32G32B32A32_SFLOAT;
			case GfxPixelFormat::BC1_RGB_UNORM:			return VK_FORMAT_BC1_RGB_UNORM_BLOCK;
			case GfxPixelFormat::BC1_RGB_SRGB:			return VK_FORMAT_BC1_RGB_SRGB_BLOCK;
			case GfxPixelFormat::BC1_RGBA_UNORM:		return VK_FORMAT_BC1_RGBA_UNORM_BLOCK;
			case GfxPixelFormat::BC1_RGBA_SRGB:			return VK_FORMAT_BC1_RGBA_SRGB_BLOCK;
			case GfxPixelFormat::BC2_UNORM:				return VK_FORMAT_BC2_UNORM_BLOCK;
			case GfxPixelFormat::BC2_SRGB:				return VK_FORMAT_BC2_SRGB_BLOCK;
			case GfxPixelFormat::BC3_UNORM:				return VK_FORMAT_BC3_UNORM_BLOCK;
			case GfxPixelFormat::BC3_SRGB:				return VK_FORMAT_BC3_SRGB_BLOCK;
			case GfxPixelFormat::BC4_UNORM:				return VK_FORMAT_BC4_UNORM_BLOCK;
			case GfxPixelFormat::BC4_SNORM:				return VK_FORMAT_BC4_SNORM_BLOCK;
			case GfxPixelFormat::BC5_UNORM:				return VK_FORMAT_BC5_UNORM_BLOCK;
			case GfxPixelFormat::BC5_SNORM:				return VK_FORMAT_BC5_SNORM_BLOCK;
			case GfxPixelFormat::BC6H_UF16:				return VK_FORMAT_BC6H_UFLOAT_BLOCK;
			case GfxPixelFormat::BC6H_SF16:				return VK_FORMAT_BC6H_SFLOAT_BLOCK;
			case GfxPixelFormat::BC7_UNORM:				return VK_FORMAT_BC7_UNORM_BLOCK;
			case GfxPixelFormat::BC7_SRGB:				return VK_FORMAT_BC7_SRGB_BLOCK;
			case GfxPixelFormat::D32_SFLOAT_S8_UINT:	return VK_FORMAT_D32_SFLOAT_S8_UINT;
			case GfxPixelFormat::D32_SFLOAT:			return VK_FORMAT_D32_SFLOAT;
			case GfxPixelFormat::D24_UNORM_S8_UINT:		return VK_FORMAT_D24_UNORM_S8_UINT;
			case GfxPixelFormat::D16_UNORM:				return VK_FORMAT_D16_UNORM;
			default:
				LOGCRITICAL("Pixel format not registered on this function");
				return VK_FORMAT_UNDEFINED;
		}
	}

	VkSampleCountFlagBits GfxTextureVK::GetVkMSAA(const GfxMSAA& msaa)
	{
		switch (msaa)
		{
			case GfxMSAA::X1:	return VK_SAMPLE_COUNT_1_BIT;
			case GfxMSAA::X2:	return VK_SAMPLE_COUNT_2_BIT;
			case GfxMSAA::X4:	return VK_SAMPLE_COUNT_4_BIT;
			case GfxMSAA::X8:	return VK_SAMPLE_COUNT_8_BIT;
			case GfxMSAA::X16:	return VK_SAMPLE_COUNT_16_BIT;
			case GfxMSAA::X32:	return VK_SAMPLE_COUNT_32_BIT;
			case GfxMSAA::X64:	return VK_SAMPLE_COUNT_64_BIT;
			default:
				LOGCRITICAL("MSAA type not registered on this function");
				return VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM;
		}
	}

	VkImageType GfxTextureVK::GetVkImageType(const GfxTextureType& type)
	{
		switch (type)
		{
			case GfxTextureType::TEXTURE_1D:
			case GfxTextureType::TEXTURE_ARRAY_1D:
				return VK_IMAGE_TYPE_1D;

			case GfxTextureType::CUBEMAP:
			case GfxTextureType::CUBEMAP_ARRAY:
			case GfxTextureType::TEXTURE_2D:
			case GfxTextureType::TEXTURE_ARRAY_2D:
				return VK_IMAGE_TYPE_2D;

			case GfxTextureType::TEXTURE_3D:
				return VK_IMAGE_TYPE_3D;

			default:
				LOGCRITICAL("Texture type not registered on this function");
				return VK_IMAGE_TYPE_MAX_ENUM;
		}
	}

	VkImageViewType GfxTextureVK::GetVkImageViewType(const GfxTextureType& type)
	{
		switch (type)
		{
			case GfxTextureType::TEXTURE_1D:		return VK_IMAGE_VIEW_TYPE_1D;
			case GfxTextureType::TEXTURE_2D:		return VK_IMAGE_VIEW_TYPE_2D;
			case GfxTextureType::TEXTURE_3D:		return VK_IMAGE_VIEW_TYPE_3D;
			case GfxTextureType::TEXTURE_ARRAY_1D:	return VK_IMAGE_VIEW_TYPE_1D_ARRAY;
			case GfxTextureType::TEXTURE_ARRAY_2D:	return VK_IMAGE_VIEW_TYPE_2D_ARRAY;
			case GfxTextureType::CUBEMAP:			return VK_IMAGE_VIEW_TYPE_CUBE;
			case GfxTextureType::CUBEMAP_ARRAY:		return VK_IMAGE_VIEW_TYPE_CUBE_ARRAY;
			default:
				LOGCRITICAL("Texture type not registered on this function");
				return VK_IMAGE_VIEW_TYPE_MAX_ENUM;
		}
	}

	VkImageUsageFlags GfxTextureVK::GetVkUsageFlags(const GfxTextureFlags& flags)
	{
		VkImageUsageFlags usageFlags = 0;

		if (flags & GfxTextureFlags_BIND_AS_SHADER_RESOURCE)
			usageFlags |= VK_IMAGE_USAGE_SAMPLED_BIT;

		if (flags & GfxTextureFlags_BIND_AS_RENDER_TARGET)
			usageFlags |= VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

		if (flags & GfxTextureFlags_BIND_AS_DEPTH_STENCIL)
			usageFlags |= VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;

		if (flags & GfxTextureFlags_COPY_DESTINATION)
			usageFlags |= VK_IMAGE_USAGE_TRANSFER_DST_BIT;

		if (flags & GfxTextureFlags_COPY_SOURCE)
			usageFlags |= VK_IMAGE_USAGE_TRANSFER_SRC_BIT;

		if (flags & GfxTextureFlags_ALLOW_UNORDERED_ACCESS)
			usageFlags |= VK_IMAGE_USAGE_STORAGE_BIT;

		return usageFlags;
	}

	VkImageCreateFlags GfxTextureVK::GetVkImageCreateFlags(const GfxTextureType& type)
	{
		switch (type)
		{
			case GfxTextureType::TEXTURE_1D:
			case GfxTextureType::TEXTURE_2D:
			case GfxTextureType::TEXTURE_3D:
			case GfxTextureType::TEXTURE_ARRAY_1D:
			case GfxTextureType::TEXTURE_ARRAY_2D:
				return 0;

			case GfxTextureType::CUBEMAP:
			case GfxTextureType::CUBEMAP_ARRAY:
				return VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;

			default:
				LOGCRITICAL("Texture type not registered on this function");
				return 0;
		}
	}

	VkImageAspectFlags GfxTextureVK::GetVkImageAspectMask(const GfxPixelFormat& format)
	{
		VkImageAspectFlags aspectMask;
		if (IsDepthPixelFormat(format))
		{
			aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
			if (PixelFormatHasStencil(format))
				aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
		}
		else
			aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		return aspectMask;
	}

	void GfxTextureVK::SetDebugName()
	{
		GfxDevice& gfxDevice = GfxDevice::GetInstance();
		gfxDevice.SetObjectDebugName({ m_Name, VK_OBJECT_TYPE_IMAGE, reinterpret_cast<uint64_t>(m_Image) });

		if (m_MultisampledImage)
		{
			std::string postFix = "";
			switch (m_Descriptor.msaa)
			{
				case GfxMSAA::X1:	postFix = "_X1"; break;
				case GfxMSAA::X2:	postFix = "_X2"; break;
				case GfxMSAA::X4:	postFix = "_X4"; break;
				case GfxMSAA::X8:	postFix = "_X8"; break;
				case GfxMSAA::X16:	postFix = "_X16"; break;
				case GfxMSAA::X32:	postFix = "_X32"; break;
				case GfxMSAA::X64:	postFix = "_X64"; break;
			}

			gfxDevice.SetObjectDebugName({ m_Name + postFix, VK_OBJECT_TYPE_IMAGE, reinterpret_cast<uint64_t>(m_MultisampledImage) });
		}
	}

	void GfxTextureVK::CopyForDeletion(GfxTextureDeleteContiner* container)
	{
		container->views = m_Views;
		container->multisampledViews = m_MultisampledViews;

		if (m_HasOwnership)
		{
			container->image = m_Image;
			container->allocation = m_Allocation;

			container->multisampledImage = m_MultisampledImage;
			container->multisampledAllocation = m_MultisampledAllocation;
		}
	}
}
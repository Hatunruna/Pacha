#include "GfxTexture.h"

namespace Pacha
{
	GfxTextureBase::GfxTextureBase(const GfxTextureDescriptor& descriptor, std::shared_ptr<uint8_t> data)
	{
		PACHA_ASSERT(descriptor.format != GfxPixelFormat::UNDEFINED, "Undefined pixel format used");

		m_Descriptor = descriptor;

		if (m_Descriptor.type == GfxTextureType::CUBEMAP)
			m_Descriptor.slices = 6;

		if (m_Descriptor.type == GfxTextureType::CUBEMAP_ARRAY)
			m_Descriptor.slices *= 6;

		if (m_Descriptor.flags & GfxTextureFlags_SRGB_HINT)
			m_Descriptor.format = GetSRGB(m_Descriptor.format);

		if (m_Descriptor.keepCpuData)
			m_Data = data;

		m_Size = CalculateTextureDataSizeAndMipsInfo(m_Descriptor, m_MipsInfo);
	}

	GfxTextureBase::~GfxTextureBase()
	{ }

	uint32_t GfxTextureBase::GetWidth() const
	{
		return m_Descriptor.width;
	}

	uint32_t GfxTextureBase::GetHeight() const
	{
		return m_Descriptor.height;
	}

	GfxTextureType GfxTextureBase::GetType() const
	{
		return m_Descriptor.type;
	}

	GfxPixelFormat GfxTextureBase::GetFormat() const
	{
		return m_Descriptor.format;
	}

	GfxMSAA GfxTextureBase::GetMSAA() const
	{
		return m_Descriptor.msaa;
	}

	uint32_t GfxTextureBase::GetMipCount() const
	{
		return m_Descriptor.mips;
	}

	uint32_t GfxTextureBase::GetSlicesCount() const
	{
		return m_Descriptor.slices;
	}

	size_t GfxTextureBase::GetSize() const
	{
		return m_Size;
	}

	const std::vector<GfxMipInfo>& GfxTextureBase::GetMipsInfo() const
	{
		return m_MipsInfo;
	}

	const std::string& GfxTextureBase::GetName() const
	{
		return m_Name;
	}

	void GfxTextureBase::SetName(const std::string& name)
	{
		m_Name = name;
		SetDebugName();
	}

	uint64_t GfxTextureBase::GetHash() const
	{
		return m_Hash;
	}

	const GfxTextureDescriptor& GfxTextureBase::GetDescriptor() const
	{
		return m_Descriptor;
	}

	size_t GfxTextureBase::CalculateTextureDataSize(const GfxTextureDescriptor& descriptor)
	{
		size_t size = 0;
		bool isBlockCompressed = IsBlockCompressedPixelFormat(descriptor.format);
		for (uint32_t j = 0; j < descriptor.slices; ++j)
		{
			for (uint32_t i = 0; i < descriptor.mips; ++i)
			{
				size_t mipWidth = static_cast<size_t>(descriptor.width >> i);
				size_t mipHeight = static_cast<size_t>(descriptor.height >> i);

				size_t mipSize = 0;
				if (isBlockCompressed)
				{
					size_t numBlocksWide = (mipWidth + 3) / 4;
					size_t numBlocksHeight = (mipHeight + 3) / 4;
					mipSize = numBlocksWide * numBlocksHeight * GetCompressedBlockSize(descriptor.format);
				}
				else
				{
					size_t rowBytes = mipWidth * GetPixelSize(descriptor.format);
					mipSize = rowBytes * mipHeight;
				}

				mipSize *= static_cast<size_t>(descriptor.depth);
				size += mipSize;
			}
		}

		return size;
	}

	std::vector<GfxMipInfo> GfxTextureBase::CalculateMipsInfo(const GfxTextureDescriptor& descriptor)
	{
		std::vector<GfxMipInfo> mipsInfo = {};
		mipsInfo.resize(descriptor.slices * descriptor.mips);

		size_t offset = 0;
		bool isBlockCompressed = IsBlockCompressedPixelFormat(descriptor.format);
		for (uint32_t j = 0; j < descriptor.slices; ++j)
		{
			for (uint32_t i = 0; i < descriptor.mips; ++i)
			{
				size_t mipWidth = static_cast<size_t>(descriptor.width >> i);
				size_t mipHeight = static_cast<size_t>(descriptor.height >> i);

				size_t mipSize = 0;
				if (isBlockCompressed)
				{
					size_t numBlocksWide = (mipWidth + 3) / 4;
					size_t numBlocksHeight = (mipHeight + 3) / 4;
					mipSize = numBlocksWide * numBlocksHeight * GetCompressedBlockSize(descriptor.format);
				}
				else
				{
					size_t rowBytes = mipWidth * GetPixelSize(descriptor.format);
					mipSize = rowBytes * mipHeight;
				}

				mipSize *= static_cast<size_t>(descriptor.depth);
				mipsInfo[i + (j * descriptor.slices)] = { mipSize, offset };

				offset += mipSize;
			}
		}

		return mipsInfo;
	}

	size_t GfxTextureBase::CalculateTextureDataSizeAndMipsInfo(const GfxTextureDescriptor& descriptor, std::vector<GfxMipInfo>& mipsInfo)
	{
		mipsInfo.resize(descriptor.slices * descriptor.mips);

		size_t size = 0;
		size_t offset = 0;
		bool isBlockCompressed = IsBlockCompressedPixelFormat(descriptor.format);
		for (uint32_t j = 0; j < descriptor.slices; ++j)
		{
			for (uint32_t i = 0; i < descriptor.mips; ++i)
			{
				size_t mipWidth = static_cast<size_t>(descriptor.width >> i);
				size_t mipHeight = static_cast<size_t>(descriptor.height >> i);

				size_t mipSize = 0;
				if (isBlockCompressed)
				{
					size_t numBlocksWide = (mipWidth + 3) / 4;
					size_t numBlocksHeight = (mipHeight + 3) / 4;
					mipSize = numBlocksWide * numBlocksHeight * GetCompressedBlockSize(descriptor.format);
				}
				else
				{
					size_t rowBytes = mipWidth * GetPixelSize(descriptor.format);
					mipSize = rowBytes * mipHeight;
				}

				mipSize *= static_cast<size_t>(descriptor.depth);
				mipsInfo[i + (j * descriptor.slices)] = { mipSize, offset };

				offset += mipSize;
				size += mipSize;
			}
		}

		return size;
	}
}
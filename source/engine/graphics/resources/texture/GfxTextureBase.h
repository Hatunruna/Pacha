#pragma once
#include "GfxPixelFormat.h"
#include "graphics/resources/GfxResourceState.h"

#include <string>
#include <vector>
#include <glm/glm.hpp>

namespace Pacha
{
	enum class GfxMSAA
	{
		X1,
		X2,
		X4,
		X8,
		X16,
		X32,
		X64
	};

	enum GfxTextureFlagBits
	{
		GfxTextureFlags_SRGB_HINT					= 1 << 0,
		GfxTextureFlags_BIND_AS_SHADER_RESOURCE		= 1 << 1,
		GfxTextureFlags_BIND_AS_DEPTH_STENCIL		= 1 << 2,
		GfxTextureFlags_BIND_AS_RENDER_TARGET		= 1 << 3,
		GfxTextureFlags_ALLOW_UNORDERED_ACCESS		= 1 << 4,
		GfxTextureFlags_COPY_DESTINATION			= 1 << 5,
		GfxTextureFlags_COPY_SOURCE					= 1 << 6,
		GfxTextureFlags_AUTO_RESOLVE_MSAA			= 1 << 7,
		GfxTextureFlags_RENDER_INTO_SUB_RESOURCES	= 1 << 8,

		GfxTextureFlags_DEFAULT			= GfxTextureFlags_BIND_AS_SHADER_RESOURCE | GfxTextureFlags_COPY_DESTINATION,
		GfxTextureFlags_DEFAULT_RT		= GfxTextureFlags_BIND_AS_RENDER_TARGET | GfxTextureFlags_BIND_AS_SHADER_RESOURCE | GfxTextureFlags_AUTO_RESOLVE_MSAA,
		GfxTextureFlags_DEFAULT_DEPTH	= GfxTextureFlags_BIND_AS_DEPTH_STENCIL | GfxTextureFlags_BIND_AS_SHADER_RESOURCE
	};
	typedef uint32_t GfxTextureFlags;

	enum class GfxTextureType
	{
		TEXTURE_1D,
		TEXTURE_2D,
		TEXTURE_3D,
		TEXTURE_ARRAY_1D,
		TEXTURE_ARRAY_2D,
		CUBEMAP,
		CUBEMAP_ARRAY
	};

	inline bool IsArrayTexture(const GfxTextureType textureType)
	{
		return textureType == GfxTextureType::TEXTURE_ARRAY_1D || textureType == GfxTextureType::TEXTURE_ARRAY_2D || textureType == GfxTextureType::CUBEMAP_ARRAY;
	}

	inline bool IsCubemapTexture(const GfxTextureType textureType)
	{
		return textureType == GfxTextureType::CUBEMAP || textureType == GfxTextureType::CUBEMAP_ARRAY;
	}

	struct GfxTextureDescriptor
	{
		uint32_t width = 1;
		uint32_t height = 1;
		uint32_t depth = 1;

		GfxTextureFlags flags = GfxTextureFlags_DEFAULT;
		GfxTextureType type = GfxTextureType::TEXTURE_2D;
		GfxPixelFormat format = GfxPixelFormat::RGBA8_UNORM;

		uint32_t mips = 1;
		uint32_t slices = 1;
		GfxMSAA msaa = GfxMSAA::X1;
		bool keepCpuData = false;

		friend bool operator == (const GfxTextureDescriptor& lhs, const GfxTextureDescriptor& rhs)
		{
			return memcmp(&lhs, &rhs, sizeof(GfxTextureDescriptor)) == 0;
		}

		friend bool operator != (const GfxTextureDescriptor& lhs, const GfxTextureDescriptor& rhs)
		{
			return memcmp(&lhs, &rhs, sizeof(GfxTextureDescriptor)) != 0;
		}
	};

	struct GfxMipInfo
	{
		size_t size = 0;
		size_t offset = 0;
	};

	class GfxTextureBase
	{
	public:
		virtual void SetFromNativeHandle(void* handle, void* multisampledHandle, const GfxTextureDescriptor& descriptor) = 0;

		uint32_t GetWidth() const;
		uint32_t GetHeight() const;
		GfxTextureType GetType() const;
		GfxPixelFormat GetFormat() const;

		GfxMSAA GetMSAA() const;
		uint32_t GetMipCount() const;
		uint32_t GetSlicesCount() const;
		size_t GetSize() const;
		const std::vector<GfxMipInfo>& GetMipsInfo() const;

		const std::string& GetName() const;
		void SetName(const std::string& name);

		uint64_t GetHash() const;
		const GfxTextureDescriptor& GetDescriptor() const;

		static size_t CalculateTextureDataSize(const GfxTextureDescriptor& descriptor);
		static std::vector<GfxMipInfo> CalculateMipsInfo(const GfxTextureDescriptor& descriptor);
		static size_t CalculateTextureDataSizeAndMipsInfo(const GfxTextureDescriptor& descriptor, std::vector<GfxMipInfo>& mipOffsets);

	protected:
		GfxTextureBase() = default;
		GfxTextureBase(const GfxTextureDescriptor& descriptor, std::shared_ptr<uint8_t> data);

		virtual ~GfxTextureBase();
		virtual void SetDebugName() = 0;

		std::string m_Name;
		uint64_t m_Hash = 0;

		size_t m_Size = 0;
		std::vector<GfxMipInfo> m_MipsInfo;

		GfxTextureDescriptor m_Descriptor = {};
		std::shared_ptr<uint8_t> m_Data = nullptr;

	private:
		virtual void CopyForDeletion(struct GfxTextureDeleteContiner* container) = 0;
	};
}
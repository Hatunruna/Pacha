#pragma once
#include "GfxTextureBase.h"

#include <vector>
#include <vk_mem_alloc.h>

namespace Pacha
{
	struct GfxTextureDeleteContiner
	{
		VkImage image = VK_NULL_HANDLE;
		VmaAllocation allocation = VK_NULL_HANDLE;
		std::vector<VkImageView> views;
	
		VkImage multisampledImage = VK_NULL_HANDLE;
		VmaAllocation multisampledAllocation = VK_NULL_HANDLE;
		std::vector<VkImageView> multisampledViews;
	};

	class GfxTextureVK : public GfxTextureBase
	{
		friend class GfxMemoryManager;

	public:
		GfxTextureVK() = default;
		GfxTextureVK(const GfxTextureDescriptor& descriptor, const std::shared_ptr<uint8_t>& data, const GfxResourceState& afterUploadState = GfxResourceState::SHADER_RESOURCE);
		~GfxTextureVK();
		
		void SetFromNativeHandle(void* handle, void* multisampledHandle, const GfxTextureDescriptor& descriptor) override;
		
		VkImage GetImage();
		VkImageView GetImageView(const uint32_t& mip = 0, const uint32_t& slice = 0);

		VkImage GetMultisampledImage();
		VkImageView GetMultisampledImageView(const uint32_t& mip = 0, const uint32_t& slice = 0);

		static VkFormat GetVkFormat(const GfxPixelFormat& format);
		static VkSampleCountFlagBits GetVkMSAA(const GfxMSAA& msaa);
		static VkImageType GetVkImageType(const GfxTextureType& type);
		static VkImageViewType GetVkImageViewType(const GfxTextureType& type);
		static VkImageUsageFlags GetVkUsageFlags(const GfxTextureFlags& flags);
		static VkImageCreateFlags GetVkImageCreateFlags(const GfxTextureType& type);
		static VkImageAspectFlags GetVkImageAspectMask(const GfxPixelFormat& format);

	private:
		bool m_HasOwnership = false;
		VkImage m_Image = VK_NULL_HANDLE;
		VmaAllocation m_Allocation = VK_NULL_HANDLE;
		std::vector<VkImageView> m_Views = { VK_NULL_HANDLE };

		VkImage m_MultisampledImage = VK_NULL_HANDLE;
		VmaAllocation m_MultisampledAllocation = VK_NULL_HANDLE;
		std::vector<VkImageView> m_MultisampledViews = { VK_NULL_HANDLE };

		void CreateViews(const VkImage& image, std::vector<VkImageView>& views);
		void SetDebugName() override;
		void CopyForDeletion(GfxTextureDeleteContiner* container) override;
	};
}
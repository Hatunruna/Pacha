#pragma once
#include "GfxCommandBufferBase.h"

#include <volk.h>

namespace Pacha
{
	class GfxCommandBufferVK : public GfxCommandBufferBase
	{
	public:
		GfxCommandBufferVK();
		~GfxCommandBufferVK();

		void BeginCommandBuffer() override;
		void EndCommandBuffer() override;

		void BeginSectionMarker(const std::string& markerName, const glm::vec4& color) override;
		void EndSectionMarker() override;

		void BeginRenderPass(GfxRenderTarget& renderTarget, const GfxClearValues& clearValues = {}, const GfxSubResourceTarget& subResource = {}) override;
		void BeginRenderPass(GfxRenderTarget& renderTarget, const GfxViewport& viewport, const GfxClearValues& clearValues = {}, const GfxSubResourceTarget& subResource = {}) override;
		void BeginRenderPass(GfxRenderTarget& renderTarget, const GfxViewport& viewport, const GfxRect& scissor, const GfxClearValues& clearValues = {}, const GfxSubResourceTarget& subResource = {}) override;
		void EndRenderPass() override;

		void CopyBufferToBuffer(GfxBuffer* source, GfxBuffer* destination, const size_t size = 0, const size_t sourceDataOffset = 0, const size_t destinationDataOffset = 0) override;
		void CopyBufferToTexture(GfxBuffer* source, GfxTexture* destination, const size_t sourceDataOffset = 0)override;
		void CopyBufferToTextureSubresource(GfxBuffer* source, GfxTexture* destination, const uint32_t mip, const uint32_t slice, const size_t sourceDataOffset = 0) override;

		void CopyTextureToTexture(GfxTexture* source, GfxTexture* destination) override;
		void CopyTextureSubresourceToTextureSubresource(GfxTexture* source, const uint32_t sourceMip, const uint32_t sourceSlice, GfxTexture* destination, const uint32_t destinationMip, const uint32_t destinationSlice) override;
		void CopyTextureToBuffer(GfxTexture* source, GfxBuffer* destination) override;
		void CopyTextureSubresourceToBuffer(GfxTexture* source, const uint32_t mip, const uint32_t slice, GfxBuffer* destination) override;

		void TransitionBuffer(GfxBuffer* buffer, const GfxResourceState sourceState, const GfxResourceState destinationState) override;
		void TransitionTexture(GfxTexture* texture, const GfxResourceState sourceState, const GfxResourceState destinationState) override;
		void TransitionTextureSubresource(GfxTexture* texture, const uint32_t mip, const uint32_t slice, const GfxResourceState sourceState, const GfxResourceState destinationState) override;

		void BlitTexture(GfxTexture* source, GfxTexture* destination, const GfxFilterMode filterMode = GfxFilterMode::LINEAR) override;
		void BlitTextureSubresources(GfxTexture* source, const uint32_t sourceMip, const uint32_t sourceSlice, GfxTexture* destination, const uint32_t destinationMip, const uint32_t destinationSlice, const GfxFilterMode filterMode = GfxFilterMode::LINEAR) override;

		void DrawRenderBatch(const class GfxSceneView* sceneView, const std::vector<GfxRenderBatch>& renderBatches, const PachaId& passId) override;

		VkCommandBuffer& GetBuffer();
		static VkAccessFlags GetVkAccessFlags(const GfxResourceState& resourceState);
		static VkPipelineStageFlags GetVkPipelineStageFlags(const GfxResourceState& resourceState);
		static VkImageLayout GetVkImageLayout(const GfxResourceState& resourceState);
		static VkImageAspectFlags GetVkAspectMask(const GfxPixelFormat& format);

	private:
		VkCommandPool m_CommandPool = VK_NULL_HANDLE;
		VkCommandBuffer m_CommandBuffer = VK_NULL_HANDLE;
		VkCommandBufferBeginInfo m_CommandBufferBeginInfo = {};

		void SetDebugName() override;
	};
}
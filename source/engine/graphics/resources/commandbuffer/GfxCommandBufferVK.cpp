#include "GfxCommandBufferVK.h"

#include "core/Assert.h"
#include "graphics/device/GfxDevice.h"
#include "graphics/renderpipeline/GfxSceneView.h"
#include "graphics/utilities/GfxBindlessResourceManager.h"

namespace Pacha
{
	GfxCommandBufferVK::GfxCommandBufferVK()
	{
		GfxDevice& gfxDevice = GfxDevice::GetInstance();
		const VkDevice& device = gfxDevice.GetVkDevice();

		VkCommandPoolCreateInfo pollCreateInfo = {};
		pollCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
		pollCreateInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
		pollCreateInfo.queueFamilyIndex = gfxDevice.GetGraphicsQueueFamilyIndex();
		vkCreateCommandPool(device, &pollCreateInfo, nullptr, &m_CommandPool);

		VkCommandBufferAllocateInfo bufferAllocInfo = {};
		bufferAllocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		bufferAllocInfo.commandBufferCount = 1;
		bufferAllocInfo.commandPool = m_CommandPool;
		bufferAllocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		vkAllocateCommandBuffers(device, &bufferAllocInfo, &m_CommandBuffer);

		m_CommandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		m_CommandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
	}

	GfxCommandBufferVK::~GfxCommandBufferVK()
	{
		GfxDevice& gfxDevice = GfxDevice::GetInstance();
		const VkDevice& device = gfxDevice.GetVkDevice();
		vkDestroyCommandPool(device, m_CommandPool, nullptr);
	}

	void GfxCommandBufferVK::BeginCommandBuffer()
	{
		vkBeginCommandBuffer(m_CommandBuffer, &m_CommandBufferBeginInfo);
	}

	void GfxCommandBufferVK::EndCommandBuffer()
	{
		vkEndCommandBuffer(m_CommandBuffer);
	}

	void GfxCommandBufferVK::BeginSectionMarker(const std::string& markerName, const glm::vec4& color)
	{
		GfxDebugBeginMarkePayload payload(markerName, color, m_CommandBuffer);
		GfxDevice::GetInstance().SectionMarkerBegin(payload);
	}

	void GfxCommandBufferVK::EndSectionMarker()
	{
		GfxDebugEndMarkePayload payload(m_CommandBuffer);
		GfxDevice::GetInstance().SectionMarkerEnd(payload);
	}

	void GfxCommandBufferVK::BeginRenderPass(GfxRenderTarget& renderTarget, const GfxClearValues& clearValues, const GfxSubResourceTarget& subResource)
	{
		GfxRect scissor = {};
		scissor.width = renderTarget.GetWidth();
		scissor.height = renderTarget.GetHeight();

		GfxViewport viewport = {};
		viewport.maxDepth = 1.f;
		viewport.width = static_cast<float>(scissor.width);
		viewport.height = static_cast<float>(scissor.height);

		BeginRenderPass(renderTarget, viewport, scissor, clearValues, subResource);
	}

	void GfxCommandBufferVK::BeginRenderPass(GfxRenderTarget& renderTarget, const GfxViewport& viewport, const GfxClearValues& clearValues, const GfxSubResourceTarget& subResource)
	{
		GfxRect scissor = {};
		scissor.width = renderTarget.GetWidth();
		scissor.height = renderTarget.GetHeight();

		BeginRenderPass(renderTarget, viewport, scissor, clearValues, subResource);
	}

	void GfxCommandBufferVK::BeginRenderPass(GfxRenderTarget& renderTarget, const GfxViewport& viewport, const GfxRect& scissor, const GfxClearValues& clearValues, const GfxSubResourceTarget& subResource)
	{
		VkRenderPassBeginInfo beginInfo = {};
		beginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		beginInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
		beginInfo.pClearValues = reinterpret_cast<const VkClearValue*>(clearValues.data());
		beginInfo.renderPass = renderTarget.GetRenderPass();
		beginInfo.framebuffer = renderTarget.GetFrameBuffer(subResource);
		beginInfo.renderArea.offset = { 0, 0 };
		beginInfo.renderArea.extent = { renderTarget.GetWidth(), renderTarget.GetHeight() };

		VkRect2D vkscissor = {};
		memcpy(&vkscissor, &scissor, sizeof(GfxRect));

		VkViewport vkviewport = {};
		memcpy(&vkviewport, &viewport, sizeof(GfxViewport));

		vkCmdSetScissor(m_CommandBuffer, 0, 1, &vkscissor);
		vkCmdSetViewport(m_CommandBuffer, 0, 1, &vkviewport);
		vkCmdBeginRenderPass(m_CommandBuffer, &beginInfo, VK_SUBPASS_CONTENTS_INLINE);
	}

	void GfxCommandBufferVK::EndRenderPass()
	{
		vkCmdEndRenderPass(m_CommandBuffer);
	}

	void GfxCommandBufferVK::CopyBufferToBuffer(GfxBuffer* source, GfxBuffer* destination, const size_t size, const size_t sourceOffset, const size_t destinationOffset)
	{
		PACHA_ASSERT(source, "Source buffer was null");
		PACHA_ASSERT(destination, "Destination buffer was null");

		VkBufferCopy bufferCopyInfo = {};
		bufferCopyInfo.srcOffset = sourceOffset;
		bufferCopyInfo.dstOffset = destinationOffset;
		bufferCopyInfo.size = size == 0 ? source->GetSize() : size;
		vkCmdCopyBuffer(m_CommandBuffer, source->GetBuffer(), destination->GetBuffer(), 1, &bufferCopyInfo);
	}

	void GfxCommandBufferVK::CopyBufferToTexture(GfxBuffer* source, GfxTexture* destination, const size_t sourceDataOffset)
	{
		PACHA_ASSERT(source, "Source buffer was null");
		PACHA_ASSERT(destination, "Destination texture was null");

		const GfxTextureDescriptor& descriptor = destination->GetDescriptor();
		const std::vector<GfxMipInfo>& mipsInfo = destination->GetMipsInfo();
		const bool isTex3D = descriptor.type == GfxTextureType::TEXTURE_3D;

		std::vector<VkBufferImageCopy> copyRegions;
		copyRegions.resize(mipsInfo.size(), {});

		const size_t sliceSize = destination->GetSize() / descriptor.slices;
		for (uint32_t slice = 0; slice < descriptor.slices; ++slice)
		{
			const uint32_t sliceMipOffset = slice * descriptor.mips;
			for (uint32_t mip = 0; mip < descriptor.mips; ++mip)
			{
				const uint32_t index = sliceMipOffset + mip;
				copyRegions[index].imageSubresource.aspectMask = GetVkAspectMask(descriptor.format);
				copyRegions[index].imageSubresource.mipLevel = mip;
				copyRegions[index].imageSubresource.layerCount = 1;
				copyRegions[index].imageSubresource.baseArrayLayer = slice;
				copyRegions[index].imageExtent.width = descriptor.width >> mip;
				copyRegions[index].imageExtent.height = descriptor.height >> mip;
				copyRegions[index].imageExtent.depth = isTex3D ? descriptor.depth >> mip : 1;
				copyRegions[index].bufferOffset = sourceDataOffset + (sliceSize * slice) + mipsInfo[mip].offset;
			}
		}

		vkCmdCopyBufferToImage(m_CommandBuffer, source->GetBuffer(), destination->GetImage(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, static_cast<uint32_t>(copyRegions.size()), copyRegions.data());
	}

	void GfxCommandBufferVK::CopyBufferToTextureSubresource(GfxBuffer* source, GfxTexture* destination, const uint32_t mip, const uint32_t slice, const size_t sourceDataOffset)
	{
		PACHA_ASSERT(source, "Source buffer was null");
		PACHA_ASSERT(destination, "Destination texture was null");

		const GfxTextureDescriptor& descriptor = destination->GetDescriptor();
		PACHA_ASSERT(mip <= descriptor.mips && slice <= descriptor.slices, "Subresource index out of scope");

		const bool isTex3D = descriptor.type == GfxTextureType::TEXTURE_3D;

		VkBufferImageCopy copyRegion = {};
		copyRegion.imageSubresource.aspectMask = GetVkAspectMask(descriptor.format);
		copyRegion.imageSubresource.mipLevel = mip;
		copyRegion.imageSubresource.layerCount = 1;
		copyRegion.imageSubresource.baseArrayLayer = slice;
		copyRegion.imageExtent.width = descriptor.width >> mip;
		copyRegion.imageExtent.height = descriptor.height >> mip;
		copyRegion.imageExtent.depth = isTex3D ? descriptor.depth >> mip : 1;
		copyRegion.bufferOffset = sourceDataOffset;

		vkCmdCopyBufferToImage(m_CommandBuffer, source->GetBuffer(), destination->GetImage(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &copyRegion);
	}

	void GfxCommandBufferVK::CopyTextureToTexture(GfxTexture* source, GfxTexture* destination)
	{
		PACHA_ASSERT(source, "Source texture was null");
		PACHA_ASSERT(destination, "Destination texture was null");

		const GfxTextureDescriptor& sourceDescriptor = source->GetDescriptor();
		const GfxTextureDescriptor& destinationDescriptor = destination->GetDescriptor();
		const bool isTex3D = sourceDescriptor.type == GfxTextureType::TEXTURE_3D;

		std::vector<VkImageCopy> copyRegions;
		copyRegions.resize(sourceDescriptor.mips * sourceDescriptor.slices, {});

		for (uint32_t slice = 0; slice < sourceDescriptor.slices; ++slice)
		{
			const uint32_t sliceMipOffset = slice * sourceDescriptor.mips;
			for (uint32_t mip = 0; mip < sourceDescriptor.mips; ++mip)
			{
				const uint32_t index = sliceMipOffset + mip;

				VkImageSubresourceLayers sourceSubresource = {};
				sourceSubresource.aspectMask = GetVkAspectMask(sourceDescriptor.format);
				sourceSubresource.baseArrayLayer = slice;
				sourceSubresource.layerCount = 1;
				sourceSubresource.mipLevel = mip;

				VkImageSubresourceLayers destinationSubresource = {};
				destinationSubresource.aspectMask = GetVkAspectMask(destinationDescriptor.format);
				destinationSubresource.baseArrayLayer = slice;
				destinationSubresource.layerCount = 1;
				destinationSubresource.mipLevel = mip;

				copyRegions[index].srcOffset = {};
				copyRegions[index].dstOffset = {};

				copyRegions[index].extent.width = sourceDescriptor.width >> mip;
				copyRegions[index].extent.height = sourceDescriptor.height >> mip;
				copyRegions[index].extent.depth = isTex3D ? sourceDescriptor.depth >> mip : 1;

				copyRegions[index].srcSubresource = sourceSubresource;
				copyRegions[index].dstSubresource = destinationSubresource;
			}
		}

		vkCmdCopyImage(m_CommandBuffer, source->GetImage(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, destination->GetImage(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, static_cast<uint32_t>(copyRegions.size()), copyRegions.data());
	}

	void GfxCommandBufferVK::CopyTextureSubresourceToTextureSubresource(GfxTexture* source, const uint32_t sourceMip, const uint32_t sourceSlice, GfxTexture* destination, const uint32_t destinationMip, const uint32_t destinationSlice)
	{
		PACHA_ASSERT(source, "Source texture was null");
		PACHA_ASSERT(destination, "Destination texture was null");

		const GfxTextureDescriptor& sourceDescriptor = source->GetDescriptor();
		const GfxTextureDescriptor& destinationDescriptor = destination->GetDescriptor();
		const bool isTex3D = sourceDescriptor.type == GfxTextureType::TEXTURE_3D;

		VkImageSubresourceLayers sourceSubresource = {};
		sourceSubresource.aspectMask = GetVkAspectMask(sourceDescriptor.format);
		sourceSubresource.baseArrayLayer = sourceSlice;
		sourceSubresource.layerCount = 1;
		sourceSubresource.mipLevel = sourceMip;

		VkImageSubresourceLayers destinationSubresource = {};
		destinationSubresource.aspectMask = GetVkAspectMask(destinationDescriptor.format);
		destinationSubresource.baseArrayLayer = destinationSlice;
		destinationSubresource.layerCount = 1;
		destinationSubresource.mipLevel = destinationMip;

		VkImageCopy copyRegion;
		copyRegion.extent.width = sourceDescriptor.width >> sourceMip;
		copyRegion.extent.height = sourceDescriptor.height >> sourceMip;
		copyRegion.extent.depth = isTex3D ? sourceDescriptor.depth >> sourceMip : 1;
		copyRegion.srcSubresource = sourceSubresource;
		copyRegion.dstSubresource = destinationSubresource;
		copyRegion.srcOffset = {};
		copyRegion.dstOffset = {};

		vkCmdCopyImage(m_CommandBuffer, source->GetImage(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, destination->GetImage(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &copyRegion);
	}

	void GfxCommandBufferVK::CopyTextureToBuffer(GfxTexture* source, GfxBuffer* destination)
	{
		PACHA_ASSERT(source, "Source texture was null");
		PACHA_ASSERT(destination, "Destination buffer was null");

		const GfxTextureDescriptor& descriptor = source->GetDescriptor();
		const std::vector<GfxMipInfo>& mipsInfo = source->GetMipsInfo();
		const bool isTex3D = descriptor.type == GfxTextureType::TEXTURE_3D;

		std::vector<VkBufferImageCopy> copyRegions;
		copyRegions.resize(mipsInfo.size(), {});

		const size_t sliceSize = destination->GetSize() / descriptor.slices;
		for (uint32_t slice = 0; slice < descriptor.slices; ++slice)
		{
			const uint32_t sliceMipOffset = slice * descriptor.mips;
			for (uint32_t mip = 0; mip < descriptor.mips; ++mip)
			{
				const uint32_t index = sliceMipOffset + mip;
				copyRegions[index].imageSubresource.aspectMask = GetVkAspectMask(descriptor.format);
				copyRegions[index].imageSubresource.mipLevel = mip;
				copyRegions[index].imageSubresource.layerCount = 1;
				copyRegions[index].imageSubresource.baseArrayLayer = slice;
				copyRegions[index].imageExtent.width = descriptor.width >> mip;
				copyRegions[index].imageExtent.height = descriptor.height >> mip;
				copyRegions[index].imageExtent.depth = isTex3D ? descriptor.depth >> mip : 1;
				copyRegions[index].bufferOffset = (sliceSize * slice) + mipsInfo[mip].offset;
			}
		}

		vkCmdCopyImageToBuffer(m_CommandBuffer, source->GetImage(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, destination->GetBuffer(), static_cast<uint32_t>(copyRegions.size()), copyRegions.data());
	}

	void GfxCommandBufferVK::CopyTextureSubresourceToBuffer(GfxTexture* source, const uint32_t mip, const uint32_t slice, GfxBuffer* destination)
	{
		PACHA_ASSERT(source, "Source texture was null");
		PACHA_ASSERT(destination, "Destination buffer was null");

		const GfxTextureDescriptor& descriptor = source->GetDescriptor();
		const bool isTex3D = descriptor.type == GfxTextureType::TEXTURE_3D;

		VkBufferImageCopy copyRegion;
		copyRegion.imageSubresource.aspectMask = GetVkAspectMask(descriptor.format);
		copyRegion.imageSubresource.mipLevel = mip;
		copyRegion.imageSubresource.layerCount = 1;
		copyRegion.imageSubresource.baseArrayLayer = slice;
		copyRegion.imageExtent.width = descriptor.width >> mip;
		copyRegion.imageExtent.height = descriptor.height >> mip;
		copyRegion.imageExtent.depth = isTex3D ? descriptor.depth >> mip : 1;
		copyRegion.bufferOffset = 0;

		vkCmdCopyImageToBuffer(m_CommandBuffer, source->GetImage(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, destination->GetBuffer(), 1, &copyRegion);
	}

	void GfxCommandBufferVK::TransitionBuffer(GfxBuffer* buffer, const GfxResourceState sourceState, const GfxResourceState destinationState)
	{
		PACHA_ASSERT(buffer, "Buffer was null");

		VkBufferMemoryBarrier barrier = {};
		barrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
		barrier.size = VK_WHOLE_SIZE;
		barrier.buffer = buffer->GetBuffer();
		barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.srcAccessMask = GetVkAccessFlags(sourceState);
		barrier.dstAccessMask = GetVkAccessFlags(destinationState);

		vkCmdPipelineBarrier(m_CommandBuffer, GetVkPipelineStageFlags(sourceState), GetVkPipelineStageFlags(destinationState), 0, 0, nullptr, 1, &barrier, 0, nullptr);
	}

	void GfxCommandBufferVK::TransitionTexture(GfxTexture* texture, const GfxResourceState sourceState, const GfxResourceState destinationState)
	{
		PACHA_ASSERT(texture, "Texture was null");
		const GfxTextureDescriptor& descriptor = texture->GetDescriptor();

		VkImageSubresourceRange subresourceRange;
		subresourceRange.aspectMask = GetVkAspectMask(descriptor.format);
		subresourceRange.baseMipLevel = 0;
		subresourceRange.levelCount = descriptor.mips;
		subresourceRange.baseArrayLayer = 0;
		subresourceRange.layerCount = descriptor.slices;

		VkImageMemoryBarrier imageMemoryBarrier = {};
		imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		imageMemoryBarrier.image = texture->GetImage();
		imageMemoryBarrier.subresourceRange = subresourceRange;
		imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		imageMemoryBarrier.srcAccessMask = GetVkAccessFlags(sourceState);
		imageMemoryBarrier.dstAccessMask = GetVkAccessFlags(destinationState);
		imageMemoryBarrier.oldLayout = GetVkImageLayout(sourceState);
		imageMemoryBarrier.newLayout = GetVkImageLayout(destinationState);

		vkCmdPipelineBarrier(m_CommandBuffer, GetVkPipelineStageFlags(sourceState), GetVkPipelineStageFlags(destinationState), 0, 0, nullptr, 0, nullptr, 1, &imageMemoryBarrier);
	}

	void GfxCommandBufferVK::TransitionTextureSubresource(GfxTexture* texture, const uint32_t mip, const uint32_t slice, const GfxResourceState sourceState, const GfxResourceState destinationState)
	{
		PACHA_ASSERT(texture, "Texture was null");

		const GfxTextureDescriptor& descriptor = texture->GetDescriptor();
		PACHA_ASSERT(mip <= descriptor.mips && slice <= descriptor.slices, "Subresource index out of scope");

		VkImageSubresourceRange subresourceRange;
		subresourceRange.aspectMask = GetVkAspectMask(descriptor.format);
		subresourceRange.baseMipLevel = mip;
		subresourceRange.levelCount = 1;
		subresourceRange.baseArrayLayer = slice;
		subresourceRange.layerCount = 1;

		VkImageMemoryBarrier imageMemoryBarrier = {};
		imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		imageMemoryBarrier.image = texture->GetImage();
		imageMemoryBarrier.subresourceRange = subresourceRange;
		imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		imageMemoryBarrier.srcAccessMask = GetVkAccessFlags(sourceState);
		imageMemoryBarrier.dstAccessMask = GetVkAccessFlags(destinationState);
		imageMemoryBarrier.oldLayout = GetVkImageLayout(sourceState);
		imageMemoryBarrier.newLayout = GetVkImageLayout(destinationState);

		vkCmdPipelineBarrier(m_CommandBuffer, GetVkPipelineStageFlags(sourceState), GetVkPipelineStageFlags(destinationState), 0, 0, nullptr, 0, nullptr, 1, &imageMemoryBarrier);
	}

	void GfxCommandBufferVK::BlitTexture(GfxTexture* source, GfxTexture* destination, const GfxFilterMode filterMode)
	{
		PACHA_ASSERT(source, "Source texture was null");
		PACHA_ASSERT(destination, "Destination texture was null");

		const GfxTextureDescriptor& sourceDescriptor = source->GetDescriptor();
		const GfxTextureDescriptor& destinationDescriptor = destination->GetDescriptor();
		const bool isSourceTex3D = sourceDescriptor.type == GfxTextureType::TEXTURE_3D;
		const bool isDestinationTex3D = sourceDescriptor.type == GfxTextureType::TEXTURE_3D;

		std::vector<VkImageBlit> blitRegions;
		blitRegions.resize(sourceDescriptor.mips * sourceDescriptor.slices, {});

		for (uint32_t slice = 0; slice < sourceDescriptor.slices; ++slice)
		{
			const uint32_t sliceMipOffset = slice * sourceDescriptor.mips;
			for (uint32_t mip = 0; mip < sourceDescriptor.mips; ++mip)
			{
				const uint32_t index = sliceMipOffset + mip;

				VkImageSubresourceLayers sourceSubresource = {};
				sourceSubresource.aspectMask = GetVkAspectMask(sourceDescriptor.format);
				sourceSubresource.baseArrayLayer = slice;
				sourceSubresource.layerCount = 1;
				sourceSubresource.mipLevel = mip;

				VkImageSubresourceLayers destinationSubresource = {};
				destinationSubresource.aspectMask = GetVkAspectMask(destinationDescriptor.format);
				destinationSubresource.baseArrayLayer = slice;
				destinationSubresource.layerCount = 1;
				destinationSubresource.mipLevel = mip;

				blitRegions[index].srcOffsets[0] = {};
				blitRegions[index].dstOffsets[0] = {};

				blitRegions[index].srcOffsets[1].x = sourceDescriptor.width >> mip;
				blitRegions[index].srcOffsets[1].y = sourceDescriptor.height >> mip;
				blitRegions[index].srcOffsets[1].z = isSourceTex3D ? sourceDescriptor.depth >> mip : 1;

				blitRegions[index].dstOffsets[1].x = destinationDescriptor.width >> mip;
				blitRegions[index].dstOffsets[1].y = destinationDescriptor.height >> mip;
				blitRegions[index].dstOffsets[1].z = isDestinationTex3D ? destinationDescriptor.depth >> mip : 1;

				blitRegions[index].srcSubresource = sourceSubresource;
				blitRegions[index].dstSubresource = destinationSubresource;
			}
		}

		vkCmdBlitImage(m_CommandBuffer, source->GetImage(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, destination->GetImage(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, static_cast<uint32_t>(blitRegions.size()), blitRegions.data(), GfxSamplerVK::GetVkFilterMode(filterMode));
	}

	void GfxCommandBufferVK::BlitTextureSubresources(GfxTexture* source, const uint32_t sourceMip, const uint32_t sourceSlice, GfxTexture* destination, const uint32_t destinationMip, const uint32_t destinationSlice, const GfxFilterMode filterMode)
	{
		PACHA_ASSERT(source, "Source texture was null");
		PACHA_ASSERT(destination, "Destination texture was null");

		const GfxTextureDescriptor& sourceDescriptor = source->GetDescriptor();
		const GfxTextureDescriptor& destinationDescriptor = destination->GetDescriptor();
		const bool isSourceTex3D = sourceDescriptor.type == GfxTextureType::TEXTURE_3D;
		const bool isDestinationTex3D = sourceDescriptor.type == GfxTextureType::TEXTURE_3D;

		VkImageSubresourceLayers sourceSubresource = {};
		sourceSubresource.aspectMask = GetVkAspectMask(sourceDescriptor.format);
		sourceSubresource.baseArrayLayer = sourceSlice;
		sourceSubresource.layerCount = 1;
		sourceSubresource.mipLevel = sourceMip;

		VkImageSubresourceLayers destinationSubresource = {};
		destinationSubresource.aspectMask = GetVkAspectMask(destinationDescriptor.format);
		destinationSubresource.baseArrayLayer = destinationSlice;
		destinationSubresource.layerCount = 1;
		destinationSubresource.mipLevel = destinationMip;

		VkImageBlit blitRegion;
		blitRegion.srcOffsets[0] = {};
		blitRegion.dstOffsets[0] = {};

		blitRegion.srcOffsets[1].x = sourceDescriptor.width >> sourceMip;
		blitRegion.srcOffsets[1].y = sourceDescriptor.height >> sourceMip;
		blitRegion.srcOffsets[1].z = isSourceTex3D ? sourceDescriptor.depth >> sourceMip : 1;

		blitRegion.dstOffsets[1].x = destinationDescriptor.width >> destinationMip;
		blitRegion.dstOffsets[1].y = destinationDescriptor.height >> destinationMip;
		blitRegion.dstOffsets[1].z = isDestinationTex3D ? destinationDescriptor.depth >> destinationMip : 1;

		blitRegion.srcSubresource = sourceSubresource;
		blitRegion.dstSubresource = destinationSubresource;

		vkCmdBlitImage(m_CommandBuffer, source->GetImage(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, destination->GetImage(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &blitRegion, GfxSamplerVK::GetVkFilterMode(filterMode));
	}

	void GfxCommandBufferVK::DrawRenderBatch(const GfxSceneView* /*sceneView*/, const std::vector<GfxRenderBatch>& renderBatches, const PachaId& passId)
	{
		PACHA_ASSERT(passId, "Invalid PassId");
		
		if (renderBatches.empty())
			return;
		
		constexpr uint64_t kModelMask = 0xFFFFFF0000000000;
		constexpr uint64_t kMaterialMask = 0x000000FFFF000000;

		uint64_t currentBatchHash = 0;
		for (auto& renderBatch : renderBatches)
		{
			const GfxDrawCall& drawCall = renderBatch.drawCall;
			if (currentBatchHash != drawCall.hash)
			{
				const bool isModelDifferent = (currentBatchHash & kModelMask) != (drawCall.hash & kModelMask);
				const bool isMaterialDifferent = (currentBatchHash & kMaterialMask) != (drawCall.hash & kMaterialMask);
				
				const GfxRenderable* const& renderable = renderBatch.drawCall.renderable;
				const GfxMeshRendererProxy& renderProxy = renderable->GetMeshRendererProxy();
				const GfxMesh* mesh = renderProxy.GetMeshes()[drawCall.meshIndex];
				
				GfxVertexBuffer* vertexBuffer = mesh->GetVertexBuffer();
				GfxIndexBufferBase* indexBuffer = mesh->GetIndexBuffer();
				
				const uint32_t bindlessIndex = vertexBuffer->GetBindlessIndex();
				const bool usingBindless = bindlessIndex != GfxBindlessResourceManager::kInvalidBindlessIndex;

				if (isModelDifferent || isMaterialDifferent)
				{
					if (isModelDifferent)
					{
						if(!usingBindless)
						{
							VkDeviceSize bufferOffsets[2] = {};
							const VkBuffer buffers[2] =
							{
								vertexBuffer->GetBuffer(),
								vertexBuffer->GetBuffer()
							};

							GfxModel* model = mesh->GetModel();
							if (mesh->GetVertexLayout() & GfxVertexLayout_ALPHA_MASK)
							{
								bufferOffsets[0] = model->GetAlphaPositionDataOffset();
								bufferOffsets[1] = model->GetAlphaInterleavedDataOffset();
							}
							else
							{
								bufferOffsets[0] = 0;
								bufferOffsets[1] = model->GetInterleavedDataOffset();
							}

							vkCmdBindVertexBuffers(m_CommandBuffer, 0, 2, buffers, 0);
						}

						vkCmdBindIndexBuffer(m_CommandBuffer, indexBuffer->GetBuffer(), 0, GfxBuffer::GetVkIndexType(indexBuffer->GetType()));
					}
					
					if (isMaterialDifferent)
					{
						//vkCmdBindPipeline(m_CommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, combinationData.pipeline);
						//vkCmdBindDescriptorSets(m_CommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, combinationData.pipelineLayout, 0, static_cast<uint32_t>(descriptorSets.size()), descriptorSets.data(), 0, nullptr);
					}
				}

				if (usingBindless)
				{
					//ShaderPushConstants pushConstants;
					//pushConstants.batchOffset = batch.batchOffset;
					//pushConstants.viewProjection = viewProjection;
					//vkCmdPushConstants(m_CommandBuffer, combinationData.pipelineLayout, VK_SHADER_STAGE_ALL, 0, sizeof(ShaderPushConstants), &pushConstants);
				}

				vkCmdDrawIndexed(m_CommandBuffer, mesh->GetIndexCount(), renderBatch.instanceCount, mesh->GetFirstIndex(), mesh->GetFirstVertex(), 0);

			}

			currentBatchHash = drawCall.hash;
		}
	}

	VkCommandBuffer& GfxCommandBufferVK::GetBuffer()
	{
		return m_CommandBuffer;
	}

	VkAccessFlags GfxCommandBufferVK::GetVkAccessFlags(const GfxResourceState& resourceState)
	{
		switch (resourceState)
		{
			default:
				LOGCRITICAL("Resource state not registered in this function");
				return 0;

			case GfxResourceState::UNDEFINED:
				return 0;

			case GfxResourceState::INDEX_BUFFER:
				return VK_ACCESS_INDEX_READ_BIT;

			case GfxResourceState::VERTEX_BUFFER:
				return VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT;

			case GfxResourceState::CONSTANT_BUFFER:
				return VK_ACCESS_UNIFORM_READ_BIT;

			case GfxResourceState::RENDER_TARGET:
				return VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

			case GfxResourceState::UNORDERED_ACCESS:
				return VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_SHADER_WRITE_BIT;

			case GfxResourceState::DEPTH_READ:
				return VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT;

			case GfxResourceState::DEPTH_WRITE:
				return VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

			case GfxResourceState::SHADER_RESOURCE:
				return VK_ACCESS_SHADER_READ_BIT;

			case GfxResourceState::INDIRECT_ARGUMENT:
				return VK_ACCESS_INDIRECT_COMMAND_READ_BIT;

			case GfxResourceState::COPY_DESTINATION:
				return VK_ACCESS_TRANSFER_WRITE_BIT;

			case GfxResourceState::COPY_SOURCE:
				return VK_ACCESS_TRANSFER_READ_BIT;

			case GfxResourceState::PRESENT:
				return VK_ACCESS_MEMORY_READ_BIT;

			case GfxResourceState::SHADING_RATE_MASK:
				return VK_ACCESS_FRAGMENT_SHADING_RATE_ATTACHMENT_READ_BIT_KHR;

			case GfxResourceState::ACCELERATION_STRUCTURE_BUILD:
				return VK_ACCESS_ACCELERATION_STRUCTURE_WRITE_BIT_KHR;

			case GfxResourceState::ACCELERATION_STRUCTURE_SHADER_RESOURCE:
				return VK_ACCESS_ACCELERATION_STRUCTURE_READ_BIT_KHR;
		}
	}

	VkPipelineStageFlags GfxCommandBufferVK::GetVkPipelineStageFlags(const GfxResourceState& resourceState)
	{
		switch (resourceState)
		{
			default:
				LOGCRITICAL("Resource state not registered in this function");
				return VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;

			case GfxResourceState::UNDEFINED:
				return VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;

			case GfxResourceState::INDEX_BUFFER:
				return VK_PIPELINE_STAGE_VERTEX_INPUT_BIT;

			case GfxResourceState::VERTEX_BUFFER:
				return VK_PIPELINE_STAGE_VERTEX_INPUT_BIT;

			case GfxResourceState::CONSTANT_BUFFER:
				return	VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT |
					VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT |
					VK_PIPELINE_STAGE_GEOMETRY_SHADER_BIT |
					VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT |
					VK_PIPELINE_STAGE_TESSELLATION_CONTROL_SHADER_BIT |
					VK_PIPELINE_STAGE_TESSELLATION_EVALUATION_SHADER_BIT;

			case GfxResourceState::RENDER_TARGET:
				return VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

			case GfxResourceState::UNORDERED_ACCESS:
				return	VK_PIPELINE_STAGE_VERTEX_SHADER_BIT |
					VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT |
					VK_PIPELINE_STAGE_GEOMETRY_SHADER_BIT |
					VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT |
					VK_PIPELINE_STAGE_TESSELLATION_CONTROL_SHADER_BIT |
					VK_PIPELINE_STAGE_TESSELLATION_EVALUATION_SHADER_BIT;

			case GfxResourceState::DEPTH_READ:
			case GfxResourceState::DEPTH_WRITE:
				return	VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT |
					VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;

			case GfxResourceState::SHADER_RESOURCE:
				return VK_PIPELINE_STAGE_VERTEX_SHADER_BIT |
					VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT |
					VK_PIPELINE_STAGE_GEOMETRY_SHADER_BIT |
					VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT |
					VK_PIPELINE_STAGE_TESSELLATION_CONTROL_SHADER_BIT |
					VK_PIPELINE_STAGE_TESSELLATION_EVALUATION_SHADER_BIT;

			case GfxResourceState::INDIRECT_ARGUMENT:
				return VK_PIPELINE_STAGE_DRAW_INDIRECT_BIT;

			case GfxResourceState::COPY_DESTINATION:
				return VK_PIPELINE_STAGE_TRANSFER_BIT;

			case GfxResourceState::COPY_SOURCE:
				return VK_PIPELINE_STAGE_TRANSFER_BIT;

			case GfxResourceState::PRESENT:
				return VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;

			case GfxResourceState::SHADING_RATE_MASK:
				return VK_PIPELINE_STAGE_FRAGMENT_SHADING_RATE_ATTACHMENT_BIT_KHR;

			case GfxResourceState::ACCELERATION_STRUCTURE_BUILD:
				return VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_KHR;

			case GfxResourceState::ACCELERATION_STRUCTURE_SHADER_RESOURCE:
				return VK_PIPELINE_STAGE_RAY_TRACING_SHADER_BIT_KHR;
		}
	}

	VkImageLayout GfxCommandBufferVK::GetVkImageLayout(const GfxResourceState& resourceState)
	{
		switch (resourceState)
		{
			default:
				LOGCRITICAL("Resource state not registered in this function");
				return VK_IMAGE_LAYOUT_UNDEFINED;

			case GfxResourceState::INDEX_BUFFER:
			case GfxResourceState::VERTEX_BUFFER:
			case GfxResourceState::CONSTANT_BUFFER:
			case GfxResourceState::INDIRECT_ARGUMENT:
			case GfxResourceState::ACCELERATION_STRUCTURE_BUILD:
			case GfxResourceState::ACCELERATION_STRUCTURE_SHADER_RESOURCE:
				LOGCRITICAL("Resource state is not a texture compatible state");
				return VK_IMAGE_LAYOUT_UNDEFINED;

			case GfxResourceState::UNDEFINED:
				return VK_IMAGE_LAYOUT_UNDEFINED;

			case GfxResourceState::RENDER_TARGET:
				return VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

			case GfxResourceState::UNORDERED_ACCESS:
				return	VK_IMAGE_LAYOUT_GENERAL;

			case GfxResourceState::DEPTH_READ:
				return	VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;

			case GfxResourceState::DEPTH_WRITE:
				return	VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

			case GfxResourceState::SHADER_RESOURCE:
				return VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

			case GfxResourceState::COPY_DESTINATION:
				return VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;

			case GfxResourceState::COPY_SOURCE:
				return VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;

			case GfxResourceState::PRESENT:
				return VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

			case GfxResourceState::SHADING_RATE_MASK:
				return VK_IMAGE_LAYOUT_FRAGMENT_SHADING_RATE_ATTACHMENT_OPTIMAL_KHR;
		}
	}

	VkImageAspectFlags GfxCommandBufferVK::GetVkAspectMask(const GfxPixelFormat& format)
	{
		if (format == GfxPixelFormat::D16_UNORM || format == GfxPixelFormat::D32_SFLOAT)
			return VK_IMAGE_ASPECT_DEPTH_BIT;

		if (format == GfxPixelFormat::D24_UNORM_S8_UINT || format == GfxPixelFormat::D32_SFLOAT_S8_UINT)
			return VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;

		return VK_IMAGE_ASPECT_COLOR_BIT;
	}

	void GfxCommandBufferVK::SetDebugName()
	{
		GfxDevice& gfxDevice = GfxDevice::GetInstance();
		gfxDevice.SetObjectDebugName({ m_Name, VK_OBJECT_TYPE_COMMAND_BUFFER, reinterpret_cast<uint64_t>(m_CommandBuffer) });
	}
}
#pragma once
#if RENDERER_VK
#include "graphics/resources/commandbuffer/GfxCommandBufferVK.h"
#endif

namespace Pacha
{
#if RENDERER_VK
	using GfxCommandBuffer = GfxCommandBufferVK;
#endif
}
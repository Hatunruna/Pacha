#pragma once
#include "graphics/structures/GfxRect.h"
#include "graphics/structures/GfxViewport.h"
#include "graphics/renderpipeline/GfxRenderBatch.h"

#include "graphics/resources/GfxResourceState.h"
#include "graphics/resources/buffer/GfxBuffer.h"
#include "graphics/resources/sampler/GfxSampler.h"
#include "graphics/resources/texture/GfxTexture.h"
#include "graphics/resources/rendertarget/GfxRenderTarget.h"

#include <glm/glm.hpp>

namespace Pacha
{	
	union GfxClearColorValue
	{
		glm::vec4 fColor;
		glm::ivec4 iColor;
		glm::uvec4 uColor;
	};

	struct GfxClearDepthStencilValue
	{
		float depth;
		uint32_t stencil;
	};

	union GfxClearValue
	{
		GfxClearColorValue color;
		GfxClearDepthStencilValue depthStencil;
	};

	typedef std::array<GfxClearValue, 9> GfxClearValues;

	class GfxCommandBufferBase
	{
	public:
		virtual ~GfxCommandBufferBase() {}

		const std::string& GetName() const { return m_Name; }
		void SetName(const std::string& name) { m_Name = name; SetDebugName(); };

		virtual void BeginCommandBuffer() = 0;
		virtual void EndCommandBuffer() = 0;

		virtual void BeginSectionMarker(const std::string& markerName, const glm::vec4& color) = 0;
		virtual void EndSectionMarker() = 0;

		virtual void BeginRenderPass(GfxRenderTarget& renderTarget, const GfxClearValues& clearValues = {}, const GfxSubResourceTarget& subResource = {}) = 0;
		virtual void BeginRenderPass(GfxRenderTarget& renderTarget, const GfxViewport& viewport, const GfxClearValues& clearValues = {}, const GfxSubResourceTarget& subResource = {}) = 0;
		virtual void BeginRenderPass(GfxRenderTarget& renderTarget, const GfxViewport& viewport, const GfxRect& scissor, const GfxClearValues& clearValues = {}, const GfxSubResourceTarget& subResource = {}) = 0;
		virtual void EndRenderPass() = 0;

		virtual void CopyBufferToBuffer(GfxBuffer* source, GfxBuffer* destination, const size_t size = 0, const size_t sourceDataOffset = 0, const size_t destinationOffset = 0) = 0;
		virtual void CopyBufferToTexture(GfxBuffer* source, GfxTexture* destination, const size_t sourceDataOffset = 0) = 0;
		virtual void CopyBufferToTextureSubresource(GfxBuffer* source, GfxTexture* destination, const uint32_t mip, const uint32_t slice, const size_t sourceDataOffset = 0) = 0;

		virtual void CopyTextureToTexture(GfxTexture* source, GfxTexture* destination) = 0;
		virtual void CopyTextureSubresourceToTextureSubresource(GfxTexture* source, const uint32_t sourceMip, const uint32_t sourceSlice, GfxTexture* destination, const uint32_t destinationMip, const uint32_t destinationSlice) = 0;

		virtual void CopyTextureToBuffer(GfxTexture* source, GfxBuffer* destination) = 0;
		virtual void CopyTextureSubresourceToBuffer(GfxTexture* source, const uint32_t mip, const uint32_t slice, GfxBuffer* destination) = 0;

		virtual void TransitionBuffer(GfxBuffer* buffer, const GfxResourceState sourceState, const GfxResourceState destinationState) = 0;
		virtual void TransitionTexture(GfxTexture* texture, const GfxResourceState sourceState, const GfxResourceState destinationState) = 0;
		virtual void TransitionTextureSubresource(GfxTexture* texture, const uint32_t mip, const uint32_t slice, const GfxResourceState sourceState, const GfxResourceState destinationState) = 0;
	
		virtual void BlitTexture(GfxTexture* source, GfxTexture* destination, const GfxFilterMode filterMode = GfxFilterMode::LINEAR) = 0;
		virtual void BlitTextureSubresources(GfxTexture* source, const uint32_t sourceMip, const uint32_t sourceSlice, GfxTexture* destination, const uint32_t destinationMip, const uint32_t destinationSlice, const GfxFilterMode filterMode = GfxFilterMode::LINEAR) = 0;

		virtual void DrawRenderBatch(const class GfxSceneView* scene, const std::vector<GfxRenderBatch>& renderBatches, const PachaId& passId) = 0;

	protected:
		std::string m_Name = {};
		virtual void SetDebugName() = 0;
	};
}
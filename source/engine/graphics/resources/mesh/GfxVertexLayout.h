#pragma once
#include "graphics/resources/buffer/GfxVertexDescriptor.h"

namespace Pacha
{
	enum GfxVertexLayout
	{
		GfxVertexLayout_CUSTOM				= 0,

		GfxVertexLayout_COMPRESSED			= 1 << 0,
		GfxVertexLayout_COMPRESSED_ALPHA	= 1 << 1,
		
		GfxVertexLayout_UNCOMPRESSED		= 1 << 2,
		GfxVertexLayout_UNCOMPRESSED_ALPHA	= 1 << 3,

		GfxVertexLayout_ALPHA_MASK			= GfxVertexLayout_COMPRESSED_ALPHA | GfxVertexLayout_UNCOMPRESSED_ALPHA
	};

	enum GfxVertexLayoutGrouping
	{
		GfxVertexLayoutGrouping_COMPRESSED		= GfxVertexLayout_COMPRESSED | GfxVertexLayout_COMPRESSED_ALPHA,
		GfxVertexLayoutGrouping_UNCOMPRESSED	= GfxVertexLayout_UNCOMPRESSED | GfxVertexLayout_UNCOMPRESSED_ALPHA
	};

	enum GfxVertexLayoutDataNeeds
	{
		GfxVertexLayoutDataNeeds_NORMALS	= GfxVertexLayoutGrouping_COMPRESSED | GfxVertexLayoutGrouping_UNCOMPRESSED,
		GfxVertexLayoutDataNeeds_TANGENTS	= GfxVertexLayoutGrouping_COMPRESSED | GfxVertexLayoutGrouping_UNCOMPRESSED,
		GfxVertexLayoutDataNeeds_UVS		= GfxVertexLayoutGrouping_COMPRESSED | GfxVertexLayoutGrouping_UNCOMPRESSED
	};

	namespace VertexLayout
	{
		//{ POSITION }, { TEXCOORD, NORMAL, TANGENT }
		static const GfxVertexDescriptor Standard =
		{{
			{{
				{ GfxVertexElementType::POSITION,	GfxVertexFormat::RGB32_FLOAT  },
			}},
			{{
				{ GfxVertexElementType::TEXCOORD0,	GfxVertexFormat::RG32_FLOAT   },
				{ GfxVertexElementType::NORMAL,		GfxVertexFormat::RGB32_FLOAT  },
				{ GfxVertexElementType::TANGENT,	GfxVertexFormat::RGBA32_FLOAT }
			}}
		}};

		//{ POSITION, TEXCOORD }, { NORMAL, TANGENT }
		static const GfxVertexDescriptor StandardAlpha =
		{{
			{{
				{ GfxVertexElementType::POSITION,	GfxVertexFormat::RGB32_FLOAT  },
				{ GfxVertexElementType::TEXCOORD0,	GfxVertexFormat::RG32_FLOAT   }
			}},
			{{
				{ GfxVertexElementType::NORMAL,		GfxVertexFormat::RGB32_FLOAT  },
				{ GfxVertexElementType::TANGENT,	GfxVertexFormat::RGBA32_FLOAT }
			}}
		}};

		//{ POSITION }, { TEXCOORD, QTANGENT }
		static const GfxVertexDescriptor Compressed =
		{{
			{{
				{ GfxVertexElementType::POSITION,	GfxVertexFormat::RGB16_SNORM  },
			}},
			{{
				{ GfxVertexElementType::TEXCOORD0,	GfxVertexFormat::RG16_UNORM   },
				{ GfxVertexElementType::NORMAL,		GfxVertexFormat::RGBA16_SNORM }
			}}
		}};

		//{ POSITION, TEXCOORD }, { QTANGENT }
		static const GfxVertexDescriptor CompressedAlpha =
		{{
			{{
				{ GfxVertexElementType::POSITION,	GfxVertexFormat::RGB16_SNORM  },
				{ GfxVertexElementType::TEXCOORD0,	GfxVertexFormat::RG16_UNORM   }
			}},
			{{
				{ GfxVertexElementType::NORMAL,		GfxVertexFormat::RGBA16_SNORM },
			}}
		}};

		inline const GfxVertexDescriptor& GetDescriptor(const GfxVertexLayout layout)
		{
			switch (layout)
			{
				case GfxVertexLayout::GfxVertexLayout_UNCOMPRESSED:			return Standard;
				case GfxVertexLayout::GfxVertexLayout_UNCOMPRESSED_ALPHA:	return StandardAlpha;
				case GfxVertexLayout::GfxVertexLayout_COMPRESSED:			return Compressed;
				case GfxVertexLayout::GfxVertexLayout_COMPRESSED_ALPHA:		return CompressedAlpha;
			}

			return Standard;
		}
	}
}
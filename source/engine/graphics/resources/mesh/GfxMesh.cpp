#include "GfxMesh.h"
#include "graphics/resources/mesh/GfxModel.h"

namespace Pacha
{
	GfxMesh::GfxMesh(const GfxPrimitiveType primitiveType, const GfxVertexLayout vertexLayout, GfxModel* parentModel, const uint32_t subMeshIndex)
		: m_PrimitiveType(primitiveType)
		, m_VertexLayout(vertexLayout)
		, m_ParentModel(parentModel)
		, m_SubMeshIndex(subMeshIndex)
	{
		if (m_ParentModel)
			m_ModelId = m_ParentModel->GetId();
		else
			m_ModelId = static_cast<uint64_t>(GfxModel::GenerateModelId());
	}

	GfxMesh::~GfxMesh()
	{
		if (!m_ParentModel)
			GfxModel::ReturnModelId(static_cast<uint16_t>(m_ModelId));
	}

	void GfxMesh::Initialize(GfxVertexBuffer* vertexBuffer, GfxIndexBufferBase* indexBuffer)
	{
		PACHA_ASSERT(vertexBuffer, "VertexBuffer can not be null");
		PACHA_ASSERT(indexBuffer, "IndexBuffer can not be null");

		m_VertexBuffer = vertexBuffer;
		m_IndexBuffer = indexBuffer;

		const GfxVertexDescriptor& descriptor = VertexLayout::GetDescriptor(m_VertexLayout);
		m_VertexCount = static_cast<uint32_t>(m_VertexBuffer->GetSize() / descriptor.GetStride());

		GfxIndexBufferBase* indexBufferBase = static_cast<GfxIndexBufferBase*>(m_IndexBuffer);
		m_IndexCount = static_cast<uint32_t>(m_IndexBuffer->GetSize() / indexBufferBase->GetElementSize());
	}
	
	void GfxMesh::Initialize(GfxVertexBuffer* vertexBuffer, const uint32_t vertexCount, const uint32_t firstVertex, GfxIndexBufferBase* indexBuffer, const uint32_t indexCount, const uint32_t firstIndex)
	{
		PACHA_ASSERT(vertexBuffer, "VertexBuffer can not be null");
		PACHA_ASSERT(indexBuffer, "IndexBuffer can not be null");

		m_FirstIndex = firstIndex;
		m_IndexCount = indexCount;
		m_IndexBuffer = indexBuffer;
		
		m_FirstVertex = firstVertex;
		m_VertexCount = vertexCount;
		m_VertexBuffer = vertexBuffer;

		if (m_VertexCount == 0)
		{
			const GfxVertexDescriptor& descriptor = VertexLayout::GetDescriptor(m_VertexLayout);
			m_VertexCount = static_cast<uint32_t>(m_VertexBuffer->GetSize() / descriptor.GetStride());
		}

		if (m_IndexCount == 0)
		{
			GfxIndexBufferBase* indexBufferBase = static_cast<GfxIndexBufferBase*>(m_IndexBuffer);
			m_IndexCount = static_cast<uint32_t>(m_IndexBuffer->GetSize() / indexBufferBase->GetElementSize());
		}
	}
	
	void GfxMesh::SetName(const std::string& name)
	{
		m_Name = name;
	}
	
	const std::string& GfxMesh::GetName()
	{
		return m_Name;
	}
	
	void GfxMesh::SetAABB(const AABB& aabb)
	{
		m_AABB = aabb;
	}
	
	const AABB& GfxMesh::GetAABB()
	{
		return m_AABB;
	}

	GfxModel* GfxMesh::GetModel() const
	{
		return m_ParentModel;
	}
	
	uint32_t GfxMesh::GetSubMeshIndex() const
	{
		return m_SubMeshIndex;
	}
	
	uint64_t GfxMesh::GetModelId() const
	{
		return m_ModelId;
	}
}

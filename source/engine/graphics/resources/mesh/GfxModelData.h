#pragma once
#include "GfxMesh.h"
#include "math/AABB.h"
#include "utilities/PachaUUID.h"
#include "graphics/resources/material/GfxMaterial.h"

#include <glm/glm.hpp>

namespace Pacha
{
	struct GfxModelData
	{
	public:
		struct Node
		{
			std::string name = {};
			std::vector<uint32_t> meshes = {};
			std::vector<uint32_t> children = {};
			glm::mat4 transform = glm::mat4(1.f);
		};

		struct Mesh
		{
			std::string name = {};

			AABB aabb = {};
			uint32_t material = UINT32_MAX;
			GfxPrimitiveType primitiveType = GfxPrimitiveType::TRIANGLES;

			uint32_t indexCount = 0;
			uint32_t firstIndex = 0;

			uint32_t vertexCount = 0;
			uint32_t firstVertex = 0;

			GfxVertexLayout vertexLayout = GfxVertexLayout_COMPRESSED;
		};

		struct Material
		{
			std::string name = {};

			glm::vec4 diffuse = glm::vec4(1.f);
			glm::vec3 emissive = glm::vec3(1.f);
			float metallic = 0.f;
			float roughness = 0.5f;

			float alphaCutoff = 0.5f;
			bool doubleSided = false;
			GfxRenderType renderType = GfxRenderType::OPAQUE;

			PachaUUID diffuseTexture;
			PachaUUID metallicRoughnessTexture;
			PachaUUID normalTexture;
			PachaUUID occlusionTexture;
			PachaUUID emissiveTexture;
		};

		std::vector<GfxModelData::Node> nodes;
		std::vector<GfxModelData::Mesh> meshes;
		std::vector<GfxModelData::Material> materials;

		GfxIndexType indexType;
		size_t indexDataSize;
		size_t vertexDataSize;
		size_t interleavedDataOffset;
		size_t alphaPositionDataOffset;
		size_t alphaInterleavedDataOffset;
	};
}
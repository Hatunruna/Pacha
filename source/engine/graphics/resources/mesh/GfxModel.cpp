#include "GfxModel.h"

namespace Pacha
{
	SafeQueue<uint16_t> GfxModel::m_FreeIndexList;
	std::atomic_uint16_t GfxModel::m_ModelCounter;

	GfxModel::GfxModel(GfxVertexBuffer* vertexBuffer, GfxIndexBufferBase* indexBuffer, const GfxModelData& modelData)
		: m_VertexBuffer(vertexBuffer)
		, m_IndexBuffer(indexBuffer)
	{
		PACHA_ASSERT(vertexBuffer, "Vertex Buffer cannot be null");
		PACHA_ASSERT(indexBuffer, "Index Buffer cannot be null");

		m_Id = GenerateModelId();
		PACHA_ASSERT(m_ModelCounter < UINT16_MAX, "Amount of models exceeded");

		m_InterleavedDataOffset = modelData.interleavedDataOffset;
		m_AlphaPositionDataOffset = modelData.alphaPositionDataOffset;
		m_AlphaInterleavedDataOffset = modelData.alphaInterleavedDataOffset;

		InitializeMeshses(modelData);
		InitializeMaterials(modelData);
	}

	GfxModel::~GfxModel()
	{
		ReturnModelId(m_Id);

		if (m_VertexBuffer)
			delete m_VertexBuffer;

		if (m_IndexBuffer)
			delete m_IndexBuffer;

		for (auto& mesh : m_Meshes)
			delete mesh;
	}

	void GfxModel::SetName(const std::string& name)
	{
		m_Name = name;
	}

	const std::string& GfxModel::GetName()
	{
		return m_Name;
	}

	GfxVertexBuffer* const& GfxModel::GetVertexBuffer() const
	{
		return m_VertexBuffer;
	}

	GfxIndexBufferBase* const& GfxModel::GetIndexBuffer() const
	{
		return m_IndexBuffer;
	}

	GfxMesh* GfxModel::GetMesh(const size_t meshIndex) const
	{
		PACHA_ASSERT(meshIndex < m_Meshes.size(), "Mesh index out of range");
		return m_Meshes.at(meshIndex);
	}

	const std::vector<GfxMesh*>& GfxModel::GetMeshses() const
	{
		return m_Meshes;
	}

	GfxMaterial* GfxModel::GetMaterial(const size_t materialIndex) const
	{
		PACHA_ASSERT(materialIndex < m_Materials.size(), "Material index out of range");
		return m_Materials.at(materialIndex);
	}

	const std::vector<GfxMaterial*>& GfxModel::GetMaterials() const
	{
		return m_Materials;
	}

	uint64_t GfxModel::GetId() const
	{
		return static_cast<uint64_t>(m_Id);
	}

	size_t GfxModel::GetInterleavedDataOffset() const
	{
		return m_InterleavedDataOffset;
	}

	size_t GfxModel::GetAlphaPositionDataOffset() const
	{
		return m_AlphaPositionDataOffset;
	}

	size_t GfxModel::GetAlphaInterleavedDataOffset() const
	{
		return m_AlphaInterleavedDataOffset;
	}

	void GfxModel::InitializeMeshses(const GfxModelData& modelData)
	{
		for (size_t i = 0; i < modelData.meshes.size(); ++i)
		{
			const GfxModelData::Mesh& mesh = modelData.meshes[i];

			GfxMesh* gfxMesh = new GfxMesh(mesh.primitiveType, mesh.vertexLayout, this, static_cast<uint32_t>(i));
			gfxMesh->Initialize(m_VertexBuffer, mesh.vertexCount, mesh.firstVertex, m_IndexBuffer, mesh.indexCount, mesh.firstIndex);
			gfxMesh->SetName(mesh.name);
			gfxMesh->SetAABB(mesh.aabb);
			m_Meshes.push_back(gfxMesh);
		}
	}

	void GfxModel::InitializeMaterials(const GfxModelData& /*modelData*/)
	{
	}

	uint16_t GfxModel::GenerateModelId()
	{
		uint16_t id = 0;
		if (!m_FreeIndexList.TryFront(id))
			id = m_ModelCounter.fetch_add(1);
		return id;
	}

	void GfxModel::ReturnModelId(uint16_t modelId)
	{
		m_FreeIndexList.Push(modelId);
	}
}
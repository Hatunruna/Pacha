#pragma once
#include "GfxModelData.h"
#include "graphics/resources/buffer/GfxIndexBuffer.h"
#include "graphics/resources/buffer/GfxVertexBuffer.h"

namespace Pacha
{
	class GfxModel
	{
	public:
		GfxModel(GfxVertexBuffer* vertexBuffer, GfxIndexBufferBase* indexBuffer, const GfxModelData& modelData);
		~GfxModel();

		void SetName(const std::string& name);
		const std::string& GetName();

		GfxVertexBuffer* const& GetVertexBuffer() const;
		GfxIndexBufferBase* const& GetIndexBuffer() const;

		GfxMesh* GetMesh(const size_t meshIndex) const;
		const std::vector<GfxMesh*>& GetMeshses() const;

		GfxMaterial* GetMaterial(const size_t materialIndex) const;
		const std::vector<GfxMaterial*>& GetMaterials() const;

		uint64_t GetId() const;
		size_t GetInterleavedDataOffset() const;
		size_t GetAlphaPositionDataOffset() const;
		size_t GetAlphaInterleavedDataOffset() const;

	private:
		friend class GfxMesh;

		std::string m_Name = {};
		GfxVertexBuffer* m_VertexBuffer = nullptr;
		GfxIndexBufferBase* m_IndexBuffer = nullptr;

		size_t m_InterleavedDataOffset = 0;
		size_t m_AlphaPositionDataOffset = 0;
		size_t m_AlphaInterleavedDataOffset = 0;

		std::vector<GfxMesh*> m_Meshes;
		std::vector<GfxMaterial*> m_Materials;

		uint16_t m_Id = 0;
		static SafeQueue<uint16_t> m_FreeIndexList;
		static std::atomic_uint16_t m_ModelCounter;

		static uint16_t GenerateModelId();
		static void ReturnModelId(uint16_t modelId);

		void InitializeMeshses(const GfxModelData& modelData);
		void InitializeMaterials(const GfxModelData& modelData);
	};
}
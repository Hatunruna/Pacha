#pragma once
#include "math/AABB.h"
#include "graphics/resources/mesh/GfxVertexLayout.h"
#include "graphics/resources/buffer/GfxIndexBuffer.h"
#include "graphics/resources/buffer/GfxVertexBuffer.h"

namespace Pacha
{
	enum class GfxPrimitiveType
	{
		POINTS,
		LINES,
		LINE_STRIP,
		TRIANGLES,
		TRIANGLE_STRIP,
		TRIANGLE_FAN,
		PATCH_LIST
	};

	class GfxMesh
	{
	public:
		GfxMesh(const GfxPrimitiveType primitiveType, const GfxVertexLayout vertexLayout, class GfxModel* parentModel = nullptr, const uint32_t subMeshIndex = 0);
		virtual ~GfxMesh();

		void Initialize(GfxVertexBuffer* vertexBuffer, GfxIndexBufferBase* indexBuffer);
		void Initialize(GfxVertexBuffer* vertexBuffer, const uint32_t vertexCount, const uint32_t firstVertex, GfxIndexBufferBase* indexBuffer, const uint32_t indexCount, const uint32_t firstIndex);

		void SetName(const std::string& name);
		const std::string& GetName();

		void SetAABB(const AABB& aabb);
		const AABB& GetAABB();

		class GfxModel* GetModel() const;
		uint32_t GetSubMeshIndex() const;

		uint32_t GetFirstIndex() const { return m_FirstIndex; }
		uint32_t GetIndexCount() const { return m_IndexCount; }
		GfxIndexBufferBase* GetIndexBuffer() const { return m_IndexBuffer; }

		uint32_t GetFirstVertex() const { return m_FirstVertex; }
		uint32_t GetVertexCount() const { return m_VertexCount; }
		GfxVertexBuffer* GetVertexBuffer() const { return m_VertexBuffer; }

		uint64_t GetModelId() const;
		GfxPrimitiveType GetPrimitiveType() const { return m_PrimitiveType; }
		GfxVertexLayout GetVertexLayout() const { return m_VertexLayout; }

	protected:
		AABB m_AABB = {};
		std::string m_Name = {};

		uint64_t m_ModelId = 0;
		uint32_t m_SubMeshIndex = 0;
		class GfxModel* m_ParentModel = nullptr;
		
		GfxPrimitiveType m_PrimitiveType = GfxPrimitiveType::TRIANGLES;
		GfxVertexLayout m_VertexLayout = GfxVertexLayout::GfxVertexLayout_COMPRESSED;

		uint32_t m_FirstIndex = 0;
		uint32_t m_IndexCount = 0;
		GfxIndexBufferBase* m_IndexBuffer = nullptr;
		
		uint32_t m_FirstVertex = 0;
		uint32_t m_VertexCount = 0;
		GfxVertexBuffer* m_VertexBuffer = nullptr;
	};
}
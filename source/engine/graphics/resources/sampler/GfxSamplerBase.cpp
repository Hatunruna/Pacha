#include "GfxSamplerBase.h"

namespace Pacha
{
	GfxSamplerBase::~GfxSamplerBase()
	{
	}
	
	void GfxSamplerBase::Build(const GfxSamplerDescriptor& descriptor)
	{
		m_Descriptor = descriptor;
		BuildSampler();
	}

	const GfxSamplerDescriptor& GfxSamplerBase::GetDescriptor()
	{
		return m_Descriptor;
	}
}

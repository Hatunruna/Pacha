#include "GfxSamplerVK.h"
#include "graphics/device/GfxDevice.h"

namespace Pacha
{
	GfxSamplerVK::~GfxSamplerVK()
	{
		if (m_Sampler != VK_NULL_HANDLE)
		{
			GfxDevice& gfxDevice = GfxDevice::GetInstance();
			vkDestroySampler(gfxDevice.GetVkDevice(), m_Sampler, nullptr);
		}
	}

	const VkSampler& GfxSamplerVK::GetSampler()
	{
		PACHA_ASSERT(m_Sampler != VK_NULL_HANDLE, "Sampler not built");
		return m_Sampler;
	}

	VkSamplerAddressMode GfxSamplerVK::GetVkWrapMode(const GfxWrapMode& wrapMode)
	{
		switch (wrapMode)
		{
			case GfxWrapMode::CLAMP_TO_BORDER:		return VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
			case GfxWrapMode::CLAMP_TO_EDGE:		return VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
			case GfxWrapMode::MIRRORED_REPEAT:		return VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT;
			case GfxWrapMode::MIRROR_CLAMP_TO_EDGE:	return VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE;
			case GfxWrapMode::REPEAT:				return VK_SAMPLER_ADDRESS_MODE_REPEAT;
			default:
				LOGCRITICAL("Wrap mode not registered on this function");
				return VK_SAMPLER_ADDRESS_MODE_MAX_ENUM;
		}
	}

	VkFilter GfxSamplerVK::GetVkFilterMode(const GfxFilterMode& filterMode)
	{
		switch (filterMode)
		{
			case GfxFilterMode::LINEAR:		return VK_FILTER_LINEAR;
			case GfxFilterMode::NEAREST:	return VK_FILTER_NEAREST;
			default:
				LOGCRITICAL("Filter mode not registered on this function");
				return VK_FILTER_MAX_ENUM;
		}
	}

	VkSamplerMipmapMode GfxSamplerVK::GetVkMipMapFilterMode(const GfxFilterMode& filterMode)
	{
		switch (filterMode)
		{
			case GfxFilterMode::LINEAR:		return VK_SAMPLER_MIPMAP_MODE_LINEAR;
			case GfxFilterMode::NEAREST:	return VK_SAMPLER_MIPMAP_MODE_NEAREST;
			default:
				LOGCRITICAL("Filter mode not registered on this function");
				return VK_SAMPLER_MIPMAP_MODE_MAX_ENUM;
		}
	}

	VkCompareOp GfxSamplerVK::GetVkCompareOperation(const GfxCompareOperation& compareOperation)
	{
		switch (compareOperation)
		{
			case GfxCompareOperation::OFF:
			case GfxCompareOperation::ALWAYS:			return VK_COMPARE_OP_ALWAYS;
			case GfxCompareOperation::EQUAL:			return VK_COMPARE_OP_EQUAL;
			case GfxCompareOperation::GREATER:			return VK_COMPARE_OP_GREATER;
			case GfxCompareOperation::GREATER_OR_EQUAL:	return VK_COMPARE_OP_GREATER_OR_EQUAL;
			case GfxCompareOperation::LESS:				return VK_COMPARE_OP_LESS;
			case GfxCompareOperation::LESS_OR_EQUAL:	return VK_COMPARE_OP_LESS_OR_EQUAL;
			case GfxCompareOperation::NEVER:			return VK_COMPARE_OP_NEVER;
			case GfxCompareOperation::NOT_EQUAL:		return VK_COMPARE_OP_NOT_EQUAL;
			default:
				LOGCRITICAL("Compare operation not registered on this function");
				return VK_COMPARE_OP_MAX_ENUM;
		}
	}

	VkBorderColor GfxSamplerVK::GetVkBorderColor(const GfxBorderColor& borderColor)
	{
		switch (borderColor)
		{
			case GfxBorderColor::FLOAT_OPAQUE_BLACK:		return VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK;
			case GfxBorderColor::FLOAT_OPAQUE_WHITE:		return VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
			case GfxBorderColor::FLOAT_TRANSPARENT_BLACK:	return VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK;
			case GfxBorderColor::INT_OPAQUE_BLACK:			return VK_BORDER_COLOR_INT_OPAQUE_BLACK;
			case GfxBorderColor::INT_OPAQUE_WHITE:			return VK_BORDER_COLOR_INT_OPAQUE_WHITE;
			case GfxBorderColor::INT_TRANSPARENT_BLACK:		return VK_BORDER_COLOR_INT_TRANSPARENT_BLACK;
			default:
				LOGCRITICAL("Border color not registered on this function");
				return VK_BORDER_COLOR_MAX_ENUM;
		}
	}

	void GfxSamplerVK::BuildSampler()
	{
		GfxDevice& gfxDevice = GfxDevice::GetInstance();

		if (m_Sampler != VK_NULL_HANDLE)
			vkDestroySampler(gfxDevice.GetVkDevice(), m_Sampler, nullptr);

		VkSamplerCreateInfo samplerCreateInfo{};
		samplerCreateInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		samplerCreateInfo.addressModeU = GetVkWrapMode(m_Descriptor.wrapModeU);
		samplerCreateInfo.addressModeV = GetVkWrapMode(m_Descriptor.wrapModeV);
		samplerCreateInfo.addressModeW = GetVkWrapMode(m_Descriptor.wrapModeW);
		samplerCreateInfo.minFilter = GetVkFilterMode(m_Descriptor.filterMode);
		samplerCreateInfo.magFilter = GetVkFilterMode(m_Descriptor.filterMode);
		samplerCreateInfo.mipmapMode = GetVkMipMapFilterMode(m_Descriptor.mipsFilterMode);
		samplerCreateInfo.borderColor = GetVkBorderColor(m_Descriptor.borderColor);
		samplerCreateInfo.compareOp = GetVkCompareOperation(m_Descriptor.compareOperation);

		samplerCreateInfo.maxLod = m_Descriptor.maxLod;
		samplerCreateInfo.minLod = m_Descriptor.minLod;
		samplerCreateInfo.mipLodBias = m_Descriptor.lodBias;
		samplerCreateInfo.maxAnisotropy = m_Descriptor.anisotropy;
		samplerCreateInfo.anisotropyEnable = m_Descriptor.anisotropy > 0.f;
		samplerCreateInfo.compareEnable = m_Descriptor.compareOperation != GfxCompareOperation::OFF;

		VkResult result = VK_SUCCESS;
		if ((result = vkCreateSampler(gfxDevice.GetVkDevice(), &samplerCreateInfo, nullptr, &m_Sampler)) != VK_SUCCESS)
			LOGERROR("Failed to create sampler: " << GfxDeviceVK::GetErrorString(result));
	}
}

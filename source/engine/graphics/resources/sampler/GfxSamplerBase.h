#pragma once

namespace Pacha
{
	enum class GfxFilterMode
	{
		NEAREST,
		LINEAR
	};

	enum class GfxWrapMode
	{
		REPEAT,
		MIRRORED_REPEAT,
		CLAMP_TO_EDGE,
		CLAMP_TO_BORDER,
		MIRROR_CLAMP_TO_EDGE
	};

	enum class GfxBorderColor
	{
		FLOAT_TRANSPARENT_BLACK,
		FLOAT_OPAQUE_BLACK,
		FLOAT_OPAQUE_WHITE,

		INT_TRANSPARENT_BLACK,
		INT_OPAQUE_BLACK,
		INT_OPAQUE_WHITE,
	};

	enum class GfxCompareOperation
	{
		OFF,
		NEVER,
		LESS,
		EQUAL,
		LESS_OR_EQUAL,
		GREATER,
		NOT_EQUAL,
		GREATER_OR_EQUAL,
		ALWAYS
	};

	struct GfxSamplerDescriptor
	{
		GfxFilterMode filterMode = GfxFilterMode::LINEAR;
		GfxFilterMode mipsFilterMode = GfxFilterMode::LINEAR;

		GfxWrapMode wrapModeU = GfxWrapMode::REPEAT;
		GfxWrapMode wrapModeV = GfxWrapMode::REPEAT;
		GfxWrapMode wrapModeW = GfxWrapMode::REPEAT;

		GfxCompareOperation compareOperation = GfxCompareOperation::OFF;
		GfxBorderColor borderColor = GfxBorderColor::FLOAT_OPAQUE_WHITE;

		float anisotropy = 0.f;
		float maxLod = 1000.0f;
		float minLod = 1.f;
		float lodBias = 0.f;

		void SetWrapMode(const GfxWrapMode& wrapMode)
		{
			wrapModeU = wrapMode;
			wrapModeV = wrapMode;
			wrapModeW = wrapMode; 
		}
	};

	class GfxSamplerBase
	{
	public:
		GfxSamplerBase() = default;
		virtual ~GfxSamplerBase();

		void Build(const GfxSamplerDescriptor& descriptor);
		const GfxSamplerDescriptor& GetDescriptor();

	protected:
		GfxSamplerDescriptor m_Descriptor;

	private:
		virtual void BuildSampler() = 0;
	};
}
#pragma once
#include "GfxSamplerBase.h"

#include <volk.h>

namespace Pacha
{
	class GfxSamplerVK : public GfxSamplerBase
	{
	public:
		~GfxSamplerVK();
		const VkSampler& GetSampler();
		
		static VkSamplerAddressMode GetVkWrapMode(const GfxWrapMode& wrapMode);
		static VkFilter GetVkFilterMode(const GfxFilterMode& filterMode);
		static VkSamplerMipmapMode GetVkMipMapFilterMode(const GfxFilterMode& filterMode);
		static VkCompareOp GetVkCompareOperation(const GfxCompareOperation& compareOperation);
		static VkBorderColor GetVkBorderColor(const GfxBorderColor& borderColor);

	private:
		VkSampler m_Sampler = VK_NULL_HANDLE;
		void BuildSampler() override;
	};
}
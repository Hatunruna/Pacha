#pragma once

#if RENDERER_VK
#include "graphics/resources/sampler/GfxSamplerVK.h"
#endif

namespace Pacha
{
#if RENDERER_VK
	using GfxSampler = GfxSamplerVK;
#endif
}
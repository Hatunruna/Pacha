#pragma once
#if RENDERER_VK
#include "GfxTechniqueVK.h"
#endif

namespace Pacha
{
#if RENDERER_VK
	using GfxTechnique = GfxTechniqueVK;
#endif
}
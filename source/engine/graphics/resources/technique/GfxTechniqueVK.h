#pragma once
#include "GfxTechniqueBase.h"

#include <volk.h>

namespace Pacha
{
	class GfxTechniqueVK : public GfxTechniqueBase
	{
	public:
		GfxTechniqueVK(const std::unordered_map<PachaId, GfxTechniquePass>& passes, const GfxShaderReflectionInfo& reflectionInfo);
		~GfxTechniqueVK();

	private:
		VkDescriptorSetLayout m_DescriptorSetLayout = VK_NULL_HANDLE;

		void CreateDescriptorSetLayout();
	};
}
#include "GfxTechniqueBase.h"
#include "core/Assert.h"

namespace Pacha
{
	GfxTechniqueBase::GfxTechniqueBase(const std::unordered_map<PachaId, GfxTechniquePass>& passes, const GfxShaderReflectionInfo& reflectionInfo)
		: m_ReflectionInfo(reflectionInfo), m_Passes(passes)
	{ }

	bool GfxTechniqueBase::HasPass(const PachaId& passId) const
	{
		return m_Passes.count(passId);
	}

	const std::unordered_map<PachaId, GfxTechniquePass>& GfxTechniqueBase::GetPasses() const
	{
		return m_Passes;
	}

	const std::unordered_map<PachaId, GfxBufferInfo>& GfxTechniqueBase::GetBuffers() const
	{
		return m_ReflectionInfo.buffers;
	}

	const std::unordered_map<PachaId, GfxConstantBufferInfo>& GfxTechniqueBase::GetConstantBuffers() const
	{
		return m_ReflectionInfo.constantBuffers;
	}

	const std::unordered_map<PachaId, GfxTextureInfo>& GfxTechniqueBase::GetTextures() const
	{
		return m_ReflectionInfo.textures;
	}

	const std::unordered_map<PachaId, GfxSamplerInfo>& GfxTechniqueBase::GetSamplers() const
	{
		return m_ReflectionInfo.samplers;
	}

	bool GfxTechniqueBase::HasBuffer(const PachaId& id)
	{
		return m_ReflectionInfo.buffers.count(id);
	}

	bool GfxTechniqueBase::HasConstantBuffer(const PachaId& id)
	{
		return m_ReflectionInfo.constantBuffers.count(id);
	}

	bool GfxTechniqueBase::ConstantBufferHasMember(const PachaId& bufferId, const PachaId& memberId)
	{
		PACHA_ASSERT(HasConstantBuffer(bufferId), "Buffer does not exist in current technique");
		return m_ReflectionInfo.constantBuffers[bufferId].members.count(memberId);
	}

	bool GfxTechniqueBase::HasTexture(const PachaId& id)
	{
		return m_ReflectionInfo.textures.count(id);
	}

	bool GfxTechniqueBase::HasSampler(const PachaId& id)
	{
		return m_ReflectionInfo.samplers.count(id);
	}

	const GfxBufferInfo& GfxTechniqueBase::GetBufferInfo(const PachaId& bufferId)
	{
		PACHA_ASSERT(HasBuffer(bufferId), "Buffer does not exist in technique");
		return m_ReflectionInfo.buffers[bufferId];
	}

	const GfxConstantBufferInfo& GfxTechniqueBase::GetConstantBufferInfo(const PachaId& bufferId)
	{
		PACHA_ASSERT(HasConstantBuffer(bufferId), "Buffer does not exist in technique");
		return m_ReflectionInfo.constantBuffers[bufferId];
	}

	const GfxBufferMemberInfo& GfxTechniqueBase::GetConstantBufferMemberInfo(const PachaId& bufferId, const PachaId& memberId)
	{
		PACHA_ASSERT(ConstantBufferHasMember(bufferId, memberId), "Buffer does not contain member");
		return m_ReflectionInfo.constantBuffers[bufferId].members[memberId];
	}

	const GfxTextureInfo& GfxTechniqueBase::GetTextureInfo(const PachaId& textureId)
	{
		PACHA_ASSERT(HasTexture(textureId), "Texture does not exist in technique");
		return m_ReflectionInfo.textures[textureId];
	}

	const GfxSamplerInfo& GfxTechniqueBase::GetSamplerInfo(const PachaId& samplerId)
	{
		PACHA_ASSERT(HasSampler(samplerId), "Sampler does not exist in technique");
		return m_ReflectionInfo.samplers[samplerId];
	}

	const GfxRendererState& GfxTechniqueBase::GetRendererState(const PachaId& pass)
	{
		PACHA_ASSERT(m_Passes.count(pass), "Pass not found");
		return m_Passes[pass].rendererState;
	}
}

#pragma once
#include "GfxRendererState.h"
#include "utilities/PachaId.h"
#include "graphics/resources/shader/GfxShader.h"

#include <filesystem>
#include <unordered_map>

namespace Pacha
{
	enum GfxBufferMemberTraitsBits
	{
		GfxBufferMemberTraits_INT			= 1 << 0,
		GfxBufferMemberTraits_UINT			= 1 << 1,
		GfxBufferMemberTraits_BOOL			= 1 << 2,
		GfxBufferMemberTraits_FLOAT			= 1 << 3,
		GfxBufferMemberTraits_DOUBLE		= 1 << 4,
		GfxBufferMemberTraits_STRUCT		= 1 << 5,

		GfxBufferMemberTraits_VECTOR		= 1 << 6,

		GfxBufferMemberTraits_COMPONENTS_1	= 1 << 7,
		GfxBufferMemberTraits_COMPONENTS_2	= 1 << 8,
		GfxBufferMemberTraits_COMPONENTS_3	= 1 << 9,
		GfxBufferMemberTraits_COMPONENTS_4	= 1 << 10
	};
	typedef uint32_t GfxBufferMemberTraits;

	enum class GfxTextureDescriptorType
	{
		SAMPLED_IMAGE,
		STORAGE_IMAGE
	};

	enum GfxBufferDescriptorType
	{
		GfxBufferDescriptorType_APPEND_CONSUME		= 1 << 0,
		GfxBufferDescriptorType_BYTE_ADDRESS		= 1 << 1,
		GfxBufferDescriptorType_RW_BYTE_ADDRESS		= 1 << 2,
		GfxBufferDescriptorType_FORMATTED			= 1 << 3,
		GfxBufferDescriptorType_RW_FORMATTED		= 1 << 4,
		GfxBufferDescriptorType_STRUCTURED			= 1 << 5,
		GfxBufferDescriptorType_RW_STRUCTURED		= 1 << 6,
		GfxBufferDescriptorType_CONSTANT_STATIC		= 1 << 7,
		GfxBufferDescriptorType_CONSTANT_DYNAMIC	= 1 << 8,
		GfxBufferDescriptorType_CONSTANT			= GfxBufferDescriptorType_CONSTANT_STATIC | GfxBufferDescriptorType_CONSTANT_DYNAMIC
	};

	struct GfxBufferMemberInfo
	{
		uint32_t size = 0;
		uint32_t offset = 0;
		GfxBufferMemberTraits traits = 0;
		std::unordered_map<PachaId, std::shared_ptr<GfxBufferMemberInfo>> structMembers;
	};

	struct GfxConstantBufferInfo
	{
		uint32_t size;
		uint32_t binding;
		GfxBufferDescriptorType type;
		std::unordered_map<PachaId, GfxBufferMemberInfo> members;
	};

	struct GfxBufferInfo
	{
		uint32_t binding;
		GfxBufferDescriptorType type;
	};

	struct GfxTextureInfo
	{
		uint32_t binding;
		GfxTextureDescriptorType type;
	};

	struct GfxSamplerInfo
	{
		uint32_t binding;
	};

	struct GfxShaderReflectionInfo
	{
		std::unordered_map<PachaId, GfxBufferInfo> buffers;
		std::unordered_map<PachaId, GfxSamplerInfo> samplers;
		std::unordered_map<PachaId, GfxTextureInfo> textures;
		std::unordered_map<PachaId, GfxConstantBufferInfo> constantBuffers;
	};

	struct GfxTechniquePass
	{
		PachaId passId = {};
		std::vector<PachaId> defines = {};
		GfxRendererState rendererState = {};
		std::vector<std::vector<size_t>> uniqueGroups = {};
		std::unordered_map<GfxShaderType, ShaderBitSet> stageDefineMask = {};
		std::unordered_map<GfxShaderType, std::unordered_map<ShaderBitSet, uint64_t>> shaderHashes = {};
	};

	class GfxTechniqueBase
	{
	public:
		GfxTechniqueBase(const std::unordered_map<PachaId, GfxTechniquePass>& passes, const GfxShaderReflectionInfo& reflectionInfo);
		
		bool HasPass(const PachaId& passId) const;
		const std::unordered_map<PachaId, GfxTechniquePass>& GetPasses() const;

		const std::unordered_map<PachaId, GfxBufferInfo>& GetBuffers() const;
		const std::unordered_map<PachaId, GfxConstantBufferInfo>& GetConstantBuffers() const;

		const std::unordered_map<PachaId, GfxTextureInfo>& GetTextures() const;
		const std::unordered_map<PachaId, GfxSamplerInfo>& GetSamplers() const;

		bool HasBuffer(const PachaId& id);
		bool HasConstantBuffer(const PachaId& id);
		bool ConstantBufferHasMember(const PachaId& bufferId, const PachaId& memberId);

		bool HasTexture(const PachaId& id);
		bool HasSampler(const PachaId& id);

		const GfxBufferInfo& GetBufferInfo(const PachaId& bufferId);
		const GfxConstantBufferInfo& GetConstantBufferInfo(const PachaId& bufferId);
		const GfxBufferMemberInfo& GetConstantBufferMemberInfo(const PachaId& bufferId, const PachaId& memberId);
		
		const GfxTextureInfo& GetTextureInfo(const PachaId& textureId);
		const GfxSamplerInfo& GetSamplerInfo(const PachaId& samplerId);
		
		const GfxRendererState& GetRendererState(const PachaId& pass);

	protected:
		GfxShaderReflectionInfo m_ReflectionInfo;
		std::unordered_map<PachaId, GfxTechniquePass> m_Passes;
	};
}
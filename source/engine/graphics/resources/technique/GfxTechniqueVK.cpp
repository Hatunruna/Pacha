#include "GfxTechniqueVK.h"
#include "graphics/device/GfxDevice.h"

namespace Pacha
{
	GfxTechniqueVK::GfxTechniqueVK(const std::unordered_map<PachaId, GfxTechniquePass>& passes, const GfxShaderReflectionInfo& reflectionInfo)
		: GfxTechniqueBase(passes, reflectionInfo)
	{
		CreateDescriptorSetLayout();
		//LoadShaderBlobs()
	}

	GfxTechniqueVK::~GfxTechniqueVK()
	{
		GfxDevice& device = GfxDevice::GetInstance();
		vkDestroyDescriptorSetLayout(device.GetVkDevice(), m_DescriptorSetLayout, nullptr);
	}

	void GfxTechniqueVK::CreateDescriptorSetLayout()
	{
		std::vector<VkDescriptorSetLayoutBinding> layoutBindings = {};

		for (auto const& [id, textureInfo] : m_ReflectionInfo.textures)
		{
			VkDescriptorSetLayoutBinding layout;
			layout.binding = textureInfo.binding;
			layout.stageFlags = VK_SHADER_STAGE_ALL;
			layout.descriptorCount = 1;
			layout.pImmutableSamplers = nullptr;
			
			switch (textureInfo.type)
			{
				case GfxTextureDescriptorType::SAMPLED_IMAGE:
					layout.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
					break;
				case GfxTextureDescriptorType::STORAGE_IMAGE:
					layout.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
					break;
			}

			layoutBindings.push_back(layout);
		}

		for (auto const& [id, samplerInfo] : m_ReflectionInfo.samplers)
		{
			VkDescriptorSetLayoutBinding layout;
			layout.binding = samplerInfo.binding;
			layout.stageFlags = VK_SHADER_STAGE_ALL;
			layout.descriptorCount = 1;
			layout.pImmutableSamplers = nullptr;
			layout.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLER;

			layoutBindings.push_back(layout);
		}

		for (auto const& [id, bufferInfo] : m_ReflectionInfo.constantBuffers)
		{
			VkDescriptorSetLayoutBinding layout;
			layout.binding = bufferInfo.binding;
			layout.stageFlags = VK_SHADER_STAGE_ALL;
			layout.descriptorCount = 1;
			layout.pImmutableSamplers = nullptr;
			layout.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;

			layoutBindings.push_back(layout);
		}

		for (auto const& [id, bufferInfo] : m_ReflectionInfo.buffers)
		{
			VkDescriptorSetLayoutBinding layout;
			layout.binding = bufferInfo.binding;
			layout.stageFlags = VK_SHADER_STAGE_ALL;
			layout.descriptorCount = 1;
			layout.pImmutableSamplers = nullptr;

			switch (bufferInfo.type)
			{
				default: 
					layout.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
					break;
			
				case GfxBufferDescriptorType_FORMATTED:
					layout.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER;
					break;

				case GfxBufferDescriptorType_RW_FORMATTED:
					layout.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER;
					break;
			}
			
			layoutBindings.push_back(layout);
		}

		VkDescriptorSetLayoutCreateInfo descriptorLayoutCreateInfo = {};
		descriptorLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		descriptorLayoutCreateInfo.bindingCount = static_cast<uint32_t>(layoutBindings.size());
		descriptorLayoutCreateInfo.pBindings = layoutBindings.data();

		GfxDevice& device = GfxDevice::GetInstance();
		if (vkCreateDescriptorSetLayout(device.GetVkDevice(), &descriptorLayoutCreateInfo, nullptr, &m_DescriptorSetLayout) != VK_SUCCESS)
			LOGCRITICAL("Failed to create descriptor set layout");
	}
}

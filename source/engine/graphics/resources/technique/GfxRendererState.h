#pragma once
#include "utilities/Hash.h"
#include "graphics/ui/utilities/EditorEnums.h"

#include <vector>

namespace Pacha
{
	EDITOR_ENUM(GfxFillMode,
		SOLID,
		WIREFRAME,
	);

	EDITOR_ENUM(GfxWindingOrder,
		CLOCK_WISE,
		COUNTER_CLOCK_WISE
	);

	EDITOR_ENUM(GfxFaceCulling,
		OFF,
		FRONT,
		BACK,
		FRONT_BACK
	);

	EDITOR_ENUM(GfxDepthTest,
		OFF,
		NEVER,
		LESS,
		GREATER,
		EQUAL,
		ALWAYS,
		LEQUAL,
		GEQUAL,
		NOT_EQUAL
	);

	EDITOR_ENUM(GfxBlendingFactor,
		ONE,
		ZERO,
		SRC_COLOR,
		ONE_MINUS_SRC_COLOR,
		DST_COLOR,
		ONE_MINUS_DST_COLOR,
		SRC_ALPHA,
		ONE_MINUS_SRC_ALPHA,
		DST_ALPHA,
		ONE_MINUS_DST_ALPHA
	);

	EDITOR_ENUM(GfxBlendingOperation,
		ADD,
		SUBSTRACT,
		REVERSE_SUBSTRACT,
		MIN,
		MAX
	);

	EDITOR_ENUM(GfxLogicalBlending,
		OFF,
		CLEAR,
		AND,
		XOR,
		OR,
		NOR,
		NAND,
		SET
	);

	EDITOR_FLAGS(GfxColorMask,
		GfxColorMask_R = 1,
		GfxColorMask_G = 2,
		GfxColorMask_B = 4,
		GfxColorMask_A = 8
	);

	struct GfxRendererState
	{
		struct GfxRenderTargetState
		{
			struct GfxBlendFactors
			{
				GfxBlendingFactor sourceColor = GfxBlendingFactor::SRC_ALPHA;
				GfxBlendingFactor destinationColor = GfxBlendingFactor::ONE_MINUS_SRC_ALPHA;

				GfxBlendingFactor sourceAlpha = GfxBlendingFactor::SRC_ALPHA;
				GfxBlendingFactor destinationAlpha = GfxBlendingFactor::ONE_MINUS_SRC_ALPHA;

				bool operator==(const GfxBlendFactors& other) const
				{
					return memcmp(this, &other, sizeof(GfxBlendFactors)) == 0;
				}

				size_t GetHash() const { return m_Hash; }
				void CalculateHash()
				{
					m_Hash = Hash(sourceColor);
					HashCombine(m_Hash, destinationColor);
					HashCombine(m_Hash, sourceAlpha);
					HashCombine(m_Hash, destinationAlpha);
				};

			private:
				size_t m_Hash = 0;
			};

			struct GfxBlendOperations
			{
				GfxBlendingOperation color = GfxBlendingOperation::ADD;
				GfxBlendingOperation alpha = GfxBlendingOperation::ADD;

				bool operator==(const GfxBlendOperations& other) const
				{
					return memcmp(this, &other, sizeof(GfxBlendOperations)) == 0;
				}

				size_t GetHash() const { return m_Hash; }
				void CalculateHash()
				{
					m_Hash = Hash(color);
					HashCombine(m_Hash, alpha);
				};

			private:
				size_t m_Hash = 0;
			};

			bool blendEnabled = true;
			GfxBlendFactors blendFactors = {};
			GfxBlendOperations blendOperations = {};
			uint32_t colorMask = GfxColorMask_R | GfxColorMask_G | GfxColorMask_B | GfxColorMask_A;

			bool operator==(const GfxRenderTargetState& other) const
			{
				return memcmp(this, &other, sizeof(GfxRenderTargetState)) == 0;
			}

			size_t GetHash() const { return m_Hash; }
			void CalculateHash()
			{
				m_Hash = Hash(blendEnabled);
				HashCombine(m_Hash, colorMask);

				blendFactors.CalculateHash();
				HashCombine(m_Hash, blendFactors);

				blendOperations.CalculateHash();
				HashCombine(m_Hash, blendOperations);
			};

		private:
			size_t m_Hash = 0;
		};

		uint32_t patchControlPoints = 0;

		float lineWidth = 1.f;
		float depthBias = 0.f;
		float depthBiasSlope = 0.f;

		bool depthWrite = true;
		bool depthClamp = false;
		bool alphaToCoverage = false;

		GfxFillMode fillMode = GfxFillMode::SOLID;
		GfxDepthTest depthTest = GfxDepthTest::LEQUAL;
		GfxFaceCulling faceCulling = GfxFaceCulling::BACK;
		GfxWindingOrder windingOrder = GfxWindingOrder::COUNTER_CLOCK_WISE;
		GfxLogicalBlending logicalBlending = GfxLogicalBlending::OFF;
		std::vector<GfxRenderTargetState> renderTargetStates = std::vector<GfxRenderTargetState>(1);

		bool operator==(const GfxRendererState& other) const
		{
			return memcmp(this, &other, sizeof(GfxRendererState) - sizeof(std::vector<GfxRenderTargetState>)) == 0
				&& renderTargetStates == other.renderTargetStates;
		}

		size_t GetHash() const { return m_Hash; }
		void CalculateHash() 
		{
			m_Hash = Hash(patchControlPoints);

			HashCombine(m_Hash, lineWidth);
			HashCombine(m_Hash, depthBias);
			HashCombine(m_Hash, depthBiasSlope);
			HashCombine(m_Hash, depthWrite);
			HashCombine(m_Hash, depthClamp);
			HashCombine(m_Hash, alphaToCoverage);
			HashCombine(m_Hash, depthTest);
			HashCombine(m_Hash, faceCulling);
			HashCombine(m_Hash, windingOrder);
			HashCombine(m_Hash, logicalBlending);

			for (auto& renderTargetState : renderTargetStates)
			{
				renderTargetState.CalculateHash();
				HashCombine(m_Hash, renderTargetState);
			}
		};

	private:
		size_t m_Hash = 0;
	};
}

namespace std
{
	template <>
	struct hash<Pacha::GfxRendererState::GfxRenderTargetState::GfxBlendFactors>
	{
		size_t operator()(const Pacha::GfxRendererState::GfxRenderTargetState::GfxBlendFactors& blendFactors) const
		{
			return blendFactors.GetHash();
		}
	};

	template <>
	struct hash<Pacha::GfxRendererState::GfxRenderTargetState::GfxBlendOperations>
	{
		size_t operator()(const Pacha::GfxRendererState::GfxRenderTargetState::GfxBlendOperations& blendOperations) const
		{
			return blendOperations.GetHash();
		}
	};

	template <>
	struct hash<Pacha::GfxRendererState::GfxRenderTargetState>
	{
		size_t operator()(const Pacha::GfxRendererState::GfxRenderTargetState& renderTargetState) const
		{
			return renderTargetState.GetHash();
		}
	};

	template <>
	struct hash<Pacha::GfxRendererState>
	{
		size_t operator()(const Pacha::GfxRendererState& rendererState) const
		{
			return rendererState.GetHash();
		}
	};
}
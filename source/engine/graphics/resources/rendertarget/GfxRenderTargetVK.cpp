#include "GfxRenderTargetVK.h"

#include "graphics/device/GfxDevice.h"
#include "graphics/utilities/GfxMemoryManager.h"
#include "graphics/resources/commandbuffer/GfxCommandBuffer.h"

namespace Pacha
{
	GfxRenderTargetVK::GfxRenderTargetVK(const GfxRenderPassDependency& dependency, const GfxMultiViewInfo& multiViewInfo)
		: GfxRenderTargetBase(dependency, multiViewInfo)
	{ }

	GfxRenderTargetVK::~GfxRenderTargetVK()
	{
		GfxMemoryManager::GetInstance().AddRenderTargetToDelete(*this);
	}

	VkRenderPass GfxRenderTargetVK::GetRenderPass()
	{
		if (HasDataChanged())
		{
			GfxMemoryManager::GetInstance().AddRenderTargetToDelete(*this);
			m_RenderPass = VK_NULL_HANDLE;
			m_Framebuffers.clear();

			CreateRenderPass();
		}

		PACHA_ASSERT(m_RenderPass != VK_NULL_HANDLE, "Renderpass not found");
		return m_RenderPass;
	}

	VkFramebuffer GfxRenderTargetVK::GetFrameBuffer(const GfxSubResourceTarget& targets)
	{
		const auto& fbFound = m_Framebuffers.find(targets);
		if (fbFound != m_Framebuffers.end())
			return fbFound->second;
		else
		{
			GfxDevice& gfxDevice = GfxDevice::GetInstance();
			const VkDevice& device = gfxDevice.GetVkDevice();
			VkFramebuffer& framebuffer = m_Framebuffers[targets];

			std::vector<VkImageView> attachments;
			for (size_t i = 0; i < m_ColorAttachments.size(); ++i)
			{
				auto& attachment = m_ColorAttachments[i];
				if (attachment.texture)
				{
					GfxTextureVK*& texture = attachment.texture;
					uint32_t viewIndex = GfxSubResourceTarget::CalculateViewIndex(targets.colorAttachments[i], texture);

					bool autoResolveMSAA = texture->GetMSAA() > GfxMSAA::X1 && texture->GetDescriptor().flags & GfxTextureFlags_AUTO_RESOLVE_MSAA;
					if (autoResolveMSAA)
					{
						attachments.push_back(texture->GetMultisampledImageView(viewIndex));
						attachments.push_back(texture->GetImageView(viewIndex));
					}
					else
						attachments.push_back(texture->GetImageView(viewIndex));
				}
			}

			if (m_DepthStencilAttachment.texture)
			{
				uint32_t viewIndex = GfxSubResourceTarget::CalculateViewIndex(targets.depthStencilAttachment, m_DepthStencilAttachment.texture);
				attachments.push_back(m_DepthStencilAttachment.texture->GetImageView(viewIndex));
			}

			VkFramebufferCreateInfo framebufferCreateInfo = {};
			framebufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			framebufferCreateInfo.renderPass = m_RenderPass;
			framebufferCreateInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
			framebufferCreateInfo.pAttachments = attachments.data();
			framebufferCreateInfo.width = GetWidth();
			framebufferCreateInfo.height = GetHeight();
			framebufferCreateInfo.layers = 1;

			VkResult result = VK_SUCCESS;
			if ((result = vkCreateFramebuffer(device, &framebufferCreateInfo, nullptr, &framebuffer)) != VK_SUCCESS)
				LOGCRITICAL("Failed to create framebuffer: " << GfxDeviceVK::GetErrorString(result));
			return framebuffer;
		}
	}

	void GfxRenderTargetVK::CreateRenderPass()
	{
		GfxDevice& gfxDevice = GfxDevice::GetInstance();
		const VkDevice& device = gfxDevice.GetVkDevice();

		std::vector<VkAttachmentDescription> attachmentDescriptions;
		std::vector<VkAttachmentReference> colorAttachmentsReferences;
		std::vector<VkAttachmentReference> depthAttachmentsReferences;
		std::vector<VkAttachmentReference> resolvedAttachmentsReferences;

		//Color attachments
		for (auto& colorAttachment : m_ColorAttachments)
		{
			if (colorAttachment.texture)
			{
				GfxTextureVK*& texture = colorAttachment.texture;

				VkFormat format = GfxTextureVK::GetVkFormat(texture->GetFormat());
				VkSampleCountFlagBits msaa = GfxTextureVK::GetVkMSAA(texture->GetMSAA());

				VkAttachmentLoadOp loadOp = GetVkLoadOperation(colorAttachment.loadOperation);
				VkAttachmentStoreOp storeOp = GetVkStoreOperation(colorAttachment.storeOperation);
				VkImageLayout initialLayout = GfxCommandBufferVK::GetVkImageLayout(colorAttachment.initialState);
				VkImageLayout finalLayout = GfxCommandBufferVK::GetVkImageLayout(colorAttachment.finalState);

				bool autoResolveMSAA = (msaa > VK_SAMPLE_COUNT_1_BIT) && texture->GetDescriptor().flags & GfxTextureFlags_AUTO_RESOLVE_MSAA;

				VkAttachmentDescription attachmentDescription = {};
				attachmentDescription.format = format;
				attachmentDescription.samples = msaa;
				attachmentDescription.loadOp = loadOp;
				attachmentDescription.storeOp = autoResolveMSAA ? VK_ATTACHMENT_STORE_OP_DONT_CARE : storeOp;
				attachmentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
				attachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
				attachmentDescription.initialLayout = initialLayout;
				attachmentDescription.finalLayout = autoResolveMSAA ? VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL : finalLayout;
				attachmentDescriptions.push_back(attachmentDescription);

				VkAttachmentReference colorAttachmentReference = {};
				colorAttachmentReference.attachment = static_cast<uint32_t>(attachmentDescriptions.size() - 1);
				colorAttachmentReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
				colorAttachmentsReferences.push_back(colorAttachmentReference);

				if (autoResolveMSAA)
				{
					VkAttachmentDescription resolveAttachmentDesc = {};
					resolveAttachmentDesc.format = format;
					resolveAttachmentDesc.samples = VK_SAMPLE_COUNT_1_BIT;
					resolveAttachmentDesc.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
					resolveAttachmentDesc.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
					resolveAttachmentDesc.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
					resolveAttachmentDesc.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
					resolveAttachmentDesc.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
					resolveAttachmentDesc.finalLayout = finalLayout;
					attachmentDescriptions.push_back(resolveAttachmentDesc);

					VkAttachmentReference resolveAttachmentReference = {};
					resolveAttachmentReference.attachment = static_cast<uint32_t>(attachmentDescriptions.size() - 1);
					resolveAttachmentReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
					resolvedAttachmentsReferences.push_back(resolveAttachmentReference);
				}
			}
		}

		//Depth Stencil
		if (m_DepthStencilAttachment.texture)
		{
			GfxTextureVK*& texture = m_DepthStencilAttachment.texture;
			VkFormat format = GfxTextureVK::GetVkFormat(texture->GetFormat());
			VkSampleCountFlagBits msaa = GfxTextureVK::GetVkMSAA(texture->GetMSAA());

			VkAttachmentLoadOp loadOp = GetVkLoadOperation(m_DepthStencilAttachment.loadOperation);
			VkAttachmentStoreOp storeOp = GetVkStoreOperation(m_DepthStencilAttachment.storeOperation);

			VkAttachmentDescription depthAttachmentDesc = {};
			depthAttachmentDesc.format = format;
			depthAttachmentDesc.samples = msaa;
			depthAttachmentDesc.loadOp = loadOp;
			depthAttachmentDesc.storeOp = storeOp;
			depthAttachmentDesc.stencilLoadOp = loadOp;
			depthAttachmentDesc.stencilStoreOp = storeOp;
			depthAttachmentDesc.initialLayout = GfxCommandBufferVK::GetVkImageLayout(m_DepthStencilAttachment.initialState);
			depthAttachmentDesc.finalLayout = GfxCommandBufferVK::GetVkImageLayout(m_DepthStencilAttachment.finalState);
			attachmentDescriptions.push_back(depthAttachmentDesc);

			VkAttachmentReference depthAttachmentReference = {};
			depthAttachmentReference.attachment = static_cast<uint32_t>(attachmentDescriptions.size() - 1);
			depthAttachmentReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
			depthAttachmentsReferences.push_back(depthAttachmentReference);
		}

		VkSubpassDescription subpassDescription = {};
		subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;

		if (colorAttachmentsReferences.size())
		{
			subpassDescription.colorAttachmentCount = static_cast<uint32_t>(colorAttachmentsReferences.size());
			subpassDescription.pColorAttachments = colorAttachmentsReferences.data();
		}

		if (depthAttachmentsReferences.size())
			subpassDescription.pDepthStencilAttachment = depthAttachmentsReferences.data();

		if (resolvedAttachmentsReferences.size())
			subpassDescription.pResolveAttachments = resolvedAttachmentsReferences.data();

		subpassDescription.inputAttachmentCount = 0;
		subpassDescription.pInputAttachments = nullptr;
		subpassDescription.preserveAttachmentCount = 0;
		subpassDescription.pPreserveAttachments = nullptr;

		VkRenderPassCreateInfo renderPassCreateInfo = {};
		renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		renderPassCreateInfo.attachmentCount = static_cast<uint32_t>(attachmentDescriptions.size());
		renderPassCreateInfo.pAttachments = attachmentDescriptions.data();
		renderPassCreateInfo.subpassCount = 1;
		renderPassCreateInfo.pSubpasses = &subpassDescription;

		VkSubpassDependency renderPassDependency = {};
		renderPassDependency.srcSubpass = m_Dependency.type == GfxRenderPassDependencyType::SOURCE_EXTERNAL ? VK_SUBPASS_EXTERNAL : 0;
		renderPassDependency.dstSubpass = m_Dependency.type == GfxRenderPassDependencyType::DESTINATION_EXTERNAL ? VK_SUBPASS_EXTERNAL : 0;
		renderPassDependency.dependencyFlags = 0;
		renderPassDependency.srcStageMask = GetVkPipelineStage(m_Dependency.sourceStage);
		renderPassDependency.dstStageMask = GetVkPipelineStage(m_Dependency.destinationStage);
		renderPassDependency.srcAccessMask = GetVkAccessMask(m_Dependency.sourceStage);
		renderPassDependency.dstAccessMask = GetVkAccessMask(m_Dependency.destinationStage);

		if (m_Dependency.type != GfxRenderPassDependencyType::DISABLED)
		{
			renderPassCreateInfo.dependencyCount = 1;
			renderPassCreateInfo.pDependencies = &renderPassDependency;
		}
		else
		{
			renderPassCreateInfo.dependencyCount = 0;
			renderPassCreateInfo.pDependencies = nullptr;
		}

		VkRenderPassMultiviewCreateInfo multiviewCreateInfo = {};
		multiviewCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_MULTIVIEW_CREATE_INFO;
		multiviewCreateInfo.subpassCount = 1;
		multiviewCreateInfo.correlationMaskCount = 1;
		multiviewCreateInfo.pViewMasks = &m_MultiViewInfo.viewsMask;
		multiviewCreateInfo.pCorrelationMasks = &m_MultiViewInfo.viewsMask;

		if (m_MultiViewInfo.viewsCount > 1 && gfxDevice.SupportsMultiViewRendering())
			renderPassCreateInfo.pNext = &multiviewCreateInfo;

		VkResult result = VK_SUCCESS;
		if ((result = vkCreateRenderPass(device, &renderPassCreateInfo, nullptr, &m_RenderPass)) != VK_SUCCESS)
		{
			LOGERROR("Error while creating render pass: " << GfxDeviceVK::GetErrorString(result));
			return;
		}
	}

	void GfxRenderTargetVK::CopyForDeletion(GfxRenderTargetDeleteContiner* container)
	{
		container->renderpass = m_RenderPass;
		container->framebuffers.reserve(m_Framebuffers.size());
		for (auto& [target, frameBuffer] : m_Framebuffers)
			container->framebuffers.push_back(frameBuffer);
	}

	VkAttachmentLoadOp GfxRenderTargetVK::GetVkLoadOperation(const GfxLoadOperation& loadOp)
	{
		switch (loadOp)
		{
			case GfxLoadOperation::LOAD:		return VK_ATTACHMENT_LOAD_OP_LOAD;
			case GfxLoadOperation::CLEAR:		return VK_ATTACHMENT_LOAD_OP_CLEAR;
			case GfxLoadOperation::DONT_CARE:	return VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			default:
				LOGCRITICAL("Load Operation not registered on this function");
				return VK_ATTACHMENT_LOAD_OP_MAX_ENUM;
		}
	}

	VkAttachmentStoreOp GfxRenderTargetVK::GetVkStoreOperation(const GfxStoreOperation& storeOp)
	{
		switch (storeOp)
		{
			case GfxStoreOperation::STORE:		return VK_ATTACHMENT_STORE_OP_STORE;
			case GfxStoreOperation::DONT_CARE:	return VK_ATTACHMENT_STORE_OP_DONT_CARE;
			default:
				LOGCRITICAL("Store Operation not registered on this function");
				return VK_ATTACHMENT_STORE_OP_MAX_ENUM;
		}
	}

	VkPipelineStageFlags GfxRenderTargetVK::GetVkPipelineStage(const GfxPipelineStageFlags& stage)
	{
		VkPipelineStageFlags stageFlags = 0;

		if (stage == GfxPipelineStageFlags_ALL_COMMANDS)
			return VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;

		if ((stage & GfxPipelineStageFlags_ALL_GRAPHICS) == GfxPipelineStageFlags_ALL_GRAPHICS)
			stageFlags |= VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT;
		else
		{
			if (stage & GfxPipelineStageFlags_RENDER_TARGET)
				stageFlags |= VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

			if (stage & GfxPipelineStageFlags_DEPTH_READ_WRITE)
				stageFlags |= VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;

			if (stage & GfxPipelineStageFlags_DRAW_INDIRECT)
				stageFlags |= VK_PIPELINE_STAGE_DRAW_INDIRECT_BIT;

			if (stage & GfxPipelineStageFlags_VERTEX_INPUT)
				stageFlags |= VK_PIPELINE_STAGE_VERTEX_INPUT_BIT;

			if (stage & GfxPipelineStageFlags_VERTEX_SHADER)
				stageFlags |= VK_PIPELINE_STAGE_VERTEX_SHADER_BIT;

			if (stage & GfxPipelineStageFlags_PIXEL_SHADER)
				stageFlags |= VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;

			if (stage & GfxPipelineStageFlags_NON_PIXEL_SHADING)
				stageFlags |= VK_PIPELINE_STAGE_GEOMETRY_SHADER_BIT | VK_PIPELINE_STAGE_TESSELLATION_CONTROL_SHADER_BIT | VK_PIPELINE_STAGE_TESSELLATION_EVALUATION_SHADER_BIT;

		}
		
		if (stage & GfxPipelineStageFlags_COMPUTE_SHADER)
			stageFlags |= VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT;

		if (stage & GfxPipelineStageFlags_TRANSFER)
			stageFlags |= VK_PIPELINE_STAGE_TRANSFER_BIT;

		if (GfxDevice::SupportsVariableRateShading())
		{
			if (stage & GfxPipelineStageFlags_FRAGMENT_SHADING_RATE)
				stageFlags |= VK_PIPELINE_STAGE_SHADING_RATE_IMAGE_BIT_NV;
		}

		if (GfxDevice::SupportsRaytracing())
		{
			if (stage & GfxPipelineStageFlags_RAY_TRACING_SHADER)
				stageFlags |= VK_PIPELINE_STAGE_RAY_TRACING_SHADER_BIT_KHR;
			
			if (stage & GfxPipelineStageFlags_ACCELERATION_STRUCTURE_BUILD)
				stageFlags |= VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_KHR;
		}

		return stageFlags;
	}

	VkAccessFlags GfxRenderTargetVK::GetVkAccessMask(const GfxPipelineStageFlags& stage)
	{
		VkAccessFlags accessFlags = 0;

		if (stage & GfxPipelineStageFlags_RENDER_TARGET)
			accessFlags |= VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

		if (stage & GfxPipelineStageFlags_DEPTH_READ_WRITE)
			accessFlags |= VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

		if (stage & GfxPipelineStageFlags_DRAW_INDIRECT)
			accessFlags |= VK_ACCESS_INDIRECT_COMMAND_READ_BIT;

		if (stage & GfxPipelineStageFlags_VERTEX_INPUT)
			accessFlags |= VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT | VK_ACCESS_INDEX_READ_BIT;

		if (stage & GfxPipelineStageFlags_VERTEX_SHADER		||
			stage & GfxPipelineStageFlags_PIXEL_SHADER		||
			stage & GfxPipelineStageFlags_NON_PIXEL_SHADING ||
			stage & GfxPipelineStageFlags_COMPUTE_SHADER	||
			stage & GfxPipelineStageFlags_RAY_TRACING_SHADER)
			accessFlags |= VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_SHADER_WRITE_BIT | VK_ACCESS_UNIFORM_READ_BIT;

		if (stage & GfxPipelineStageFlags_TRANSFER)
			accessFlags |= VK_ACCESS_TRANSFER_READ_BIT | VK_ACCESS_TRANSFER_WRITE_BIT;

		if (GfxDevice::SupportsVariableRateShading())
		{
			if (stage & GfxPipelineStageFlags_FRAGMENT_SHADING_RATE)
				accessFlags |= VK_ACCESS_FRAGMENT_SHADING_RATE_ATTACHMENT_READ_BIT_KHR;
		}

		if (GfxDevice::SupportsRaytracing())
		{
			if (stage & GfxPipelineStageFlags_RAY_TRACING_SHADER)
				accessFlags |= VK_ACCESS_ACCELERATION_STRUCTURE_READ_BIT_KHR;

			if (stage & GfxPipelineStageFlags_ACCELERATION_STRUCTURE_BUILD)
				accessFlags |= VK_ACCESS_ACCELERATION_STRUCTURE_WRITE_BIT_KHR;
		}

		return accessFlags;
	}
}
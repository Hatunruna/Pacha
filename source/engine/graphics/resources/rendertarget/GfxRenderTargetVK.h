#pragma once
#include "GfxRenderTargetBase.h"

#include <unordered_map>

namespace Pacha
{
	struct GfxRenderTargetDeleteContiner
	{
		VkRenderPass renderpass = VK_NULL_HANDLE;
		std::vector<VkFramebuffer> framebuffers;
	};

	class GfxRenderTargetVK : public GfxRenderTargetBase
	{
		friend class GfxMemoryManager;

	public:
		GfxRenderTargetVK(const GfxRenderPassDependency& dependency = GfxRenderPassDependency::SourceExternal(), const GfxMultiViewInfo& multiViewInfo = {});
		virtual ~GfxRenderTargetVK();

		VkRenderPass GetRenderPass();
		VkFramebuffer GetFrameBuffer(const GfxSubResourceTarget& targets);

		static VkAttachmentLoadOp GetVkLoadOperation(const GfxLoadOperation& loadOp);
		static VkAttachmentStoreOp GetVkStoreOperation(const GfxStoreOperation& storeOp);
		static VkPipelineStageFlags GetVkPipelineStage(const GfxPipelineStageFlags& stage);
		static VkAccessFlags GetVkAccessMask(const GfxPipelineStageFlags& stage);

	private:
		VkRenderPass m_RenderPass = VK_NULL_HANDLE;
		std::unordered_map<GfxSubResourceTarget, VkFramebuffer> m_Framebuffers;

		void CreateRenderPass();
		void CopyForDeletion(GfxRenderTargetDeleteContiner* container) override;
	};
}
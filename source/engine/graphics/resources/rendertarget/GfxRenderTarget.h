#pragma once
#if RENDERER_VK
#include "graphics/resources/rendertarget/GfxRenderTargetVK.h"
#endif

namespace Pacha
{
#if RENDERER_VK
	using GfxRenderTarget = GfxRenderTargetVK;
#endif
}
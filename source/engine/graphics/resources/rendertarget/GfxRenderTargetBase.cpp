#include "GfxRenderTargetBase.h"
#include "core/Assert.h"

namespace Pacha
{
	GfxRenderTargetBase::GfxRenderTargetBase(const GfxRenderPassDependency& dependency, const GfxMultiViewInfo& multiViewInfo)
	{
		m_Dependency = dependency;
		m_MultiViewInfo = multiViewInfo;
	}

	GfxRenderTargetBase::~GfxRenderTargetBase()
	{ }

	bool GfxRenderTargetBase::ColorAttachmentDiffer(const uint32_t& index, GfxTexture* colorTexture)
	{
		PACHA_ASSERT(colorTexture, "Color texture is null");
		PACHA_ASSERT(index < 8, "Attachment index out of range");
		return m_ColorHashes[index] != colorTexture->GetHash();
	}

	bool GfxRenderTargetBase::DepthStencilAttachmentDiffer(GfxTexture* depthStencilTexture)
	{
		PACHA_ASSERT(depthStencilTexture, "Depth Stencil texture is null");
		return m_DepthHash != depthStencilTexture->GetHash();
	}

	bool GfxRenderTargetBase::HasDataChanged()
	{
		bool changed = m_HasDataChanged;
		m_HasDataChanged = false;
		return changed;
	}

	GfxMSAA GfxRenderTargetBase::GetMSAA()
	{
		PACHA_ASSERT(m_ColorAttachments.size() || m_DepthStencilAttachment.texture, "No attahcment has been set");

		if (m_ColorAttachments.size())
			return m_ColorAttachments[0].texture->GetMSAA();
		else
			return m_DepthStencilAttachment.texture->GetMSAA();
	}

	uint32_t GfxRenderTargetBase::GetWidth()
	{
		PACHA_ASSERT(m_ColorAttachments.size() || m_DepthStencilAttachment.texture, "No attahcment has been set");

		if (m_ColorAttachments.size())
			return m_ColorAttachments[0].texture->GetWidth();
		else
			return m_DepthStencilAttachment.texture->GetWidth();
	}

	uint32_t GfxRenderTargetBase::GetHeight()
	{
		PACHA_ASSERT(m_ColorAttachments.size() || m_DepthStencilAttachment.texture, "No attahcment has been set");

		if (m_ColorAttachments.size())
			return m_ColorAttachments[0].texture->GetHeight();
		else
			return m_DepthStencilAttachment.texture->GetHeight();
	}

	void GfxRenderTargetBase::SetAttachments(const std::vector<GfxRenderTargetAttachment>& colorAttachments, const GfxRenderTargetAttachment& depthStencilAttachment)
	{
		SetColorAttachments(colorAttachments);
		SetDepthStencilAttachment(depthStencilAttachment);
	}

	void GfxRenderTargetBase::SetColorAttachments(const std::vector<GfxRenderTargetAttachment>& colorAttachments)
	{
		for (auto const& attachment : colorAttachments)
			SetColorAttachment(attachment);
	}

	void GfxRenderTargetBase::SetColorAttachment(const GfxRenderTargetAttachment& colorAttachment)
	{
		PACHA_ASSERT(colorAttachment.texture, "Color Attachment texture is null");
		PACHA_ASSERT(colorAttachment.index < 8, "Attachment index out of range");

		if (m_ColorAttachments.size() <= colorAttachment.index)
			m_ColorAttachments.resize(colorAttachment.index + 1);

		m_ColorAttachments[colorAttachment.index] = colorAttachment;
		m_ColorHashes[colorAttachment.index] = colorAttachment.texture->GetHash();

		m_HasDataChanged = true;
	}

	void GfxRenderTargetBase::SetDepthStencilAttachment(const GfxRenderTargetAttachment& depthStencilAttachment)
	{
		PACHA_ASSERT(depthStencilAttachment.texture, "DepthStencil texture is null");
		m_DepthHash = depthStencilAttachment.texture->GetHash();
		m_DepthStencilAttachment = depthStencilAttachment;

		m_HasDataChanged = true;
	}

	GfxTexture* GfxRenderTargetBase::GetColorAttachmentTexture(const uint32_t& index)
	{
		PACHA_ASSERT(index < m_ColorAttachments.size(), "Requested attachment out of range");
		return m_ColorAttachments[index].texture;
	}

	const GfxRenderTargetAttachment& GfxRenderTargetBase::GetColorAttachment(const uint32_t& index)
	{
		PACHA_ASSERT(index < m_ColorAttachments.size(), "Requested attachment out of range");
		return m_ColorAttachments[index];
	}

	GfxTexture* GfxRenderTargetBase::GetDepthStencilAttachmentTexture()
	{
		return m_DepthStencilAttachment.texture;
	}

	const GfxRenderTargetAttachment& GfxRenderTargetBase::GetDepthStencilAttachment()
	{
		return m_DepthStencilAttachment;
	}
	void GfxRenderTargetBase::SetMultiViewInfo(const GfxMultiViewInfo& multiViewInfo)
	{
		m_MultiViewInfo = multiViewInfo;
		m_HasDataChanged = true;
	}
	
	void GfxRenderTargetBase::SetDependency(const GfxRenderPassDependency& dependency)
	{
		m_Dependency = dependency;
		m_HasDataChanged = true;
	}
}
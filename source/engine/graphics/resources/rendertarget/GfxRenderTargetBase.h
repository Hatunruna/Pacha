#pragma once
#include "utilities/Hash.h"
#include "graphics/resources/texture/GfxTexture.h"

#include <array>
#include <vector>

namespace Pacha
{
	struct GfxSubResourceTarget
	{
		struct MipSlice
		{
			uint32_t mip = 0;
			uint32_t slice = 0;
		};

		MipSlice depthStencilAttachment = {};
		std::array<MipSlice, 8> colorAttachments;

		static uint32_t CalculateViewIndex(const MipSlice& target, GfxTexture* texture)
		{
			return target.slice * texture->GetMipCount() + target.mip;
		}

		bool operator==(const GfxSubResourceTarget& rhs) const
		{
			return memcmp(this, &rhs, sizeof(GfxSubResourceTarget)) == 0;
		}
	};
}

namespace std
{
	template <>
	struct hash<Pacha::GfxSubResourceTarget>
	{
		size_t operator()(const Pacha::GfxSubResourceTarget& target) const
		{
			size_t hash = Pacha::Hash(target.depthStencilAttachment.mip);
			Pacha::HashCombine(hash, target.depthStencilAttachment.slice);

			for (size_t i = 0; i < 8; ++i)
			{
				Pacha::HashCombine(hash, target.colorAttachments[i].mip);
				Pacha::HashCombine(hash, target.colorAttachments[i].slice);
			}

			return hash;
		}
	};
}

namespace Pacha
{
	struct GfxMultiViewInfo
	{
		uint32_t viewsCount = 1;
		uint32_t viewsMask = 0b00000001;
	};

	enum class GfxLoadOperation
	{
		LOAD,
		CLEAR,
		DONT_CARE
	};

	enum class GfxStoreOperation
	{
		STORE,
		DONT_CARE
	};

	enum class GfxRenderPassDependencyType
	{
		DISABLED,
		SOURCE_EXTERNAL,
		DESTINATION_EXTERNAL
	};

	struct GfxRenderPassDependency
	{
		GfxRenderPassDependencyType type;
		GfxPipelineStageFlags sourceStage;
		GfxPipelineStageFlags destinationStage;

		constexpr static GfxRenderPassDependency NoDependency()
		{
			GfxRenderPassDependency dependency = {
				GfxRenderPassDependencyType::DISABLED,
				GfxPipelineStageFlags_ALL_GRAPHICS,
				GfxPipelineStageFlags_ALL_GRAPHICS
			};
			return dependency;
		}

		constexpr static GfxRenderPassDependency SourceExternal()
		{
			GfxRenderPassDependency dependency = {
				GfxRenderPassDependencyType::SOURCE_EXTERNAL,
				GfxPipelineStageFlags_ALL_GRAPHICS,
				GfxPipelineStageFlags_ALL_GRAPHICS
			};
			return dependency;
		}

		constexpr static GfxRenderPassDependency DestinationExternal()
		{
			GfxRenderPassDependency dependency = {
				GfxRenderPassDependencyType::DESTINATION_EXTERNAL,
				GfxPipelineStageFlags_ALL_GRAPHICS,
				GfxPipelineStageFlags_ALL_GRAPHICS
			};
			return dependency;
		}
	};

	struct GfxRenderTargetAttachment
	{
		uint32_t index = 0;
		GfxTexture* texture = nullptr;

		GfxLoadOperation loadOperation = GfxLoadOperation::DONT_CARE;
		GfxStoreOperation storeOperation = GfxStoreOperation::STORE;

		GfxResourceState initialState = GfxResourceState::UNDEFINED;
		GfxResourceState finalState = GfxResourceState::UNDEFINED;
	};

	class GfxRenderTargetBase
	{
		friend class GfxMemoryManager;

	public:
		GfxMSAA GetMSAA();
		uint32_t GetWidth();
		uint32_t GetHeight();

		void SetMultiViewInfo(const GfxMultiViewInfo& multiViewInfo);
		void SetDependency(const GfxRenderPassDependency& dependency);

		void SetAttachments(const std::vector<GfxRenderTargetAttachment>& colorAttachments, const GfxRenderTargetAttachment& depthStencilAttachment);
		void SetColorAttachments(const std::vector<GfxRenderTargetAttachment>& colorAttachments);
		void SetColorAttachment(const GfxRenderTargetAttachment& colorAttachment);
		void SetDepthStencilAttachment(const GfxRenderTargetAttachment& depthStencilAttachment);

		bool ColorAttachmentDiffer(const uint32_t& index, GfxTexture* colorTexture);
		bool DepthStencilAttachmentDiffer(GfxTexture* depthStencilTexture);

		GfxTexture* GetColorAttachmentTexture(const uint32_t& index = 0);
		const GfxRenderTargetAttachment& GetColorAttachment(const uint32_t& index = 0);

		GfxTexture* GetDepthStencilAttachmentTexture();
		const GfxRenderTargetAttachment& GetDepthStencilAttachment();

	protected:
		GfxRenderTargetBase(const GfxRenderPassDependency& dependency = GfxRenderPassDependency::SourceExternal(), const GfxMultiViewInfo& multiViewInfo = {});
		virtual ~GfxRenderTargetBase();

		GfxMultiViewInfo m_MultiViewInfo;
		GfxRenderPassDependency m_Dependency;

		GfxRenderTargetAttachment m_DepthStencilAttachment;
		std::vector<GfxRenderTargetAttachment> m_ColorAttachments;

		uint64_t m_DepthHash;
		std::array<uint64_t, 8> m_ColorHashes;

		bool HasDataChanged();

	private:
		bool m_HasDataChanged = false;
		virtual void CopyForDeletion(struct GfxRenderTargetDeleteContiner* container) = 0;
	};
}
#pragma once
#include <stdint.h>

namespace Pacha
{
	enum class GfxShaderType
	{
		VERTEX,
		HULL,
		DOMAIN,
		GEOMETRY,
		PIXEL,
		COMPUTE,
		AMPLIFICATION,
		MESH,
		RAY_GEN,
		ANY_HIT,
		CLOSEST_HIT,
		MISS,
		INTERSECTION,
		CALLABLE
	};

	struct GfxShaderSourceInfo
	{
		uint8_t* data;
		uint64_t size;
		uint64_t variant;
	};

	class GfxShaderBase
	{
	public:
		GfxShaderBase(const GfxShaderType& type);
		GfxShaderType GetShaderType();

	protected:
		GfxShaderType m_Type;
	};
};
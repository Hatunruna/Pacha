#pragma once
#include "graphics/resources/shader/GfxShaderBase.h"

#include <vector>
#include <volk.h>
#include <unordered_map>

namespace Pacha
{
	class GfxShaderVK : public GfxShaderBase
	{
	public:
		GfxShaderVK(const GfxShaderType& type, const std::vector<GfxShaderSourceInfo>& shaderSources);
		const VkShaderModule& GetShderModule(const uint64_t& variant);

		static VkShaderStageFlagBits GetShaderStageVK(const GfxShaderType& type);

	private:
		std::unordered_map<uint64_t, VkShaderModule> m_Modules;
	};
};
#include "GfxShaderBase.h"

namespace Pacha
{
	GfxShaderBase::GfxShaderBase(const GfxShaderType& type)
	{
		m_Type = type;
	}
	
	GfxShaderType GfxShaderBase::GetShaderType()
	{
		return m_Type;
	}
}

#pragma once
#include "collections/DynamicBitSet.h"

#if RENDERER_VK
#include "graphics/resources/shader/GfxShaderVK.h"
#endif

namespace Pacha
{
	using ShaderBitSet = DynamicBitSet<uint32_t>;

#if RENDERER_VK
	using GfxShader = GfxShaderVK;
#endif
}
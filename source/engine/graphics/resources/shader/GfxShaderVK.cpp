#include "GfxShaderVK.h"
#include "core/Assert.h"
#include "graphics/device/GfxDevice.h"

namespace Pacha
{
	GfxShaderVK::GfxShaderVK(const GfxShaderType& type, const std::vector<GfxShaderSourceInfo>& shaderSources)
		: GfxShaderBase(type)
	{
		GfxDevice& gfxDevice = GfxDevice::GetInstance();
		const VkDevice& device = gfxDevice.GetVkDevice();

		for (auto const& source : shaderSources)
		{
			VkShaderModuleCreateInfo shaderModuleCreateInfo = {};
			shaderModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
			shaderModuleCreateInfo.codeSize = source.size;
			shaderModuleCreateInfo.pCode = reinterpret_cast<const uint32_t*>(source.data);

			VkResult result = VK_SUCCESS;
			if ((result = vkCreateShaderModule(device, &shaderModuleCreateInfo, nullptr, &m_Modules[source.variant])) != VK_SUCCESS)
			{
				LOGERROR("Could not create shader module: " << GfxDeviceVK::GetErrorString(result));
				continue;
			}
		}
	}

	const VkShaderModule& GfxShaderVK::GetShderModule(const uint64_t& variant)
	{
		PACHA_ASSERT(m_Modules.count(variant), "Variant not present in shader object");
		return m_Modules[variant];
	}

	VkShaderStageFlagBits GfxShaderVK::GetShaderStageVK(const GfxShaderType& type)
	{
		switch (type)
		{
			default:
			{
				LOGCRITICAL("Shader Type not added to this function");
				return VK_SHADER_STAGE_FLAG_BITS_MAX_ENUM;
			}

			case GfxShaderType::VERTEX:			return VK_SHADER_STAGE_VERTEX_BIT;
			case GfxShaderType::HULL:			return VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
			case GfxShaderType::DOMAIN:			return VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
			case GfxShaderType::GEOMETRY:		return VK_SHADER_STAGE_GEOMETRY_BIT;
			case GfxShaderType::PIXEL:			return VK_SHADER_STAGE_FRAGMENT_BIT;
			case GfxShaderType::COMPUTE:		return VK_SHADER_STAGE_COMPUTE_BIT;
			case GfxShaderType::RAY_GEN:		return VK_SHADER_STAGE_RAYGEN_BIT_KHR;
			case GfxShaderType::ANY_HIT:		return VK_SHADER_STAGE_ANY_HIT_BIT_KHR;
			case GfxShaderType::CLOSEST_HIT:	return VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR;
			case GfxShaderType::MISS:			return VK_SHADER_STAGE_MISS_BIT_KHR;
			case GfxShaderType::INTERSECTION:	return VK_SHADER_STAGE_INTERSECTION_BIT_KHR;
			case GfxShaderType::CALLABLE:		return VK_SHADER_STAGE_CALLABLE_BIT_KHR;
		}
	}
}

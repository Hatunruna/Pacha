#pragma once
#include "GfxBuffer.h"
#include "GfxVertexDescriptor.h"

namespace Pacha
{
	class GfxVertexBuffer : public GfxBuffer
	{
	public:
		GfxVertexBuffer(const size_t size, std::shared_ptr<uint8_t> data, const GfxResourceState afterUploadState = GfxResourceState::VERTEX_BUFFER);

		~GfxVertexBuffer()
		{ }

		uint32_t GetBindlessIndex() const { return m_BindlessIndex; }

	private:
		uint32_t m_BindlessIndex;
	};
}
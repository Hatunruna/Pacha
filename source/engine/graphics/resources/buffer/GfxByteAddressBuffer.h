#pragma once
#include "GfxBuffer.h"

namespace Pacha
{
	class GfxByteAddressBuffer : public GfxBuffer
	{
	public:
		GfxByteAddressBuffer(const size_t size, std::shared_ptr<uint8_t> data, const GfxResourceState afterUploadState = GfxResourceState::SHADER_RESOURCE)
			: GfxBuffer({ size, GfxBufferFlags_RAW_VIEW | GfxBufferFlags_COPY_DESTINATION, GfxPixelFormat::UNDEFINED }, data, afterUploadState)
		{ }

		~GfxByteAddressBuffer()
		{ }
	};
}
#pragma once
#include "GfxBuffer.h"

namespace Pacha
{
	class GfxAppendConsumeStructuredBuffer : public GfxBuffer
	{
	public:
		GfxAppendConsumeStructuredBuffer(const size_t size, std::shared_ptr<uint8_t> data, const GfxResourceState afterUploadState = GfxResourceState::SHADER_RESOURCE)
			: GfxBuffer({ size, GfxBufferFlags_STRUCTURED | GfxBufferFlags_APPEND_CONSUME | GfxBufferFlags_COPY_DESTINATION, GfxPixelFormat::UNDEFINED }, data, afterUploadState)
		{ }

		~GfxAppendConsumeStructuredBuffer()
		{ }
	};
}
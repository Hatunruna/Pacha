#pragma once
#include "GfxVertexFormat.h"

#include <vector>

namespace Pacha
{
	enum class GfxVertexElementType
	{
		POSITION,
		NORMAL,
		TANGENT,
		COLOR,
		TEXCOORD0,
		TEXCOORD1,
		TEXCOORD2,
		TEXCOORD3,
		TEXCOORD4,
		TEXCOORD5,
		TEXCOORD6,
		TEXCOORD7,
		TEXCOORD8
	};

	struct GfxVertexElement
	{
		GfxVertexElementType type;
		GfxVertexFormat format;
	};

	class GfxVertexStream
	{
	public:
		GfxVertexStream(const GfxVertexElement& element)
		{
			m_Stride = GetVertexFormatSize(element.format);
			m_Elements.push_back(element);
		}

		GfxVertexStream(const std::vector<GfxVertexElement>& elements)
		{
			for (const auto& element : elements)
				AddElement(element);
		}

		void AddElement(const GfxVertexElement& element)
		{
			m_Stride += GetVertexFormatSize(element.format);
			m_Elements.push_back(element);
		}

		uint32_t GetStride() const { return m_Stride; }
		const std::vector<GfxVertexElement>& GetElements() { return m_Elements; }

	private:
		uint32_t m_Stride = 0;
		std::vector<GfxVertexElement> m_Elements = {};
	};

	class GfxVertexDescriptor
	{
	public:
		GfxVertexDescriptor() = default;
		GfxVertexDescriptor(const GfxVertexStream& stream)
		{
			m_Streams.push_back(stream);
			m_Stride = stream.GetStride();
		}

		GfxVertexDescriptor(const std::vector<GfxVertexStream>& streams)
		{
			m_Streams = streams;
			for(auto& stream : m_Streams)
				m_Stride += stream.GetStride();
		}

		const GfxVertexStream& GetStream(size_t index) const
		{
			PACHA_ASSERT(index < m_Streams.size(), "Index out of range");
			return m_Streams[index];
		}

		uint32_t GetStride() const
		{
			return m_Stride;
		}

	private:
		uint32_t m_Stride = 0;
		std::vector<GfxVertexStream> m_Streams = {};
	};
}
#pragma once
#include "GfxBuffer.h"

#include <vector>

namespace Pacha
{
	enum class GfxIndirectBufferType
	{
		DRAW,
		DRAW_INDEXED,
		DISPATCH
	};

	struct GfxDispatchIndirectCommand
	{
		uint32_t x;
		uint32_t y;
		uint32_t z;
	};

	struct GfxDrawIndirectCommand 
	{
		uint32_t vertexCount;
		uint32_t instanceCount;
		uint32_t firstVertex;
		uint32_t firstInstance;
	};

	struct GfxDrawIndexedIndirectCommand 
	{
		uint32_t indexCount;
		uint32_t instanceCount;
		uint32_t firstIndex;
		int32_t vertexOffset;
		uint32_t firstInstance;
	};

	class GfxIndirectBufferBase : public GfxBuffer
	{
	public:
		virtual GfxIndirectBufferType GetType() const = 0;
	};

	template<GfxIndirectBufferType IndirectType>
	class GfxIndirectBuffer : public GfxIndirectBufferBase
	{
		using GfxIndirectBufferBase::GfxIndirectBufferBase;
		using T = std::conditional_t<IndirectType == GfxIndirectBufferType::DRAW, GfxDrawIndirectCommand,
			std::conditional_t<IndirectType == GfxIndirectBufferType::DRAW_INDEXED, GfxDrawIndexedIndirectCommand,
			GfxDispatchIndirectCommand>>;

	public:
		GfxIndirectBuffer(const size_t elementsCount)
			: GfxBuffer({ elementsCount * sizeof(T), GfxBufferFlags_INDIRECT, GfxPixelFormat::UNDEFINED }, nullptr, GfxResourceState::INDIRECT_ARGUMENT)
		{ }

		~GfxIndirectBuffer()
		{ }

		GfxIndirectBufferType GetType() const override
		{
			return IndirectType;
		}
	};
}
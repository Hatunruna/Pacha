#pragma once
#include "GfxBufferBase.h"

#include <memory>
#include <vk_mem_alloc.h>

namespace Pacha
{
	struct GfxBufferDeleteContiner
	{
		VkBuffer buffer = VK_NULL_HANDLE;
		VmaAllocation allocation = VK_NULL_HANDLE;
	};

	class GfxBufferVK : public GfxBufferBase
	{
		friend class GfxMemoryManager;

	public:
		GfxBufferVK() = default;
		GfxBufferVK(const GfxBufferDescriptor& descriptor, const std::shared_ptr<uint8_t>& data, const GfxResourceState& afterUploadState = GfxResourceState::SHADER_RESOURCE);
		~GfxBufferVK();

		const VkBuffer& GetBuffer();
		const VkBufferView& GetBufferView();
		void SetFromNativeHandle(void* handle, const GfxBufferDescriptor& descriptor) override;

		static VkBufferUsageFlags GetUsageFlags(const GfxBufferFlags flags);
		static VkIndexType GetVkIndexType(const GfxIndexType type);

	private:
		void SetDebugName() override;
		void FlushImplementation() override;
		void CopyForDeletion(GfxBufferDeleteContiner* container) override;

		bool m_HasOwnership = false;
		VkBuffer m_Buffer = VK_NULL_HANDLE;
		VkBufferView m_BufferView = VK_NULL_HANDLE;
		VmaAllocation m_Allocation = VK_NULL_HANDLE;
	};
}
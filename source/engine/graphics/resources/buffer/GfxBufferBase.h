#pragma once
#include "graphics/resources/GfxResourceState.h"
#include "graphics/resources/texture/GfxPixelFormat.h"

#include <string>

namespace Pacha
{
	enum class GfxIndexType
	{
		U16,
		U32
	};

	enum GfxBufferFlagBits
	{
		//Access
		GfxBufferFlags_COPY_DESTINATION						= 1 << 0,
		GfxBufferFlags_COPY_SOURCE							= 1 << 1,
		GfxBufferFlags_CPU_ACCESS							= 1 << 2,
		GfxBufferFlags_ALLOW_UNORDERED_ACCESS				= 1 << 3,
		
		//Usage
		GfxBufferFlags_INDEX								= 1 << 4,
		GfxBufferFlags_VERTEX								= 1 << 5,
		GfxBufferFlags_CONSTANT								= 1 << 6,
		GfxBufferFlags_INDIRECT								= 1 << 7,
		GfxBufferFlags_FORMATTED							= 1 << 8,
		GfxBufferFlags_STRUCTURED							= 1 << 9,
		GfxBufferFlags_STAGING								= 1 << 10,
		GfxBufferFlags_RAW_VIEW								= 1 << 11,
		GfxBufferFlags_APPEND_CONSUME						= 1 << 12,

		//Not implemented yet
		GfxBufferFlags_ACCELERATION_STRUCTURE_BUILD_INPUT	= 1 << 13,
		GfxBufferFlags_ACCELERATION_STRUCTURE_STORAGE		= 1 << 14,
		GfxBufferFlags_SHADER_BINDING_TABLE					= 1 << 15
	};
	typedef uint32_t GfxBufferFlags;

	struct GfxBufferDescriptor
	{
		size_t size = 0;
		GfxBufferFlags flags = 0;
		GfxPixelFormat format = GfxPixelFormat::UNDEFINED;

		friend bool operator == (const GfxBufferDescriptor& lhs, const GfxBufferDescriptor& rhs)
		{
			return memcmp(&lhs, &rhs, sizeof(GfxBufferDescriptor)) == 0;
		}

		friend bool operator != (const GfxBufferDescriptor& lhs, const GfxBufferDescriptor& rhs)
		{
			return memcmp(&lhs, &rhs, sizeof(GfxBufferDescriptor)) != 0;
		}
	};

	class GfxBufferBase
	{
		friend class GfxMemoryManager;

	public:
		virtual ~GfxBufferBase();
		virtual void SetFromNativeHandle(void* handle, const GfxBufferDescriptor& descriptor) = 0;

		const std::string& GetName() const;
		void SetName(const std::string& name);

		uint8_t* GetData() const;
		size_t GetHash() const;
		size_t GetSize() const;
		GfxBufferFlags GetFlags() const;
		const GfxBufferDescriptor& GetDescriptor() const;

		void Flush();

	protected:
		GfxBufferBase() = default;
		GfxBufferBase(const GfxBufferDescriptor& descriptor);

		virtual void SetDebugName() = 0;
		virtual void FlushImplementation() = 0;
		
		size_t m_Hash = 0;
		GfxBufferDescriptor m_Descriptor = {};
		
		std::string m_Name = {};
		uint8_t* m_Data = nullptr;

	private:
		virtual void CopyForDeletion(struct GfxBufferDeleteContiner* container) = 0;
	};
}
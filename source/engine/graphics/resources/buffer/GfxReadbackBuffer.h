#pragma once
#include "GfxBuffer.h"

namespace Pacha
{
	class GfxReadbackBuffer : public GfxBuffer
	{
	public:
		GfxReadbackBuffer(const size_t size)
			: GfxBuffer({ size, GfxBufferFlags_COPY_DESTINATION | GfxBufferFlags_CPU_ACCESS, GfxPixelFormat::UNDEFINED }, nullptr, GfxResourceState::COPY_DESTINATION)
		{ }

		~GfxReadbackBuffer()
		{ }
	};
}
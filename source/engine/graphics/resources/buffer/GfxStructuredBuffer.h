#pragma once
#include "GfxBuffer.h"

namespace Pacha
{
	class GfxStructuredBuffer : public GfxBuffer
	{
	public:
		GfxStructuredBuffer(const size_t size, std::shared_ptr<uint8_t> data, const GfxResourceState afterUploadState = GfxResourceState::SHADER_RESOURCE)
			: GfxBuffer({ size, GfxBufferFlags_STRUCTURED | GfxBufferFlags_COPY_DESTINATION, GfxPixelFormat::UNDEFINED }, data, afterUploadState)
		{ }

		~GfxStructuredBuffer()
		{ }
	};
}
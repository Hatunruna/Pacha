#pragma once
#include "GfxBuffer.h"

namespace Pacha
{
	class GfxRWStructuredBuffer : public GfxBuffer
	{
	public:
		GfxRWStructuredBuffer(const size_t size, std::shared_ptr<uint8_t> data, const GfxResourceState afterUploadState = GfxResourceState::SHADER_RESOURCE)
			: GfxBuffer({ size, GfxBufferFlags_STRUCTURED | GfxBufferFlags_ALLOW_UNORDERED_ACCESS | GfxBufferFlags_COPY_DESTINATION, GfxPixelFormat::UNDEFINED }, data, afterUploadState)
		{ }

		~GfxRWStructuredBuffer()
		{ }
	};
}
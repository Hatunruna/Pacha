#pragma once
#include "GfxBuffer.h"

namespace Pacha
{
	class GfxRWByteAddressBuffer : public GfxBuffer
	{
	public:
		GfxRWByteAddressBuffer(const size_t size, std::shared_ptr<uint8_t> data, const GfxResourceState afterUploadState = GfxResourceState::SHADER_RESOURCE)
			: GfxBuffer({ size, GfxBufferFlags_RAW_VIEW | GfxBufferFlags_ALLOW_UNORDERED_ACCESS | GfxBufferFlags_COPY_DESTINATION, GfxPixelFormat::UNDEFINED }, data, afterUploadState)
		{ }

		~GfxRWByteAddressBuffer()
		{ }
	};
}
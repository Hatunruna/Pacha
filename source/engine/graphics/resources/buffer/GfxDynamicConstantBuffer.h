#pragma once
#include "GfxBuffer.h"

namespace Pacha
{
	class GfxDynamicConstantBuffer : public GfxBuffer
	{
	public:
		GfxDynamicConstantBuffer(const size_t size, uint32_t frames, const GfxResourceState afterUploadState = GfxResourceState::CONSTANT_BUFFER)
			: GfxBuffer({ size * frames, GfxBufferFlags_CONSTANT | GfxBufferFlags_CPU_ACCESS, GfxPixelFormat::UNDEFINED }, nullptr, afterUploadState)
			, m_ElementSize(size)
			, m_Frames(frames)
		{ }

		uint8_t* GetFrameData()
		{
			m_CurrentFrame = (m_CurrentFrame + 1) % m_Frames;
			return GetData() + (m_ElementSize * m_CurrentFrame);
		}

		size_t GetFrameOffset()
		{
			return m_ElementSize * m_CurrentFrame;
		}

	private:
		uint32_t m_Frames = 1;
		size_t m_ElementSize = 0;
		uint32_t m_CurrentFrame = 0;
	};
}
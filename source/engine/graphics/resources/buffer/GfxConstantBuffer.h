#pragma once
#include "GfxBuffer.h"

namespace Pacha
{
	class GfxConstantBuffer : public GfxBuffer
	{
	public:
		GfxConstantBuffer(const size_t size, std::shared_ptr<uint8_t> data, const GfxResourceState afterUploadState = GfxResourceState::CONSTANT_BUFFER)
			: GfxBuffer({ size, GfxBufferFlags_CONSTANT | GfxBufferFlags_COPY_DESTINATION, GfxPixelFormat::UNDEFINED }, data, afterUploadState)
		{ }

		~GfxConstantBuffer()
		{ }
	};
}
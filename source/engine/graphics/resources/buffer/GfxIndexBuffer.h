#pragma once
#include "GfxBuffer.h"

#include <vector>

namespace Pacha
{
	class GfxIndexBufferBase : public GfxBuffer
	{
	public:
		using GfxBuffer::GfxBuffer;
		virtual GfxIndexType GetType() const = 0;
		virtual uint32_t GetElementSize() const = 0;
	};

	template<GfxIndexType IndexType>
	class GfxIndexBuffer : public GfxIndexBufferBase
	{
		using GfxIndexBufferBase::GfxIndexBufferBase;
		using T = std::conditional_t<IndexType == GfxIndexType::U16, uint16_t, uint32_t>;

	public:
		GfxIndexBuffer(const size_t size, std::shared_ptr<uint8_t> data, const GfxResourceState afterUploadState = GfxResourceState::INDEX_BUFFER)
			: GfxIndexBufferBase({ size, GfxBufferFlags_INDEX | GfxBufferFlags_COPY_DESTINATION, GfxPixelFormat::UNDEFINED }, data, afterUploadState)
		{ }

		~GfxIndexBuffer()
		{ }

		GfxIndexType GetType() const override
		{
			return IndexType;
		}

		uint32_t GetElementSize() const override
		{
			return sizeof(T);
		}
	};
}
#pragma once
#include "GfxBuffer.h"

namespace Pacha
{
	class GfxFormattedBuffer : public GfxBuffer
	{
	public:
		GfxFormattedBuffer(GfxPixelFormat format, const size_t size, std::shared_ptr<uint8_t> data, const GfxResourceState afterUploadState = GfxResourceState::SHADER_RESOURCE)
			: GfxBuffer({ size, GfxBufferFlags_FORMATTED | GfxBufferFlags_COPY_DESTINATION, format }, data, afterUploadState)
		{ }

		~GfxFormattedBuffer()
		{ }
	};
}
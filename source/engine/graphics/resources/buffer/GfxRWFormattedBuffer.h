#pragma once
#include "GfxBuffer.h"

namespace Pacha
{
	class GfxRWFormattedBuffer : public GfxBuffer
	{
	public:
		GfxRWFormattedBuffer(GfxPixelFormat format, const size_t size, std::shared_ptr<uint8_t> data, const GfxResourceState afterUploadState = GfxResourceState::SHADER_RESOURCE)
			: GfxBuffer({ size, GfxBufferFlags_FORMATTED | GfxBufferFlags_COPY_DESTINATION | GfxBufferFlags_ALLOW_UNORDERED_ACCESS, format }, data, afterUploadState)
		{ }

		~GfxRWFormattedBuffer()
		{ }
	};
}
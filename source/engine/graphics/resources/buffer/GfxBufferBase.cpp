#include "GfxBuffer.h"
#include "core/Assert.h"
#include "graphics/device/GfxDevice.h"
#include "graphics/utilities/GfxBindlessResourceManager.h"

namespace Pacha
{
	GfxBufferBase::GfxBufferBase(const GfxBufferDescriptor& descriptor)
	{
		PACHA_ASSERT(descriptor.size != 0, "Buffer size must be greater than 0");
		m_Descriptor = descriptor;
	}

	GfxBufferBase::~GfxBufferBase()
	{
		if (GfxDevice::SupportsBindlessResources() && (m_Descriptor.flags & GfxBufferFlags_RAW_VIEW))
		{
			GfxBindlessResourceManager& bindlessManager = GfxBindlessResourceManager::GetInstance();
			bindlessManager.FreeRawBuffer(static_cast<GfxBuffer*>(this));
		}
	}

	const std::string& GfxBufferBase::GetName() const
	{
		return m_Name;
	}

	void GfxBufferBase::SetName(const std::string& name)
	{
		m_Name = name;
		SetDebugName();
	}

	uint8_t* GfxBufferBase::GetData() const
	{
		PACHA_ASSERT((m_Descriptor.flags & GfxBufferFlags_CPU_ACCESS) || (m_Descriptor.flags & GfxBufferFlags_STAGING), "Buffer is not CPU accessible or a staging buffer");
		return m_Data;
	}

	size_t GfxBufferBase::GetHash() const
	{
		return m_Hash;
	}

	size_t GfxBufferBase::GetSize() const
	{
		return m_Descriptor.size;
	}

	GfxBufferFlags GfxBufferBase::GetFlags() const
	{
		return m_Descriptor.flags;
	}

	const GfxBufferDescriptor& GfxBufferBase::GetDescriptor() const
	{
		return m_Descriptor;
	}

	void GfxBufferBase::Flush()
	{
		PACHA_ASSERT(m_Descriptor.flags & GfxBufferFlags_STAGING, "Can't flush non staging buffers");
		FlushImplementation();
	}
}
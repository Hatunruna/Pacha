#include "GfxBufferVK.h"
#include "graphics/device/GfxDevice.h"
#include "graphics/utilities/GfxMemoryManager.h"

namespace Pacha
{
	GfxBufferVK::GfxBufferVK(const GfxBufferDescriptor& descriptor, const std::shared_ptr<uint8_t>& data, const GfxResourceState& afterUploadState)
		: GfxBufferBase(descriptor)
	{
		m_HasOwnership = true;
		GfxDevice& gfxDevice = GfxDevice::GetInstance();
		const VmaAllocator& allocator = gfxDevice.GetMemoryAllocator();

		VkBufferCreateInfo bufferCreateInfo = {};
		bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		bufferCreateInfo.size = m_Descriptor.size;
		bufferCreateInfo.usage = GetUsageFlags(m_Descriptor.flags);
		bufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

		const bool staging = m_Descriptor.flags & GfxBufferFlags_STAGING;
		const bool cpuAccess = m_Descriptor.flags & GfxBufferFlags_CPU_ACCESS;

		VmaAllocationInfo allocationInfo = {};
		VmaAllocationCreateInfo allocationCreateInfo = {};
		allocationCreateInfo.usage = VMA_MEMORY_USAGE_AUTO;
		allocationCreateInfo.flags = VMA_ALLOCATION_CREATE_STRATEGY_BEST_FIT_BIT;

		if (staging)
			allocationCreateInfo.flags |= VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT;
		else if (cpuAccess)
			allocationCreateInfo.flags |= VMA_ALLOCATION_CREATE_HOST_ACCESS_RANDOM_BIT;
		else
			allocationCreateInfo.flags |= VMA_ALLOCATION_CREATE_DEDICATED_MEMORY_BIT;

		VkResult result = VK_SUCCESS;
		if ((result = vmaCreateBuffer(allocator, &bufferCreateInfo, &allocationCreateInfo, &m_Buffer, &m_Allocation, &allocationInfo)) != VK_SUCCESS)
			LOGCRITICAL("Failed to create buffer: " << GfxDeviceVK::GetErrorString(result));

		if (m_Descriptor.flags & GfxBufferFlags_FORMATTED)
		{
			VkBufferViewCreateInfo viewCreateInfo = {};
			viewCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_VIEW_CREATE_INFO;
			viewCreateInfo.buffer = m_Buffer;
			viewCreateInfo.format = GfxTextureVK::GetVkFormat(m_Descriptor.format);
			viewCreateInfo.offset = 0;
			viewCreateInfo.range = VK_WHOLE_SIZE;

			if ((result = vkCreateBufferView(gfxDevice.GetVkDevice(), &viewCreateInfo, nullptr, &m_BufferView)) != VK_SUCCESS)
				LOGCRITICAL("Failed to create buffer view: " << GfxDeviceVK::GetErrorString(result));
		}

		if (cpuAccess)
		{
			if ((result = vmaMapMemory(allocator, m_Allocation, reinterpret_cast<void**>(&m_Data))) != VK_SUCCESS)
				LOGCRITICAL("Failed to map buffer: " << GfxDeviceVK::GetErrorString(result));
		}

		if (cpuAccess && data)
			memcpy(m_Data, data.get(), m_Descriptor.size);
		else if (m_Descriptor.flags & GfxBufferFlags_COPY_DESTINATION && data)
			GfxMemoryManager::GetInstance().AddBufferUpload(this, data, GfxResourceState::UNDEFINED, afterUploadState);

		m_Hash = Hash(m_Buffer);
	}

	GfxBufferVK::~GfxBufferVK()
	{
		GfxDevice& gfxDevice = GfxDevice::GetInstance();
		const VmaAllocator& allocator = gfxDevice.GetMemoryAllocator();

		if (m_Descriptor.flags & GfxBufferFlags_CPU_ACCESS)
			vmaUnmapMemory(allocator, m_Allocation);

		GfxMemoryManager::GetInstance().AddBufferToDelete(*this);
	}

	const VkBuffer& GfxBufferVK::GetBuffer()
	{
		return m_Buffer;
	}

	const VkBufferView& GfxBufferVK::GetBufferView()
	{
		return m_BufferView;
	}

	void GfxBufferVK::SetFromNativeHandle(void* handle, const GfxBufferDescriptor& descriptor)
	{
		PACHA_ASSERT(!m_HasOwnership, "Can not set native handle to a managed buffer");
		PACHA_ASSERT(handle, "Handle can not be null");

		if (!m_HasOwnership)
		{
			m_Buffer = reinterpret_cast<VkBuffer>(handle);
			m_Descriptor = descriptor;
			m_Hash = Hash(m_Buffer);
		}
	}

	VkIndexType GfxBufferVK::GetVkIndexType(const GfxIndexType type)
	{
		switch (type)
		{
			default:
				LOGERROR("Unrecognized technique extension type.");
			case GfxIndexType::U16:
				return VK_INDEX_TYPE_UINT16;
			case GfxIndexType::U32:
				return VK_INDEX_TYPE_UINT32;
		}
	}

	VkBufferUsageFlags GfxBufferVK::GetUsageFlags(const GfxBufferFlags flags)
	{
		VkBufferUsageFlags usageFlags = 0;

		if (flags & (GfxBufferFlags_STRUCTURED | GfxBufferFlags_RAW_VIEW | GfxBufferFlags_APPEND_CONSUME))
			usageFlags |= VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;

		if (flags & GfxBufferFlags_CONSTANT)
			usageFlags |= VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;

		if (flags & GfxBufferFlags_FORMATTED)
		{
			if (flags & GfxBufferFlags_ALLOW_UNORDERED_ACCESS)
				usageFlags |= VK_BUFFER_USAGE_STORAGE_TEXEL_BUFFER_BIT;
			else
				usageFlags |= VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT;
		}

		if (flags & GfxBufferFlags_COPY_DESTINATION)
			usageFlags |= VK_BUFFER_USAGE_TRANSFER_DST_BIT;

		if (flags & GfxBufferFlags_COPY_SOURCE)
			usageFlags |= VK_BUFFER_USAGE_TRANSFER_SRC_BIT;

		if (flags & GfxBufferFlags_INDEX)
			usageFlags |= VK_BUFFER_USAGE_INDEX_BUFFER_BIT;

		if (flags & GfxBufferFlags_VERTEX)
			usageFlags |= VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;

		if (flags & GfxBufferFlags_INDIRECT)
			usageFlags |= VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT;

		if (flags & GfxBufferFlags_ACCELERATION_STRUCTURE_BUILD_INPUT)
			usageFlags |= VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR;

		if (flags & GfxBufferFlags_ACCELERATION_STRUCTURE_STORAGE)
			usageFlags |= VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR;

		if (flags & GfxBufferFlags_SHADER_BINDING_TABLE)
			usageFlags |= VK_BUFFER_USAGE_SHADER_BINDING_TABLE_BIT_KHR;

		return usageFlags;
	}

	void GfxBufferVK::SetDebugName()
	{
		GfxDevice& gfxDevice = GfxDevice::GetInstance();
		gfxDevice.SetObjectDebugName({ m_Name, VK_OBJECT_TYPE_BUFFER, reinterpret_cast<uint64_t>(m_Buffer) });
	}

	void GfxBufferVK::FlushImplementation()
	{
		GfxDevice& gfxDevice = GfxDevice::GetInstance();
		const VmaAllocator& allocator = gfxDevice.GetMemoryAllocator();

		VkResult result = VK_SUCCESS;
		if ((result = vmaFlushAllocation(allocator, m_Allocation, 0, m_Descriptor.size)) != VK_SUCCESS)
			LOGCRITICAL("Failed to flush buffer: " << GfxDeviceVK::GetErrorString(result));
	}

	void GfxBufferVK::CopyForDeletion(GfxBufferDeleteContiner* container)
	{
		if (m_HasOwnership)
		{
			container->buffer = m_Buffer;
			container->allocation = m_Allocation;
		}
	}
}
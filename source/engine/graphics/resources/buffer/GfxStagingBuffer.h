#pragma once
#include "GfxBuffer.h"

namespace Pacha
{
	class GfxStagingBuffer : public GfxBuffer
	{
	public:
		GfxStagingBuffer(const size_t size)
			: GfxBuffer({ size, GfxBufferFlags_STAGING | GfxBufferFlags_CPU_ACCESS | GfxBufferFlags_COPY_SOURCE, GfxPixelFormat::UNDEFINED }, nullptr, GfxResourceState::UNDEFINED)
		{ }

		~GfxStagingBuffer()
		{ }
	};
}
#pragma once
#if RENDERER_VK
#include "graphics/resources/buffer/GfxBufferVK.h"
#endif

namespace Pacha
{
#if RENDERER_VK
	using GfxBuffer = GfxBufferVK;
#endif
}
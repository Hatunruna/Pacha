#pragma once
#include "core/Log.h"
#include "core/Assert.h"

namespace Pacha
{
	enum class GfxVertexFormat
	{
		R8_SINT,
		R8_SNORM,
		R8_UINT,
		R8_UNORM,

		RG8_SINT,
		RG8_SNORM,
		RG8_UINT,
		RG8_UNORM,

		RGB8_SINT,
		RGB8_SNORM,
		RGB8_UINT,
		RGB8_UNORM,

		RGBA8_SINT,
		RGBA8_SNORM,
		RGBA8_UINT,
		RGBA8_UNORM,

		R16_FLOAT,
		R16_SINT,
		R16_SNORM,
		R16_UINT,
		R16_UNORM,

		RG16_FLOAT,
		RG16_SINT,
		RG16_SNORM,
		RG16_UINT,
		RG16_UNORM,

		RGB16_FLOAT,
		RGB16_SINT,
		RGB16_SNORM,
		RGB16_UINT,
		RGB16_UNORM,

		RGBA16_FLOAT,
		RGBA16_SINT,
		RGBA16_SNORM,
		RGBA16_UINT,
		RGBA16_UNORM,

		R32_FLOAT,
		R32_SINT,
		R32_UINT,

		RG32_FLOAT,
		RG32_SINT,
		RG32_UINT,

		RGB32_FLOAT,
		RGB32_SINT,
		RGB32_UINT,

		RGBA32_FLOAT,
		RGBA32_SINT,
		RGBA32_UINT
	};

	inline uint32_t GetVertexFormatSize(const GfxVertexFormat& format)
	{
		switch (format)
		{
			default:
				LOGCRITICAL("Vertex format not registered on this function");
				return 0;

			case GfxVertexFormat::R8_UINT:
			case GfxVertexFormat::R8_SINT:
			case GfxVertexFormat::R8_UNORM:
			case GfxVertexFormat::R8_SNORM:
				return 1;

			case GfxVertexFormat::RG8_UINT:
			case GfxVertexFormat::RG8_SINT:
			case GfxVertexFormat::RG8_UNORM:
			case GfxVertexFormat::RG8_SNORM:

			case GfxVertexFormat::R16_UINT:
			case GfxVertexFormat::R16_SINT:
			case GfxVertexFormat::R16_UNORM:
			case GfxVertexFormat::R16_SNORM:
			case GfxVertexFormat::R16_FLOAT:
				return 2;

			case GfxVertexFormat::RGB8_UINT:
			case GfxVertexFormat::RGB8_SINT:
			case GfxVertexFormat::RGB8_UNORM:
			case GfxVertexFormat::RGB8_SNORM:
				return 3;

			case GfxVertexFormat::RGBA8_UINT:
			case GfxVertexFormat::RGBA8_SINT:
			case GfxVertexFormat::RGBA8_UNORM:
			case GfxVertexFormat::RGBA8_SNORM:

			case GfxVertexFormat::RG16_UINT:
			case GfxVertexFormat::RG16_SINT:
			case GfxVertexFormat::RG16_UNORM:
			case GfxVertexFormat::RG16_SNORM:
			case GfxVertexFormat::RG16_FLOAT:

			case GfxVertexFormat::R32_UINT:
			case GfxVertexFormat::R32_SINT:
			case GfxVertexFormat::R32_FLOAT:
				return 4;

			case GfxVertexFormat::RGB16_UINT:
			case GfxVertexFormat::RGB16_SINT:
			case GfxVertexFormat::RGB16_UNORM:
			case GfxVertexFormat::RGB16_SNORM:
			case GfxVertexFormat::RGB16_FLOAT:
				return 6;

			case GfxVertexFormat::RGBA16_UINT:
			case GfxVertexFormat::RGBA16_SINT:
			case GfxVertexFormat::RGBA16_UNORM:
			case GfxVertexFormat::RGBA16_SNORM:
			case GfxVertexFormat::RGBA16_FLOAT:

			case GfxVertexFormat::RG32_UINT:
			case GfxVertexFormat::RG32_SINT:
			case GfxVertexFormat::RG32_FLOAT:
				return 8;

			case GfxVertexFormat::RGB32_UINT:
			case GfxVertexFormat::RGB32_SINT:
			case GfxVertexFormat::RGB32_FLOAT:
				return 12;

			case GfxVertexFormat::RGBA32_UINT:
			case GfxVertexFormat::RGBA32_SINT:
			case GfxVertexFormat::RGBA32_FLOAT:
				return 16;
		}
	}
}
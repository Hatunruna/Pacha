#include "GfxVertexBuffer.h"
#include "graphics/device/GfxDevice.h"
#include "graphics/utilities/GfxBindlessResourceManager.h"

namespace Pacha
{
	GfxVertexBuffer::GfxVertexBuffer(const size_t size, std::shared_ptr<uint8_t> data, const GfxResourceState afterUploadState)
		: GfxBuffer({ size, GfxBufferFlags_VERTEX | GfxBufferFlags_RAW_VIEW | GfxBufferFlags_COPY_DESTINATION, GfxPixelFormat::UNDEFINED }, data, afterUploadState)
		, m_BindlessIndex(GfxBindlessResourceManager::kInvalidBindlessIndex)
	{
		if (GfxDevice::SupportsBindlessResources())
		{
			GfxBindlessResourceManager& bindlessManager = GfxBindlessResourceManager::GetInstance();
			m_BindlessIndex = bindlessManager.AddRawBuffer(this);
		}
	}
}

#pragma once
#include "utilities/Enum.h"

namespace Pacha
{
	enum GfxPipelineStageFlagBits
	{
		GfxPipelineStageFlags_RENDER_TARGET					= 1 << 0,
		GfxPipelineStageFlags_DEPTH_READ_WRITE				= 1 << 1,
		GfxPipelineStageFlags_DRAW_INDIRECT					= 1 << 2,
		GfxPipelineStageFlags_VERTEX_INPUT					= 1 << 3,
		GfxPipelineStageFlags_VERTEX_SHADER					= 1 << 4,
		GfxPipelineStageFlags_PIXEL_SHADER					= 1 << 5,
		GfxPipelineStageFlags_NON_PIXEL_SHADING				= 1 << 6,
		GfxPipelineStageFlags_ALL_GRAPHICS					= InclusiveBitOr(6),

		GfxPipelineStageFlags_RAY_TRACING_SHADER			= 1 << 7,
		GfxPipelineStageFlags_FRAGMENT_SHADING_RATE			= 1 << 8,
		GfxPipelineStageFlags_COMPUTE_SHADER				= 1 << 9,
		GfxPipelineStageFlags_TRANSFER						= 1 << 10,
		GfxPipelineStageFlags_ACCELERATION_STRUCTURE_BUILD	= 1 << 11,
	
		GfxPipelineStageFlags_ALL_COMMANDS					= InclusiveBitOr(11)
	};
	typedef uint32_t GfxPipelineStageFlags;

	enum class GfxResourceState
	{
		UNDEFINED,
		INDEX_BUFFER,
		VERTEX_BUFFER,
		CONSTANT_BUFFER,
		RENDER_TARGET,
		UNORDERED_ACCESS,
		DEPTH_READ,
		DEPTH_WRITE,
		SHADER_RESOURCE,
		INDIRECT_ARGUMENT,
		COPY_DESTINATION,
		COPY_SOURCE,
		PRESENT,
		SHADING_RATE_MASK,
		ACCELERATION_STRUCTURE_BUILD,
		ACCELERATION_STRUCTURE_SHADER_RESOURCE
	};
}
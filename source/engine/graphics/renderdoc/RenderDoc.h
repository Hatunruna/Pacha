#pragma once
#include "core/Log.h"

#include <renderdoc_app.h>

#if PLATFORM_WINDOWS
#include <Windows.h>
#elif PLATFORM_LINUX
#include <dlfcn.h>
#endif

namespace Pacha
{
	class RenderDoc
	{
	private:
		RenderDoc()
		{
			int returnValue = 0;

#if PLATFORM_WINDOWS
			if (HMODULE module = GetModuleHandle("renderdoc.dll"))
			{
				pRENDERDOC_GetAPI RENDERDOC_GetAPI = reinterpret_cast<pRENDERDOC_GetAPI>(GetProcAddress(module, "RENDERDOC_GetAPI"));
				returnValue = RENDERDOC_GetAPI(eRENDERDOC_API_Version_1_6_0, reinterpret_cast<void**>(&m_RenderDocAPI));
			}
#elif PLATFORM_LINUX
			if (void* mod = dlopen("librenderdoc.so", RTLD_NOW | RTLD_NOLOAD))
			{
				pRENDERDOC_GetAPI RENDERDOC_GetAPI = (pRENDERDOC_GetAPI)dlsym(mod, "RENDERDOC_GetAPI");
				returnValue = RENDERDOC_GetAPI(eRENDERDOC_API_Version_1_6_0, reinterpret_cast<void**>(&m_RenderDocAPI));
			}
#endif

			if (returnValue == 1)
			{
				m_RenderDocAPI->SetCaptureOptionU32(eRENDERDOC_Option_HookIntoChildren, 1);
				m_RenderDocAPI->MaskOverlayBits(eRENDERDOC_Overlay_None, 0);
			}
		}

		RenderDoc(const RenderDoc&) = delete;
		RenderDoc& operator=(const RenderDoc&) = delete;

	public:
		static RenderDoc& GetInstance()
		{
			static RenderDoc sInstance;
			return sInstance;
		}

		bool IsRunning()
		{
			return m_RenderDocAPI != nullptr;
		}

		void BeginFrame()
		{
			if (m_RenderDocAPI && m_ShouldCapture)
			{
				m_ShouldCapture = false;
				m_RenderDocAPI->StartFrameCapture(nullptr, nullptr);
			}
		}

		void EndFrame()
		{
			if (m_RenderDocAPI && m_RenderDocAPI->IsFrameCapturing())
			{
				m_RenderDocAPI->EndFrameCapture(nullptr, nullptr);
				LOGTRACE("RenderDoc finished capturing a frame");
			}
		}

		void TriggerCapture()
		{
			if (!m_ShouldCapture && m_RenderDocAPI && !m_RenderDocAPI->IsFrameCapturing())
				m_ShouldCapture = true;
		}

	private:
		bool m_ShouldCapture = false;
		RENDERDOC_API_1_6_0* m_RenderDocAPI = nullptr;
	};
}
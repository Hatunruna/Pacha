#pragma once
#include "GfxMeshRendererProxy.h"

#include <glm/glm.hpp>

namespace Pacha
{
	class GfxRenderable
	{
	public:
		GfxRenderable(uint32_t index, const glm::mat4& modelMatrix, const MeshRenderer& meshRenderer)
			: m_Index(index)
			, m_ModelMatrix(modelMatrix)
			, m_MeshRenderer(meshRenderer)
		{};

		const uint32_t GetIndex() const { return m_Index; }
		glm::vec3 GetWorldPosition() const { return m_ModelMatrix[3]; }
		const glm::mat4& GetModelMatrix() const { return m_ModelMatrix; }
		const GfxMeshRendererProxy& GetMeshRendererProxy() const { return m_MeshRenderer; }

	private:
		const uint32_t m_Index;
		const glm::mat4 m_ModelMatrix;
		const GfxMeshRendererProxy m_MeshRenderer;
	};
}
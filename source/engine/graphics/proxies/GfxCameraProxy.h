#pragma once
#include "ecs/Entity.h"
#include "ecs/components/Camera.h"
#include "ecs/components/Transform.h"

namespace Pacha
{
	class GfxCameraProxy
	{
	public:
		GfxCameraProxy(const Camera& camera)
			: m_CameraId(camera.GetCameraId())
			, m_Viewport(camera.GetViewport())
			, m_RenderTarget(camera.GetRenderTarget())
			, m_CameraType(camera.GetType())
			, m_View(camera.GetViewMatrix())
			, m_Projection(camera.GetProjectionMatrix())
			, m_ViewProjection(camera.GetViewProjectionMatrix())
			, m_InverseViewProjection(camera.GetInverseViewProjectionMatrix())
		{}

		size_t GetCameraId() const { return m_CameraId; }
		const GfxViewport& GetViewport() const { return m_Viewport; }
		GfxRenderTarget* GetRenderTarget() const { return m_RenderTarget; }
		CameraType GetType() const { return m_CameraType; }

		const glm::mat4& GetViewMatrix() const { return m_View; }
		const glm::mat4& GetProjectionMatrix() const { return m_Projection; }
		const glm::mat4& GetViewProjectionMatrix() const { return m_ViewProjection; }
		const glm::mat4& GetInverseViewProjectionMatrix() const { return m_InverseViewProjection; }
		const glm::vec3 GetWorldPosition() const { return m_View[3]; }

	private:
		size_t m_CameraId = {};
		GfxViewport m_Viewport = {};
		GfxRenderTarget* m_RenderTarget = nullptr;
		CameraType m_CameraType = CameraType::GAME;

		glm::mat4 m_View = glm::mat4(1.f);
		glm::mat4 m_Projection = glm::mat4(1.f);
		glm::mat4 m_ViewProjection = glm::mat4(1.f);
		glm::mat4 m_InverseViewProjection = glm::mat4(1.f);
	};
}
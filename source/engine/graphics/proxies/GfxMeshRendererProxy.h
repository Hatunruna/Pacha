#pragma once
#include "GfxMaterialProxy.h"
#include "ecs/components/MeshRenderer.h"

namespace Pacha
{
	class GfxMeshRendererProxy
	{
	public:
		GfxMeshRendererProxy(const MeshRenderer& meshRenderer)
			: m_OBB(meshRenderer.GetOBB())
			, m_Meshes(meshRenderer.GetMeshes())
			, m_RenderFlags(meshRenderer.GetRenderFlags())
		{
			const std::vector<GfxMaterial*>& materials = meshRenderer.GetMaterials();
			m_Materials.reserve(materials.size());
			
			for (auto material : materials)
			{
				if (material)
					m_Materials.push_back(new GfxMaterialProxy(material));
				else
					m_Materials.push_back(nullptr);
			}
		}

		~GfxMeshRendererProxy()
		{
			for (auto material : m_Materials)
				delete material;
		}

		const OBB& GetOBB() const { return m_OBB; }
		RenderFlags GetRenderFlags() const { return m_RenderFlags; }
		const std::vector<GfxMesh*>& GetMeshes() const { return m_Meshes; }
		const std::vector<GfxMaterialProxy*>& GetMaterials() const { return m_Materials; }

	private:
		OBB m_OBB = {};
		RenderFlags m_RenderFlags = 0;
		std::vector<GfxMesh*> m_Meshes;
		std::vector<GfxMaterialProxy*> m_Materials;
	};
}
#pragma once
#include "graphics/resources/material/GfxMaterial.h"

namespace Pacha
{
	class GfxMaterialProxy
	{
	public:
		GfxMaterialProxy(GfxMaterial* material)
			: m_Id(material->GetId())
			, m_Technique(material->GetTechnique())
			, m_HwdMaterial(material->GetHwdMaterial())
			, m_RenderType(material->GetRenderType())
			, m_RendererState(material->GetRendererState())
		{}

		uint64_t GetId() const { return m_Id; }
		GfxTechnique* GetTechnique() const { return m_Technique; }
		GfxHwdMaterial* GetHwdMaterial() const { return m_HwdMaterial; }
		GfxRenderType GetRenderType() const { return m_RenderType; }
		const GfxRendererState& GetRendererStates() const { return m_RendererState; }

	private:
		uint64_t m_Id = 0;
		GfxTechnique* m_Technique = nullptr;
		GfxHwdMaterial* m_HwdMaterial = nullptr;
		
		GfxRenderType m_RenderType;
		GfxRendererState m_RendererState;
	};
}
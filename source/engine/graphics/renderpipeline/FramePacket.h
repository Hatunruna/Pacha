#pragma once
#include "utilities/PachaId.h"
#include "ecs/components/Camera.h"
#include "utilities/timer/Timer.h"

#include "graphics/proxies/GfxRenderable.h"
#include "graphics/proxies/GfxCameraProxy.h"

#if PACHA_EDITOR
#include "graphics/ui/UITexture.h"
#include "graphics/imgui/ImDrawDataSnapshot.h"
#endif

#include <memory>
#include <unordered_map>

namespace Pacha
{
	struct FramePacket
	{
		uint64_t simulationFrame;
		std::vector<GfxCameraProxy> cameras;
		std::vector<GfxRenderable> dynamicObjects;
		std::vector<GfxRenderable> selectedObjects;
		glm::vec2 viewportResolution = glm::vec2(1.f);

#if PACHA_EDITOR
		std::vector<UITexture> uiTextures;
		std::unordered_map<struct ImGuiViewport*, ImDrawDataSnapshot> drawDataSnapshots;
#endif

#if !PACHA_SHIPPING
		std::deque<ProfilerStats> cpuProfilerStats;
#endif

		void Clear()
		{
			cameras.clear();
			dynamicObjects.clear();
			selectedObjects.clear();

#if PACHA_EDITOR
			uiTextures.clear();
			for (auto& [viewport, snapshot] : drawDataSnapshots)
				snapshot.Clear();
#endif
		}
	};
}
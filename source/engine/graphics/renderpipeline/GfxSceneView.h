#pragma once
#include "math/Frustum.h"
#include "GfxRenderBatch.h"
#include "graphics/memory/GpuRingBuffer.h"

#include <vector>
#include <glm/glm.hpp>

namespace Pacha
{
	struct GfxSceneViewDescriptor
	{
		const GfxCameraProxy* camera = nullptr;
		GfxSceneViewType type = GfxSceneViewType_GAME;

		friend bool operator==(const GfxSceneViewDescriptor& lhs, const GfxSceneViewDescriptor& rhs)
		{
			const bool eq = lhs.camera->GetCameraId() == rhs.camera->GetCameraId();
			return eq;
		}
	};

	class GfxSceneView
	{
	public:
		GfxSceneView() = default;
		GfxSceneView(GfxSceneViewDescriptor descriptor)
			: m_Descriptor(descriptor)
			, m_Frustum(descriptor.camera->GetViewProjectionMatrix())
		{
			m_Hash = Pacha::Hash(descriptor);
		}

		const GfxRenderBatches& GetRenderBatches(const GfxRenderBatchDescriptor& descriptor) const
		{
			PACHA_ASSERT(m_RenderBatches.count(descriptor), "RenderBatchDescriptor not found");
			return m_RenderBatches.at(descriptor);
		}

		const GfxSceneViewDescriptor& GetDescriptor() const
		{
			return m_Descriptor;
		}

		const Frustum& GetFrustum() const
		{
			return m_Frustum;
		}

		const GfxRenderList& GetRenderList() const
		{
			return m_RenderList;
		}

		size_t GetTotalBatchedObjects() const
		{
			return m_TotalBatchedObjects;
		}

		size_t GetHash() const
		{
			return m_Hash;
		}

		void SetRenderList(GfxRenderList& renderList)
		{
			m_RenderList = std::move(renderList);
		}

		void SetRenderBatches(const GfxRenderBatchDescriptor& descriptor, GfxRenderBatches& renderBatches)
		{
			m_RenderBatches[descriptor] = std::move(renderBatches);
		}

		void SetInstanceIndexList(const GfxRenderBatchDescriptor& descriptor, GpuRingBufferAllocation& allocation)
		{
			m_InstanceIndexList[descriptor] = std::move(allocation);
		}

		void SetTotalBatchedObjects(size_t totalBatchedObjects)
		{
			m_TotalBatchedObjects = totalBatchedObjects;
		}

		PachaId GetResourceId(const PachaId& resourceId) const
		{
			return PachaId(resourceId.GetHash() ^ m_Hash, resourceId.GetName());
		}

		void SetConstantBuffer(class GfxDynamicConstantBuffer* buffer)
		{
			m_ConstantBuffer = buffer;
		}

		class GfxDynamicConstantBuffer* GetConstantBuffer() const
		{
			return m_ConstantBuffer;
		}

	private:
		Frustum m_Frustum;
		GfxSceneViewDescriptor m_Descriptor = {};
		class GfxDynamicConstantBuffer* m_ConstantBuffer = nullptr;
		std::vector<const GfxRenderable*> m_VisibleDynamicObjects = {};

		size_t m_Hash = 0;
		GfxRenderList m_RenderList;
		size_t m_TotalBatchedObjects = 0;

		std::unordered_map<GfxRenderBatchDescriptor, GfxRenderBatches> m_RenderBatches;
		std::unordered_map<GfxRenderBatchDescriptor, GpuRingBufferAllocation> m_InstanceIndexList;
	};
}

namespace std
{
	template <>
	struct hash<Pacha::GfxSceneViewDescriptor>
	{
		size_t operator()(const Pacha::GfxSceneViewDescriptor& descriptor) const
		{
			return descriptor.camera->GetCameraId();
		}
	};
}
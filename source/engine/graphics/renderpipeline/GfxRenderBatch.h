#pragma once
#include "GfxRenderList.h"

#include <vector>

namespace Pacha
{
	struct GfxRenderBatch
	{
		GfxDrawCall drawCall;
		uint32_t instanceCount;
		uint32_t instanceDataOffset;
	};

	struct GfxRenderBatches
	{
		std::vector<GfxRenderBatch> opaque;
		std::vector<GfxRenderBatch> alphaTest;
		std::vector<GfxRenderBatch> transparent;
	};
}
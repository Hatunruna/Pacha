#include "GfxRenderPipeline.h"

#include "core/Assert.h"
#include "math/Utilities.h"
#include "core/SystemInfo.h"
#include "core/time/RenderTime.h"
#include "graphics/swapchain/GfxSwapchain.h"
#include "graphics/renderpass/GfxRenderPass.h"

namespace Pacha
{
	GfxRenderPipeline::GfxRenderPipeline(const PachaId& id)
		: m_Id(id), m_RenderCpuProfiler("Render"_ID), m_SetupCpuProfiler("Setup"_ID)
	{
		GfxBufferDescriptor descriptor = {};
		descriptor.size = SizeMB(1ull);
		descriptor.flags = GfxBufferFlags_STRUCTURED | GfxBufferFlags_RAW_VIEW | GfxBufferFlags_CPU_ACCESS;
		
		GfxSwapchain& gfxSwapchain = GfxSwapchain::GetMainSwapchain();
		m_DynamicFrameMemory = new GpuRingBuffer(descriptor, gfxSwapchain.GetBackBufferCount());
		m_GlobalStateBuffer = new GfxDynamicConstantBuffer(sizeof(Shaders::GlobalState), gfxSwapchain.GetBackBufferCount());
	}

	GfxRenderPipeline::~GfxRenderPipeline()
	{
		delete m_GlobalStateBuffer;
		delete m_DynamicFrameMemory;

		for (auto& aliasedGroup : m_AliasedResourceGroups)
			m_ResourceAliaser.FreeAliasingGroupMemory(aliasedGroup);

		for (auto& [id, managedTexture] : m_ManagedTextures)
			delete managedTexture;

		m_SceneViewData.Clear();
		m_ManagedTextures.clear();
	}

	void GfxRenderPipeline::Setup(RenderContext& context)
	{
		NOT_SHIPPING(m_SetupCpuProfiler.BeginProfiler();)

		m_CallbackWaitEvents.clear();
		m_CallbackSignalEvents.clear();

		m_BufferAliases.clear();
		m_RegisteredBuffers.clear();
		
		m_TextureAliases.clear();
		m_RegisteredTextures.clear();

		for (auto& handler : m_CallbackHandlers)
			handler->Clear();

		m_CommandBufferLookup.clear();
		m_ExecutableRenderPasses.clear();
		m_RenderBatchDescriptors.clear();

		RenderTime& renderTime = RenderTime::GetInstance();
		m_DynamicFrameMemory->ReleaseCompletedFrames(renderTime.GetFrameCount());

#if PACHA_EDITOR
		SetTargetResolution(context.GetFramePacket().viewportResolution);
#endif

		for (auto& renderPass : m_RenderPasses)
		{
			if (renderPass->ShouldRun(context))
			{
				GfxRenderPass* renderPassPtr = renderPass.get();
				m_CommandBufferLookup[renderPassPtr] = nullptr;
				m_ExecutableRenderPasses.push_back(renderPassPtr);
				m_RenderBatchDescriptors.emplace(renderPassPtr->GetRenderBatchDescriptor());
			}
		}
		
		GatherSceneViews(context);

		const size_t dynamicObjectsMemoryRequired = CalculateDynamicObjectsFrameMemoryRequirements(context); 
		if (m_DynamicFrameMemory->GetMaxSize() < dynamicObjectsMemoryRequired)
		{
			delete m_DynamicFrameMemory;

			GfxBufferDescriptor descriptor = {};
			descriptor.flags = GfxBufferFlags_RAW_VIEW | GfxBufferFlags_CPU_ACCESS;
			descriptor.size = dynamicObjectsMemoryRequired;

			GfxSwapchain& gfxSwapchain = GfxSwapchain::GetMainSwapchain();
			m_DynamicFrameMemory = new GpuRingBuffer(descriptor, gfxSwapchain.GetBackBufferCount());
		}

		UpdateGlobalState(context);
		CopyPerInstanceData(context);

		m_SceneViewData.ClearFrameCounted();
		for (auto& [descriptor, sceneView] : context.GetSceneViews())
		{
			BuildSceneViewData(sceneView);
			CompactRenderLists(sceneView);
		}

		for (auto& renderPass : m_ExecutableRenderPasses)
		{
			renderPass->SetupViewData(context);
			renderPass->RequestResources(context);
		}

		SetupResources();
		m_DynamicFrameMemory->FinishCurrentFrame(renderTime.GetFrameCount());

		NOT_SHIPPING(m_SetupCpuProfiler.EndProfiler();)
	}

	void GfxRenderPipeline::Render(RenderContext& context, CommandBufferAllocator& commandBufferAllocator)
	{
		NOT_SHIPPING(m_RenderCpuProfiler.BeginProfiler();)
		marl::WaitGroup tasksWaitGroup(static_cast<uint32_t>(m_ExecutableRenderPasses.size()));
		for (auto& renderPass : m_ExecutableRenderPasses)
		{
			marl::schedule([&, tasksWaitGroup]
			{
				if (m_CallbackWaitEvents.count(renderPass->GetId()))
				{
					for (PachaId& eventId : m_CallbackWaitEvents.at(renderPass->GetId()))
					{
						if (m_CallbackSignalEvents.count(eventId))
							m_CallbackSignalEvents.at(eventId).wait();
					}
				}

				GfxCommandBuffer& cmdBuffer = commandBufferAllocator.GetCommandBuffer();
				cmdBuffer.SetName(renderPass->GetId().GetName());
				m_CommandBufferLookup[renderPass] = &cmdBuffer;

				cmdBuffer.BeginCommandBuffer();
				CPU_BEGIN_PROFILING(renderPass);
				GPU_BEGIN_PROFILING(renderPass, &cmdBuffer);

				cmdBuffer.BeginSectionMarker(renderPass->GetId().GetName(), glm::vec4(0.f, 0.5f, 0.f, 1.f));
				renderPass->Render(context, cmdBuffer);
				cmdBuffer.EndSectionMarker();

				GPU_END_PROFILING(renderPass, &cmdBuffer);
				CPU_END_PROFILING(renderPass);
				cmdBuffer.EndCommandBuffer();

				tasksWaitGroup.done();
			});
		}
		tasksWaitGroup.wait();
		NOT_SHIPPING(m_RenderCpuProfiler.EndProfiler();)
	}

	size_t GfxRenderPipeline::GetPassesCount()
	{
		return m_RenderPasses.size();
	}

	void GfxRenderPipeline::SetTargetResolution(const glm::uvec2& resolution)
	{
		m_TargetResolution = resolution;
	}

	const glm::uvec2& GfxRenderPipeline::GetTargetResolution() const
	{
		return m_TargetResolution;
	}

	GfxTexture* GfxRenderPipeline::GetTexture(const PachaId& id)
	{
		PACHA_ASSERT(m_RegisteredTextures.count(id) || (m_TextureAliases.count(id) && m_RegisteredTextures.count(m_TextureAliases[id])), "Texture '" << id.GetName() << "' not requested");
		const PachaId realId = m_TextureAliases.count(id) ? m_TextureAliases[id] : id;
		return m_RegisteredTextures[realId];
	}

	GfxBuffer* GfxRenderPipeline::GetBuffer(const PachaId& id)
	{
		PACHA_ASSERT(m_RegisteredBuffers.count(id) || (m_BufferAliases.count(id) && m_RegisteredBuffers.count(m_BufferAliases[id])), "Buffer '" << id.GetName() << "' not requested");
		const PachaId realId = m_BufferAliases.count(id) ? m_BufferAliases[id] : id;
		return m_RegisteredBuffers[realId];
	}

	void GfxRenderPipeline::RequestPersistentTexture(const PachaId& resourceId, const GfxTextureDescriptor& descriptor)
	{
		PACHA_ASSERT(!m_ResourceRequests.count(resourceId), "There is already a request for creating a resource with name " << resourceId.GetName());
		GfxResourceRequest& request = m_ResourceRequests[resourceId];

		request.accessMask = UINT64_MAX;
		request.descriptor.texture = descriptor;
		request.type = GfxResourceRequestType::TEXTURE;
		request.size = GfxTextureBase::CalculateTextureDataSize(descriptor);
	}

	void GfxRenderPipeline::RequestTexture(const PachaId& passId, const PachaId& resourceId, const GfxTextureDescriptor& descriptor)
	{
		PACHA_ASSERT(!m_ResourceRequests.count(resourceId), "There is already a request for creating a resource with name " << resourceId.GetName());
		GfxResourceRequest& request = m_ResourceRequests[resourceId];

		request.descriptor.texture = descriptor;
		request.type = GfxResourceRequestType::TEXTURE;
		request.accessMask |= (static_cast<uint64_t>(1) << m_RenderPassLookup[passId]);
		request.size = GfxTextureBase::CalculateTextureDataSize(descriptor);
	}

	void GfxRenderPipeline::RequestPersistentBuffer(const PachaId& resourceId, const GfxBufferDescriptor& descriptor)
	{
		PACHA_ASSERT(!m_ResourceRequests.count(resourceId), "There is already a request for creating a resource with name " << resourceId.GetName());
		GfxResourceRequest& request = m_ResourceRequests[resourceId];

		request.descriptor.buffer = descriptor;
		request.type = GfxResourceRequestType::BUFFER;
		request.accessMask = UINT64_MAX;
		request.size = descriptor.size;
	}

	void GfxRenderPipeline::RequestBuffer(const PachaId& passId, const PachaId& resourceId, const GfxBufferDescriptor& descriptor)
	{
		PACHA_ASSERT(!m_ResourceRequests.count(resourceId), "There is already a request for creating a resource with name " << resourceId.GetName());
		GfxResourceRequest& request = m_ResourceRequests[resourceId];

		request.descriptor.buffer = descriptor;
		request.type = GfxResourceRequestType::BUFFER;
		request.accessMask |= (static_cast<uint64_t>(1) << m_RenderPassLookup[passId]);
		request.size = descriptor.size;
	}

	void GfxRenderPipeline::ReadWrite(const PachaId& passId, const PachaId& resourceId)
	{
		PACHA_ASSERT(m_ResourceRequests.count(resourceId), "There is no resource of name '" << resourceId.GetName() << "' in the pipeline");
		m_ResourceRequests[resourceId].accessMask |= (static_cast<uint64_t>(1) << m_RenderPassLookup[passId]);
	}

	void GfxRenderPipeline::AliasTexture(const PachaId& originalId, const PachaId& newId)
	{
		PACHA_ASSERT(m_ResourceRequests.count(originalId), "Texture '" << originalId.GetName() << "' not requested");
		PACHA_ASSERT(!m_ResourceRequests.count(newId), "Texture '" << newId.GetName() << "' already requested");
		PACHA_ASSERT(!m_TextureAliases.count(newId), "Texture alias '" << newId.GetName() << "' already registered");
		m_TextureAliases[newId] = originalId;
	}

	void GfxRenderPipeline::AliasBuffer(const PachaId& originalId, const PachaId& newId)
	{
		PACHA_ASSERT(m_ResourceRequests.count(originalId), "Buffer '" << originalId.GetName() << "' not requested");
		PACHA_ASSERT(!m_ResourceRequests.count(newId), "Buffer '" << newId.GetName() << "' already requested");
		PACHA_ASSERT(!m_BufferAliases.count(newId), "Buffer alias '" << newId.GetName() << "' already registered");
		m_BufferAliases[newId] = originalId;
	}

	GfxRenderPass* GfxRenderPipeline::GetRenderPass(const PachaId& passId)
	{
		PACHA_ASSERT(m_RenderPassLookup.count(passId), "RenderPass not present in current pipeline");
		return m_RenderPasses[m_RenderPassLookup[passId]].get();
	}

	const std::vector<class GfxRenderPass*>& GfxRenderPipeline::GetExecutableRenderPasses()
	{
		return m_ExecutableRenderPasses;
	}

	PachaId GfxRenderPipeline::GetId() const
	{
		return m_Id;
	}

	const CpuProfiler& GfxRenderPipeline::GetSetupCpuProfiler() const
	{
		return m_SetupCpuProfiler;
	}

	const CpuProfiler& GfxRenderPipeline::GetRenderCpuProfiler() const
	{
		return m_RenderCpuProfiler;
	}

	std::vector<GfxCommandBuffer*> GfxRenderPipeline::GetSubmissionOrderedCommandBuffers()
	{
		std::vector<GfxCommandBuffer*> commandBuffers;
		commandBuffers.reserve(m_CommandBufferLookup.size());

		for (auto& renderPass : m_ExecutableRenderPasses)
			if (GfxCommandBuffer* commandBuffer = m_CommandBufferLookup[renderPass])
				commandBuffers.push_back(commandBuffer);

		return commandBuffers;
	}

	void GfxRenderPipeline::AddCallbackHandler(GfxRenderPipelineCallbackHandler* handler)
	{
		m_CallbackHandlers.push_back(handler);
	}

	bool GfxRenderPipeline::InputCallbackExists(const PachaId& id)
	{
		for (const auto& handler : m_CallbackHandlers)
			if (handler->Contains(id))
				return true;
		return false;
	}

	void GfxRenderPipeline::GatherSceneViews(RenderContext& context)
	{
		const FramePacket& framePacket = context.GetFramePacket();
		for (auto& camera : framePacket.cameras)
		{
			GfxSceneViewDescriptor descriptor = {};
			descriptor.camera = &camera;
			descriptor.type = static_cast<GfxSceneViewType>(camera.GetType());

			GfxSceneView& sceneView = context.AddSceneView(descriptor);
			CullSceneView(sceneView, context);
		}

		for (auto& renderPass : m_ExecutableRenderPasses)
		{
			std::vector<GfxSceneViewDescriptor> descriptors = renderPass->GetSceneViewDescriptors();
			for (auto& descriptor : descriptors)
			{
				GfxSceneView& sceneView = context.AddSceneView(descriptor);
				CullSceneView(sceneView, context);
			}
		}
	}

	void GfxRenderPipeline::UpdateGlobalState(RenderContext&)
	{
		RenderTime& time = RenderTime::GetInstance();
		m_GlobalState.delta = time.GetDelta();
		m_GlobalState.frame = static_cast<uint32_t>(time.GetFrameCount());
		m_GlobalState.time = time.GetTimeSinceStart();

		memcpy(m_GlobalStateBuffer->GetFrameData(), &m_GlobalState, sizeof(Shaders::GlobalState));
	}

	void GfxRenderPipeline::CopyPerInstanceData(RenderContext& context)
	{
		const FramePacket& framePacket = context.GetFramePacket();
		const size_t neededMemory = sizeof(Shaders::InstanceData) * framePacket.dynamicObjects.size();

		m_DynamicObjectsAllocation = m_DynamicFrameMemory->Allocate(neededMemory);
		for (auto const& object : framePacket.dynamicObjects)
		{
			Shaders::InstanceData* instanceData = reinterpret_cast<Shaders::InstanceData*>(m_DynamicObjectsAllocation.mappedDataPtr + (object.GetIndex() * sizeof(Shaders::InstanceData)));
			instanceData->model = object.GetModelMatrix();
			instanceData->normalMatrix = glm::transpose(instanceData->model);
		}
	}

	void GfxRenderPipeline::ProcessResourceRequest(const GfxAliasingRequest& aliasedRequest)
	{
		if (aliasedRequest.request.type == GfxResourceRequestType::TEXTURE)
		{
			if (m_ManagedTextures.count(aliasedRequest.id))
			{
				GfxTexture* texture = m_ManagedTextures[aliasedRequest.id];
				if (texture->GetDescriptor() != aliasedRequest.request.descriptor.texture)
				{
					delete texture;
					m_ManagedTextures[aliasedRequest.id] = new GfxTexture(aliasedRequest.request.descriptor.texture, nullptr);
					m_ManagedTextures[aliasedRequest.id]->SetName(aliasedRequest.id.GetName());
				}
			}
			else
			{
				m_ManagedTextures[aliasedRequest.id] = new GfxTexture(aliasedRequest.request.descriptor.texture, nullptr);
				m_ManagedTextures[aliasedRequest.id]->SetName(aliasedRequest.id.GetName());
			}

			RegisterTexture(aliasedRequest.id, m_ManagedTextures[aliasedRequest.id]);
		}
		else if (aliasedRequest.request.type == GfxResourceRequestType::BUFFER)
		{
			if (m_ManagedBuffers.count(aliasedRequest.id))
			{
				GfxBuffer* buffer = m_ManagedBuffers[aliasedRequest.id];
				if (buffer->GetDescriptor() != aliasedRequest.request.descriptor.buffer)
				{
					delete buffer;
					m_ManagedBuffers[aliasedRequest.id] = new GfxBuffer(aliasedRequest.request.descriptor.buffer, nullptr);
					m_ManagedBuffers[aliasedRequest.id]->SetName(aliasedRequest.id.GetName());
				}
			}
			else
			{
				m_ManagedBuffers[aliasedRequest.id] = new GfxBuffer(aliasedRequest.request.descriptor.buffer, nullptr);
				m_ManagedBuffers[aliasedRequest.id]->SetName(aliasedRequest.id.GetName());
			}

			RegisterBuffer(aliasedRequest.id, m_ManagedBuffers[aliasedRequest.id]);
		}
	}

	void GfxRenderPipeline::SetupResources()
	{
		//Build access blocks
		struct IdRequestPair
		{
			PachaId id;
			GfxResourceRequest request;
		};

		std::vector<IdRequestPair> resourceRequests;
		resourceRequests.reserve(m_ResourceRequests.size());

		for (auto& [id, request] : m_ResourceRequests)
		{
			if (request.accessMask != 0)
			{
				uint32_t minBit = FindFirstBitSet64(request.accessMask);
				uint32_t maxBit = FindLastBitSet64(request.accessMask);
				request.accessMask = ((static_cast<uint64_t>(1) << maxBit) - (static_cast<uint64_t>(1) << minBit)) | (static_cast<uint64_t>(1) << maxBit);
			}

			resourceRequests.push_back({ id, request });
		}

		//Sort descending on size
		std::sort(resourceRequests.begin(), resourceRequests.end(), [](const IdRequestPair& lhs, const IdRequestPair& rhs)
		{ return lhs.request.size > rhs.request.size; });

		//Find Aliasing Groups
		std::vector<GfxAliasingGroupDescriptor> aliasingGroups;
		std::vector<IdRequestPair>::iterator resourceRequest = resourceRequests.begin();
		while (resourceRequest != resourceRequests.end())
		{
			bool foundGroup = false;
			for (auto& group : aliasingGroups)
			{
				GfxAliasingIntersection intersectionType = group.Intersect(resourceRequest->request.accessMask);
				if (intersectionType == GfxAliasingIntersection::NONE)
				{
					group.Insert({ resourceRequest->id, 0, resourceRequest->request });
					foundGroup = true;
					break;
				}
				else if (intersectionType == GfxAliasingIntersection::PARTIAL)
				{
					size_t offset = 0;
					for (auto& member : group.members)
					{
						if (member.request.accessMask & resourceRequest->request.accessMask)
						{
							offset = member.offset + member.request.size;
							if (offset + resourceRequest->request.size <= group.members[0].request.size)
							{
								group.Insert({ resourceRequest->id, offset, resourceRequest->request });
								foundGroup = true;
							}
							break;
						}
					}

					if (foundGroup)
						break;
				}
				else
				{
					continue;
				}
			}

			if (!foundGroup)
			{
				GfxAliasingGroupDescriptor group;
				group.Insert({ resourceRequest->id, 0, resourceRequest->request });
				aliasingGroups.push_back(group);
			}

			resourceRequest = resourceRequests.erase(resourceRequest);
		}

		//Determine which groups need recreation or removal
		for (auto aliasedGroup = aliasingGroups.begin(); aliasedGroup != aliasingGroups.end();)
		{
			if (aliasedGroup->members.size() == 1)
			{
				++aliasedGroup;
				continue;
			}

			for (auto resourceGroup = m_AliasedResourceGroups.begin(); resourceGroup != m_AliasedResourceGroups.end();)
			{
				if (resourceGroup->ContainsAllResources(aliasedGroup->members))
				{
					bool needsRecreating = false;
					for (auto const& member : aliasedGroup->members)
						needsRecreating |= resourceGroup->ResourceDiffer(member);

					if (needsRecreating)
					{
						m_ResourceAliaser.FreeAliasingGroupMemory(*resourceGroup);
						resourceGroup = m_AliasedResourceGroups.erase(resourceGroup);
					}
					else
					{
						for (auto const& [id, texture] : resourceGroup->textures)
							RegisterTexture(id, texture);

						for (auto const& [id, buffer] : resourceGroup->buffers)
							RegisterBuffer(id, buffer);

						aliasedGroup = aliasingGroups.erase(aliasedGroup);
						++resourceGroup;
						continue;
					}
				}
				else
				{
					m_ResourceAliaser.FreeAliasingGroupMemory(*resourceGroup);
					resourceGroup = m_AliasedResourceGroups.erase(resourceGroup);
				}
			}

			++aliasedGroup;
		}

		//Create Resources
		for (auto const& group : aliasingGroups)
		{
			if (group.members.size() > 1)
			{
				GfxAliasedResourceGroup aliasedGroup;
				if (m_ResourceAliaser.AliasResources(group, aliasedGroup))
				{
					for (auto const& [id, texture] : aliasedGroup.textures)
						RegisterTexture(id, texture);

					for (auto const& [id, buffer] : aliasedGroup.buffers)
						RegisterBuffer(id, buffer);

					m_AliasedResourceGroups.push_back(aliasedGroup);
				}
				else
				{
					for (auto const& member : group.members)
						ProcessResourceRequest(member);
				}
			}
			else
				ProcessResourceRequest(group.members[0]);
		}

		//Remove unnecesary resources
		std::vector<PachaId> texturesToRemove;
		for (const auto& [managedTextureId, texture] : m_ManagedTextures)
		{
			if (!m_ResourceRequests.count(managedTextureId))
				texturesToRemove.push_back(managedTextureId);
		}

		std::vector<PachaId> buffersToRemove;
		for (const auto& [managedBufferId, buffer] : m_ManagedBuffers)
		{
			if (!m_ResourceRequests.count(managedBufferId))
				buffersToRemove.push_back(managedBufferId);
		}

		for (auto const& texture : texturesToRemove)
		{
			delete m_ManagedTextures[texture];
			m_ManagedTextures.erase(texture);
		}

		for (auto const& buffer : buffersToRemove)
		{
			delete m_ManagedBuffers[buffer];
			m_ManagedBuffers.erase(buffer);
		}

		m_ResourceRequests.clear();
	}

	void GfxRenderPipeline::RegisterTexture(const PachaId& id, GfxTexture* texture)
	{
		m_RegisteredTextures[id] = texture;
	}

	void GfxRenderPipeline::RegisterBuffer(const PachaId& id, GfxBuffer* buffer)
	{
		m_RegisteredBuffers[id] = buffer;
	}

	void GfxRenderPipeline::CullSceneView(GfxSceneView& sceneView, RenderContext& context)
	{
		//https://aras-p.info/blog/2014/01/16/rough-sorting-by-depth/
		auto FloatToBits = [](float value, uint32_t bitsToTake) -> uint32_t
		{
			union { float f; uint32_t i; } f2i;
			f2i.f = value;
			return f2i.i >> (32 - bitsToTake);
		};

		const GfxSceneViewDescriptor& sceneViewDescriptor = sceneView.GetDescriptor();
		const GfxCameraProxy* const& camera = sceneViewDescriptor.camera;
		const glm::vec3 cameraWorldPosition = camera->GetWorldPosition();

		GfxRenderList renderList = {};
		const FramePacket& framePacket = context.GetFramePacket();
		const bool isShadowView = sceneViewDescriptor.type & GfxSceneViewType_SHADOWS;

		for (auto& dynamicObject : framePacket.dynamicObjects)
		{
			const GfxMeshRendererProxy& proxy = dynamicObject.GetMeshRendererProxy();
			const RenderFlags renderFlags = proxy.GetRenderFlags();

			if (isShadowView && !(renderFlags & RenderFlags_CAST_SHADOWS))
				continue;

			if (sceneView.GetFrustum().ContainsOBB(proxy.GetOBB(), isShadowView))
			{
				const float cameraDistance = glm::distance2(dynamicObject.GetWorldPosition(), cameraWorldPosition);
				const uint32_t cameraDistanceBits = FloatToBits(cameraDistance, 24);

				const std::vector<GfxMesh*> meshes = proxy.GetMeshes();
				const std::vector<GfxMaterialProxy*>& materials = proxy.GetMaterials();

				for (size_t i = 0; i < meshes.size(); ++i)
				{
					const GfxMesh* mesh = meshes[i];
					const GfxMaterialProxy* material = materials[i];

					if (mesh && material)
					{
						const GfxRenderType renderType = material->GetRenderType();
						const GfxVertexLayout vertexLayout = mesh->GetVertexLayout();
						
						const GfxRenderTypeFlags renderTypeBits = static_cast<GfxRenderTypeFlags>(renderType);
						const uint64_t vertexLayoutBits = static_cast<uint64_t>(vertexLayout);

						const uint64_t modelId = mesh->GetModelId();
						const uint64_t materialId = material->GetId();

						GfxDrawCall drawCall;
						drawCall.meshIndex = i;
						drawCall.renderable = &dynamicObject;		
						drawCall.hash |= modelId			<< 48;	//16 bit ModelId
						drawCall.hash |= vertexLayoutBits	<< 40;	//8 bits VertexLayout
						drawCall.hash |= materialId			<< 24;	//16 bit MaterialId
						drawCall.hash |= cameraDistanceBits;		//24 bit CameraDistance

						if (renderTypeBits & GfxRenderTypeFlags_OPAQUE)
							renderList.opaque.push_back(drawCall);
						else if (renderTypeBits & GfxRenderTypeFlags_ALPHA_TEST)
							renderList.alphaTest.push_back(drawCall);
						else if (renderTypeBits & GfxRenderTypeFlags_TRANSPARENT)
							renderList.transparent.push_back(drawCall);
					}
				}
			}
		}

		std::sort(renderList.opaque.begin(), renderList.opaque.end(), [](const GfxDrawCall& lhs, const GfxDrawCall& rhs) { return lhs.hash < rhs.hash; });
		std::sort(renderList.alphaTest.begin(), renderList.alphaTest.end(), [](const GfxDrawCall& lhs, const GfxDrawCall& rhs) { return lhs.hash < rhs.hash; });
		std::sort(renderList.transparent.begin(), renderList.transparent.end(), [](const GfxDrawCall& lhs, const GfxDrawCall& rhs) { return lhs.hash > rhs.hash; });
		sceneView.SetRenderList(renderList);
	}

	void GfxRenderPipeline::CompactRenderLists(GfxSceneView& sceneView)
	{
		auto Compact = [](const GfxRenderBatchDescriptor& descriptor, GfxRenderTypeFlags renderType, const std::vector<GfxDrawCall>& drawCalls, std::vector<GfxRenderBatch>& renderBatch, uint32_t dataOffset, uint32_t* instanceIndexData)
		{
			auto SetInstanceIndex = [&](const GfxDrawCall& drawCall, size_t drawCallIndex)
			{ *(instanceIndexData + dataOffset + static_cast<uint32_t>(drawCallIndex)) = drawCall.renderable->GetIndex(); };

			if (drawCalls.empty())
				return;

			uint32_t instanceCount = 0;
			const bool isMaterialRelevant = descriptor.materialRelevance & renderType;
			const size_t hashMask = isMaterialRelevant ? 0xFFFFFFFFFF000000 : 0xFFFFFF0000000000;

			SetInstanceIndex(drawCalls[0], 0);
			for (size_t i = 1; i < drawCalls.size(); ++i)
			{
				const GfxDrawCall& currentDrawCall = drawCalls[i];
				const GfxDrawCall& previousDrawCall = drawCalls[i - 1];

				const size_t currentDrawCallHash = currentDrawCall.hash & hashMask;
				const size_t previousDrawCallHash = previousDrawCall.hash & hashMask;

				++instanceCount;
				SetInstanceIndex(currentDrawCall, i);

				if (currentDrawCallHash != previousDrawCallHash)
				{
					GfxRenderBatch& batch = renderBatch.emplace_back();
					batch.drawCall.hash = previousDrawCallHash;
					batch.drawCall.meshIndex = previousDrawCall.meshIndex;
					batch.drawCall.renderable = previousDrawCall.renderable;

					batch.instanceCount = instanceCount;
					batch.instanceDataOffset = dataOffset;

					dataOffset += instanceCount;
					instanceCount = 0;
				}
			}

			if (instanceCount)
			{
				GfxRenderBatch& batch = renderBatch.emplace_back();
				batch.drawCall = drawCalls.back();
				batch.instanceCount = instanceCount;
				batch.instanceDataOffset = dataOffset;
			}
		};

		for (const auto& descriptor : m_RenderBatchDescriptors)
		{
			const GfxSceneViewDescriptor& sceneViewDescriptor = sceneView.GetDescriptor();
			if (!(descriptor.sceneViewType & sceneViewDescriptor.type))
				continue;

			uint32_t dataOffset = 0;
			GfxRenderBatches renderBatches = {};
			const GfxRenderList& renderList = sceneView.GetRenderList();

			const size_t neededMemory = sceneView.GetTotalBatchedObjects() * sizeof(uint32_t);
			GpuRingBufferAllocation indexAllocation = m_DynamicFrameMemory->Allocate(neededMemory);
			uint32_t* instanceIndexData = reinterpret_cast<uint32_t*>(indexAllocation.mappedDataPtr);

			if (descriptor.renderTypeRelevance & GfxRenderTypeFlags_OPAQUE)
			{
				Compact(descriptor, GfxRenderTypeFlags_OPAQUE, renderList.opaque, renderBatches.opaque, dataOffset, instanceIndexData);
				dataOffset += static_cast<uint32_t>(renderList.opaque.size());
			}
			
			if (descriptor.renderTypeRelevance & GfxRenderTypeFlags_ALPHA_TEST)
			{
				Compact(descriptor, GfxRenderTypeFlags_ALPHA_TEST, renderList.alphaTest, renderBatches.alphaTest, dataOffset, instanceIndexData);
				dataOffset += static_cast<uint32_t>(renderList.alphaTest.size());
			}

			if (descriptor.renderTypeRelevance & GfxRenderTypeFlags_TRANSPARENT)
				Compact(descriptor, GfxRenderTypeFlags_TRANSPARENT, renderList.transparent, renderBatches.transparent, dataOffset, instanceIndexData);

			sceneView.SetRenderBatches(descriptor, renderBatches);
			sceneView.SetInstanceIndexList(descriptor, indexAllocation);
		}
	}

	void GfxRenderPipeline::BuildSceneViewData(GfxSceneView& sceneView)
	{
		GfxDynamicConstantBuffer*& sceneViewData = m_SceneViewData.FindOrAdd(sceneView.GetDescriptor());
		if (!sceneViewData)
		{
			GfxSwapchain& gfxSwapchain = GfxSwapchain::GetMainSwapchain();
			sceneViewData = new GfxDynamicConstantBuffer(sizeof(Shaders::SceneViewData), gfxSwapchain.GetBackBufferCount());
		}

		const GfxCameraProxy* sceneCameraProxy = sceneView.GetDescriptor().camera;

		Shaders::SceneViewData viewData = {};
		viewData.viewProjection = sceneCameraProxy->GetViewProjectionMatrix();
		viewData.worldPos = sceneCameraProxy->GetWorldPosition();

		memcpy(sceneViewData->GetFrameData(), &viewData, sizeof(Shaders::SceneViewData));
		sceneView.SetConstantBuffer(sceneViewData);
	}

	size_t GfxRenderPipeline::CalculateDynamicObjectsFrameMemoryRequirements(RenderContext& context)
	{
		const FramePacket& framePacket = context.GetFramePacket();

		size_t totalGlobalBatchedObjects = 0;
		for (auto& [descriptor, sceneView] : context.GetSceneViews())
		{
			for (const auto& batchDescriptor : m_RenderBatchDescriptors)
			{
				if (!(batchDescriptor.sceneViewType & sceneView.GetDescriptor().type))
					continue;

				size_t totalBatchedObjects = 0;
				const GfxRenderList& renderList = sceneView.GetRenderList();

				if (batchDescriptor.renderTypeRelevance & GfxRenderTypeFlags_OPAQUE)
					totalBatchedObjects += renderList.opaque.size();

				if (batchDescriptor.renderTypeRelevance & GfxRenderTypeFlags_ALPHA_TEST)
					totalBatchedObjects += renderList.alphaTest.size();

				if (batchDescriptor.renderTypeRelevance & GfxRenderTypeFlags_TRANSPARENT)
					totalBatchedObjects += renderList.transparent.size();

				totalGlobalBatchedObjects += totalBatchedObjects;
				sceneView.SetTotalBatchedObjects(totalBatchedObjects);
			}
		}

		size_t neededMemorySize = sizeof(Shaders::InstanceData) * framePacket.dynamicObjects.size();
		neededMemorySize += sizeof(uint32_t) * totalGlobalBatchedObjects;
		return neededMemorySize;
	}
}
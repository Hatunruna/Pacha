#include "StandardRenderPipeline.h"

#include "graphics/renderpass/DepthPrepass.h"
#include "graphics/renderpass/BaseRenderPass.h"
#include "graphics/renderpass/ShadowRenderPass.h"
#include "graphics/renderpass/PostProcessingRenderPass.h"

#if PACHA_EDITOR
#include "graphics/renderpass/IdRenderPass.h"
#include "graphics/renderpass/IconsRenderPass.h"
#include "graphics/renderpass/GizmosRenderPass.h"
#include "graphics/renderpass/EditorUIRenderPass.h"
#include "graphics/renderpass/SelectionRenderPass.h"
#include "graphics/renderpass/EditorHighlightsRenderPass.h"
#endif

namespace Pacha
{
	StandardRenderPipeline::StandardRenderPipeline()
		: GfxRenderPipeline("StandardRenderPipeline"_ID)
	{
		AddRenderPass(DepthPrepass);
		AddRenderPass(ShadowRenderPass);
		AddRenderPass(BaseRenderPass);
		AddRenderPass(PostProcessingRenderPass);

#if PACHA_EDITOR
		AddRenderPass(IdRenderPass);
		AddRenderPass(SelectionRenderPass);
		AddRenderPass(EditorHighlightsRenderPass);
		AddRenderPass(GizmosRenderPass);
		AddRenderPass(IconsRenderPass);
		AddRenderPass(EditorUiRenderPass);
#endif
	}
}
#pragma once
#include "core/Assert.h"
#include "GfxSceneView.h"
#include "graphics/structures/Shaders.h"
#include "collections/FrameCountedMap.h"
#include "graphics/memory/GpuRingBuffer.h"
#include "utilities/profiling/CpuProfiler.h"
#include "core/graph/context/RenderContext.h"
#include "graphics/utilities/GfxResourceAliasing.h"
#include "graphics/allocators/CommandBufferAllocator.h"
#include "graphics/resources/buffer/GfxConstantBuffer.h"

#include <unordered_map>
#include <marl/event.h>
#include <marl/waitgroup.h>

namespace Pacha
{
	class GfxRenderPipeline
	{
	private:
		class GfxRenderPipelineCallbackHandler
		{
		public:
			GfxRenderPipelineCallbackHandler(GfxRenderPipeline* pipeline)
			{
				pipeline->AddCallbackHandler(this);
			}

			virtual void Clear() = 0;
			virtual bool Contains(const PachaId& id) = 0;
		};

		template<typename... Types>
		class GfxCallbackHandler : public GfxRenderPipelineCallbackHandler
		{
		public:
			using GfxRenderPipelineCallbackHandler::GfxRenderPipelineCallbackHandler;

			void AddCallback(const PachaId& callbackId, const std::function<void(Types...)>& callback)
			{
				m_Callbacks[callbackId].push_back(callback);
			}

			void ReserveCallback(const PachaId& callbackId)
			{
				m_Callbacks[callbackId] = {};
			}

			void CallCallbacks(const PachaId& callbackId, const Types&... values)
			{
				for (auto const& callbacks : m_Callbacks[callbackId])
					callbacks(values...);
			}

			void Clear() override
			{
				m_Callbacks.clear();
			}

			bool Contains(const PachaId& id) override
			{
				return m_Callbacks.count(id);
			}

		private:
			std::unordered_map<PachaId, std::vector<std::function<void(Types...)>>> m_Callbacks;
		};

	public:
		GfxRenderPipeline(const PachaId& id);
		virtual ~GfxRenderPipeline();

		void Setup(RenderContext& context);
		void Render(RenderContext& context, CommandBufferAllocator& commandBufferAllocator);

		size_t GetPassesCount();
		void SetTargetResolution(const glm::uvec2& resolution);
		const glm::uvec2& GetTargetResolution() const;

		template<typename T>
		void AddRenderPass(const PachaId& id)
		{
			PACHA_ASSERT(m_RenderPasses.size() < 63, "Maximum number of render passes reached");
			std::unique_ptr<T> newRenderPass = std::make_unique<T>(id, this);
			m_RenderPassLookup[id] = m_RenderPasses.size();
			m_RenderPasses.push_back(std::move(newRenderPass));
		}

		template<typename... Types>
		void RegisterInputCallback(const PachaId& callbackId)
		{
			PACHA_ASSERT(!InputCallbackExists(callbackId), "Callback with id '" << callbackId.GetName() << "' alredy registered");
			GfxCallbackHandler<Types...>& handler = GetCallbackHandler<Types...>();
			handler.ReserveCallback(callbackId);
		}

		template<typename... Types>
		void CallInputCallback(const PachaId& callbackId, const Types&... values)
		{
			PACHA_ASSERT(InputCallbackExists(callbackId), "No callback with id '" << callbackId.GetName() << "' registered");
			GfxCallbackHandler<Types...>& handler = GetCallbackHandler<Types...>();

			PACHA_ASSERT(handler.Contains(callbackId), "Callback found but function signatures not match");
			handler.CallCallbacks(callbackId, values...);

			m_CallbackSignalEvents[callbackId].signal();
		}

		template<typename... Types>
		void WaitForInputCallback(const PachaId& taskId, const PachaId& callbackId, const std::function<void(Types...)>& callback)
		{
			PACHA_ASSERT(InputCallbackExists(callbackId), "No callback with id '" << callbackId.GetName() << "' registered");
			GfxCallbackHandler<Types...>& handler = GetCallbackHandler<Types...>();

			PACHA_ASSERT(handler.Contains(callbackId), "Callback found but function signatures do not match");
			handler.AddCallback(callbackId, callback);

			if (!m_CallbackSignalEvents.count(callbackId))
				m_CallbackSignalEvents.emplace(callbackId, marl::Event(marl::Event::Mode::Manual));
			m_CallbackWaitEvents[taskId].push_back(callbackId);
		}

		//Resources ----------------------------------------------------------------------------------------------------
		GfxTexture* GetTexture(const PachaId& id);
		GfxBuffer* GetBuffer(const PachaId& id);

		void RequestPersistentTexture(const PachaId& id, const GfxTextureDescriptor& descriptor);
		void RequestTexture(const PachaId& passId, const PachaId& id, const GfxTextureDescriptor& descriptor);

		void RequestPersistentBuffer(const PachaId& id, const GfxBufferDescriptor& descriptor);
		void RequestBuffer(const PachaId& passId, const PachaId& id, const GfxBufferDescriptor& descriptor);

		void ReadWrite(const PachaId& passId, const PachaId& id);
		void AliasTexture(const PachaId& originalId, const PachaId& newId);
		void AliasBuffer(const PachaId& originalId, const PachaId& newId);

		class GfxRenderPass* GetRenderPass(const PachaId& passId);
		std::vector<GfxCommandBuffer*> GetSubmissionOrderedCommandBuffers();
		const std::vector<class GfxRenderPass*>& GetExecutableRenderPasses();

		PachaId GetId() const;

#if !PACHA_SHIPPING
		const CpuProfiler& GetSetupCpuProfiler() const;
		const CpuProfiler& GetRenderCpuProfiler() const;
#endif

	private:
		std::vector<class GfxRenderPass*> m_ExecutableRenderPasses;
		std::vector<std::unique_ptr<class GfxRenderPass>> m_RenderPasses;
		std::vector<GfxRenderPipelineCallbackHandler*> m_CallbackHandlers;

		std::unordered_map<PachaId, GfxBuffer*> m_ManagedBuffers;
		std::unordered_map<PachaId, GfxBuffer*> m_RegisteredBuffers;
		std::unordered_map<PachaId, PachaId> m_BufferAliases;

		std::unordered_map<PachaId, GfxTexture*> m_ManagedTextures;
		std::unordered_map<PachaId, GfxTexture*> m_RegisteredTextures;
		std::unordered_map<PachaId, PachaId> m_TextureAliases;

		GfxResourceAliasing m_ResourceAliaser;
		std::vector<GfxAliasedResourceGroup> m_AliasedResourceGroups;
		std::unordered_map<PachaId, GfxResourceRequest> m_ResourceRequests;

		std::unordered_map<PachaId, uint64_t> m_RenderPassLookup;
		std::unordered_map<class GfxRenderPass*, GfxCommandBuffer*> m_CommandBufferLookup;

		std::unordered_map<PachaId, marl::Event> m_CallbackSignalEvents;
		std::unordered_map<PachaId, std::vector<PachaId>> m_CallbackWaitEvents;

		GpuRingBuffer* m_DynamicFrameMemory = nullptr;
		GpuRingBufferAllocation m_DynamicObjectsAllocation = {};
		
		Shaders::GlobalState m_GlobalState = {};
		GfxDynamicConstantBuffer* m_GlobalStateBuffer = nullptr;
		FrameCountedMap<GfxSceneViewDescriptor, GfxDynamicConstantBuffer*, 3, true> m_SceneViewData;

		std::unordered_set<GfxRenderBatchDescriptor> m_RenderBatchDescriptors;

		PachaId m_Id;
		glm::uvec2 m_TargetResolution = { 1, 1 };
		
#if !PACHA_SHIPPING
		CpuProfiler m_SetupCpuProfiler;
		CpuProfiler m_RenderCpuProfiler;
#endif

		void AddCallbackHandler(GfxRenderPipelineCallbackHandler* handler);
		bool InputCallbackExists(const PachaId& id);

		void UpdateGlobalState(RenderContext& context);
		void CopyPerInstanceData(RenderContext& context);
		
		void GatherSceneViews(RenderContext& context);
		void ProcessResourceRequest(const GfxAliasingRequest& request);

		virtual void SetupResources();
		void RegisterTexture(const PachaId& id, GfxTexture* texture);
		void RegisterBuffer(const PachaId& id, GfxBuffer* buffer);

		void CullSceneView(GfxSceneView& sceneView, RenderContext& context);
		void CompactRenderLists(GfxSceneView& sceneView);
		void BuildSceneViewData(GfxSceneView& sceneView);
		size_t CalculateDynamicObjectsFrameMemoryRequirements(RenderContext& context);

		template<typename... Types>
		GfxCallbackHandler<Types...>& GetCallbackHandler()
		{
			static GfxCallbackHandler<Types...> s_Handler(this);
			return s_Handler;
		}
	};
}

#define AddRenderPass(type) AddRenderPass<type>(#type##_ID)
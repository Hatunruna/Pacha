#pragma once
#include "utilities/Hash.h"
#include "graphics/proxies/GfxRenderable.h"
#include "graphics/proxies/GfxCameraProxy.h"
#include "graphics/resources/technique/GfxRendererState.h"

namespace Pacha
{
	enum GfxSceneViewTypeBits : uint8_t
	{
		GfxSceneViewType_GAME		= 1 << 0,
		GfxSceneViewType_EDITOR		= 1 << 1,
		GfxSceneViewType_PREVIEW	= 1 << 2,
		GfxSceneViewType_SHADOWS	= 1 << 3
	};
	typedef uint8_t GfxSceneViewType;

	enum GfxRenderTypeFlagBits : uint8_t
	{
		GfxRenderTypeFlags_OPAQUE			= GfxRenderType::OPAQUE,
		GfxRenderTypeFlags_ALPHA_TEST		= GfxRenderType::ALPHA_TEST,
		GfxRenderTypeFlags_TRANSPARENT		= GfxRenderType::TRANSPARENT,

		GfxRenderTypeFlags_ALL_OPAQUE		= GfxRenderTypeFlags_OPAQUE | GfxRenderTypeFlags_ALPHA_TEST,
		GfxRenderTypeFlags_ALL_TRANSPARENT	= GfxRenderTypeFlags_TRANSPARENT,
		GfxRenderTypeFlags_ALL				= GfxRenderTypeFlags_ALL_OPAQUE | GfxRenderTypeFlags_ALL_TRANSPARENT
	};
	typedef uint8_t GfxRenderTypeFlags;

	struct GfxDrawCall
	{
		size_t hash;
		size_t meshIndex = 0;
		const GfxRenderable* renderable = nullptr;
	};

	struct GfxRenderBatchDescriptor
	{
		GfxSceneViewType sceneViewType = GfxSceneViewType_GAME;
		GfxRenderTypeFlags renderTypeRelevance = GfxRenderTypeFlags_ALL;
		GfxRenderTypeFlags materialRelevance = GfxRenderTypeFlags_ALL;

		friend bool operator==(const GfxRenderBatchDescriptor& lhs, const GfxRenderBatchDescriptor& rhs)
		{
			return memcmp(&lhs, &rhs, sizeof(GfxRenderBatchDescriptor)) == 0;
		}
	};

	struct GfxRenderList
	{
		std::vector<GfxDrawCall> opaque;
		std::vector<GfxDrawCall> alphaTest;
		std::vector<GfxDrawCall> transparent;

		size_t TotalDrawCalls()
		{
			return opaque.size() + alphaTest.size() + transparent.size();
		}
	};
}

namespace std
{
	template <>
	struct hash<Pacha::GfxRenderBatchDescriptor>
	{
		size_t operator()(const Pacha::GfxRenderBatchDescriptor& descriptor) const
		{
			size_t hash = Pacha::Hash(descriptor.sceneViewType);
			Pacha::HashCombine(hash, descriptor.materialRelevance);
			Pacha::HashCombine(hash, descriptor.renderTypeRelevance);
			return hash;
		}
	};
}
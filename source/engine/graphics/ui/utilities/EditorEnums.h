#pragma once

#if !PACHA_EDITOR

#define EDITOR_FLAGS(type, ...)					\
typedef uint32_t type##Flags;					\
enum type##FlagBits { __VA_ARGS__ };

#define EDITOR_FLAGS_SIZE(type, sizeType, ...)	\
typedef sizeType type##Flags;					\
enum type##FlagBits : sizeType { __VA_ARGS__ };

#define EDITOR_ENUM(type, ...)					\
enum class type { __VA_ARGS__ };

#define EDITOR_ENUM_SIZE(type, sizeType, ...)	\
enum class type : sizeType { __VA_ARGS__ };

#endif
#pragma once
#if RENDERER_VK
#include "GfxResourceAliasingVK.h"
#endif

namespace Pacha
{
#if RENDERER_VK
	using GfxResourceAliasing = GfxResourceAliasingVK;
#endif
}
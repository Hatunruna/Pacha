#include "GfxResourceAliasingBase.h"

namespace Pacha
{
	void GfxResourceAliasingBase::FreeAliasingGroupMemory(GfxAliasedResourceGroup& group)
	{
		for (auto& [id, texture] : group.textures)
			delete texture;

		for (auto& [id, buffer] : group.buffers)
			delete buffer;

		group.buffers.clear();
		group.textures.clear();
	}
}
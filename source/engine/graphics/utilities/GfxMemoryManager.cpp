#include "GfxMemoryManager.h"
#include "math/Utilities.h"
#include "graphics/utilities/GfxDeleter.h"
#include "graphics/swapchain/GfxSwapchain.h"

namespace Pacha
{
	template<typename T>
	T& ThreadSafeEmplaceBack(std::vector<T>& vector)
	{
		static std::mutex sMutex;
		std::unique_lock<std::mutex> lock(sMutex);
		return vector.emplace_back();
	}

	template<typename T>
	void ThreadSafePushBack(std::vector<T>& vector, T& value)
	{
		static std::mutex sMutex;
		std::unique_lock<std::mutex> lock(sMutex);
		vector.push_back(value);
	}
	
	GfxMemoryManager::GfxMemoryManager()
	{
		GfxSwapchain& gfxSwapchain = GfxSwapchain::GetMainSwapchain();
		const uint32_t backBufferCount = gfxSwapchain.GetBackBufferCount();

		m_WaitGroups.resize(backBufferCount);
		m_ObjectDeleteOperations.resize(backBufferCount);
		m_UploadOperations.resize(backBufferCount);
	}

	void GfxMemoryManager::FreeStagingBuffers()
	{
		for (size_t i = 0; i < m_UploadOperations.size(); ++i)
		{
			UploadOperationsQueue& operations = m_UploadOperations[i];
			for (auto& it : operations.stagingBuffers)
				delete it.buffer;
		}
	}

	void GfxMemoryManager::AddBufferUpload(GfxBuffer* destination, const std::shared_ptr<uint8_t>& sourceData, const GfxResourceState sourceState, const GfxResourceState destinationState)
	{
		uint32_t index = m_CurrentIndex;
		marl::WaitGroup& waitGroup = m_WaitGroups[index];
		waitGroup.add();

		UploadOperationsQueue& operations = m_UploadOperations[index];
		GfxBufferUploadOperation& uploadOperation = operations.bufferUploadOperations.emplace_back();
		IndexOffset indexOffset = GetBufferIndexAndOffset(destination->GetSize(), 1, operations.stagingBuffers);

		uploadOperation.destination = destination;
		uploadOperation.offset = indexOffset.offset;
		uploadOperation.stagingBufferIndex = indexOffset.index;
		uploadOperation.sourceState = sourceState;
		uploadOperation.destinationState = destinationState;

		marl::schedule([&, waitGroup, destination, sourceData]
		{
			unsigned char* stagingData = operations.stagingBuffers[uploadOperation.stagingBufferIndex].buffer->GetData();
			memcpy(stagingData + uploadOperation.offset, sourceData.get(), destination->GetSize());
			waitGroup.done();
		});
	}

	void GfxMemoryManager::AddTextureUpload(GfxTexture* destination, const std::shared_ptr<uint8_t>& sourceData, const GfxResourceState sourceState, const GfxResourceState destinationState)
	{
		size_t alignment = 1;
		const size_t size = destination->GetSize();
		const GfxPixelFormat format = destination->GetFormat();
		
		if (IsBlockCompressedPixelFormat(format))
			alignment = GetCompressedBlockSize(format);
		else
			alignment = GetPixelSize(format);

		uint32_t index = m_CurrentIndex;
		marl::WaitGroup& waitGroup = m_WaitGroups[index];
		waitGroup.add();

		UploadOperationsQueue& operations = m_UploadOperations[index];
		GfxTextureUploadOperation& uploadOperation = operations.textureUploadOperations.emplace_back();
		IndexOffset indexOffset = GetBufferIndexAndOffset(size, alignment, operations.stagingBuffers);

		uploadOperation.destination = destination;
		uploadOperation.offset = indexOffset.offset;
		uploadOperation.stagingBufferIndex = indexOffset.index;
		uploadOperation.sourceState = sourceState;
		uploadOperation.destinationState = destinationState;

		marl::schedule([&, waitGroup, destination, sourceData]
		{
			unsigned char* stagingData = operations.stagingBuffers[uploadOperation.stagingBufferIndex].buffer->GetData();
			memcpy(stagingData + uploadOperation.offset, sourceData.get(), destination->GetSize());
			waitGroup.done();
		});
	}

	GfxCommandBuffer* GfxMemoryManager::GetUploadCommandBuffer()
	{
		uint32_t newIndex = (m_CurrentIndex + 1) % m_WaitGroups.size();
		uint32_t lastIndex = m_CurrentIndex.exchange(newIndex);
		
		m_WaitGroups[lastIndex].wait();
		return GenerateCommandBuffer(lastIndex);
	}

	GfxCommandBuffer* GfxMemoryManager::GenerateCommandBuffer(const uint32_t index)
	{
		UploadOperationsQueue& operations = m_UploadOperations[index];
		if (operations.bufferUploadOperations.size() || operations.textureUploadOperations.size())
		{
			for (auto const& it : operations.stagingBuffers)
				it.buffer->Flush();

			GfxCommandBuffer& commandBuffer = operations.commandBuffer;
			commandBuffer.BeginCommandBuffer();

			for (auto const& it : operations.bufferUploadOperations)
			{
				if(it.sourceState != GfxResourceState::COPY_DESTINATION)
					commandBuffer.TransitionBuffer(it.destination, it.sourceState, GfxResourceState::COPY_DESTINATION);
				commandBuffer.CopyBufferToBuffer(operations.stagingBuffers[it.stagingBufferIndex].buffer, it.destination, it.destination->GetSize(), it.offset);
				commandBuffer.TransitionBuffer(it.destination, GfxResourceState::COPY_DESTINATION, it.destinationState);
			}

			for (auto const& it : operations.textureUploadOperations)
			{
				if (it.sourceState != GfxResourceState::COPY_DESTINATION)
					commandBuffer.TransitionTexture(it.destination, it.sourceState, GfxResourceState::COPY_DESTINATION);
				commandBuffer.CopyBufferToTexture(operations.stagingBuffers[it.stagingBufferIndex].buffer, it.destination, it.offset);
				commandBuffer.TransitionTexture(it.destination, GfxResourceState::COPY_DESTINATION, it.destinationState);
			}

			commandBuffer.EndCommandBuffer();
			operations.bufferUploadOperations.clear();
			operations.textureUploadOperations.clear();
			return &commandBuffer;
		}
		else
		{
			for (auto& it : operations.stagingBuffers)
				delete it.buffer;

			operations.stagingBuffers.clear();
			return nullptr;
		}
	}

	void GfxMemoryManager::AddTextureToDelete(GfxTexture& texture)
	{
		DeleteObjectsQueue& deleteObjects = m_ObjectDeleteOperations[m_CurrentIndex];
		GfxTextureDeleteContiner& container = ThreadSafeEmplaceBack(deleteObjects.textures);
		texture.CopyForDeletion(&container);
	}

	void GfxMemoryManager::AddBufferToDelete(GfxBuffer& buffer)
	{
		DeleteObjectsQueue& deleteObjects = m_ObjectDeleteOperations[m_CurrentIndex];
		GfxBufferDeleteContiner& container = ThreadSafeEmplaceBack(deleteObjects.buffers);
		buffer.CopyForDeletion(&container);
	}

	void GfxMemoryManager::AddRenderTargetToDelete(GfxRenderTarget& renderTarget)
	{
		DeleteObjectsQueue& deleteObjects = m_ObjectDeleteOperations[m_CurrentIndex];
		GfxRenderTargetDeleteContiner& container = ThreadSafeEmplaceBack(deleteObjects.renderTargets);
		renderTarget.CopyForDeletion(&container);
	}

	void GfxMemoryManager::AddAliasedResourceGroupToDelete(GfxAliasedResourceGroup& aliasingGroup)
	{
		DeleteObjectsQueue& deleteObjects = m_ObjectDeleteOperations[m_CurrentIndex];
		ThreadSafePushBack(deleteObjects.aliasingGroups, aliasingGroup);
	}

	void GfxMemoryManager::DeletePendingObjects()
	{
		DeleteObjectsQueue& deleteObjects = m_ObjectDeleteOperations[m_CurrentIndex];
		deleteObjects.DeleteObjects();
		deleteObjects.Clear();
	}

	void GfxMemoryManager::DeleteAllPendingObjects()
	{
		for (size_t i = 0; i < m_ObjectDeleteOperations.size(); ++i)
		{
			DeleteObjectsQueue& deleteObjects = m_ObjectDeleteOperations[i];
			deleteObjects.DeleteObjects();
		}
	}

	GfxMemoryManager::IndexOffset GfxMemoryManager::GetBufferIndexAndOffset(const size_t allocationSize, const size_t alignment, std::vector<TrackedStagingBuffer>& stagingBuffers)
	{
		constexpr size_t kBaseAllocationSize = SizeMB(64);

		IndexOffset indexOffset = {};
		if (stagingBuffers.size() == 0)
		{
			size_t bufferSize = std::max(kBaseAllocationSize, NextPowerOf2(allocationSize));

			TrackedStagingBuffer buffer;
			buffer.buffer = new GfxStagingBuffer(bufferSize);
			buffer.claimedSize = allocationSize;
			stagingBuffers.push_back(buffer);
		}
		else
		{
			bool foundSpace = false;
			for (size_t i = 0; i < stagingBuffers.size(); ++i)
			{
				TrackedStagingBuffer& stagingBuffer = stagingBuffers[i];
				size_t remainingSize = stagingBuffer.buffer->GetSize() - stagingBuffer.claimedSize;
				if (remainingSize >= allocationSize + (alignment - 1))
				{
					const size_t alignedOffset = RoundToNextMultiple(stagingBuffer.claimedSize, alignment);

					foundSpace = true;
					indexOffset.offset = alignedOffset;
					indexOffset.index = static_cast<uint32_t>(i);
					stagingBuffer.claimedSize += (allocationSize + (alignedOffset - stagingBuffer.claimedSize));
					break;
				}
			}

			if (!foundSpace)
			{
				size_t bufferSize = std::max(kBaseAllocationSize, NextPowerOf2(allocationSize));

				TrackedStagingBuffer buffer;
				buffer.buffer = new GfxStagingBuffer(bufferSize);
				buffer.claimedSize = allocationSize;
				indexOffset.index = static_cast<uint32_t>(stagingBuffers.size());

				stagingBuffers.push_back(buffer);
			}
		}

		return indexOffset;
	}
	
	void GfxMemoryManager::DeleteObjectsQueue::DeleteObjects()
	{
		GfxDeleter deleter;
		for (auto& buffer : buffers)
			deleter.Delete(buffer);

		for (auto& texture : textures)
			deleter.Delete(texture);

		for (auto& renderTarget : renderTargets)
			deleter.Delete(renderTarget);

		for (auto& aliasingGroup : aliasingGroups)
			deleter.Delete(aliasingGroup);
	}
	
	void GfxMemoryManager::DeleteObjectsQueue::Clear()
	{
		buffers.clear();
		textures.clear();
		renderTargets.clear();
		aliasingGroups.clear();
	}
}

#pragma once
#if RENDERER_VK
#include "graphics/utilities/GfxDeleterVK.h"
#endif

namespace Pacha
{
#if RENDERER_VK
	using GfxDeleter = GfxDeleterVK;
#endif
}
#pragma once
#if RENDERER_VK
#include "GfxAliasedResourceGroupVK.h"
#endif

namespace Pacha
{
#if RENDERER_VK
	using GfxAliasedResourceGroup = GfxAliasedResourceGroupVK;
#endif
}
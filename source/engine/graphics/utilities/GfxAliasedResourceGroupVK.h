#pragma once
#include "GfxAliasedResourceGroupBase.h"

namespace Pacha
{
	struct GfxAliasedResourceGroupVK : public GfxAliasedResourceGroupBase
	{
		VmaAllocation allocation = VK_NULL_HANDLE;
		std::unordered_map<PachaId, VkImage> vkTextures;
		std::unordered_map<PachaId, VkBuffer> vkBuffers;

		VmaAllocation msAllocation = VK_NULL_HANDLE;
		std::unordered_map<PachaId, VkImage> msVkTextures;
	};
}
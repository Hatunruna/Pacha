#include "GfxResourceAliasingVK.h"
#include "graphics/device/GfxDevice.h"
#include "graphics/utilities/GfxMemoryManager.h"

namespace Pacha
{
	bool GfxResourceAliasingVK::AliasResources(const GfxAliasingGroupDescriptor& outGroupDescriptor, GfxAliasedResourceGroup& outGroup)
	{
		GfxDevice& gfxDevice = GfxDevice::GetInstance();
		const VmaAllocator& allocator = gfxDevice.GetMemoryAllocator();
		const VkDevice& device = gfxDevice.GetVkDevice();

		std::vector<GfxAliasingRequest> msaaRequests;
		std::vector<VkMemoryRequirements> memoryRequirements;

		VkResult result = VK_SUCCESS;
		VkMemoryRequirements requirements = {};
		for (auto const& aliasingRequest : outGroupDescriptor.members)
		{
			if (aliasingRequest.request.type == GfxResourceRequestType::TEXTURE)
			{
				const GfxTextureDescriptor& descriptor = aliasingRequest.request.descriptor.texture;

				VkImageUsageFlags usageFlags = GfxTextureVK::GetVkUsageFlags(descriptor.flags);
				bool autoResolveMSAA = descriptor.msaa > GfxMSAA::X1 && descriptor.flags & GfxTextureFlags_AUTO_RESOLVE_MSAA;

				VkImageCreateInfo imageCreateInfo = {};
				imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
				imageCreateInfo.imageType = GfxTextureVK::GetVkImageType(descriptor.type);
				imageCreateInfo.extent.width = descriptor.width;
				imageCreateInfo.extent.height = descriptor.height;
				imageCreateInfo.extent.depth = descriptor.depth;
				imageCreateInfo.mipLevels = descriptor.mips;
				imageCreateInfo.arrayLayers = descriptor.slices;
				imageCreateInfo.format = GfxTextureVK::GetVkFormat(descriptor.format);
				imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
				imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
				imageCreateInfo.usage = usageFlags;
				imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
				imageCreateInfo.samples = autoResolveMSAA ? VK_SAMPLE_COUNT_1_BIT : GfxTextureVK::GetVkMSAA(descriptor.msaa);
				imageCreateInfo.flags = GfxTextureVK::GetVkImageCreateFlags(descriptor.type);

				VkImage image = VK_NULL_HANDLE;
				if ((result = vkCreateImage(gfxDevice.GetVkDevice(), &imageCreateInfo, nullptr, &image)) != VK_SUCCESS)
					LOGCRITICAL("Failed to create aliased image: " << GfxDeviceVK::GetErrorString(result));

				outGroup.vkTextures[aliasingRequest.id] = image;
				outGroup.msVkTextures[aliasingRequest.id] = VK_NULL_HANDLE;

				vkGetImageMemoryRequirements(device, image, &requirements);
				memoryRequirements.push_back(requirements);

				if (autoResolveMSAA)
					msaaRequests.push_back(aliasingRequest);
			}
			else if (aliasingRequest.request.type == GfxResourceRequestType::BUFFER)
			{
				const GfxBufferDescriptor& descriptor = aliasingRequest.request.descriptor.buffer;

				VkBufferCreateInfo bufferCreateInfo = {};
				bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
				bufferCreateInfo.size = descriptor.size;
				bufferCreateInfo.usage = GfxBufferVK::GetUsageFlags(descriptor.flags);
				bufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
				bufferCreateInfo.queueFamilyIndexCount = 0;
				bufferCreateInfo.pQueueFamilyIndices = nullptr;

				VkBuffer buffer = VK_NULL_HANDLE;
				if ((result = vkCreateBuffer(device, &bufferCreateInfo, nullptr, &buffer)) != VK_SUCCESS)
					LOGCRITICAL("Failed to create aliased buffer: " << GfxDeviceVK::GetErrorString(result));

				vkGetBufferMemoryRequirements(device, buffer, &requirements);
				memoryRequirements.push_back(requirements);

				outGroup.vkBuffers[aliasingRequest.id] = buffer;
			}
		}

		if (!ProcessMSAARequests(msaaRequests, outGroup))
			return false;

		VkMemoryRequirements finalMemReq = {};
		finalMemReq.memoryTypeBits = ~0u;

		for (auto const& requirement : memoryRequirements)
		{
			finalMemReq.size = std::max(finalMemReq.size, requirement.size);
			finalMemReq.alignment = std::max(finalMemReq.alignment, requirement.alignment);
			finalMemReq.memoryTypeBits &= requirement.memoryTypeBits;
		}

		if (finalMemReq.memoryTypeBits != 0)
		{
			VmaAllocationCreateInfo allocCreateInfo = {};
			allocCreateInfo.flags = VMA_ALLOCATION_CREATE_CAN_ALIAS_BIT;
			allocCreateInfo.preferredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

			if ((result = vmaAllocateMemory(allocator, &finalMemReq, &allocCreateInfo, &outGroup.allocation, nullptr)) != VK_SUCCESS)
				LOGCRITICAL("Failed to allocate memory for aliasing group: " << GfxDeviceVK::GetErrorString(result));

			for (auto& [id, image] : outGroup.vkTextures)
			{
				if ((result = vmaBindImageMemory2(allocator, outGroup.allocation, outGroupDescriptor.GetMemberOffset(id), image, nullptr)) != VK_SUCCESS)
					LOGCRITICAL("Failed to bind aliased memory to image: " << GfxDeviceVK::GetErrorString(result));

				GfxTexture* texture = new GfxTexture();
				texture->SetFromNativeHandle(image, outGroup.msVkTextures[id], outGroupDescriptor.GetMemberTextureDescriptor(id));
				texture->SetName(id.GetName());
				outGroup.textures[id] = texture;
			}

			for (auto& [id, buffer] : outGroup.vkBuffers)
			{
				if ((result = vmaBindBufferMemory2(allocator, outGroup.allocation, outGroupDescriptor.GetMemberOffset(id), buffer, nullptr)) != VK_SUCCESS)
					LOGCRITICAL("Failed to bind aliased memory to image: " << GfxDeviceVK::GetErrorString(result));

				GfxBuffer* pBuffer = new GfxBuffer();
				pBuffer->SetFromNativeHandle(buffer, outGroupDescriptor.GetMemberBufferDescriptor(id));
				pBuffer->SetName(id.GetName());
				outGroup.buffers[id] = pBuffer;
			}

			return true;
		}
		else
		{
			for (auto& [id, image] : outGroup.vkTextures)
				vkDestroyImage(device, image, nullptr);

			for (auto& [id, buffer] : outGroup.vkBuffers)
				vkDestroyBuffer(device, buffer, nullptr);

			return false;
		}
	}

	void GfxResourceAliasingVK::FreeAliasingGroupMemory(GfxAliasedResourceGroup& group)
	{
		GfxResourceAliasingBase::FreeAliasingGroupMemory(group);
		GfxMemoryManager::GetInstance().AddAliasedResourceGroupToDelete(group);
	}

	bool GfxResourceAliasingVK::ProcessMSAARequests(std::vector<GfxAliasingRequest>& requests, GfxAliasedResourceGroup& group)
	{
		if (requests.empty())
			return true;

		const VkImageUsageFlags kAttchmentMask = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
		
		GfxDevice& gfxDevice = GfxDevice::GetInstance();
		const VmaAllocator& allocator = gfxDevice.GetMemoryAllocator();
		const VkDevice& device = gfxDevice.GetVkDevice();

		VkResult result = VK_SUCCESS;
		VkMemoryRequirements requirements = {};
		std::vector<VkMemoryRequirements> memoryRequirements;

		for (auto& request : requests)
		{
			const GfxTextureDescriptor& descriptor = request.request.descriptor.texture;

			VkImageCreateInfo imageCreateInfo = {};
			imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
			imageCreateInfo.imageType = GfxTextureVK::GetVkImageType(descriptor.type);
			imageCreateInfo.extent.width = descriptor.width;
			imageCreateInfo.extent.height = descriptor.height;
			imageCreateInfo.extent.depth = descriptor.depth;
			imageCreateInfo.mipLevels = descriptor.mips;
			imageCreateInfo.arrayLayers = descriptor.slices;
			imageCreateInfo.format = GfxTextureVK::GetVkFormat(descriptor.format);
			imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
			imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			imageCreateInfo.usage = VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | (GfxTextureVK::GetVkUsageFlags(descriptor.flags) & kAttchmentMask);
			imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
			imageCreateInfo.samples = GfxTextureVK::GetVkMSAA(descriptor.msaa);
			imageCreateInfo.flags = GfxTextureVK::GetVkImageCreateFlags(descriptor.type);

			VkImage image = VK_NULL_HANDLE;
			if ((result = vkCreateImage(gfxDevice.GetVkDevice(), &imageCreateInfo, nullptr, &image)) != VK_SUCCESS)
				LOGCRITICAL("Failed to create aliased image: " << GfxDeviceVK::GetErrorString(result));

			group.msVkTextures[request.id] = image;
			vkGetImageMemoryRequirements(device, image, &requirements);
			memoryRequirements.push_back(requirements);
			request.request.size = requirements.size;
		}

		std::sort(requests.begin(), requests.end(), [](const GfxAliasingRequest& lhs, const GfxAliasingRequest& rhs)
		{ return lhs.request.size > rhs.request.size; });

		size_t memBlockSize = requests[0].request.size;
		for (size_t i = 0; i < requests.size(); ++i)
		{
			GfxAliasingRequest& request = requests[i];
			for (int64_t j = static_cast<int64_t>(i) - 1; j >= 0; --j)
			{
				GfxAliasingRequest& otherRequest = requests[j];
				if (request.request.accessMask & otherRequest.request.accessMask)
				{
					size_t offset = otherRequest.offset + otherRequest.request.size;
					size_t neededSize = offset + request.request.size;
					request.offset = offset;

					if (neededSize > memBlockSize)
						memBlockSize = neededSize;

					break;
				}
			}
		}

		VkMemoryRequirements finalMemReq = {};
		finalMemReq.size = memBlockSize;
		finalMemReq.memoryTypeBits = ~0u;

		for (auto const& requirement : memoryRequirements)
		{
			finalMemReq.alignment = std::max(finalMemReq.alignment, requirement.alignment);
			finalMemReq.memoryTypeBits &= requirement.memoryTypeBits;
		}

		if (finalMemReq.memoryTypeBits != 0)
		{
			VmaAllocationCreateInfo allocCreateInfo = {};
			allocCreateInfo.flags = VMA_ALLOCATION_CREATE_CAN_ALIAS_BIT;
			allocCreateInfo.preferredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
#if PLATFORM_ANDROID
			allocCreateInfo.usage = VMA_MEMORY_USAGE_GPU_LAZILY_ALLOCATED;
			allocCreateInfo.requiredFlags = VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT;
#endif
			if ((result = vmaAllocateMemory(allocator, &finalMemReq, &allocCreateInfo, &group.msAllocation, nullptr)) != VK_SUCCESS)
				LOGCRITICAL("Failed to allocate MS memory for aliasing group: " << GfxDeviceVK::GetErrorString(result));
		}

		for (auto& [id, image] : group.msVkTextures)
		{
			size_t offset = 0;
			for (auto& request : requests)
			{
				if (request.id == id)
					offset = request.offset;
			}

			if ((result = vmaBindImageMemory2(allocator, group.msAllocation, offset, image, nullptr)) != VK_SUCCESS)
				LOGCRITICAL("Failed to bind aliased memory to image: " << GfxDeviceVK::GetErrorString(result));
		}

		return false;
	}
}

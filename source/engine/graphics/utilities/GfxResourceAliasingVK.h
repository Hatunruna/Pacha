#pragma once
#include "GfxResourceAliasingBase.h"

namespace Pacha
{
	class GfxResourceAliasingVK : public GfxResourceAliasingBase
	{
	public:
		bool AliasResources(const GfxAliasingGroupDescriptor& requests, GfxAliasedResourceGroup& outGroup) override;
		void FreeAliasingGroupMemory(GfxAliasedResourceGroup& group) override;
	
	private:
		bool ProcessMSAARequests(std::vector<GfxAliasingRequest>& requests, GfxAliasedResourceGroup& group);
	};
}
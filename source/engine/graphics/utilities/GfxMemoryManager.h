#pragma once
#include "graphics/resources/buffer/GfxBuffer.h"
#include "graphics/resources/texture/GfxTexture.h"
#include "graphics/utilities/GfxResourceAliasing.h"
#include "graphics/resources/buffer/GfxStagingBuffer.h"
#include "graphics/resources/commandbuffer/GfxCommandBuffer.h"

#include <marl/waitgroup.h>

namespace Pacha
{
	struct GfxBufferUploadOperation
	{
		size_t offset = 0;
		uint32_t stagingBufferIndex = 0;
		GfxBuffer* destination = nullptr;

		GfxResourceState sourceState;
		GfxResourceState destinationState;
	};

	struct GfxTextureUploadOperation
	{
		size_t offset = 0;
		uint32_t stagingBufferIndex = 0;
		GfxTexture* destination = nullptr;

		GfxResourceState sourceState;
		GfxResourceState destinationState;
	};

	class GfxMemoryManager
	{
	private:
		GfxMemoryManager();
		GfxMemoryManager(const GfxMemoryManager&) = delete;
		GfxMemoryManager& operator=(const GfxMemoryManager&) = delete;

	public:
		static GfxMemoryManager& GetInstance()
		{
			static GfxMemoryManager sInstance;
			return sInstance;
		}

		void FreeStagingBuffers();
		
		//Upload Operations
		void AddBufferUpload(GfxBuffer* destination, const std::shared_ptr<uint8_t>& sourceData, const GfxResourceState sourceState, const GfxResourceState destinationState);
		void AddTextureUpload(GfxTexture* destination, const std::shared_ptr<uint8_t>& sourceData, const GfxResourceState sourceState, const GfxResourceState destinationState);
		GfxCommandBuffer* GetUploadCommandBuffer();

		//Delete Operations
		void AddTextureToDelete(GfxTexture& texture);
		void AddBufferToDelete(GfxBuffer& buffer);
		void AddRenderTargetToDelete(GfxRenderTarget& renderTarget);
		void AddAliasedResourceGroupToDelete(GfxAliasedResourceGroup& aliasingGroup);
		void DeletePendingObjects();
		void DeleteAllPendingObjects();

	private:
		struct IndexOffset
		{
			uint32_t index;
			size_t offset;
		};

		struct TrackedStagingBuffer
		{
			size_t claimedSize = 0;
			GfxStagingBuffer* buffer = nullptr;
		};

		struct UploadOperationsQueue
		{
			GfxCommandBuffer commandBuffer;
			std::vector<TrackedStagingBuffer> stagingBuffers;
			std::vector<GfxBufferUploadOperation> bufferUploadOperations;
			std::vector<GfxTextureUploadOperation> textureUploadOperations;
		};

		struct DeleteObjectsQueue
		{
			std::vector<GfxBufferDeleteContiner> buffers;
			std::vector<GfxTextureDeleteContiner> textures;
			std::vector<GfxAliasedResourceGroup> aliasingGroups;
			std::vector<GfxRenderTargetDeleteContiner> renderTargets;

			void DeleteObjects();
			void Clear();
		};
		
		std::atomic_uint32_t m_CurrentIndex = 0;
		std::vector<marl::WaitGroup> m_WaitGroups;
		std::vector<DeleteObjectsQueue> m_ObjectDeleteOperations;
		std::vector<UploadOperationsQueue> m_UploadOperations;

		GfxCommandBuffer* GenerateCommandBuffer(const uint32_t index);
		IndexOffset GetBufferIndexAndOffset(const size_t allocationSize, const size_t alignment, std::vector<TrackedStagingBuffer>& stagingBuffers);
	};
}
#include "GfxBindlessResourceManagerBase.h"

namespace Pacha
{
	uint32_t GfxBindlessResourceManagerBase::AddRawBuffer(GfxBuffer* buffer)
	{
		PACHA_ASSERT(m_RawBufferTracker.count < kMaxBindlessResources, "Maximum amount of buffers exceded!");
		PACHA_ASSERT(buffer->GetDescriptor().flags & GfxBufferFlags_RAW_VIEW, "Trying to add a Non-RawView buffer as a RawView");

		uint32_t index = 0;
		if (!m_RawBufferTracker.freeList.TryFront(index))
			index = m_RawBufferTracker.count.fetch_add(1);

		m_RawBufferTracker.lookUp[buffer] = index;
		return index;
	}

	uint32_t GfxBindlessResourceManagerBase::GetRawBufferBindlessIndex(GfxBuffer* buffer)
	{
		auto value = m_RawBufferTracker.lookUp.find(buffer);
		if (value != m_RawBufferTracker.lookUp.end())
			return value->second;
		return kInvalidBindlessIndex;
	}

	void GfxBindlessResourceManagerBase::FreeRawBuffer(GfxBuffer* buffer)
	{
		if (m_RawBufferTracker.lookUp.count(buffer))
			m_RawBufferTracker.freeList.Push(m_RawBufferTracker.lookUp[buffer]);
	}

	uint32_t GfxBindlessResourceManagerBase::AddTexture2D(GfxTexture* texture)
	{
		PACHA_ASSERT(m_Texture2dTracker.count < kMaxBindlessResources, "Maximum amount of textures exceded!");
		PACHA_ASSERT(texture->GetDescriptor().type == GfxTextureType::TEXTURE_2D, "Trying to add a Non-Texture2D texture as a Texture2D");

		uint32_t index = 0;
		if (!m_Texture2dTracker.freeList.TryFront(index))
			index = m_Texture2dTracker.count.fetch_add(1);

		m_Texture2dTracker.lookUp[texture] = index;
		return index;
	}

	uint32_t GfxBindlessResourceManagerBase::GetTexture2dBindlessIndex(GfxTexture* texture)
	{
		auto value = m_Texture2dTracker.lookUp.find(texture);
		if (value != m_Texture2dTracker.lookUp.end())
			return value->second;
		return kInvalidBindlessIndex;
	}

	void GfxBindlessResourceManagerBase::FreeTexture2D(GfxTexture* texture)
	{
		if (m_Texture2dTracker.lookUp.count(texture))
			m_Texture2dTracker.freeList.Push(m_Texture2dTracker.lookUp[texture]);
	}
}

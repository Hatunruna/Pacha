#include "GfxBindlessResourceManagerVK.h"
#include "graphics/device/GfxDevice.h"

namespace Pacha
{
	bool GfxBindlessResourceManagerVK::Initialize()
	{
		const std::vector<VkDescriptorPoolSize> kPoolSizes =
		{
			{ VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, GfxBindlessResourceManagerBase::kMaxBindlessResources },
			{ VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, GfxBindlessResourceManagerBase::kMaxBindlessResources },
		};

		VkDescriptorPoolCreateInfo poolCreateInfo = {};
		poolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
		poolCreateInfo.flags = VK_DESCRIPTOR_POOL_CREATE_UPDATE_AFTER_BIND_BIT;
		poolCreateInfo.maxSets = 1;
		poolCreateInfo.poolSizeCount = static_cast<uint32_t>(kPoolSizes.size());
		poolCreateInfo.pPoolSizes = kPoolSizes.data();

		VkResult result = VK_SUCCESS;
		GfxDevice& gfxDevice = GfxDevice::GetInstance();
		if ((result = vkCreateDescriptorPool(gfxDevice.GetVkDevice(), &poolCreateInfo, nullptr, &m_DescriptorPool)) != VK_SUCCESS)
		{
			LOGCRITICAL("Failed to create descriptor pool: " << GfxDeviceVK::GetErrorString(result));
			return false;
		}

		VkDescriptorSetLayoutBinding rawBuffersBinding = {};
		rawBuffersBinding.binding = 0;
		rawBuffersBinding.descriptorCount = GfxBindlessResourceManagerBase::kMaxBindlessResources;
		rawBuffersBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
		rawBuffersBinding.pImmutableSamplers = nullptr;
		rawBuffersBinding.stageFlags = VK_SHADER_STAGE_ALL;

		VkDescriptorSetLayoutBinding texture2dBinding = rawBuffersBinding;
		texture2dBinding.binding = 1;
		texture2dBinding.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;

		const std::vector<VkDescriptorSetLayoutBinding> kLayoutBindings =
		{
			rawBuffersBinding,
			texture2dBinding
		};

		const std::vector<VkDescriptorBindingFlags> kBindingFlags(kLayoutBindings.size(), VK_DESCRIPTOR_BINDING_UPDATE_AFTER_BIND_BIT | VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT);
		VkDescriptorSetLayoutBindingFlagsCreateInfo bindingFlagsCreateInfo = {};
		bindingFlagsCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO;
		bindingFlagsCreateInfo.pBindingFlags = kBindingFlags.data();
		bindingFlagsCreateInfo.bindingCount = static_cast<uint32_t>(kBindingFlags.size());

		VkDescriptorSetLayoutCreateInfo setLayoutCreateInfo = {};
		setLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		setLayoutCreateInfo.flags = VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT;
		setLayoutCreateInfo.bindingCount = static_cast<uint32_t>(kLayoutBindings.size());
		setLayoutCreateInfo.pBindings = kLayoutBindings.data();
		setLayoutCreateInfo.pNext = &bindingFlagsCreateInfo;

		if ((result = vkCreateDescriptorSetLayout(gfxDevice.GetVkDevice(), &setLayoutCreateInfo, nullptr, &m_DescriptorSetLayout)) != VK_SUCCESS)
		{
			LOGCRITICAL("Failed to create descriptor set layout: " << GfxDeviceVK::GetErrorString(result));
			return false;
		}

		VkDescriptorSetAllocateInfo setAllocateInfo = {};
		setAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		setAllocateInfo.descriptorSetCount = 1;
		setAllocateInfo.descriptorPool = m_DescriptorPool;
		setAllocateInfo.pSetLayouts = &m_DescriptorSetLayout;

		if ((result = vkAllocateDescriptorSets(gfxDevice.GetVkDevice(), &setAllocateInfo, &m_DescriptorSet)) != VK_SUCCESS)
		{
			LOGCRITICAL("Failed to create descriptor set layout: " << GfxDeviceVK::GetErrorString(result));
			return false;
		}

		return true;
	}

	void GfxBindlessResourceManagerVK::Release()
	{
		GfxDevice& gfxDevice = GfxDevice::GetInstance();
		
		if(m_DescriptorSetLayout != VK_NULL_HANDLE)
			vkDestroyDescriptorSetLayout(gfxDevice.GetVkDevice(), m_DescriptorSetLayout, nullptr);
		
		if (m_DescriptorPool != VK_NULL_HANDLE)
			vkDestroyDescriptorPool(gfxDevice.GetVkDevice(), m_DescriptorPool, nullptr);
	}

	uint32_t GfxBindlessResourceManagerVK::AddRawBuffer(GfxBuffer* buffer)
	{
		uint32_t index = GfxBindlessResourceManagerBase::AddRawBuffer(buffer);
		//Queue descriptor update
		return index;
	}

	uint32_t GfxBindlessResourceManagerVK::AddTexture2D(GfxTexture* texture)
	{
		uint32_t index = GfxBindlessResourceManagerBase::AddTexture2D(texture);
		//Queue descriptor update
		return index;
	}

	VkDescriptorSet GfxBindlessResourceManagerVK::GetDescriptorSet() const
	{
		return m_DescriptorSet;
	}
}

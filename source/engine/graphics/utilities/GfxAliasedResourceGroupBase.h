#pragma once
#include "utilities/PachaId.h"
#include "graphics/resources/buffer/GfxBuffer.h"
#include "graphics/resources/texture/GfxTexture.h"

namespace Pacha
{
	enum GfxResourceRequestType
	{
		TEXTURE,
		BUFFER
	};

	struct GfxResourceRequest
	{
		size_t size = 0;
		uint64_t accessMask = 0;
		GfxResourceRequestType type = GfxResourceRequestType::TEXTURE;

		union Descriptors
		{
			GfxBufferDescriptor buffer;
			GfxTextureDescriptor texture;

			Descriptors()
			{
				texture = {};
			}

		} descriptor;
	};

	struct GfxAliasingRequest
	{
		PachaId id = {};
		size_t offset = 0;
		GfxResourceRequest request = {};
	};

	struct GfxAliasedResourceGroupBase
	{
		std::unordered_map<PachaId, GfxBuffer*> buffers;
		std::unordered_map<PachaId, GfxTexture*> textures;

		size_t ResourcesCount() const
		{
			return buffers.size() + textures.size();
		}

		bool ContainsResource(const PachaId& id) const
		{
			if (textures.count(id))
				return true;

			if (buffers.count(id))
				return true;

			return false;
		}

		bool ContainsAllResources(const std::vector<GfxAliasingRequest>& requests) const
		{
			if (requests.size() != ResourcesCount())
				return false;

			bool result = true;
			for (auto const& request : requests)
				result &= ContainsResource(request.id);
			return result;
		}

		bool ResourceDiffer(const GfxAliasingRequest& aliasingRequest) const
		{
			if (aliasingRequest.request.type == GfxResourceRequestType::TEXTURE)
			{
				if (textures.count(aliasingRequest.id))
					return textures.at(aliasingRequest.id)->GetDescriptor() != aliasingRequest.request.descriptor.texture;
			}
			else if (aliasingRequest.request.type == GfxResourceRequestType::BUFFER)
			{
				if (buffers.count(aliasingRequest.id))
					return buffers.at(aliasingRequest.id)->GetSize() != aliasingRequest.request.descriptor.buffer.size;
			}
			return true;
		}
	};
}
#pragma once
#include "core/Log.h"
#include "graphics/device/GfxDevice.h"
#include "graphics/resources/buffer/GfxBuffer.h"
#include "graphics/resources/texture/GfxTexture.h"
#include "graphics/utilities/GfxResourceAliasing.h"
#include "graphics/resources/rendertarget/GfxRenderTarget.h"

namespace Pacha
{
	class GfxDeleterVK
	{
	public:
		void Delete(GfxBufferDeleteContiner& container)
		{
			GfxDevice& gfxDevice = GfxDevice::GetInstance();
			
			const VkDevice& device = gfxDevice.GetVkDevice();
			const VmaAllocator& allocator = gfxDevice.GetMemoryAllocator();

			vkDestroyBuffer(device, container.buffer, nullptr);
			vmaFreeMemory(allocator, container.allocation);
		}

		void Delete(GfxTextureDeleteContiner& container)
		{
			GfxDevice& gfxDevice = GfxDevice::GetInstance();

			const VkDevice& device = gfxDevice.GetVkDevice();
			const VmaAllocator& allocator = gfxDevice.GetMemoryAllocator();

			for (auto& it : container.views)
				vkDestroyImageView(device, it, nullptr);

			for (auto& it : container.multisampledViews)
				vkDestroyImageView(device, it, nullptr);

			vkDestroyImage(device, container.image, nullptr);
			vkDestroyImage(device, container.multisampledImage, nullptr);

			vmaFreeMemory(allocator, container.allocation);
			vmaFreeMemory(allocator, container.multisampledAllocation);
		}

		void Delete(GfxRenderTargetDeleteContiner& container)
		{
			GfxDevice& gfxDevice = GfxDevice::GetInstance();
			const VkDevice& device = gfxDevice.GetVkDevice();

			vkDestroyRenderPass(device, container.renderpass, nullptr);
			for (auto& frameBuffer : container.framebuffers)
				vkDestroyFramebuffer(device, frameBuffer, nullptr);
		}

		void Delete(GfxAliasedResourceGroup& container)
		{
			GfxDevice& gfxDevice = GfxDevice::GetInstance();
			
			const VkDevice& device = gfxDevice.GetVkDevice();
			const VmaAllocator& allocator = gfxDevice.GetMemoryAllocator();

			for (auto& [id, image] : container.vkTextures)
				vkDestroyImage(device, image, nullptr);

			for (auto& [id, image] : container.msVkTextures)
				vkDestroyImage(device, image, nullptr);

			for (auto& [id, buffer] : container.vkBuffers)
				vkDestroyBuffer(device, buffer, nullptr);

			vmaFreeMemory(allocator, container.allocation);
			vmaFreeMemory(allocator, container.msAllocation);
		}
	};
}
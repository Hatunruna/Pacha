#pragma once
#if RENDERER_VK
#include "GfxBindlessResourceManagerVK.h"
#endif

namespace Pacha
{
#if RENDERER_VK
	using GfxBindlessResourceManager = GfxBindlessResourceManagerVK;
#endif
}
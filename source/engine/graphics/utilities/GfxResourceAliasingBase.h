#pragma once
#include "GfxAliasedResourceGroup.h"

namespace Pacha
{
	enum class GfxAliasingIntersection
	{
		NONE,
		PARTIAL,
		FULL
	};

	struct GfxAliasingGroupDescriptor
	{
		uint64_t groupMask = 0;
		std::vector<GfxAliasingRequest> members;

		void Insert(const GfxAliasingRequest& candidate)
		{
			members.push_back(candidate);
			groupMask |= candidate.request.accessMask;
		}

		GfxAliasingIntersection Intersect(const uint64_t& accessMask)
		{
			uint64_t intersection = groupMask & accessMask;
			if(intersection == groupMask)
				return GfxAliasingIntersection::FULL;
			else if (intersection == 0)
				return GfxAliasingIntersection::NONE;
			else
				return GfxAliasingIntersection::PARTIAL;
		}

		size_t GetMemberOffset(const PachaId& id) const
		{
			for (auto& member : members)
				if (member.id == id)
					return member.offset;
			return 0;
		}

		const GfxTextureDescriptor GetMemberTextureDescriptor(const PachaId& id) const
		{
			for (auto& member : members)
				if (member.id == id)
					return member.request.descriptor.texture;
			return {};
		}

		const GfxBufferDescriptor GetMemberBufferDescriptor(const PachaId& id) const
		{
			for (auto& member : members)
				if (member.id == id)
					return member.request.descriptor.buffer;
			return {};
		}
	};

	class GfxResourceAliasingBase
	{
	public:
		virtual bool AliasResources(const GfxAliasingGroupDescriptor& inGroup, GfxAliasedResourceGroup& outGroup) = 0;
		virtual void FreeAliasingGroupMemory(GfxAliasedResourceGroup& group);
	};
}
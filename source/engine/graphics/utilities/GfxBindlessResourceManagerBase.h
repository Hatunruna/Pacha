#pragma once

#include "collections/SafeQueue.h"
#include "graphics/resources/buffer/GfxBuffer.h"
#include "graphics/resources/texture/GfxTexture.h"

namespace Pacha
{
	class GfxBindlessResourceManagerBase
	{
		template<typename T>
		struct BindlessIndexTracker
		{
			std::atomic_uint32_t count = 0;
			SafeQueue<uint32_t> freeList = {};
			std::unordered_map<T*, uint32_t> lookUp = {};
		};

	public:
		static constexpr uint32_t kMaxBindlessResources = 16536;
		static constexpr uint32_t kInvalidBindlessIndex = static_cast<uint32_t>(-1);

		virtual bool Initialize() = 0;
		virtual void Release() = 0;

		virtual uint32_t AddRawBuffer(GfxBuffer* buffer);
		uint32_t GetRawBufferBindlessIndex(GfxBuffer* buffer);
		void FreeRawBuffer(GfxBuffer* buffer);
		
		virtual uint32_t AddTexture2D(GfxTexture* texture);
		uint32_t GetTexture2dBindlessIndex(GfxTexture* texture);
		void FreeTexture2D(GfxTexture* texture);

	protected:
		GfxBindlessResourceManagerBase() = default;
		~GfxBindlessResourceManagerBase() = default;

		BindlessIndexTracker<GfxBuffer> m_RawBufferTracker;
		BindlessIndexTracker<GfxTexture> m_Texture2dTracker;
	};
}
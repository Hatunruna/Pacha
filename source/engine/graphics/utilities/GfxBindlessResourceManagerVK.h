#pragma once
#include "GfxBindlessResourceManagerBase.h"

namespace Pacha
{
	class GfxBindlessResourceManagerVK : public GfxBindlessResourceManagerBase
	{
	private:
		GfxBindlessResourceManagerVK() = default;
		GfxBindlessResourceManagerVK(const GfxBindlessResourceManagerVK&) = delete;
		GfxBindlessResourceManagerVK& operator=(const GfxBindlessResourceManagerVK&) = delete;

	public:
		static GfxBindlessResourceManagerVK& GetInstance()
		{
			static GfxBindlessResourceManagerVK sInstance;
			return sInstance;
		}

		bool Initialize() override;
		void Release() override;

		uint32_t AddRawBuffer(GfxBuffer* buffer) override;
		uint32_t AddTexture2D(GfxTexture* texture) override;

		VkDescriptorSet GetDescriptorSet() const;
	
	private:
		VkDescriptorSet m_DescriptorSet = {};
		VkDescriptorPool m_DescriptorPool = {};
		VkDescriptorSetLayout m_DescriptorSetLayout = {};
	};
}
#include "GfxRenderPass.h"

namespace Pacha
{
	GfxRenderPass::GfxRenderPass(const PachaId& id, class GfxRenderPipeline* pipeline)
		CPU_PROFILER_CONSTRUCTOR(id)
		GPU_PROFILER_CONSTRUCTOR(id)
	{
		m_PassId = id;
		m_Pipeline = pipeline;
	}

	GfxRenderPass::~GfxRenderPass()
	{ }

	void GfxRenderPass::SetupViewData(RenderContext& context)
	{
		m_RenderTargets.ClearFrameCounted();
		m_SceneViews = context.GetCompatibleSceneViews(m_RenderBatchDescriptor);
	}

	GfxRenderTarget& GfxRenderPass::AllocateRenderTarget(const GfxSceneView* sceneView)
	{
		PACHA_ASSERT(sceneView, "SceneView is NULL");
		return m_RenderTargets.FindOrAdd(sceneView->GetHash());
	}
}
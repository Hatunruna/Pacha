#pragma once
#include "graphics/renderpass/GfxRenderPass.h"
#include "graphics/resources/rendertarget/GfxRenderTarget.h"

namespace Pacha
{
	class ShadowRenderPass : public GfxRenderPass
	{
	public:
		ShadowRenderPass(const PachaId& id, GfxRenderPipeline* parentPipeline);

		void RequestResources(RenderContext& context) override;
		std::vector<GfxSceneViewDescriptor> GetSceneViewDescriptors() override;
		void Render(RenderContext& context, GfxCommandBuffer& commandBuffer) override;
	};
}
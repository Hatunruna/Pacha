#include "ShadowRenderPass.h"

namespace Pacha
{
	ShadowRenderPass::ShadowRenderPass(const PachaId& id, GfxRenderPipeline* parentPipeline)
		: GfxRenderPass(id, parentPipeline)
	{
		m_RenderBatchDescriptor.sceneViewType = GfxSceneViewType_SHADOWS;
		m_RenderBatchDescriptor.materialRelevance = GfxRenderTypeFlags_ALPHA_TEST;
	}

	void ShadowRenderPass::RequestResources(RenderContext&)
	{
	}

	std::vector<GfxSceneViewDescriptor> ShadowRenderPass::GetSceneViewDescriptors()
	{
		return std::vector<GfxSceneViewDescriptor>();
	}

	void ShadowRenderPass::Render(RenderContext&, GfxCommandBuffer&)
	{

	}
}

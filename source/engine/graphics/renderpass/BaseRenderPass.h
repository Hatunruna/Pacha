#pragma once
#include "graphics/renderpass/GfxRenderPass.h"

namespace Pacha
{
	class BaseRenderPass : public GfxRenderPass
	{
	public:
		BaseRenderPass(const PachaId& id, class GfxRenderPipeline* parentPipeline);

		void RequestResources(RenderContext& context) override;
		void Render(RenderContext& context, GfxCommandBuffer& commandBuffer) override;
	};
}
#include "DepthPrepass.h"
#include "graphics/renderpipeline/GfxRenderPipeline.h"

namespace Pacha
{
	DepthPrepass::DepthPrepass(const PachaId& id, GfxRenderPipeline* parentPipeline)
		: GfxRenderPass(id, parentPipeline)
	{
		m_RenderBatchDescriptor.sceneViewType = GfxSceneViewType_GAME | GfxSceneViewType_EDITOR | GfxSceneViewType_PREVIEW;
		m_RenderBatchDescriptor.materialRelevance = GfxRenderTypeFlags_ALPHA_TEST;
	}

	void DepthPrepass::RequestResources(RenderContext&)
	{
		glm::uvec2 resolution = GetTargetResolution();
		for (auto& sceneView : m_SceneViews)
		{
			const GfxSceneViewDescriptor& sceneViewDescriptor = sceneView->GetDescriptor();
			const bool isEditorCamera = sceneViewDescriptor.type & GfxSceneViewType_EDITOR;
			const bool isPreviewCamera = sceneViewDescriptor.type & GfxSceneViewType_PREVIEW;

			if (isPreviewCamera)
				resolution /= 8;

			GfxTextureDescriptor depthDescriptor = {};
			depthDescriptor.width = resolution.x;
			depthDescriptor.height = resolution.y;
			depthDescriptor.flags = GfxTextureFlags_DEFAULT_DEPTH;
			depthDescriptor.format = GfxPixelFormat::D24_UNORM_S8_UINT;

			const PachaId mainDepthBufferId = sceneView->GetResourceId("MainDepthBuffer"_ID);
			RequestTexture(mainDepthBufferId, depthDescriptor);
			ReadWrite(mainDepthBufferId);

			if (isEditorCamera)
				AliasTexture(mainDepthBufferId, "MainDepthBuffer"_ID);
		}
	}

	void DepthPrepass::Render(RenderContext&, GfxCommandBuffer& commandBuffer)
	{
		for (auto sceneView : m_SceneViews)
		{
			GfxRenderTarget& renderTarget = AllocateRenderTarget(sceneView);
			const PachaId mainDepthBufferId = sceneView->GetResourceId("MainDepthBuffer"_ID);

			GfxTexture* depthBuffer = GetTexture(mainDepthBufferId);
			if (renderTarget.DepthStencilAttachmentDiffer(depthBuffer))
			{
				GfxRenderTargetAttachment depthAttachment;
				depthAttachment.texture = depthBuffer;
				depthAttachment.initialState = GfxResourceState::UNDEFINED;
				depthAttachment.finalState = GfxResourceState::DEPTH_WRITE;
				depthAttachment.loadOperation = GfxLoadOperation::CLEAR;
				depthAttachment.storeOperation = GfxStoreOperation::STORE;
				renderTarget.SetDepthStencilAttachment(depthAttachment);
			}

			GfxClearValues clearvalues;
			clearvalues[0].depthStencil = { 1.f, 0 };
			commandBuffer.BeginRenderPass(renderTarget, clearvalues);

			//const GfxRenderBatches& renderBatches = sceneView->GetRenderBatches(m_RenderBatchDescriptor);
			//commandBuffer.DrawRenderBatch(renderBatches.opaque, "PreDepth"_ID);
			//commandBuffer.DrawRenderBatch(renderBatches.alphaTest, "PreDepth"_ID);

			commandBuffer.EndRenderPass();
		}
	}
}
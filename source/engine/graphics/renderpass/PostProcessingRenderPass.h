#pragma once
#include "graphics/renderpass/GfxRenderPass.h"
#include "graphics/resources/rendertarget/GfxRenderTarget.h"

namespace Pacha
{
	class PostProcessingRenderPass : public GfxRenderPass
	{
	public:
		using GfxRenderPass::GfxRenderPass;
		void RequestResources(RenderContext& context) override;
		void Render(RenderContext& context, GfxCommandBuffer& commandBuffer) override;
	};
}
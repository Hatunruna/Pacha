#include "BaseRenderPass.h"

namespace Pacha
{
	BaseRenderPass::BaseRenderPass(const PachaId& id, GfxRenderPipeline* parentPipeline)
		: GfxRenderPass(id, parentPipeline)
	{
		m_RenderBatchDescriptor.sceneViewType = GfxSceneViewType_GAME | GfxSceneViewType_EDITOR | GfxSceneViewType_PREVIEW;
	}

	void BaseRenderPass::RequestResources(RenderContext&)
	{
		glm::uvec2 resolution = GetTargetResolution();
		for (auto& sceneView : m_SceneViews)
		{
			const GfxSceneViewDescriptor& sceneViewDescriptor = sceneView->GetDescriptor();
			const bool isEditorCamera = sceneViewDescriptor.type & GfxSceneViewType_EDITOR;
			const bool isPreviewCamera = sceneViewDescriptor.type & GfxSceneViewType_PREVIEW;

			if (isPreviewCamera)
				resolution /= 8;

			GfxTextureDescriptor colorDescriptor = {};
			colorDescriptor.width = resolution.x;
			colorDescriptor.height = resolution.y;
			colorDescriptor.flags = GfxTextureFlags_DEFAULT_RT;
			colorDescriptor.format = GfxPixelFormat::RGBA16_FLOAT;

			const PachaId mainColorBufferId = sceneView->GetResourceId("MainColorBuffer"_ID);
			const PachaId mainDepthBufferId = sceneView->GetResourceId("MainDepthBuffer"_ID);

			RequestTexture(mainColorBufferId, colorDescriptor);
			ReadWrite(mainColorBufferId);
			ReadWrite(mainDepthBufferId);

			if (isEditorCamera)
				AliasTexture(mainColorBufferId, "MainColorBuffer"_ID);
		}
	}

	void BaseRenderPass::Render(RenderContext&, GfxCommandBuffer& commandBuffer)
	{
		for (auto& sceneView : m_SceneViews)
		{
			GfxRenderTarget& renderTarget = AllocateRenderTarget(sceneView);
			const PachaId mainColorBufferId = sceneView->GetResourceId("MainColorBuffer"_ID);
			const PachaId mainDepthBufferId = sceneView->GetResourceId("MainDepthBuffer"_ID);

			GfxTexture* colorBuffer = GetTexture(mainColorBufferId);
			if (renderTarget.ColorAttachmentDiffer(0, colorBuffer))
			{
				GfxRenderTargetAttachment colorAttachment = {};
				colorAttachment.index = 0;
				colorAttachment.texture = colorBuffer;
				colorAttachment.initialState = GfxResourceState::UNDEFINED;
				colorAttachment.finalState = GfxResourceState::RENDER_TARGET;
				colorAttachment.loadOperation = GfxLoadOperation::CLEAR;
				colorAttachment.storeOperation = GfxStoreOperation::STORE;
				renderTarget.SetColorAttachment(colorAttachment);
			}

			GfxTexture* depthBuffer = GetTexture(mainDepthBufferId);
			if (renderTarget.DepthStencilAttachmentDiffer(depthBuffer))
			{
				GfxRenderTargetAttachment depthAttachment = {};
				depthAttachment.texture = depthBuffer;
				depthAttachment.initialState = GfxResourceState::DEPTH_WRITE;
				depthAttachment.finalState = GfxResourceState::DEPTH_READ;
				depthAttachment.loadOperation = GfxLoadOperation::LOAD;
				depthAttachment.storeOperation = GfxStoreOperation::STORE;
				renderTarget.SetDepthStencilAttachment(depthAttachment);
			}

			GfxClearValues clearvalues;
			clearvalues[0].color.fColor = { 0.1f, 0.1f, 0.1f, 1.f };
			commandBuffer.BeginRenderPass(renderTarget, clearvalues);

			//const GfxRenderBatches& renderBatches = sceneView->GetRenderBatches(m_RenderBatchDescriptor);
			//commandBuffer.DrawRenderBatch(sceneView, m_PassData, renderBatches.opaque, "Base"_ID);
			//commandBuffer.DrawRenderBatch(sceneView, m_PassData, renderBatches.alphaTest, "Base"_ID);
			//commandBuffer.DrawRenderBatch(sceneView, m_PassData, renderBatches.transparent, "Base"_ID);

			commandBuffer.EndRenderPass();
		}
	}
}
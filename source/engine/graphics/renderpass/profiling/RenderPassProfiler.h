#pragma once
#include "utilities/profiling/GpuProfiler.h"

#if PACHA_SHIPPING
#define GPU_PROFILER_CONSTRUCTOR(id)
#define GPU_INHERIT_PROFILER

#define GPU_BEGIN_PROFILING(renderpass, commandbuffer)
#define GPU_END_PROFILING(renderpass, commandbuffer)

#define GPU_PROFILER_BEGIN_CAPTURE(event)
#define GPU_PROFILER_END_CAPTURE(event)
#else
#define GPU_PROFILER_CONSTRUCTOR(id) , RenderPassProfiler(id)
#define GPU_INHERIT_PROFILER , public RenderPassProfiler

#define GPU_BEGIN_PROFILING(renderpass, commandbuffer) renderpass->RenderPassProfiler::BeginProfiling(commandbuffer)
#define GPU_END_PROFILING(renderpass, commandbuffer) renderpass->RenderPassProfiler::EndProfiling(commandbuffer)

#define GPU_PROFILER_BEGIN_CAPTURE(event) m_RenderPassProfiler.BeginCapture(#event##_ID)
#define GPU_PROFILER_END_CAPTURE(event) m_RenderPassProfiler.EndCapture(#event##_ID)
#endif

namespace Pacha
{
	class RenderPassProfiler
	{
	public:
		RenderPassProfiler(const PachaId& id);

		void BeginProfiling(void* payload = nullptr);
		void EndProfiling(void* payload = nullptr);
		GpuProfiler& GetProfiler();

	protected:
		GpuProfiler m_RenderPassProfiler;
	};
}

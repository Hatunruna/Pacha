#include "RenderPassProfiler.h"

namespace Pacha
{
	RenderPassProfiler::RenderPassProfiler(const PachaId& id)
		: m_RenderPassProfiler(id)
	{
	}

	void RenderPassProfiler::BeginProfiling(void* payload)
	{
		m_RenderPassProfiler.BeginProfiler(payload);
	}

	void RenderPassProfiler::EndProfiling(void* payload)
	{
		m_RenderPassProfiler.EndProfiler(payload);
	}

	GpuProfiler& RenderPassProfiler::GetProfiler()
	{
		return m_RenderPassProfiler;
	}
}
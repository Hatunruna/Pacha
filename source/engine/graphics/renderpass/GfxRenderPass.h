#pragma once
#include "core/PlatformInterop.h"
#include "collections/FrameCountedMap.h"
#include "core/graph/context/RenderContext.h"
#include "core/graph/profiling/CpuTaskProfiler.h"
#include "graphics/renderpipeline/GfxSceneView.h"
#include "graphics/renderpipeline/GfxRenderList.h"
#include "graphics/renderpipeline/GfxRenderPipeline.h"
#include "graphics/renderpass/profiling/RenderPassProfiler.h"
#include "graphics/resources/commandbuffer/GfxCommandBuffer.h"

namespace Pacha
{
	class GfxRenderPass CPU_INHERIT_PROFILER GPU_INHERIT_PROFILER
	{
	public:
		GfxRenderPass(const PachaId& id, class GfxRenderPipeline* parentPipeline);
		virtual ~GfxRenderPass();

		virtual void RequestResources(RenderContext& context) = 0;
		virtual void Render(RenderContext& context, GfxCommandBuffer& commandBuffer) = 0;

		void SetupViewData(RenderContext& context);
		GfxRenderTarget& AllocateRenderTarget(const class GfxSceneView* sceneView);

		virtual bool ShouldRun(RenderContext&) { return true; }
		virtual std::vector<GfxSceneViewDescriptor> GetSceneViewDescriptors() { return {}; };

		const PachaId& GetId() const { return m_PassId; }
		const GfxRenderBatchDescriptor& GetRenderBatchDescriptor() const { return m_RenderBatchDescriptor; }

		template<typename... Types>
		inline void RegisterInputCallback(const PachaId& calllbackId)
		{
			m_Pipeline->RegisterInputCallback<std::decay_t<Types>...>(calllbackId);
		}

		template<typename... Types>
		inline void CallInputCallback(const PachaId& calllbackId, const Types&... values)
		{
			m_Pipeline->CallInputCallback<std::decay_t<Types>...>(calllbackId, values...);
		}

		template<typename RenderPass, typename... Types>
		inline void WaitForInputCallback(const PachaId& callbackId, void(RenderPass::* callback)(Types... Args))
		{
			std::function<void(std::decay_t<Types>...)> function = AutomaticPlaceholderExpansionBind(callback);
			m_Pipeline->WaitForInputCallback<std::decay_t<Types>...>(m_PassId, callbackId, function);
		}

		inline GfxTexture* GetTexture(const PachaId& resourceId)
		{
			return m_Pipeline->GetTexture(resourceId);
		}

		inline GfxBuffer* GetBuffer(const PachaId& resourceId)
		{
			return m_Pipeline->GetBuffer(resourceId);
		}

		inline void RequestTexture(const PachaId& resourceId, const GfxTextureDescriptor& descriptor)
		{
			m_Pipeline->RequestTexture(m_PassId, resourceId, descriptor);
		}

		inline void RequestPersistentTexture(const PachaId& resourceId, const GfxTextureDescriptor& descriptor)
		{
			m_Pipeline->RequestPersistentTexture(resourceId, descriptor);
		}

		inline void RequestBuffer(const PachaId& resourceId, const GfxBufferDescriptor& descriptor)
		{
			m_Pipeline->RequestBuffer(m_PassId, resourceId, descriptor);
		}

		inline void RequestPersistentBuffer(const PachaId& resourceId, const GfxBufferDescriptor& descriptor)
		{
			m_Pipeline->RequestPersistentBuffer(resourceId, descriptor);
		}

		inline void ReadWrite(const PachaId& resourceId)
		{
			m_Pipeline->ReadWrite(m_PassId, resourceId);
		}

		inline void AliasTexture(const PachaId& originalId, const PachaId& newId)
		{
			m_Pipeline->AliasTexture(originalId, newId);
		}

		inline void AliasBuffer(const PachaId& originalId, const PachaId& newId)
		{
			m_Pipeline->AliasBuffer(originalId, newId);
		}

		inline const glm::uvec2& GetTargetResolution()
		{
			return m_Pipeline->GetTargetResolution();
		}

	protected:
		GfxRenderBatchDescriptor m_RenderBatchDescriptor = {};
		std::vector<const class GfxSceneView*> m_SceneViews = {};
		FrameCountedMap<uint64_t, GfxRenderTarget, 3> m_RenderTargets = {};

	private:
		PachaId m_PassId;
		GfxRenderPipeline* m_Pipeline = nullptr;

		template<typename RenderPass, typename... Types, int... Index>
		auto AutomaticPlaceholderExpansionBind(void(RenderPass::* callback)(Types... Args), std::integer_sequence<int, Index...>)
		{
			return std::bind(callback, static_cast<RenderPass*>(this), Placeholder<1 + Index>{}...);
		}

		template<typename RenderPass, typename... Types>
		auto AutomaticPlaceholderExpansionBind(void(RenderPass::* callback)(Types... Args))
		{
			return AutomaticPlaceholderExpansionBind(callback, std::make_integer_sequence<int, sizeof...(Types)>{});
		}
	};
}
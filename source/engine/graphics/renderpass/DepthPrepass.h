#pragma once
#include "graphics/renderpass/GfxRenderPass.h"
#include "graphics/resources/rendertarget/GfxRenderTarget.h"

namespace Pacha
{
	class DepthPrepass : public GfxRenderPass
	{
	public:
		DepthPrepass(const PachaId& id, GfxRenderPipeline* parentPipeline);
		void RequestResources(RenderContext& context) override;
		void Render(RenderContext& context, GfxCommandBuffer& commandBuffer) override;
	};
}
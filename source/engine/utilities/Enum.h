#pragma once
#include <cstdint>

namespace Pacha
{
	template<typename T>
	constexpr T InclusiveBitOr(const T shift)
	{
		T value = 0;
		for (T i = 0; i <= shift; ++i)
			value |= static_cast<T>(1) << i;
		return value;
	}

	template<typename T>
	constexpr T ExclusiveBitOr(const T shift)
	{
		T value = 0;
		for (T i = 0; i < shift; ++i)
			value |= static_cast<T>(1) << i;
		return value;
	}

	template<typename T>
	constexpr T InclusiveBitOrRange(const T begin, const T end)
	{
		T value = 0;
		for (T i = begin; i <= end; ++i)
			value |= static_cast<T>(1) << i;
		return value;
	}

	template<typename T>
	constexpr T ExclusiveBitOrRange(const T begin, const T end)
	{
		T value = 0;
		for (T i = begin; i < end; ++i)
			value |= static_cast<T>(1) << i;
		return value;
	}
}
#pragma once
#include "core/Assert.h"
#include "serialization/Serialization.h"

#include <string>
#include <stdint.h>
#include <unordered_map>
#include <unordered_set>

namespace Pacha
{
	class PachaUUID : public SerializableElement
	{
	public:
		PachaUUID() = default;
		PachaUUID(const uint64_t uuid);
		PachaUUID(const PachaUUID& other);
		
		void Initialize();
		uint64_t GetID() const;
		std::string GetString() const;
		bool IsValid() const;

		operator uint64_t() const
		{
			return GetID();
		}

		operator std::string()
		{
			return GetString();
		}

		bool operator==(const PachaUUID& rhs)
		{
			return m_UUID == rhs.m_UUID;
		}

		friend bool operator==(const PachaUUID& lhs, const PachaUUID& rhs)
		{
			return lhs.m_UUID == rhs.m_UUID;
		}

	private:

		uint64_t m_UUID = 0;
		static std::unordered_set<uint64_t> sUsedIds;

		friend class Serialization::Accessor;
		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& serializer)
		{
			if constexpr (IsSaving)
				PACHA_ASSERT(IsValid(), "Trying to save an invalid UUID");

			serializer(m_UUID);

			if constexpr (!IsSaving)
				sUsedIds.emplace(m_UUID);
		}
	};
}

namespace std
{
	template <>
	struct hash<Pacha::PachaUUID>
	{
		size_t operator()(const Pacha::PachaUUID& uuid) const
		{
			return static_cast<size_t>(uuid.GetID());
		}
	};
}
#include "PachaUUID.h"
#include "core/Assert.h"

#include <random>
#include <string>

namespace Pacha
{
	std::unordered_set<uint64_t> PachaUUID::sUsedIds;

	PachaUUID::PachaUUID(const uint64_t uuid)
	{
		PACHA_ASSERT(!sUsedIds.count(uuid), "UUID already present in the UsedMap");

		m_UUID = uuid;
		sUsedIds.emplace(m_UUID);
	}

	PachaUUID::PachaUUID(const PachaUUID& other)
	{
		sUsedIds.erase(m_UUID);
		m_UUID = other.m_UUID;
	}

	void PachaUUID::Initialize()
	{
		static std::random_device sRandomDevice;
		static std::mt19937_64 sEngine(sRandomDevice());
		static std::uniform_int_distribution<uint64_t> sUniformDistribution;

		m_UUID = sUniformDistribution(sEngine);
		
		while (m_UUID == 0 || sUsedIds.count(m_UUID))
			m_UUID = sUniformDistribution(sEngine);

		sUsedIds.emplace(m_UUID);
	}
	
	std::string PachaUUID::GetString() const
	{
		PACHA_ASSERT(IsValid(), "UUID not valid");
		return std::to_string(m_UUID);
	}

	bool PachaUUID::IsValid() const
	{
		return m_UUID != 0;
	}

	uint64_t PachaUUID::GetID() const
	{
		PACHA_ASSERT(IsValid(), "UUID not valid");
		return m_UUID;
	}
}
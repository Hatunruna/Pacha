#pragma once
#include "Hash.h"
#include "core/Assert.h"
#include "serialization/Serialization.h"

#include <unordered_map>

namespace Pacha
{
	class PachaId : public SerializableObject
	{
	public:
		constexpr PachaId()
		{
			m_Hash = 0;
			m_Name = nullptr;
		}

		constexpr PachaId(size_t hash, const char* name)
		{
			m_Hash = hash;
			m_Name = name;
		}

		size_t GetHash() const
		{
			return m_Hash;
		}

		const char* GetName() const
		{
			return m_Name;
		}

		operator bool() const
		{
			return m_Hash != 0;
		}

		operator size_t() const
		{
			return m_Hash;
		}

		operator const char* () const
		{
			return m_Name;
		}

		bool operator==(const PachaId& rhs)
		{
			return m_Hash == rhs.m_Hash;
		}

		bool operator!=(const PachaId& rhs)
		{
			return m_Hash != rhs.m_Hash;
		}

		friend bool operator==(const PachaId& lhs, const PachaId& rhs)
		{
			return lhs.m_Hash == rhs.m_Hash;
		}

		friend bool operator!=(const PachaId& lhs, const PachaId& rhs)
		{
			return lhs.m_Hash != rhs.m_Hash;
		}

		static PachaId Create(const std::string& string)
		{
			return { FNV1aHash(string), HoldName(string).c_str() };
		}

	private:
		size_t m_Hash = 0;
		const char* m_Name = "";

		static std::string& HoldName(const std::string& name)
		{
			static std::unordered_map<std::string, std::string> sNameHolder;
			if (!sNameHolder.count(name))
				sNameHolder[name] = name;
			return sNameHolder[name];
		}
		
		friend class Serialization::Accessor;
		template<bool IsSaving, typename Serializer>
		void Serialize(Serializer& serializer)
		{
			if constexpr (IsSaving)
			{
				serializer
				(
					MakeNVP("Hash", m_Hash),
					MakeNVP("Name", std::string(m_Name))
				);
			}
			else
			{
				std::string name = {};
				serializer
				(
					MakeNVP("Hash", m_Hash),
					MakeNVP("Name", name)
				);

				m_Name = HoldName(name).c_str();
			}
		}
	};

	constexpr PachaId operator"" _ID(const char* string, size_t)
	{
		return { FNV1aHash(string), string };
	}
}

namespace std
{
	template <>
	struct hash<Pacha::PachaId>
	{
		size_t operator()(const Pacha::PachaId& id) const
		{
			return id.GetHash();
		}
	};
}
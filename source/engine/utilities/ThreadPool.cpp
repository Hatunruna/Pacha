#include "ThreadPool.h"

namespace Pacha
{
	ThreadPool::ThreadPool(const size_t& numThreads)
	{
		std::atomic_store(&m_EndThread, false);
	
		m_NumThreads = numThreads;
		m_Threads.resize(numThreads);

		for (size_t i = 0; i < m_NumThreads; ++i)
			m_Threads[i] = std::thread(&ThreadPool::InfiniteLoop, this);
	}

	ThreadPool::~ThreadPool()
	{
		std::atomic_store(&m_EndThread, true);
		m_InternalWaitCondition.notify_all();

		for (size_t i = 0; i < m_NumThreads; ++i)
			if (m_Threads[i].joinable())
				m_Threads[i].join();

		m_Threads.clear();
	}

	void ThreadPool::SetThreadCount(const size_t& numThreads)
	{
		std::atomic_store(&m_EndThread, true);
		m_InternalWaitCondition.notify_all();

		for (size_t i = 0; i < m_NumThreads; ++i)
			if (m_Threads[i].joinable())
				m_Threads[i].join();

		m_Threads.clear();

		std::atomic_store(&m_EndThread, false);
		m_NumThreads = numThreads;
		m_Threads.resize(numThreads);

		for (size_t i = 0; i < m_NumThreads; ++i)
			m_Threads[i] = std::thread(&ThreadPool::InfiniteLoop, this);
	}

	void ThreadPool::AddFunction(const std::function<void()>& function)
	{
		{
			std::unique_lock<std::mutex> lock(m_Mutex);
			m_FunctionsQueue.push(function);
		}

		m_InternalWaitCondition.notify_one();
	}

	void ThreadPool::Wait()
	{
		{
			std::unique_lock<std::mutex> lock(m_Mutex);
			m_WaitCondition.wait(lock, [&] { return m_WorkCounter == 0 && m_FunctionsQueue.empty(); });
		}
	}

	const size_t& ThreadPool::GetNumThreads() const
	{
		return m_NumThreads;
	}

	void ThreadPool::InfiniteLoop()
	{
		std::function<void()> localJob = nullptr;

		while (true)
		{
			{
				std::unique_lock<std::mutex> lock(m_Mutex);

				bool exit = false;
				m_InternalWaitCondition.wait(lock, [&]
				{
					exit = std::atomic_load(&m_EndThread);
					return !m_FunctionsQueue.empty() || exit;
				});

				if (exit)
					return;

				localJob = m_FunctionsQueue.front();
				m_FunctionsQueue.pop();
				++m_WorkCounter;
			}

			localJob();

			{
				std::unique_lock<std::mutex> lock(m_Mutex);
				--m_WorkCounter;
				m_WaitCondition.notify_one();
			}
		}
	}
}
#pragma once
#include <thread>
#include <condition_variable>
#include <mutex>
#include <atomic>
#include <functional>
#include <queue>
#include <vector>

namespace Pacha
{
	class ThreadPool
	{
	public:
		ThreadPool(const size_t& numThreads = std::thread::hardware_concurrency());
		~ThreadPool();

		void SetThreadCount(const size_t& numThreads);

		void AddFunction(const std::function<void()>& function);
		void Wait();

		const size_t& GetNumThreads() const;

	private:
		void InfiniteLoop();

		size_t m_NumThreads = 0;
		std::vector<std::thread> m_Threads;
	
		size_t m_WorkCounter = 0;
		std::atomic_bool m_EndThread;

		std::mutex m_Mutex;
		std::condition_variable m_InternalWaitCondition;
		std::condition_variable m_WaitCondition;

		std::queue<std::function<void()>> m_FunctionsQueue;
	};
}
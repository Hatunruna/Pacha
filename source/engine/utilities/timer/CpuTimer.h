#pragma once
#include "Timer.h"

#include <chrono>
#include <vector>

namespace Pacha
{
	class CpuTimer : public Timer
	{
	public:
		void StartCapture(void* payload = nullptr) override;
		void EndCapture(void* payload = nullptr) override;
		float GetElapsedTime() const override;

	private:
		bool m_Started = false;
		float m_ElapsedTime = 0;
		
		std::chrono::high_resolution_clock::time_point m_StartTimePoint;
		std::chrono::high_resolution_clock::time_point m_EndTimePoint;
	};

	class CpuPerformanceTimer : public PerformanceTimer<CpuTimer, CpuPerformanceTimer, 30>
	{
	public:
		CpuPerformanceTimer(const PachaId& id);
	};
}
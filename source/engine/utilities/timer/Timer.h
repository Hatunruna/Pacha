#pragma once
#include "core/Log.h"
#include "utilities/PachaId.h"

#include <vector>
#include <marl/ticket.h>

namespace Pacha
{
	struct ProfilerStats
	{
		PachaId id;
		float time = 0.f;
		std::vector<uint32_t> children = {};
	};

	class Timer
	{
	public:
		virtual ~Timer() { };
		virtual void StartCapture(void* payload = nullptr) = 0;
		virtual void EndCapture(void* payload = nullptr) = 0;
		virtual float GetElapsedTime() const = 0;
	};

	template<typename TimerType, typename EntryType, uint32_t SampleCount>
	class PerformanceTimer
	{
	public:
		PerformanceTimer(const PachaId& id)
			: m_Id(id)
		{
			m_Samples.fill(0);
		}

		void StartCapture(void* payload = nullptr)
		{
			m_Timer.StartCapture(payload);
		}

		void EndCapture(void* payload = nullptr)
		{
			m_Timer.EndCapture(payload);
		}

		float GetLastCaptureTime() const
		{
			return m_LastCaptureTime;
		}

		float GetAverageCaptureTime() const
		{
			return m_AverageCaptureTime;
		}

		const char* GetName() const
		{
			return m_Id.GetName();
		}

		const PachaId& GetId() const
		{
			return m_Id;
		}

		std::vector<EntryType>& GetChildren()
		{
			return m_Children;
		}

		const std::vector<float>& GetSamples()
		{
			return m_Samples;
		}

		void ComputeAverage()
		{
			m_LastCaptureTime = m_Timer.GetElapsedTime();

			m_SamplesSum -= m_Samples[m_SampleIndex];
			m_Samples[m_SampleIndex] = m_LastCaptureTime;
			m_SamplesSum += m_Samples[m_SampleIndex];

			m_AverageCaptureTime = m_SamplesSum / SampleCount;
			m_SampleIndex = (m_SampleIndex + 1) % SampleCount;

			for (auto& it : m_Children)
				it.ComputeAverage();
		}

		EntryType& GetChild(const PachaId& id)
		{
			for (auto& it : m_Children)
			{
				if (it.GetId() == id)
					return it;
			}

			return InvalidEntryType();
		}

		EntryType& GetOrAddChild(const PachaId& id)
		{
			for (auto& it : m_Children)
			{
				if (it.GetId() == id)
					return it;
			}

			static marl::Ticket::Queue ticketQueue;
			marl::Ticket ticket = ticketQueue.take();
			ticket.wait();

			m_Children.push_back(EntryType(id));
			EntryType& element = m_Children.back();

			ticket.done();
			return element;
		}

		static EntryType& InvalidEntryType()
		{
			static PachaId invalidId;
			static EntryType invalid(invalidId);
			return invalid;
		}

	protected:
		TimerType m_Timer;
		std::vector<EntryType> m_Children;

	private:
		uint32_t m_SampleIndex = 0;
		float m_SamplesSum = 0;
		float m_LastCaptureTime = 0;
		float m_AverageCaptureTime = 0;

		PachaId m_Id;
		std::array<float, SampleCount> m_Samples;
	};

	template<typename T>
	static void GatherTimerEntryStats(T& timerEntry, std::deque<ProfilerStats>& stats)
	{
		ProfilerStats& taskStats = stats.emplace_back();
		taskStats.id = timerEntry.GetId();
		taskStats.time = timerEntry.GetAverageCaptureTime();

		for (auto& child : timerEntry.GetChildren())
		{
			taskStats.children.push_back(static_cast<uint32_t>(stats.size()));
			GatherTimerEntryStats(child, stats);
		}
	}
}
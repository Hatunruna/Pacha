#include "CpuTimer.h"

namespace Pacha
{
	void CpuTimer::StartCapture(void*)
	{
		if (!m_Started)
		{
			m_StartTimePoint = std::chrono::high_resolution_clock::now();
			m_Started = true;
		}
	}

	void CpuTimer::EndCapture(void*)
	{
		if (m_Started)
		{
			m_EndTimePoint = std::chrono::high_resolution_clock::now();
			std::chrono::duration<float> elapsed = m_EndTimePoint - m_StartTimePoint;
			m_ElapsedTime = elapsed.count();
			m_Started = false;
		}
	}

	float CpuTimer::GetElapsedTime() const
	{
		return m_ElapsedTime;
	}

	CpuPerformanceTimer::CpuPerformanceTimer(const PachaId& id)
		: PerformanceTimer(id)
	{ }
}

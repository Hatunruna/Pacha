#include "GpuTimerVK.h"
#include "graphics/device/GfxDevice.h"

#include <chrono>

namespace Pacha
{
	GpuTimerVK::GpuTimerVK()
	{
		GfxDevice& device = GfxDevice::GetInstance();

		VkQueryPoolCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO;
		createInfo.queryType = VK_QUERY_TYPE_TIMESTAMP;
		createInfo.queryCount = 2;
		vkCreateQueryPool(device.GetVkDevice(), &createInfo, nullptr, &m_QueryPool);
	}

	GpuTimerVK::~GpuTimerVK()
	{
		GfxDevice& device = GfxDevice::GetInstance();
		vkDestroyQueryPool(device.GetVkDevice(), m_QueryPool, nullptr);
	}

	void GpuTimerVK::StartCapture(void* payload)
	{
		if (!m_FirstRun)
		{
			GfxDevice& device = GfxDevice::GetInstance();

			size_t timestamps[4];
			vkGetQueryPoolResults(device.GetVkDevice(), m_QueryPool, 0, 2, sizeof(size_t) * 4, timestamps, sizeof(size_t) * 2, VK_QUERY_RESULT_64_BIT | VK_QUERY_RESULT_WITH_AVAILABILITY_BIT);

			if (timestamps[1] && timestamps[3])
			{
				std::chrono::nanoseconds nanoseconds = std::chrono::nanoseconds(static_cast<size_t>((timestamps[2] - timestamps[0]) * device.GetTimestampPeriod()));
				m_LastFrameTime = std::chrono::duration_cast<std::chrono::duration<float>>(nanoseconds).count();
			}
		}
		else
			m_FirstRun = false;

		GfxCommandBuffer* payloadBuffer = reinterpret_cast<GfxCommandBuffer*>(payload);
		vkCmdResetQueryPool(payloadBuffer->GetBuffer(), m_QueryPool, 0, 2);
		vkCmdWriteTimestamp(payloadBuffer->GetBuffer(), VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, m_QueryPool, 0);
	}

	void GpuTimerVK::EndCapture(void* payload)
	{
		GfxCommandBuffer* payloadBuffer = reinterpret_cast<GfxCommandBuffer*>(payload);
		vkCmdWriteTimestamp(payloadBuffer->GetBuffer(), VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, m_QueryPool, 1);
	}

	float GpuTimerVK::GetElapsedTime() const
	{
		return m_LastFrameTime;
	}
}

#pragma once
#include "Timer.h"

#include <volk.h>

namespace Pacha
{
	class GpuTimerVK : public Timer
	{
	public:
		GpuTimerVK();
		~GpuTimerVK();

		void StartCapture(void* payload) override;
		void EndCapture(void* payload) override;
		float GetElapsedTime() const override;

	private:
		bool m_FirstRun = true;
		float m_LastFrameTime = 0;
		VkQueryPool m_QueryPool = VK_NULL_HANDLE;
	};
}
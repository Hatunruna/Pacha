#pragma once
#if RENDERER_VK
#include "GpuTimerVK.h"
#endif

namespace Pacha
{
#if RENDERER_VK
	using GpuTimer = GpuTimerVK;
#endif

	class GpuPerformanceTimer : public PerformanceTimer<GpuTimer, GpuPerformanceTimer, 30>
	{
	public:
		GpuPerformanceTimer(const PachaId& id)
			: PerformanceTimer(id)
		{ }
	};
}
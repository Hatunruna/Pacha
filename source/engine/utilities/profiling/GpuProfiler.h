#pragma once
#include "utilities/timer/GpuTimer.h"

#include <stack>
#include <vector>
#include <unordered_map>

namespace Pacha
{
	class GpuProfiler
	{
	public:
		GpuProfiler(const PachaId& id);

		void BeginProfiler(void* payload = nullptr);
		void EndProfiler(void* payload = nullptr);
		GpuPerformanceTimer& GetTimer();

		void StartCapture(const PachaId& event, void* payload = nullptr);
		void EndCapture(const PachaId& event, void* payload = nullptr);

		float GetCaptureTime() const;
		float GetAverageCaptureTime() const;
		const PachaId& GetId() const;

	private:
		GpuPerformanceTimer m_Timer;
		PachaId m_Id;
	};

	class ScopedGpuProfilerCapture
	{
	public:
		ScopedGpuProfilerCapture(const PachaId& id, GpuProfiler& profiler, void* payload = nullptr)
			: m_Id(id), m_Profiler(profiler), m_Payload(payload)
		{
			m_Profiler.StartCapture(m_Id, m_Payload);
		}
		
		~ScopedGpuProfilerCapture()
		{
			m_Profiler.EndCapture(m_Id, m_Payload);
		}

		GpuProfiler& GetProfiler()
		{
			return m_Profiler;
		}

	private:
		PachaId m_Id;
		GpuProfiler& m_Profiler;
		void* m_Payload = nullptr;
	};
}
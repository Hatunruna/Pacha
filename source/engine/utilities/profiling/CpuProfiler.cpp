#include "CpuProfiler.h"
#include "core/Assert.h"

namespace Pacha
{
	CpuProfiler::CpuProfiler(const PachaId& id)
		: m_Timer(id), m_Id(id)
	{ }

	void CpuProfiler::BeginProfiler()
	{
		m_Timer.StartCapture();
	}

	void CpuProfiler::EndProfiler()
	{
		for (auto& child : m_Timer.GetChildren())
		{
			child.EndCapture();
			child.ComputeAverage();
		}

		m_Timer.EndCapture();
		m_Timer.ComputeAverage();
	}

	void CpuProfiler::StartCapture(const PachaId& event)
	{
		m_Timer.GetOrAddChild(event).StartCapture();
	}

	void CpuProfiler::EndCapture(const PachaId& event)
	{
		m_Timer.GetChild(event).EndCapture();
	}

	float CpuProfiler::GetAverageCaptureTime() const
	{
		return m_Timer.GetAverageCaptureTime();
	}

	const PachaId& CpuProfiler::GetId() const
	{
		return m_Id;
	}

	float CpuProfiler::GetCaptureTime() const
	{
		return m_Timer.GetLastCaptureTime();
	}

	CpuPerformanceTimer& CpuProfiler::GetTimer()
	{
		return m_Timer;
	}
}
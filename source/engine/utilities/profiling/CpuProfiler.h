#pragma once
#include "utilities/timer/CpuTimer.h"

#include <stack>
#include <vector>
#include <unordered_map>

namespace Pacha
{
	class CpuProfiler
	{
	public:
		CpuProfiler(const PachaId& id);

		void BeginProfiler();
		void EndProfiler();
		CpuPerformanceTimer& GetTimer();

		void StartCapture(const PachaId& event);
		void EndCapture(const PachaId& event);

		float GetCaptureTime() const;
		float GetAverageCaptureTime() const;
		const PachaId& GetId() const;

	private:
		PachaId m_Id;
		CpuPerformanceTimer m_Timer;
	};

	class ScopedProfilerCapture
	{
	public:
		ScopedProfilerCapture(const PachaId& id, CpuProfiler& profiler)
			: m_Id(id), m_Profiler(profiler)
		{
			m_Profiler.StartCapture(m_Id);
		}
		
		~ScopedProfilerCapture()
		{
			m_Profiler.EndCapture(m_Id);
		}

		CpuProfiler& GetProfiler()
		{
			return m_Profiler;
		}

	private:
		PachaId m_Id;
		CpuProfiler& m_Profiler;
	};
}
#include "GpuProfiler.h"
#include "core/Assert.h"

namespace Pacha
{
	GpuProfiler::GpuProfiler(const PachaId& id)
		: m_Timer(id), m_Id(id)
	{ }

	void GpuProfiler::BeginProfiler(void* payload)
	{
		m_Timer.StartCapture(payload);
	}

	void GpuProfiler::EndProfiler(void* payload)
	{
		for (auto& child : m_Timer.GetChildren())
		{
			child.EndCapture(payload);
			child.ComputeAverage();
		}

		m_Timer.EndCapture(payload);
		m_Timer.ComputeAverage();
	}

	void GpuProfiler::StartCapture(const PachaId& event, void* payload)
	{
		m_Timer.GetOrAddChild(event).StartCapture(payload);
	}

	void GpuProfiler::EndCapture(const PachaId& event, void* payload)
	{
		m_Timer.GetChild(event).EndCapture(payload);
	}

	float GpuProfiler::GetAverageCaptureTime() const
	{
		return m_Timer.GetAverageCaptureTime();
	}

	const PachaId& GpuProfiler::GetId() const
	{
		return m_Id;
	}

	float GpuProfiler::GetCaptureTime() const
	{
		return m_Timer.GetLastCaptureTime();
	}

	GpuPerformanceTimer& GpuProfiler::GetTimer()
	{
		return m_Timer;
	}
}
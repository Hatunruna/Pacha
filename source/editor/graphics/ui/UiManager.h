#pragma once
#include "UITexture.h"
#include "core/Assert.h"
#include "EditorWidget.h"
#include "utilities/PachaId.h"
#include "graphics/imgui/ImGuiImage.h"
#include "graphics/resources/texture/GfxTexture.h"

#include <imgui.h>
#include <unordered_map>

namespace Pacha
{
	class UiManager
	{
	private:
		UiManager() = default;
		UiManager(const UiManager&) = delete;
		UiManager& operator=(const UiManager&) = delete;

	public:
		static UiManager& GetInstance()
		{
			static UiManager sInstance;
			return sInstance;
		}

		~UiManager();

		void Render(class SimulationContext* simulationContext);
		void FreeResources();

		template<typename T, typename ...Args, typename = std::enable_if_t<std::is_base_of_v<EditorWidget, T>>>
		T* RegisterWidget(const PachaId& id, Args... values)
		{
			PACHA_ASSERT(!m_Widgets.count(id), "Widget id already registered");
			T* newWidget = new T(values...);
			newWidget->SetId(id);

			if (!m_Locked)
				m_Widgets[id] = newWidget;
			else
				m_WidgetsToAdd[id] = newWidget;
			
			return newWidget;
		}

		void RemoveWidget(const PachaId& id);
		ImTextureID GetTextureId(GfxTexture* texture);

		ImTextureID GetTextureImGuiID(GfxTexture* texture, const GfxResourceState currentState);
		ImTextureID GetPipelineTextureImGuiID(const PachaId& id, const GfxResourceState currentState);
		const UITexture& GetUITexture(const struct FramePacket* framePacket, const ImTextureID id);

	private:
		bool m_Locked = false;
		ImGuiImage m_ImGuiImageManager;
		FramePacket* m_CurrentFramePacket = nullptr;
		
		std::vector<PachaId> m_WidgetsToRemove;
		std::unordered_map<PachaId, EditorWidget*> m_Widgets;
		std::unordered_map<PachaId, EditorWidget*> m_WidgetsToAdd;

		std::unordered_map<PachaId, size_t> m_PachaIdToImguiTextureLookup;
		std::unordered_map<GfxTexture*, size_t> m_GfxTextureToImguiTextureLookup;
	};
}
#pragma once
#include "utilities/PachaId.h"

#include <imgui.h>
#include <IconsFontAwesome6.h>
#include <portable-file-dialogs.h>

namespace Pacha
{
	class EditorWidget
	{
	public:
		EditorWidget() = default;
		virtual ~EditorWidget() {};
		virtual void Render(class SimulationContext* simulationContext) = 0;

		void SetId(const PachaId& id) { m_Id = id; }
		const PachaId& GetId() const { return m_Id; }

	protected:
		PachaId m_Id;
		void AutoRemove(const bool& shouldRemove);
	};
}
#pragma once
#include "utilities/PachaId.h"
#include "graphics/resources/texture/GfxTexture.h"

namespace Pacha
{
	struct UITexture
	{
		PachaId id;
		GfxTexture* texture = nullptr;
		GfxResourceState state = GfxResourceState::UNDEFINED;
	};
}
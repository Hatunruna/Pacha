#include "UiManager.h"
#include "EditorWidget.h"
#include "core/graph/context/SimulationContext.h"

namespace Pacha
{
	UiManager::~UiManager()
	{
		for (auto const& [id, widget] : m_Widgets)
			delete widget;
		m_Widgets.clear();
	}

	void UiManager::RemoveWidget(const PachaId& id)
	{
		PACHA_ASSERT(m_Widgets.count(id), "Widget Id not registered");

		if (m_Locked)
			m_WidgetsToRemove.push_back(id);
		else
		{
			delete m_Widgets[id];
			m_Widgets.erase(id);
		}
	}

	ImTextureID UiManager::GetTextureId(GfxTexture* texture)
	{
		return m_ImGuiImageManager.Get(texture);
	}

	void UiManager::Render(SimulationContext* simulationContext)
	{
		m_CurrentFramePacket = &simulationContext->GetFramePacket();

		m_PachaIdToImguiTextureLookup.clear();
		m_GfxTextureToImguiTextureLookup.clear();

		m_Locked = true;

		for (auto const& [id, widget] : m_Widgets)
			widget->Render(simulationContext);

		m_Locked = false;

		if (!m_WidgetsToRemove.empty())
		{
			for (auto const& widgetId : m_WidgetsToRemove)
			{
				delete m_Widgets[widgetId];
				m_Widgets.erase(widgetId);
			}

			m_WidgetsToRemove.clear();
		}

		if (!m_WidgetsToAdd.empty())
		{
			m_Widgets.insert(m_WidgetsToAdd.begin(), m_WidgetsToAdd.end());
			m_WidgetsToAdd.clear();
		}
	}

	void UiManager::FreeResources()
	{
		m_ImGuiImageManager.FreeResources();
	}

	ImTextureID UiManager::GetTextureImGuiID(GfxTexture* texture, const GfxResourceState currentState)
	{
		PACHA_ASSERT(texture, "Texture can not be null");

		auto found = m_GfxTextureToImguiTextureLookup.find(texture);
		if (found == m_GfxTextureToImguiTextureLookup.end())
		{
			size_t textureOffset = m_CurrentFramePacket->uiTextures.size();

			UITexture uiTexture;
			uiTexture.texture = texture;
			uiTexture.state = currentState;
			m_CurrentFramePacket->uiTextures.push_back(uiTexture);

			m_GfxTextureToImguiTextureLookup[texture] = textureOffset;
			return reinterpret_cast<ImTextureID>(textureOffset);
		}

		return reinterpret_cast<ImTextureID>(found->second);
	}

	ImTextureID UiManager::GetPipelineTextureImGuiID(const PachaId& id, const GfxResourceState currentState)
	{
		auto found = m_PachaIdToImguiTextureLookup.find(id);
		if (found == m_PachaIdToImguiTextureLookup.end())
		{
			size_t textureOffset = m_CurrentFramePacket->uiTextures.size();
			
			UITexture uiTexture;
			uiTexture.id = id;
			uiTexture.state = currentState;
			m_CurrentFramePacket->uiTextures.push_back(uiTexture);

			m_PachaIdToImguiTextureLookup[id] = textureOffset;
			return reinterpret_cast<ImTextureID>(textureOffset);
		}

		return reinterpret_cast<ImTextureID>(found->second);
	}

	const UITexture& UiManager::GetUITexture(const FramePacket* framePacket, const ImTextureID id)
	{
		size_t textureOffset = reinterpret_cast<size_t>(id);
		if (textureOffset < framePacket->uiTextures.size())
			return framePacket->uiTextures[textureOffset];
		else
		{
			static UITexture sNullTexture = {};
			return sNullTexture;
		}
	}
}

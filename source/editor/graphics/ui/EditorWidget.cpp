#include "EditorWidget.h"
#include "graphics/ui/UiManager.h"

namespace Pacha
{
	void EditorWidget::AutoRemove(const bool& shouldRemove)
	{
		if (shouldRemove)
			UiManager::GetInstance().RemoveWidget(m_Id);
	}
}
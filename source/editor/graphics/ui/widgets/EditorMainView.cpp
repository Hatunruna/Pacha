#include "EditorMainView.h"
#include "input/Input.h"
#include "level/LevelManager.h"
#include "platform/GfxWindow.h"
#include "core/ProjectConstants.h"
#include "platform/PlatformEvents.h"
#include "utilities/EditorSelectionManager.h"
#include "graphics/ui/utilities/AssetUtilities.h"
#include "graphics/ui/utilities/EntityUtilities.h"

#include <imgui_internal.h>

namespace Pacha
{
	EditorMainView::EditorMainView()
	{
		LevelManager& levelManager = LevelManager::GetInstance();
		m_EditorLevel = levelManager.New();

		m_GameView = std::make_unique<GameView>();
		m_LevelView = std::make_unique<LevelView>();
		m_ConsoleLog = std::make_unique<ConsoleLog>();
		m_LevelExplorer = std::make_unique<LevelExplorer>();
		m_ResourceBrowser = std::make_unique<ResourceBrowser>();
		m_EntityInspector = std::make_unique<EntityInspector>();
	}

	EditorMainView::~EditorMainView()
	{
		delete m_EditorLevel;
	}

	glm::uvec2 EditorMainView::GetGameViewResolution()
	{
		return m_GameView->GetResolution();
	}

	glm::uvec2 EditorMainView::GetLevelViewResolution()
	{
		return m_LevelView->GetResolution();
	}

	void EditorMainView::Render(SimulationContext* simulationContext)
	{
		DrawMenuBar();
		DrawToolBar();
		DoDockArea(simulationContext);
		HandleKeyboardShortcuts();
	}

	void EditorMainView::DrawMenuBar()
	{
		if (ImGui::BeginMainMenuBar())
		{
			DrawFileMenu();
			DrawEditMenu();
			DrawEntityMenu();
			DrawAssetMenu();
			DrawWindowMenu();


			ImGui::EndMainMenuBar();
		}
	}

	void EditorMainView::DrawToolBar()
	{
		ImGuiContext* context = ImGui::GetCurrentContext();
		ImGuiViewportP* viewport = context->Viewports[0];
		ImGuiWindow* toolBarWindow = ImGui::FindWindowByName("##Toolbar");

		ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoSavedSettings;
		if (toolBarWindow == NULL || toolBarWindow->BeginCount == 0)
		{
			ImVec2 toolBarPos = ImVec2(viewport->Pos.x + viewport->WorkOffsetMin.x, viewport->Pos.y + viewport->WorkOffsetMin.y);
			ImVec2 toolBarSize = ImVec2(viewport->Size.x - viewport->WorkOffsetMin.x + viewport->WorkOffsetMin.x, -1.0f);
			ImGui::SetNextWindowPos(toolBarPos);
			ImGui::SetNextWindowSize(toolBarSize);
		}

		ImGui::SetNextWindowViewport(viewport->ID);
		if (ImGui::Begin("##Toolbar", nullptr, window_flags))
		{
			//if (ImGui::RadioButton(ICON_FA_STREET_VIEW, m_LevelView->IsTransformGuizmoModeInUse(ImGuizmo::MODE::LOCAL)))
				//m_LevelView->SetTransformGizmoMode(ImGuizmo::MODE::LOCAL);

			ImGui::SameLine();

			//if (ImGui::RadioButton(ICON_FA_GLOBE_AMERICAS, m_LevelView->IsTransformGuizmoModeInUse(ImGuizmo::MODE::WORLD)))
			//	m_LevelView->SetTransformGizmoMode(ImGuizmo::MODE::WORLD);

			ImGui::SameLine();
			//ImGui::Text(ICON_FA_GRIP_LINES_VERTICAL);
			ImGui::SameLine();

			//if (ImGui::RadioButton(ICON_FA_ARROWS_ALT, m_LevelView->IsTransformGuizmoOperationInUse(ImGuizmo::OPERATION::TRANSLATE)))
			//	m_LevelView->SetTransformGizmoOperation(ImGuizmo::OPERATION::TRANSLATE);

			ImGui::SameLine();

			//if (ImGui::RadioButton(ICON_FA_SYNC_ALT, m_LevelView->IsTransformGuizmoOperationInUse(ImGuizmo::OPERATION::ROTATE)))
			//	m_LevelView->SetTransformGizmoOperation(ImGuizmo::OPERATION::ROTATE);

			ImGui::SameLine();

			//if (ImGui::RadioButton(ICON_FA_EXPAND_ARROWS_ALT, m_LevelView->IsTransformGuizmoOperationInUse(ImGuizmo::OPERATION::SCALE)))
			//	m_LevelView->SetTransformGizmoOperation(ImGuizmo::OPERATION::SCALE);

			ImGui::SameLine();

			//if (ImGui::RadioButton(ICON_FA_EXPAND, m_LevelView->IsTransformGuizmoOperationInUse(ImGuizmo::OPERATION::BOUNDS)))
			//	m_LevelView->SetTransformGizmoOperation(ImGuizmo::OPERATION::BOUNDS);

			const float playButtonWidth = 25.f;
			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.f, 0.f, 0.f, 0.f));
			ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4(0.f, 0.f, 0.f, 0.f));
			ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4(0.f, 0.f, 0.f, 0.f));

			ImGui::SameLine(ImGui::GetWindowWidth() - (playButtonWidth + ImGui::GetStyle().ItemSpacing.x * 2));
			if (ImGui::Button("##HiddenPlayButton", ImVec2(playButtonWidth, 0)))
			{
				//Start PlayMode
			}

			bool wasHovered = ImGui::IsItemHovered();
			bool wasActive = ImGui::IsItemActive();

			if (wasActive)
				ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.f, 1.f, 0.f, 1.f));
			else if (wasHovered)
				ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.f, 0.75f, 0.f, 1.f));

			ImGui::SameLine(ImGui::GetWindowWidth() - (playButtonWidth + ImGui::GetStyle().ItemSpacing.x * 2));
			ImGui::TextUnformatted(ICON_FA_PLAY);

			if (wasActive || wasHovered)
				ImGui::PopStyleColor(1);

			ImGui::PopStyleColor(3);
		}

		toolBarWindow = ImGui::GetCurrentWindow();
		if (toolBarWindow->BeginCount == 1)
			viewport->WorkOffsetMin.y += toolBarWindow->Size.y;

		ImGui::End();
	}

	void EditorMainView::DoDockArea(SimulationContext* simulationContext)
	{
		ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDocking;
		window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
		window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;

		ImGuiContext& g = *GImGui;
		ImGuiViewportP* viewport = g.Viewports[0];

		ImGuiWindow* dockSpaceWindow = ImGui::FindWindowByName("##DockSpaceWindow");
		if (dockSpaceWindow == NULL || dockSpaceWindow->BeginCount == 0)
		{
			ImVec2 dockPos = ImVec2(viewport->Pos.x, viewport->Pos.y + viewport->WorkOffsetMin.y);
			ImVec2 dockSize = ImVec2(viewport->Size.x, viewport->Size.y - viewport->WorkOffsetMin.y);

			ImGui::SetNextWindowPos(dockPos);
			ImGui::SetNextWindowSize(dockSize);
		}

		ImGui::SetNextWindowViewport(viewport->ID);

		ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));

		ImGui::Begin("##DockSpaceWindow", NULL, window_flags);
		ImGui::PopStyleVar(3);

		bool newLayout = false;
		ImGuiID dockSpaceID = ImGui::GetID("Main DockSpace");
		ImGuiDockNode* mainDockNode = ImGui::DockBuilderGetNode(dockSpaceID);
		if (mainDockNode == NULL)
		{
			newLayout = true;
			ImGui::DockBuilderAddNode(dockSpaceID, ImGuiDockNodeFlags_DockSpace);

			const glm::uvec2& windowSize = GfxWindow::GetMainWindow().GetSize();
			ImGui::DockBuilderSetNodeSize(dockSpaceID, ImVec2(static_cast<float>(windowSize.x), static_cast<float>(windowSize.y)));

			//Order matters!
			ImGuiID mainDock = dockSpaceID;
			ImGuiID entityInspectorID = ImGui::DockBuilderSplitNode(mainDock, ImGuiDir_Right, 0.2f, NULL, &mainDock);
			ImGuiID consoleLogID = ImGui::DockBuilderSplitNode(mainDock, ImGuiDir_Down, 0.25f, NULL, &mainDock);
			ImGuiID resourcesID = ImGui::DockBuilderSplitNode(consoleLogID, ImGuiDir_Left, 0.35f, NULL, &consoleLogID);
			ImGuiID levelExplorer = ImGui::DockBuilderSplitNode(mainDock, ImGuiDir_Left, 0.2f, NULL, &mainDock);
			ImGuiID levelViewID = ImGui::DockBuilderGetCentralNode(dockSpaceID)->ID;
			ImGuiID gameViewId = ImGui::DockBuilderGetCentralNode(dockSpaceID)->ID;

			ImGui::DockBuilderDockWindow("Console Log", consoleLogID);
			ImGui::DockBuilderDockWindow("Resource Browser", resourcesID);
			ImGui::DockBuilderDockWindow("Level View", levelViewID);
			ImGui::DockBuilderDockWindow("Game View", gameViewId);
			ImGui::DockBuilderDockWindow("Level Explorer", levelExplorer);
			ImGui::DockBuilderDockWindow("Entity Inspector", entityInspectorID);
			ImGui::DockBuilderFinish(dockSpaceID);
		}

		ImGui::DockSpace(dockSpaceID, ImVec2(0.0f, 0.0f), ImGuiDockNodeFlags_None);

		m_LevelView->Draw(m_ShowLevelView, simulationContext);
		m_GameView->Draw(m_ShowGameView, simulationContext);
		m_ConsoleLog->Draw(m_ShowConsoleLog);
		m_LevelExplorer->Draw(m_ShowLevelExplorer);
		m_ResourceBrowser->Draw(m_ShowResourceBrowser);
		m_EntityInspector->Draw(m_ShowEntityInspector);

		if (newLayout && m_ShowLevelView)
			ImGui::FocusWindow(m_LevelView->GetWindow());

		ImGui::End();
	}

	void EditorMainView::DrawFileMenu()
	{
		if (ImGui::BeginMenu("File"))
		{
			if (ImGui::MenuItem("New Level", "| Ctrl+N"))
				NewLevel();

			if (ImGui::MenuItem("Open Level", "| Ctrl+O"))
				OpenLevel();

			if (ImGui::MenuItem("Save Level", "| Ctrl+S"))
				SaveLevel();

			if (ImGui::MenuItem("Save Level As", "| Ctrl+Alt+S"))
				SaveLevelAs();

			ImGui::Separator();

			if (ImGui::MenuItem("Exit"))
				PlatformEvents::GetInstance().TriggerQuit();

			ImGui::EndMenu();
		}
	}

	void EditorMainView::DrawEditMenu()
	{
		if (ImGui::BeginMenu("Edit"))
		{
			ImGui::EndMenu();
		}
	}

	void EditorMainView::DrawEntityMenu()
	{
		if (ImGui::BeginMenu("Entity"))
		{
			EntityUtilities::DrawBarMenu();
			ImGui::EndMenu();
		}
	}

	void EditorMainView::DrawAssetMenu()
	{
		if (ImGui::BeginMenu("Assets"))
		{
			AssetUtilities::DrawBarMenu();
			ImGui::EndMenu();
		}
	}

	void EditorMainView::DrawWindowMenu()
	{
		if (ImGui::BeginMenu("Window"))
		{
			ImGui::MenuItem("Console Log", NULL, &m_ShowConsoleLog, !m_ShowConsoleLog);
			ImGui::MenuItem("Entity Inspector", NULL, &m_ShowEntityInspector, !m_ShowEntityInspector);
			ImGui::MenuItem("Game View", NULL, &m_ShowGameView, !m_ShowGameView);
			ImGui::MenuItem("Level Explorer", NULL, &m_ShowLevelExplorer, !m_ShowLevelExplorer);
			ImGui::MenuItem("Level View", NULL, &m_ShowLevelView, !m_ShowLevelView);
			ImGui::MenuItem("Resource Browser", NULL, &m_ShowResourceBrowser, !m_ShowResourceBrowser);
			ImGui::EndMenu();
		}
	}

	void EditorMainView::NewLevel()
	{
		EditorSelectionManager& selectionManager = EditorSelectionManager::GetInstance();
		selectionManager.ClearLevelViewSelection();
		selectionManager.ClearSelection();

		LevelManager& levelManager = LevelManager::GetInstance();
		m_EditorLevel = levelManager.New();
	}

	void EditorMainView::OpenLevel()
	{
		if(m_EditorLevel->IsDirty())
		{
			pfd::button result = pfd::message("Current level has unsaved cahnges", "Do you want to save the current level?", pfd::choice::yes_no, pfd::icon::warning).result();
			if(result == pfd::button::yes)
				SaveLevel();
		}

		const std::string assetsPath = ProjectConstants::GetInstance().GetAssetsPath().string();
		const std::vector<std::string> filter = {"Level (*.plvl)", "*.plvl"};

		std::vector<std::string> levelSelection = pfd::open_file("Select a level file", assetsPath, filter, pfd::opt::force_path).result();
		if (!levelSelection.empty())
		{
			LevelManager& levelManager = LevelManager::GetInstance();
			m_EditorLevel = levelManager.LoadAndSet(levelSelection[0], true);
		}
	}

	void EditorMainView::SaveLevel()
	{
		if(m_EditorLevel->IsDirty())
		{
			LevelManager& levelManager = LevelManager::GetInstance();
			if (!m_EditorLevel->HasPath())
			{
				std::filesystem::path path = m_EditorLevel->GetPath();
				if(ShowSaveLevelDialog(path))
					levelManager.Save(m_EditorLevel, path);
			}
			else
				levelManager.Save(m_EditorLevel);
		}
	}

	void EditorMainView::SaveLevelAs()
	{
		std::filesystem::path path;
		if (ShowSaveLevelDialog(path))
		{
			LevelManager& levelManager = LevelManager::GetInstance();
			levelManager.Save(m_EditorLevel, path);
		}
	}

	void EditorMainView::HandleKeyboardShortcuts()
	{
		bool lalt = Input::GetKey(KeyCode::LALT);
		bool lctrl = Input::GetKey(KeyCode::LCTRL);
		bool skey = Input::GetKeyDown(KeyCode::S);

		bool saveLevelKS = lctrl && !lalt && skey;
		bool saveLevelAsKS = lctrl && lalt && skey;
		bool newLevelKS = lctrl && Input::GetKeyDown(KeyCode::N);
		bool openLevelKS = lctrl && Input::GetKeyDown(KeyCode::O);

		if (newLevelKS)
			NewLevel();

		if (openLevelKS)
			OpenLevel();

		if (saveLevelKS)
			SaveLevel();

		if (saveLevelAsKS)
			SaveLevelAs();
	}

	bool EditorMainView::ShowSaveLevelDialog(std::filesystem::path& outPath)
	{
		const std::string assetsPath = ProjectConstants::GetInstance().GetAssetsPath().string();
		const std::vector<std::string> filter = {"Level (*.plvl)", "*.plvl"};
		
		std::string savePath = pfd::save_file("Save level", assetsPath, filter).result();
		if (!savePath.empty())
		{
			outPath = savePath;
			return true;
		}

		return false;
	}
}

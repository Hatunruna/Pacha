#pragma once
#include "core/graph/context/SimulationContext.h"

#include <atomic>
#include <glm/glm.hpp>

namespace Pacha
{
	enum class AspectRatio
	{
		_4x3,
		_5x4,
		_16x9,
		_16x10,
		FREE_ASPECT
	};

	class GameView
	{
	public:
		void Draw(bool& showWindow, SimulationContext* simulationContext);
		glm::uvec2 GetResolution();

	private:
		bool m_ShowStats = false;
		AspectRatio m_AspectRatio = AspectRatio::FREE_ASPECT;
		std::atomic<glm::uvec2> m_Resolution = glm::uvec2(1, 1);

		glm::vec4 CalculateTextureRect();
	};
}
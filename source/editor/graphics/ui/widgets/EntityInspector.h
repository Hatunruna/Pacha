#pragma once
#include "ecs/Entity.h"

#include <vector>

namespace Pacha
{
	class EntityInspector
	{
	public:
		void Draw(bool& showWindow);

	private:
		void DrawSingleEntity(Entity* entity);
		void DrawEntities(const std::vector<Entity*>& entities);
	};
}
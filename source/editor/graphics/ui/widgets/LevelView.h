#pragma once
#include "core/graph/context/SimulationContext.h"

#include <atomic>
#include <glm/glm.hpp>

struct ImGuiWindow;

namespace Pacha
{
	class LevelView
	{
	public:
		void Draw(bool& showWindow, SimulationContext* simulationContext);
		glm::uvec2 GetResolution();
		struct ImGuiWindow* GetWindow();

	private:
		bool m_ShowStats = false;
		struct ImGuiWindow* m_Window = nullptr;
		std::atomic<glm::uvec2> m_Resolution = glm::uvec2(1, 1);
	};
}
#pragma once
#include "ecs/Entity.h"

#include <vector>
#include <unordered_map>

namespace Pacha
{
	class LevelExplorer
	{
	public:
		void Draw(bool& showWindow);

	private:
		void RecursiveExplorerBuild(class Transform* root, const std::unordered_set<Transform*>& parents);
		void DoDragAndDrop(Transform* transform);
		void DoEntitySelection(Transform* transform, const bool& selected, const bool& overArrow = false);
		std::unordered_set<Transform*> GetUniqueParents(const std::vector<Entity*>& selectedEntities);
	};
}
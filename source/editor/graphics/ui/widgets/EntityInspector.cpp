#include "EntityInspector.h"

#include "input/Input.h"
#include "ecs/Component.h"
#include "graphics/ui/ImGuiString.h"
#include "utilities/EditorSelectionManager.h"
#include "graphics/ui/utilities/InspectorBuilder.h"

#include <imgui.h>

namespace Pacha
{
	void EntityInspector::Draw(bool& showWindow)
	{
		if (!showWindow)
			return;

		if (ImGui::Begin("Entity Inspector", &showWindow, ImGuiWindowFlags_NoCollapse))
		{
			EditorSelectionManager& selectionManager = EditorSelectionManager::GetInstance();
			const size_t elementsCount = selectionManager.GetSelectionCount();
			
			if (elementsCount > 0)
			{
				std::vector<Entity*>& selectedEntities = selectionManager.GetSelectedEntities();
				if (elementsCount == 1)
					DrawSingleEntity(selectedEntities[0]);
				else
					DrawEntities(selectedEntities);
			}
		}

		ImGui::End();
	}

	void EntityInspector::DrawSingleEntity(Entity* entity)
	{
		bool entityActive = entity->IsActive();
		if (ImGui::Checkbox("##Active", &entityActive))
			entity->SetActive(entityActive);

		ImGui::SameLine();
		std::string nameInputId = "##" + std::to_string(static_cast<uint64_t>(entity->GetEnttId()));

		ImGui::SetNextItemWidth(-1);

		if (Input::GetKeyDown(KeyCode::F2))
		{
			ImGui::SetWindowFocus();
			ImGui::SetKeyboardFocusHere();
		}

		std::string entityName = entity->GetName();
		if (ImGui::InputText(nameInputId.c_str(), &entityName, ImGuiInputTextFlags_EnterReturnsTrue))
			entity->SetName(entityName);

		ImGui::Separator();

		static InspectorBuilder sBuilder;
		for (auto&& [id, storage] : entity->GetLevel()->GetRegistry().storage())
		{
			if (storage.type() != entt::type_id<Entity>())
			{
				if (storage.contains(entity->m_EnttId))
				{
					if (Component* component = static_cast<Component*>(storage.value(entity->m_EnttId)))
						component->DrawInspectorUI(sBuilder);
				}
			}
		}

		ImGui::Separator();
	}

	void EntityInspector::DrawEntities(const std::vector<Entity*>&)
	{
	}
}

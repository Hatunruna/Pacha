#include "LevelExplorer.h"
#include "input/Input.h"
#include "level/LevelManager.h"
#include "ecs/components/Transform.h"
#include "utilities/EditorSelectionManager.h"

#include <imgui.h>
#include <imgui_internal.h>

namespace Pacha
{
	void LevelExplorer::Draw(bool& showWindow)
	{
		if (!showWindow)
			return;

		if (ImGui::Begin("Level Explorer", &showWindow, ImGuiWindowFlags_NoCollapse))
		{
			ImVec2 cursorPos = ImGui::GetCursorPos();
			EditorSelectionManager& selectionManager = EditorSelectionManager::GetInstance();

			std::unordered_set<Transform*> parents;
			if (selectionManager.HasLevelViewSelection())
			{
				const std::vector<Entity*>& levelViewSelection = selectionManager.GetLevelViewSelectedEntities();
				parents = GetUniqueParents(levelViewSelection);
				selectionManager.ClearLevelViewSelection();
			}

			RecursiveExplorerBuild(LevelManager::GetInstance().GetCurrentLevel()->GetRootTransform(), parents);

			ImGui::SetCursorPos(cursorPos);
			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.f, 0.f, 0.f, 0.f));
			ImGui::InvisibleButton("##hidden", ImVec2(-1, -1));
			ImGui::PopStyleColor();

			if (ImGui::BeginDragDropTarget())
			{
				if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("TRANSFORM_DROP"))
				{
					Transform* transform = *static_cast<Transform**>(payload->Data);
					transform->SetParent(nullptr, true);
				}
				ImGui::EndDragDropTarget();
			}

			if (ImGui::IsItemClicked() && !Input::GetKey(KeyCode::LCTRL))
				selectionManager.ClearSelection();
		}

		ImGui::End();
	}

	void LevelExplorer::RecursiveExplorerBuild(Transform* root, const std::unordered_set<Transform*>& parents)
	{
		EditorSelectionManager& selectionManager = EditorSelectionManager::GetInstance();

		for (auto& it : root->GetChildren())
		{
			if (it->GetEntity()->GetEditorFlags() & EditorFlags_HIDDEN_IN_EDITOR)
				continue;

			std::string name = it->GetEntity()->GetName() + "##" + std::to_string(static_cast<uint64_t>(it->GetEntity()->GetEnttId()));
			bool isActive = it->GetEntity()->IsActive();
			bool isSelected = selectionManager.IsInSelection(it->GetEntity());

			if (it->HasChildren())
			{
				ImGuiTreeNodeFlags treeNodeFlags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_SpanAvailWidth;
				if (isSelected) treeNodeFlags |= ImGuiTreeNodeFlags_Selected;

				if (!isActive)
					ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.5f, 0.5f, 0.5f, 1.f));

				ImGuiWindow* window = ImGui::GetCurrentWindowRead();
				float min = window->Rect().Min.x + (ImGui::GetTreeNodeToLabelSpacing() * window->DC.TreeDepth);
				float max = window->Rect().Min.x + (ImGui::GetTreeNodeToLabelSpacing() * (window->DC.TreeDepth + 1));
				bool mouseOverArrow = ImGui::GetMousePos().x >= min && ImGui::GetMousePos().x <= max;

				if (!parents.empty())
					ImGui::SetNextItemOpen(parents.count(it));

				bool treeNodeOpen = ImGui::TreeNodeEx(name.c_str(), treeNodeFlags);
				DoEntitySelection(it, isSelected, mouseOverArrow);
				DoDragAndDrop(it);
				
				if (treeNodeOpen)
				{
					RecursiveExplorerBuild(it, parents);
					ImGui::TreePop();
				}

				if (!isActive)
					ImGui::PopStyleColor();
			}
			else
			{
				if (!isActive)
					ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.5f, 0.5f, 0.5f, 1.f));

				ImGuiTreeNodeFlags treeNodeFlags = ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_SpanAvailWidth;
				if (isSelected) treeNodeFlags |= ImGuiTreeNodeFlags_Selected;

				ImGui::TreeNodeEx(name.c_str(), treeNodeFlags);
				DoEntitySelection(it, isSelected);
				DoDragAndDrop(it);

				if (!isActive)
					ImGui::PopStyleColor();
				ImGui::TreePop();
			}
		}
	}

	void LevelExplorer::DoDragAndDrop(Transform* transform)
	{
		if (Input::GetKey(KeyCode::LCTRL))
			return;

		if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_None))
		{
			ImGui::SetDragDropPayload("TRANSFORM_DROP", &transform, sizeof(Transform**));
			ImGui::TextUnformatted(transform->GetEntity()->GetName().c_str());
			ImGui::EndDragDropSource();
		}

		if (ImGui::BeginDragDropTarget())
		{
			if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("TRANSFORM_DROP"))
			{
				Transform* payloadTransform = *static_cast<Transform**>(payload->Data);
				payloadTransform->SetParent(transform, true);
			}
			ImGui::EndDragDropTarget();
		}
	}

	void LevelExplorer::DoEntitySelection(Transform* transform, const bool& selected, const bool& overArrow)
	{
		if (overArrow)
			return;

		EditorSelectionManager& selectionManager = EditorSelectionManager::GetInstance();
		if (ImGui::IsItemHovered() && ImGui::IsMouseReleased(ImGuiMouseButton_Left))
		{
			if (Input::GetKey(KeyCode::LCTRL))
			{
				if (!selected)
					selectionManager.AddToSelection(transform->GetEntity());
				else
					selectionManager.RemoveFromSelection(transform->GetEntity());
			}
			else
			{
				selectionManager.ClearSelection();
				selectionManager.AddToSelection(transform->GetEntity());
			}
		}
	}

	std::unordered_set<Transform*> LevelExplorer::GetUniqueParents(const std::vector<Entity*>& selectedEntities)
	{
		std::unordered_set<Transform*> uniqueParents;
		for (auto& entity : selectedEntities)
		{
			if (entity->HasComponent<Transform>())
			{
				Transform* transform = entity->GetComponent<Transform>();
				std::vector<Transform*> parents = transform->GetParents();
				uniqueParents.insert(parents.begin(), parents.end());
			}
		}

		return uniqueParents;
	}
}

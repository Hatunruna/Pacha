#include "ConsoleLog.h"

#include <IconsFontAwesome6.h>

namespace Pacha
{
	ConsoleLog::ConsoleLog()
	{
		m_OriginalCoutBuffer = std::cout.rdbuf();
		std::cout.rdbuf(this);
	}

	ConsoleLog::~ConsoleLog()
	{
		std::cout.rdbuf(m_OriginalCoutBuffer);
		m_OriginalCoutBuffer = nullptr;
	}

	void ConsoleLog::Draw(bool& showWindow)
	{
		if (!showWindow)
			return;

		if (ImGui::Begin("Console Log", &showWindow, ImGuiWindowFlags_NoCollapse))
		{
			if (ImGui::RadioButton(ICON_FA_CLIPBOARD_LIST "##ConsoleLog", m_FilterTrace))
			{
				m_FilterTrace = !m_FilterTrace;
				m_CategoryFilterUpdated = true;
			}

			ImGui::SameLine();
			ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1.f, 1.f, 0.f, 1.f));
			if (ImGui::RadioButton(ICON_FA_TRIANGLE_EXCLAMATION "##ConsoleLog", m_FilterWarning))
			{
				m_FilterWarning = !m_FilterWarning;
				m_CategoryFilterUpdated = true;
			}
			ImGui::PopStyleColor();

			ImGui::SameLine();
			ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1.f, 0.f, 0.f, 1.f));
			if (ImGui::RadioButton(ICON_FA_CIRCLE_EXCLAMATION "##ConsoleLog", m_FilterError))
			{
				m_FilterError = !m_FilterError;
				m_CategoryFilterUpdated = true;
			}
			ImGui::PopStyleColor();

			ImGui::SameLine(ImGui::GetWindowWidth() - 150);
			ImGui::Text(ICON_FA_MAGNIFYING_GLASS);

			ImGui::SameLine();
			m_LogFilter.Draw("##ConsoleLogFilter", 100.0f);

			ImGui::SameLine(ImGui::GetWindowWidth() - 25);
			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.f, 0.f, 0.f, 0.f));
			ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4(0.75f, 0.f, 0.f, 1.f));
			ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4(1.f, 0.f, 0.f, 1.f));
			if (ImGui::Button(ICON_FA_RECTANGLE_XMARK "##ConsoleLog"))
				ClearLog();
			ImGui::PopStyleColor(3);
			ImGui::Separator();

			ImGui::BeginChild("ConsoleLogHorizontalScroll", ImVec2(0, 0), false, ImGuiWindowFlags_HorizontalScrollbar);

			if (m_CategoryFilterUpdated)
			{
				std::vector<std::string> categoryFilters;
				if (m_FilterTrace) categoryFilters.push_back("[TRACE]");
				if (m_FilterWarning) categoryFilters.push_back("[WARNING]");
				if (m_FilterError) categoryFilters.push_back("[ERROR]");

				std::string categoryFiltersString;
				for (size_t i = 0; i < categoryFilters.size(); ++i)
				{
					categoryFiltersString += categoryFilters[i];
					if (i != categoryFilters.size() - 1)
						categoryFiltersString += ",";
				}

				memset(m_LogCategoryFilter.InputBuf, '\0', IM_ARRAYSIZE(m_LogCategoryFilter.InputBuf));
				memcpy(m_LogCategoryFilter.InputBuf, categoryFiltersString.data(), categoryFiltersString.size());
				m_LogCategoryFilter.Build();
				m_CategoryFilterUpdated = false;
			}

			for (auto const& it : m_LogEntries)
			{
				if (m_LogCategoryFilter.PassFilter(it.c_str()) && m_LogFilter.PassFilter(it.c_str()))
				{
					ImGui::TextUnformatted(it.c_str());
					ImGui::Separator();
				}
			}

			if (Incremented())
				ImGui::SetScrollHereY(1.0f);

			ImGui::EndChild();
		}

		ImGui::End();
	}

	void ConsoleLog::ClearLog()
	{
		m_LogEntries.clear();
		m_StringAccumulator.clear();
		m_LastEntriesSize = m_EntriesSize = 0;
	}

	bool ConsoleLog::Incremented()
	{
		bool result = m_EntriesSize > m_LastEntriesSize;
		if (result)
		{
			m_LastEntriesSize = m_EntriesSize;
			return true;
		}

		return false;
	}

	const std::vector<std::string>& ConsoleLog::GetLogEntries() const
	{
		return m_LogEntries;
	}

	std::streambuf::int_type ConsoleLog::overflow(int_type character)
	{
		if (character == '\n')
		{
			m_LogEntries.push_back(m_StringAccumulator);
			m_EntriesSize = m_LogEntries.size();
			m_StringAccumulator.clear();
		}
		else
			m_StringAccumulator += static_cast<char>(character);

		return character;
	}

	std::streamsize ConsoleLog::xsputn(const char* text, std::streamsize characterCount)
	{
		m_StringAccumulator += text;
		return characterCount;
	}
}
#include "GameView.h"
#include "utilities/Stats.h"
#include "utilities/RenderDoc.h"
#include "graphics/ui/UiManager.h"

#include <imgui.h>
#include <algorithm>

namespace Pacha
{
	void GameView::Draw(bool& showWindow, SimulationContext* simulationContext)
	{
		if (!showWindow)
			return;

		if (ImGui::Begin("Game View", &showWindow, ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoCollapse))
		{
			if (ImGui::BeginMenuBar())
			{
				if (ImGui::BeginMenu("Aspect Ratio"))
				{
					if (ImGui::MenuItem("4x3", nullptr, m_AspectRatio == AspectRatio::_4x3))
						m_AspectRatio = AspectRatio::_4x3;

					if (ImGui::MenuItem("5x4", nullptr, m_AspectRatio == AspectRatio::_5x4))
						m_AspectRatio = AspectRatio::_5x4;

					if (ImGui::MenuItem("16x9", nullptr, m_AspectRatio == AspectRatio::_16x9))
						m_AspectRatio = AspectRatio::_16x9;

					if (ImGui::MenuItem("16x10", nullptr, m_AspectRatio == AspectRatio::_16x10))
						m_AspectRatio = AspectRatio::_16x10;

					if (ImGui::MenuItem("Free Aspect", nullptr, m_AspectRatio == AspectRatio::FREE_ASPECT))
						m_AspectRatio = AspectRatio::FREE_ASPECT;

					ImGui::EndMenu();
				}

				ShowStatsButton(m_ShowStats);
				ShowRenderDocButton("GameView");
				ImGui::EndMenuBar();
			}

			if (ImGui::BeginChild("GameViewRenderTarget", ImVec2(0, 0), false, ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoScrollbar))
			{
				glm::vec4 textureRect = CalculateTextureRect();
				ImVec2 windowTopLeft = ImGui::GetCursorScreenPos();

				ImGui::SetCursorScreenPos(ImVec2(textureRect.x, textureRect.y));
				m_Resolution = glm::uvec2(textureRect.z, textureRect.w);

				const ImTextureID textureId = UiManager::GetInstance().GetPipelineTextureImGuiID("MainColorBuffer"_ID, GfxResourceState::RENDER_TARGET);
				ImGui::Image(textureId, ImVec2(textureRect.z, textureRect.w));

				DrawStats(m_ShowStats, windowTopLeft, ImGui::GetWindowSize(), simulationContext);
			}
			ImGui::EndChild();
		}
		ImGui::End();
	}

	glm::uvec2 GameView::GetResolution()
	{
		return m_Resolution;
	}

	glm::vec4 GameView::CalculateTextureRect()
	{
		ImVec2 textureSize;
		ImVec2 windowTopLeft = ImGui::GetCursorScreenPos();

		if (m_AspectRatio == AspectRatio::FREE_ASPECT)
			textureSize = ImGui::GetWindowSize();
		else
		{
			ImVec2 windowSize = ImGui::GetWindowSize();

			float width = 0;
			float height = 0;
			float aspectRatio = 0;

			if (windowSize.x >= windowSize.y)
			{
				switch (m_AspectRatio)
				{
					case AspectRatio::FREE_ASPECT:
						break;
					case AspectRatio::_4x3:
						aspectRatio = 3.f / 4.f;
						break;
					case AspectRatio::_5x4:
						aspectRatio = 4.f / 5.f;
						break;
					case AspectRatio::_16x9:
						aspectRatio = 9.f / 16.f;
						break;
					case AspectRatio::_16x10:
						aspectRatio = 10.f / 16.f;
						break;
				}

				width = windowSize.x;
				height = std::floor(width * aspectRatio);

				if (height > windowSize.y)
				{
					height = windowSize.y;
					width = std::floor(height / aspectRatio);
				}
			}
			else
			{
				switch (m_AspectRatio)
				{
					case AspectRatio::FREE_ASPECT:
						break;
					case AspectRatio::_4x3:
						aspectRatio = 4.f / 3.f;
						break;
					case AspectRatio::_5x4:
						aspectRatio = 5.f / 4.f;
						break;
					case AspectRatio::_16x9:
						aspectRatio = 16.f / 9.f;
						break;
					case AspectRatio::_16x10:
						aspectRatio = 16.f / 10.f;
						break;
				}

				height = windowSize.y;
				width = std::floor(height * aspectRatio);

				if (width > windowSize.x)
				{
					width = windowSize.x;
					height = std::floor(width / aspectRatio);
				}
			}

			windowTopLeft.x += (windowSize.x - width) * 0.5f;
			windowTopLeft.y += (windowSize.y - height) * 0.5f;
			textureSize = ImVec2(std::max(1.f, width), std::max(1.f, height));
		}

		return glm::vec4(windowTopLeft.x, windowTopLeft.y, textureSize.x, textureSize.y);
	}
}

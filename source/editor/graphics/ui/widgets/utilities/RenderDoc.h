#pragma once
#include "graphics/renderdoc/RenderDoc.h"

#include <imgui.h>
#include <stdint.h>

namespace Pacha
{
	inline void ShowRenderDocButton(const std::string& ownerID)
	{
		const uint32_t kStatsButtonWidth = 60;
		const uint32_t kRenderDocButtonWidth = 85;

		float windowWidth = ImGui::GetWindowWidth();
		float spacing = ImGui::GetStyle().ItemSpacing.x;

		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.f, 0.f, 0.f, 0.f));

		RenderDoc& renderDoc = RenderDoc::GetInstance();
		if (renderDoc.IsRunning())
		{
			std::string buttonId = "RD Capture##" + ownerID;
			ImGui::SameLine(windowWidth - ((kRenderDocButtonWidth + kStatsButtonWidth) + spacing * 3));
			if (ImGui::Button(buttonId.c_str(), ImVec2(kRenderDocButtonWidth, 0)))
				renderDoc.TriggerCapture();
		}

		ImGui::PopStyleColor();
	}
}
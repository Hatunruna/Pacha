#pragma once
#include "math/Utilities.h"
#include "utilities/Macros.h"
#include "memory/HeapTracker.h"
#include "core/time/RenderTime.h"
#include "collections/CircularArray.h"
#include "graphics/device/GfxDevice.h"
#include "core/graph/context/RenderContext.h"
#include "graphics/renderpipeline/GfxRenderPipeline.h"

#include <imgui.h>
#include <implot.h>

#include <stdint.h>
#include <unordered_map>

namespace Pacha
{
	struct StatsTimeGraphEntry
	{
		double gameThread;
		double renderThread;
		double gpu;
		double frame;
	};

	static void ShowStatsButton(bool& showStats)
	{
		const uint32_t kStatsButtonWidth = 60;

		float windowWidth = ImGui::GetWindowWidth();
		float spacing = ImGui::GetStyle().ItemSpacing.x;

		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.f, 0.f, 0.f, 0.f));
		ImGui::SameLine(windowWidth - (kStatsButtonWidth + spacing * 2));

		std::string buttonId = "Stats##StatsButton";
		if (ImGui::Button(buttonId.c_str(), ImVec2(kStatsButtonWidth, 0)))
			showStats = !showStats;

		ImGui::PopStyleColor();
	}

	static void DrawTableEntry(const std::deque<ProfilerStats>& stats, uint32_t currentIndex)
	{
		static const float kTimeValueCursorOffset = (60.f - ImGui::CalcTextSize("9.99999").x) / 2.f;

		ImGui::TableNextRow();
		ImGui::TableNextColumn();

		const ProfilerStats& currentStats = stats[currentIndex];
		std::string currentStatId = currentStats.id.GetName();
		currentStatId += "##Stats";

		if (currentStats.children.size())
		{
			bool open = ImGui::TreeNodeEx(currentStatId.c_str(), ImGuiTreeNodeFlags_SpanAllColumns);
			ImGui::TableNextColumn();
			ImGui::SetCursorPosX(ImGui::GetCursorPosX() + kTimeValueCursorOffset);
			ImGui::Text("%.5f", currentStats.time * 1000.f);

			if (open)
			{
				for (const auto& child : currentStats.children)
					DrawTableEntry(stats, child);
				ImGui::TreePop();
			}
		}
		else
		{
			ImGui::TreeNodeEx(currentStatId.c_str(), ImGuiTreeNodeFlags_NoTreePushOnOpen | ImGuiTreeNodeFlags_Bullet | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_SpanAllColumns);
			ImGui::TableNextColumn();
			ImGui::SetCursorPosX(ImGui::GetCursorPosX() + kTimeValueCursorOffset);
			ImGui::Text("%.5f", currentStats.time * 1000.f);
		}
	}

	static void DrawTimerTable(const std::deque<ProfilerStats>& stats, bool drawUntracked = true)
	{
		static const float kTimeValueCursorOffset = (60.f - ImGui::CalcTextSize("9.99999").x) / 2.f;

		if (stats.empty())
			return;

		std::string tableId = "##StatsTimingsTable";
		ImGuiTableFlags flags = ImGuiTableFlags_Borders | ImGuiTableFlags_ScrollX | ImGuiTableFlags_ScrollY | ImGuiTableFlags_SizingStretchSame | ImGuiTableFlags_RowBg;
		if (ImGui::BeginTable(tableId.c_str(), 2, flags))
		{
			ImGui::TableSetupColumn("Name", ImGuiTableColumnFlags_WidthStretch);
			ImGui::TableSetupColumn("Time (ms)", ImGuiTableColumnFlags_WidthFixed, 60);
			ImGui::TableHeadersRow();
			
			ImGui::TableNextRow();
			ImGui::TableNextColumn();

			const ProfilerStats& rootStat = stats.front();
			std::string rootStatId = rootStat.id.GetName();
			rootStatId += "##Stats";

			ImGui::TreeNodeEx(rootStatId.c_str(), ImGuiTreeNodeFlags_NoTreePushOnOpen | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_SpanAllColumns);
			ImGui::TableNextColumn();
			ImGui::SetCursorPosX(ImGui::GetCursorPosX() + kTimeValueCursorOffset);
			ImGui::Text("%.5f", rootStat.time * 1000.f);
			ImGui::TableNextRow();

			for (const auto& child : rootStat.children)
				DrawTableEntry(stats, child);

			ImGui::TableNextRow();
			ImGui::TableNextColumn();

			if (drawUntracked)
			{
				const ProfilerStats& untrackedStat = stats.back();
				std::string untrackedStatId = untrackedStat.id.GetName();
				untrackedStatId += "##Stats";

				ImGui::TableNextRow();
				ImGui::TableNextColumn();

				ImGui::TreeNodeEx(untrackedStatId.c_str(), ImGuiTreeNodeFlags_NoTreePushOnOpen | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_SpanAllColumns);
				ImGui::TableNextColumn();
				ImGui::SetCursorPosX(ImGui::GetCursorPosX() + kTimeValueCursorOffset);
				ImGui::Text("%.5f", untrackedStat.time * 1000.f);
				ImGui::TableNextRow();
			}

			ImGui::EndTable();
		}
	}

	static void DrawStats(const bool showStats, const ImVec2& topLeft, const ImVec2& size, SimulationContext* simulationContext)
	{
		static CircularArray<StatsTimeGraphEntry, 240> sGraphArray;

		if (showStats)
		{
			const FramePacket& framePacket = simulationContext->GetFramePacket();

			RenderTime& renderTime = RenderTime::GetInstance();
			const std::deque<ProfilerStats> renderThreadStats = renderTime.GetCpuProfilerStats();
			const std::deque<ProfilerStats> gpuStats = renderTime.GetGpuProfilerStats();

			if (sGraphArray.Peek().frame != framePacket.simulationFrame)
			{
				StatsTimeGraphEntry entry;
				entry.frame = static_cast<double>(framePacket.simulationFrame);
				entry.gameThread = framePacket.cpuProfilerStats.front().time * 1000.f;
				entry.renderThread = renderThreadStats.front().time * 1000.f;
				entry.gpu = gpuStats.front().time * 1000.f;
				sGraphArray.Push(entry);
			}

			constexpr ImGuiWindowFlags kWindowFlags = ImGuiWindowFlags_NoNav | ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoDocking;
			const ImVec2 kStatsWindowSize = ImVec2(size.x * 0.5f, size.y);

			ImGui::PushStyleColor(ImGuiCol_ChildBg, ImVec4(0.25f, 0.25f, 0.25f, 0.75f));
			ImGui::SetNextWindowPos(ImVec2(topLeft.x + size.x - kStatsWindowSize.x, topLeft.y));

			if (ImGui::BeginChild("Stats##Window", kStatsWindowSize, ImGuiChildFlags_Border, kWindowFlags))
			{
				uint32_t fps = static_cast<uint32_t>(std::roundf(1.f / renderTime.GetSmoothDelta()));
				ImGui::Text("FPS: %d", fps);

				GfxDevice& gfxDevice = GfxDevice::GetInstance();
				std::string vramUsage = "VRAM Usage: " + ByteSizeToShortString(gfxDevice.GetVramUsed());
				const float vramUsageTextWidth = ImGui::CalcTextSize(vramUsage.c_str()).x;
				ImGui::SameLine(ImGui::GetContentRegionAvail().x - vramUsageTextWidth + ImGui::GetStyle().ItemSpacing.x);
				ImGui::Text(vramUsage.c_str());

				HeapTracker& heapTracker = HeapTracker::GetInstance();
				ImGui::Text("Heap Allocations: %d", heapTracker.GetAllocationsCount());

				std::string heapUsage = "Heap Usage: " + ByteSizeToShortString(heapTracker.GetHeapSize());
				const float HeapUsageTextWidth = ImGui::CalcTextSize(heapUsage.c_str()).x;
				ImGui::SameLine(ImGui::GetContentRegionAvail().x - HeapUsageTextWidth + ImGui::GetStyle().ItemSpacing.x);
				ImGui::Text(heapUsage.c_str());

				if (ImPlot::BeginPlot("##Scrolling", ImVec2(-1, 150), ImPlotFlags_NoFrame | ImPlotFlags_NoMouseText | ImPlotFlags_NoMenus))
				{
					const auto& data = sGraphArray.GetData();
					ImPlot::SetupLegend(ImPlotLocation_NorthEast, ImPlotLegendFlags_Horizontal);
					ImPlot::SetupAxes(nullptr, nullptr, ImPlotAxisFlags_NoTickLabels | ImPlotAxisFlags_NoHighlight | ImPlotAxisFlags_NoMenus, ImPlotAxisFlags_NoHighlight | ImPlotAxisFlags_NoMenus | ImPlotAxisFlags_AutoFit);
					ImPlot::SetupAxisLimits(ImAxis_X1, static_cast<double>(framePacket.simulationFrame) - sGraphArray.Size(), static_cast<double>(framePacket.simulationFrame), ImGuiCond_Always);

					ImPlot::SetNextFillStyle(IMPLOT_AUTO_COL, 0.5f);
					ImPlot::PlotShaded("GT", &data[0].frame, &data[0].gameThread, sGraphArray.Size(), -INFINITY, 0, sGraphArray.GetIndex(), sizeof(StatsTimeGraphEntry));

					ImPlot::SetNextFillStyle(IMPLOT_AUTO_COL, 0.5f);
					ImPlot::PlotShaded("RT", &data[0].frame, &data[0].renderThread, sGraphArray.Size(), -INFINITY, 0, sGraphArray.GetIndex(), sizeof(StatsTimeGraphEntry));

					ImPlot::SetNextFillStyle(IMPLOT_AUTO_COL, 0.5f);
					ImPlot::PlotShaded("GPU", &data[0].frame, &data[0].gpu, sGraphArray.Size(), -INFINITY, 0, sGraphArray.GetIndex(), sizeof(StatsTimeGraphEntry));
					ImPlot::EndPlot();
				}

				if (ImGui::BeginTabBar("##StatsTabBar", ImGuiTabBarFlags_None))
				{
					std::string simulationTabId = "Game Thread##Stats";
					if (ImGui::BeginTabItem(simulationTabId.c_str()))
					{
						DrawTimerTable(framePacket.cpuProfilerStats);
						ImGui::EndTabItem();
					}

					std::string rendergrphCpuTabId = "Render Thread##Stats";
					if (ImGui::BeginTabItem(rendergrphCpuTabId.c_str()))
					{
						DrawTimerTable(renderThreadStats);
						ImGui::EndTabItem();
					}

					std::string rendergrphGpuTabId = "GPU##Stats";
					if (ImGui::BeginTabItem(rendergrphGpuTabId.c_str()))
					{
						DrawTimerTable(gpuStats, false);
						ImGui::EndTabItem();
					}
					ImGui::EndTabBar();
				}
			}

			ImGui::EndChild();
			ImGui::PopStyleColor();
		}
	}
}
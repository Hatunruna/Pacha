#pragma once
#include "graphics/ui/EditorWidget.h"
#include "graphics/ui/utilities/EditorEnums.h"
#include "graphics/utilities/textures/TextureParser.h"

#include <filesystem>

namespace Pacha
{
	class TextureImporter : public EditorWidget
	{
	public:
		void Render(SimulationContext* simulationContext) override;

		static bool ImportFromFile(const std::filesystem::path& source, const std::filesystem::path& destination, const bool generateMips, const bool srgb, std::string& errorMessage);
		static bool ImportFromMemory(uint8_t* source, size_t sourceDataSize, const std::filesystem::path& textureExtension, const std::filesystem::path& textureAssetPath, const bool generateMips, const bool srgb, std::string& errorMessage);
		static bool IsSupportedImageFormat(const std::filesystem::path& texturePath);

	private:
		static std::unique_ptr<TextureParser> GetTextureParser(const std::filesystem::path& texturePath, uint8_t* source = nullptr, size_t sourceDataSize = 0);

		bool m_ShowErrorModal = false;
		std::string m_ModalErrorMessage = {};

		bool m_IsSRGB = true;
		bool m_GenerateMipMaps = true;
		std::string m_SourcePath = {};
		std::string m_DestinationPath = {};
	};
}
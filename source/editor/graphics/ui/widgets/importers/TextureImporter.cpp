#include "TextureImporter.h"
#include "core/ProjectConstants.h"
#include "assets/AssetManager.h"
#include "graphics/ui/ImGuiString.h"
#include "graphics/device/GfxDevice.h"
#include "assets/utilities/Ktx2Writer.h"
#include "assets/utilities/Ktx2FormatHelpers.h"
#include "graphics/utilities/GfxMemoryManager.h"
#include "graphics/resources/texture/GfxTexture.h"
#include "graphics/utilities/textures/StbParser.h"
#include "graphics/resources/buffer/GfxReadbackBuffer.h"

namespace Pacha
{
	void TextureImporter::Render(SimulationContext*)
	{
		bool removeWidget = false;
		ImGui::OpenPopup("Texture Importer");
		ImGui::SetNextWindowSize(ImVec2(400, 200), ImGuiCond_FirstUseEver);
		if (ImGui::BeginPopupModal("Texture Importer", nullptr, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar))
		{
			ImGui::Text("Source Texture");
			ImGui::SetNextItemWidth(ImGui::GetWindowWidth() - 50);
			ImGui::InputText("##TISourcePath", &m_SourcePath, ImGuiInputTextFlags_ReadOnly);
			ImGui::SameLine();

			if (ImGui::Button(ICON_FA_FOLDER_OPEN "##TISourcePathButton", ImVec2(-1, 0)))
			{
				const std::vector<std::string> filter = {"Textures (*.jpg *.jpeg *.png *.bmp *.psd *.tga *.gif *.hdr *.pic *.ppm *.pgm)", "*.jpg *.jpeg *.png *.bmp *.psd *.tga *.gif *.hdr *.pic *.ppm *.pgm"};
				std::vector<std::string> textureSelection = pfd::open_file("Select a texture", pfd::path::home(), filter).result();
				if (!textureSelection.empty())
					m_SourcePath = textureSelection[0];
			}

			ImGui::Text("Destination Folder");
			ImGui::SetNextItemWidth(ImGui::GetWindowWidth() - 50);
			ImGui::InputText("##TIDestinationPath", &m_DestinationPath, ImGuiInputTextFlags_ReadOnly);
			ImGui::SameLine();

			if (ImGui::Button(ICON_FA_FOLDER_OPEN "##TIDestinationPathButton", ImVec2(-1, 0)))
			{
				const std::string assetsPath = ProjectConstants::GetInstance().GetAssetsPath().string();
				std::string destinationFolder = pfd::select_folder("Select destination folder", assetsPath, pfd::opt::force_path).result();
				if (!destinationFolder.empty())
				{
					if (std::filesystem::proximate(destinationFolder, assetsPath) != destinationFolder)
						m_DestinationPath = destinationFolder;
				}
			}

			ImGui::Checkbox("Is SRGB ##TISRGB", &m_IsSRGB);
			ImGui::Checkbox("Generate MipMaps##TIGenMips", &m_GenerateMipMaps);

			const bool disabled = m_SourcePath.empty() || m_DestinationPath.empty() || m_SourcePath == m_DestinationPath;
			ImGui::BeginDisabled(disabled);
			const bool import = ImGui::Button("Import##MIImportButton");
			ImGui::EndDisabled();

			if (import)
			{
				try
				{
					if (ImportFromFile(m_SourcePath, m_DestinationPath, m_GenerateMipMaps, m_IsSRGB, m_ModalErrorMessage))
						removeWidget = true;
					else
						m_ShowErrorModal = true;
				}
				catch (const std::exception& error)
				{
					m_ModalErrorMessage = "Failed to import texture, see console for more details.";
					m_ShowErrorModal = true;
					LOGERROR(error.what());
				}
			}

			ImGui::SameLine(ImGui::GetWindowWidth() - 50);
			if (ImGui::Button("Close##TICloseButton"))
			{
				ImGui::CloseCurrentPopup();
				removeWidget = true;
			}

			if (m_ShowErrorModal)
			{
				ImGui::OpenPopup("Texture Importer Error");
				ImGui::SetNextWindowSize(ImVec2(300, 100), ImGuiCond_FirstUseEver);
				if (ImGui::BeginPopupModal("Texture Importer Error", &m_ShowErrorModal, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar))
				{
					ImGui::TextWrappedUnformatted(m_ModalErrorMessage.c_str());

					ImGui::SetCursorPos(ImVec2(125, 75));
					if (ImGui::Button("Close##TIErrorClose", ImVec2(50, 20)))
					{
						ImGui::CloseCurrentPopup();
						m_ShowErrorModal = false;
					}
					ImGui::EndPopup();
				}
			}

			ImGui::EndPopup();
		}

		AutoRemove(removeWidget);
	}

	bool TextureImporter::ImportFromFile(const std::filesystem::path& source, const std::filesystem::path& destination, const bool generateMips, const bool srgb, std::string& errorMessage)
	{
		if (IsSupportedImageFormat(source))
		{
			std::unique_ptr<TextureParser> parser = GetTextureParser(source);

			const std::filesystem::path& assetsPath = ProjectConstants::GetInstance().GetAssetsPath();
			const std::filesystem::path libraryPath = ProjectConstants::GetInstance().GetLibraryPath();

			if (parser->Initialize() && parser->ProcessTexture())
			{
				uint8_t* sourceData = parser->GetData();
				GfxTextureDescriptor descriptor = parser->GetDescriptor();

				if (srgb)
					descriptor.format = GetSRGB(descriptor.format);

				if (generateMips)
				{
					size_t mip0DataSize = GfxTextureBase::CalculateTextureDataSize(descriptor);

					uint32_t mipsW = static_cast<uint32_t>(log2(descriptor.width)) + 1u;
					uint32_t mipsH = static_cast<uint32_t>(log2(descriptor.height)) + 1u;

					descriptor.mips = std::min(mipsW, mipsH);
					descriptor.flags = GfxTextureFlags_BIND_AS_RENDER_TARGET | GfxTextureFlags_COPY_SOURCE | GfxTextureFlags_COPY_DESTINATION | GfxTextureFlags_RENDER_INTO_SUB_RESOURCES;

					size_t fullTextureDataSize = GfxTextureBase::CalculateTextureDataSize(descriptor);
					std::shared_ptr<uint8_t> tempData = std::shared_ptr<uint8_t>(new uint8_t[fullTextureDataSize]);
					memcpy(tempData.get(), sourceData, mip0DataSize);

					GfxTexture* uploadTexture = new GfxTexture(descriptor, tempData, GfxResourceState::COPY_DESTINATION);
					GfxReadbackBuffer* readbackBuffer = new GfxReadbackBuffer(GfxTextureBase::CalculateTextureDataSize(descriptor));

					GfxCommandBuffer mipGenCommandBuffer;
					mipGenCommandBuffer.BeginCommandBuffer();

					for (uint32_t slice = 0; slice < descriptor.slices; ++slice)
					{
						mipGenCommandBuffer.TransitionTextureSubresource(uploadTexture, 0, slice, GfxResourceState::COPY_DESTINATION, GfxResourceState::COPY_SOURCE);
						for (uint32_t mip = 1; mip < descriptor.mips; ++mip)
						{
							mipGenCommandBuffer.BlitTextureSubresources(uploadTexture, mip - 1, slice, uploadTexture, mip, slice);
							mipGenCommandBuffer.TransitionTextureSubresource(uploadTexture, mip, slice, GfxResourceState::COPY_DESTINATION, GfxResourceState::COPY_SOURCE);
						}
					}

					mipGenCommandBuffer.CopyTextureToBuffer(uploadTexture, readbackBuffer);
					mipGenCommandBuffer.EndCommandBuffer();

					GfxDevice& gfxDevice = GfxDevice::GetInstance();
					GfxMemoryManager& memoryManager = GfxMemoryManager::GetInstance();

					GfxCommandBuffer* uploadCommandBuffer = memoryManager.GetUploadCommandBuffer();
					gfxDevice.SubmitAndWait({ uploadCommandBuffer, &mipGenCommandBuffer });

					uint8_t* newData = new uint8_t[readbackBuffer->GetSize()];
					memcpy(newData, readbackBuffer->GetData(), readbackBuffer->GetSize());
					sourceData = newData;

					delete uploadTexture;
					delete readbackBuffer;
				}

				const std::filesystem::path dstWFileName = destination / source.filename();
				const std::filesystem::path relativePath = std::filesystem::relative(dstWFileName, assetsPath);
				std::filesystem::copy_file(source, dstWFileName, std::filesystem::copy_options::update_existing);

				AssetManager& assetManager = AssetManager::GetInstance();
				TextureAsset* textureAsset = assetManager.GetOrCreate<TextureAsset>(relativePath);

				const std::filesystem::path libTexturePath = libraryPath / (textureAsset->GetUUID().GetString() + ".ktx2");

				Ktx2Writer ktx2Writer(libTexturePath);
				if (!ktx2Writer.Write(descriptor, sourceData))
				{
					errorMessage = "Failed to write ktx2 file, see console for more details.";
					return false;
				}

				if (generateMips)
					delete[] sourceData;

				return true;
			}

			switch (parser->GetError())
			{
				case TextureParserError::NONE:
					break;
				case TextureParserError::FAILED_TO_LOAD:
					errorMessage = "Failed to load texture, see console for more details.";
					break;
				case TextureParserError::FAILED_TO_PROCESS:
					errorMessage = "Failed to process texture, see console for more details.";
					break;
			}

			return false;
		}

		errorMessage = "Failed to import texture, it is not a supported format";
		return false;
	}

	bool TextureImporter::ImportFromMemory(uint8_t* source, size_t sourceDataSize, const std::filesystem::path& textureExtension, const std::filesystem::path& assetLookupPath, const bool generateMips, const bool srgb, std::string& errorMessage)
	{
		if (IsSupportedImageFormat(textureExtension))
		{
			std::unique_ptr<TextureParser> parser = GetTextureParser(textureExtension, source, sourceDataSize);
			const std::filesystem::path libraryPath = ProjectConstants::GetInstance().GetLibraryPath();

			if (parser->Initialize() && parser->ProcessTexture())
			{
				uint8_t* sourceData = parser->GetData();
				GfxTextureDescriptor descriptor = parser->GetDescriptor();

				if (srgb)
					descriptor.format = GetSRGB(descriptor.format);

				if (generateMips)
				{
					size_t mip0DataSize = GfxTextureBase::CalculateTextureDataSize(descriptor);

					descriptor.mips = static_cast<uint32_t>(log2(descriptor.width)) + 1u;
					descriptor.flags = GfxTextureFlags_BIND_AS_RENDER_TARGET | GfxTextureFlags_COPY_SOURCE | GfxTextureFlags_COPY_DESTINATION | GfxTextureFlags_RENDER_INTO_SUB_RESOURCES;

					size_t fullTextureDataSize = GfxTextureBase::CalculateTextureDataSize(descriptor);
					std::shared_ptr<uint8_t> tempData = std::shared_ptr<uint8_t>(new uint8_t[fullTextureDataSize]);
					memcpy(tempData.get(), sourceData, mip0DataSize);

					GfxTexture* uploadTexture = new GfxTexture(descriptor, tempData, GfxResourceState::COPY_DESTINATION);
					GfxReadbackBuffer* readbackBuffer = new GfxReadbackBuffer(GfxTextureBase::CalculateTextureDataSize(descriptor));

					GfxCommandBuffer mipGenCommandBuffer;
					mipGenCommandBuffer.BeginCommandBuffer();

					for (uint32_t slice = 0; slice < descriptor.slices; ++slice)
					{
						mipGenCommandBuffer.TransitionTextureSubresource(uploadTexture, 0, slice, GfxResourceState::COPY_DESTINATION, GfxResourceState::COPY_SOURCE);
						for (uint32_t mip = 1; mip < descriptor.mips; ++mip)
						{
							mipGenCommandBuffer.BlitTextureSubresources(uploadTexture, mip - 1, slice, uploadTexture, mip, slice);
							mipGenCommandBuffer.TransitionTextureSubresource(uploadTexture, mip, slice, GfxResourceState::COPY_DESTINATION, GfxResourceState::COPY_SOURCE);
						}
					}

					mipGenCommandBuffer.CopyTextureToBuffer(uploadTexture, readbackBuffer);
					mipGenCommandBuffer.EndCommandBuffer();

					GfxDevice& gfxDevice = GfxDevice::GetInstance();
					GfxMemoryManager& memoryManager = GfxMemoryManager::GetInstance();

					GfxCommandBuffer* uploadCommandBuffer = memoryManager.GetUploadCommandBuffer();
					gfxDevice.SubmitAndWait({ uploadCommandBuffer, &mipGenCommandBuffer });

					uint8_t* newData = new uint8_t[readbackBuffer->GetSize()];
					memcpy(newData, readbackBuffer->GetData(), readbackBuffer->GetSize());
					sourceData = newData;

					delete uploadTexture;
					delete readbackBuffer;
				}

				AssetManager& assetManager = AssetManager::GetInstance();
				TextureAsset* textureAsset = assetManager.GetOrCreate<TextureAsset>(assetLookupPath);

				const std::filesystem::path libTexturePath = libraryPath / (textureAsset->GetUUID().GetString() + ".ktx2");

				Ktx2Writer ktx2Writer(libTexturePath);
				if (!ktx2Writer.Write(descriptor, sourceData))
				{
					errorMessage = "Failed to write ktx2 file, see console for more details.";
					return false;
				}

				if (generateMips)
					delete[] sourceData;

				return true;
			}

			switch (parser->GetError())
			{
				case TextureParserError::NONE:
					break;
				case TextureParserError::FAILED_TO_LOAD:
					errorMessage = "Failed to load texture, see console for more details.";
					break;
				case TextureParserError::FAILED_TO_PROCESS:
					errorMessage = "Failed to process texture, see console for more details.";
					break;
			}

			return false;
		}

		errorMessage = "Failed to import texture, it is not a supported format";
		return false;
	}

	bool TextureImporter::IsSupportedImageFormat(const std::filesystem::path& texturePath)
	{
		std::string lowerCaseExtension = texturePath.extension().string();
		std::transform(lowerCaseExtension.begin(), lowerCaseExtension.end(), lowerCaseExtension.begin(), ::tolower);

		switch (FNV1aHash(lowerCaseExtension))
		{
			case FNV1aHash(".jpg"):
			case FNV1aHash(".jpeg"):
			case FNV1aHash(".png"):
			case FNV1aHash(".bmp"):
			case FNV1aHash(".psd"):
			case FNV1aHash(".tga"):
			case FNV1aHash(".gif"):
			case FNV1aHash(".hdr"):
			case FNV1aHash(".pic"):
			case FNV1aHash(".ppm"):
			case FNV1aHash(".pgm"):
				return true;
		}

		return false;
	}

	std::unique_ptr<TextureParser> TextureImporter::GetTextureParser(const std::filesystem::path& texturePath, uint8_t* source, size_t sourceDataSize)
	{
		std::string lowerCaseExtension = texturePath.extension().string();
		std::transform(lowerCaseExtension.begin(), lowerCaseExtension.end(), lowerCaseExtension.begin(), ::tolower);

		switch (FNV1aHash(lowerCaseExtension))
		{
			case FNV1aHash(".jpg"):
			case FNV1aHash(".jpeg"):
			case FNV1aHash(".png"):
			case FNV1aHash(".bmp"):
			case FNV1aHash(".psd"):
			case FNV1aHash(".tga"):
			case FNV1aHash(".gif"):
			case FNV1aHash(".hdr"):
			case FNV1aHash(".pic"):
			case FNV1aHash(".ppm"):
			case FNV1aHash(".pgm"):
				return source ? std::make_unique<StbParser>(source, sourceDataSize) : std::make_unique<StbParser>(texturePath);
		}

		return nullptr;
	}
}
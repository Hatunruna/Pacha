#include "ModelImporter.h"
#include "TextureImporter.h"
#include "graphics/ui/ImGuiString.h"
#include "serialization/JsonSerializer.h"
#include "graphics/ui/utilities/EditorEnums.h"
#include "graphics/utilities/models/GltfParser.h"

#include <fstream>

namespace Pacha
{
	void ModelImporter::Render(SimulationContext*)
	{
		bool removeWidget = false;
		ImGui::OpenPopup("Model Importer");
		ImGui::SetNextWindowSize(ImVec2(400, 155), ImGuiCond_FirstUseEver);
		if (ImGui::BeginPopupModal("Model Importer", nullptr, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar))
		{
			ImGui::Text("Source Model");
			ImGui::SetNextItemWidth(ImGui::GetWindowWidth() - 50);
			ImGui::InputText("##MISourcePath", &m_SourcePath, ImGuiInputTextFlags_ReadOnly);
			ImGui::SameLine();

			if (ImGui::Button(ICON_FA_FOLDER_OPEN "##MISourcePathButton", ImVec2(-1, 0)))
			{
				const std::vector<std::string> filter = {"Models (*.glb *.gltf)", "*.glb *.gltf"};
				std::vector<std::string> modelSelection = pfd::open_file("Select a model", pfd::path::home(), filter).result();
				if (!modelSelection.empty())
					m_SourcePath = modelSelection[0];
			}

			ImGui::Text("Destination Folder");
			ImGui::SetNextItemWidth(ImGui::GetWindowWidth() - 50);
			ImGui::InputText("##MIDestinationPath", &m_DestinationPath, ImGuiInputTextFlags_ReadOnly);
			ImGui::SameLine();

			if (ImGui::Button(ICON_FA_FOLDER_OPEN "##MIDestinationPathButton", ImVec2(-1, 0)))
			{
				const std::string assetsPath = ProjectConstants::GetInstance().GetAssetsPath().string();
				std::string destinationFolder = pfd::select_folder("Select destination folder", assetsPath, pfd::opt::force_path).result();
				if (!destinationFolder.empty())
				{
					if (std::filesystem::proximate(destinationFolder, assetsPath) != destinationFolder)
						m_DestinationPath = destinationFolder;
				}
			}

			ImGui::Checkbox("Compressed Vertex Data", &m_UseCompressedVertexData);

			const bool disabled = m_SourcePath.empty() || m_DestinationPath.empty() || m_SourcePath == m_DestinationPath;
			ImGui::BeginDisabled(disabled);
			const bool import = ImGui::Button("Import##MIImportButton");
			ImGui::EndDisabled();

			if (import)
			{
				try
				{
					if (ImportFromFile(m_SourcePath, m_DestinationPath, m_UseCompressedVertexData, m_ModalErrorMessage))
						removeWidget = true;
					else
						m_ShowErrorModal = true;
				}
				catch (const std::exception& error)
				{
					m_ModalErrorMessage = "Failed to import model, see console for more details.";
					m_ShowErrorModal = true;
					LOGERROR(error.what());
				}
			}

			ImGui::SameLine(ImGui::GetWindowWidth() - 50);
			if (ImGui::Button("Close##MICloseButton"))
			{
				ImGui::CloseCurrentPopup();
				removeWidget = true;
			}

			if (m_ShowErrorModal)
			{
				ImGui::OpenPopup("Model Importer Error");
				ImGui::SetNextWindowSize(ImVec2(300, 100), ImGuiCond_FirstUseEver);
				if (ImGui::BeginPopupModal("Model Importer Error", &m_ShowErrorModal, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar))
				{
					ImGui::TextWrappedUnformatted(m_ModalErrorMessage.c_str());

					ImGui::SetCursorPos(ImVec2(125, 75));
					if (ImGui::Button("Close##MIErrorClose", ImVec2(50, 20)))
					{
						ImGui::CloseCurrentPopup();
						m_ShowErrorModal = false;
					}
					ImGui::EndPopup();
				}
			}

			ImGui::EndPopup();
		}

		AutoRemove(removeWidget);
	}

	bool ModelImporter::ImportFromFile(const std::filesystem::path& source, const std::filesystem::path& destination, const bool useCompressedVertexData, std::string& errorMessage)
	{
		if (IsSupportedFormat(source))
		{
			const std::filesystem::path& assetsPath = ProjectConstants::GetInstance().GetAssetsPath();
			const std::filesystem::path libraryPath = ProjectConstants::GetInstance().GetLibraryPath();
			std::unique_ptr<ModelParser> parser = GetModelParser(source, useCompressedVertexData);

			if (parser->Initialize() && parser->ProcessModel(destination))
			{
				AssetManager& assetManager = AssetManager::GetInstance();
				const std::filesystem::path modelDestination = destination / parser->GetPath().filename();

				for (auto& dependency : parser->GetDependencies())
				{
					if (dependency.type & ModelDependencyType_TEXTURE)
					{
						if (TextureImporter::IsSupportedImageFormat(dependency.path))
						{
							const bool isSRGB = dependency.type == ModelDependencyType_COLOR_TEXTURE;
							if (!TextureImporter::ImportFromFile(dependency.path, destination, true, isSRGB, errorMessage))
								return false;
						}
					}
					else
					{
						std::filesystem::copy_file(dependency.path, destination / dependency.path.filename(), std::filesystem::copy_options::update_existing);
					}
				}

				std::filesystem::path relativePath = std::filesystem::relative(modelDestination, assetsPath);
				std::filesystem::copy_file(parser->GetPath(), modelDestination, std::filesystem::copy_options::update_existing);

				ModelAsset* modelAsset = assetManager.GetOrCreate<ModelAsset>(relativePath);
				const std::filesystem::path libModelPath = libraryPath / (modelAsset->GetUUID().GetString() + ".pmdl");

				const MeshData& indexData = parser->GetIndexData();
				const MeshData& vertexData = parser->GetVertexData();
				PACHA_ASSERT(vertexData.data, "Vertex data should never be null");

				GfxModelData modelData = {};
				modelData.nodes = parser->GetModelNodes();
				modelData.meshes = parser->GetModelMeshes();
				modelData.materials = parser->GetModelMaterials();
				modelData.indexType = parser->GetIndexType();
				modelData.indexDataSize = indexData.size;
				modelData.vertexDataSize = vertexData.size;
				modelData.interleavedDataOffset = parser->GetInterleavedDataOffset();
				modelData.alphaPositionDataOffset = parser->GetAlphaPositionDataOffset();
				modelData.alphaInterleavedDataOffset = parser->GetAlphaInterleavedDataOffset();
				
				JsonSerializer serializer;
				serializer.StartSerialization();
				serializer(modelData);
				serializer.EndSerialization();
				
				const std::string modelDefinition = serializer.GetJson();
				const size_t jsonSize = modelDefinition.size();

				std::ofstream processedModel(libModelPath, std::ios::binary);
				processedModel.write(reinterpret_cast<const char*>(&jsonSize), sizeof(size_t));
				processedModel.write(modelDefinition.c_str(), modelDefinition.size());
				processedModel.write(reinterpret_cast<char*>(vertexData.data), vertexData.size);

				if (indexData.data)
					processedModel.write(reinterpret_cast<char*>(indexData.data), indexData.size);

				processedModel.close();
				return true;
			}

			switch (parser->GetError())
			{
				case ModelParserError::NONE:
					break;
				case ModelParserError::FAILED_TO_LOAD:
					errorMessage = "Failed to load model, see console for more details.";
					break;
				case ModelParserError::FAILED_TO_PROCESS:
					errorMessage = "Failed to process model, see console for more details.";
					break;
			}

			return false;
		}

		errorMessage = "Failed to import model, it is not a supported model format.";
		return false;
	}

	bool ModelImporter::IsSupportedFormat(const std::filesystem::path& modelPath)
	{
		std::string lowerCaseExtension = modelPath.extension().string();
		std::transform(lowerCaseExtension.begin(), lowerCaseExtension.end(), lowerCaseExtension.begin(), ::tolower);

		switch (FNV1aHash(lowerCaseExtension))
		{
			case FNV1aHash(".gltf"):
			case FNV1aHash(".glb"):
				return true;
		}

		return false;
	}

	std::unique_ptr<ModelParser> ModelImporter::GetModelParser(const std::filesystem::path& modelPath, const bool useCompressedVertexData)
	{
		std::string lowerCaseExtension = modelPath.extension().string();
		std::transform(lowerCaseExtension.begin(), lowerCaseExtension.end(), lowerCaseExtension.begin(), ::tolower);

		switch (FNV1aHash(lowerCaseExtension))
		{
			case FNV1aHash(".gltf"):
			case FNV1aHash(".glb"):
				return std::make_unique<GltfParser>(modelPath, useCompressedVertexData);
		}

		return nullptr;
	}
}
#pragma once
#include "graphics/ui/EditorWidget.h"
#include "graphics/utilities/models/ModelParser.h"
#include "graphics/resources/mesh/GfxVertexLayout.h"


namespace Pacha
{
	class ModelImporter : public EditorWidget
	{
	public:
		void Render(SimulationContext* simulationContext) override;
		static bool ImportFromFile(const std::filesystem::path& source, const std::filesystem::path& destination, const bool useCompressedVertexData, std::string& errorMessage);
		static bool IsSupportedFormat(const std::filesystem::path& modelPath);

	private:
		static std::unique_ptr<ModelParser> GetModelParser(const std::filesystem::path& modelPath, const bool useCompressedVertexData);

		bool m_ShowErrorModal = false;
		std::string m_ModalErrorMessage = {};

		std::string m_SourcePath = {};
		std::string m_DestinationPath = {};
		bool m_UseCompressedVertexData = true;
	};
}
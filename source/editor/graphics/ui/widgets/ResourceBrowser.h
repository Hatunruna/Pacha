#pragma once
#include "utilities/DirectoryWatcher.h"
#include "utilities/DirectoryListener.h"

#include <string>

namespace Pacha
{
	class ResourceBrowser : public DirectoryListener
	{
	public:
		ResourceBrowser();
		void Draw(bool& showWindow);

	private:
		std::string m_SearchString = {};
		std::vector<DirectoryNode*> m_SearchResults;
		
		DirectoryWatcher m_DirectoryWatcher;
		bool m_DirectorySelectionChangedInDirectoryView = false;

		DirectoryNode* m_SelectedDirectory = nullptr;
		std::filesystem::path m_SelectedDirectoryRelativePath = {};

		void DrawTreeViewRecursive(const DirectoryNode& root, const std::unordered_set<DirectoryNode*>& parents);
		void DrawDirectoryView();
		void DrawSearchResults();

		bool HandleDirectoryViewSelection(const DirectoryNode& node);
		void SetSelectedDirectory(DirectoryNode* directory);

		void OnDirectoryRemoved(const DirectoryNode* node) override;
		void OnDirectoryMoved(const DirectoryNode* node, const std::filesystem::path&) override;
	};
}
#pragma once
#include "graphics/ui/EditorWidget.h"
#include "graphics/ui/widgets/GameView.h"
#include "graphics/ui/widgets/LevelView.h"
#include "graphics/ui/widgets/ConsoleLog.h"
#include "graphics/ui/widgets/LevelExplorer.h"
#include "graphics/ui/widgets/EntityInspector.h"
#include "graphics/ui/widgets/ResourceBrowser.h"

#include <memory>

namespace Pacha
{
	class EditorMainView : public EditorWidget
	{
	public:
		EditorMainView();
		~EditorMainView();

		glm::uvec2 GetGameViewResolution();
		glm::uvec2 GetLevelViewResolution();

	private:
		void Render(SimulationContext* simulationContext) override;

		void DrawMenuBar();
		void DrawToolBar();
		void DoDockArea(SimulationContext* simulationContext);

		void DrawFileMenu();
		void DrawEditMenu();
		void DrawEntityMenu();
		void DrawAssetMenu();
		void DrawWindowMenu();

		void NewLevel();
		void OpenLevel();
		void SaveLevel();
		void SaveLevelAs();

		void HandleKeyboardShortcuts();

		Level* m_EditorLevel = nullptr;
		bool ShowSaveLevelDialog(std::filesystem::path& outPath);

		bool m_ShowGameView = true;
		std::unique_ptr<GameView> m_GameView = nullptr;

		bool m_ShowLevelView = true;
		std::unique_ptr<LevelView> m_LevelView = nullptr;

		bool m_ShowConsoleLog = true;
		std::unique_ptr<ConsoleLog> m_ConsoleLog = nullptr;

		bool m_ShowLevelExplorer = true;
		std::unique_ptr<LevelExplorer> m_LevelExplorer = nullptr;

		bool m_ShowResourceBrowser = true;
		std::unique_ptr<ResourceBrowser> m_ResourceBrowser = nullptr;

		bool m_ShowEntityInspector = true;
		std::unique_ptr<EntityInspector> m_EntityInspector = nullptr;
	};
}
#pragma once

#include <imgui.h>
#include <string>
#include <vector>
#include <iostream>
#include <streambuf>

namespace Pacha
{
	class ConsoleLog : std::basic_streambuf<char>
	{
	public:
		ConsoleLog();
		~ConsoleLog();

		void Draw(bool& showWindow);

		void ClearLog();
		bool Incremented();
		const std::vector<std::string>& GetLogEntries() const;

	protected:
		int_type overflow(int_type character) override;
		std::streamsize xsputn(const char* text, std::streamsize characterCount) override;

	private:
		std::streambuf* m_OriginalCoutBuffer;
		std::string m_StringAccumulator;

		size_t m_EntriesSize = 0;
		size_t m_LastEntriesSize = 0;
		std::vector<std::string> m_LogEntries;

		ImGuiTextFilter m_LogFilter;
		ImGuiTextFilter m_LogCategoryFilter;

		bool m_FilterTrace = true;
		bool m_FilterWarning = true;
		bool m_FilterError = true;
		bool m_CategoryFilterUpdated = false;
	};
}
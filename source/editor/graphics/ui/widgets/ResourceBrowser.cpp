#include "ResourceBrowser.h"
#include "graphics/ui/ImGuiString.h"

#include <imgui.h>
#include <imgui_internal.h>

namespace Pacha
{
	const std::vector<std::string> kFilteredExtensions =
	{
		".uuid",
		".tmp"
	};

	ResourceBrowser::ResourceBrowser()
	{
		m_EventFilter = DirectoryEventFlags_TYPE_DIRECTORY | DirectoryEventFlags_MOVED | DirectoryEventFlags_REMOVED;
		m_DirectoryWatcher.Initialize(kFilteredExtensions);
		m_DirectoryWatcher.RegisterListener(this);
	}

	void ResourceBrowser::Draw(bool& showWindow)
	{
		m_DirectoryWatcher.ProcessEvents();

		if (!showWindow)
			return;

		if (ImGui::Begin("Resource Browser", &showWindow, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse))
		{
			if (ImGui::BeginTable("##ResourceBrowser", 2, ImGuiTableFlags_Resizable | ImGuiTableFlags_BordersInnerV))
			{
				ImGui::TableNextColumn();
				std::unordered_set<DirectoryNode*> parents;
				if (m_DirectorySelectionChangedInDirectoryView)
				{
					parents = m_SelectedDirectory->GatherParents();
					m_DirectorySelectionChangedInDirectoryView = false;
				}

				ImGui::BeginChild("ResourceBrowserTreeViewVerticalScroll", ImVec2(0, 0), false, ImGuiWindowFlags_HorizontalScrollbar);
				DrawTreeViewRecursive(m_DirectoryWatcher.GetRootNode(), parents);
				ImGui::EndChild();

				ImGui::TableNextColumn();
				if (ImGui::InputText("##ResourceBrowserSearchBox", &m_SearchString))
				{
					m_SearchResults.clear();

					std::string lowerSearchString = m_SearchString;
					std::transform(lowerSearchString.begin(), lowerSearchString.end(), lowerSearchString.begin(), ::tolower);

					m_DirectoryWatcher.GetRootNode().FindInChildrenRecursive(lowerSearchString, m_SearchResults);
				}

				ImGui::SameLine();
				if (ImGui::Button("Clear##ResourceBrowserClearButton"))
				{
					m_SearchString.clear();
					m_SearchResults.clear();
				}

				ImGui::BeginChild("ResourceBrowserDirectoryView");
				if (m_SearchString.empty())
					DrawDirectoryView();
				else
					DrawSearchResults();
				ImGui::EndChild();

				ImGui::EndTable();
			}
		}

		ImGui::End();
	}

	void ResourceBrowser::DrawTreeViewRecursive(const DirectoryNode& root, const std::unordered_set<DirectoryNode*>& parents)
	{
		for (auto& it : root.GetChildren())
		{
			if (it.GetType() == DirectoryNodeType::DIRECTORY)
			{
				std::string dirName = it.GetRelativePath().stem().string() + "##" + it.GetRelativePath().string();
				if (it.HasDirectoryChildNodes())
				{
					ImGuiTreeNodeFlags treeNodeFlags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_SpanAvailWidth;
					if (m_SelectedDirectory == &it) 
						treeNodeFlags |= ImGuiTreeNodeFlags_Selected;

					ImGuiWindow* window = ImGui::GetCurrentWindowRead();
					float min = window->Rect().Min.x + (ImGui::GetTreeNodeToLabelSpacing() * window->DC.TreeDepth);
					float max = window->Rect().Min.x + (ImGui::GetTreeNodeToLabelSpacing() * (window->DC.TreeDepth + 1));
					bool mouseOverArrow = ImGui::GetMousePos().x >= min && ImGui::GetMousePos().x <= max;

					if (parents.size())
						ImGui::SetNextItemOpen(parents.count(const_cast<DirectoryNode*>(&it)));

					bool isOpen = ImGui::TreeNodeEx(dirName.c_str(), treeNodeFlags);
					//ResourcesPopUpContext(it);

					if (!mouseOverArrow && ImGui::IsItemClicked())
						SetSelectedDirectory(const_cast<DirectoryNode*>(&it));

					if (isOpen)
					{
						DrawTreeViewRecursive(it, parents);
						ImGui::TreePop();
					}
				}
				else
				{
					ImGuiTreeNodeFlags treeNodeFlags = ImGuiTreeNodeFlags_SpanAvailWidth | ImGuiTreeNodeFlags_Leaf;
					
					if (m_SelectedDirectory == &it)
						treeNodeFlags |= ImGuiTreeNodeFlags_Selected;
					
					ImGui::TreeNodeEx(dirName.c_str(), treeNodeFlags);
					//ResourcesPopUpContext(it);

					if (ImGui::IsItemClicked())
						SetSelectedDirectory(const_cast<DirectoryNode*>(&it));

					ImGui::TreePop();
				}
			}
		}
	}

	void ResourceBrowser::DrawDirectoryView()
	{
		if (!m_DirectoryWatcher.HasPendingEvents() || !m_SelectedDirectory)
			return;

		std::string tableId = "##Table" + m_SelectedDirectory->GetRelativePath().string();
		if (ImGui::BeginTable(tableId.c_str(), 2, ImGuiTableFlags_ScrollX | ImGuiTableFlags_Sortable))
		{
			ImGui::TableSetupScrollFreeze(0, 1);

			ImGui::TableSetupColumn("Name");
			ImGui::TableSetupColumn("Type");
			ImGui::TableHeadersRow();

			for (auto& it : m_SelectedDirectory->GetChildren())
			{
				ImGui::TableNextRow();
				ImGui::TableNextColumn();

				std::string entryLabel = it.GetRelativePath().filename().string() + "##" + it.GetRelativePath().string();
				if (ImGui::Selectable(entryLabel.c_str(), false, ImGuiSelectableFlags_AllowDoubleClick))
				{
					if (HandleDirectoryViewSelection(it))
						break;
				}

				ImGui::TableNextColumn();
				ImGui::TextUnformatted(it.GetTypeAsString());
			}

			if (ImGuiTableSortSpecs* specs = ImGui::TableGetSortSpecs())
			{
				if (specs->SpecsDirty)
				{
					//Column index must be kept in synch with DirectorySortCriteria
					DirectorySortCriteria criteria = static_cast<DirectorySortCriteria>(specs->Specs[0].ColumnIndex);
					DirectorySortDirection direction = static_cast<DirectorySortDirection>(specs->Specs[0].SortDirection);
					m_SelectedDirectory->SortChildren(criteria, direction);
					specs->SpecsDirty = false;
				}
			}

			ImGui::EndTable();
		}
	}

	void ResourceBrowser::DrawSearchResults()
	{
		std::string tableId = "##ResourceBrowserSearchResultsTable";
		if (ImGui::BeginTable(tableId.c_str(), 3, ImGuiTableFlags_ScrollX))
		{
			ImGui::TableSetupScrollFreeze(0, 1);

			ImGui::TableSetupColumn("Name");
			ImGui::TableSetupColumn("Type");
			ImGui::TableSetupColumn("Path");
			ImGui::TableHeadersRow();

			for (auto& it : m_SearchResults)
			{
				ImGui::TableNextRow();
				ImGui::TableNextColumn();

				std::string entryLabel = it->GetRelativePath().filename().string() + "##" + it->GetRelativePath().string();
				if (ImGui::Selectable(entryLabel.c_str(), false, ImGuiSelectableFlags_AllowDoubleClick))
				{
					if (HandleDirectoryViewSelection(*it))
						break;
				}

				ImGui::TableNextColumn();
				ImGui::TextUnformatted(it->GetTypeAsString());

				ImGui::TableNextColumn();
				ImGui::TextUnformatted(it->GetRelativePath().string().c_str());
			}

			ImGui::EndTable();
		}
	}

	bool ResourceBrowser::HandleDirectoryViewSelection(const DirectoryNode& node)
	{
		if (ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left))
		{
			if (node.GetType() == DirectoryNodeType::DIRECTORY)
			{
				m_SearchString.clear();
				m_SearchResults.clear();
				m_DirectorySelectionChangedInDirectoryView = true;
				SetSelectedDirectory(const_cast<DirectoryNode*>(&node));
				return true;
			}
		}
		else
		{

		}

		return false;
	}

	void ResourceBrowser::SetSelectedDirectory(DirectoryNode* directory)
	{
		m_SelectedDirectory = directory;
		m_SelectedDirectoryRelativePath = directory->GetRelativePath();
	}

	void ResourceBrowser::OnDirectoryRemoved(const DirectoryNode* node)
	{
		if (node == m_SelectedDirectory)
		{
			m_SelectedDirectory = nullptr;
			m_SelectedDirectoryRelativePath.clear();
		}
	}
	
	void ResourceBrowser::OnDirectoryMoved(const DirectoryNode*, const std::filesystem::path& oldRelativePath)
	{
		if (oldRelativePath == m_SelectedDirectoryRelativePath)
		{
			m_SelectedDirectory = nullptr;
			m_SelectedDirectoryRelativePath.clear();
		}
	}
}

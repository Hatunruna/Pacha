#include "LevelView.h"
#include "utilities/Stats.h"
#include "utilities/RenderDoc.h"
#include "graphics/ui/UiManager.h"

#include <imgui.h>
#include <imgui_internal.h>

namespace Pacha
{
	void LevelView::Draw(bool& showWindow, SimulationContext* simulationContext)
	{
		if (!showWindow)
			return;

		if (ImGui::Begin("Level View", &showWindow, ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoCollapse))
		{
			m_Window = ImGui::GetCurrentWindow();

			if (ImGui::BeginMenuBar())
			{
				if (ImGui::BeginMenu("Render Mode"))
				{
					//if (ImGui::MenuItem("Standard", nullptr, m_EditorRenderPipeline->GetRenderMode() == RenderMode::STANDARD))
					//	m_EditorRenderPipeline->SetRenderMode(RenderMode::STANDARD);
					//
					//ImGui::Separator();
					//
					//if (ImGui::MenuItem("Albedo", nullptr, m_EditorRenderPipeline->GetRenderMode() == RenderMode::ALBEDO))
					//	m_EditorRenderPipeline->SetRenderMode(RenderMode::ALBEDO);
					//
					//if (ImGui::MenuItem("Normals", nullptr, m_EditorRenderPipeline->GetRenderMode() == RenderMode::NORMALS))
					//	m_EditorRenderPipeline->SetRenderMode(RenderMode::NORMALS);
					//
					//if (ImGui::MenuItem("Shadow Cascades", nullptr, m_EditorRenderPipeline->GetRenderMode() == RenderMode::SHADOW_CASCADES))
					//	m_EditorRenderPipeline->SetRenderMode(RenderMode::SHADOW_CASCADES);

					ImGui::EndMenu();
				}

				if (ImGui::BeginMenu("Gizmos"))
				{
					//if (ImGui::MenuItem("3D Gizmos", nullptr, m_EditorRenderPipeline->IsGizmoEnabled(EditorGizmos::_3D_GIZMOS)))
					//	m_EditorRenderPipeline->ToggleGizmo(EditorGizmos::_3D_GIZMOS);
					//
					//if (ImGui::MenuItem("Bounding Box", nullptr, m_EditorRenderPipeline->IsGizmoEnabled(EditorGizmos::BOUNDING_BOX)))
					//	m_EditorRenderPipeline->ToggleGizmo(EditorGizmos::BOUNDING_BOX);
					//
					//if (ImGui::MenuItem("Grid", nullptr, m_EditorRenderPipeline->IsGizmoEnabled(EditorGizmos::GRID)))
					//	m_EditorRenderPipeline->ToggleGizmo(EditorGizmos::GRID);
					//
					//if (ImGui::MenuItem("Icons", nullptr, m_EditorRenderPipeline->AreIconsEnabled()))
					//	m_EditorRenderPipeline->ToggleIcons();
					//
					//if (ImGui::MenuItem("Selection Outiline", nullptr, m_EditorRenderPipeline->IsSelectionOutlineEnabled()))
					//	m_EditorRenderPipeline->ToggleSelectionOutline();

					ImGui::EndMenu();
				}

				ShowStatsButton(m_ShowStats);
				ShowRenderDocButton("LevelView");

				ImGui::EndMenuBar();
			}

			if (ImGui::BeginChild("LevelViewRenderTarget", ImVec2(0, 0), false, ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoScrollbar))
			{
				ImVec2 windowSize = ImGui::GetWindowSize();
				ImVec2 windowTopLeft = ImGui::GetCursorScreenPos();
				m_Resolution = glm::uvec2(windowSize.x, windowSize.y);

				const ImTextureID textureId = UiManager::GetInstance().GetPipelineTextureImGuiID("MainColorBuffer"_ID, GfxResourceState::RENDER_TARGET);
				ImGui::Image(textureId, windowSize);
				
				DrawStats(m_ShowStats, windowTopLeft, windowSize, simulationContext);
			}
			ImGui::EndChild();
		}
		ImGui::End();
	}

	glm::uvec2 LevelView::GetResolution()
	{
		return m_Resolution;
	}

	ImGuiWindow* LevelView::GetWindow()
	{
		return m_Window;
	}
}

#pragma once
#include "ecs/Entity.h"
#include "ecs/components/Transform.h"

#include <imgui.h>

namespace Pacha
{
	class EntityUtilities
	{
	public:
		static void DrawBarMenu()
		{
			if (ImGui::MenuItem("New Entity"))
				CreateEmptyEntity();
		}

	private:
		static Entity* CreateEmptyEntity()
		{
			Entity* newEntity = CreateEntity("New Entity");
			newEntity->AddComponent<Transform>();
			return newEntity;
		}
	};
}
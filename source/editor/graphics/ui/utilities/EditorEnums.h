#pragma once
#include "utilities/Enum.h"
#include "utilities/Macros.h"

#include <vector>
#include <string>
#include <imgui.h>
#include <algorithm>

namespace Pacha
{
	namespace EditorUI
	{
		template<typename T, typename = std::enable_if_t<std::is_integral_v<T> && std::is_signed_v<T>>>
		class EditorEnum
		{
		private:
			enum class BinaryOperation
			{
				OR,
				AND,
				LSHIFT,
				RSHIFT,
				NEG,
				XOR
			};

		protected:
			static std::vector<std::string> GenerateEnumNames(std::string rawValues)
			{
				std::vector<std::string> items;
				rawValues.erase(std::remove_if(rawValues.begin(), rawValues.end(), isblank), rawValues.end());

				size_t pos = rawValues.find_first_of(',');
				while (pos != std::string::npos)
				{
					std::string token = rawValues.substr(0, pos);
					std::replace_if(token.begin(), token.end(), [](const char& value) { return value == '_'; }, ' ');
					std::transform(token.begin(), token.end(), token.begin(), [](const char& value) { return static_cast<char>(tolower(value)); });
					token[0] = static_cast<char>(toupper(token[0]));

					size_t blankPos = 0;
					blankPos = token.find(' ', blankPos);
					while (blankPos != std::string::npos)
					{
						token[blankPos + 1] = static_cast<char>(toupper(token[blankPos + 1]));
						blankPos = token.find(' ', blankPos + 1);
					}

					rawValues = rawValues.substr(pos + 1);
					pos = rawValues.find_first_of(',');
					items.push_back(token);
				}

				if (rawValues.size() > 0)
				{
					std::replace_if(rawValues.begin(), rawValues.end(), [](const char& value) { return value == '_'; }, ' ');
					std::transform(rawValues.begin(), rawValues.end(), rawValues.begin(), [](const char& value) { return static_cast<char>(tolower(value)); });
					rawValues[0] = static_cast<char>(toupper(rawValues[0]));

					size_t blankPos = 0;
					blankPos = rawValues.find(' ', blankPos);
					while (blankPos != std::string::npos)
					{
						rawValues[blankPos + 1] = static_cast<char>(toupper(rawValues[blankPos + 1]));
						blankPos = rawValues.find(' ', blankPos + 1);
					}

					items.push_back(rawValues);
				}

				for (auto& item : items)
				{
					size_t eqPos = item.find('=');
					item = item.substr(0, eqPos);
				}

				return items;
			}

			static std::vector<T> GenerateEnumValues(std::string rawValues)
			{
				std::vector<std::string> items;
				rawValues.erase(std::remove_if(rawValues.begin(), rawValues.end(), isblank), rawValues.end());

				size_t pos = rawValues.find_first_of(',');
				while (pos != std::string::npos)
				{
					std::string token = rawValues.substr(0, pos);
					items.push_back(token);

					rawValues = rawValues.substr(pos + 1);
					pos = rawValues.find_first_of(',');
				}

				if (rawValues.size() > 0)
					items.push_back(rawValues);

				std::vector<std::pair<std::string, std::string>> stringValueMap;
				std::vector <std::pair<std::string, T>> realValuesMap;

				for (auto& item : items)
				{
					size_t eqPos = item.find('=');
					if (eqPos == std::string::npos)
						stringValueMap.push_back(std::make_pair(item, ""));
					else
						stringValueMap.push_back(std::make_pair(item.substr(0, eqPos), item.substr(eqPos + 1, item.size() - eqPos + 1)));
				}

				T baseValue = 0;
				for (const auto& stringValuePair : stringValueMap)
				{
					if (stringValuePair.second.empty())
					{
						realValuesMap.push_back(std::make_pair(stringValuePair.first, baseValue));
						++baseValue;
					}
					else if (stringValuePair.second.find_first_not_of("0123456789") == std::string::npos)
					{
						T value = static_cast<T>(_atoi64(stringValuePair.second.c_str()));
						realValuesMap.push_back(std::make_pair(stringValuePair.first, value));
						baseValue = ++value;
					}
					else if ((pos = stringValuePair.second.find_first_of("|<>~&^")) != std::string::npos)
					{
						std::string stringPart = stringValuePair.second;
						std::vector<std::string> parts;
						std::vector<BinaryOperation> operations;

						while (pos != std::string::npos)
						{
							std::string token = stringPart.substr(0, pos);
							if (!token.empty())
								parts.push_back(token);

							size_t skip = 1;
							if (stringPart[pos] == '|')
								operations.push_back(BinaryOperation::OR);
							else if (stringPart[pos] == '&')
								operations.push_back(BinaryOperation::AND);
							else if (stringPart[pos] == '^')
								operations.push_back(BinaryOperation::XOR);
							else if (stringPart[pos] == '~')
								operations.push_back(BinaryOperation::NEG);
							else if (stringPart[pos] == '<')
							{
								operations.push_back(BinaryOperation::LSHIFT);
								skip = 2;
							}
							else if (stringPart[pos] == '>')
							{
								operations.push_back(BinaryOperation::RSHIFT);
								skip = 2;
							}

							stringPart = stringPart.substr(pos + skip);
							pos = stringPart.find_first_of("|<>~&^");
						}

						if (stringPart.size() > 0)
							parts.push_back(stringPart);

						T value = 0;
						if (parts[0].find_first_not_of("0123456789") != std::string::npos)
						{
							auto found = std::find_if(realValuesMap.begin(), realValuesMap.end(), [&](const std::pair<std::string, T>& value) {return value.first == parts[0]; });
							if (found != realValuesMap.end())
								value = found->second;
						}
						else
							value = std::atoi(parts[0].c_str());

						size_t k = 0;
						for (size_t i = 1; i < parts.size(); ++i)
						{
							T tempValue = 0;
							if (parts[i].find_first_not_of("0123456789") != std::string::npos)
							{
								auto found = std::find_if(realValuesMap.begin(), realValuesMap.end(), [&](const std::pair<std::string, T>& value) {return value.first == parts[i]; });
								if (found != realValuesMap.end())
									tempValue = found->second;
							}
							else
								tempValue = static_cast<T>(_atoi64(parts[i].c_str()));

							switch (operations[k])
							{
								case BinaryOperation::OR:
									if (operations.size() > 1 && operations[k + 1] == BinaryOperation::NEG)
									{
										tempValue = ~tempValue;
										++k;
									}
									value |= tempValue;
									break;
								case BinaryOperation::AND:
									if (operations.size() > 1 && operations[k + 1] == BinaryOperation::NEG)
									{
										tempValue = ~tempValue;
										++k;
									}
									value &= tempValue;
									break;
								case BinaryOperation::XOR:
									if (operations.size() > 1 && operations[k + 1] == BinaryOperation::NEG)
									{
										tempValue = ~tempValue;
										++k;
									}
									value ^= tempValue;
									break;
								case BinaryOperation::LSHIFT:
									if (operations.size() > 1 && operations[k + 1] == BinaryOperation::NEG)
									{
										tempValue = ~tempValue;
										++k;
									}
									value <<= tempValue;
									break;
								case BinaryOperation::RSHIFT:
									if (operations.size() > 1 && operations[k + 1] == BinaryOperation::NEG)
									{
										tempValue = ~tempValue;
										++k;
									}
									value >>= tempValue;
									break;
								case BinaryOperation::NEG:
									value = ~value;
									break;
							}

							++k;
						}

						realValuesMap.push_back(std::make_pair(stringValuePair.first, value));
						baseValue = ++value;
					}
					else
					{
						auto found = std::find_if(realValuesMap.begin(), realValuesMap.end(), [&](const std::pair<std::string, T>& value) {return value.first == stringValuePair.second; });
						if (found != realValuesMap.end())
						{
							T value = found->second;
							realValuesMap.push_back(std::make_pair(stringValuePair.first, value));
							baseValue = ++value;
						}
					}
				}

				std::vector<T> values;
				for (const auto& realValue : realValuesMap)
					values.push_back(realValue.second);

				return values;
			}
		};

		template<typename T>
		class EnumData { };
	}
}

#define EDITOR_ENUM_DATA(type, sizeType, ...)												\
namespace EditorUI																			\
{																							\
	template<>																				\
	class EnumData<type> : EditorEnum<sizeType>												\
	{																						\
	public:																					\
		static const std::vector<std::string>& GetValueNames()								\
		{																					\
			static const std::vector<std::string> names = GenerateEnumNames(#__VA_ARGS__);	\
			return names;																	\
		}																					\
																							\
		static const std::vector<sizeType>& GetValues()										\
		{																					\
			static const std::vector<sizeType> values = GenerateEnumValues(#__VA_ARGS__);	\
			return values;																	\
		}																					\
	};																						\
}

#if PACHA_EDITOR

#define EDITOR_FLAGS(type, ...)							\
typedef uint32_t type##Flags;							\
enum type##FlagBits { __VA_ARGS__ };					\
EDITOR_ENUM_DATA(type##FlagBits, int32_t, __VA_ARGS__)

#define EDITOR_FLAGS_SIZE(type, sizeType, ...)			\
typedef sizeType type##Flags;							\
enum type##FlagBits : sizeType { __VA_ARGS__ };			\
EDITOR_ENUM_DATA(type##FlagBits, sizeType, __VA_ARGS__)

#define EDITOR_ENUM(type, ...)					\
enum class type { __VA_ARGS__ };				\
EDITOR_ENUM_DATA(type, int32_t, __VA_ARGS__)

#define EDITOR_ENUM_SIZE(type, sizeType, ...)	\
enum class type : sizeType { __VA_ARGS__ };		\
EDITOR_ENUM_DATA(type, sizeType, __VA_ARGS__)

#endif

#define IMGUI_ENUM(type, variable)																				\
{																												\
	const uint64_t selectedValue = static_cast<uint64_t>(variable);												\
	const std::vector<uint64_t>& values = EditorUI::EnumData<type>::GetValues();								\
	const std::vector<std::string>& valueNames = EditorUI::EnumData<type>::GetValueNames();						\
	if (ImGui::BeginCombo(VARIABLE_IMGUI_ID(variable), valueNames[selectedValue].c_str()))						\
	{																											\
		for (size_t i = 0; i < values.size(); ++i)																\
		{																										\
			bool selected = values[i] == selectedValue;															\
			if (ImGui::Checkbox(valueNames[i].c_str(), &selected))												\
			{																									\
				if (selected)																					\
					variable = static_cast<type>(values[i]);													\
																												\
				ImGui::CloseCurrentPopup();																		\
				break;																							\
			}																									\
		}																										\
		ImGui::EndCombo();																						\
	}																											\
}

#define IMGUI_FLAGS(type, variable)																		\
{																										\
	std::string selectedName = "Empty";																	\
	const uint64_t value = static_cast<uint64_t>(variable);												\
	const std::vector<uint64_t>& values = EditorUI::EnumData<type##FlagBits>::GetValues();				\
	const std::vector<std::string>& valueNames = EditorUI::EnumData<type##FlagBits>::GetValueNames();	\
	if(value != 0)																						\
	{																									\
		for (size_t i = 0; i < values.size(); ++i)														\
		{																								\
			if ((value & values[i]) == value)															\
			{																							\
				selectedName = valueNames[i];															\
				break;																					\
			}																							\
			else if(value & values[i])																	\
				selectedName = "Mixed";																	\
		}																								\
	}																									\
	if (ImGui::BeginCombo(VARIABLE_IMGUI_ID(variable), selectedName.c_str()))							\
	{																									\
		for (size_t i = 0; i < values.size(); ++i)														\
		{																								\
			bool selected = value & values[i];															\
			if (ImGui::Checkbox(valueNames[i].c_str(), &selected))										\
			{																							\
				if (selected)																			\
					variable = value | values[i];														\
				else																					\
					variable = value & ~values[i];														\
			}																							\
		}																								\
		ImGui::EndCombo();																				\
	}																									\
}

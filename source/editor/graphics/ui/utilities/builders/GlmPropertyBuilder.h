#pragma once
#include "PropertyBuilder.h"

#include <imgui.h>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

namespace Pacha::UI
{
	template<>
	inline bool PropertyField(glm::vec3& value)
	{
		const float lineHeight = ImGui::GetFrameHeight();
		const float widthAvailable = ImGui::GetContentRegionAvail().x;
		const bool hideButtons = widthAvailable < 200;
		const float buttonsSize = hideButtons ? 0.f : (lineHeight * 3);
		const float spacing = ImGui::GetStyle().ItemSpacing.x * (hideButtons ? 2 : 5);
		const float inputWidth = (widthAvailable - buttonsSize - spacing) / 3;

		bool valueChanged = false;
		if (!hideButtons)
		{
			ImGui::Button("X", ImVec2(lineHeight, lineHeight));
			ImGui::SameLine();
		}

		ImGui::SetNextItemWidth(inputWidth);
		valueChanged |= ImGui::DragFloat("##X", &value.x, 1.f, 0.f, 0.f, "%.2f", ImGuiSliderFlags_NoRoundToFormat);
		ImGui::SameLine();
		
		if (!hideButtons)
		{
			ImGui::Button("Y", ImVec2(lineHeight, lineHeight));
			ImGui::SameLine();
		}
		
		ImGui::SetNextItemWidth(inputWidth);
		valueChanged |= ImGui::DragFloat("##Y", &value.y, 1.f, 0.f, 0.f, "%.2f", ImGuiSliderFlags_NoRoundToFormat);
		ImGui::SameLine();
		
		if (!hideButtons)
		{
			ImGui::Button("Z", ImVec2(lineHeight, lineHeight));
			ImGui::SameLine();
		}

		ImGui::SetNextItemWidth(inputWidth);
		valueChanged |= ImGui::DragFloat("##Z", &value.z, 1.f, 0.f, 0.f, "%.2f", ImGuiSliderFlags_NoRoundToFormat);
		
		return valueChanged;
	}

	template<>
	inline bool PropertyField(glm::quat& value)
	{
		glm::vec3 euler = glm::degrees(glm::eulerAngles(value));
		bool valueChanged = PropertyField(euler);
		value = glm::quat(glm::radians(euler));
		return valueChanged;
	}
}
#pragma once

namespace Pacha::UI
{
	template<typename T>
	inline bool PropertyField(T&)
	{
		static_assert("Builder not implemented for type");
		return false;
	}
}
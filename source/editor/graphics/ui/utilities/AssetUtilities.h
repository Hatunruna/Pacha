#pragma once
#include "ecs/Entity.h"
#include "graphics/ui/UiManager.h"
#include "graphics/ui/widgets/importers/ModelImporter.h"
#include "graphics/ui/widgets/importers/TextureImporter.h"

#include <imgui.h>
#include <filesystem>

namespace Pacha
{
	class AssetUtilities
	{
	public:
		static inline void DrawBarMenu()
		{
			UiManager& uiManager = UiManager::GetInstance();

			if (ImGui::BeginMenu("Import Asset"))
			{
				if (ImGui::MenuItem("3D Model"))
					uiManager.RegisterWidget<ModelImporter>("ModelImporter"_ID);

				if (ImGui::MenuItem("Texture"))
					uiManager.RegisterWidget<TextureImporter>("TextureImporter"_ID);
				
				ImGui::EndMenu();
			}
		}
	};
}
#pragma once
#include "PropertyBuilders.h"
#include "core/PlatformInterop.h"

#include <imgui.h>

#include <string>
#include <sstream>
#include <stack>

namespace Pacha
{
	class InspectorBuilder
	{
	public:
		bool NonRemovableComponentHeader(const char* label)
		{
			return ImGui::CollapsingHeader(label, ImGuiTreeNodeFlags_DefaultOpen);
		}

		template<typename T>
		bool ComponentHeader(const char* label, class Entity* entity)
		{
			bool keepComponent = true;
			bool shouldDraw = ImGui::CollapsingHeader(label, &keepComponent, ImGuiTreeNodeFlags_DefaultOpen);
		
			if (!keepComponent)
			{
				entity->RemoveComponent<T>();
				return false;
			}

			return shouldDraw;
		}

		float GetLabelWidth(const char* text)
		{
			return ImGui::CalcTextSize(text).x + ImGui::GetStyle().ItemSpacing.x * 2;
		}

		void PushLabelAlignment(float alignment)
		{
			m_LabelAlignment.push(alignment);
		}

		void PopLabelAlignment()
		{
			m_LabelAlignment.pop();
		}

		template<typename T>
		bool PropertyField(T& value)
		{
			return Pacha::UI::PropertyField(PropertyId(value), value);
		}

		template<typename T>
		bool PropertyField(const char* label, T& value)
		{
			const std::string propertyId = PropertyId(value);

			float sameLineOffset = 0;
			if (!m_LabelAlignment.empty())
			{
				sameLineOffset = m_LabelAlignment.top();
			}
			
			ImGui::Text(label);
			ImGui::SameLine(sameLineOffset);

			ImGui::PushID(propertyId.c_str());
			ImGui::BeginGroup();

			bool valueChanged = Pacha::UI::PropertyField(value);

			ImGui::EndGroup();
			ImGui::PopID();

			return valueChanged;
		}

	private:

		std::stack<float> m_LabelAlignment;

		template<typename T>
		std::string PropertyId(T& value)
		{
			std::stringstream stream;
			stream << &value;
			return "##" + stream.str();
		}
	};
}
#pragma once
#include "graphics/renderpipeline/GfxRenderPipeline.h"
#include "graphics/resources/texture/GfxTexture.h"

namespace Pacha
{
	class EditorRenderPipeline : public GfxRenderPipeline
	{
	public:
		EditorRenderPipeline();
	};
}
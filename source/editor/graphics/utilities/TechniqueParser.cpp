#include "TechniqueParser.h"
#include "core/Log.h"

#include <fstream>
#include <cstring>

namespace Pacha
{
	TechniqueParser::~TechniqueParser()
	{
		if (m_TechniqueText != nullptr)
			delete m_TechniqueText;
	}

	bool TechniqueParser::Initialize(const std::filesystem::path& techniquePath)
	{
		std::ifstream file;
		file.open(techniquePath, std::ios::binary | std::ios::ate);

		if (file.is_open())
		{
			size_t size = file.tellg();
			file.seekg(0, std::ios::beg);

			m_Scanner.start = m_TechniqueText = new char[size + 1];
			m_Scanner.current = m_Scanner.start;
			m_Scanner.line = 1;

			if (!file.read(const_cast<char*>(m_Scanner.start), size))
			{
				LOGERROR("Failed to read technique file " << techniquePath);
				delete m_TechniqueText;

				m_TechniqueText = nullptr;
				m_Scanner.start = nullptr;
				m_Scanner.current = nullptr;
				return false;
			}

			const_cast<char*>(m_Scanner.start)[size] = '\0';
			file.close();

			m_TechniqueName = techniquePath.filename().string();
			return true;
		}
		else
		{
			LOGERROR("Failed to load technique file " << techniquePath);
			return false;
		}
	}

	bool TechniqueParser::Parse(bool includeResources)
	{
		if (m_Scanner.start == nullptr)
			return false;

		Token token = ScanToken();
		if (includeResources)
		{
			while (token.type != TokenType::ERROR && token.type != TokenType::END_OF_FILE)
			{
				m_Tokens.push_back(token);
				token = ScanToken();
			}
		}
		else
		{
			while (token.type != TokenType::ERROR && token.type != TokenType::END_OF_FILE)
			{
				if (token.type != TokenType::RESOURCES)
					m_Tokens.push_back(token);
				else
				{
					token = ScanToken();
					if (token.type != TokenType::COLON)
					{
						LOGERROR("Technique " << m_TechniqueName << " - Expected ':' at line " << token.line);
						return false;
					}

					token = ScanToken();
					if (token.type != TokenType::TEXT_BLOCK)
					{
						LOGERROR("Technique " << m_TechniqueName << " - Expected text block at line " << token.line);
						return false;
					}
				}

				token = ScanToken();
			}
		}

		if (token.type == TokenType::ERROR)
		{
			LOGERROR("Technique " << m_TechniqueName << " - " << token.start << " at line " << token.line);
			return false;
		}

		return true;
	}

	const std::deque<Token>& TechniqueParser::GetTokens()
	{
		return m_Tokens;
	}

	const std::string& TechniqueParser::GetTechniqueName()
	{
		return m_TechniqueName;
	}

	char TechniqueParser::Advance(const uint32_t& amount)
	{
		m_Scanner.current += amount;
		return m_Scanner.current[-1];
	}

	char TechniqueParser::Peek()
	{
		return *m_Scanner.current;
	}

	char TechniqueParser::PeekNext()
	{
		if (IsAtEnd())
			return '\0';
		return m_Scanner.current[1];
	}

	bool TechniqueParser::Match(const char& expected)
	{
		if (IsAtEnd())
			return false;

		if (*m_Scanner.current != expected)
			return false;

		m_Scanner.current++;
		return true;
	}

	bool TechniqueParser::MatchKeyword(const uint32_t start, const uint32_t length, const char* rest)
	{
		return (static_cast<uint32_t>(m_Scanner.current - m_Scanner.start) == start + length) && (memcmp(m_Scanner.start + start, rest, length) == 0);
	}

	bool TechniqueParser::IsAlpha(const char& c)
	{
		return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_';
	}

	bool TechniqueParser::IsDigit(const char& c)
	{
		return c >= '0' && c <= '9';
	}

	bool TechniqueParser::IsAtEnd()
	{
		return *m_Scanner.current == '\0';
	}

	void TechniqueParser::SkipWhitespace()
	{
		while (true)
		{
			char c = Peek();
			switch (c)
			{
				case ' ':
				case '\r':
				case '\t':
					Advance();
					break;
				case '\n':
					m_Scanner.line++;
					Advance();
					break;
				case '/':
				{
					if (PeekNext() == '/')
					{
						while (Peek() != '\n' && !IsAtEnd())
							Advance();
					}
					else if (PeekNext() == '*')
					{
						Advance(2);
						while (Peek() != '*' && PeekNext() != '/' && !IsAtEnd())
						{
							if (Peek() == '\n')
								m_Scanner.line++;
							Advance();
						}
						Advance(2);
					}
					else
						return;
					break;
				}
				default:
					return;
			}
		}
	}

	void TechniqueParser::SkipNewLine()
	{
		while (true)
		{
			char c = Peek();
			switch (c)
			{
				case ' ':
				case '\r':
				case '\t':
					Advance();
					break;
				case '\n':
					m_Scanner.line++;
					Advance();
					break;
				default:
					return;
			}
		}
	}

	Token TechniqueParser::ScanToken()
	{
		SkipWhitespace();
		m_Scanner.start = m_Scanner.current;

		if (IsAtEnd())
			return MakeToken(TokenType::END_OF_FILE);

		char c = Advance();
		if (IsAlpha(c))
			return Identifier();

		if (IsDigit(c))
			return Number();

		switch (c)
		{
			case '{': return MakeToken(TokenType::LEFT_CURLY_BRACKET);
			case '}': return MakeToken(TokenType::RIGHT_CURLY_BRACKET);
			case '(': return MakeToken(TokenType::LEFT_PARENTHESIS);
			case ')': return MakeToken(TokenType::RIGHT_PARENTHESIS);
			case ':': return MakeToken(TokenType::COLON);
			case ',': return MakeToken(TokenType::COMMA);
			case '-': return MakeToken(TokenType::MINUS);
			case '|': return MakeToken(TokenType::PIPE);
			case '#': return IncludeString();
			case '<': return TextBlock();
		}

		return ErrorToken("Unexpected character");
	}

	Token TechniqueParser::MakeToken(const TokenType& type)
	{
		Token token;
		token.type = type;
		token.start = m_Scanner.start;
		token.length = (uint32_t)(m_Scanner.current - m_Scanner.start);
		token.line = m_Scanner.line;
		return token;
	}

	Token TechniqueParser::ErrorToken(const char* message)
	{
		Token token;
		token.type = TokenType::ERROR;
		token.start = message;
		token.length = (uint32_t)strlen(message);
		token.line = m_Scanner.line;
		return token;
	}

	Token TechniqueParser::Identifier()
	{
		while (IsAlpha(Peek()) || IsDigit(Peek()))
			Advance();
		return MakeToken(IdentifierType());
	}

	Token TechniqueParser::Number()
	{
		while (IsDigit(Peek()))
			Advance();

		if (Peek() == '.' && IsDigit(PeekNext()))
		{
			Advance();
			while (IsDigit(Peek()))
				Advance();
		}

		return MakeToken(TokenType::NUMBER);
	}

	Token TechniqueParser::TextBlock()
	{
		if (Match('|'))
		{
			SkipNewLine();

			uint32_t line = m_Scanner.line;
			m_Scanner.start = m_Scanner.current;

			while (!IsAtEnd())
			{
				if (Peek() == '|' && PeekNext() == '>')
					break;

				if (Peek() == '\n')
					m_Scanner.line++;
				Advance();
			}

			if (IsAtEnd())
				return ErrorToken("Unterminated text block");

			Token token;
			token.type = TokenType::TEXT_BLOCK;
			token.start = m_Scanner.start;
			token.length = (uint32_t)(m_Scanner.current - m_Scanner.start);
			token.line = line;

			Advance(2);
			return token;
		}
		else
			return ErrorToken("Unexpected character");
	}

	Token TechniqueParser::IncludeString()
	{
		if (memcmp(m_Scanner.start, "#include", 7) == 0)
		{
			Advance(7);
			SkipWhitespace();

			if (Match('"'))
			{
				m_Scanner.start = m_Scanner.current;
				while ((IsAlpha(Peek()) || Peek() == '.' || Peek() == '/') && !IsAtEnd())
				{
					if (Peek() == '\n')
						return ErrorToken("Include string format error");
					Advance();
				}

				if (Peek() == '"')
				{
					Token returnToken = MakeToken(TokenType::INCLUDE_STRING);
					Advance();
					return returnToken;
				}
			}
		}

		return ErrorToken("Unexpected character");
	}

	TokenType TechniqueParser::IdentifierType()
	{
		if (m_Scanner.current - m_Scanner.start == 1)
			return TokenType::STRING_VARIABLE;

		switch (m_Scanner.start[0])
		{
			case 'A':
			{
				switch (m_Scanner.start[1])
				{
					case 'H': return CheckKeyword(2, 1, "S", TokenType::DEFINE_STAGE_AHS);
					case 'd': return CheckKeyword(2, 1, "d", TokenType::ADD);
					case 'l':
					{
						switch (m_Scanner.start[2])
						{
							case 'w': return CheckKeyword(3, 3, "ays", TokenType::ALWAYS);
						}
						if (MatchKeyword(2, 13, "phaToCoverage")) return TokenType::ALPHA_TO_COVERAGE;
						if (MatchKeyword(2, 7, "phaTest")) return TokenType::ALPHA_TEST;
						break;
					}
					case 'n':
					{
						switch (m_Scanner.start[2])
						{
							case 'y': return CheckKeyword(3, 3, "Hit", TokenType::ANY_HIT);
							case 'd': return TokenType::AND;
						}
						break;
					}
					case 'm': return CheckKeyword(2, 11, "plification", TokenType::AMPLIFICATION);
				}

				return CheckKeyword(1, 1, "S", TokenType::DEFINE_STAGE_AS);
			}
			case 'B':
			{
				switch (m_Scanner.start[1])
				{
					case 'a':  return CheckKeyword(2, 2, "ck", TokenType::BACK);
				}
				if (MatchKeyword(1, 4, "lend")) return TokenType::BLEND;
				if (MatchKeyword(1, 6, "lendOp")) return TokenType::BLEND_OPERATION;
				break;
			}
			case 'C':
			{
				switch (m_Scanner.start[1])
				{
					case 'A': return CheckKeyword(2, 1, "S", TokenType::DEFINE_STAGE_CAS);
					case 'C': return CheckKeyword(2, 1, "W", TokenType::COUNTER_CLOCKWISE);
					case 'H': return CheckKeyword(2, 1, "S", TokenType::DEFINE_STAGE_CHS);
					case 'W': return TokenType::CLOCKWISE;
					case 'a': return CheckKeyword(2, 6, "llable", TokenType::CALLABLE);
					case 'l':
					{
						switch (m_Scanner.start[2])
						{
							case 'e': return CheckKeyword(3, 2, "ar", TokenType::CLEAR);
							case 'o':
							{
								if (MatchKeyword(3, 7, "sestHit")) return TokenType::CLOSEST_HIT;
								break;
							}
							break;
						}
						break;
					}
					case 'o':
					{
						switch (m_Scanner.start[2])
						{
							case 'l': return CheckKeyword(3, 6, "orMask", TokenType::COLOR_MASK);
							case 'm': return CheckKeyword(3, 4, "pute", TokenType::COMPUTE);
						}
						break;
					}
					case 'u':
					{
						switch (m_Scanner.start[2])
						{
							case 'l': return CheckKeyword(3, 1, "l", TokenType::CULL);
						}
					}
				}
				break;
			}
			case 'D':
			{
				switch (m_Scanner.start[1])
				{
					case 'e':
					{
						switch (m_Scanner.start[2])
						{
							case 'f': return CheckKeyword(3, 4, "ines", TokenType::DEFINES);
							case 'p': return CheckKeyword(3, 6, "thBias", TokenType::DEPTH_BIAS);
						}
						break;
					}
					case 'o': return CheckKeyword(2, 4, "main", TokenType::DOMAIN);
					case 's':
					{
						if (MatchKeyword(2, 6, "tColor")) return TokenType::DST_COLOR;
						if (MatchKeyword(2, 6, "tAlpha")) return TokenType::DST_ALPHA;
					}
				}

				return CheckKeyword(1, 1, "S", TokenType::DEFINE_STAGE_DS);
			}
			case 'E':
			{
				switch (m_Scanner.start[1])
				{
					case 'q': return CheckKeyword(2, 3, "ual", TokenType::EQUAL);
				}
				break;
			}
			case 'F':
			{
				switch (m_Scanner.start[1])
				{
					case 'i':  return CheckKeyword(2, 6, "llMode", TokenType::FILL_MODE);
					case 'r':
					{
						switch (m_Scanner.start[2])
						{
							case 'o':
							{
								if (MatchKeyword(3, 2, "nt")) return TokenType::FRONT;
								if (MatchKeyword(3, 6, "ntBack")) return TokenType::FRONT_BACK;
								break;
							}
						}
					}
				}
				break;
			}
			case 'G':
			{
				switch (m_Scanner.start[1])
				{
					case 'e':
					{
						switch (m_Scanner.start[2])
						{
							case 'o': return CheckKeyword(3, 5, "metry", TokenType::GEOMETRY);
							case 'q': return CheckKeyword(3, 3, "ual", TokenType::GEQUAL);
						}
						break;
					}
					case 'r': return CheckKeyword(2, 5, "eater", TokenType::GREATER);
				}

				return CheckKeyword(1, 1, "S", TokenType::DEFINE_STAGE_GS);
			}
			case 'H':
			{
				switch (m_Scanner.start[1])
				{
					case 'u': return CheckKeyword(2, 2, "ll", TokenType::HULL);
				}

				return CheckKeyword(1, 1, "S", TokenType::DEFINE_STAGE_HS);
			}
			case 'I':
			{
				switch (m_Scanner.start[1])
				{
					case 'N': return CheckKeyword(2, 1, "S", TokenType::DEFINE_STAGE_INS);
					case 'n': return CheckKeyword(2, 10, "tersection", TokenType::INTERSECTION);
				}
				break;
			}
			case 'L':
			{
				switch (m_Scanner.start[1])
				{
					case 'e':
					{
						switch (m_Scanner.start[2])
						{
							case 's': return CheckKeyword(3, 1, "s", TokenType::LESS);
							case 'q': return CheckKeyword(3, 3, "ual", TokenType::LEQUAL);
						}
						break;
					}
					case 'i': return CheckKeyword(2, 7, "neWidth", TokenType::LINE_WIDTH);
					case 'o': return CheckKeyword(2, 12, "gicalBlendOp", TokenType::LOGICAL_BLEND_OPERATION);
				}
				break;
			}
			case 'M':
			{
				switch (m_Scanner.start[1])
				{
					case 'S': return CheckKeyword(2, 1, "S", TokenType::DEFINE_STAGE_MSS);
					case 'a': return CheckKeyword(2, 1, "x", TokenType::MAX);
					case 'e': return CheckKeyword(2, 2, "sh", TokenType::MESH);
					case 'i':
					{
						switch (m_Scanner.start[2])
						{
							case 's': return CheckKeyword(3, 1, "s", TokenType::MISS);
							case 'n': return TokenType::MIN;
						}
					}
				}

				return CheckKeyword(1, 1, "S", TokenType::DEFINE_STAGE_MS);
			}
			case 'N':
			{
				switch (m_Scanner.start[1])
				{
					case 'a': return CheckKeyword(2, 2, "nd", TokenType::NAND);
					case 'e': return CheckKeyword(2, 3, "ver", TokenType::NEVER);
					case 'o':
					{
						switch (m_Scanner.start[2])
						{
							case 'r': return TokenType::NOR;
							case 't': return CheckKeyword(3, 5, "Equal", TokenType::NOT_EQUAL);
						}
					}
				}
				break;
			}
			case 'O':
			{
				switch (m_Scanner.start[1])
				{
					case 'n':
					{
						if (MatchKeyword(2, 1, "e")) return TokenType::ONE;
						if (MatchKeyword(2, 14, "eMinusSrcColor")) return TokenType::ONE_MINUS_SRC_COLOR;
						if (MatchKeyword(2, 14, "eMinusDstColor")) return TokenType::ONE_MINUS_DST_COLOR;
						if (MatchKeyword(2, 14, "eMinusSrcAlpha")) return TokenType::ONE_MINUS_SRC_ALPHA;
						if (MatchKeyword(2, 14, "eMinusDstAlpha")) return TokenType::ONE_MINUS_DST_ALPHA;
						return TokenType::ON;
					}
					case 'r': return TokenType::OR;
					case 'f': return CheckKeyword(2, 1, "f", TokenType::OFF);
					case 'p': return CheckKeyword(2, 4, "aque", TokenType::OPAQUE);
				}
				break;
			}
			case 'P':
			{
				switch (m_Scanner.start[1])
				{
					case 'a':
					{
						switch (m_Scanner.start[2])
						{
							case 's': return CheckKeyword(3, 1, "s", TokenType::PASS);
							case 't': return CheckKeyword(3, 15, "chControlPoints", TokenType::PATCH_CONTROL_POINTS);
						}
						break;
					}
					case 'i': return CheckKeyword(2, 3, "xel", TokenType::PIXEL);
				}

				return CheckKeyword(1, 1, "S", TokenType::DEFINE_STAGE_PS);
			}
			case 'R':
			{
				switch (m_Scanner.start[1])
				{
					case 'G': return CheckKeyword(2, 1, "S", TokenType::DEFINE_STAGE_RGS);
					case 'a': return CheckKeyword(2, 4, "yGen", TokenType::RAY_GEN);
					case 'e':
					{
						switch (m_Scanner.start[2])
						{
							case 's': return CheckKeyword(3, 6, "ources", TokenType::RESOURCES);
							case 'v': return CheckKeyword(3, 13, "erseSubstract", TokenType::REVERSE_SUBSTRACT);
						}
					}
				}
				break;
			}
			case 'S':
			{
				switch (m_Scanner.start[1])
				{
					case 'e': return CheckKeyword(2, 1, "t", TokenType::SET);
					case 'o':
					{
						switch (m_Scanner.start[2])
						{
							case 'u': return CheckKeyword(3, 4, "rces", TokenType::SOURCES);
							case 'l': return CheckKeyword(3, 2, "id", TokenType::SOLID);
						}
						break;
					}
					case 'r':
					{
						switch (m_Scanner.start[2])
						{
							case 'c':
							{
								if (MatchKeyword(3, 5, "Color")) return TokenType::SRC_COLOR;
								if (MatchKeyword(3, 5, "Alpha")) return TokenType::SRC_ALPHA;
								break;
							}
						}
						break;
					}
					case 'u': return CheckKeyword(2, 7, "bstract", TokenType::SUBSTRACT);
				}
				break;
			}
			case 'T':
			{
				switch (m_Scanner.start[1])
				{
					case 'r': return CheckKeyword(2, 9, "ansparent", TokenType::TRANSPARENT);
				}
				break;
			}
			case 'V':
			{
				switch (m_Scanner.start[1])
				{
					case 'e': return CheckKeyword(2, 4, "rtex", TokenType::VERTEX);
				}

				return CheckKeyword(1, 1, "S", TokenType::DEFINE_STAGE_VS);
			}
			case 'W':
			{
				switch (m_Scanner.start[1])
				{
					case 'i':
					{
						switch (m_Scanner.start[2])
						{
							case 'n': return CheckKeyword(3, 9, "dingOrder", TokenType::WINDING_ORDER);
							case 'r': return CheckKeyword(3, 6, "eframe", TokenType::WIREFRAME);
						}
					}
				}
				break;
			}
			case 'X':
			{
				switch (m_Scanner.start[1])
				{
					case 'o': return CheckKeyword(2, 1, "r", TokenType::XOR);
				}
				break;
			}
			case 'Z':
			{
				switch (m_Scanner.start[1])
				{
					case 'e': return CheckKeyword(2, 2, "ro", TokenType::ZERO);
					case 'C': return CheckKeyword(2, 4, "lamp", TokenType::ZCLAMP);
					case 'T': return CheckKeyword(2, 3, "est", TokenType::ZTEST);
					case 'W': return CheckKeyword(2, 4, "rite", TokenType::ZWRITE);
				}
				break;
			}
		}

		return TokenType::STRING_VARIABLE;
	}

	TokenType TechniqueParser::CheckKeyword(const uint32_t& start, const uint32_t& length, const char* rest, const TokenType& type)
	{
		if (MatchKeyword(start, length, rest))
			return type;
		return TokenType::STRING_VARIABLE;
	}
}

#pragma once
#include "utilities/Enum.h"

#include <deque>
#include <string>
#include <filesystem>

namespace Pacha
{
	enum TokenType
	{
		// Single-character tokens
		LEFT_CURLY_BRACKET,
		RIGHT_CURLY_BRACKET,
		LEFT_PARENTHESIS,
		RIGHT_PARENTHESIS,

		PIPE,
		COMMA,
		MINUS,
		COLON,

		// Literals
		NUMBER,
		TEXT_BLOCK,
		INCLUDE_STRING,
		STRING_VARIABLE,

		// Entrypoints
		PASS,		
		RESOURCES,
		SOURCES,

		// Define Stages
		DEFINE_STAGE_VS,
		DEFINE_STAGE_HS,
		DEFINE_STAGE_DS,
		DEFINE_STAGE_GS,
		DEFINE_STAGE_PS,
		DEFINE_STAGE_AS,
		DEFINE_STAGE_MS,
		DEFINE_STAGE_RGS,
		DEFINE_STAGE_AHS,
		DEFINE_STAGE_CHS,
		DEFINE_STAGE_MSS,
		DEFINE_STAGE_INS,
		DEFINE_STAGE_CAS,

		// Pass Keywords
		DEFINES,

		ZWRITE,
		ZTEST,
		ZCLAMP,
		ALPHA_TO_COVERAGE,

		BLEND,
		BLEND_OPERATION,
		COLOR_MASK,

		CULL,
		FILL_MODE,
		LOGICAL_BLEND_OPERATION,
		WINDING_ORDER,

		LINE_WIDTH,
		DEPTH_BIAS,
		PATCH_CONTROL_POINTS,

		VERTEX,
		HULL,
		DOMAIN,
		GEOMETRY,
		PIXEL,
		COMPUTE,
		AMPLIFICATION,
		MESH,
		RAY_GEN,
		ANY_HIT,
		CLOSEST_HIT,
		MISS,
		INTERSECTION,
		CALLABLE,

		// Bool
		ON,
		OFF,

		// Render Type
		OPAQUE,
		ALPHA_TEST,
		TRANSPARENT,

		// Fill Mode
		SOLID,
		WIREFRAME,

		// Winding Order
		CLOCKWISE,
		COUNTER_CLOCKWISE,

		// Face Culling
		FRONT,
		BACK,
		FRONT_BACK,

		// Depth Test
		NEVER,
		LESS,
		GREATER,
		EQUAL,
		ALWAYS,
		LEQUAL,
		GEQUAL,
		NOT_EQUAL,

		// Blending Factor
		ONE,
		ZERO,
		SRC_COLOR,
		ONE_MINUS_SRC_COLOR,
		DST_COLOR,
		ONE_MINUS_DST_COLOR,
		SRC_ALPHA,
		ONE_MINUS_SRC_ALPHA,
		DST_ALPHA,
		ONE_MINUS_DST_ALPHA,

		// Blending Operations
		ADD,
		SUBSTRACT,
		REVERSE_SUBSTRACT,
		MIN,
		MAX,

		// Lgical Bland
		CLEAR,
		AND,
		XOR,
		OR,
		NOR,
		NAND,
		SET,

		// System
		ERROR,
		END_OF_FILE,
	};

	inline bool IsDefineStageToken(const TokenType token)
	{
		return token >= DEFINE_STAGE_VS && token <= DEFINE_STAGE_CAS;
	}

	inline bool IsPassKeywordToken(const TokenType token)
	{
		return token >= DEFINES && token <= CALLABLE;
	}

	inline bool IsBoolToken(const TokenType token)
	{
		return token >= ON && token <= OFF;
	}

	inline bool IsBlendModeToken(const TokenType token)
	{
		return token >= ONE && token <= ONE_MINUS_DST_ALPHA;
	}

	struct Scanner
	{
		uint32_t line = 1;
		const char* start = nullptr;
		const char* current = nullptr;
	};

	struct Token
	{
		uint32_t line = 1;
		uint32_t length = 1;
		const char* start = nullptr;
		TokenType type = TokenType::END_OF_FILE;
	};

	class TechniqueParser
	{
	public:
		TechniqueParser() = default;
		~TechniqueParser();

		bool Initialize(const std::filesystem::path& techniquePath);
		bool Parse(bool includeResources = true);
		const std::deque<Token>& GetTokens();
		const std::string& GetTechniqueName();

	private:
		char Advance(const uint32_t& amount = 1);
		char Peek();
		char PeekNext();
		bool Match(const char& expected);
		bool MatchKeyword(const uint32_t start, const uint32_t length, const char* rest);

		bool IsAlpha(const char& c);
		bool IsDigit(const char& c);
		bool IsAtEnd();

		void SkipWhitespace();
		void SkipNewLine();

		Token ScanToken();
		Token MakeToken(const TokenType& type);
		Token ErrorToken(const char* message);

		Token Identifier();
		Token Number();
		Token TextBlock();
		Token IncludeString();

		TokenType IdentifierType();
		TokenType CheckKeyword(const uint32_t& start, const uint32_t& length, const char* rest, const TokenType& type);

		Scanner m_Scanner;
		std::deque<Token> m_Tokens;
		std::string m_TechniqueName = {};
		const char* m_TechniqueText = nullptr;
	};
}
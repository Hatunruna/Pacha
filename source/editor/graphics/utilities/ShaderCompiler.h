#pragma once
#include "graphics/resources/shader/GfxShader.h"
#include "graphics/resources/technique/GfxTechnique.h"

#if PLATFORM_WINDOWS
#include <atlcomcli.h>
#endif

#include <dxc/dxcapi.h>

namespace Pacha
{
	struct ShaderCompileOptions
	{
		std::wstring entryPoint = {};
		std::vector<LPCWSTR> defines = {};
		GfxShaderType shaderType = GfxShaderType::VERTEX;
		
		bool reflection = false;
		bool generateDebugInfo = false;
	};

	class ShaderCompiler
	{
	public:
		ShaderCompiler();

		bool CompileShader(const std::string& shaderSource, ShaderCompileOptions& compileOptions);
		bool GetReflectionData(GfxShaderReflectionInfo& outReflectionInfo);

		CComPtr<IDxcBlob> GetByteCode();
		uint64_t GetHash();

	private:
		uint32_t m_ReflectionSet = 2;
		CComPtr<IDxcResult> m_CompiledShader;

		uint64_t m_ShaderHash = 0;
		CComPtr<IDxcUtils> m_DXCUtils;
		CComPtr<IDxcCompiler3> m_DXCCompiler;
		CComPtr<IDxcIncludeHandler> m_DXCIncludeHandler;
	};
}
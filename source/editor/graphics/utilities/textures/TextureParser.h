#pragma once
#include "graphics/resources/texture/GfxTextureVK.h"

#include <filesystem>

namespace Pacha
{
	enum class TextureParserError
	{
		NONE,
		FAILED_TO_LOAD,
		FAILED_TO_PROCESS
	};

	class TextureParser
	{
	public:
		TextureParser(const std::filesystem::path& path)
			: m_TexturePath(path) {}
		TextureParser(uint8_t* sourceData, size_t dataSize)
			: m_SourceDataSize(dataSize), m_SourceData(sourceData) {}

		virtual ~TextureParser() {}
		virtual bool Initialize() = 0;
		virtual bool ProcessTexture() = 0;
	
		uint8_t* GetData() { return m_Data; }
		const GfxTextureDescriptor& GetDescriptor() { return m_Descriptor; }

		TextureParserError GetError() { return m_Error; };
		const std::filesystem::path& GetPath() { return m_TexturePath; }

	protected:
		GfxTextureDescriptor m_Descriptor = {};
		
		uint8_t* m_Data = nullptr;

		size_t m_SourceDataSize = 0;
		uint8_t* m_SourceData = nullptr;

		std::filesystem::path m_TexturePath = {};
		TextureParserError m_Error = TextureParserError::NONE;
	};
}
#pragma once
#include "TextureParser.h"

namespace Pacha
{
	class StbParser : public TextureParser
	{
	public:
		using TextureParser::TextureParser;
		~StbParser();

		bool Initialize() override;
		bool ProcessTexture() override;

	private:
		bool m_IsHDR = false;
		bool m_Is16Bit = false;
		int32_t m_NumChannels = 0;
	};
}
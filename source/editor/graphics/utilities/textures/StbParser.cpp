#include "StbParser.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

namespace Pacha
{
	StbParser::~StbParser()
	{
		stbi_image_free(m_Data);
	}

	bool StbParser::Initialize()
	{
		int32_t width = 0;
		int32_t height = 0;

		if (m_SourceData)
		{
			if (stbi_info_from_memory(m_SourceData, static_cast<int>(m_SourceDataSize), &width, &height, &m_NumChannels))
			{
				if (m_NumChannels == 3)
					m_NumChannels = 4;

				m_Descriptor.width = width;
				m_Descriptor.height = height;

				m_IsHDR = static_cast<bool>(stbi_is_hdr_from_memory(m_SourceData, static_cast<int>(m_SourceDataSize)));
				m_Is16Bit = static_cast<bool>(stbi_is_16_bit_from_memory(m_SourceData, static_cast<int>(m_SourceDataSize)));
				return true;
			}
		}
		else
		{
			if (stbi_info(m_TexturePath.string().c_str(), &width, &height, &m_NumChannels))
			{
				if (m_NumChannels == 3)
					m_NumChannels = 4;

				m_Descriptor.width = width;
				m_Descriptor.height = height;

				m_IsHDR = static_cast<bool>(stbi_is_hdr(m_TexturePath.string().c_str()));
				m_Is16Bit = static_cast<bool>(stbi_is_16_bit(m_TexturePath.string().c_str()));
				return true;
			}
		}

		m_Error = TextureParserError::FAILED_TO_LOAD;
		return false;
	}
	
	bool StbParser::ProcessTexture()
	{
		int32_t width = 0;
		int32_t height = 0;
		
		if (m_SourceData)
		{
			if (m_Is16Bit)
			{
				if (stbi_us* data = stbi_load_16_from_memory(m_SourceData, static_cast<int>(m_SourceDataSize), &width, &height, nullptr, m_NumChannels))
					m_Data = reinterpret_cast<uint8_t*>(data);
			}
			else if (m_IsHDR)
			{
				if (float* data = stbi_loadf_from_memory(m_SourceData, static_cast<int>(m_SourceDataSize), &width, &height, nullptr, m_NumChannels))
					m_Data = reinterpret_cast<uint8_t*>(data);
			}
			else
			{
				if (stbi_uc* data = stbi_load_from_memory(m_SourceData, static_cast<int>(m_SourceDataSize), &width, &height, nullptr, m_NumChannels))
					m_Data = data;
			}
		}
		else
		{
			if (m_Is16Bit)
			{
				if (stbi_us* data = stbi_load_16(m_TexturePath.string().c_str(), &width, &height, nullptr, m_NumChannels))
					m_Data = reinterpret_cast<uint8_t*>(data);
			}
			else if (m_IsHDR)
			{
				if (float* data = stbi_loadf(m_TexturePath.string().c_str(), &width, &height, nullptr, m_NumChannels))
					m_Data = reinterpret_cast<uint8_t*>(data);
			}
			else
			{
				if (stbi_uc* data = stbi_load(m_TexturePath.string().c_str(), &width, &height, nullptr, m_NumChannels))
					m_Data = data;
			}
		}

		if (m_Data)
		{
			if (m_IsHDR)
			{
				if (m_NumChannels == 1)
					m_Descriptor.format = GfxPixelFormat::R32_FLOAT;
				else if(m_NumChannels == 2)
					m_Descriptor.format = GfxPixelFormat::RG32_FLOAT;
				else
					m_Descriptor.format = GfxPixelFormat::RGBA32_FLOAT;
			}
			else if (m_Is16Bit)
			{
				if (m_NumChannels == 1)
					m_Descriptor.format = GfxPixelFormat::R16_FLOAT;
				else if (m_NumChannels == 2)
					m_Descriptor.format = GfxPixelFormat::RG16_FLOAT;
				else
					m_Descriptor.format = GfxPixelFormat::RGBA16_FLOAT;
			}
			else
			{
				if (m_NumChannels == 1)
					m_Descriptor.format = GfxPixelFormat::R8_UNORM;
				else if (m_NumChannels == 2)
					m_Descriptor.format = GfxPixelFormat::RG8_UNORM;
				else
					m_Descriptor.format = GfxPixelFormat::RGBA8_UNORM;
			}

			return true;
		}

		m_Error = TextureParserError::FAILED_TO_PROCESS;
		LOGERROR(stbi_failure_reason());
		return false;
	}
}

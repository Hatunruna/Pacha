#pragma once
#include "TechniqueParser.h"
#include "utilities/PachaId.h"
#include "graphics/resources/technique/GfxTechnique.h"
#include "graphics/resources/technique/GfxRendererState.h"

#include <unordered_set>

namespace Pacha
{
	enum class GfxTechniqueType
	{
		RASTERIZATION,
		COMPUTE,
		RAYTRACING
	};

	struct GfxTechniqueCompilerPass
	{
		GfxTechniquePass pass;

		size_t sourceIndex = {};
		ShaderBitSet sharedDefinesMask = {};
		std::unordered_map<GfxShaderType, std::string> shaderEntryPoints = {};
	};

	class TechniqueCompiler
	{
	public:
		TechniqueCompiler() = default;
		bool Compile(const std::filesystem::path& techniquePath);
		const std::vector<GfxTechniqueCompilerPass>& GetCompilerPasses();

	private:
		GfxTechniqueType m_TechniqueType;

		std::string m_Resources;
		GfxShaderReflectionInfo m_ReflectionInfo;
		std::vector<GfxTechniqueCompilerPass> m_CompilerPasses;

		size_t m_SourceIndex = 0;
		std::vector<std::string> m_Sources;
		std::deque<TechniqueParser> m_TechniqueParsers;
		std::unordered_set<PachaId> m_DynamicBuffers;

		bool ResourcesReflection();
		bool GetTechniqueType(const std::filesystem::path& techniquePath);
		bool ExtractTechniqueData(const std::filesystem::path& techniquePath);
		
		void ValidateDefineMasks();
		void ProcessResources();
		void ProcessSources();

		bool GetPassData(std::deque<Token>::iterator& token);
		bool GetTextBlock(std::deque<Token>::iterator& token, std::string& value);
		bool GetDefines(std::deque<Token>::iterator& token, GfxTechniqueCompilerPass& pass);
		
		bool GetBoolValue(std::deque<Token>::iterator& token, bool& boolValue);
		bool GetStringValue(std::deque<Token>::iterator& token, std::string& stringValue);

		bool GetBlendValue(std::deque<Token>::iterator& token, GfxRendererState& rendererState);
		bool GetBlendFactor(std::deque<Token>::iterator& token, GfxBlendingFactor& blendFactor);
		bool GetBlendOperationValue(std::deque<Token>::iterator& token, GfxRendererState& rendererState);
		bool GetBlendOperation(std::deque<Token>::iterator& token, GfxBlendingOperation& blendOperation);
		bool GetColorMaskValue(std::deque<Token>::iterator& token, GfxRendererState& rendererState);
		
		bool GetDepthBias(std::deque<Token>::iterator& token, GfxRendererState& rendererState);
		bool GetDepthTestValue(std::deque<Token>::iterator& token, GfxRendererState& rendererState);
		bool GetLogicalBlendOperation(std::deque<Token>::iterator& token, GfxRendererState& rendererState);
		bool GetCullMode(std::deque<Token>::iterator& token, GfxRendererState& rendererState);

		bool GetFillMode(std::deque<Token>::iterator& token, GfxRendererState& rendererState);
		bool GetWindingOrder(std::deque<Token>::iterator& token, GfxRendererState& rendererState);
		bool GetDefineStageFromToken(const std::deque<Token>::iterator& token, GfxShaderType& outType);

		inline bool CheckForColon(std::deque<Token>::iterator& token);
		inline bool CheckForRenderTarget(std::deque<Token>::iterator& token, GfxRendererState& rendererState, uint32_t& renderTarget);
		inline bool IsRGBAStringCorrectlyFormatted(const std::string& rgbaString);
		inline void AdvanceToken(std::deque<Token>::iterator& token, const uint32_t amount = 1);

		template<typename T, bool ColonCheck = true>
		bool GetNumberValue(std::deque<Token>::iterator& token, T& value)
		{
			if constexpr (ColonCheck)
			{
				if (!CheckForColon(token))
					return false;
			}

			if (token->type == TokenType::NUMBER)
			{
				std::string number(token->start, token->length);
				if constexpr (std::is_integral<T>::value)
					value = static_cast<T>(std::stoi(number));
				else if constexpr (std::is_floating_point<T>::value)
					value = static_cast<T>(std::stof(number));
			}
			else
			{
				LOGERROR("Unexpected token at line:" << token->line << " - Expected a number value");
				return false;
			}

			return true;
		}
	};
}
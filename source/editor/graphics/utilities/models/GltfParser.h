#pragma once
#include "ModelParser.h"
#include "assets/AssetManager.h"
#include "core/ProjectConstants.h"
#include "graphics/ui/widgets/importers/TextureImporter.h"

#include <rapidjson/document.h>
#include <rapidjson/prettywriter.h>

#define TINYGLTF_USE_CPP14
#define TINYGLTF_USE_RAPIDJSON
#define TINYGLTF_NOEXCEPTION
#define TINYGLTF_NO_STB_IMAGE
#define TINYGLTF_NO_EXTERNAL_IMAGE
#define TINYGLTF_NO_STB_IMAGE_WRITE
#define TINYGLTF_NO_INCLUDE_STB_IMAGE
#define TINYGLTF_NO_INCLUDE_RAPIDJSON
#define TINYGLTF_NO_INCLUDE_STB_IMAGE_WRITE
#include <tiny_gltf.h>

namespace Pacha
{
	enum class GLTFType
	{
		GLTF,
		GLB
	};

	class GltfParser : public ModelParser
	{
	public:
		using ModelParser::ModelParser;

		bool Initialize() override;
		bool ProcessModel(const std::filesystem::path& assetsProjectPath = {}) override;
		std::unordered_set<ModelDependency> GetDependencies() override;
		
	private:
		GLTFType m_Type;
		tinygltf::Model m_Model;
		
		bool ProcessMaterials(const std::filesystem::path& assetsProjectPath) override;

		bool ProcessNode(const tinygltf::Node& node, GfxModelData::Node& modelNode);
		bool ProcessMesh(const tinygltf::Mesh& mesh, GfxModelData::Node& modelNode);
		void GetIndicesData(const tinygltf::Model& model, const int indicesIndex, std::vector<uint32_t>& indices);
		
		template<typename T>
		bool ImportBuiltInTexture(const T& textureInfo, const std::filesystem::path& assetsProjectPath, const bool srgb, PachaUUID& outuuid)
		{
			if (textureInfo.index >= 0)
			{
				const tinygltf::Image& image = m_Model.images[m_Model.textures[textureInfo.index].source];
				const tinygltf::BufferView& bufferView = m_Model.bufferViews[image.bufferView];
				const tinygltf::Buffer& buffer = m_Model.buffers[bufferView.buffer];
				const std::string textureFileName = image.name + "." + image.mimeType.substr(6);

				const std::filesystem::path relativePath = std::filesystem::relative(assetsProjectPath / m_ModelPath.filename(), ProjectConstants::GetInstance().GetAssetsPath());
				const std::string assetLookupString = relativePath.string() + ":" + textureFileName;

				std::string error = {};
				if (!TextureImporter::ImportFromMemory(const_cast<uint8_t*>(&buffer.data[bufferView.byteOffset]), bufferView.byteLength, textureFileName, assetLookupString, true, srgb, error))
				{
					LOGERROR("Failed to process model '" << m_ModelPath << "': GLB Texture load failed - " << error);
					m_Error = ModelParserError::FAILED_TO_PROCESS;
					return false;
				}

				AssetManager& assetManager = AssetManager::GetInstance();
				if (TextureAsset* textureAsset = assetManager.Get<TextureAsset>(assetLookupString))
					outuuid = textureAsset->GetUUID();

				return true;
			}

			return false;
		}

		template<typename T>
		bool GetTexturePath(const T& textureInfo, std::filesystem::path& path, const std::filesystem::path& modelProjectPath = {})
		{
			if (textureInfo.index >= 0)
			{
				path = m_ModelPath.parent_path() / m_Model.images[m_Model.textures[textureInfo.index].source].uri;
				if (!modelProjectPath.empty())
				{
					path = modelProjectPath / path.filename();
					path = std::filesystem::relative(path, ProjectConstants::GetInstance().GetAssetsPath());
				}
				return true;
			}

			return false;
		}

		template<typename T>
		void GetVertexData(const tinygltf::Model& model, const tinygltf::Accessor& accessor, std::vector<T>& data)
		{
			const tinygltf::BufferView& bufferView = model.bufferViews[accessor.bufferView];
			const size_t byteStride = accessor.ByteStride(bufferView);
			const size_t fileOffset = bufferView.byteOffset + accessor.byteOffset;
			const size_t bytesToRead = byteStride * accessor.count;

			data.resize(accessor.count);

			if (byteStride == sizeof(T))
				memcpy(data.data(), model.buffers[bufferView.buffer].data.data() + fileOffset, bytesToRead);
			else
			{
				size_t writeOffset = 0;
				size_t readOffset = fileOffset;
				size_t readCap = fileOffset + bytesToRead;
				while (readOffset < readCap)
				{
					memcpy(data.data() + writeOffset, model.buffers[bufferView.buffer].data.data() + readOffset, sizeof(T));
					readOffset += byteStride;
					writeOffset += 1;
				}
			}
		}
	};
}
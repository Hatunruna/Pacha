#define TINYGLTF_IMPLEMENTATION

#include "GltfParser.h"
#include "core/Assert.h"
#include "assets/AssetManager.h"
#include "graphics/ui/widgets/importers/TextureImporter.h"

#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>

namespace Pacha
{
	bool GLBLoadImageData(tinygltf::Image* /*image*/, const int /*image_idx*/, std::string* /*err*/, std::string* /*warn*/, int /*req_width*/, int /*req_height*/, const unsigned char* /*bytes*/, int /*size*/, void* /*user_data*/)
	{
		//We don't load image data, we wait until we process materials so we know the image type and can preserve srgb
		return true;
	}

	bool GltfParser::Initialize()
	{
		std::string error;
		std::string warning;
		tinygltf::TinyGLTF loader;
		std::string modelExtension = m_ModelPath.extension().string();

		if (modelExtension == ".gltf")
		{
			m_Type = GLTFType::GLTF;
			if (!loader.LoadASCIIFromFile(&m_Model, &error, nullptr, m_ModelPath.string()))
			{
				LOGERROR("Failed to load model '" << m_ModelPath << "' | Error: " << error);
				m_Error = ModelParserError::FAILED_TO_LOAD;
				return false;
			}
		}
		else
		{
			m_Type = GLTFType::GLB;
			loader.SetImageLoader(&GLBLoadImageData, nullptr);

			if (!loader.LoadBinaryFromFile(&m_Model, &error, nullptr, m_ModelPath.string()))
			{
				LOGERROR("Failed to load model '" << m_ModelPath << "' | Error: " << error);
				m_Error = ModelParserError::FAILED_TO_LOAD;
				return false;
			}
		}

		if (m_Model.scenes.size() > 1)
		{
			LOGERROR("Failed to process model '" << m_ModelPath << "': GLTF models should only have 1 scene");
			m_Error = ModelParserError::FAILED_TO_PROCESS;
			return false;
		}

		return true;
	}

	bool GltfParser::ProcessModel(const std::filesystem::path& assetsProjectPath)
	{
		if (!ProcessMaterials(assetsProjectPath))
			return false;

		m_Nodes.resize(m_Model.nodes.size());
		for (int const& nodeIndex : m_Model.scenes[0].nodes)
		{
			if (!ProcessNode(m_Model.nodes[nodeIndex], m_Nodes[nodeIndex]))
				return false;
		}

		return ProcessSubMeshes();
	}

	bool GltfParser::ProcessMaterials(const std::filesystem::path& assetsProjectPath)
	{
		std::filesystem::path texturePath;
		AssetManager& assetManager = AssetManager::GetInstance();

		m_Materials.resize(m_Model.materials.size());
		for (size_t i = 0; i < m_Model.materials.size(); ++i)
		{
			GfxModelData::Material& modelMaterial = m_Materials[i];
			const tinygltf::Material& material = m_Model.materials[i];

			modelMaterial.name = material.name;
			modelMaterial.alphaCutoff = static_cast<float>(material.alphaCutoff);
			modelMaterial.doubleSided = static_cast<float>(material.doubleSided);
			modelMaterial.metallic = static_cast<float>(material.pbrMetallicRoughness.metallicFactor);
			modelMaterial.roughness = static_cast<float>(material.pbrMetallicRoughness.roughnessFactor);

			const std::vector<double>& baseColorValue = material.pbrMetallicRoughness.baseColorFactor;
			modelMaterial.diffuse = glm::vec4(baseColorValue[0], baseColorValue[1], baseColorValue[2], baseColorValue[3]);

			const std::vector<double>& emissiveFactorValue = material.emissiveFactor;
			modelMaterial.emissive = glm::vec3(emissiveFactorValue[0], emissiveFactorValue[1], emissiveFactorValue[2]);

			if (material.alphaMode == "OPAQUE")
				modelMaterial.renderType = GfxRenderType::OPAQUE;
			else if (material.alphaMode == "MASK")
				modelMaterial.renderType = GfxRenderType::ALPHA_TEST;
			else if (material.alphaMode == "BLEND")
				modelMaterial.renderType = GfxRenderType::TRANSPARENT;

			if (m_Type == GLTFType::GLTF)
			{
				if (GetTexturePath(material.pbrMetallicRoughness.baseColorTexture, texturePath, assetsProjectPath))
					if (TextureAsset* textureAsset = assetManager.Get<TextureAsset>(texturePath))
						modelMaterial.diffuseTexture = textureAsset->GetUUID();

				if (GetTexturePath(material.pbrMetallicRoughness.metallicRoughnessTexture, texturePath, assetsProjectPath))
					if (TextureAsset* textureAsset = assetManager.Get<TextureAsset>(texturePath))
						modelMaterial.metallicRoughnessTexture = textureAsset->GetUUID();

				if (GetTexturePath(material.normalTexture, texturePath, assetsProjectPath))
					if (TextureAsset* textureAsset = assetManager.Get<TextureAsset>(texturePath))
						modelMaterial.normalTexture = textureAsset->GetUUID();

				if (GetTexturePath(material.occlusionTexture, texturePath, assetsProjectPath))
					if (TextureAsset* textureAsset = assetManager.Get<TextureAsset>(texturePath))
						modelMaterial.occlusionTexture = textureAsset->GetUUID();

				if (GetTexturePath(material.emissiveTexture, texturePath, assetsProjectPath))
					if (TextureAsset* textureAsset = assetManager.Get<TextureAsset>(texturePath))
						modelMaterial.emissiveTexture = textureAsset->GetUUID();
			}
			else
			{
				if (!ImportBuiltInTexture(material.pbrMetallicRoughness.baseColorTexture, assetsProjectPath, true, modelMaterial.diffuseTexture) && m_Error != ModelParserError::NONE)
					return false;

				if (!ImportBuiltInTexture(material.pbrMetallicRoughness.metallicRoughnessTexture, assetsProjectPath, false, modelMaterial.metallicRoughnessTexture) && m_Error != ModelParserError::NONE)
					return false;

				if (!ImportBuiltInTexture(material.normalTexture, assetsProjectPath, false, modelMaterial.normalTexture) && m_Error != ModelParserError::NONE)
					return false;

				if (!ImportBuiltInTexture(material.occlusionTexture, assetsProjectPath, false, modelMaterial.occlusionTexture) && m_Error != ModelParserError::NONE)
					return false;

				if (!ImportBuiltInTexture(material.emissiveTexture, assetsProjectPath, true, modelMaterial.emissiveTexture) && m_Error != ModelParserError::NONE)
					return false;
			}
		}

		return true;
	}

	std::unordered_set<ModelDependency> GltfParser::GetDependencies()
	{
		if (m_Type == GLTFType::GLB)
			return {};

		std::unordered_set<ModelDependency> dependencies;
		for (const auto& material : m_Model.materials)
		{
			const tinygltf::TextureInfo& albedoTexture = material.pbrMetallicRoughness.baseColorTexture;
			const tinygltf::TextureInfo& metallicRoughnessTexture = material.pbrMetallicRoughness.metallicRoughnessTexture;
			const tinygltf::TextureInfo& emissiveTexture = material.emissiveTexture;
			const tinygltf::OcclusionTextureInfo& occlusionTexture = material.occlusionTexture;
			const tinygltf::NormalTextureInfo& normalTexture = material.normalTexture;

			std::filesystem::path texturePath;
			if (GetTexturePath(albedoTexture, texturePath))
				dependencies.insert({ texturePath, ModelDependencyType_COLOR_TEXTURE });

			if (GetTexturePath(metallicRoughnessTexture, texturePath))
				dependencies.insert({ texturePath, ModelDependencyType_DATA_TEXTURE });

			if (GetTexturePath(emissiveTexture, texturePath))
				dependencies.insert({ texturePath, ModelDependencyType_COLOR_TEXTURE });

			if (GetTexturePath(occlusionTexture, texturePath))
				dependencies.insert({ texturePath, ModelDependencyType_DATA_TEXTURE });

			if (GetTexturePath(normalTexture, texturePath))
				dependencies.insert({ texturePath, ModelDependencyType_DATA_TEXTURE });
		}

		for (const auto& buffer : m_Model.buffers)
			dependencies.insert({ m_ModelPath.parent_path() / buffer.uri, ModelDependencyType_BINARY_DATA });

		return dependencies;
	}

	bool GltfParser::ProcessNode(const tinygltf::Node& node, GfxModelData::Node& modelNode)
	{
		if (node.camera != -1 || node.light != -1 || node.emitter != -1)
		{
			LOGERROR("Failed to process model '" << m_ModelPath << "': GLTF models should only contain meshes");
			m_Error = ModelParserError::FAILED_TO_PROCESS;
			return false;
		}

		if (!node.name.empty())
			modelNode.name = node.name;
		else
			modelNode.name = "Node " + std::to_string(m_Nodes.size());

		if (node.mesh != -1)
		{
			if (!ProcessMesh(m_Model.meshes[node.mesh], modelNode))
				return false;
		}

		/*if (node.skin != -1)
			ProcessSkin();*/

		if (node.matrix.size() > 0)
		{
			modelNode.transform = glm::mat4
			(
				static_cast<float>(node.matrix[0]),
				static_cast<float>(node.matrix[1]),
				static_cast<float>(node.matrix[2]),
				static_cast<float>(node.matrix[3]),
				static_cast<float>(node.matrix[4]),
				static_cast<float>(node.matrix[5]),
				static_cast<float>(node.matrix[6]),
				static_cast<float>(node.matrix[7]),
				static_cast<float>(node.matrix[8]),
				static_cast<float>(node.matrix[9]),
				static_cast<float>(node.matrix[10]),
				static_cast<float>(node.matrix[11]),
				static_cast<float>(node.matrix[12]),
				static_cast<float>(node.matrix[13]),
				static_cast<float>(node.matrix[14]),
				static_cast<float>(node.matrix[15])
			);
		}
		else
		{
			if (node.translation.size() > 0)
			{
				glm::vec3 translation = glm::vec3(
					static_cast<float>(node.translation[0]),
					static_cast<float>(node.translation[1]),
					static_cast<float>(node.translation[2])
				);

				modelNode.transform = glm::translate(translation);
			}

			if (node.rotation.size() > 0)
			{
				glm::quat rotation = glm::normalize(glm::quat(
					static_cast<float>(node.rotation[3]),
					static_cast<float>(node.rotation[0]),
					static_cast<float>(node.rotation[1]),
					static_cast<float>(node.rotation[2])
				));

				modelNode.transform *= glm::toMat4(rotation);
			}

			if (node.scale.size() > 0)
			{
				glm::vec3 scale = glm::vec3(
					static_cast<float>(node.scale[0]),
					static_cast<float>(node.scale[1]),
					static_cast<float>(node.scale[2])
				);

				modelNode.transform *= glm::scale(scale);
			}
		}

		for (int const& childIndex : node.children)
		{
			if (!ProcessNode(m_Model.nodes[childIndex], m_Nodes[childIndex]))
				return false;
		}

		return true;
	}

	bool GltfParser::ProcessMesh(const tinygltf::Mesh& mesh, GfxModelData::Node& modelNode)
	{
		for (auto const& primitive : mesh.primitives)
		{
			GfxModelData::Mesh subMesh;
			SubMeshInputData subMeshInputData;

			subMesh.vertexLayout = m_UseCompressedVertexData ? GfxVertexLayout_COMPRESSED : GfxVertexLayout_UNCOMPRESSED;
			if (primitive.material != -1)
			{
				subMesh.material = primitive.material;
				if (m_Materials[subMesh.material].renderType == GfxRenderType::ALPHA_TEST)
					subMesh.vertexLayout = static_cast<GfxVertexLayout>(subMesh.vertexLayout << 1);
			}

			if (primitive.indices != -1)
				GetIndicesData(m_Model, primitive.indices, subMeshInputData.indices);

			for (auto const& [attributeName, attributeIndex] : primitive.attributes)
			{
				const tinygltf::Accessor& accessor = m_Model.accessors[attributeIndex];

				const bool isPositions = attributeName.compare("POSITION") == 0;
				const bool isNormals = attributeName.compare("NORMAL") == 0;
				const bool isTangents = attributeName.compare("TANGENT") == 0;
				const bool isTexcoords0 = attributeName.compare("TEXCOORD_0") == 0;

				if (isPositions)
				{
					glm::vec3 min = glm::vec3(accessor.minValues[0], accessor.minValues[1], accessor.minValues[2]);
					glm::vec3 max = glm::vec3(accessor.maxValues[0], accessor.maxValues[1], accessor.maxValues[2]);
					subMesh.aabb = AABB(min, max);

					GetVertexData(m_Model, accessor, subMeshInputData.positions);
				}
				else if (isNormals && ShouldGetNormals(subMesh.vertexLayout))
					GetVertexData(m_Model, accessor, subMeshInputData.normals);
				else if (isTangents && ShouldGetTangents(subMesh.vertexLayout))
					GetVertexData(m_Model, accessor, subMeshInputData.tangents);
				else if (isTexcoords0 && ShouldGetTexcoords0(subMesh.vertexLayout))
					GetVertexData(m_Model, accessor, subMeshInputData.texcoords0);
			}

			if (!ValidateSubMeshInputData(subMesh.vertexLayout, subMeshInputData))
				return false;

			if (!mesh.name.empty())
				subMesh.name = mesh.name;
			else
				subMesh.name = "Mesh " + std::to_string(m_Meshes.size());

			switch (primitive.mode)
			{
				case TINYGLTF_MODE_TRIANGLES:
					subMesh.primitiveType = GfxPrimitiveType::TRIANGLES;
					break;
				case TINYGLTF_MODE_POINTS:
					subMesh.primitiveType = GfxPrimitiveType::POINTS;
					break;
				case TINYGLTF_MODE_LINE:
					subMesh.primitiveType = GfxPrimitiveType::LINES;
					break;
				case TINYGLTF_MODE_LINE_LOOP:
					subMesh.primitiveType = GfxPrimitiveType::LINE_STRIP;
					break;
				case TINYGLTF_MODE_LINE_STRIP:
					subMesh.primitiveType = GfxPrimitiveType::LINE_STRIP;
					break;
				case TINYGLTF_MODE_TRIANGLE_STRIP:
					subMesh.primitiveType = GfxPrimitiveType::TRIANGLE_STRIP;
					break;
				case TINYGLTF_MODE_TRIANGLE_FAN:
					subMesh.primitiveType = GfxPrimitiveType::TRIANGLE_FAN;
					break;
			}

			modelNode.meshes.push_back(static_cast<uint32_t>(m_Meshes.size()));

			m_Meshes.push_back(subMesh);
			m_MeshInputData.push_back(subMeshInputData);
		}

		return true;
	}

	void GltfParser::GetIndicesData(const tinygltf::Model& model, const int indicesIndex, std::vector<uint32_t>& indices)
	{
		const tinygltf::Accessor& accessor = model.accessors[indicesIndex];
		const tinygltf::BufferView& bufferView = model.bufferViews[accessor.bufferView];

		const size_t byteStride = accessor.ByteStride(bufferView);
		const size_t fileOffset = bufferView.byteOffset + accessor.byteOffset;
		const size_t bytesToRead = byteStride * accessor.count;

		auto CopyIndices = [&](void* destination, size_t elementSize)
		{
			if (byteStride == elementSize)
				memcpy(destination, model.buffers[bufferView.buffer].data.data() + fileOffset, bytesToRead);
			else
			{
				size_t writeOffset = 0;
				size_t readOffset = fileOffset;
				size_t readCap = fileOffset + bytesToRead;
				while (readOffset < readCap)
				{
					memcpy(reinterpret_cast<uint8_t*>(destination) + writeOffset, model.buffers[bufferView.buffer].data.data() + readOffset, elementSize);
					readOffset += byteStride;
					writeOffset += elementSize;
				}
			}
		};

		switch (accessor.componentType)
		{
			case TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE:
			{
				std::vector<uint8_t> tempIndices(accessor.count);
				CopyIndices(tempIndices.data(), sizeof(uint8_t));

				indices.reserve(accessor.count);
				std::transform(tempIndices.begin(), tempIndices.end(), std::back_inserter(indices), [](uint8_t& x) -> uint32_t { return static_cast<uint32_t>(x); });
				break;
			}
			case TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT:
			{
				std::vector<uint16_t> tempIndices(accessor.count);
				CopyIndices(tempIndices.data(), sizeof(uint16_t));

				indices.reserve(accessor.count);
				std::transform(tempIndices.begin(), tempIndices.end(), std::back_inserter(indices), [](uint16_t& x) -> uint32_t { return static_cast<uint32_t>(x); });
				break;
			}
			case TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT:
			{
				indices.resize(accessor.count);
				CopyIndices(indices.data(), sizeof(uint32_t));
				break;
			}
		}
	}
}

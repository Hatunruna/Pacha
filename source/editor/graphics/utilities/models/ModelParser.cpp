#include "ModelParser.h"
#include "utilities/ThreadPool.h"

#include <glm/gtc/quaternion.hpp>
#include <meshoptimizer.h>

namespace Pacha
{
	ModelParser::ModelParser(const std::filesystem::path& path, const bool useCompressedVertexData)
		: m_ModelPath(path), m_UseCompressedVertexData(useCompressedVertexData)
	{ }

	ModelParser::~ModelParser()
	{
		delete[] m_VertexData.data;
		delete[] m_IndexData.data;
	}

	bool ModelParser::ProcessSubMeshes()
	{
		PACHA_ASSERT(m_MeshInputData.size() == m_Meshes.size(), "The number of meshses doesn't match the amount of input data");

		std::vector<size_t> meshIndex(m_Meshes.size(), 0);
		for (size_t i = 0; i < meshIndex.size(); ++i)
			meshIndex[i] = i;
		std::sort(meshIndex.begin(), meshIndex.end(), [&](const size_t& lhs, const size_t& rhs) { return m_Meshes[lhs].vertexLayout < m_Meshes[rhs].vertexLayout; });

		size_t totalIndices = 0;
		uint32_t maxIndexValue = 0;
		size_t totalPositionsSize = 0;
		size_t totalInterleavedSize = 0;
		GfxVertexLayout currentVertexLayout = m_Meshes[meshIndex[0]].vertexLayout;

		for (size_t index : meshIndex)
		{
			SubMeshInputData& inputData = m_MeshInputData[index];

			std::vector<meshopt_Stream> streams;
			if (!inputData.positions.empty())	streams.push_back({ inputData.positions.data(),	sizeof(glm::packed_vec3), sizeof(glm::packed_vec3) });
			if (!inputData.normals.empty())		streams.push_back({ inputData.normals.data(),	sizeof(glm::packed_vec3), sizeof(glm::packed_vec3) });
			if (!inputData.texcoords0.empty())	streams.push_back({ inputData.texcoords0.data(),sizeof(glm::packed_vec2), sizeof(glm::packed_vec2) });
			if (!inputData.tangents.empty())	streams.push_back({ inputData.tangents.data(),	sizeof(glm::packed_vec4), sizeof(glm::packed_vec4) });

			size_t vertexCount = 0;
			SubMeshInputData oldData = inputData;
			std::vector<unsigned int> remapTable(inputData.positions.size());

			if (inputData.indices.empty())
			{
				inputData.indices.resize(inputData.positions.size());
				vertexCount = meshopt_generateVertexRemapMulti(&remapTable[0], NULL, inputData.positions.size(), inputData.positions.size(), streams.data(), streams.size());
				meshopt_remapIndexBuffer(inputData.indices.data(), NULL, inputData.indices.size(), &remapTable[0]);
			}
			else
			{
				vertexCount = meshopt_generateVertexRemapMulti(&remapTable[0], inputData.indices.data(), inputData.indices.size(), inputData.positions.size(), streams.data(), streams.size());
				meshopt_remapIndexBuffer(inputData.indices.data(), inputData.indices.data(), inputData.indices.size(), &remapTable[0]);
			}

			totalIndices += inputData.indices.size();
			maxIndexValue = std::max(maxIndexValue, *std::max_element(inputData.indices.begin(), inputData.indices.end()));

			inputData.positions.resize(vertexCount);
			inputData.normals.resize(vertexCount);
			inputData.tangents.resize(vertexCount);
			inputData.texcoords0.resize(vertexCount);

			ThreadPool workers(4);
			if (!inputData.positions.empty())	workers.AddFunction([&]() {meshopt_remapVertexBuffer(inputData.positions.data(), oldData.positions.data(), oldData.positions.size(), sizeof(glm::packed_vec3), &remapTable[0]); });
			if (!inputData.normals.empty())		workers.AddFunction([&]() {meshopt_remapVertexBuffer(inputData.normals.data(), oldData.normals.data(), oldData.normals.size(), sizeof(glm::packed_vec3), &remapTable[0]); });
			if (!inputData.texcoords0.empty())	workers.AddFunction([&]() {meshopt_remapVertexBuffer(inputData.texcoords0.data(), oldData.texcoords0.data(), oldData.texcoords0.size(), sizeof(glm::packed_vec2), &remapTable[0]); });
			if (!inputData.tangents.empty())	workers.AddFunction([&]() {meshopt_remapVertexBuffer(inputData.tangents.data(), oldData.tangents.data(), oldData.tangents.size(), sizeof(glm::packed_vec4), &remapTable[0]); });
			workers.Wait();

			meshopt_optimizeVertexCache(inputData.indices.data(), inputData.indices.data(), inputData.indices.size(), vertexCount);
			meshopt_optimizeOverdraw(inputData.indices.data(), inputData.indices.data(), inputData.indices.size(), &inputData.positions[0].x, vertexCount, sizeof(glm::packed_vec3), 1.05f);
			meshopt_optimizeVertexFetchRemap(&remapTable[0], inputData.indices.data(), inputData.indices.size(), vertexCount);

			if (!inputData.positions.empty())	workers.AddFunction([&]() {meshopt_remapVertexBuffer(inputData.positions.data(), inputData.positions.data(), oldData.positions.size(), sizeof(glm::packed_vec3), &remapTable[0]); });
			if (!inputData.normals.empty())		workers.AddFunction([&]() {meshopt_remapVertexBuffer(inputData.normals.data(), inputData.normals.data(), oldData.normals.size(), sizeof(glm::packed_vec3), &remapTable[0]); });
			if (!inputData.texcoords0.empty())	workers.AddFunction([&]() {meshopt_remapVertexBuffer(inputData.texcoords0.data(), inputData.texcoords0.data(), oldData.texcoords0.size(), sizeof(glm::packed_vec2), &remapTable[0]); });
			if (!inputData.tangents.empty())	workers.AddFunction([&]() {meshopt_remapVertexBuffer(inputData.tangents.data(), inputData.tangents.data(), oldData.tangents.size(), sizeof(glm::packed_vec4), &remapTable[0]); });
			workers.Wait();

			if (m_Meshes[index].vertexLayout != currentVertexLayout)
			{
				m_AlphaPositionDataOffset = totalPositionsSize;
				m_AlphaInterleavedDataOffset = totalInterleavedSize;

				currentVertexLayout = m_Meshes[index].vertexLayout;
			}

			const GfxVertexDescriptor& vertexDescriptor = VertexLayout::GetDescriptor(m_Meshes[index].vertexLayout);
			const GfxVertexStream& positionStream = vertexDescriptor.GetStream(0);
			const GfxVertexStream& interleavedDataStream = vertexDescriptor.GetStream(1);

			totalPositionsSize += inputData.positions.size() * positionStream.GetStride();
			totalInterleavedSize += inputData.positions.size() * interleavedDataStream.GetStride();
		}

		m_InterleavedDataOffset = totalPositionsSize;

		if (maxIndexValue <= USHRT_MAX)
			m_IndexType = GfxIndexType::U16;

		const size_t indexTypeSize = (m_IndexType == GfxIndexType::U32 ? sizeof(uint32_t) : sizeof(uint16_t));

		m_IndexData.size = totalIndices * indexTypeSize;
		m_IndexData.data = new uint8_t[m_IndexData.size];

		m_VertexData.size = totalPositionsSize + totalInterleavedSize;
		m_VertexData.data = new uint8_t[m_VertexData.size];

		size_t indexCount = 0;
		size_t firstVertex = 0;
		size_t positionDataOffset = 0;
		size_t interleavedDataOffset = m_InterleavedDataOffset;
		currentVertexLayout = m_Meshes[meshIndex[0]].vertexLayout;

		for (size_t index : meshIndex)
		{
			GfxModelData::Mesh& subMesh = m_Meshes[index];
			SubMeshInputData& inputData = m_MeshInputData[index];
			subMesh.indexCount = static_cast<uint32_t>(inputData.indices.size());

			//Indices
			if (m_IndexType == GfxIndexType::U16)
			{
				std::vector<uint16_t> convertedIndices;
				convertedIndices.reserve(inputData.indices.size());
				std::transform(inputData.indices.begin(), inputData.indices.end(), std::back_inserter(convertedIndices), [](uint32_t& x) -> uint16_t { return static_cast<uint16_t>(x); });

				memcpy(m_IndexData.data + indexCount, convertedIndices.data(), subMesh.indexCount);
			}
			else
				memcpy(m_IndexData.data + indexCount, inputData.indices.data(), subMesh.indexCount);

			subMesh.firstIndex = static_cast<uint32_t>(indexCount);
			indexCount += subMesh.indexCount;

			//Vertex
			if (subMesh.vertexLayout != currentVertexLayout)
			{
				firstVertex = 0;
				currentVertexLayout = subMesh.vertexLayout;
			}

			const GfxVertexDescriptor& vertexDescriptor = VertexLayout::GetDescriptor(subMesh.vertexLayout);
			PackVertexData(subMesh, inputData, m_VertexData.data + positionDataOffset, m_VertexData.data + interleavedDataOffset);
			subMesh.vertexCount = static_cast<uint32_t>(inputData.positions.size());
			subMesh.firstVertex = static_cast<uint32_t>(firstVertex);

			firstVertex += inputData.positions.size();
			positionDataOffset += vertexDescriptor.GetStream(0).GetStride() * inputData.positions.size();
			interleavedDataOffset += vertexDescriptor.GetStream(1).GetStride() * inputData.positions.size();
		}

		std::sort(m_Meshes.begin(), m_Meshes.end(), [&](const GfxModelData::Mesh& lhs, const GfxModelData::Mesh& rhs) { return lhs.vertexLayout < rhs.vertexLayout; });
		return true;
	}

	bool ModelParser::ValidateSubMeshInputData(const GfxVertexLayout layout, SubMeshInputData& inputData)
	{
		if (inputData.positions.empty())
		{
			LOGERROR("Failed to process model '" << m_ModelPath << "': Model is missing position data");
			m_Error = ModelParserError::FAILED_TO_PROCESS;
			return false;
		}

		if (ShouldGetNormals(layout) && inputData.normals.empty())
		{
			LOGERROR("Failed to process model '" << m_ModelPath << "': Model is missing normals data");
			m_Error = ModelParserError::FAILED_TO_PROCESS;
			return false;
		}

		if (ShouldGetTexcoords0(layout) && inputData.texcoords0.empty())
		{
			LOGERROR("Failed to process model '" << m_ModelPath << "': Model is missing texcoord0 data");
			m_Error = ModelParserError::FAILED_TO_PROCESS;
			return false;
		}

		if (ShouldGetTangents(layout) && inputData.tangents.empty())
		{
			LOGERROR("Failed to process model '" << m_ModelPath << "': Model is missing tangents data");
			m_Error = ModelParserError::FAILED_TO_PROCESS;
			return false;
		}

		return true;
	}

	void ModelParser::PackVertexData(const GfxModelData::Mesh& subMesh, const SubMeshInputData& inputData, uint8_t* positionData, uint8_t* interleavedData)
	{
		switch (subMesh.vertexLayout)
		{
			case GfxVertexLayout_UNCOMPRESSED:
				PackVertexDataUncompressed(inputData, positionData, interleavedData); break;
			case GfxVertexLayout_UNCOMPRESSED_ALPHA:
				PackVertexDataUncompressedAlpha(inputData, positionData, interleavedData);  break;
			case GfxVertexLayout_COMPRESSED:
				PackVertexDataCompressed(subMesh, inputData, positionData, interleavedData);  break;
			case GfxVertexLayout_COMPRESSED_ALPHA:
				PackVertexDataCompressedAlpha(subMesh, inputData, positionData, interleavedData);  break;
		}
	}

	void ModelParser::PackVertexDataUncompressed(const SubMeshInputData& inputData, uint8_t* positionData, uint8_t* interleavedData)
	{
		//Copy positions into first stream
		const size_t positionsSize = sizeof(glm::vec3) * inputData.positions.size();
		memcpy(positionData, inputData.positions.data(), positionsSize);

		size_t dataOffset = positionsSize;
		//Copy rest of data interleaved into second stream
		for (size_t i = 0; i < inputData.positions.size(); ++i)
		{
			//texcoords
			memcpy(interleavedData + dataOffset, &inputData.texcoords0[i], sizeof(glm::vec2));
			dataOffset += sizeof(glm::vec2);

			//normal
			memcpy(interleavedData + dataOffset, &inputData.normals[i], sizeof(glm::vec3));
			dataOffset += sizeof(glm::vec3);

			//tangent
			memcpy(interleavedData + dataOffset, &inputData.tangents[i], sizeof(glm::vec4));
			dataOffset += sizeof(glm::vec4);
		}
	}

	void ModelParser::PackVertexDataUncompressedAlpha(const SubMeshInputData& inputData, uint8_t* positionData, uint8_t* interleavedData)
	{
		//Copy positions and texcoords interleaved into first stream

		size_t dataOffset = 0;
		for (size_t i = 0; i < inputData.positions.size(); ++i)
		{
			memcpy(positionData + dataOffset, &inputData.positions[i], sizeof(glm::vec3));
			dataOffset += sizeof(glm::vec3);

			memcpy(positionData + dataOffset, &inputData.texcoords0[i], sizeof(glm::vec2));
			dataOffset += sizeof(glm::vec2);
		}

		//Copy rest of data interleaved into second stream
		for (size_t i = 0; i < inputData.positions.size(); ++i)
		{
			//normal
			memcpy(interleavedData + dataOffset, &inputData.normals[i], sizeof(glm::vec3));
			dataOffset += sizeof(glm::vec3);

			//tangent
			memcpy(interleavedData + dataOffset, &inputData.tangents[i], sizeof(glm::vec4));
			dataOffset += sizeof(glm::vec4);
		}
	}

	void ModelParser::PackVertexDataCompressed(const GfxModelData::Mesh& subMesh, const SubMeshInputData& inputData, uint8_t* positionData, uint8_t* interleavedData)
	{
		std::vector<glm::i16vec3> compressedPositions;
		compressedPositions.reserve(inputData.positions.size());

		const glm::packed_vec3 aabbCenter = subMesh.aabb.GetCenter();
		const glm::packed_vec3 aabbExtents = subMesh.aabb.GetExtents();

		for (const auto& position : inputData.positions)
		{
			glm::i16vec3 compressedPosition = glm::clamp(((position - aabbCenter) / aabbExtents) * glm::packed_vec3(SHRT_MAX), glm::packed_vec3(SHRT_MIN), glm::packed_vec3(SHRT_MAX));
			compressedPositions.push_back(compressedPosition);
		}

		//Copy compressed positions into first stream
		size_t compressedPositionsSize = sizeof(glm::i16vec3) * inputData.positions.size();
		memcpy(positionData, compressedPositions.data(), compressedPositionsSize);

		size_t dataOffset = compressedPositionsSize;
		//Copy rest of data interleaved into second stream
		for (size_t i = 0; i < inputData.positions.size(); ++i)
		{
			//Compressed Texcoords
			glm::i16vec2 compressedTexcoord = glm::clamp(inputData.texcoords0[i] * glm::packed_vec2(SHRT_MAX), glm::packed_vec2(SHRT_MIN), glm::packed_vec2(SHRT_MAX));
			memcpy(interleavedData + dataOffset, &compressedTexcoord, sizeof(glm::i16vec2));
			dataOffset += sizeof(glm::i16vec2);

			//QTangent
			const glm::vec3& normal = inputData.normals[i];
			const glm::vec4& tangent = inputData.tangents[i];
			const glm::vec3 bitangent = glm::cross(normal, glm::vec3(tangent)) * tangent.w;
			const glm::i16vec4 qTangent = GetQTangent(normal, tangent, bitangent);

			memcpy(interleavedData + dataOffset, &qTangent, sizeof(glm::i16vec4));
			dataOffset += sizeof(glm::i16vec4);
		}
	}

	void ModelParser::PackVertexDataCompressedAlpha(const GfxModelData::Mesh& subMesh, const SubMeshInputData& inputData, uint8_t* positionData, uint8_t* interleavedData)
	{
		const glm::packed_vec3 aabbCenter = subMesh.aabb.GetCenter();
		const glm::packed_vec3 aabbExtents = subMesh.aabb.GetExtents();

		//Copy compressed positions and texcoords interleaved into first stream

		size_t dataOffset = 0;
		for (size_t i = 0; i < inputData.positions.size(); ++i)
		{
			glm::i16vec3 compressedPosition = glm::clamp(((inputData.positions[i] - aabbCenter) / aabbExtents) * glm::packed_vec3(SHRT_MAX), glm::packed_vec3(SHRT_MIN), glm::packed_vec3(SHRT_MAX));
			memcpy(positionData + dataOffset, &compressedPosition, sizeof(glm::i16vec3));
			dataOffset += sizeof(glm::i16vec3);

			glm::i16vec2 compressedTexcoord = glm::clamp(inputData.texcoords0[i] * glm::packed_vec2(SHRT_MAX), glm::packed_vec2(SHRT_MIN), glm::packed_vec2(SHRT_MAX));
			memcpy(positionData + dataOffset, &compressedTexcoord, sizeof(glm::i16vec2));
			dataOffset += sizeof(glm::i16vec2);
		}

		//Copy rest of data into second stream
		for (size_t i = 0; i < inputData.positions.size(); ++i)
		{
			//QTangent
			const glm::vec3& normal = inputData.normals[i];
			const glm::vec4& tangent = inputData.tangents[i];
			const glm::vec3 bitangent = glm::cross(normal, glm::vec3(tangent)) * tangent.w;
			const glm::i16vec4 qTangent = GetQTangent(normal, tangent, bitangent);

			memcpy(interleavedData + dataOffset, &qTangent, sizeof(glm::i16vec4));
			dataOffset += sizeof(glm::i16vec4);
		}
	}

	//https://www.yosoygames.com.ar/wp/2018/03/vertex-formats-part-1-compression/
	glm::i16vec4 ModelParser::GetQTangent(const glm::vec3& normal, const glm::vec4& tangent, const glm::vec3& bitangent)
	{
		const float kBias = 1.0 / SHRT_MAX;
		float kNormalizingFactor = glm::sqrt(1 - kBias * kBias);

		glm::mat3 tbn = { normal, tangent, bitangent };
		glm::quat qTangent(tbn);
		qTangent = glm::normalize(qTangent);

		if (qTangent.w < 0)
			qTangent = -qTangent;

		if (qTangent.w < kBias)
		{
			qTangent.w = kBias;
			qTangent.x *= kNormalizingFactor;
			qTangent.y *= kNormalizingFactor;
			qTangent.z *= kNormalizingFactor;
		}

		glm::vec3 naturalBinormal = glm::cross(normal, glm::vec3(tangent));
		if (glm::dot(naturalBinormal, bitangent) <= 0)
			qTangent = -qTangent;

		glm::vec4 compressedQTangent = glm::vec4(qTangent.x, qTangent.y, qTangent.z, qTangent.w);
		return glm::clamp(compressedQTangent * glm::vec4(SHRT_MAX), glm::vec4(SHRT_MIN), glm::vec4(SHRT_MAX));
	}

	bool ModelParser::ShouldGetNormals(const GfxVertexLayout layout)
	{
		return layout & GfxVertexLayoutDataNeeds_NORMALS;
	}

	bool ModelParser::ShouldGetTangents(const GfxVertexLayout layout)
	{
		return layout & GfxVertexLayoutDataNeeds_TANGENTS;
	}

	bool ModelParser::ShouldGetTexcoords0(const GfxVertexLayout layout)
	{
		return layout & GfxVertexLayoutDataNeeds_UVS;
	}
}

#pragma once
#include "utilities/Enum.h"
#include "utilities/Hash.h"
#include "graphics/resources/mesh/GfxModel.h"

#include <string>
#include <vector>
#include <filesystem>
#include <unordered_map>
#include <glm/gtc/type_aligned.hpp>

namespace Pacha
{
	struct MeshData
	{
		size_t size = 0;
		uint8_t* data = nullptr;
	};

	struct SubMeshInputData
	{
		std::vector<uint32_t> indices = {};
		std::vector<glm::packed_vec3> positions = {};
		std::vector<glm::packed_vec3> normals = {};
		std::vector<glm::packed_vec4> tangents = {};
		std::vector<glm::packed_vec2> texcoords0 = {};
	};

	enum ModelDependencyType
	{
		ModelDependencyType_COLOR_TEXTURE = 1 << 0,
		ModelDependencyType_DATA_TEXTURE = 1 << 1,
		ModelDependencyType_TEXTURE = InclusiveBitOr(2),
		ModelDependencyType_BINARY_DATA = 1 << 3
	};

	struct ModelDependency
	{
		std::filesystem::path path = {};
		ModelDependencyType type = ModelDependencyType_COLOR_TEXTURE;

		bool operator==(const ModelDependency& rhs) const
		{
			return path == rhs.path && type == rhs.type;
		}
	};

	enum class ModelParserError
	{
		NONE,
		FAILED_TO_LOAD,
		FAILED_TO_PROCESS
	};

	class ModelParser
	{
	public:
		ModelParser(const std::filesystem::path& path, const bool useCompressedVertexData);
		virtual ~ModelParser();

		virtual bool Initialize() = 0;
		virtual bool ProcessModel(const std::filesystem::path& assetsProjectPath = {}) = 0;
		virtual std::unordered_set<ModelDependency> GetDependencies() = 0;

		const MeshData& GetIndexData() const { return m_IndexData; }
		const MeshData& GetVertexData() const { return m_VertexData; }
		const GfxIndexType& GetIndexType() const { return m_IndexType; }
		size_t GetInterleavedDataOffset() const { return m_InterleavedDataOffset; }
		size_t GetAlphaPositionDataOffset() const { return m_AlphaPositionDataOffset; }
		size_t GetAlphaInterleavedDataOffset() const { return m_AlphaInterleavedDataOffset; }

		const std::vector<GfxModelData::Node>& GetModelNodes() { return m_Nodes; }
		const std::vector<GfxModelData::Mesh>& GetModelMeshes() { return m_Meshes; }
		const std::vector<GfxModelData::Material>& GetModelMaterials() { return m_Materials; }

		ModelParserError GetError() { return m_Error; };
		const std::filesystem::path& GetPath() { return m_ModelPath; }

	protected:
		ModelParserError m_Error = {};
		std::filesystem::path m_ModelPath = {};

		bool m_UseCompressedVertexData = true;
		size_t m_InterleavedDataOffset = 0;
		size_t m_AlphaPositionDataOffset = 0;
		size_t m_AlphaInterleavedDataOffset = 0;
		GfxIndexType m_IndexType = GfxIndexType::U32;

		MeshData m_IndexData = {};
		MeshData m_VertexData = {};

		std::vector<GfxModelData::Node> m_Nodes;
		std::vector<GfxModelData::Mesh> m_Meshes;
		std::vector<GfxModelData::Material> m_Materials;

		std::vector<SubMeshInputData> m_MeshInputData;

		virtual bool ProcessMaterials(const std::filesystem::path& assetsProjectPath) = 0;

		bool ProcessSubMeshes();
		bool ValidateSubMeshInputData(const GfxVertexLayout layout, SubMeshInputData& inputData);
		void PackVertexData(const GfxModelData::Mesh& subMesh, const SubMeshInputData& inputData, uint8_t* positionData, uint8_t* interleavedData);

		bool ShouldGetNormals(const GfxVertexLayout layout);
		bool ShouldGetTangents(const GfxVertexLayout layout);
		bool ShouldGetTexcoords0(const GfxVertexLayout layout);

	private:
		void PackVertexDataUncompressed(const SubMeshInputData& inputData, uint8_t* positionData, uint8_t* interleavedData);
		void PackVertexDataUncompressedAlpha(const SubMeshInputData& inputData, uint8_t* positionData, uint8_t* interleavedData);
		void PackVertexDataCompressed(const GfxModelData::Mesh& subMesh, const SubMeshInputData& inputData, uint8_t* positionData, uint8_t* interleavedData);
		void PackVertexDataCompressedAlpha(const GfxModelData::Mesh& subMesh, const SubMeshInputData& inputData, uint8_t* positionData, uint8_t* interleavedData);

		glm::i16vec4 GetQTangent(const glm::vec3& normal, const glm::vec4& tangent, const glm::vec3& bitangent);
	};
}

namespace std
{
	template <>
	struct hash<Pacha::ModelDependency>
	{
		size_t operator()(const Pacha::ModelDependency& dependency) const
		{
			size_t hash = Pacha::Hash(dependency.type);
			Pacha::HashCombine(hash, dependency.path);
			return hash;
		}
	};
}
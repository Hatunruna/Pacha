#include "TechniqueCompiler.h"
#include "ShaderCompiler.h"

#include "core/Log.h"
#include "fileio/File.h"
#include "assets/AssetManager.h"
#include "core/ProjectConstants.h"
#include "serialization/JsonSerializer.h"

namespace Pacha
{
	bool TechniqueCompiler::Compile(const std::filesystem::path& techniquePath)
	{
		if (File::Exists(techniquePath))
		{
			if (!GetTechniqueType(techniquePath))
				return false;

			if (!ExtractTechniqueData(techniquePath))
				return false;

			if (!ResourcesReflection())
				return false;

			AssetManager& assetManager = AssetManager::GetInstance();
			ProjectConstants& projectConstants = ProjectConstants::GetInstance();

			std::filesystem::path relativePath = std::filesystem::relative(techniquePath, projectConstants.GetAssetsPath());

			TechniqueAsset* techniqueAsset = assetManager.GetOrCreate<TechniqueAsset>(relativePath);
			const std::filesystem::path libTechniquePath = projectConstants.GetLibraryPath() / (techniqueAsset->GetUUID().GetString() + ".tech");

			std::unordered_map<PachaId, GfxTechniquePass> passes;
			for (auto& compilerPass : m_CompilerPasses)
				passes[compilerPass.pass.passId] = compilerPass.pass;

			JsonSerializer serializer;
			serializer.StartSerialization();
			serializer(Serialization::MakeNVP("Passes", passes));
			serializer(Serialization::MakeNVP("ReflectionInfo", m_ReflectionInfo));
			serializer(Serialization::MakeNVP("Sources", m_Sources));
			serializer.EndSerialization();
			serializer.SaveToFile(libTechniquePath);
		}
		else
		{
			LOGERROR("Technique file not found at path '" << techniquePath << "'");
			return false;
		}

		return true;
	}

	const std::vector<GfxTechniqueCompilerPass>& TechniqueCompiler::GetCompilerPasses()
	{
		return m_CompilerPasses;
	}

	bool TechniqueCompiler::ResourcesReflection()
	{
		std::string reflectionSource = m_Resources;
		reflectionSource += "\n void main(){}";

		ShaderCompileOptions options;
		options.entryPoint = L"main";
		options.reflection = true;

		ShaderCompiler compiler;
		if (!compiler.CompileShader(reflectionSource, options))
			return false;

		if (!compiler.GetReflectionData(m_ReflectionInfo))
			return false;

		for (auto& [bufferId, bufferInfo] : m_ReflectionInfo.buffers)
		{
			auto found = m_DynamicBuffers.find(bufferId);
			if (found != m_DynamicBuffers.end())
				bufferInfo.type = GfxBufferDescriptorType_CONSTANT_DYNAMIC;
		}

		return true;
	}

	bool TechniqueCompiler::GetTechniqueType(const std::filesystem::path& techniquePath)
	{
		std::string lowerCaseExtension = techniquePath.extension().string();
		std::transform(lowerCaseExtension.begin(), lowerCaseExtension.end(), lowerCaseExtension.begin(), ::tolower);

		switch (FNV1aHash(lowerCaseExtension))
		{
			case FNV1aHash(".tech"):
				m_TechniqueType = GfxTechniqueType::RASTERIZATION;
				break;
			case FNV1aHash(".ctech"):
				m_TechniqueType = GfxTechniqueType::COMPUTE;
				break;
			case FNV1aHash(".rtech"):
				m_TechniqueType = GfxTechniqueType::RAYTRACING;
				break;

			default:
				LOGERROR("Unrecognized technique extension type.");
				return false;
		}

		return true;
	}

	bool TechniqueCompiler::ExtractTechniqueData(const std::filesystem::path& techniquePath)
	{
		TechniqueParser& parser = m_TechniqueParsers.emplace_back();
		if (!parser.Initialize(techniquePath))
			return false;

		if (!parser.Parse())
			return false;

		m_Sources.resize(1);
		std::deque<Token> tokens = parser.GetTokens();
		std::deque<Token>::iterator token = tokens.begin();

		while (token != tokens.end())
		{
			switch (token->type)
			{
				case TokenType::INCLUDE_STRING:
				{
					std::string includeString(token->start, token->length);
					std::filesystem::path includePath(includeString);
					includePath = techniquePath.parent_path() / includePath;

					TechniqueParser& includeParser = m_TechniqueParsers.emplace_back();
					if (!includeParser.Initialize(includePath))
						return false;

					if (!includeParser.Parse(false))
						return false;

					tokens.erase(token);

					m_Sources.resize(m_Sources.size() + 1);
					const std::deque<Token>& includeTokens = includeParser.GetTokens();
					tokens.insert(tokens.begin(), includeTokens.begin(), includeTokens.end());
					token = tokens.begin();
					continue;
				}
				case TokenType::PASS:
				{
					if (!GetPassData(token))
						return false;
					break;
				}
				case TokenType::RESOURCES:
				{
					if (!GetTextBlock(token, m_Resources))
						return false;

					std::string lineNumber = "#line " + std::to_string(token->line) + " \"" + parser.GetTechniqueName() + '"' + '\n';
					m_Resources.insert(0, lineNumber);
					break;
				}
				case TokenType::SOURCES:
				{
					if (!GetTextBlock(token, m_Sources[m_SourceIndex]))
						return false;

					std::string lineNumber = "#line " + std::to_string(token->line) + " \"" + m_TechniqueParsers[m_Sources.size() - 1 - m_SourceIndex].GetTechniqueName() + '"' + '\n';
					m_Sources[m_SourceIndex].insert(0, lineNumber);

					++m_SourceIndex;
					break;
				}
				default:
				{
					LOGERROR("Unexpected token at line: " << token->line);
					return false;
				}
			}

			AdvanceToken(token);
		}

		ValidateDefineMasks();
		ProcessResources();
		ProcessSources();
		return true;
	}

	void TechniqueCompiler::ValidateDefineMasks()
	{
		for (auto& compilerPass : m_CompilerPasses)
		{
			for (auto& [stage, bitmask] : compilerPass.pass.stageDefineMask)
			{
				if (compilerPass.sharedDefinesMask.GetBitCount() > bitmask.GetBitCount())
					bitmask.Resize(compilerPass.sharedDefinesMask.GetBitCount());

				bitmask |= compilerPass.sharedDefinesMask;
			}
		}
	}

	void TechniqueCompiler::ProcessResources()
	{
		size_t offset = m_Resources.find("[[Dynamic]]");
		while (offset != std::string::npos)
		{
			size_t cbufferPos = m_Resources.find("cbuffer ", offset + 13);
			if (cbufferPos != std::string::npos)
			{
				const size_t afterNameSpace = m_Resources.find_first_of(" :\n\r\t", cbufferPos + 8);
				const std::string bufferName = std::string(m_Resources.begin() + cbufferPos + 8, m_Resources.begin() + afterNameSpace);

				const PachaId bufferId = PachaId::Create(bufferName);
				m_DynamicBuffers.insert(bufferId);
			}
			else
			{
				cbufferPos = m_Resources.find("ConstantBuffer<", offset + 13);
				if (cbufferPos != std::string::npos)
				{
					const size_t closingBracket = m_Resources.find(">", cbufferPos + 15);
					const size_t endSemiColon = m_Resources.find(";", closingBracket);
					std::string bufferName = std::string(m_Resources.begin() + closingBracket + 1, m_Resources.begin() + endSemiColon);
					bufferName.erase(remove_if(bufferName.begin(), bufferName.end(), isspace), bufferName.end());

					const PachaId bufferId = PachaId::Create(bufferName);
					m_DynamicBuffers.insert(bufferId);
				}
			}

			m_Resources.erase(m_Resources.begin() + offset, m_Resources.begin() + offset + 11);
			offset = m_Resources.find("[[Dynamic]]");
		}
	}

	void TechniqueCompiler::ProcessSources()
	{
		for (auto& source : m_Sources)
			source.insert(0, m_Resources);
	}

	bool TechniqueCompiler::GetPassData(std::deque<Token>::iterator& token)
	{
		if (!CheckForColon(token))
			return false;

		if (token->type != TokenType::STRING_VARIABLE)
		{
			LOGERROR("Unexpected token at line: " << token->line << " - Expected pass name");
			return false;
		}

		GfxTechniqueCompilerPass compilerPass;
		GfxRendererState& rendererState = compilerPass.pass.rendererState;

		compilerPass.pass.passId = PachaId::Create(std::string(token->start, token->length));
		compilerPass.sourceIndex = m_SourceIndex;

		AdvanceToken(token);
		if (token->type != TokenType::LEFT_CURLY_BRACKET)
		{
			LOGERROR("Unexpected token at line: " << token->line << " - Expected curly bracket");
			return false;
		}

		AdvanceToken(token);
		while (IsPassKeywordToken(token->type))
		{
			if (token->type == TokenType::DEFINES)
			{
				if (!GetDefines(token, compilerPass))
					return false;
			}
			else
			{
				if (m_TechniqueType == GfxTechniqueType::RASTERIZATION)
				{
					switch (token->type)
					{
						case TokenType::ZWRITE:						if (!GetBoolValue(token, rendererState.depthWrite))				return false;	break;
						case TokenType::ZCLAMP:						if (!GetBoolValue(token, rendererState.depthClamp))				return false;	break;
						case TokenType::ALPHA_TO_COVERAGE:			if (!GetBoolValue(token, rendererState.alphaToCoverage))		return false;	break;

						case TokenType::BLEND:						if (!GetBlendValue(token, rendererState))						return false;	break;
						case TokenType::BLEND_OPERATION:			if (!GetBlendOperationValue(token, rendererState))				return false;	break;
						case TokenType::COLOR_MASK:					if (!GetColorMaskValue(token, rendererState))					return false;	break;

						case TokenType::ZTEST:						if (!GetDepthTestValue(token, rendererState))					return false;	break;
						case TokenType::LOGICAL_BLEND_OPERATION:	if (!GetLogicalBlendOperation(token, rendererState))			return false;	break;
						case TokenType::CULL:						if (!GetCullMode(token, rendererState))							return false;	break;

						case TokenType::FILL_MODE:					if (!GetFillMode(token, rendererState))							return false;	break;
						case TokenType::WINDING_ORDER:				if (!GetWindingOrder(token, rendererState))						return false;	break;

						case TokenType::LINE_WIDTH:					if (!GetNumberValue(token, rendererState.lineWidth))			return false;	break;
						case TokenType::PATCH_CONTROL_POINTS:		if (!GetNumberValue(token, rendererState.patchControlPoints))	return false;	break;
						case TokenType::DEPTH_BIAS:					if (!GetDepthBias(token, rendererState))						return false;	break;

						case TokenType::VERTEX:						if (!GetStringValue(token, compilerPass.shaderEntryPoints[GfxShaderType::VERTEX]))			return false;	break;
						case TokenType::HULL:						if (!GetStringValue(token, compilerPass.shaderEntryPoints[GfxShaderType::HULL]))			return false;	break;
						case TokenType::DOMAIN:						if (!GetStringValue(token, compilerPass.shaderEntryPoints[GfxShaderType::DOMAIN]))			return false;	break;
						case TokenType::GEOMETRY:					if (!GetStringValue(token, compilerPass.shaderEntryPoints[GfxShaderType::GEOMETRY]))		return false;	break;
						case TokenType::PIXEL:						if (!GetStringValue(token, compilerPass.shaderEntryPoints[GfxShaderType::PIXEL]))			return false;	break;
						case TokenType::AMPLIFICATION:				if (!GetStringValue(token, compilerPass.shaderEntryPoints[GfxShaderType::AMPLIFICATION]))	return false;	break;
						case TokenType::MESH:						if (!GetStringValue(token, compilerPass.shaderEntryPoints[GfxShaderType::MESH]))			return false;	break;

						default:
							LOGERROR("Unexpected token at line: " << token->line << " - Expected a keyword");
							return false;
					}
				}
				else if (m_TechniqueType == GfxTechniqueType::COMPUTE)
				{
					switch (token->type)
					{
						case TokenType::COMPUTE:					if (!GetStringValue(token, compilerPass.shaderEntryPoints[GfxShaderType::COMPUTE]))			return false;	break;

						default:
							LOGERROR("Unexpected token at line: " << token->line << " - Expected a keyword");
							return false;
					}
				}
				else if (m_TechniqueType == GfxTechniqueType::RAYTRACING)
				{
					switch (token->type)
					{
						case TokenType::RAY_GEN:					if (!GetStringValue(token, compilerPass.shaderEntryPoints[GfxShaderType::RAY_GEN]))			return false;	break;
						case TokenType::ANY_HIT:					if (!GetStringValue(token, compilerPass.shaderEntryPoints[GfxShaderType::ANY_HIT]))			return false;	break;
						case TokenType::CLOSEST_HIT:				if (!GetStringValue(token, compilerPass.shaderEntryPoints[GfxShaderType::CLOSEST_HIT]))		return false;	break;
						case TokenType::MISS:						if (!GetStringValue(token, compilerPass.shaderEntryPoints[GfxShaderType::MISS]))			return false;	break;
						case TokenType::INTERSECTION:				if (!GetStringValue(token, compilerPass.shaderEntryPoints[GfxShaderType::INTERSECTION]))	return false;	break;
						case TokenType::CALLABLE:					if (!GetStringValue(token, compilerPass.shaderEntryPoints[GfxShaderType::CALLABLE]))		return false;	break;

						default:
							LOGERROR("Unexpected token at line: " << token->line << " - Expected a keyword");
							return false;
					}
				}
			}

			AdvanceToken(token);
		}

		if (token->type != TokenType::RIGHT_CURLY_BRACKET)
		{
			LOGERROR("Unexpected token at line: " << token->line << " - Expected a keyword");
			return false;
		}

		m_CompilerPasses.push_back(compilerPass);
		return true;
	}

	bool TechniqueCompiler::GetTextBlock(std::deque<Token>::iterator& token, std::string& value)
	{
		if (!CheckForColon(token))
			return false;

		if (token->type == TokenType::TEXT_BLOCK)
			value = std::string(token->start, token->length);
		else
		{
			LOGERROR("Unexpected token at line: " << token->line << " - Expected a text block");
			return false;
		}

		return true;
	}

	bool TechniqueCompiler::GetDefines(std::deque<Token>::iterator& token, GfxTechniqueCompilerPass& compilerPass)
	{
		GfxTechniquePass& pass = compilerPass.pass;

		if (!CheckForColon(token))
			return false;

		if (token->type != TokenType::LEFT_CURLY_BRACKET)
		{
			LOGERROR("Unexpected token at line: " << token->line << " - Expected a left curly bracket");
			return false;
		}

		bool isReadingStage = false;
		bool isReadingGroup = false;
		GfxShaderType currentStage = {};
		std::vector<size_t> uniqueGroup = {};

		AdvanceToken(token);
		while (token->type != TokenType::RIGHT_CURLY_BRACKET)
		{
			if (IsDefineStageToken(token->type))
			{
				isReadingStage = true;
				if (!GetDefineStageFromToken(token, currentStage))
					return false;

				AdvanceToken(token);
				if (token->type != TokenType::COLON)
				{
					LOGERROR("Unexpected token at line: " << token->line << " - Expected a colon");
					return false;
				}
			}
			else if (token->type == TokenType::STRING_VARIABLE)
			{
				size_t bitIndex = 0;
				std::string define = std::string(token->start, token->length);
				PachaId defineId = PachaId::Create(define);

				const auto& found = std::find(pass.defines.begin(), pass.defines.end(), defineId);
				if (found == pass.defines.end())
				{
					bitIndex = pass.defines.size();
					pass.defines.push_back(defineId);
				}
				else
					bitIndex = found - pass.defines.begin();

				if (isReadingGroup)
					uniqueGroup.push_back(bitIndex);

				if (isReadingStage)
					pass.stageDefineMask[currentStage].Set(bitIndex, true);
				else
					compilerPass.sharedDefinesMask.Set(bitIndex, true);
			}
			else if (token->type == TokenType::LEFT_PARENTHESIS)
			{
				isReadingGroup = true;
			}
			else if (token->type == TokenType::RIGHT_PARENTHESIS)
			{
				isReadingGroup = false;
				pass.uniqueGroups.push_back(uniqueGroup);
				uniqueGroup.clear();
			}
			else
			{
				LOGERROR("Unexpected token at line: " << token->line << " - Expected a define stage or a string variable");
				return false;
			}

			AdvanceToken(token);
		}

		return true;
	}

	bool TechniqueCompiler::GetBoolValue(std::deque<Token>::iterator& token, bool& boolValue)
	{
		if (!CheckForColon(token))
			return false;

		if (IsBoolToken(token->type))
			boolValue = token->type == TokenType::ON;
		else
		{
			LOGERROR("Unexpected token at line: " << token->line << " - Expected a boolean value");
			return false;
		}

		return true;
	}

	bool TechniqueCompiler::GetStringValue(std::deque<Token>::iterator& token, std::string& stringValue)
	{
		if (!CheckForColon(token))
			return false;

		if (token->type == TokenType::STRING_VARIABLE)
			stringValue = std::string(token->start, token->length);
		else
		{
			LOGERROR("Unexpected token at line: " << token->line << " - Expected a string value");
			return false;
		}

		return true;
	}

	bool TechniqueCompiler::GetDepthBias(std::deque<Token>::iterator& token, GfxRendererState& rendererState)
	{
		if (!CheckForColon(token))
			return false;

		if (!GetNumberValue<float, false>(token, rendererState.depthBias))
			return false;

		AdvanceToken(token);
		if (token->type == TokenType::COMMA)
		{
			AdvanceToken(token);
			if (!GetNumberValue<float, false>(token, rendererState.depthBiasSlope))
				return false;
		}
		else
		{
			LOGERROR("Unexpected token at line: " << token->line << " - Expected a comma");
			return false;
		}

		return false;
	}

	bool TechniqueCompiler::GetDepthTestValue(std::deque<Token>::iterator& token, GfxRendererState& rendererState)
	{
		if (!CheckForColon(token))
			return false;

		if (IsBoolToken(token->type))
		{
			if (token->type == TokenType::OFF)
				rendererState.depthTest = GfxDepthTest::OFF;
			else
			{
				LOGERROR("Unexpected token at line: " << token->line << " - Expected an Off value since DepthTest can only be deactivated or set to an specific mode");
				return false;
			}
		}
		else
		{
			switch (token->type)
			{
				case TokenType::NEVER:		rendererState.depthTest = GfxDepthTest::NEVER;		break;
				case TokenType::LESS:		rendererState.depthTest = GfxDepthTest::LESS;		break;
				case TokenType::GREATER:	rendererState.depthTest = GfxDepthTest::GREATER;	break;
				case TokenType::EQUAL:		rendererState.depthTest = GfxDepthTest::EQUAL;		break;
				case TokenType::ALWAYS:		rendererState.depthTest = GfxDepthTest::ALWAYS;		break;
				case TokenType::LEQUAL:		rendererState.depthTest = GfxDepthTest::LEQUAL;		break;
				case TokenType::GEQUAL:		rendererState.depthTest = GfxDepthTest::GEQUAL;		break;
				case TokenType::NOT_EQUAL:	rendererState.depthTest = GfxDepthTest::NOT_EQUAL;	break;
				default:
					LOGERROR("Unexpected token at line: " << token->line << " - Expected a DepthTest mode");
					return false;
			}
		}

		return true;
	}

	bool TechniqueCompiler::GetLogicalBlendOperation(std::deque<Token>::iterator& token, GfxRendererState& rendererState)
	{
		if (!CheckForColon(token))
			return false;

		if (IsBoolToken(token->type))
		{
			if (token->type == TokenType::OFF)
				rendererState.logicalBlending = GfxLogicalBlending::OFF;
			else
			{
				LOGERROR("Unexpected token at line: " << token->line << " - Expected an Off value since LogicalBlending can only be deactivated or set to an specific mode");
				return false;
			}
		}
		else
		{
			switch (token->type)
			{
				case TokenType::CLEAR:	rendererState.logicalBlending = GfxLogicalBlending::CLEAR;	break;
				case TokenType::AND:	rendererState.logicalBlending = GfxLogicalBlending::AND;	break;
				case TokenType::XOR:	rendererState.logicalBlending = GfxLogicalBlending::XOR;	break;
				case TokenType::OR:		rendererState.logicalBlending = GfxLogicalBlending::OR;		break;
				case TokenType::NOR:	rendererState.logicalBlending = GfxLogicalBlending::NOR;	break;
				case TokenType::NAND:	rendererState.logicalBlending = GfxLogicalBlending::NAND;	break;
				case TokenType::SET:	rendererState.logicalBlending = GfxLogicalBlending::SET;	break;
				default:
					LOGERROR("Unexpected token at line: " << token->line << " - Expected a LogicalBlending mode");
					return false;
			}
		}

		return true;
	}

	bool TechniqueCompiler::GetBlendValue(std::deque<Token>::iterator& token, GfxRendererState& rendererState)
	{
		uint32_t renderTarget = 0;
		if (!CheckForRenderTarget(token, rendererState, renderTarget))
			return false;

		if (IsBoolToken(token->type))
		{
			if (token->type == TokenType::OFF)
				rendererState.renderTargetStates[renderTarget].blendEnabled = false;
			else
			{
				LOGERROR("Unexpected token at line: " << token->line << " - Expected an Off value since blend modes can only be deactivated");
				return false;
			}
		}
		else if (IsBlendModeToken(token->type))
		{
			if (!GetBlendFactor(token, rendererState.renderTargetStates[renderTarget].blendFactors.sourceColor))
				return false;

			AdvanceToken(token);
			if (!GetBlendFactor(token, rendererState.renderTargetStates[renderTarget].blendFactors.destinationColor))
				return false;

			AdvanceToken(token);
			if (token->type == TokenType::COMMA)
			{
				AdvanceToken(token);
				if (!GetBlendFactor(token, rendererState.renderTargetStates[renderTarget].blendFactors.sourceAlpha))
					return false;

				AdvanceToken(token);
				if (!GetBlendFactor(token, rendererState.renderTargetStates[renderTarget].blendFactors.destinationAlpha))
					return false;
			}
		}
		else
		{
			LOGERROR("Unexpected token at line: " << token->line << " - Expected a RenderTarget number, a boolean or a BlendFactor");
			return false;
		}

		return true;
	}

	bool TechniqueCompiler::GetBlendFactor(std::deque<Token>::iterator& token, GfxBlendingFactor& blendFactor)
	{
		switch (token->type)
		{
			case TokenType::ONE:					blendFactor = GfxBlendingFactor::ONE;					break;
			case TokenType::ZERO:					blendFactor = GfxBlendingFactor::ZERO;					break;
			case TokenType::SRC_COLOR:				blendFactor = GfxBlendingFactor::SRC_COLOR;				break;
			case TokenType::ONE_MINUS_SRC_COLOR:	blendFactor = GfxBlendingFactor::ONE_MINUS_SRC_COLOR;	break;
			case TokenType::DST_COLOR:				blendFactor = GfxBlendingFactor::DST_COLOR;				break;
			case TokenType::ONE_MINUS_DST_COLOR:	blendFactor = GfxBlendingFactor::ONE_MINUS_DST_COLOR;	break;
			case TokenType::SRC_ALPHA:				blendFactor = GfxBlendingFactor::SRC_ALPHA;				break;
			case TokenType::ONE_MINUS_SRC_ALPHA:	blendFactor = GfxBlendingFactor::ONE_MINUS_SRC_ALPHA;	break;
			case TokenType::DST_ALPHA:				blendFactor = GfxBlendingFactor::DST_ALPHA;				break;
			case TokenType::ONE_MINUS_DST_ALPHA:	blendFactor = GfxBlendingFactor::ONE_MINUS_DST_ALPHA;	break;
			default:
			{
				LOGERROR("Unexpected token at line: " << token->line << " - Expected a BlendFactor");
				return false;
			}
		}

		return true;
	}

	bool TechniqueCompiler::GetBlendOperationValue(std::deque<Token>::iterator& token, GfxRendererState& rendererState)
	{
		uint32_t renderTarget = 0;
		if (!CheckForRenderTarget(token, rendererState, renderTarget))
			return false;

		if (!GetBlendOperation(token, rendererState.renderTargetStates[renderTarget].blendOperations.color))
			return false;

		AdvanceToken(token);
		if (token->type == TokenType::COMMA)
		{
			AdvanceToken(token);
			if (!GetBlendOperation(token, rendererState.renderTargetStates[renderTarget].blendOperations.alpha))
				return false;
		}

		return false;
	}

	bool TechniqueCompiler::GetBlendOperation(std::deque<Token>::iterator& token, GfxBlendingOperation& blendOperation)
	{
		switch (token->type)
		{
			case TokenType::ADD:				blendOperation = GfxBlendingOperation::ADD;					break;
			case TokenType::SUBSTRACT:			blendOperation = GfxBlendingOperation::SUBSTRACT;			break;
			case TokenType::REVERSE_SUBSTRACT:	blendOperation = GfxBlendingOperation::REVERSE_SUBSTRACT;	break;
			case TokenType::MIN:				blendOperation = GfxBlendingOperation::MIN;					break;
			case TokenType::MAX:				blendOperation = GfxBlendingOperation::MAX;					break;
			default:
			{
				LOGERROR("Unexpected token at line: " << token->line << " - Expected a BlendOperation");
				return false;
			}
		}

		return true;
	}

	bool TechniqueCompiler::GetColorMaskValue(std::deque<Token>::iterator& token, GfxRendererState& rendererState)
	{
		uint32_t renderTarget = 0;
		if (!CheckForRenderTarget(token, rendererState, renderTarget))
			return false;

		if (token->type == TokenType::STRING_VARIABLE)
		{
			std::string rgbaString(token->start, token->length);
			if (IsRGBAStringCorrectlyFormatted(rgbaString))
			{
				for (size_t i = 0; i < rgbaString.size(); ++i)
				{
					if (rgbaString[i] == 'R')
						rendererState.renderTargetStates[renderTarget].colorMask |= 1;
					else if (rgbaString[i] == 'G')
						rendererState.renderTargetStates[renderTarget].colorMask |= 2;
					else if (rgbaString[i] == 'B')
						rendererState.renderTargetStates[renderTarget].colorMask |= 4;
					else if (rgbaString[i] == 'A')
						rendererState.renderTargetStates[renderTarget].colorMask |= 8;
				}
			}
			else
			{
				LOGERROR("Unexpected token at line: " << token->line << " - RGBA string not correctly formatted");
				return false;
			}
		}
		else
		{
			LOGERROR("Unexpected token at line: " << token->line << " - Expected a String value");
			return false;

		}

		return true;
	}

	bool TechniqueCompiler::GetCullMode(std::deque<Token>::iterator& token, GfxRendererState& rendererState)
	{
		if (!CheckForColon(token))
			return false;

		if (IsBoolToken(token->type))
		{
			if (token->type == TokenType::OFF)
				rendererState.faceCulling = GfxFaceCulling::OFF;
			else
			{
				LOGERROR("Unexpected token at line: " << token->line << " - Expected an Off value since face culling can only be deactivated or set to a specific mode");
				return false;
			}
		}
		else
		{
			switch (token->type)
			{
				case TokenType::FRONT:		rendererState.faceCulling = GfxFaceCulling::FRONT; 		break;
				case TokenType::BACK:		rendererState.faceCulling = GfxFaceCulling::BACK;		break;
				case TokenType::FRONT_BACK:	rendererState.faceCulling = GfxFaceCulling::FRONT_BACK;	break;
				default:
					LOGERROR("Unexpected token at line: " << token->line << " - Expected a FaceCulling mode");
					return false;
			}
		}

		return true;
	}

	bool TechniqueCompiler::GetFillMode(std::deque<Token>::iterator& token, GfxRendererState& rendererState)
	{
		if (!CheckForColon(token))
			return false;

		switch (token->type)
		{
			case TokenType::SOLID:			rendererState.fillMode = GfxFillMode::SOLID;		break;
			case TokenType::WIREFRAME:		rendererState.fillMode = GfxFillMode::WIREFRAME;	break;
			default:
				LOGERROR("Unexpected token at line: " << token->line << " - Expected a FillMode");
				return false;
		}

		return true;
	}

	bool TechniqueCompiler::GetWindingOrder(std::deque<Token>::iterator& token, GfxRendererState& rendererState)
	{
		if (!CheckForColon(token))
			return false;

		switch (token->type)
		{
			case TokenType::CLOCKWISE:			rendererState.windingOrder = GfxWindingOrder::CLOCK_WISE;			break;
			case TokenType::COUNTER_CLOCKWISE:	rendererState.windingOrder = GfxWindingOrder::COUNTER_CLOCK_WISE;	break;
			default:
				LOGERROR("Unexpected token at line: " << token->line << " - Expected a WindingOrder mode");
				return false;
		}

		return true;
	}

	bool TechniqueCompiler::GetDefineStageFromToken(const std::deque<Token>::iterator& token, GfxShaderType& outType)
	{
		switch (token->type)
		{
			case TokenType::DEFINE_STAGE_VS:	outType = GfxShaderType::VERTEX;		break;
			case TokenType::DEFINE_STAGE_HS:	outType = GfxShaderType::HULL;			break;
			case TokenType::DEFINE_STAGE_DS:	outType = GfxShaderType::DOMAIN;		break;
			case TokenType::DEFINE_STAGE_GS:	outType = GfxShaderType::GEOMETRY;		break;
			case TokenType::DEFINE_STAGE_PS:	outType = GfxShaderType::PIXEL;			break;
			case TokenType::DEFINE_STAGE_AS:	outType = GfxShaderType::AMPLIFICATION; break;
			case TokenType::DEFINE_STAGE_MS:	outType = GfxShaderType::MESH;			break;
			case TokenType::DEFINE_STAGE_RGS:	outType = GfxShaderType::RAY_GEN;		break;
			case TokenType::DEFINE_STAGE_AHS:	outType = GfxShaderType::ANY_HIT;		break;
			case TokenType::DEFINE_STAGE_CHS:	outType = GfxShaderType::CLOSEST_HIT;	break;
			case TokenType::DEFINE_STAGE_MSS:	outType = GfxShaderType::MISS;			break;
			case TokenType::DEFINE_STAGE_INS:	outType = GfxShaderType::INTERSECTION;	break;
			case TokenType::DEFINE_STAGE_CAS:	outType = GfxShaderType::CALLABLE;		break;

			default:
				LOGERROR("Unexpected token at line: " << token->line << " - Expected a define stage");
				return false;
		}

		return true;
	}

	inline bool TechniqueCompiler::CheckForColon(std::deque<Token>::iterator& token)
	{
		AdvanceToken(token);

		if (token->type != TokenType::COLON)
		{
			LOGERROR("Unexpected token at line: " << token->line << " - Expected a ':'");
			return false;
		}

		AdvanceToken(token);
		return true;
	}

	inline bool TechniqueCompiler::CheckForRenderTarget(std::deque<Token>::iterator& token, GfxRendererState& rendererState, uint32_t& renderTarget)
	{
		if (!CheckForColon(token))
			return false;

		if (token->type == TokenType::NUMBER)
		{
			std::string number(token->start, token->length);
			renderTarget = static_cast<uint32_t>(std::stoi(number));

			if (rendererState.renderTargetStates.size() < renderTarget)
				rendererState.renderTargetStates.resize(renderTarget);

			AdvanceToken(token);
			if (token->type != TokenType::PIPE)
			{
				LOGERROR("Unexpected token at line: " << token->line << " - Expected a '|' character");
				return false;
			}
			AdvanceToken(token);
		}

		return true;
	}

	inline bool TechniqueCompiler::IsRGBAStringCorrectlyFormatted(const std::string& rgbaString)
	{
		if (rgbaString.size() <= 4)
		{
			if (rgbaString.find_first_not_of("RGBA") == std::string::npos)
				return true;

		}

		return false;
	}

	inline void TechniqueCompiler::AdvanceToken(std::deque<Token>::iterator& token, const uint32_t amount)
	{
		token += amount;
	}
}

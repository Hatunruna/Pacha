#include "ShaderCompiler.h"
#include "core/SystemInfo.h"

#include <queue>
#include <unordered_map>

#if RENDERER_VK
#include <spirv_reflect.h>
#endif

namespace Pacha
{
	const std::unordered_map<GfxShaderType, LPCWSTR> kShaderProfiles =
	{
		{ GfxShaderType::VERTEX,		L"vs_6_7" },
		{ GfxShaderType::HULL,			L"hs_6_7" },
		{ GfxShaderType::DOMAIN,		L"ds_6_7" },
		{ GfxShaderType::GEOMETRY,		L"gs_6_7" },
		{ GfxShaderType::PIXEL,			L"ps_6_7" },
		{ GfxShaderType::COMPUTE,		L"cs_6_7" },
		{ GfxShaderType::AMPLIFICATION,	L"as_6_7" },
		{ GfxShaderType::MESH,			L"ms_6_7" },
		{ GfxShaderType::RAY_GEN,		L"lib_6_7" },
		{ GfxShaderType::ANY_HIT,		L"lib_6_7" },
		{ GfxShaderType::CLOSEST_HIT,	L"lib_6_7" },
		{ GfxShaderType::MISS,			L"lib_6_7" },
		{ GfxShaderType::INTERSECTION,	L"lib_6_7" },
		{ GfxShaderType::CALLABLE,		L"lib_6_7" }
	};

	ShaderCompiler::ShaderCompiler()
	{
		HRESULT hr = DxcCreateInstance(CLSID_DxcUtils, IID_PPV_ARGS(&m_DXCUtils));
		if (FAILED(hr))
			LOGCRITICAL("Failed to initialize dxc library utilities.");

		hr = DxcCreateInstance(CLSID_DxcCompiler, IID_PPV_ARGS(&m_DXCCompiler));
		if (FAILED(hr))
			LOGCRITICAL("Failed to initialize dxc compiler.");

		hr = m_DXCUtils->CreateDefaultIncludeHandler(&m_DXCIncludeHandler);
		if (FAILED(hr))
			LOGCRITICAL("Failed to initialize dxc compiler include handler.");
	}

	bool ShaderCompiler::CompileShader(const std::string& shaderSource, ShaderCompileOptions& compileOptions)
	{
		DxcBuffer sourceBuffer = {};
		sourceBuffer.Encoding = 0;
		sourceBuffer.Ptr = shaderSource.data();
		sourceBuffer.Size = shaderSource.size();
		
		std::vector<LPCWSTR> arguments;
		if (!compileOptions.defines.empty())
		{
			arguments.reserve(arguments.size() + (compileOptions.defines.size() * 2));
			for (const auto& define : compileOptions.defines)
			{
				arguments.push_back(L"-D");
				arguments.push_back(define);
			}
		}

		arguments.push_back(L"-E");
		arguments.push_back(compileOptions.entryPoint.c_str());

		arguments.push_back(L"-T");
		arguments.push_back(kShaderProfiles.at(compileOptions.shaderType));

		arguments.push_back(L"-HV 2021");
		arguments.push_back(L"-enable-16bit-types");
		
		arguments.push_back(L"-Qstrip_debug");
		arguments.push_back(L"-Qstrip_priv");
		arguments.push_back(L"-Qstrip_rootsignature");
		
		arguments.push_back(DXC_ARG_WARNINGS_ARE_ERRORS);
		arguments.push_back(DXC_ARG_ALL_RESOURCES_BOUND);

		if (compileOptions.shaderType == GfxShaderType::COMPUTE)
		{
			m_ReflectionSet = 0;
			arguments.push_back(L"-auto-binding-space 0");
		}
		else
			arguments.push_back(L"-auto-binding-space 2");

		if constexpr (SystemInfo::RendererIsVulkan)
		{
			arguments.push_back(L"-spirv");
			arguments.push_back(L"-fspv-target-env=vulkan1.2");

			arguments.push_back(L"-fvk-invert-y");
			arguments.push_back(L"-fvk-use-gl-layout");
			arguments.push_back(L"-fspv-reduce-load-size");
			arguments.push_back(L"-fspv-preserve-bindings");
			arguments.push_back(L"-fvk-auto-shift-bindings");

			arguments.push_back(L"-fspv-extension=KHR");
			arguments.push_back(L"-fspv-extension=SPV_GOOGLE_user_type");
			arguments.push_back(L"-fspv-extension=SPV_GOOGLE_hlsl_functionality1");
		
			if (compileOptions.reflection)
				arguments.push_back(L"-fspv-reflect");

			if (compileOptions.generateDebugInfo)
				arguments.push_back(L"-fspv-debug=vulkan-with-source");
		}

		if constexpr (SystemInfo::RendererIsDX12)
		{
			arguments.push_back(DXC_ARG_DEBUG);
			arguments.push_back(DXC_ARG_DEBUG_NAME_FOR_BINARY);
			arguments.push_back(L"-Qstrip_reflection");
		}

		if (compileOptions.reflection)
			arguments.push_back(DXC_ARG_OPTIMIZATION_LEVEL0);
		else
			arguments.push_back(DXC_ARG_OPTIMIZATION_LEVEL3);

		HRESULT hr = m_DXCCompiler->Compile(&sourceBuffer, arguments.data(), static_cast<UINT32>(arguments.size()), m_DXCIncludeHandler, IID_PPV_ARGS(&m_CompiledShader));

		if (FAILED(hr))
		{
			LOGERROR("Failed to compile shader.");
			return false;
		}

		CComPtr<IDxcBlobUtf8> errors = {};
		hr = m_CompiledShader->GetOutput(DXC_OUT_ERRORS, IID_PPV_ARGS(&errors), nullptr);

		if (FAILED(hr))
		{
			LOGERROR("Failed to get shader compilation errors.");
			return false;
		}

		if (errors && errors->GetStringLength() > 0)
		{
			std::string errorString = std::string(errors->GetStringPointer(), errors->GetStringLength());
			LOGERROR(errorString.c_str());
			return false;
		}

		if constexpr (SystemInfo::RendererIsDX12)
		{
			CComPtr<IDxcBlob> hash;
			if (SUCCEEDED(m_CompiledShader->GetOutput(DXC_OUT_SHADER_HASH, IID_PPV_ARGS(&hash), nullptr)))
			{
				DxcShaderHash* shaderHash = static_cast<DxcShaderHash*>(hash->GetBufferPointer());
				for (size_t i = 0; i < 16; ++i)
					HashCombine(m_ShaderHash, shaderHash->HashDigest[i]);
			}
			else
			{
				LOGERROR("Failed to generate shader hash");
				return false;
			}
		}

		if constexpr (SystemInfo::RendererIsVulkan)
		{
			//Slow alternative for hashing SPIRV since DXC doesn't support shader hashing for SPIRV
			CComPtr<IDxcBlob> byteCode = GetByteCode();
			size_t numUINT64 = byteCode->GetBufferSize() / sizeof(uint64_t);
			size_t uint64Offset = numUINT64 * sizeof(uint64_t);
			size_t remainingBytes = byteCode->GetBufferSize() - (uint64Offset);

			for (size_t i = 0; i < numUINT64; ++i)
			{
				uint64_t byteCodeNum = *reinterpret_cast<uint64_t*>(reinterpret_cast<char*>(byteCode->GetBufferPointer()) + (sizeof(uint64_t) * i));
				HashCombine(m_ShaderHash, byteCodeNum);
			}

			for (size_t i = 0; i < remainingBytes; ++i)
				HashCombine(m_ShaderHash, reinterpret_cast<char*>(byteCode->GetBufferPointer()) + uint64Offset + i);
		}

		return true;
	}

	bool ShaderCompiler::GetReflectionData(GfxShaderReflectionInfo& outReflectionInfo)
	{
		if constexpr (SystemInfo::RendererIsVulkan)
		{
			auto GetTraits = [](SpvReflectTypeDescription* typeDescription) -> uint32_t
			{
				if (typeDescription->type_flags & SPV_REFLECT_TYPE_FLAG_STRUCT)
					return GfxBufferMemberTraits_STRUCT;

				uint32_t traits = 0;
				if (typeDescription->type_flags & SPV_REFLECT_TYPE_FLAG_VECTOR)
				{
					traits |= GfxBufferMemberTraits_VECTOR;

					if (typeDescription->traits.numeric.vector.component_count == 1)
						traits |= GfxBufferMemberTraits_COMPONENTS_1;
					else if (typeDescription->traits.numeric.vector.component_count == 2)
						traits |= GfxBufferMemberTraits_COMPONENTS_2;
					else if (typeDescription->traits.numeric.vector.component_count == 3)
						traits |= GfxBufferMemberTraits_COMPONENTS_3;
					else if (typeDescription->traits.numeric.vector.component_count == 4)
						traits |= GfxBufferMemberTraits_COMPONENTS_4;
				}
				
				if (typeDescription->type_flags & SPV_REFLECT_TYPE_FLAG_BOOL)
					traits |= GfxBufferMemberTraits_BOOL;
				else if (typeDescription->type_flags & SPV_REFLECT_TYPE_FLAG_INT)
				{
					if (typeDescription->traits.numeric.scalar.signedness)
						traits |= GfxBufferMemberTraits_INT;
					else
						traits |= GfxBufferMemberTraits_UINT;
				}
				else if (typeDescription->type_flags & SPV_REFLECT_TYPE_FLAG_FLOAT)
				{
					if (typeDescription->traits.numeric.scalar.width > 32)
						traits |= GfxBufferMemberTraits_DOUBLE;
					else
						traits |= GfxBufferMemberTraits_FLOAT;
				}

				return traits;
			};

			CComPtr<IDxcBlob> byteCode = GetByteCode();
			if (!byteCode)
				return false;

			SpvReflectShaderModule shaderModule = {};
			if (spvReflectCreateShaderModule(byteCode->GetBufferSize(), byteCode->GetBufferPointer(), &shaderModule) != SPV_REFLECT_RESULT_SUCCESS)
			{
				LOGERROR("Failed to initialize shader reflection module.");
				return false;
			}

			if (const SpvReflectDescriptorSet* descriptorSet = spvReflectGetDescriptorSet(&shaderModule, m_ReflectionSet, nullptr))
			{
				for (uint32_t i = 0; i < descriptorSet->binding_count; ++i)
				{
					SpvReflectDescriptorBinding* const& binding = descriptorSet->bindings[i];
					switch (binding->descriptor_type)
					{
						case SPV_REFLECT_DESCRIPTOR_TYPE_SAMPLER:
						{
							GfxSamplerInfo info = {};
							info.binding = binding->binding;
							outReflectionInfo.samplers[PachaId::Create(binding->name)] = info;
							break;
						}
						case SPV_REFLECT_DESCRIPTOR_TYPE_SAMPLED_IMAGE:
						{
							GfxTextureInfo info = {};
							info.binding = binding->binding;
							info.type = GfxTextureDescriptorType::SAMPLED_IMAGE;
							outReflectionInfo.textures[PachaId::Create(binding->name)] = info;
							break;
						}
						case SPV_REFLECT_DESCRIPTOR_TYPE_STORAGE_IMAGE:
						{
							GfxTextureInfo info = {};
							info.binding = binding->binding;
							info.type = GfxTextureDescriptorType::STORAGE_IMAGE;
							outReflectionInfo.textures[PachaId::Create(binding->name)] = info;
							break;
						}
						case SPV_REFLECT_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER:
						{
							GfxBufferInfo info = {};
							info.binding = binding->binding;
							info.type = GfxBufferDescriptorType_FORMATTED;
							outReflectionInfo.buffers[PachaId::Create(binding->name)] = info;
							break;
						}
						case SPV_REFLECT_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER:
						{
							GfxBufferInfo info = {};
							info.binding = binding->binding;
							info.type = GfxBufferDescriptorType_RW_FORMATTED;
							outReflectionInfo.buffers[PachaId::Create(binding->name)] = info;
							break;
						}
						case SPV_REFLECT_DESCRIPTOR_TYPE_STORAGE_BUFFER:
						{
							GfxBufferInfo info = {};
							info.binding = binding->binding;
							info.type = GfxBufferDescriptorType_BYTE_ADDRESS;
							outReflectionInfo.buffers[PachaId::Create(binding->name)] = info;
							break;
						}
						case SPV_REFLECT_DESCRIPTOR_TYPE_UNIFORM_BUFFER:
						{
							GfxConstantBufferInfo info = {};
							info.size = binding->block.size;
							info.binding = binding->binding;
							info.type = GfxBufferDescriptorType_CONSTANT_STATIC;
							
							for (uint32_t j = 0; j < binding->block.member_count; ++j)
							{
								SpvReflectBlockVariable& member = binding->block.members[j];
								GfxBufferMemberInfo& memberInfo = info.members[PachaId::Create(member.name)];

								memberInfo.size = member.size;
								memberInfo.offset = member.offset;
								memberInfo.traits = GetTraits(member.type_description);

								if (memberInfo.traits & GfxBufferMemberTraits_STRUCT)
								{
									std::queue<std::pair<GfxBufferMemberInfo*, SpvReflectBlockVariable*>> processingQueue;
									processingQueue.push(std::make_pair(&memberInfo, &member));

									while (!processingQueue.empty())
									{
										const std::pair<GfxBufferMemberInfo*, SpvReflectBlockVariable*> currentPair = processingQueue.front();
										processingQueue.pop();

										GfxBufferMemberInfo* const& parentMemberInfo = currentPair.first;
										SpvReflectBlockVariable* const& parentStructMember = currentPair.second;

										for (uint32_t k = 0; k < parentStructMember->member_count; ++k)
										{
											SpvReflectBlockVariable& structMember = parentStructMember->members[k];
											std::shared_ptr<GfxBufferMemberInfo>& structMemberInfo = parentMemberInfo->structMembers[PachaId::Create(structMember.name)];

											structMemberInfo = std::make_shared<GfxBufferMemberInfo>();
											structMemberInfo->size = structMember.size;
											structMemberInfo->offset = structMember.offset;
											structMemberInfo->traits = GetTraits(structMember.type_description);

											if (structMemberInfo->traits & GfxBufferMemberTraits_STRUCT)
												processingQueue.push(std::make_pair(structMemberInfo.get(), &structMember));
										}
									}
								}
							}
							
							outReflectionInfo.constantBuffers[PachaId::Create(binding->name)] = info;
							break;
						}
						default:
							break;
					}
				}
			}

			spvReflectDestroyShaderModule(&shaderModule);
		}
		else
		{

		}

		return true;
	}

	CComPtr<IDxcBlob> ShaderCompiler::GetByteCode()
	{
		CComPtr<IDxcBlob> byteCode = nullptr;
		HRESULT hr = m_CompiledShader->GetOutput(DXC_OUT_OBJECT, IID_PPV_ARGS(&byteCode), NULL);

		if (FAILED(hr))
		{
			LOGERROR("Failed to get shader bytecode.");
			return nullptr;
		}

		return byteCode;
	}

	uint64_t ShaderCompiler::GetHash()
	{
		return m_ShaderHash;
	}
}
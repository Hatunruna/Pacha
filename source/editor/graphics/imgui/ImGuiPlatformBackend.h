#pragma once
#include "platform/GfxWindow.h"

#include <imgui.h>

#include <unordered_map>

namespace Pacha
{
	class ImGuiPlatformBackend
	{
	public:
		static bool Initialize();
		static void Destroy();
		static GfxWindow* GetWindow(ImGuiViewport* viewport);

	private:
		static void ImGuiCreateWindow(ImGuiViewport* viewport);
		static void ImGuiDestroyWindow(ImGuiViewport* viewport);
		static void ImGuiShowWindow(ImGuiViewport* viewport);
		static void ImGuiSetWindowPosition(ImGuiViewport* viewport, ImVec2 position);
		static ImVec2 ImGuiGetWindowPosition(ImGuiViewport* viewport);
		static void ImGuiSetWindowSize(ImGuiViewport* viewport, ImVec2 size);
		static ImVec2 ImGuiGetWindowSize(ImGuiViewport* viewport);
		static void ImGuiSetWindowFocus(ImGuiViewport* viewport);
		static bool ImGuiGetWindowFocus(ImGuiViewport* viewport);
		static bool ImGuiGetWindowMinimized(ImGuiViewport* viewport);
		static void ImGuiSetWindowTitle(ImGuiViewport* viewport, const char* title);
		static void ImGuiRenderWindow(ImGuiViewport* viewport, void* userData);
		static void ImGuiSwapBuffers(ImGuiViewport* viewport, void* userData);
		static void ImGuiSetWindowAlpha(ImGuiViewport* viewport, float alpha);

		static std::unordered_map<ImGuiViewport*, GfxWindow*> m_Windows;
	};
}
#pragma once
#include "platform/GfxWindow.h"
#include "graphics/resources/commandbuffer/GfxCommandBuffer.h"

#include <imgui.h>

namespace Pacha
{
	class ImGuiBackendSpecific
	{
	public:
		static bool PlatformInitialize(const GfxWindow& window);
		static void PlatformDestroy();
		static void PlatformCreateViewportData(ImGuiViewport* viewport, GfxWindow* window);
		static void PlatformDestroyViewportData(ImGuiViewport* viewport);

		static bool RendererInitialize();
		static void RendererDestroy();
		static void RendererCreateViewportData(ImGuiViewport* viewport);
		static void RendererDestroyViewportData(void* rendererUserData);
		static void RendererFillCommandBuffer(ImDrawData* drawData, GfxCommandBuffer* commandBuffer);
	};
}
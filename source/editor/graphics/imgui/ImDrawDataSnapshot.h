// Usage:
//  static ImDrawDataSnapshot snapshot; // Important: make persistent accross frames to reuse buffers.
//  snapshot.SnapUsingSwap(ImGui::GetDrawData(), ImGui::GetTime());
//  [...]
//  ImGui_ImplDX11_RenderDrawData(&snapshot.DrawData);

// FIXME: Could store an ID in ImDrawList to make this easier for user.
#include "imgui_internal.h" // ImPool<>, ImHashData

struct ImDrawDataSnapshotEntry
{
    ImDrawList* SrcCopy = NULL;     // Drawlist owned by main context
    ImDrawList* OurCopy = NULL;     // Our copy
};

struct ImDrawDataSnapshot
{
    // Members
    ImDrawData                      DrawData;
    ImPool<ImDrawDataSnapshotEntry> Cache;

    // Functions
    ~ImDrawDataSnapshot() { Clear(); }
    void Clear()
    {
        for (int n = 0; n < Cache.GetMapSize(); n++)
            if (ImDrawDataSnapshotEntry* entry = Cache.TryGetMapData(n))
                IM_DELETE(entry->OurCopy);
        Cache.Clear();
        DrawData.Clear();
    }

    void SnapUsingSwap(ImDrawData* src)
    {
        ImDrawData* dst = &DrawData;
        IM_ASSERT(src != dst && src->Valid);

        // Copy all fields except CmdLists[]
        ImVector<ImDrawList*> backup_draw_list;
        backup_draw_list.swap(src->CmdLists);
        IM_ASSERT(src->CmdLists.Data == NULL);
        *dst = *src;
        backup_draw_list.swap(src->CmdLists);

        // Swap and mark as used
        for (ImDrawList* src_list : src->CmdLists)
        {
            ImDrawDataSnapshotEntry* entry = GetOrAddEntry(src_list);
            if (entry->OurCopy == NULL)
            {
                entry->SrcCopy = src_list;
                entry->OurCopy = IM_NEW(ImDrawList)(src_list->_Data);
            }
            IM_ASSERT(entry->SrcCopy == src_list);
            entry->SrcCopy->CmdBuffer.swap(entry->OurCopy->CmdBuffer); // Cheap swap
            entry->SrcCopy->IdxBuffer.swap(entry->OurCopy->IdxBuffer);
            entry->SrcCopy->VtxBuffer.swap(entry->OurCopy->VtxBuffer);
            entry->SrcCopy->CmdBuffer.reserve(entry->OurCopy->CmdBuffer.Capacity); // Preserve bigger size to avoid reallocs for two consecutive frames
            entry->SrcCopy->IdxBuffer.reserve(entry->OurCopy->IdxBuffer.Capacity);
            entry->SrcCopy->VtxBuffer.reserve(entry->OurCopy->VtxBuffer.Capacity);
            dst->CmdLists.push_back(entry->OurCopy);
        }
    }

    // Internals
    ImGuiID                         GetDrawListID(ImDrawList* src_list) { return ImHashData(&src_list, sizeof(src_list)); }     // Hash pointer
    ImDrawDataSnapshotEntry*        GetOrAddEntry(ImDrawList* src_list) { return Cache.GetOrAddByKey(GetDrawListID(src_list)); }
};
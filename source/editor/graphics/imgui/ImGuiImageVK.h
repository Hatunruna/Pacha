#pragma once
#include "ImGuiImageBase.h"

#include <unordered_map>

namespace Pacha
{
	class ImGuiImageVK : public ImGuiImageBase
	{
		struct DescriptorSet
		{
			uint32_t framesTillDelete;
			VkDescriptorSet descriptor;
		};

	public:
		ImGuiImageVK();
		ImTextureID Get(GfxTexture* texture) override;
		void FreeResources() override;

	private:
		GfxSampler m_Sampler;
		uint32_t m_FramesTillDelete = 3;
		std::unordered_map<VkImageView, DescriptorSet> m_DescriptorSets;
	};
}
#include "ImGuiRendererBackend.h"
#include "ImGuiPlatformBackend.h"
#include "ImGuiBackendSpecific.h"
#include "graphics/ui/UiManager.h"
#include "graphics/device/GfxDevice.h"
#include "graphics/swapchain/GfxSwapchain.h"
#include "graphics/renderpass/GfxRenderPass.h"

namespace Pacha
{
	std::vector<ViewportReleaseData> ImGuiRendererBackend::m_ViewportReleaseData;
	std::unordered_map<ImGuiViewport*, ViewportData> ImGuiRendererBackend::m_VieportData;

	bool ImGuiRendererBackend::Initialize()
	{
		if (!ImGuiBackendSpecific::RendererInitialize())
			return false;

		ImGuiPlatformIO& platform_io = ImGui::GetPlatformIO();
		if (ImGui::GetIO().ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
		{
			platform_io.Renderer_CreateWindow = ImGuiCreateWindow;
			platform_io.Renderer_DestroyWindow = ImGuiDestroyWindow;
			platform_io.Renderer_SetWindowSize = ImGuiSetWindowSize;
			platform_io.Renderer_RenderWindow = ImGuiRenderWindow;
			platform_io.Renderer_SwapBuffers = ImGuiSwapBuffers;
		}

		return true;
	}

	void ImGuiRendererBackend::Destroy()
	{
		for (auto viewportReleaseData = m_ViewportReleaseData.begin(); viewportReleaseData != m_ViewportReleaseData.end();)
			ImGuiBackendSpecific::RendererDestroyViewportData(viewportReleaseData->rendererData);

		ImGuiBackendSpecific::RendererDestroy();
	}

	void ImGuiRendererBackend::ReleasePendingData()
	{
		if (m_ViewportReleaseData.empty())
			return;

		GfxDevice::GetInstance().WaitIdle();
		
		for (auto& viewportReleaseData : m_ViewportReleaseData)
		{
			ImGuiBackendSpecific::RendererDestroyViewportData(viewportReleaseData.rendererData);

			ViewportData& data = viewportReleaseData.viewportData;
			for (size_t i = 0; i < data.commandBuffers.size(); ++i)
				delete data.commandBuffers[i];

			data.swapchain->Destroy();
			delete data.swapchain;
		}

		m_ViewportReleaseData.clear();
	}

	void ImGuiRendererBackend::ImGuiCreateWindow(ImGuiViewport* viewport)
	{
		GfxWindow* window = ImGuiPlatformBackend::GetWindow(viewport);
		if (window)
		{
			ViewportData& data = m_VieportData[viewport];
			data.swapchain = new GfxSwapchain();
			data.swapchain->Initialize(window);

			data.commandBuffers.resize(data.swapchain->GetBackBufferCount());
			for (size_t i = 0; i < data.commandBuffers.size(); ++i)
				data.commandBuffers[i] = new GfxCommandBuffer();

			ImGuiBackendSpecific::RendererCreateViewportData(viewport);
		}
	}

	void ImGuiRendererBackend::ImGuiDestroyWindow(ImGuiViewport* viewport)
	{
		if (m_VieportData.count(viewport))
		{
			m_ViewportReleaseData.push_back({ viewport->RendererUserData, m_VieportData[viewport], m_VieportData[viewport].swapchain->GetBackBufferCount() + 1 });
			viewport->RendererUserData = nullptr;
			m_VieportData.erase(viewport);
		}
	}

	void ImGuiRendererBackend::ImGuiSetWindowSize(ImGuiViewport*, ImVec2)
	{ }

	void ImGuiRendererBackend::ImGuiRenderWindow(ImGuiViewport* viewport, void* userData)
	{
		GfxRenderTarget* backbuffer = nullptr;
		GfxCommandBuffer* commandBuffer = nullptr;

		UiManager& uiManager = UiManager::GetInstance();
		RendererUserData* rendererUserData = static_cast<RendererUserData*>(userData);

		const bool isMainViewport = viewport == ImGui::GetMainViewport();
		if (isMainViewport)
		{
			commandBuffer = rendererUserData->commandBuffer;
			backbuffer = &GfxSwapchain::GetMainSwapchain().GetCurrentBackbuffer();
		}
		else
		{
			if (!m_VieportData.count(viewport))
				return;

			ViewportData& data = m_VieportData[viewport];
			if (!data.swapchain->Prepare())
				return;

			data.readyToPresent = true;
			backbuffer = &data.swapchain->GetCurrentBackbuffer();

			commandBuffer = data.commandBuffers[data.swapchain->GetCurrentFrameIndex()];
			commandBuffer->BeginCommandBuffer();
		}

		for (int i = 0; i < rendererUserData->drawData->CmdListsCount; ++i)
		{
			ImDrawList* drawList = rendererUserData->drawData->CmdLists[i];
			for (ImDrawCmd& cmd : drawList->CmdBuffer)
			{
				const UITexture& uiTexture = uiManager.GetUITexture(rendererUserData->framePacket, cmd.TextureId);
				GfxTexture* texture = uiTexture.id ? rendererUserData->renderPass->GetTexture(uiTexture.id) : uiTexture.texture;

				if (texture)
				{
					cmd.TextureId = uiManager.GetTextureId(texture);
					if (!rendererUserData->transitionedTextures.count(uiTexture.id) && uiTexture.state != GfxResourceState::SHADER_RESOURCE)
					{
						commandBuffer->TransitionTexture(texture, uiTexture.state, GfxResourceState::SHADER_RESOURCE);
						rendererUserData->transitionedTextures.emplace(uiTexture.id);
					}
				}
			}
		}

		commandBuffer->BeginRenderPass(*backbuffer);
		ImGuiBackendSpecific::RendererFillCommandBuffer(rendererUserData->drawData, commandBuffer);
		commandBuffer->EndRenderPass();

		if (!isMainViewport)
			commandBuffer->EndCommandBuffer();
	}

	void ImGuiRendererBackend::ImGuiSwapBuffers(ImGuiViewport* viewport, void*)
	{
		if (m_VieportData.count(viewport))
		{
			ViewportData& data = m_VieportData[viewport];
			if (data.readyToPresent)
			{
				GfxCommandBuffer* commandBuffer = data.commandBuffers[data.swapchain->GetCurrentFrameIndex()];
				data.swapchain->AddCommandBuffer(commandBuffer);
				data.swapchain->Present();

				data.readyToPresent = false;
			}
		}
	}
}
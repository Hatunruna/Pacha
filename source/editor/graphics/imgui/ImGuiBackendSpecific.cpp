#include "ImGuiBackendSpecific.h"
#include "core/Log.h"
#include "graphics/device/GfxDevice.h"
#include "graphics/swapchain/GfxSwapchain.h"

#if PLATFORM_DESKTOP
#include <backends/imgui_impl_sdl2.cpp>
#endif

#if RENDERER_VK
#include <imgui_impl_vulkan.cpp>
#endif

bool Pacha::ImGuiBackendSpecific::PlatformInitialize(const GfxWindow& window)
{
#if PLATFORM_DESKTOP
	if (!ImGui_ImplSDL2_Init(reinterpret_cast<SDL_Window*>(window.GetHandle()), nullptr, nullptr))
	{
		LOGCRITICAL("Failed to initialize ImGui's SDL2 implementation");
		return false;
	}

	return true;
#else
	return false;
#endif
}

void Pacha::ImGuiBackendSpecific::PlatformDestroy()
{
	ImGui_ImplSDL2_Shutdown();
}

void Pacha::ImGuiBackendSpecific::PlatformCreateViewportData(ImGuiViewport* viewport, GfxWindow* window)
{
#if PLATFORM_DESKTOP
	ImGui_ImplSDL2_ViewportData* viewportData = IM_NEW(ImGui_ImplSDL2_ViewportData)();
	viewportData->Window = reinterpret_cast<SDL_Window*>(window->GetHandle());
	viewportData->WindowID = SDL_GetWindowID(viewportData->Window);
	
	viewport->PlatformHandle = viewportData->Window;
	viewport->PlatformUserData = viewportData;
#endif
}

void Pacha::ImGuiBackendSpecific::PlatformDestroyViewportData(ImGuiViewport* viewport)
{
#if PLATFORM_DESKTOP
	ImGui_ImplSDL2_ViewportData* viewportData = static_cast<ImGui_ImplSDL2_ViewportData*>(viewport->PlatformUserData);
	viewportData->Window = nullptr;
	IM_DELETE(viewportData);
	
	viewport->PlatformUserData = nullptr;
#endif
}

bool Pacha::ImGuiBackendSpecific::RendererInitialize()
{
#if RENDERER_VK
	GfxDevice& gfxDevice = GfxDevice::GetInstance();
	GfxSwapchain& gfxSwapchain = GfxSwapchain::GetMainSwapchain();
	GfxRenderTarget& backBuffer = gfxSwapchain.GetCurrentBackbuffer();

	ImGui_ImplVulkan_InitInfo imguiVulkanInitInfo = {};
	imguiVulkanInitInfo.Instance = gfxDevice.GetVkInstance();
	imguiVulkanInitInfo.PhysicalDevice = gfxDevice.GetVkPhysicalDevice();
	imguiVulkanInitInfo.Device = gfxDevice.GetVkDevice();
	imguiVulkanInitInfo.QueueFamily = gfxDevice.GetGraphicsQueueFamilyIndex();
	imguiVulkanInitInfo.Queue = gfxDevice.GetGraphicsQueue();
	imguiVulkanInitInfo.MinImageCount = gfxSwapchain.GetBackBufferCount();
	imguiVulkanInitInfo.ImageCount = gfxSwapchain.GetBackBufferCount();
	imguiVulkanInitInfo.RenderPass = backBuffer.GetRenderPass();

	VkDescriptorPoolSize descriptorPoolSizes[] = { { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1 } };
	VkDescriptorPoolCreateInfo descriptorPoolInfo = {};
	descriptorPoolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	descriptorPoolInfo.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
	descriptorPoolInfo.maxSets = 128;
	descriptorPoolInfo.poolSizeCount = 1;
	descriptorPoolInfo.pPoolSizes = descriptorPoolSizes;

	VkResult result = VK_SUCCESS;
	if ((result = vkCreateDescriptorPool(gfxDevice.GetVkDevice(), &descriptorPoolInfo, nullptr, &imguiVulkanInitInfo.DescriptorPool)) != VK_SUCCESS)
	{
		LOGCRITICAL("Failed to create descriptor pool for ImGUI: " << GfxDeviceVK::GetErrorString(result));
		return false;
	}

	auto LoadVulkanFunction = [](const char* name, void* instance)
	{ return vkGetInstanceProcAddr(*reinterpret_cast<VkInstance*>(instance), name); };

	if (!ImGui_ImplVulkan_LoadFunctions(LoadVulkanFunction, &imguiVulkanInitInfo.Instance))
	{
		LOGCRITICAL("Failed to load Vulkan entry points for ImGui");
		return false;
	}

	if (!ImGui_ImplVulkan_Init(&imguiVulkanInitInfo))
	{
		LOGCRITICAL("Failed to initialize ImGui's Vulkan implementation");
		return false;
	}

	if (!ImGui_ImplVulkan_CreateFontsTexture())
	{
		LOGCRITICAL("Failed to initialize ImGui's Vulkan implementation");
		return false;
	}

	return true;
#else
	return false;
#endif
}

void Pacha::ImGuiBackendSpecific::RendererDestroy()
{
#if RENDERER_VK
	GfxDevice& gfxDevice = GfxDevice::GetInstance();
	ImGui_ImplVulkan_Data* bd = ImGui_ImplVulkan_GetBackendData();

	ImGui_ImplVulkan_DestroyFontsTexture();
	if (bd->VulkanInitInfo.DescriptorPool != VK_NULL_HANDLE)
		vkDestroyDescriptorPool(gfxDevice.GetVkDevice(), bd->VulkanInitInfo.DescriptorPool, nullptr);

	ImGui_ImplVulkan_Shutdown();
#endif
}

void Pacha::ImGuiBackendSpecific::RendererCreateViewportData(ImGuiViewport* viewport)
{
#if RENDERER_VK
	ImGui_ImplVulkan_ViewportData* vulkanViewportData = IM_NEW(ImGui_ImplVulkan_ViewportData)();
	viewport->RendererUserData = vulkanViewportData;
#endif
}

void Pacha::ImGuiBackendSpecific::RendererDestroyViewportData(void* rendererUserData)
{
#if RENDERER_VK
	GfxDevice& gfxDevice = GfxDevice::GetInstance();
	ImGui_ImplVulkan_ViewportData* vulkanViewportData = static_cast<ImGui_ImplVulkan_ViewportData*>(rendererUserData);
	ImGui_ImplVulkan_DestroyWindowRenderBuffers(gfxDevice.GetVkDevice(), &vulkanViewportData->RenderBuffers, nullptr);
	IM_DELETE(vulkanViewportData);
#endif
}

void Pacha::ImGuiBackendSpecific::RendererFillCommandBuffer(ImDrawData* drawData, GfxCommandBuffer* commandBuffer)
{
#if RENDERER_VK
	ImGui_ImplVulkan_RenderDrawData(drawData, commandBuffer->GetBuffer());
#endif
}

#pragma once
#if RENDERER_VK
#include "ImGuiImageVK.h"
#endif

namespace Pacha
{
#if RENDERER_VK
	using ImGuiImage = ImGuiImageVK;
#endif
}
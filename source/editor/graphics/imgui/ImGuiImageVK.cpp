#include "ImGuiImageVK.h"
#include "graphics/swapchain/GfxSwapchain.h"

#include <backends/imgui_impl_vulkan.h>

namespace Pacha
{
	ImGuiImageVK::ImGuiImageVK()
	{
		GfxSamplerDescriptor samplerDescriptor = {};
		samplerDescriptor.filterMode = GfxFilterMode::LINEAR;
		samplerDescriptor.SetWrapMode(GfxWrapMode::CLAMP_TO_EDGE);
		m_Sampler.Build(samplerDescriptor);

		m_FramesTillDelete = GfxSwapchain::GetMainSwapchain().GetBackBufferCount() + 1;
	}

	ImTextureID ImGuiImageVK::Get(GfxTexture* texture)
	{
		const VkImageView& imageView = texture->GetImageView(0);
		auto found = m_DescriptorSets.find(imageView);
		if (found == m_DescriptorSets.end())
		{
			DescriptorSet& descriptor = m_DescriptorSets[imageView];
			descriptor.descriptor = ImGui_ImplVulkan_AddTexture(m_Sampler.GetSampler(), imageView, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
			descriptor.framesTillDelete = m_FramesTillDelete;
			return reinterpret_cast<ImTextureID>(descriptor.descriptor);
		}
		else
		{
			found->second.framesTillDelete = m_FramesTillDelete;
			return reinterpret_cast<ImTextureID>(found->second.descriptor);
		}
	}

	void ImGuiImageVK::FreeResources()
	{
		std::vector<VkImageView> viewsToDelte;
		for (auto& [view, descriptor] : m_DescriptorSets)
		{
			if(descriptor.framesTillDelete <= 0)
			{
				ImGui_ImplVulkan_RemoveTexture(descriptor.descriptor);
				viewsToDelte.push_back(view);
			}
			else
				--descriptor.framesTillDelete;
		}

		for(auto& view : viewsToDelte)
			m_DescriptorSets.erase(view);
	}
}
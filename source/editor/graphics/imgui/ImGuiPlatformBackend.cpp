#include "ImGuiPlatformBackend.h"
#include "ImGuiBackendSpecific.h"
#include "platform/GfxWindow.h"

namespace Pacha
{
	std::unordered_map<ImGuiViewport*, GfxWindow*> ImGuiPlatformBackend::m_Windows;

	bool ImGuiPlatformBackend::Initialize()
	{
		GfxWindow& window = GfxWindow::GetMainWindow();
		if (!ImGuiBackendSpecific::PlatformInitialize(window))
			return false;

		m_Windows[ImGui::GetMainViewport()] = &window;

		ImGuiIO& io = ImGui::GetIO();
		if ((io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) && (io.BackendFlags & ImGuiBackendFlags_PlatformHasViewports))
		{
			ImGuiPlatformIO& platform_io = ImGui::GetPlatformIO();
			platform_io.Platform_CreateWindow = ImGuiCreateWindow;
			platform_io.Platform_DestroyWindow = ImGuiDestroyWindow;
			platform_io.Platform_ShowWindow = ImGuiShowWindow;
			platform_io.Platform_SetWindowPos = ImGuiSetWindowPosition;
			platform_io.Platform_GetWindowPos = ImGuiGetWindowPosition;
			platform_io.Platform_SetWindowSize = ImGuiSetWindowSize;
			platform_io.Platform_GetWindowSize = ImGuiGetWindowSize;
			platform_io.Platform_SetWindowFocus = ImGuiSetWindowFocus;
			platform_io.Platform_GetWindowFocus = ImGuiGetWindowFocus;
			platform_io.Platform_GetWindowMinimized = ImGuiGetWindowMinimized;
			platform_io.Platform_SetWindowTitle = ImGuiSetWindowTitle;
			platform_io.Platform_RenderWindow = ImGuiRenderWindow;
			platform_io.Platform_SwapBuffers = ImGuiSwapBuffers;
			platform_io.Platform_SetWindowAlpha = ImGuiSetWindowAlpha;
		}

		return true;
	}

	void ImGuiPlatformBackend::Destroy()
	{
		ImGuiBackendSpecific::PlatformDestroy();
	}

	GfxWindow* ImGuiPlatformBackend::GetWindow(ImGuiViewport* viewport)
	{
		if (m_Windows.count(viewport))
			return m_Windows[viewport];
		return nullptr;
	}

	void ImGuiPlatformBackend::ImGuiCreateWindow(ImGuiViewport* viewport)
	{
		WindowInitFlags initFlags = 0;
		initFlags |= WINDOW_HIDDEN;

		const bool noDecoration = viewport->Flags & ImGuiViewportFlags_NoDecoration;
		initFlags |= noDecoration ? WINDOW_BORDERLESS : WINDOW_RESIZABLE;

		if (viewport->Flags & ImGuiViewportFlags_TopMost)
			initFlags |= WINDOW_ALWAYS_ON_TOP;

		if (viewport->Flags & ImGuiViewportFlags_NoTaskBarIcon)
			initFlags |= WINDOW_NO_TASK_BAR_ICON;

		GfxWindow* window = new GfxWindow();
		window->Initialize(static_cast<uint32_t>(viewport->Size.x), static_cast<uint32_t>(viewport->Size.y), "ImGui", initFlags);
		window->SetPosition(glm::uvec2(viewport->Pos.x, viewport->Pos.y));
		ImGuiBackendSpecific::PlatformCreateViewportData(viewport, window);

		m_Windows[viewport] = window;
	}

	void ImGuiPlatformBackend::ImGuiDestroyWindow(ImGuiViewport* viewport)
	{
		if (m_Windows.count(viewport))
		{
			ImGuiBackendSpecific::PlatformDestroyViewportData(viewport);
			
			if (viewport != ImGui::GetMainViewport())
				delete m_Windows[viewport];

			m_Windows.erase(viewport);
		}
	}

	void ImGuiPlatformBackend::ImGuiShowWindow(ImGuiViewport* viewport)
	{
		m_Windows[viewport]->SetHidden(false);
	}

	void ImGuiPlatformBackend::ImGuiSetWindowPosition(ImGuiViewport* viewport, ImVec2 position)
	{
		m_Windows[viewport]->SetPosition(glm::uvec2(position.x, position.y));
	}

	ImVec2 ImGuiPlatformBackend::ImGuiGetWindowPosition(ImGuiViewport* viewport)
	{
		const glm::uvec2& position = m_Windows[viewport]->GetPosition();
		return ImVec2(static_cast<float>(position.x), static_cast<float>(position.y));
	}

	void ImGuiPlatformBackend::ImGuiSetWindowSize(ImGuiViewport* viewport, ImVec2 size)
	{
		m_Windows[viewport]->SetSize(glm::uvec2(size.x, size.y));
	}

	ImVec2 ImGuiPlatformBackend::ImGuiGetWindowSize(ImGuiViewport* viewport)
	{
		const glm::uvec2& size = m_Windows[viewport]->GetSize();
		return ImVec2(static_cast<float>(size.x), static_cast<float>(size.y));
	}

	void ImGuiPlatformBackend::ImGuiSetWindowFocus(ImGuiViewport* viewport)
	{
		m_Windows[viewport]->Focus();
	}

	bool ImGuiPlatformBackend::ImGuiGetWindowFocus(ImGuiViewport* viewport)
	{
		return m_Windows[viewport]->HasInputFocus();
	}

	bool ImGuiPlatformBackend::ImGuiGetWindowMinimized(ImGuiViewport* viewport)
	{
		return m_Windows[viewport]->IsMinimized();
	}

	void ImGuiPlatformBackend::ImGuiSetWindowTitle(ImGuiViewport* viewport, const char* title)
	{
		m_Windows[viewport]->SetTitle(title);
	}

	void ImGuiPlatformBackend::ImGuiRenderWindow(ImGuiViewport*, void*)
	{ }

	void ImGuiPlatformBackend::ImGuiSwapBuffers(ImGuiViewport*, void*)
	{ }

	void ImGuiPlatformBackend::ImGuiSetWindowAlpha(ImGuiViewport* viewport, float alpha)
	{
		m_Windows[viewport]->SetAlpha(alpha);
	}
}

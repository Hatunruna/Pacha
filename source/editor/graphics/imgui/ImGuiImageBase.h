#pragma once
#include "graphics/resources/texture/GfxTexture.h"
#include "graphics/resources/sampler/GfxSampler.h"

#include <imgui.h>

namespace Pacha
{
	class ImGuiImageBase
	{
	public:
		ImGuiImageBase() = default;
		virtual ~ImGuiImageBase() = default;

		virtual ImTextureID Get(GfxTexture* texture) = 0;
		virtual void FreeResources() = 0;
	};
}
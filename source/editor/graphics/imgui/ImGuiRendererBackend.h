#pragma once
#include "utilities/PachaId.h"
#include "graphics/swapchain/GfxSwapchain.h"
#include "graphics/resources/commandbuffer/GfxCommandBuffer.h"

#include <imgui.h>

#include <unordered_set>

namespace Pacha
{
	struct RendererUserData
	{
		struct ImDrawData* drawData = nullptr;
		const struct FramePacket* framePacket = nullptr;
		class GfxRenderPass* renderPass = nullptr;
		GfxCommandBuffer* commandBuffer = nullptr;
		std::unordered_set<PachaId> transitionedTextures = {};
	};

	struct ViewportData
	{
		GfxSwapchain* swapchain;
		bool readyToPresent = false;
		std::vector<GfxCommandBuffer*> commandBuffers;
	};

	struct ViewportReleaseData
	{
		void* rendererData;
		ViewportData viewportData;
		uint32_t framesTillDelete;
	};

	class ImGuiRendererBackend
	{
	public:
		static bool Initialize();
		static void Destroy();
		static void ReleasePendingData();

	private:
		static void ImGuiCreateWindow(ImGuiViewport* viewport);
		static void ImGuiDestroyWindow(ImGuiViewport* viewport);
		static void ImGuiSetWindowSize(ImGuiViewport* viewport, ImVec2 size);
		static void ImGuiRenderWindow(ImGuiViewport* viewport, void* userData);
		static void ImGuiSwapBuffers(ImGuiViewport* viewport, void* userData);

		static std::vector<ViewportReleaseData> m_ViewportReleaseData;
		static std::unordered_map<ImGuiViewport*, ViewportData> m_VieportData;
	};
}
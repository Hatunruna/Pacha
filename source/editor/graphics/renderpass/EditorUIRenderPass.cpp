#include "EditorUIRenderPass.h"
#include "graphics/ui/UiManager.h"
#include "graphics/imgui/ImGuiRendererBackend.h"

namespace Pacha
{
	void EditorUiRenderPass::RequestResources(RenderContext&)
	{
	}

	void EditorUiRenderPass::Render(RenderContext& context, GfxCommandBuffer& commandBuffer)
	{
		RendererUserData userData = {};
		userData.commandBuffer = &commandBuffer;
		userData.framePacket = &context.GetFramePacket();
		userData.renderPass = this;
		
		const FramePacket& framePacket = context.GetFramePacket();
		ImGuiPlatformIO& platform_io = ImGui::GetPlatformIO();
		for (int i = 0; i < platform_io.Viewports.Size; i++)
		{
			ImGuiViewport* viewport = platform_io.Viewports[i];
			if (viewport->Flags & ImGuiViewportFlags_IsMinimized)
				continue;

			PACHA_ASSERT(framePacket.drawDataSnapshots.count(viewport), "ImGui viewport not present in snapshots");

			userData.drawData = const_cast<ImDrawData*>(&framePacket.drawDataSnapshots.at(viewport).DrawData);
			platform_io.Renderer_RenderWindow(viewport, &userData);
		}

		UiManager& uiManager = UiManager::GetInstance();
		uiManager.FreeResources();
	}
}

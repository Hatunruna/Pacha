#pragma once
#include "graphics/renderpass/GfxRenderPass.h"

namespace Pacha
{
	class GizmosRenderPass : public GfxRenderPass
	{
	public:
		using GfxRenderPass::GfxRenderPass;
		void RequestResources(RenderContext& context) override;
		void Render(RenderContext& context, GfxCommandBuffer& commandBuffer) override;
	};
}
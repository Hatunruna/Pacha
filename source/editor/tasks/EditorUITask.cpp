#include "EditorUITask.h"
#include "level/LevelManager.h"
#include "assets/AssetManager.h"
#include "scripts/EditorCamera.h"
#include "core/ProjectConstants.h"
#include "graphics/ui/UiManager.h"
#include "graphics/ui/widgets/EditorMainView.h"
#include "graphics/imgui/ImGuiRendererBackend.h"
#include "graphics/imgui/ImGuiPlatformBackend.h"

#include <imgui.cpp>
#include <imgui_draw.cpp>
#include <imgui_tables.cpp>
#include <imgui_widgets.cpp>
#include <implot.cpp>
#include <implot_items.cpp>
#include <IconsFontAwesome6.h>

#include <filesystem>

namespace Pacha
{
	EditorUITask::~EditorUITask()
	{
		ImGuiRendererBackend::Destroy();
		ImGuiPlatformBackend::Destroy();
		ImPlot::DestroyContext();
		ImGui::DestroyContext();
	}

	bool EditorUITask::Setup()
	{
		if (SetupUI())
		{
			if (!ProjectConstants::GetInstance().HasWorkingDirectory())
			{
				//Show project wizard
			}

			if (!AssetManager::GetInstance().Initialize())
			{

				return false;
			}

			Entity* editorCamera = CreateEntity("Editor Camera", LevelManager::GetInstance().GetPersistentLevel());
			editorCamera->SetEditorFlags(EditorFlags_HIDDEN_IN_EDITOR | EditorFlags_UPDATE_IN_EDITOR_MODE);

			m_EditorCamera = editorCamera->AddComponent<EditorCamera>();
			m_EditorMainView = UiManager::GetInstance().RegisterWidget<EditorMainView>("EditorMainView"_ID);

			WaitsOn("ComponentsUpdate"_ID);
			Signals("EditorUI"_ID);
			return true;
		}

		return false;
	}

	bool EditorUITask::SetupUI()
	{
		ImGui::CreateContext();
		ImPlot::CreateContext();
		ImGui::StyleColorsDark();

		ImGuiIO& io = ImGui::GetIO();
		io.IniFilename = nullptr;
		io.ConfigWindowsMoveFromTitleBarOnly = true;
		io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
		io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
		io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;

		ImFontConfig iconsConfig;
		iconsConfig.MergeMode = true;
		iconsConfig.PixelSnapH = true;
		ImWchar iconsRanges[] = { ICON_MIN_FA, ICON_MAX_FA, 0 };
		std::string iconsPath = "./resources/fonts/FA6_Solid900.otf";

		io.Fonts->AddFontDefault();
		io.Fonts->AddFontFromFileTTF(iconsPath.c_str(), 13.0f, &iconsConfig, iconsRanges);

		ImGuiStyle& style = ImGui::GetStyle();
		style.WindowRounding = 0.0f;
		style.Colors[ImGuiCol_WindowBg].w = 1.0f;

		if (!ImGuiPlatformBackend::Initialize())
			return false;

		if (!ImGuiRendererBackend::Initialize())
			return false;

		return true;
	}

	void EditorUITask::Execute(SimulationContext& context)
	{
		ImGui::NewFrame();
		
		UiManager& uiManager = UiManager::GetInstance();
		uiManager.Render(&context);

		ImGui::Render();
		ImGui::EndFrame();

		FramePacket& framePacket = context.GetFramePacket();
		ImGuiPlatformIO& platformIO = ImGui::GetPlatformIO();
		for (auto& viewport : platformIO.Viewports)
			framePacket.drawDataSnapshots[viewport].SnapUsingSwap(viewport->DrawData);

		const glm::uvec2 resolution = m_EditorMainView->GetLevelViewResolution();
		context.GetFramePacket().viewportResolution = resolution;
	}
}

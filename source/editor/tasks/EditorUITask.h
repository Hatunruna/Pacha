#pragma once
#include "core/graph/SimulationTask.h"

namespace Pacha
{
	class EditorUITask : public SimulationTask
	{
	public:
		TASK_CONSTRUCTOR;
		~EditorUITask();

		bool Setup();
		void Execute(SimulationContext& context);
	
	private:
		bool SetupUI();

		class EditorCamera* m_EditorCamera = nullptr;
		class EditorMainView* m_EditorMainView = nullptr;
	};
}
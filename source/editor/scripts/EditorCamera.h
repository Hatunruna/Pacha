#pragma once
#include "ecs/ScriptComponent.h"

REGISTER_SCRIPT(EditorCamera);

namespace Pacha
{
	class EditorCamera : public ScriptComponent
	{
		COMPONENT_TYPE_INFO;

	public:
		void OnCreate() override;
		void OnDestroy() override;
		void OnUpdate(const SimulationTime& gameTime) override;

	private:
		class Camera* m_Camera = nullptr;
		class Transform* m_Transform = nullptr;
	};
}
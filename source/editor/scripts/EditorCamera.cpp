#include "EditorCamera.h"

#include "core/Log.h"
#include "input/Input.h"
#include "core/ProjectConstants.h"
#include "ecs/components/Camera.h"

namespace Pacha
{
	void EditorCamera::OnCreate()
	{
		PACHA_ASSERT(!ProjectConstants::GetInstance().UsingHeadlessRendering(), "Editor Camera cannot be used with headless rendering");
		m_Transform = m_Entity->AddComponent<Transform>();
		m_Camera = m_Entity->AddComponent<Camera>();
		m_Camera->SetType(CameraType::EDITOR);
	}

	void EditorCamera::OnDestroy()
	{

	}

	void EditorCamera::OnUpdate(const SimulationTime&)
	{
		
	}
}
#include "Editor.h"
#include "assets/AssetManager.h"
#include "core/time/RenderTime.h"
#include "platform/PlatformEvents.h"
#include "core/time/SimulationTime.h"
#include "graphics/renderdoc/RenderDoc.h"
#include "graphics/swapchain/GfxSwapchain.h"
#include "graphics/imgui/ImGuiRendererBackend.h"

namespace Pacha
{
	void Editor::Run()
	{
		LOGINFO("--- Running ---");

		//Simulation Tasks
		marl::schedule([&]
		{
			RenderDoc& renderDoc = RenderDoc::GetInstance();
			RenderTime& renderTime = RenderTime::GetInstance();
			GfxSwapchain& gfxSwapchain = GfxSwapchain::GetMainSwapchain();
			m_RenderGraph.Setup(gfxSwapchain);

			while (!m_CloseThreads.isSignalled())
			{
				m_SimulationGraph.WaitForPacketReady();

				const FramePacket& framePacket = m_SimulationGraph.LockFramePacket();
				renderTime.Update();

				if (gfxSwapchain.Prepare())
				{
					renderDoc.BeginFrame();
					{
						m_RenderGraph.Execute(gfxSwapchain, framePacket);
						gfxSwapchain.Present();
						PresentImGuiViewports();
					}
					renderDoc.EndFrame();
				}

				m_SimulationGraph.UnlockFramePacket();
			}

			m_RenderGraph.Shutdown();
		});

		PlatformEvents& eventsHandler = PlatformEvents::GetInstance();
		SimulationTime& simulationTime = SimulationTime::GetInstance();
		while (!eventsHandler.ShouldClose())
		{
			eventsHandler.PollEvents();
			if (m_CappedFPS != -1.f)
			{
				const float elapsed = m_SimulationTimer.GetElapsedTime();
				if (elapsed < m_CappedFPS)
					PreciseSleep(m_CappedFPS - elapsed);
			}

			simulationTime.Update();
			m_SimulationTimer.StartCapture();
			m_SimulationGraph.Execute();
			m_SimulationTimer.EndCapture();

			ImGui::UpdatePlatformWindows();

			AssetManager& assetManager = AssetManager::GetInstance();
			if (assetManager.IsDirty())
				assetManager.SaveDatabase();
		}

		m_CloseThreads.signal();
		m_SimulationGraph.Shutdown();

		m_RenderGraph.WaitTillFinished();
		m_SimulationGraph.WaitTillFinished();
	}

	void Editor::PresentImGuiViewports()
	{
		ImGuiRendererBackend::ReleasePendingData();

		ImGuiPlatformIO& platform_io = ImGui::GetPlatformIO();
		for (int i = 1; i < platform_io.Viewports.Size; i++)
		{
			ImGuiViewport* viewport = platform_io.Viewports[i];
			if (viewport->Flags & ImGuiViewportFlags_IsMinimized)
				continue;
			platform_io.Renderer_SwapBuffers(viewport, nullptr);
		}
	}
}

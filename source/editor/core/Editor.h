#include "core/Engine.h"

namespace Pacha
{
	class Editor : public Engine
	{
	public:
		void Run() override;

	private:
		void PresentImGuiViewports();
	};
}
#pragma once
#include "ecs/Entity.h"

#include <vector>

namespace Pacha
{
	class EditorSelectionManager
	{
	private:
		EditorSelectionManager() = default;
		EditorSelectionManager(const EditorSelectionManager&) = delete;
		EditorSelectionManager& operator=(const EditorSelectionManager&) = delete;

	public:
		static EditorSelectionManager& GetInstance()
		{
			static EditorSelectionManager sInstance;
			return sInstance;
		}

		void AddToSelection(Entity* entity);
		void RemoveFromSelection(Entity* entity);
		bool IsInSelection(Entity* entity);
		void ClearSelection();
		size_t GetSelectionCount();

		bool HasLevelViewSelection();
		void AddLevelViewSelection(Entity* entity);
		void ClearLevelViewSelection();

		std::vector<Entity*>& GetSelectedEntities();
		std::vector<Entity*>& GetLevelViewSelectedEntities();

	private:
		std::vector<Entity*> m_SelectedEntities;
		std::vector<Entity*> m_LevelViewSelection;
	};
}
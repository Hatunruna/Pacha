#include "DirectoryNode.h"
#include "core/ProjectConstants.h"
#include "graphics/ui/widgets/importers/TextureImporter.h"

#if PLATFORM_LINUX
#include <sys/inotify.h>
#endif

#define TYPE_STRING_SWITCH_CASE(x) case DirectoryNodeType::x: return #x

namespace Pacha
{
	template<DirectorySortDirection SortDirection>
	static bool SortDirectoryByName(const DirectoryNode& lhs, const DirectoryNode& rhs)
	{
		constexpr bool ascending = SortDirection == DirectorySortDirection::ASCENDING;

		if (lhs.GetType() != rhs.GetType())
		{
			if (lhs.GetType() == DirectoryNodeType::DIRECTORY)
				return ascending;

			if (rhs.GetType() == DirectoryNodeType::DIRECTORY)
				return !ascending;
		}

		std::string lowerLHS = lhs.GetRelativePath().stem().string();
		std::transform(lowerLHS.begin(), lowerLHS.end(), lowerLHS.begin(), ::tolower);

		std::string lowerRHS = rhs.GetRelativePath().stem().string();
		std::transform(lowerRHS.begin(), lowerRHS.end(), lowerRHS.begin(), ::tolower);

		if constexpr (ascending)
			return lowerLHS < lowerRHS;
		else
			return lowerRHS < lowerLHS;
	}

	template<DirectorySortDirection SortDirection>
	static bool SortDirectoryByType(const DirectoryNode& lhs, const DirectoryNode& rhs)
	{
		constexpr bool ascending = SortDirection == DirectorySortDirection::ASCENDING;

		if (lhs.GetType() == DirectoryNodeType::DIRECTORY)
			return true;

		if (rhs.GetType() == DirectoryNodeType::DIRECTORY)
			return false;

		if (lhs.GetType() != rhs.GetType())
		{
			if constexpr (ascending)
				return lhs.GetType() < rhs.GetType();
			else
				return rhs.GetType() < lhs.GetType();
		}
		else
		{
			std::string lowerLHS = lhs.GetRelativePath().stem().string();
			std::transform(lowerLHS.begin(), lowerLHS.end(), lowerLHS.begin(), ::tolower);

			std::string lowerRHS = rhs.GetRelativePath().stem().string();
			std::transform(lowerRHS.begin(), lowerRHS.end(), lowerRHS.begin(), ::tolower);

			if constexpr (ascending)
				return lowerLHS < lowerRHS;
			else
				return lowerRHS < lowerLHS;
		}
	}

	void DirectoryNode::SetRelativePath(const std::filesystem::path& relativePath)
	{
		m_RelativePath = relativePath;
	}

	const std::filesystem::path DirectoryNode::GetFullPath()
	{
		return ProjectConstants::GetInstance().GetAssetsPath() / m_RelativePath;
	}

	const std::filesystem::path DirectoryNode::GetFilename()
	{
		return m_RelativePath.filename();
	}

#if PLATFORM_LINUX
	void DirectoryNode::CreateWatchDescriptor(int32_t fileDescriptor)
	{
		m_WatchDescriptor = inotify_add_watch(fileDescriptor, GetFullPath().c_str(), IN_CREATE | IN_DELETE | IN_MODIFY | IN_MOVE);
	}
#endif

	DirectoryNode* DirectoryNode::FindChild(const std::filesystem::path& filename)
	{
		for (auto& it : m_Children)
		{
			if (it.m_RelativePath.filename() == filename)
				return &it;
		}

		return nullptr;
	}

	DirectoryNode* DirectoryNode::FindChildRecursive(const std::filesystem::path& relativePath)
	{
		DirectoryNode* found = nullptr;
		DirectoryNode* currentParent = this;

		std::filesystem::path::iterator iterator = relativePath.begin();
		std::filesystem::path::iterator last = --relativePath.end();
		while (iterator != relativePath.end())
		{
			for (auto& child : currentParent->m_Children)
			{
				const std::filesystem::path& childPath = child.GetRelativePath();
				if (childPath.filename() == iterator->filename())
				{
					if (iterator->has_extension() || iterator == last)
					{
						found = &child;
						break;
					}
					else
					{
						currentParent = &child;
						break;
					}
				}
			}
			
			if (found)
				break;
			
			++iterator;
		}

		return found;
	}

	DirectoryNode* DirectoryNode::CreateChild(const std::filesystem::path& filename LINUX_PARAMETER(int fileDescriptor))
	{
		if (FindChild(filename))
			return nullptr;

		DirectoryNode childNode;
		childNode.m_Parent = this;
		childNode.m_RelativePath = GetRelativePath() / filename;
		childNode.m_Type = GetNodeType(childNode.GetFullPath());

#if PLATFORM_LINUX
		if (childNode.m_Type == DirectoryNodeType::DIRECTORY)
			childNode.CreateWatchDescriptor(fileDescriptor);
#endif

		m_Children.push_back(childNode);
		return &m_Children.back();
	}

	DirectoryNode* DirectoryNode::CreateChildCopy(DirectoryNode* nodeToCopy)
	{
		if (FindChild(nodeToCopy->GetFilename()))
			return nullptr;

		DirectoryNode childNode = *nodeToCopy;
		childNode.SetParent(this);
		m_Children.push_back(childNode);

		DirectoryNode* listChildPtr = &m_Children.back();
		listChildPtr->FixupRelativePath();

		return listChildPtr;
	}

	void DirectoryNode::DeleteChild(const std::filesystem::path& fileName)
	{
		for (auto iterator = m_Children.begin(); iterator != m_Children.end(); ++iterator)
		{
			if (iterator->m_RelativePath.filename() == fileName)
			{
				m_Children.erase(iterator);
				break;
			}
		}
	}

	DirectoryNode* DirectoryNode::MoveToNewParent(DirectoryNode* newParent)
	{
		if (DirectoryNode* newChild = newParent->CreateChildCopy(this))
		{
			m_Parent->DeleteChild(GetFilename());
			return newChild;
		}

		return nullptr;
	}

	std::unordered_set<DirectoryNode*> DirectoryNode::GatherParents()
	{
		std::unordered_set<DirectoryNode*> parents;

		DirectoryNode* currentParent = m_Parent;
		while (currentParent != nullptr)
		{
			parents.insert(currentParent);
			currentParent = currentParent->m_Parent;
		}

		return parents;
	}

	void DirectoryNode::FindInChildrenRecursive(const std::string& searchString, std::vector<DirectoryNode*>& searchResult) const
	{
		for (auto& it : m_Children)
		{
			std::string loweredName = it.m_RelativePath.stem().string();
			std::transform(loweredName.begin(), loweredName.end(), loweredName.begin(), ::tolower);
			if (loweredName.find(searchString) != std::string::npos)
				searchResult.push_back(const_cast<DirectoryNode*>(&it));
			it.FindInChildrenRecursive(searchString, searchResult);
		}
	}

	bool DirectoryNode::HasChildNodes() const
	{
		return !m_Children.empty();
	}

	bool DirectoryNode::HasDirectoryChildNodes() const
	{
		for (auto& it : m_Children)
			if (it.m_Type == DirectoryNodeType::DIRECTORY)
				return true;
		return false;
	}

	void DirectoryNode::FixupRelativePath()
	{
		m_RelativePath = m_Parent->GetRelativePath() / GetFilename();

		if (HasChildNodes())
			FixupRelativePathRecursive(this);
	}

	void DirectoryNode::FixupRelativePathRecursive(DirectoryNode* node)
	{
		for (auto& child : node->m_Children)
		{
			child.SetParent(node);
			child.FixupRelativePath();
		}
	}

	const char* DirectoryNode::GetTypeAsString() const
	{
		switch (m_Type)
		{
			TYPE_STRING_SWITCH_CASE(DIRECTORY);
			TYPE_STRING_SWITCH_CASE(GENERIC);
			TYPE_STRING_SWITCH_CASE(LEVEL);
			TYPE_STRING_SWITCH_CASE(MATERIAL);
			TYPE_STRING_SWITCH_CASE(MODEL);
			TYPE_STRING_SWITCH_CASE(SHADER_INCLUDE);
			TYPE_STRING_SWITCH_CASE(TECHNIQUE);
			TYPE_STRING_SWITCH_CASE(TEXTURE);
		}

		LOGERROR("Unspecified Node Type");
		return "ERROR";
	}

	void DirectoryNode::SortChildren(const DirectorySortCriteria& criteria, const DirectorySortDirection& direction)
	{
		if (criteria == DirectorySortCriteria::SORT_BY_NAME)
		{
			if (direction == DirectorySortDirection::ASCENDING)
				m_Children.sort(SortDirectoryByName<DirectorySortDirection::ASCENDING>);
			else if (direction == DirectorySortDirection::DESCENDING)
				m_Children.sort(SortDirectoryByName<DirectorySortDirection::DESCENDING>);
		}
		else if (criteria == DirectorySortCriteria::SORT_BY_TYPE)
		{
			if (direction == DirectorySortDirection::ASCENDING)
				m_Children.sort(SortDirectoryByType<DirectorySortDirection::ASCENDING>);
			else if (direction == DirectorySortDirection::DESCENDING)
				m_Children.sort(SortDirectoryByType<DirectorySortDirection::DESCENDING>);
		}
	}

	DirectoryNodeType DirectoryNode::GetNodeType(const std::filesystem::path& path)
	{
		if (std::filesystem::is_directory(path))
			return DirectoryNodeType::DIRECTORY;

		if (TextureImporter::IsSupportedImageFormat(path))
			return DirectoryNodeType::TEXTURE;

		std::string lowerCaseExtension = path.extension().string();
		std::transform(lowerCaseExtension.begin(), lowerCaseExtension.end(), lowerCaseExtension.begin(), ::tolower);

		switch (FNV1aHash(lowerCaseExtension))
		{
			case FNV1aHash(".gltf"):
			case FNV1aHash(".glb"):
				return DirectoryNodeType::MODEL;
			case FNV1aHash(".level"):
				return DirectoryNodeType::LEVEL;
			case FNV1aHash(".mat"):
				return DirectoryNodeType::MATERIAL;
			case FNV1aHash(".tech"):
			case FNV1aHash(".ctech"):
			case FNV1aHash(".rtech"):
				return DirectoryNodeType::TECHNIQUE;
			case FNV1aHash(".hlsli"):
				return DirectoryNodeType::SHADER_INCLUDE;
		}

		return DirectoryNodeType::GENERIC;
	}

	bool operator == (const DirectoryNode& lhs, const DirectoryNode& rhs)
	{
		return lhs.m_RelativePath == rhs.m_RelativePath;
	}
}

#pragma once
#include "DirectoryNode.h"
#include "DirectoryListener.h"

#include <thread>
#include <mutex>
#include <atomic>
#include <unordered_map>
#include <condition_variable>

namespace Pacha
{
	struct QueuedEvent
	{
		DirectoryNode* node = nullptr;
		std::filesystem::path oldFilenameOrPath;
		DirectoryEventFlags flags = DirectoryEventFlags_ALL_TYPES | DirectoryEventFlags_ALL_EVENTS;

		friend bool operator == (const QueuedEvent& lhs, const QueuedEvent& rhs)
		{
			return lhs.node == rhs.node &&
				lhs.oldFilenameOrPath == rhs.oldFilenameOrPath &&
				lhs.flags == rhs.flags;
		}
	};

	class DirectoryWatcher
	{
	public:
		DirectoryWatcher() = default;
		~DirectoryWatcher();

		void Initialize(const std::vector<std::string>& excludedExtensions);
		void RegisterListener(class DirectoryListener* listener);
		void UnregisterListener(class DirectoryListener* listener);
		void ProcessEvents();

		bool HasPendingEvents();
		const DirectoryNode& GetRootNode();

	private:
		DirectoryNode m_RootNode;
		std::vector<std::string> m_ExcludedExtensions;

		std::mutex m_Mutex;
		std::thread m_WatcherThread;
		std::condition_variable m_ConditionalWait;

		std::atomic_bool m_WatcherThreadReady = false;
		std::atomic_bool m_CloseWatcherThread = false;

		std::vector<QueuedEvent> m_EventQueue = {};
		std::vector<DirectoryListener*> m_Listeners = {};

#if PLATFORM_LINUX
		int m_FileDescriptor = 0;
		std::unordered_map<int, DirectoryNode*> m_FileWatchToNode;
#endif

		void RecursiveTreeBuild(DirectoryNode* root);
		bool IsAnExcludedFileType(const std::string& extension);
		void WatchDirectories();
		
		void PushQueuedEvent(const QueuedEvent& event);
		void PushQueuedEvents(const std::vector<QueuedEvent>& events);
	};
}
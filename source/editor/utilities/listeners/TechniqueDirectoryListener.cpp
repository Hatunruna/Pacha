#include "TechniqueDirectoryListener.h"

namespace Pacha
{
	TechniqueDirectoryListener::TechniqueDirectoryListener()
	{
		m_EventFilter = DirectoryEventFlags_TYPE_FILE | DirectoryEventFlags_ALL_EVENTS;
	}

	void TechniqueDirectoryListener::OnFileAdded(const DirectoryNode*)
	{
	}

	void TechniqueDirectoryListener::OnFileRemoved(const DirectoryNode*)
	{
	}

	void TechniqueDirectoryListener::OnFileModified(const DirectoryNode*)
	{
	}

	void TechniqueDirectoryListener::OnFileRenamed(const DirectoryNode*, const std::filesystem::path&)
	{
	}

    void TechniqueDirectoryListener::OnFileMoved(const DirectoryNode*, const std::filesystem::path&)
    {
    }
}
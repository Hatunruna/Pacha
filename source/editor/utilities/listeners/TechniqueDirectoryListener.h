#pragma once
#include "utilities/DirectoryListener.h"

namespace Pacha
{
	class TechniqueDirectoryListener : public DirectoryListener
	{
	public:
		TechniqueDirectoryListener();
		~TechniqueDirectoryListener() = default;

		void OnFileAdded(const DirectoryNode* file) override;
		void OnFileRemoved(const DirectoryNode* file) override;
		void OnFileModified(const DirectoryNode* file) override;
		void OnFileRenamed(const DirectoryNode* file, const std::filesystem::path& oldName) override;
        void OnFileMoved(const DirectoryNode* file, const std::filesystem::path& oldPath) override;
	};
}
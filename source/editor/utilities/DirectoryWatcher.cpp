#include "DirectoryWatcher.h"

#include "core/Log.h"
#include "core/ProjectConstants.h"

#include <queue>
#include <algorithm>
#include <filesystem>
#if PLATFORM_WINDOWS
#include <Windows.h>
#endif

#if PLATFORM_LINUX
#include <poll.h>
#include <sys/inotify.h>
#endif

namespace Pacha
{
	DirectoryWatcher::~DirectoryWatcher()
	{
		m_CloseWatcherThread = true;
		m_WatcherThread.join();
	}

	void DirectoryWatcher::Initialize(const std::vector<std::string>& excludedExtensions)
	{
		m_RootNode.SetType(DirectoryNodeType::DIRECTORY);

		m_ExcludedExtensions = excludedExtensions;
		m_WatcherThread = std::thread(&DirectoryWatcher::WatchDirectories, this);

		std::unique_lock lock(m_Mutex);
		m_ConditionalWait.wait(lock, [&] { return m_WatcherThreadReady.load(); });

#if PLATFORM_LINUX
		m_RootNode.CreateWatchDescriptor(m_FileDescriptor);
		m_FileWatchToNode[m_RootNode.GetWatchDescriptor()] = &m_RootNode;
#endif

		RecursiveTreeBuild(&m_RootNode);
	}

	void DirectoryWatcher::RegisterListener(DirectoryListener* listener)
	{
		m_Listeners.push_back(listener);
	}

	void DirectoryWatcher::UnregisterListener(DirectoryListener* listener)
	{
		auto found = std::find(m_Listeners.begin(), m_Listeners.end(), listener);
		if (found != m_Listeners.end())
			m_Listeners.erase(found);
	}

	void DirectoryWatcher::ProcessEvents()
	{
		if (m_EventQueue.empty())
			return;

		std::unique_lock<std::mutex> lock(m_Mutex);

		for (auto const& queuedEvent : m_EventQueue)
		{
			for (auto const& listener : m_Listeners)
			{
				if (listener->PassesEventFilter(queuedEvent.flags))
				{
					if (queuedEvent.flags & DirectoryEventFlags_TYPE_FILE)
					{
						if (queuedEvent.flags & DirectoryEventFlags_ADDED)
							listener->OnFileAdded(queuedEvent.node);
						else if (queuedEvent.flags & DirectoryEventFlags_REMOVED)
							listener->OnFileRemoved(queuedEvent.node);
						else if (queuedEvent.flags & DirectoryEventFlags_MODIFIED)
							listener->OnFileModified(queuedEvent.node);
						else if (queuedEvent.flags & DirectoryEventFlags_RENAMED)
							listener->OnFileRenamed(queuedEvent.node, queuedEvent.oldFilenameOrPath);
						else if (queuedEvent.flags & DirectoryEventFlags_MOVED)
							listener->OnFileMoved(queuedEvent.node, queuedEvent.oldFilenameOrPath);
					}
					else if (queuedEvent.flags & DirectoryEventFlags_TYPE_DIRECTORY)
					{
						if (queuedEvent.flags & DirectoryEventFlags_ADDED)
							listener->OnDirectoryAdded(queuedEvent.node);
						else if (queuedEvent.flags & DirectoryEventFlags_REMOVED)
							listener->OnDirectoryRemoved(queuedEvent.node);
						else if (queuedEvent.flags & DirectoryEventFlags_MODIFIED)
							listener->OnDirectoryModified(queuedEvent.node);
						else if (queuedEvent.flags & DirectoryEventFlags_RENAMED)
							listener->OnDirectoryRenamed(queuedEvent.node, queuedEvent.oldFilenameOrPath);
						else if (queuedEvent.flags & DirectoryEventFlags_MOVED)
							listener->OnDirectoryMoved(queuedEvent.node, queuedEvent.oldFilenameOrPath);
					}
				}
			}

			if (queuedEvent.flags & DirectoryEventFlags_REMOVED)
				queuedEvent.node->GetParent()->DeleteChild(queuedEvent.node->GetFilename());
		}

		m_EventQueue.clear();
	}

	bool DirectoryWatcher::HasPendingEvents()
	{
		std::unique_lock<std::mutex> lock(m_Mutex);
		return m_EventQueue.empty();
	}

	const DirectoryNode& DirectoryWatcher::GetRootNode()
	{
		return m_RootNode;
	}

	void DirectoryWatcher::RecursiveTreeBuild(DirectoryNode* parent)
	{
		std::filesystem::directory_iterator directory(parent->GetFullPath());

		for (auto const& it : directory)
		{
			const std::filesystem::path& path = it.path();
			if (it.is_directory() || (it.is_regular_file() && !IsAnExcludedFileType(path.extension().string())))
			{
				DirectoryNode* childNode = parent->CreateChild(path.filename() LINUX_PARAMETER(m_FileDescriptor));
				if (it.is_directory())
				{
#if PLATFORM_LINUX
					m_FileWatchToNode[childNode->GetWatchDescriptor()] = childNode;
#endif
					RecursiveTreeBuild(childNode);
					childNode->SortChildren(DirectorySortCriteria::SORT_BY_NAME, DirectorySortDirection::ASCENDING);
				}
			}
		}

		parent->SortChildren(DirectorySortCriteria::SORT_BY_NAME, DirectorySortDirection::ASCENDING);
	}

	bool DirectoryWatcher::IsAnExcludedFileType(const std::string& extension)
	{
		return std::find(m_ExcludedExtensions.begin(), m_ExcludedExtensions.end(), extension) != m_ExcludedExtensions.end();
	}

	void DirectoryWatcher::WatchDirectories()
	{
		auto GatherAddEvents = [](DirectoryNode* parentNode) -> std::vector<QueuedEvent>
		{
			std::vector<QueuedEvent> events;
			std::queue<DirectoryNode*> nodes;

			if (parentNode->HasChildNodes())
				for (auto& child : parentNode->GetChildren())
					nodes.push(const_cast<DirectoryNode*>(&child));

			while (!nodes.empty())
			{
				DirectoryNode* current = nodes.front();
				const bool isDirectory = current->GetType() == DirectoryNodeType::DIRECTORY;

				if (current->HasChildNodes())
					for (auto& child : current->GetChildren())
						nodes.push(const_cast<DirectoryNode*>(&child));

				QueuedEvent directoryEvent = {};
				directoryEvent.node = current;
				directoryEvent.flags = DirectoryEventFlags_ADDED;
				directoryEvent.flags |= isDirectory ? DirectoryEventFlags_TYPE_DIRECTORY : DirectoryEventFlags_TYPE_FILE;

				events.push_back(directoryEvent);
				nodes.pop();
			}

			return events;
		};
		auto GatherDeleteEvents = [](DirectoryNode* parentNode) -> std::vector<QueuedEvent>
		{
			std::vector<QueuedEvent> events;
			std::queue<DirectoryNode*> nodes;

			if (parentNode->HasChildNodes())
				for (auto& child : parentNode->GetChildren())
					nodes.push(const_cast<DirectoryNode*>(&child));

			while (!nodes.empty())
			{
				DirectoryNode* current = nodes.front();
				const bool isDirectory = current->GetType() == DirectoryNodeType::DIRECTORY;

				if (current->HasChildNodes())
					for (auto& child : current->GetChildren())
						nodes.push(const_cast<DirectoryNode*>(&child));

				QueuedEvent directoryEvent = {};
				directoryEvent.node = current;
				directoryEvent.flags = DirectoryEventFlags_REMOVED;
				directoryEvent.flags |= isDirectory ? DirectoryEventFlags_TYPE_DIRECTORY : DirectoryEventFlags_TYPE_FILE;

				events.push_back(directoryEvent);
				nodes.pop();
			}

			std::reverse(events.begin(), events.end());
			return events;
		};
		auto GatherMovedEvents = [](DirectoryNode* parentNode, const std::filesystem::path& oldRootPath) -> std::vector<QueuedEvent>
		{
			std::vector<QueuedEvent> events;
			std::queue<DirectoryNode*> nodes;

			std::filesystem::path newRootPath = parentNode->GetRelativePath();

			if (parentNode->HasChildNodes())
				for (auto& child : parentNode->GetChildren())
					nodes.push(const_cast<DirectoryNode*>(&child));

			while (!nodes.empty())
			{
				DirectoryNode* current = nodes.front();
				const bool isDirectory = current->GetType() == DirectoryNodeType::DIRECTORY;

				QueuedEvent directoryEvent = {};
				directoryEvent.node = current;
				directoryEvent.oldFilenameOrPath = oldRootPath / std::filesystem::relative(current->GetRelativePath(), newRootPath);
				directoryEvent.flags = DirectoryEventFlags_MOVED;
				directoryEvent.flags |= isDirectory ? DirectoryEventFlags_TYPE_DIRECTORY : DirectoryEventFlags_TYPE_FILE;

				events.push_back(directoryEvent);

				if (current->HasChildNodes())
					for (auto& child : current->GetChildren())
						nodes.push(const_cast<DirectoryNode*>(&child));

				nodes.pop();
			}

			return events;
		};

		auto AddNewNode = [&](DirectoryNode* parentNode, const std::filesystem::path& filename)
		{
			if (DirectoryNode* newNode = parentNode->CreateChild(filename LINUX_PARAMETER(m_FileDescriptor)))
			{
				const bool isDirectory = newNode->GetType() == DirectoryNodeType::DIRECTORY;

				QueuedEvent directoryEvent = {};
				directoryEvent.flags = DirectoryEventFlags_ADDED;
				directoryEvent.flags |= isDirectory ? DirectoryEventFlags_TYPE_DIRECTORY : DirectoryEventFlags_TYPE_FILE;
				directoryEvent.node = newNode;

				if (isDirectory)
				{
					RecursiveTreeBuild(newNode);
					PushQueuedEvents(GatherAddEvents(newNode));
				}

				PushQueuedEvent(directoryEvent);
			}
		};
		auto NodeModified = [&](DirectoryNode* parentNode, const std::filesystem::path& filename)
		{
			if (DirectoryNode* currentNode = parentNode->FindChild(filename))
			{
				const bool isDirectory = currentNode->GetType() == DirectoryNodeType::DIRECTORY;

				QueuedEvent directoryEvent = {};
				directoryEvent.node = currentNode;
				directoryEvent.flags = DirectoryEventFlags_MODIFIED;
				directoryEvent.flags |= isDirectory ? DirectoryEventFlags_TYPE_DIRECTORY : DirectoryEventFlags_TYPE_FILE;

				PushQueuedEvent(directoryEvent);
			}
		};
		auto DeleteNode = [&](DirectoryNode* parentNode, const std::filesystem::path& filename)
		{
			if (DirectoryNode* childToDelete = parentNode->FindChild(filename))
			{
				const bool isDirectory = childToDelete->GetType() == DirectoryNodeType::DIRECTORY;

				QueuedEvent directoryEvent = {};
				directoryEvent.node = childToDelete;
				directoryEvent.flags = DirectoryEventFlags_REMOVED;
				directoryEvent.flags |= isDirectory ? DirectoryEventFlags_TYPE_DIRECTORY : DirectoryEventFlags_TYPE_FILE;

				if (isDirectory)
					PushQueuedEvents(GatherDeleteEvents(childToDelete));

				PushQueuedEvent(directoryEvent);
			}
		};
		auto NodeRenamed = [&](DirectoryNode* renamedNode, const std::filesystem::path& newFilename)
		{
			const bool isDirectory = renamedNode->GetType() == DirectoryNodeType::DIRECTORY;

			std::filesystem::path oldRelativePath = renamedNode->GetRelativePath();
			std::filesystem::path newRelativePath = oldRelativePath;

			QueuedEvent directoryEvent = {};
			directoryEvent.node = renamedNode;
			directoryEvent.oldFilenameOrPath = oldRelativePath.filename();
			directoryEvent.flags = DirectoryEventFlags_RENAMED;
			directoryEvent.flags |= isDirectory ? DirectoryEventFlags_TYPE_DIRECTORY : DirectoryEventFlags_TYPE_FILE;

			newRelativePath.replace_filename(newFilename.filename());
			renamedNode->SetRelativePath(newRelativePath);
			renamedNode->FixupRelativePath();

			if (isDirectory)
				PushQueuedEvents(GatherMovedEvents(renamedNode, oldRelativePath));

			PushQueuedEvent(directoryEvent);
		};
		auto NodeMoved = [&](DirectoryNode* parentNode, DirectoryNode* movedNode)
		{
			QueuedEvent directoryEvent = {};
			directoryEvent.oldFilenameOrPath = movedNode->GetRelativePath();

			if (DirectoryNode* newNode = movedNode->MoveToNewParent(parentNode))
			{
				const bool isDirectory = newNode->GetType() == DirectoryNodeType::DIRECTORY;

				directoryEvent.node = newNode;
				directoryEvent.flags = DirectoryEventFlags_MOVED;
				directoryEvent.flags |= isDirectory ? DirectoryEventFlags_TYPE_DIRECTORY : DirectoryEventFlags_TYPE_FILE;

				if (isDirectory)
					PushQueuedEvents(GatherMovedEvents(newNode, directoryEvent.oldFilenameOrPath));

				PushQueuedEvent(directoryEvent);
			}
		};

#if PLATFORM_WINDOWS
		struct FrameCountedNodeToRemove
		{
			FrameCountedNodeToRemove(DirectoryNode* inNode)
				: node(inNode) {}

			uint32_t frame = 2;
			DirectoryNode* node = nullptr;
		};

		HANDLE watchedDirectoryHandle = CreateFileW(m_RootNode.GetFullPath().c_str(), FILE_LIST_DIRECTORY, FILE_SHARE_READ | FILE_SHARE_WRITE, nullptr, OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS | FILE_FLAG_OVERLAPPED, nullptr);
		if (watchedDirectoryHandle == INVALID_HANDLE_VALUE)
		{
			LOGCRITICAL("Failed to open directory to watch.");
			return;
		}

		m_WatcherThreadReady = true;
		m_ConditionalWait.notify_one();
		std::vector<FrameCountedNodeToRemove> trackedNodesToRemove = {};

		DWORD notificationBufferSize = 1024;
		BYTE* notificationBuffer = new BYTE[notificationBufferSize];
		const DWORD notificationTypeFilter = FILE_NOTIFY_CHANGE_LAST_WRITE | FILE_NOTIFY_CHANGE_CREATION | FILE_NOTIFY_CHANGE_FILE_NAME | FILE_NOTIFY_CHANGE_DIR_NAME;

		OVERLAPPED overlap = { 0 };
		overlap.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

		if (!overlap.hEvent)
		{
			LOGCRITICAL("Failed to create overlap event.");
			return;
		}

		while (!m_CloseWatcherThread)
		{
			DWORD bytesRead = 0;
			bool readingOperationsQueued = ReadDirectoryChangesW(watchedDirectoryHandle, notificationBuffer, notificationBufferSize, TRUE, notificationTypeFilter, &bytesRead, &overlap, nullptr);

			if (!readingOperationsQueued)
			{
				LOGCRITICAL("Failed to read directory changes.");
				break;
			}

			while (true)
			{
				bool ready = GetOverlappedResult(watchedDirectoryHandle, &overlap, &bytesRead, FALSE);
				bool needsWait = !ready && GetLastError() == ERROR_IO_INCOMPLETE;

				if (needsWait)
				{
					WaitForSingleObject(overlap.hEvent, 100);
					ready = GetOverlappedResult(watchedDirectoryHandle, &overlap, &bytesRead, FALSE);
				}

				if (ready)
				{
					DirectoryNode* nodeToRename = nullptr;
					BYTE* notificationBufferPtr = notificationBuffer;
					FILE_NOTIFY_INFORMATION* notificationInfo = nullptr;

					std::unique_lock<std::mutex> lock(m_Mutex);

					do
					{
						notificationInfo = reinterpret_cast<FILE_NOTIFY_INFORMATION*>(notificationBufferPtr);
						const std::filesystem::path relativePath = std::wstring(notificationInfo->FileName, notificationInfo->FileNameLength / sizeof(wchar_t));
						const std::filesystem::path fullPath = m_RootNode.GetFullPath() / relativePath;
						const std::filesystem::path filename = fullPath.filename();
						const std::filesystem::path parentPath = std::filesystem::relative(fullPath.parent_path(), ProjectConstants::GetInstance().GetAssetsPath());;

						if (!std::filesystem::is_directory(fullPath) && IsAnExcludedFileType(fullPath.extension().string()))
						{
							notificationBufferPtr += notificationInfo->NextEntryOffset;
							continue;
						}

						DirectoryNode* parentNode = &m_RootNode;
						if (!parentPath.empty())
						{
							if (DirectoryNode* newParentNode = m_RootNode.FindChildRecursive(parentPath))
								parentNode = newParentNode;
						}

						switch (notificationInfo->Action)
						{
							case FILE_ACTION_ADDED:
							{
								bool nodeMoved = false;
								for (auto iterator = trackedNodesToRemove.begin(); iterator != trackedNodesToRemove.end(); ++iterator)
								{
									if (iterator->node->GetFilename() == filename)
									{
										NodeMoved(parentNode, iterator->node);
										trackedNodesToRemove.erase(iterator);
										nodeMoved = true;
										break;
									}
								}

								if (!nodeMoved)
									AddNewNode(parentNode, filename);

								break;
							}
							case FILE_ACTION_REMOVED:
							{
								if (DirectoryNode* childToDelete = parentNode->FindChild(filename))
									trackedNodesToRemove.push_back(childToDelete);
								break;
							}
							case FILE_ACTION_MODIFIED:
							{
								NodeModified(parentNode, filename);
								break;
							}
							case FILE_ACTION_RENAMED_OLD_NAME:
							{
								if (DirectoryNode* currentNode = parentNode->FindChild(filename))
									nodeToRename = currentNode;
								break;
							}
							case FILE_ACTION_RENAMED_NEW_NAME:
							{
								if (nodeToRename)
								{
									NodeRenamed(nodeToRename, filename);
									nodeToRename = nullptr;
								}
								break;
							}
						}

						notificationBufferPtr += notificationInfo->NextEntryOffset;
					} while (notificationInfo->NextEntryOffset > 0);

					break;
				}

				for (auto iterator = trackedNodesToRemove.begin(); iterator != trackedNodesToRemove.end();)
				{
					if (--iterator->frame == 0)
					{
						DeleteNode(iterator->node->GetParent(), iterator->node->GetFilename());
						iterator = trackedNodesToRemove.erase(iterator);
					}
					else
						++iterator;
				}

				if (m_CloseWatcherThread)
				{
					CancelIo(watchedDirectoryHandle);
					break;
				}
			}
		}

		delete[] notificationBuffer;
		CloseHandle(watchedDirectoryHandle);
		CloseHandle(overlap.hEvent);
#endif

#if PLATFORM_LINUX
		m_FileDescriptor = inotify_init1(IN_NONBLOCK);
		if (m_FileDescriptor == -1)
		{
			LOGCRITICAL("Failed to initialize iNotify subsystem.");
			return;
		}

		m_WatcherThreadReady = true;
		m_ConditionalWait.notify_one();

		const size_t kBufferSize = (sizeof(inotify_event) + FILENAME_MAX) * 1024;
		uint8_t* buffer = new uint8_t[kBufferSize];

		pollfd pollRequest = {};
		pollRequest.fd = m_FileDescriptor;
		pollRequest.events = POLLIN;

		std::unordered_map<uint32_t, DirectoryNode*> movedFromNodes;

		while (!m_CloseWatcherThread)
		{
			if (poll(&pollRequest, 1, 100) > 0 && (pollRequest.revents & POLLIN))
			{
				ssize_t bytesRead = read(m_FileDescriptor, buffer, kBufferSize);
				if (bytesRead != -1)
				{
					std::unique_lock<std::mutex> lock(m_Mutex);

					uint32_t readOffset = 0;
					while (readOffset < bytesRead)
					{
						inotify_event* event = reinterpret_cast<inotify_event*>(buffer + readOffset);

						auto parent = m_FileWatchToNode.find(event->wd);
						if (parent != m_FileWatchToNode.end())
						{
							DirectoryNode* parentNode = parent->second;
							const std::filesystem::path filename = event->name;
							const std::filesystem::path fullPath = parentNode->GetFullPath() / filename;
							const std::filesystem::path relativePath = std::filesystem::relative(fullPath, ServiceLocator::GetProjectConstants().GetAssetsPath());

							if ((!std::filesystem::is_directory(fullPath) && IsAnExcludedFileType(fullPath.extension().string())))
							{
								readOffset += sizeof(inotify_event) + event->len;
								continue;
							}

							if (event->mask & IN_CREATE)
							{
								AddNewNode(parentNode, filename);
							}
							else if (event->mask & IN_DELETE)
							{
								DeleteNode(parentNode, filename);
							}
							else if (event->mask & IN_MODIFY)
							{
								NodeModified(parentNode, filename);
							}
							else if (event->mask & IN_MOVED_FROM)
							{
								if (DirectoryNode* currentNode = parentNode->FindChild(filename))
									movedFromNodes[event->cookie] = currentNode;
							}
							else if (event->mask & IN_MOVED_TO)
							{
								if (movedFromNodes.count(event->cookie))
								{
									DirectoryNode* movedNode = movedFromNodes[event->cookie];
									if (movedNode->GetParent() != parentNode)
										NodeMoved(parentNode, movedNode);
									else
										NodeRenamed(movedNode, filename);

									movedFromNodes.erase(event->cookie);
								}
								else
								{
									AddNewNode(parentNode, filename);
								}
							}
						}

						readOffset += sizeof(inotify_event) + event->len;
					}
				}

				for (auto& [cookie, node] : movedFromNodes)
					DeleteNode(node->GetParent(), node->GetFilename());

				movedFromNodes.clear();
			}
		}

		delete[] buffer;
#endif
	}

	void DirectoryWatcher::PushQueuedEvent(const QueuedEvent& event)
	{
		const auto& it = std::find(m_EventQueue.begin(), m_EventQueue.end(), event);
		if (it == m_EventQueue.end())
			m_EventQueue.push_back(event);
	}

	void DirectoryWatcher::PushQueuedEvents(const std::vector<QueuedEvent>& events)
	{
		for (auto& event : events)
		{
			const auto& it = std::find(m_EventQueue.begin(), m_EventQueue.end(), event);
			if (it == m_EventQueue.end())
				m_EventQueue.push_back(event);
		}
	}
}

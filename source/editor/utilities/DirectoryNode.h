#pragma once
#include "core/SystemInfo.h"

#include <list>
#include <string>
#include <filesystem>
#include <unordered_map>
#include <unordered_set>

namespace Pacha
{
	enum class DirectorySortCriteria
	{
		SORT_BY_NAME,
		SORT_BY_TYPE
	};

	enum class DirectorySortDirection
	{
		NONE,
		ASCENDING,
		DESCENDING
	};

	enum class DirectoryNodeType
	{
		//Keep sorted alphabetically
		DIRECTORY,
		GENERIC,
		LEVEL,
		MATERIAL,
		MODEL,
		SHADER_INCLUDE,
		TECHNIQUE,
		TEXTURE
	};

	class DirectoryNode
	{
	public:
		void SetRelativePath(const std::filesystem::path& relativePath);

		DirectoryNodeType GetType() const { return m_Type; }
		void SetType(const DirectoryNodeType type) { m_Type = type; }

		DirectoryNode* GetParent() const { return m_Parent; }
		void SetParent(DirectoryNode* parent) { m_Parent = parent; }

		const std::filesystem::path GetFullPath();
		const std::filesystem::path GetFilename();
		const std::filesystem::path& GetRelativePath() const { return m_RelativePath; }

		const std::list<DirectoryNode>& GetChildren() const { return m_Children; }

#if PLATFORM_LINUX
		int32_t GetWatchDescriptor() { return m_WatchDescriptor; }
		void CreateWatchDescriptor(int32_t fileDescriptor);
#endif
		DirectoryNode* FindChild(const std::filesystem::path& filename);
		DirectoryNode* FindChildRecursive(const std::filesystem::path& relativePath);
		DirectoryNode* CreateChild(const std::filesystem::path& filename LINUX_PARAMETER(int fileDescriptor));
		DirectoryNode* CreateChildCopy(DirectoryNode* nodeToCopy);
		void DeleteChild(const std::filesystem::path& filename);
		DirectoryNode* MoveToNewParent(DirectoryNode* newParent);

		std::unordered_set<DirectoryNode*> GatherParents();
		void FindInChildrenRecursive(const std::string& searchString, std::vector<DirectoryNode*>& searchResult) const;

		bool HasChildNodes() const;
		bool HasDirectoryChildNodes() const;
		void FixupRelativePath();

		const char* GetTypeAsString() const;
		void SortChildren(const DirectorySortCriteria& criteria, const DirectorySortDirection& direction);

	private:
		std::filesystem::path m_RelativePath = {};

		DirectoryNodeType m_Type;
		DirectoryNode* m_Parent = nullptr;
		std::list<DirectoryNode> m_Children;

#if PLATFORM_LINUX
		int32_t m_WatchDescriptor = 0;
#endif

		void FixupRelativePathRecursive(DirectoryNode* node);
		DirectoryNodeType GetNodeType(const  std::filesystem::path& path);
		friend bool operator == (const DirectoryNode& lhs, const DirectoryNode& rhs);
	};
}
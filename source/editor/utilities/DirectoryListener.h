#pragma once
#include "DirectoryNode.h"

namespace Pacha
{
	enum DirectoryEventFlagBits
	{
		DirectoryEventFlags_ADDED			= 1 << 0,
		DirectoryEventFlags_REMOVED			= 1 << 1,
		DirectoryEventFlags_RENAMED			= 1 << 2,
		DirectoryEventFlags_MODIFIED		= 1 << 3,
        DirectoryEventFlags_MOVED		    = 1 << 4,

		DirectoryEventFlags_TYPE_FILE		= 1 << 5,
		DirectoryEventFlags_TYPE_DIRECTORY	= 1 << 6,

		DirectoryEventFlags_ALL_TYPES		= DirectoryEventFlags_TYPE_FILE | DirectoryEventFlags_TYPE_DIRECTORY,
		DirectoryEventFlags_ALL_EVENTS		= DirectoryEventFlags_ADDED | DirectoryEventFlags_REMOVED | DirectoryEventFlags_RENAMED | DirectoryEventFlags_MODIFIED | DirectoryEventFlags_MOVED
	};
	typedef uint32_t DirectoryEventFlags;

	class DirectoryListener
	{
	public:
		virtual ~DirectoryListener() = default;

		virtual void OnFileAdded(const DirectoryNode*) {};
		virtual void OnFileRemoved(const DirectoryNode*) {};
		virtual void OnFileModified(const DirectoryNode*) {};
		virtual void OnFileRenamed(const DirectoryNode*, const std::filesystem::path&) {};
        virtual void OnFileMoved(const DirectoryNode*, const std::filesystem::path&) {};

		virtual void OnDirectoryAdded(const DirectoryNode*) {};
		virtual void OnDirectoryRemoved(const DirectoryNode*) {};
		virtual void OnDirectoryModified(const DirectoryNode*) {};
		virtual void OnDirectoryRenamed(const DirectoryNode*, const std::filesystem::path&) {};
        virtual void OnDirectoryMoved(const DirectoryNode*, const std::filesystem::path&) {};

		bool PassesEventFilter(const DirectoryEventFlags eventType) const { return eventType & m_EventFilter; }
	
	protected:
		DirectoryEventFlags m_EventFilter = DirectoryEventFlags_ALL_TYPES | DirectoryEventFlags_ALL_EVENTS;
	};
}
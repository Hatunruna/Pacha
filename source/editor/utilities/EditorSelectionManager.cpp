#include "EditorSelectionManager.h"
#include "ecs/components/MeshRenderer.h"

namespace Pacha
{
	void EditorSelectionManager::AddToSelection(Entity* entity)
	{
		if (!IsInSelection(entity))
		{
			m_SelectedEntities.push_back(entity);

			if(MeshRenderer* meshRenderer = entity->GetComponent<MeshRenderer>())
				meshRenderer->SetSelected(true);

			for (auto& meshRenderers : entity->GetComponentsInChildren<MeshRenderer>())
				meshRenderers->SetSelected(true);
		}
	}

	void EditorSelectionManager::RemoveFromSelection(Entity* entity)
	{
		auto found = std::find(m_SelectedEntities.begin(), m_SelectedEntities.end(), entity);
		if (found != m_SelectedEntities.end())
		{
			m_SelectedEntities.erase(found);

			if (MeshRenderer* meshRenderer = entity->GetComponent<MeshRenderer>())
				meshRenderer->SetSelected(false);

			for (auto& meshRenderers : entity->GetComponentsInChildren<MeshRenderer>())
				meshRenderers->SetSelected(false);
		}
	}

	bool EditorSelectionManager::IsInSelection(Entity* entity)
	{
		return std::find(m_SelectedEntities.begin(), m_SelectedEntities.end(), entity) != m_SelectedEntities.end();
	}
	
	void EditorSelectionManager::ClearSelection()
	{
		for (auto& entity : m_SelectedEntities)
		{
			if (MeshRenderer* meshRenderer = entity->GetComponent<MeshRenderer>())
				meshRenderer->SetSelected(false);

			for (auto& meshRenderers : entity->GetComponentsInChildren<MeshRenderer>())
				meshRenderers->SetSelected(false);
		}

		m_SelectedEntities.clear();
	}

	size_t EditorSelectionManager::GetSelectionCount()
	{
		return m_SelectedEntities.size();
	}
	
	std::vector<Entity*>& EditorSelectionManager::GetSelectedEntities()
	{
		return m_SelectedEntities;
	}

	bool EditorSelectionManager::HasLevelViewSelection()
	{
		return m_LevelViewSelection.size() > 0;
	}

	void EditorSelectionManager::AddLevelViewSelection(Entity* entity)
	{
		if (!IsInSelection(entity))
		{
			AddToSelection(entity);
			m_LevelViewSelection.push_back(entity);
		}
	}
	
	void EditorSelectionManager::ClearLevelViewSelection()
	{
		m_LevelViewSelection.clear();
	}
	
	std::vector<Entity*>& EditorSelectionManager::GetLevelViewSelectedEntities()
	{
		return m_LevelViewSelection;
	}
}

#include <core/Pacha.h>
using namespace Pacha;

int main(int argc, char* argv[])
{
	Application app;
	app.SetFrameLimiter(120);
	app.SetVersion(0, 1, 0);

	if (app.Initialize(argc, argv))
		app.Run();

	app.Destroy();
	return EXIT_SUCCESS;
}